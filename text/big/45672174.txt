Naina (2002 film)
{{Infobox film
| name           = Naina
| image          =
| image_size     =
| caption        =
| director       = Manobala
| producer       = V. Santhakumar
| writer         = Jagan Mohan  (dialogues) 
| screenplay     = Manobala
| story          = P. Kalaimani
| starring       =  
| music          = Sabesh-Murali
| cinematography = Pandian
| editing        = V. M. Udhayashankar
| distributor    =
| studio         = VSK Films
| released       =  
| runtime        = 145 minutes
| country        = India
| language       = Tamil
}}
 2002 Tamil Tamil comedy Manya in lead roles, with Vadivelu, Ramesh Khanna, Bhanupriya and Kovai Sarala playing supporting roles. The film, produced by V. Santhakumar, had musical score by Sabesh-Murali and was released on 30 August 2002.   

==Plot==

The film begins with Annamalai (Jayaram) who is a ghost trying to talk to his son Pasupathy (Jayaram) but nobody has been able to see or hear him since his death. Pasupathy is a taxi driver and he is in love with the lawyer Vaanathi (Manya (actress)|Manya). Aavudaiyappan (Vadivelu), a con medium, becomes the only one who can hear Annamalai. After being annoyed by Annamalai, Aavudaiyappan finally decides to help him. They meet Pasupathy but Pasupathy seems to hate his deceased father.

In the past, the widower Annamalai was a wealthy man and a womaniser. Every time there was a good news in his village, he tonsured Pasupathys head. He later got married a second time with Azhagu Nachiyar (Bhanupriya) and they had a daughter. During a ceremony, a person pushed from behind in Yajna. The villagers thought that the innocent Pasupathy killed his father. So he ran away.

Annamalai doesnt know the one who killed him. Pasupathy decides to go back to his village. He wants to prove his innocence at all costs and wants to find the one who killed his father. When Azhagu Nachiyar meets Velu who is the brother of Annamalai she gets unconscious. Annamalai regains her consciousness and make her to overhear Velus conversation. It turns out that Velu had killed Annamalai to grab his property and blamed the murder on Pasupathy. In the climax, Velu will be performing the last rites of his father while velu tries to disrupt it. Velu gets burnt and he also becomes a ghost. In the end, Pasupathy joins his fathers position in the village.

==Cast==

 
*Jayaram as Annamalai and Pasupathy Manya as Vaanathi
*Vadivelu as Aavudaiyappan
*Ramesh Khanna as Pichu
*Bhanupriya as Azhagu Nachiyar
*Kovai Sarala as Angayarkanni
*Venniradai Moorthy as Mathrubootham
*Delhi Ganesh as Sambu
*Charle as Pazhani
*Vaiyapuri
*Rajan P. Dev as Velu
*Thalaivasal Vijay Pandu as Pandu and Pundu Thyagu as Vedigundu Muthu
*Madhan Bob as Annamalai
*Chelladurai as Chelladurai
*Bonda Mani as Mani
*Jayamani
*Aruldass
*Master Bharath as young Pasupathy
 

==Soundtrack==

{{Infobox album |  
| Name        = Naina
| Type        = soundtrack
| Artist      = Sabesh-Murali
| Cover       = 
| Released    = 2002
| Recorded    = 2002 Feature film soundtrack
| Length      = 32:51
| Label       = 
| Producer    = Sabesh-Murali
| Reviews     = 
}}

The film score and the soundtrack were composed by film composer Sabesh-Murali]. The soundtrack, released in 2002, features 6 tracks with lyrics written by the director P. Vijay, Viveka and K. Subash. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Track !! Song !! Singer(s) !! Duration
|- 1 || Annamalai Annamalai || Deva (music director)|Deva, Krishnaraj || 5:01
|- 1 || Annamalai Annamalai || Vadivelu, Krishnaraj || 4:59
|- 3 || Jaathi Ponnae || Karthik (singer)|Karthik, Chitra Sivaraman || 5:12
|- 4 || Kaadhalanae Uyire || Srinivas (singer)|Srinivas, Anuradha Sriram || 5:47
|- 5 || Kichu Kichu || Rajiv Menon, Mathangi Jagdish || 5:46
|- 6 || Pallikoodam Sellum Megamei || P. Unni Krishnan, Harini || 6:06
|}

==References==
 

 
 
 
 
 