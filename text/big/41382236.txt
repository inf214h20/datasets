Splinters in the Navy
{{Infobox film
| name = Splinters in the Navy
| image =
| image_size =
| caption =
| director = Walter Forde
| producer = Julius Hagen Jack Marks   Harry Fowler Mear   Bert Lee   Robert Patrick Weston
| narrator =
| starring = Sydney Howard   Alf Goddard   Helena Pickard   Paddy Browne
| music = W.L. Trytel
| cinematography = Sydney Blythe Jack Harris
| studio = Twickenham Studios
| distributor = Woolf & Freedman Film Service
| released = November 1931
| runtime = 76 minutes
| country = United Kingdom English 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}}
Splinters in the Navy (1931 in film|1931) is a British comedy film directed by Walter Forde and starring Sydney Howard, Alf Goddard, and Helena Pickard. The film was made at Twickenham Studios, and is a sequel to the film Splinters (1929 film)|Splinters (1929), about an army concert party. A further sequel, Splinters in the Air, was released in 1937. 

==Synopsis== variety performance. Meanwhile Joe Crabbs attempts to win back his girlfriend from the Navys boxing champion.

==Cast==
* Sydney Howard as Joe Crabbs 
* Alf Goddard as Spike Higgins 
* Helena Pickard as Lottie 
* Frederick Bentley as Bill Miffins 
* Paddy Browne as Mabel 
* Rupert Lister as Admiral 
* Harold Heath as Master-at-Arms  Ian Wilson as Call Boy 
* Lew Lake as Himself 
* Hal Jones as Himself 
* Reg Stone as Himself 
* Wilfred Temple as Himself 
* Laurie Lawrence as Petty Officer 
* Thomas Thurban as Bandmaster

==References==
 

==Bibliography==
* Low, Rachael. Filmmaking in 1930s Britain. George Allen & Unwin, 1985.
* Sutton, David R. A Chorus of Raspberries: British Film Comedy 1929-1939. University of Exeter Press, 2000.
* Wood, Linda. British Films, 1927-1939. British Film Institute, 1986.

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 


 