Whose Baby?
 
{{Infobox film
| name           = Whose Baby?
| image          =
| caption        =
| director       = Clarence G. Badger
| producer       = Mack Sennett (Keystone Studios)
| writer         =
| starring       = Gloria Swanson
| music          =
| cinematography =
| editing        =
| distributor    = Triangle Film Corporation
| released       =  
| runtime        = 2 reels
| country        = United States Silent English intertitles
| budget         =
}}
 silent comedy film directed by Clarence G. Badger and starring Gloria Swanson.   

==Cast==
* Bobby Vernon
* Gloria Swanson
* Jay Dwiggins
* Martha Trick
* Robert Milliken
* Fritz Schade
* Juanita Hansen
* Sylvia Ashton
* Helen Bray
* Florence Clark
* Phyllis Haver William Irving
* Edgar Kennedy
* Myrtle Lind
* Roxana McGowan
* Virginia Nightingale
* Marvel Rea
* Earle Rodney
* Vera Steadman
* Ethel Teare
* Edith Valk
* Guy Woodward

==Reception== city and state film censorship boards. The Chicago Board of Censors cut a scene of a heavy woman sliding down a railing, two scenes of her falling in a gymnasium, and a closeup of her backed up against a wall. 

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 

 