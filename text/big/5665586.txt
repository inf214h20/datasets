Who I Am And What I Want
{{Infobox film
| name = Who I Am And What I Want
| image = wia.jpeg
| genre = Black Comedy
| director = Chris Shepherd and David Shrigley
| producer = Maria Manton  
| writer = Chris Shepherd and David Shrigley based a book by David Shrigley
| starring = Kevin Eldon
| distributor = onedotzero
| released =  
| certificate = UK18 English
| Japanese
| runtime = 7 minutes
| format = NTSC, DVD REGION 0
| catalogue number = odzdvdwia001
| barcode = 5060065280071
}}
 short directed by Chris Shepherd and David Shrigley in 2005. The film is based on the 2003 David Shrigley book of the same title.    Kevin Eldon voices the role of the films main character, Pete. The DVD was released in 2006 with a 12-page booklet with art from the film. 

==Story==
Pete lives in the woods. He survives by hunting and killing. He is an outcast from society who finds happiness in being alone. He recalls the key moments that led him to turn his back on his hometown. Pete is in a state of denial about the past, drinking, mental illness and what happens when he does not take his medication is all one big laugh to him.

==History==
This quirky funny black and white animation was commissioned in 2004 by the animate! project. On completion it was given an 18 certificate by the British Board of Film Classification, a result which David Shrigley said he was proud of. David Shrigley and Chris Shepherd talked for several years about making a film. David Shrigleys 2003 book of the same title had a story which featured a narrative, a fictional autobiography which they realised was a perfect basis for a film. 

Who I am And What I Want was programmed alongside Farbers Nerve by Morgan Miller in the 2006 Manhattan Short Film Festivals world tour, which included 137 screenings across
the US, Canada, the UK, and Europe.    Both animations were in black and white.

==Quotes==
Pete: "My name is mushroom. My name is toadstool. My name is spore. My name is fungus. My name is mildew. My name is bog, fen, marsh and swamp. My name is truffle. My name is bacteria. My name is muck...but you can call me Pete."

Pete: "Everyone around here knows me..."  Pete walks along the highstreet, he walks up to the bouncer of the Golden Nugget Bar.
Pete: "Morning" 
Bouncer: "Fuck Off!" 
Pete: "Im not allowed in the Golden Nugget...Not anymore. I dont know why Im not allowed in there. I cant remember having done anything". 

Pete: "I want to feel you breathing upon me, and I want to be water-soluble so that when I go water skiing and fall off I will dissolve before the sharks can eat me".

Pete: "I dont want to live with a herd of twats who are going to make me wear clothes and eat with a knife and fork.".

==Awards==
*2006:Winner of Tiger Short Film Award at Rotterdam International Film Festival,
*2006:Winner of Public Choice Best Film Award at the British Animation Awards,
*2006:Winner of Best Animated Film at Kino International Film Festival, Manchester,
*2006:Winner of Best Comedy Film at Stuttgart International Animation Film Festival, Germany,
*2006:Winner of Special Mention at Granada International Film Festival, Spain,
*2006:Winner of Best Animated Film at the Wood Green Int Film Festival. UK,
*2006:Winner of Public Choice Award at Ann Arbor International Film Festival. USA,
*2006:Winner of Best Animated Film at Mediawave, Hungary, 
*2006:Winner of Special Mention at the Sonar International Film Festival, Florence, Italy,
*2006:Winner of Animation Award at Darklight Film Festival, Dublin, Ireland,
*2006:Nominated for Best British Short at British Independent Film Awards, London, UK
*2006:Winner of Special Jury Prize at BAF06 - Bradford Animation Festival, UK
*2006:Winner of Millor Animancio Award at Festival Internacional De Filmets de Badalona, Spain
*2014:Winner of Filmmakers Grand Prix at Sapporro International Film Festival, Japan

==Credits==
Who I Am And What I Want, a film by Chris Shepherd and David Shrigley.
* Voice: Kevin Eldon
* Producer: Maria Manton
* Animation: Alan Andrews, Siren Halvorsen and Ellen Kleiven
* Additional Compositing: John Taylor
* Extras Editor: Rod Main
* Sound Design: Barnaby Templer and Jake Roberts
* Music: Martin Young

A Slinky Pictures Production.
An animate! commission funded by Arts Council England and Channel 4.

==References==
 

==External links==
*   official site
*  
*  
*  
*  
*  
*  
*  
*  


 