Beck – Spår i mörker
{{Infobox film
| name           = Beck – Spår i mörker
| image          = Beck 08 - Spår i mörkret.jpg
| image_size     = 
| alt            = 
| caption        = Swedish DVD-cover
| director       = Morten Arnfred
| producer       = Lars Blomgren Thomas Lydholm
| writer         = Rolf Börjlind
| starring       = Peter Haber Mikael Persbrandt Stina Rautelin Cecilia Häll Carlo Schmidt
| music          = Ulf Dageby
| cinematography = Eric Kress
| editing        = 
| studio         = Victoria Film A/S
| distributor    = Egmont Film AB Swedish Film Institute
| released       = Sweden:  
| runtime        = 90 min  (Theater)  86 min  (TV/VHS)  85 min  (DVD) 
| country        = Sweden, Germany, Denmark, Norway
| language       = Swedish
| budget         = 
| gross          = 
}}
Beck – Spår i mörker (Beck - Trails in Darkness) is a 1997 film about the Swedish police detective Martin Beck directed by Morten Arnfred. It is the eighth film about Martin Beck with Peter Haber as Martin Beck and Mikael Persbrandt as Gunvald Larsson.

== Plot ==
Martin Beck is on his way to a vacation with his colleague and girlfriend Lena Klingström when several gruesome murders in the  . The last connecting thing is that before every murder, all lights have been turned off (note that this was faked and in reality impossible).

As the investigation progresses it becomes clear that Erik Lindgren, a man who works in the underground, has some kind of connection to the murderers. Once found, he reveals that the murderers are youngsters that mostly inhabit the tunnels, playing action games (more specifically, "Final Doom") against "other fools around the globe". He suspects that his sister, Annika is part of the gang, and explains how hes tried to "pull her out of the shit". The group has even written a letter to him stating that "You talk to the cops and Kickfoot (i.e. Annika) will die". When Erik guides Beck to the entrance of the hideout of the gang, they are ambushed by the murderers, but the Swedish task force arrives just in time to save Beck, though too late for Erik.

Eventually, Beck finds Annika, who initially denies any knowledge about the murders; however, when Beck describes to Annika how much Erik loved her and that he was decapitated by the gang, she suddenly breaks down and says that he brought it on himself by trying to interfere with her business, although she did not want her peers to kill him. She eventually proceeds to describe the whole truth: the group assumes their Final Doom characters in real life and plays the game by killing people in the underground in order to obtain so-called frags and thereby advance to new levels. This activity is not restricted to Sweden, but shared with people around the world engaging in similar murders. Annika was not part of the actual killings, however, instead being in charge of the technical aspect.

Beck manages to convince Annika to take Beck and the task force to the hideout, which is empty save for a single little boy, who is playing Final Doom. He tells Annika that the others are out on a raid. By reading text files on one of the computers, Annika discovers that they are raiding the station underneath Odenplan (in practice this was filmed at St Eriksplan, which is one station west of Odenplan). Beck and the task force arrive just in time to arrest the murderers, who are armed with machetes and night vision goggles. Stefan, the leader, attempts to kill Annika upon finding out that she betrayed them, but Gunvald shoots him in the leg. When Beck arrives to the street, an American journalist asks him if there is a connection to some similar murders in New York (which is heavily implied as Annika explained that the killings were international) and he tells Klingström that he may not be able to go on the planned vacation, because he may have to go to New York and assist with the investigation.

== Cast ==
*Peter Haber as Martin Beck
*Mikael Persbrandt as Gunvald Larsson
*Stina Rautelin as Lena Klingström
*Cecilia Häll as Annika Lindgren
*Carlo Schmidt as Erik Lindgren
*Per Morberg as Joakim Wersén
*Kasper Lindström as Jens (a kid)
*Ingvar Hirdwall as Martin Becks neighbour
*Rebecka Hemse as Inger (Martin Becks daughter)
*Fredrik Ultvedt as Jens Loftsgård
*Michael Nyqvist as John Banck
*Lasse Lindroth as Peter (Ingers boyfriend)
*Staffan Göthe as Henrik Magnusson
*Margareta Niss as Elisabeth Ernström
*Ulf Eklund as Bengt Nihlfors
*Boman Oscarsson as a journalist Daniel Larsson as Stefan

== Production ==
All the footage from "Final Doom" is taken from modified parts of   by Bungie Software.

== Release and reception==
The premier in Sweden was on 31 October 1997. It was released on VHS in February 1998 and on DVD 13 November 2002.    TV4 in 2002.   

== References ==
 

== External links ==
* 
* 

 

 
 
 
 
 
 
 