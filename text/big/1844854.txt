The Day of the Dolphin
{{Infobox film
| name           = The Day of the Dolphin
| image          = Day of the dolphin ver3.jpg
| caption        = Theatrical release film poster by Tom Jung
| director       = Mike Nichols
| producer       = Robert E. Relyea Joseph E. Levine
| writer         = Buck Henry
| based on       = A Sentient Animal&nbsp;by  
| starring       = George C. Scott Trish Van Devere Paul Sorvino
| music          = Georges Delerue
| cinematography = William A. Fraker
| editing        = Sam OSteen Avco Embassy Pictures
| released       =  
| runtime        = 104 minutes
| country        = United States
| language       = English
| budget         = $8,150,000 Mike Nicholss latest: filming with dolphins
By Nora E. Taylor. The Christian Science Monitor (1908-Current file)   27 Dec 1973: B5.  
}} written by Buck Henry.

==Plot== English in yacht of the President of the United States.

==Cast==
*George C. Scott as Dr Jake Terrell
*Trish Van Devere as Maggie Terrell
*Paul Sorvino as Curtis Mahoney
*Fritz Weaver as Harold DeMilo
*Jon Korkes as David
*Edward Herrmann as Mike
*Leslie Charleson as Maryanne
*John David Carson as Larry
*Victoria Racimo as Lana
*John Dehner as Wallingford
*Severn Darden as Schwinn
*William Roerick as Dunhill
*Elizabeth Wilson as Mrs Rome
*Phyllis Davis as Receptionist
*Dolphin voices Elliot Peterson and Elliot Fink

== Production ==
The novel was published in the US in 1969 and was retitled The Day of the Dolphin. Tanked Bond
By CHRISTOPHER LEHMANN-HAUPT. New York Times (1923-Current file)   16 May 1969: 45.   The Bookshelf: Man Is About to Draft His Friend, the Dolphin
Wall Street Journal (1923 - Current file)   06 June 1969: 16. 

The film version was originally going to be directed by Roman Polanski for United Artists in 1969, with Polanski writing the script. MOVIE CALL SHEET: New Leaf Next for Weston
Martin, Betty. Los Angeles Times (1923-Current File)   25 Apr 1969: i12.   However, while Polanski was in London, England, looking for filming locations in August 1969, his pregnant wife, the actress Sharon Tate, was murdered in their Beverly Hills home by disciples of Charles Manson. Polanski returned to the United States and abandoned the project. THE WORLD OF HOLLYWOOD: Tragedy Strikes Those Who Beat Odds Against Success HOLLYWOOD ODDS
Champlin, Charles. Los Angeles Times (1923-Current File)   10 Aug 1969: B.   Dreams, Nightmares of Roman Polanski: ROMAN POLANSKI
GELMIS, JOSEPH. Los Angeles Times (1923-Current File)   26 Dec 1973: d20.  

The following year it was announced Franklin Schaffner would make the movie for the Mirisch Corporation. Son Of Help!: Son of Help!
By A.H. WEILER. New York Times (1923-Current file)   15 Feb 1970: 93  These plans were frustrated and Joseph Levine ended up buying the project from United Artists for Mike Nichols. Mike Nichols Dolphin: Mike Nichols
By A. H. WEILER. New York Times (1923-Current file)   12 Mar 1972: D13 

The film was mostly shot on Abaco Island in The Bahamas. Mike Nicholss latest: filming with dolphins
By Nora E. Taylor. The Christian Science Monitor (1908-Current file)   27 Dec 1973: B5.   Production was extremely difficult and Nichols later described it as the toughest shoot he had done to date. Movies: Levine in the land of moguls, where exploitation is king
Campell, Mary. Chicago Tribune (1963-Current file)   23 Dec 1973: e10. 

== Reception ==

The film received mixed reviews when released in 1973. Pauline Kael, the film critic for The New Yorker, suggested that if the best subject that Nichols and Henry could think of was talking dolphins, then they should quit making movies altogether. 
 Best Original Best Sound (Richard Portman and Larry Jost).    Levine also claimed the movie had guaranteed pre-sales of $8,450,000 to cover costs, including a sale to NBC, which had expressed interest into turning the story into a TV series. 

Alpha the dolphin was named best animal actor in the 24th Patsy Awards. 

Levine admitted the film was not a success:

 The rushes looked great. But it just didnt jell somehow. I really think Mike   was the wrong guy to direct. And George C. Scott!... He got paid $750,000 for that movie—and ran us over schedule. The first three days of shooting he reported in with a "virus".  

==Differences from the novel and other sources of inspiration==
Merles novel, a satire of the Cold War, is supposedly the basis for this film, but the films plot was substantially different from that of the novel. The movie is instead inspired in part from the scientist John C. Lillys life. A physician, biophysics|biophysicist, neuroscience|neuroscientist, and inventor, Lilly specialized in the study of consciousness. In 1959, he founded the Communications Research Institute at Saint Thomas, U.S. Virgin Islands|St. Thomas in the Virgin Islands and served as its director until 1968. There he worked with dolphins exploring dolphin intelligence and human-dolphin communication.
 Bottlenose dolphin John Lilly and cetacean communication.

==Cultural references==
 
* On June 25, 2007, Stephen Colbert recommended his The Colbert Report viewers rent this film after making an allusion to it that received little reaction from the studio audience.   Black and White the characters played by Robert Downey Jr. and Brooke Shields share a rambling, improvised discussion about the movie and the meanings of the dolphins relationships to the films humans specifically as a touchstone for their own relationship. 
* A reference to the film appears in the episode "Six Feet Under the Sea" on the television series Psych (TV series)|Psych. 
* The third segment of The Simpsons Treehouse of Horror XI, called Night of the Dolphin, is a comical homage to this film.  The worlds dolphins organize into an army to destroy humanity starting with Springfield. 
* The Day of the Dolphin is the seventh level of The Simpsons Game. Bart and Lisa try to stop the dolphin invasion in Springfield caused by Kang and Kodos.
* In the Good Times episode, Black Jesus, Willona Woods mentions that her date took her to see the movie.

==References==
 

==External links==
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 