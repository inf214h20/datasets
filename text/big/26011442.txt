Punjabi House
{{Infobox film
| name           = Punjabi House
| image          = Punjabi House.jpg
| image size     =
| alt            =
| caption        = VCD cover
| director       = Rafi Mecartin
| producer       = New Saga Films 
| writer         = Rafi Mecartin
| narrator       = Dileep Mohini Mohini Cochin Lal Janardhanan Janardanan Neena Kurup
| music          = Suresh Peters
| cinematography = Anandakuttan
| art director   = Valsan
| editing        = Harihara Puthran 
| studio         =
| distributor    = New Saga Films 
| released       =   as a grand Onam release. 
| runtime        = 200 minutes
| country        = India
| language       = Malayalam
| budget         = 
| gross          = 
}}
 comedy musical film written and directed by Rafi Mecartin, starring Dileep (actor)|Dileep, Mohini (Tamil actress)|Mohini, Lal (actor)|Lal, Cochin Hanifa, Harisree Asokan, Neena Kurup, and Janardhanan (actor)|Janardanan. The music was composed by Suresh Peters, making his debut as a film composer. The film was released as a grand Onam release on the day of Thiruvonam. 

The film was remade by Priyadarshan in Bollywood as Chup Chup Ke in 2006, starring Shahid Kapoor, Kareena Kapoor, Neha Dhupia, and Sunil Shetty. 


==Plot==
Unni (Dileep (actor)|Dileep) has many debts and no way to repay them. He decides to commit suicide so that his parents can use the insurance money to repay the debts, but he is saved by a fisherman named Gangadharan (Cochin Haneefa) and his employee Ramanan (Harisree Ashokan). When they notice Unni drowning, Unni pretends to be deaf and mute so that they will not understand the truth.

Gangadharan has a debt of his own, to a Punjabi family of money lenders who have settled in Kerala. He asks Unni and Ramanan to work at their home until he can repay it. There Unni meets a Punjabi girl named Pooja (Mohini (Tamil actress)|Mohini), who is partially deaf and completely mute. They fall in love and decide to get married. Unnis family finds out that he is not dead and they come to see him with his old girlfriend Sujatha (Jomol), who allows Unni to marry Pooja.

==Cast== Dileep ...  Unnikrishnan Mohini ...  Pooja Lal ...  Sikkandar Singh
*Cochin Haneefa ...  Gangadharan
*Harisree Asokan ...  Ramanan
*Jomol ...  Sujatha
*Thilakan ...  Kaimal Master Janardanan ...  Manninder Singh
*Neena Kurup ...  Poojas Cousin
*Indrans ... Uthaman
*N. F. Varghese ...  Sujathas father
*Manka Mahesh ...  Unnikrishnans Mother Kunchan ...  Thommichan
*Machan Varghese ...  panthalukaran
* Reena 
* Praseetha Menon
* Gayathri

==Critical reception==
This film was released on September 4, 1998, as an Onam release,  along with six other major releases. Harikrishnans, which featured Mohanlal and Mammootty in their first appearance together for seven years, and Summer in Bethlehem, with Suresh Gopi, Jayaram, Manju Warrier and Mohanlal, were the most eagerly awaited releases, but Punjabi House became a blockbuster in the first week alone, with a gross of more than 4 crores. It ran for 200 days and became the third-largest hit of 1998, following Harikrishnans and Summer in Bethlehem. It grossed 10 crores in total. 

==Soundtrack==
The film includes the following songs written by Gireesh Puthenchery and composed by Suresh Peters.

* Balla Balla - swarnalatha,mano and chorus
* Ellam Marakkam - M. G. Sreekumar, Sujatha Mohan (Another track was also sung by K. J. Yesudas)
* Eriyunna Karalinte - M. G. Sreekumar
* Sona Re Sona Re - M. G. Sreekumar Mano and chorus

==Dialogues==
For the dialogues in written form visit the link http://ml.wikiquote.org/wiki/പഞ്ചാബി_ഹൌസ്

==Remakes==
*Punjabi House was remade in Hindi as Chup Chup Ke,directed by Priyadarshan.
*Punjabi House was remade in Telugu as Maa Balaji, directed by Kodi Ramakrishna.
*Punjabi House was remade in Kannada as Punjabi House.

==References==
 

==External links==
 

 
 
 
 
 