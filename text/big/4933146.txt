My Super Ex-Girlfriend
{{Infobox film
| name = My Super Ex-Girlfriend
| image = exgirlposter.jpg
| caption = Theatrical release poster
| director = Ivan Reitman
| producer = Arnon Milchan Gavin Polone Don Payne
| starring = Luke Wilson Uma Thurman Anna Faris Eddie Izzard Rainn Wilson Wanda Sykes
| music = Teddy Castellucci Don Burgess
| editing = Wendy Greene Bricmont Sheldon Kahn
| studio = Regency Enterprises
| distributor = 20th Century Fox
| released =  
| runtime = 95 minutes
| country = United States
| language = English
| budget= $30 million
| gross = $60.9 million 
}}
 romantic comedy superhero film, directed by Ivan Reitman and starring Uma Thurman, Luke Wilson, Anna Faris, Eddie Izzard, Rainn Wilson and Wanda Sykes.

==Plot== neurotic and aggressive behavior, becoming more demanding and ultimately injuring Matt and destroying his bed the first time they have sex.  Soon after, Jenny reveals to him that she is in fact the voluptuous blonde superheroine, G-Girl, who accidentally received powers such as flight, superhuman strength, speed, and senses, invulnerability, super breath, and heat vision after she was exposed to radiation from a crashed meteorite as a teenager.  Jenny starts to become more controlling after she reveals her powers and Matt starts to lose his mind.

Hannah (Anna Faris), Matts co-worker, has a crush on him despite the fact that she is going out with a handsome but shallow underwear model. As Matt and Hannahs friendship develops further, and after becoming aggravated with Jennys escalating jealousy, Matt ends the relationship. An enraged Jenny vows to make Matt regret the decision, using her superpowers to publicly embarrass him, throwing his car into space and eventually causing him to lose his job as an architect when she strips him naked during an important meeting. Professor Bedlam (Eddie Izzard), Jennys former friend, and now G-Girls nemesis, contacts Matt in order to enlist his aid in defeating her. Matt refuses and makes plans to leave the city. As he does so he is contacted by Hannah who has broken with her cheating boyfriend, and after confessing their feelings to one another, they end up in bed.

Jenny (as G-Girl) discovers them in bed the next day. Enraged and jealous, she attacks the pair with a great white shark. Angered, Matt contacts Professor Bedlam and agrees to help him defeat her, as long as Bedlam retires from being a supervillain. He instructs Matt to lure Jenny to a meeting where she can be exposed to another meteorite that will draw away her powers, leaving her a mere mortal. Matt agrees and meets Jenny for a candlelit dinner at his apartment, under the pretense of wanting to resume their relationship. Hannah arrives to see Jenny sitting on Matts lap. The two women fight, and in the struggle Jennys superhero identity is revealed to Hannah. Bedlams trap is sprung, and the energy that gave Jenny her powers is drained back into the meteorite, incapacitating Jenny.

Professor Bedlam appears, but reveals that he has no intention of keeping his promise to retire from villainy and in fact plans to take the powers for himself. While he and Matt fight, Jenny crawls to the charged meteorite attempting to regain her powers. Hannah intervenes just as Jenny grabs the meteorite, which explodes in a burst of power. Both Hannah and Jenny are catapulted off the roof, apparently to their deaths; Jenny appears within seconds, powers restored, threatening even more mayhem. Only the unexpected reappearance of Hannah, who was also exposed to the meteorites energies, and now possesses the same powers as G-Girl, saves Matt. The second fight between Hannah and Jenny is a full-on super-brawl, destroying part of the neighboring properties. Finally, Matt reasons with them both and they cease fighting. He tells Jenny that Professor Bedlam is her true love. Jenny agrees and she embraces her former nemesis.

The next morning, Matt and Hannah meet up with Professor Bedlam (now just "Barry") and Jenny. As cries for help are heard from afar, Jenny and Hannah, who have become partners in crime-fighting, take off to tackle the emergency. Matt and Barry are left holding their girlfriends purses and clothes, and leave to have a beer together.

==Cast==
* Luke Wilson as Matthew "Matt" Saunders, the protagonist. He begins dating Jenny after capturing a thief who snatched her purse, but ultimately breaks up with her due to her controlling and demanding behavior. He also finds out that Jenny is in fact G-Girl.
* Uma Thurman as Jenny Johnson / G-Girl, a confused, insecure young woman who also happens to be a superheroine, thanks to her contact with a mysterious meteorite. When she falls in love with Matt, she becomes controlling and demanding with him, and when he finally tries to break up with her, sets out to make his life a living hell through abusing her super powers.
* Anna Faris as Hannah Lewis, Matts co-worker who is secretly in love with him. She winds up gaining superpowers due to contact with the same meteorite that gave Jenny her powers.
* Eddie Izzard as Barry Edward Lambert / Professor Bedlam, G-Girls ex-boyfriend and nemesis and the main antagonist. He reconciles with Jenny and retires from villainy in the end.
* Rainn Wilson as Vaughn Haige, Matts crude and sex-crazed best friend, who constantly gives him dubious advice about picking up women, despite the fact that he himself is hopeless when it comes to dating.
* Wanda Sykes as Carla Dunkirk, Matt and Hannahs nosy and uptight boss who ends up firing the former thanks to a jealous Jenny.
* Stelio Savante as Leo, one of Bedlams hired goons.
* Mike Iorio as Lenny, another of Bedlams hired goons.
* Mark Consuelos as Steve Velard, a handsome but shallow underwear model who is Hannahs boyfriend at the beginning, and is the primary reason why Matt doesnt ask her out early on. She eventually leaves him after catching him in a ménage á trois. Tara L. Thompson as Young Jenny Johnson
* Kevin Townley as Young Barry Lambert
* Tom Henry as the guy in the red cast
* Margaret Anne Florence as the bartender, who works at the bar where Matt and Vaughn like to hang out. A running gag involves Vaughns repeated attempts to date with her, which are unsuccessful until the very end when they both get caught in the middle of an epic super-battle between Jenny and Hannah.

==Production== Don Payne conceived of the idea of his first film while working on The Simpsons television series, saying that as a fan of comics, the idea of a romantic comedy with a superhero twist was "a fitting first feature". The spec script (at that time called Super Ex) attracted the attention of production company Regency Enterprises and director Ivan Reitman, and the film was fast-tracked for production. Filming took place over four weeks in New York City and featured Westchester high school Port Chester High School for the main characters high school scenes.

==Release==

===Box office===
My Super Ex-Girlfriend debuted in the United States and Canada on July 21, 2006 in 2,702 theaters. In its opening weekend, the film grossed $8,603,460 and ranked #7 in the American and Canadian box office. The film proceeded to gross $22,530,295 in the United States and Canada and $38,454,511 in other territories for a worldwide gross of $60,984,606. 

===Critical response===
The film received mixed reviews from critics. Review aggregator site Rotten Tomatoes gives it a score of 40% based on 128 reviews. The sites consensus reads: "My Super Ex-Girlfriend is an only sporadically amusing spoof on the superhero genre that misses the mark with a nerd-turned-superwoman who embodies sexist clichés.   
On Metacritic, another review aggregator, it has an average score of 50%, based on 28 reviews.   

=== Home media ===
 
The film was released on DVD on December 19, 2006 with   presentations along with English Dolby Digital 5.1 Surround tracks. Special features include deleted scenes, behind-the-scenes, and "No Sleep 2 Nite" music video by Molly McQueen.

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 