Violin (film)
{{Infobox film
| name           = Violin
| image          = Violin (film).jpg
| image_size     = 
| alt            = 
| caption        = Promotional poster
| director       = Sibi Malayil
| producer       = AOPL Entertainment
| writer         = Viju Ramachandran
| narrator       = 
| starring       = Asif Ali  Nithya Menon
| music          = Bijibal Anand Raj Anand
| cinematography = Manoj Pillai
| editing        = Bijith Bala
| studio         = AOPL Entertainment
| distributor    = APOL Cinemas Release through Lal Release
| released       =  
| runtime        = 
| country        = India
| language       = Malayalam
| budget         = 
| gross          = 
}} Malayalam musical musical romance film directed by Sibi Malayil. It stars Asif Ali and Nithya Menon in the lead roles, and Vijayaraghavan (actor)|Vijayaraghavan, Nedumudi Venu, Sreejith Ravi, Chembil Asokan, Lakshmi Ramakrishnan and Reena Basheer in other major roles. The film is about two youngsters who are brought together by their fondness to music. A musical romance film by genre, it features music composed by Bijibal and a song composed by Bollywood composer Anand Raj Anand. Rafeeq Ahmed writes the lyrics while Manoj Pillai is the cinematographer. Having filmed the major parts from Fort Kochi, the film released on 1 July 2011. 

==Plot==
The lives of Angel (Nithya Menon) and her two aunts Annie (Lakshmi Ramakrishnan) and Mercy (Reena Basheer) are transformed, when Aby (Asif Ali) walks in, as their tenant. After a bout of initial resistance from Angel, romance strikes, and the two fall in sincere love.

==Cast==
* Asif Ali as Aby
* Nithya Menon as Angel
* Lakshmi Ramakrishnan as Annie
* Reena Basheer as Mercy Vijayaraghavan as Simon
* Nedumudi Venu as doctor
* Chembil Asokan
* Sreejith Ravi as Henry
* Vijay Menon as abys father
* Neena Kurup as rose

==Soundtrack==
The soundtrack of this movie was composed by Bijibal and Anand Raj Anand for which the lyrics were penned by Rafeeq Ahmed and Santhosh Varma. All the songs of this movie were instant hits.
{| class="wikitable"
|-
! Track !! Song Title !! Singer(s) !! Lyricist
|-
| 1 || Chirakuveesi (Female) || Soumya T. R. || Rafeeq Ahmed
|-
| 2 || Himakanam || Ganesh Sundaram, Gayatri || Rafeeq Ahmed
|-
| 3 || Ente Mohangalellam || Vidhu Prathap, Cicily || Santhosh Varma
|-
| 4 || Chirakuveesi (Male) || Bijibal || Rafeeq Ahmed
|-
| 5 || Kaanakombil || Nishad, Elizabeth Raju  || Rafeeq Ahmed
|}

==References==
 

==External links==
*  
*  

==Further reading==
*  
*  

 
 
 
 
 

 