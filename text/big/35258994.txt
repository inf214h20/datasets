Idhaya Veenai
{{Infobox film
| name           = IDHAYA VEENAI           
| image          = Idhaya Veenai Poster.jpg
| image_size     =
| caption        = Film poster
| director       = Krishnan-Panju
| producer       = S. Maniyam Vidwan V.Lakshmanan
| writer         = Swornam
| narrator       = Manjula Lakshmi Lakshmi Sivakumar M. N. Nambiar
| music          = Shankar-Ganesh
| cinematography = A.Shanmugham
| editing        = M. Umanath
| studio         = Uthayam Productions
| distributor    = Uthayam Productions
| released       = 20 October 1972 
| runtime        = 146 mins
| country        = India Tamil
| budget         =
| gross          =
| website        =
}}
 Indian Tamil Tamil  directed by R. Krishnan and S. Panju, starring M. G. Ramachandran in the lead role Lakshmi, Manjula, Sivakumar, M. N. Nambiar, M. G. Chakrapani and among others. 

==Plot==
Somewhere in Chennai, several years previously...Young, Soundharam (MGR) was pushed away from the home by Sivaraman (M. G. Chakrapani), his father, a severe lawyer.

Sivaraman denies his son. Soundharam makes the promise to his father, that one day, he will beg him to recognize him.

He lives at the moment in the Kashmir as tourist guide.

When he finds Nalini (Lakshmi), his younger sister, in the middle of a group of students, Soundharam decides to go back home, to make ample knowledge with her and help it.

But at the beginning, he incurs only troubles, in particular, with Kirymani (Sivakumar), the lover of Nalini and Annamalai (M. N. Nambiar), a man with double face.

==Cast==
{| class="wikitable" width="72%"
|- bgcolor="#CCCCCC"
! Actor !! Role
|-
|-
| M. G. Ramachandran || as Sundharam
|- Lakshmi || as Nalini
|-
| Manjula Vijayakumar|Manjula|| as Vimala
|-
| Sivakumar || as Kirymani
|-
| M. N. Nambiar || as Annamalai
|-
| M. G. Chakrapani || as Sivaraman
|-
| R. S. Manohar || as Karmaigham
|-
| Thengai Srinivasan || as K.Muthu
|-
| Poornam Viswanathan || as Kumarswamy, Vimala s father
|-
| A. Sakunthala || as 
|-
| G. Sakunthala || as Mangalam, Sivaramans wife
|-
| Sachu || as
|-
| Master Sekhar || as Soundharam (child)
|-
| Master Prabhakar || as K.Muthu (child)
|-
|}

The casting is established according to the original order of the credits of opening of the movie, except those not mentioned

==Around the movie==
MGRs brother M. G. Chakrapani acts as MGRs father in the movie. Sivakumar is second hero who marries MGRs sister Lakshmi and MGRs pair is Manjula.  Some scenes were picturised in Kashmir including a song Kashmir Beautiful.

The story of this movie looks like another MGR movieEN ANNAN, 1970, managed by the even excellent duet of directors, Krishnan-Panju.

It was the second soundtrack composed by the other tandem Shankar-Ganesh for a movie of MGR.

The first one was NAAN YEN PIRANDHEN? on the same year.

This movie released just 4 days after MGR created his political party, Anna D.M.K. (ADMK)

==Soundtrack==
The music composed by Shankar-Ganesh.  

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|- Vaali (poet)|Vaali || 03:28
|-
| 2 || Kashmir Beautiful || T. M. Soundararajan || 07:28
|-
| 3 || Neeraadum || P. Susheela || 03:29
|-
| 4 || Oru Vaalum || T. M. Soundararajan || 03:38
|-
| 5 || Pon Andhi || T. M. Soundararajan, P. Susheela || Pulamaipithan || 05:49
|- Vaali || 04:14
|}

==References==
 

==External links==
 

 
 
 
 
 


 