The First $20 Million Is Always the Hardest
 
 
 
{{Infobox film
  | image = The_First_20_Million_Is_Always_the_Hardest.jpg
  | caption = Theatrical release poster Mick Jackson
  | producer = Trevor Albert
  | writer = Po Bronson (book) Jon Favreau Gary Tieche
  | starring = Adam Garcia Rosario Dawson Jake Busey Enrico Colantoni Ethan Suplee Anjul Nigam Gregory Jbara
  | music = Marco Beltrami
  | cinematography = Ronald V. Garcia
  | editing = Don Brochu
  | distributor = 20th Century Fox
  | released = June 28, 2002 (United States|U.S.)
  | runtime = 105 min. English
  | budget = $17,000,000 
  | gross  = $5,491 
}}

The First $20 Million Is Always the Hardest is a 2002 film based on a novel by technology-culture writer Po Bronson. The film stars Adam Garcia.

==Plot== massively obese, anthropophobic man; and Darrell (Jake Busey), a tall, blond, pierced, scary, germophobic, deep-voiced man with personal space issues who regularly refers to himself in the third person.
 floppy drive, and anything that holds information. The computer has been reduced to a microprocessor, a monitor, a computer mouse|mouse, a computer keyboard|keyboard, and the internet, but it is still too expensive. Having seen the rest of his team watching a hologram of an attractive lady the day before, in a dream Andy is inspired to eliminate the monitor in favor of the cheaper holographic projector. The last few hundred dollars comes off when Darrell suggests using virtual reality gloves in place of a mouse and keyboard. Tiny then writes a "hypnotizer" code to link the gloves, the projector, and the internet, and theyre done.

But immediately before he finishes, the whole team (except for Tiny, who is still writing the code) quits LaHonda after being told that there are no more funds for their project, but sign a non-exclusive patent waiver, meaning that LaHonda will share the patent rights to any technology they had developed up to that point. After leaving LaHonda, they pitch their product to numerous companies, but do not get accepted, mainly because:

*the prototype emagi (electronic magic) as it was now called, was ugly, and
*something always seemed to go wrong during the demonstration of their product.

They have almost given up hope, when in comes the lovely next-door neighbor Alisa again, whose relationship with Andy has been growing steadily. She improves its look, and when called back by an executive from one of the companies they had pitched to, to whom they had said that their design teams were working on a cosmetic model that would be ready in a couple of days when she commented, "You havent given much thought to the look of it."  After meeting with her, they agree to give her 51% of their company in exchange for getting their product manufactured and for getting Andys Porsche bought back, which he had had to sell in order to raise money to build a new emagi after leaving LaHonda. Unfortunately, she then sells the patent rights to the emagi to Francis Benoit, who plans to sell the emagi at $999 a piece and reap a huge profit. The team interrupts the meeting in which Benoit is going to introduce the emagi to the world and introduces an even newer computer he and his team developed and manufactured at LaHonda, which was in a state of disaster when they arrived. It was a small silver tube that projected a hologram and lasers which would detect where the hands were, eliminating the need even for virtual reality gloves. Also, Andy reminds Benoit of the non-exclusive patent waiver, which had even been Benoits idea in the first place.

==Production== New York Los Angeles.

Po Bronson played a cameo role in the film as one of many tuba players living in the same building as the main character. The tentative title for this movie during test screenings was "The Big Idea".

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 