Jodorowsky's Dune
 
{{Infobox film
| name           = Jodorowskys Dune
| image          = Jodorowskys Dune poster.jpg
| alt            = A cartoon desert, littered with pages of storyboards. 
| caption        = Theatrical release poster
| director       = Frank Pavich
| producer       = {{Plainlist |
* Frank Pavich
* Stephen Scarlata
* Travis Stevens
}}
| starring       = {{Plainlist |
* Alejandro Jodorowsky
* Michel Seydoux
* H. R. Giger
* Chris Foss
* Nicolas Winding Refn
* Amanda Lear Richard Stanley
}}
| music          = 
| cinematography = David Cavallo
| editing        = {{Plainlist|
* Paul Docherty
* Alex Ricciardi}}
| studio         = {{Plainlist|
* City Film
* Snowfort Pictures}}
| distributor    = Sony Pictures Classics
| released       =  
| runtime        = 90 minutes  
| country        = {{Plainlist|
* United States
* France}}
| language       = {{Plainlist |
* English
* French
* German
* Spanish
}}
| budget         = 
| gross          = $647,280 
}}
Jodorowskys Dune is a 2013 American-French documentary film directed by Frank Pavich. The film explores Chilean-French director Alejandro Jodorowskys unsuccessful attempt to adapt and film Frank Herberts 1965 science fiction novel Dune (novel)|Dune in the mid-1970s.

==Background== prog rock Gong and Magma for some of the music; artists H. R. Giger, Chris Foss, and Jean Giraud for set and character design; Dan OBannon for special effects; and Salvador Dalí, Orson Welles, Gloria Swanson, David Carradine, Mick Jagger, Amanda Lear, and others for the cast.   

Herbert traveled to Europe in 1976 to find that $2 million of the $9.5 million budget had already been spent in pre-production, and that Jodorowskys script would result in a 14-hour film ("It was the size of a phonebook", Herbert later recalled). Jodorowsky took creative liberties with the source material, but Herbert said that he and Jodorowsky had an amicable relationship.  The project ultimately stalled for financial reasons. The film rights lapsed in 1982, when they were purchased by Italian filmmaker Dino De Laurentiis, who eventually released the 1984 film Dune (film)|Dune, directed by David Lynch.

==Content==
 
The film notes that Jodorowskys script, extensive storyboards, and concept art were sent to all major film studios, and argues that these were inspirational to later film productions, including the Alien (franchise)|Alien, Star Wars, and Terminator (franchise)|Terminator series.    In particular, the Jodorowsky-assembled team of OBannon, Foss,  Giger and Giraud went on to collaborate on the 1979 film Alien (film)|Alien.   

"It was a great undertaking to do the script," Jodowrosky says in the film. "Its very, its like Proust, I compare it to great literature."

==Production==
The project was officially announced in May 2011.  Director Pavich filmed an extensive series of interviews with the principal players involved in the failed 1970s adaptation, shooting in France, Switzerland, the United Kingdom, and the United States.

==Release== premiered at the Directors Fortnight at the 2013 Cannes Film Festival in May 2013.    Sony Pictures Classics acquired the North American distribution rights to the film in July 2013,    and later announced a theatrical release date of March 7, 2014.    The film was released on DVD and on-demand on July 8, 2014.

==Reception==
The film has received critical acclaim.   declared the "entertaining documentary makes the case for this overblown epic as a legendary lost masterpiece".  Entertainment Weekly named Jodorowskys Dune as one of its 10 Best Movies of 2014. 

Review aggregation website   gives the film a 79/100 rating based on 31 critics, indicating "generally favorable reviews". 

==References==
 

==External links==
*  
*   (Sony Pictures Classics)
*  
*  
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 