Men of the Fighting Lady
{{Infobox film
| name = Men of the Fighting Lady
| image = Men of the Fighting Lady.jpeg
| caption = Theatrical poster
| director = Andrew Marton
| producer = Henry Berman
| writer = Harry A. Burns Dewey Martin Frank Lovejoy
| music = Miklós Rózsa
| cinematography = George J. Folsey
| editing = Gene Ruggiero
| distributor = Metro-Goldwyn-Mayer
| released =  
| runtime = 79 minutes
| country = United States
| language = English
| budget = $829,000 "The Eddie Mannix Ledger". Margaret Herrick Library, Center for Motion Picture Study, Los Angeles. 
| gross = $2,638,000 
}}
Men of the Fighting Lady (also known as Panther Squadron) is a 1954 Korean War drama film starring Van Johnson, Walter Pidgeon, Keenan Wynn, and directed by Andrew Marton. The screenplay was written by U.S. Navy Commander Harry A. Burns, who had written a Saturday Evening Post article, "The Case of the Blinded Pilot", an account of a U.S. Navy pilot in the Korean War, who saves a blinded Navy pilot by talking him down to a successful landing. Men of the Fighting Lady was also inspired by another Saturday Evening Post article, "The Forgotten Heroes of Korea" by James A. Michener. The original music score was composed by Miklós Rózsa.

==Plot== USS Oriskany aircraft carrier in the Sea of Japan during the Korean War, author James A. Michener (Louis Calhern) meets Commander and flight surgeon Kent Dowling (Walter Pidgeon). Dowling relates a "Christmas story" of a near-miracle.
 Dewey Martin) VF 192 squadron pilots flying Grumman F9F Panther fighter-bombers who are forced to go back to destroy an enemy railroad that is rebuilt after each attack. Their leader, Lieutenant Commander Paul Grayson (Frank Lovejoy), is even shot down during one mission and rescued from the sea. Veteran pilot Lieutenant Commander Ted Dodson (Keenan Wynn) criticizes Grayson for flying too low and risking his life. Ironically, it is Dodson who loses his life in another mission when his damaged aircraft explodes on landing.

For their 27th mission against the enemy target, the squadron flies out on Christmas Day, and Schecter is hit by enemy fire and blinded. Lieutenant Thayer (Van Johnson) guides Schecter by radio to a safe landing on the deck of the carrier. The squadron celebrates his safe return, but also mourns the loss of good men like Dodson.

==Cast==
* Van Johnson as Lieutenant (jg) Howard Thayer
* Walter Pidgeon as Commander Kent Dowling
* Louis Calhern as James A. Michener Dewey Martin as Ensign Kenneth Schechter
* Keenan Wynn as Lieutenant Commander Ted Dodson
* Frank Lovejoy as Lieutenant Commander Paul Grayson Robert Horton as Ensign Neil Conovan
* Bert Freed as Lieutenant (jg) Andrew Szymanski Lewis Martin as Commander Michael Coughlin George Cooper as Cyril Roberts
* Dick Simmons as Lieutenant Wayne Kimbrell
* Ann Baker as Mary, Schechters fiancee

==Production==
 
  USS Midway. The air pocket dropped most the plane below the landing deck level, but he managed to keep the nose up above the deck at the time of impact, severing both wings and aft fuselage, and expelling the planes cockpit and nose onto the carrier deck as a fireball erupted behind him. Except for burning his ears, Duncan survived the crash 
 USS Valley Forge, came to the aid of fellow squadron pilot Ensign Kenneth Schechter, who had been blinded by anti-aircraft fire. Thayer guided his friend to a safe landing at K-18, a U.S. Marine airfield. 

==Reception==
Due to its release shortly after the end of the Korean War, the Men of the Fighting Lady was well received by the general public as well as by critics as a mainly factual account of aerial combat. Bosley Crowther of The New York Times gave the film a very favorable review: "Men of the Fighting Lady, which came yesterday to the Globe, bearing a title that echoes a dandy factual film of World War II, turns out to be an apt successor to that saga of the aircraft carriers, translating now a stirring story of carrier planes and men in the Korean war." 

According to MGM records, Men of the Fighting Lady made $1,502,000 in the U.S. and Canada and $1,136,000 in other countries. resulting in a profit of $729,000. 

==References==

===Notes===
 

===Bibliography===
 
* Evans, Alun. Brasseys Guide to War Films. Dulles, Virginia: Potomac Books, 2000. ISBN 1-57488-263-5.

 

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 