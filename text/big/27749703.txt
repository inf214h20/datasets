Charlie Chan's Murder Cruise
{{Infobox film
| name           = Charlie Chans Murder Cruise
| image_size     =
| image	         = Charlie Chans Murder Cruise FilmPoster.jpeg
| caption        =
| director       = Eugene Forde John Stone
| writer         = Earl Derr Biggers (novel) Robert Ellis (adaptation) Helen Logan (adaptation) Robertson White Lester Ziffren
| narrator       =
| starring       = Sidney Toler Victor Sen Yung
| music          =
| cinematography =
| editing        =
| studio         = Twentieth Century-Fox
| distributor    = Twentieth Century-Fox
| released       =  
| runtime        =
| country        = United States
| language       = English
| budget         =
| gross          =
}}
Charlie Chans Murder Cruise is a 1940 murder mystery film starring Sidney Toler in his fifth of many performances as Charlie Chan. It is based on the Earl Derr Biggers novel Charlie Chan Carries On.

==Plot==
The famed detective seeks to unmask a killer on a voyage across the Pacific Ocean.

==Cast==
*Sidney Toler as Charlie Chan
*Victor Sen Yung as Jimmy Chan (as Sen Yung) Robert Lowery as Dick Kenyon
*Marjorie Weaver as Paula Drake
*Lionel Atwill as Dr. Suderman
*Don Beddoe as Fredrick Ross
*Leo G. Carroll as Prof. Gordon (as Leo Carroll)
*Cora Witherspoon as Susie Watson
*Leonard Mudie as Gerald Pendleton
*Harlan Briggs as Coroner Charles Middleton as Jeremiah Walters
*Claire Du Brey as Sarah Walters
*Kay Linaker as Linda Pendleton James Burke as Wilkie
*Richard Keene as Buttons
*Layne Tom Jr. as Willie Chan
*C. Montague Shaw as Inspector Duff

==External links==
 
* 
* 
* 
* 

 

 
 
 
 
 
 
 
 
 

 