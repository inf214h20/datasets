Ku! Kin-dza-dza
{{Infobox film
| name = Ku! Kin-dza-dza
| image = Ku! Kin-dza-dza! poster.jpg
| image_size =
| caption =
| director = Georgy Danelia, Tatyana Ilyina
| producer = Konstantin Ernst, Leonid Yarmolnik
| writer = Georgy Danelia, Andrey Usachov
| starring = Nikolai Gubenko, Andrei Leonov, Alexei Kolgan, Alexander Adabashian
| music = Giya Kancheli
| cinematography =
| distributor = CTB, Channel One Russia
| released =  
| runtime = 97 min
| country = Russia
| language = Russian
| budget = 140 mln. rubles 
}}
 animated science fiction film by Georgy Danelia. It is a remake of Danelias 1986 live-action film Kin-dza-dza!. Although it preserves much of the original movies social commentary, Ku! Kin-dza-dza is notably less dark and dystopian than original, and more targeted towards youth and an international audience.

==Plot==
The remake follows the plot of the original with minor changes. While the original story was set in 1980s, the remake is set in 2010s, some of the scenes were altered, and the two new protagonists are different from their 1986 counterparts.

A renowned cellist Vladimir Chizhov (Uncle Vova) and his teenage nephew Tolik meet an alien with a teleportation device. Tolik carelessly pushes a button on the device, and he and Uncle Vova are beamed to the planet Plyuk in Kin-dza-dza galaxy. The planet is a post-apocalyptic desert without resources, ruled by a brutal racist regime. The two travellers meet three locals, Bi, Wef and their robot Abradox, who travel on a pepelats and constantly try to cheat and betray the naive newcomers. Tolik and Uncle Vova have to go a long distance through the rusting world of Kin-dza-dza to find their way home.

==Cast==
* Nikolai Gubenko as Uncle Vova
* Ivan Tsekhmistrenko as Tolik Tsarapkin
* Andrei Leonov as Wef (son of Yevgeny Leonov, who performed the role in the original movie)
* Alexei Kolgan as Bi
* Alexander Adabashian as Abradox
* Georgy Danelia as Camomile and Diogenes
* Igor Kvasha as Yk, a caroussel owner

==Critical reception== Trud and Izvestiya, and mediocre ratings from Lenta.ru, Vedomosti and Afisha. Outside Russia, Vassilis Kroustallis at Zippyframes gave the film a positive review. 

==Awards==
* Asia Pacific Screen Awards 
* Nika Award for best animated feature. 

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 