Deep Water (film)
 
 
{{Infobox Film
| name           = Deep Water 
| image          = Deep water poster.jpg
| image_size     = 
| caption        = Theatrical release poster
| director       = Louise Osmond Jerry Rothwell
| producer       = Alison Morrow Jonny Persey John Smithson Executive producer Jonathan Banatvala François Ivernel Ralph Lee Cameron McCracken Paul Trijbits Co-producer Stewart Le Marechal
| writer         = 
| narrator       = Jean Badin Tilda Swinton
| starring       = Donald Crowhurst Clare Crowhurst
| music          = Harry Escott Molly Nyman
| sound          = Joakim Sundström
| cinematography = Nina Kellgren
| editing        = Ben Lester
| distributor    = IFC Films
| released       =  
| runtime        = 92 minutes
| country        = United Kingdom
| language       = English
| gross          = 
}}
Deep Water is a documentary film, directed by Jerry Rothwell and Louise Osmond, produced by Jonny Persey, opening in the UK on 15 December 2006. It is based on the true story of Donald Crowhurst and the 1969 Sunday Times Golden Globe Race round the world alone in a yacht.

The film has received critical acclaim. The official poster quotes The Daily Telegraph, "A movie which will reduce the hardest of hearts to a shipwreck". The film won the Best Documentary award at the 2006 Rome International Film Festival and a commendation in the Australian Film Critics Association 2007 Film Awards.

==External links== PBS 
*  

 
 
 
 
 
 


 