The Rats Are Coming! The Werewolves Are Here!
{{Infobox film
| name           = The Rats Are Coming! The Werewolves Are Here!
| image_size     =
| image	=	The Rats Are Coming! The Werewolves Are Here! FilmPoster.jpeg
| caption        =
| director       = Andy Milligan
| producer       = William Mishkin (producer)
| writer         = Andy Milligan (writer)
| narrator       =
| starring       = See below
| music          =
| cinematography = Andy Milligan
| editing        = Andy Milligan
| distributor    =
| released       =
| runtime        = 91 minutes
| country        = USA
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}

The Rats Are Coming! The Werewolves Are Here! is a 1972 American film written and directed by Andy Milligan.

== Plot==
The film centers on the eccentric Mooney family whom live in a large house in rural England in the early 1900s. The invalid patriarch Pa Mooney (Douglas Phair) is a retired medical doctor who claims to be 199 years old. His eldest daughter, Phoebe (Joan Ogden), more or less cares for him and is head of running the household. His eldest son, Mortimer (Noel Collins), is a businessman whom conducts the finances of the family and contributes to the family income. Younger daughter Monica (Hope Stansbury) is a sadist who keeps live rats as pets and frequently mutilates them and other small animals. Youngest son Malcolm (Berwick Kaler) is a halfwit with animalistic tendencies in which the family keeps him locked up in a room of the house with live chickens. The family has a secret: they are all werewolves! They are natural born, not made, werewolves whom turn once a month on the night of the full moon and Pa Mooney has been researching for years to find a way to break the family curse.

Youngest daughter Diana (Jackie Skarvellis) returns home from medical school with a new husband, a former classmate named Gerald (Ian Innes), which Pa Mooney heartily disapproves of. Pa tells Diana that she is the last hope that the family has to overcome the ancient curse since she is the only member of the family who does not turn into a werewolf on the night of the full moon. Will Diana succeed? However, Diana is eventually revealed to have other plans, and on top of that, she has her own secret to why she is "different" from the rest of the werewolf Mooney family.

== Cast ==
*Hope Stansbury as Monica Mooney
*Jackie Skarvellis as Diana
*Noel Collins as Mortimer Mooney
*Joan Ogden as Phoebe Mooney
*Douglas Phair as Pa Mooney
*Ian Innes as Gerald
*Berwick Kaler as Malcolm Mooney
*Chris Shore
*George Clark 
*Lillian Frit

== Release ==
The film was released theatrically in the United States by William Mishkin Motion Pictures in 1972. It was filmed entirely in England in 1969 back-to-back with other Andy Milligan directed films which included The Body Beneath, Bloodthirsty Butchers, and The Man with Two Heads. Some scenes were filmed nearly two years later in Staten Island at the request of producer William Mishkin to pad out the short running time.

The film was released on VHS by Midnight Video in the 1980s.

== External links ==
* 
* 

 
 
 
 
 
 
 

 