Love and Mary
 
{{Infobox film name        =Love and Mary image       =Loveandmary.jpg caption     =Theatrical Poster  director    =Elizabeth Harrison producer    =Peter James Cooper Jonathan Downs writer      =Elizabeth Harrison starring    =Lauren German Gabriel Mann Allie DeBerry Whitney Able music       =Tony Tisdale editing     =George Folsey, Jr.
|cinematography=Brad Rushing studio      =Industrial Pictures Ranch House Productions distributor =Cut Entertainment Group  released    =  runtime     =104 minutes country     =United States language    =English budget      = $500,000 (estimated) gross       = 
 }}

 Love and Mary is a 2007 romantic comedy film directed by Elizabeth Harrison and starring Lauren German and Gabriel Mann. 

==Cast==
*Lauren German as Mary
*Gabriel Mann as Jake / Brent 
*Whitney Able as Lucy
*Allie DeBerry as Sara Pedersen

==Plot==
Mary Wilson, a born and bred Texan, moves to Los Angeles to open a high class bakery, and get away from her wacky family.  After initial success she faces eviction when a bad review and a run of poor sales coincide.  Hoping to raise money from engagement presents she decides to visit her family in Texas with her fiancé, Brent.  Brent comes down with an allergy before the trip and she takes his twin brother Jake, a jailbird, to pretend to be Brent.  While in Texas she falls in love with Jake, and he with her, despite some competition from Lucy, her childhood friend, who is in on the secret and assumes Jake is "available".  Brent arrives during the engagement party and take Mary back to Los Angeles, where Marys bakery becomes a success after her grandfather gives her the award-winning recipes he and his wife collected over the years.  While setting a wedding date, Brent realizes they are not suited, Mary confesses her love to Jake, and Mary and Jake get married.

==Release==
Love and Mary was released on March 11, 2007, at South by Southwest while the DVD followed on August 11, 2008. The film was also premiered at Hollywood Film Festival on October 19, 2007. 

== Reception ==
Love and Mary received mostly negative to mixed reviews with a 26% fresh rating on Rotten Tomatoes. 

==References==
 

==External links==
*  


 
 
 
 
 
 
 
 
 