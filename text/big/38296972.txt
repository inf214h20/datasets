Sumbaran
{{Infobox film
| name           = Sumbaran
| image          = Sumbaran Marathi Movie.jpg
| alt            =  
| caption        = Theatrical release poster
| director       = Gajendra Ahire
| producer       = Anil Phatdare
| screenplay     = 
| story          = 
| starring       = Makarand Anaspure Vrinda Ahire Jeetendra Joshi Mukta Barve Siddharth Jadhav Sai Tamhankar Ravindra Mankani Ravi Kale
| music          = Vishwanath More
| cinematography = 
| editing        = 
| studio         = Everest Entertainment
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Marathi
| budget         = 
| gross          = 
}}
Sumbaran is a Marathi movie released on 31 December 2009.  Produced by Anil Phatdare and directed by Gajendra Ahire. The movie is based on two brothers walking down memory lane and going back in time when they were young.

== Synopsis ==
Veeru (Jitendra Joshi) is a maturing musician who is on his way back to the village. His elder brother Vasant and sister-in-law are also returning to their old ancestral house after ages. The journey makes all of them revisit their earlier period, with all its restful memories and tiresome truths.

One of the film’s highlights is its toughened representation of the past. A bygone era of naive childhood and sore growing up years unfolds on the canvas of a rural backdrop that’s threatened by the mortifying forces of modernization. Their wada, which holds the most defining part of their lives within its breakup walls, is set to be sold off. Will they let go of their times gone by and get carried away by the currents of time? Or will they withhold what’s dear from their pasts and move on with an entrenched pledge towards tomorrow.

== Cast ==

*Makarand Anaspure as Vasant
*Jeetendra Joshi as Veeru
*Mukta Barve as Kalyani
The cast includes Seema Deshmukh, Makarand Anaspure, Vrinda Ahire, Jeetendra Joshi, Mukta Barve, Siddharth Jadhav, Sai Tamhankar, Ravindra Mankani & Ravi Kale.

==Soundtrack==
The music is provided by Rahul Ranade.

== References ==
 
 

== External links ==
*  
*  
*  

 
 
 


 