The Greening of Planet Earth
 
The Greening of Planet Earth is a half-hour-long video produced by the coal industry, which argues that rising CO2 levels will be beneficial to agriculture, and that policies intending to reduce CO 2  levels are therefore misguided.  The video argues that rising CO 2  levels both directly stimulate plant growth and, as a result of their warming properties, cause winter temperatures to rise, thereby indirectly stimulating plant growth.  It was produced in 1991 and released the following year. A sequel, entitled, "The Greening of Planet Earth Continues", was released in 1998.  The video was narrated by Sherwood Idso.  After the video was made, it was distributed to thousands of journalists by a coal industry group.  The video became very popular viewing in the George H. W. Bush White House and elsewhere in Washington, where it was promoted before the 1992 Earth Summit,  and, according to some reports, became especially popular with then-chief of staff John H. Sununu. 

==Funding==
Funding for the video was provided by the Western Fuels Association, which paid $250,000 to produce it.    It was produced by the Greening Earth Society, which was created by the Western Fuels Association and with which the Association shared a business address. 

==References==
 

 
 
 
 
 

 