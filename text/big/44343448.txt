Sweet and Twenty
{{Infobox film
| name           = Sweet and Twenty
| image          =
| caption        =
| director       = Sidney Morgan 
| producer       = Frank E. Spring 
| writer         = Basil Hood (play)   Sidney Morgan
| starring       = Marguerite Blanche   Langhorn Burton   George Keene   Arthur Lennard
| music          = 
| cinematography = 
| editing        = 
| studio         = Progress Films
| distributor    = Butchers Film Service
| released       = November 1919  
| runtime        = 
| country        = United Kingdom 
| awards         =
| language       = Silent   English intertitles
| budget         =
| preceded_by    =
| followed_by    =
}} silent romance film directed by Sidney Morgan and starring Marguerite Blanche, Langhorn Burton and George Keene.

==Cast==
* Marguerite Blanche as Jean Trevellyn  
* Langhorn Burton as Douglas Floyd 
* George Keene as Eustace Floyd  
* Arthur Lennard as Reverend James Floyd   George Bellamy as Prynne 
* Nell Emerald as Ellen

==References==
 

==Bibliography==
* Low, Rachael. The History of the British Film 1918-1929. George Allen & Unwin, 1971.

==External links==
*  

 

 
 
 
 
 
 
 
 

 