The Casual
The Casual is a Maisha Film Lab short film  about infidelity and forgiveness in a poverty-stricken society, written and directed by Kenyan newspaper humour writer and BBC Award-winning radio playwright Mark Mutahi  in August 2008.
 
==Plot== secret affair Roger Masaba). Kim later finds out but forgives the cheaters after being promoted to foreman and his pay increased.

==Cast and crew==
The cast included John Mary Mukiza, Maureen Nankya, Roger Masaba, Samuel Lutaaya, Keloy Kemigisha plus three extras Eva Tumwesigye (as Janet), Charles Kajubi and Kalya Sam as construction workers. Stella Nambaya was the handheld baby while Bony was the stunt driver.

The crew included two participants from Mira Nairs 4th Maisha Filmmakers Training Lab, that is, Edward Aikobua (Uganda) as Production Manager and Angela Wamai (Kenya)  as First Assistant Director. The Cameraman was Tanzanian Richard Boniface Magumba, Sound recordist George Kaigwa (Kenya), Runner Timothy Tabaaro (Uganda) and Editor Anthony Muteru Paul (Kenya). The Music Score was used with permission from Just A Band (Kenya). 
 Don McKeller plus Semi Chellas (Toronto, ON, Canada), Steve Cohen, Maria Maggenti plus Craig Weiseman (New York, NY, USA), Annie Ryan (Dublin, Ireland), Barry Braverman (Los Angeles, CA, USA), Giles Khan (Bombay, India) and Myles Kane (USA).

The Casual was developed within six days in six different locations supposedly within one mile in a part of Kampala, Uganda near Royal Impala Hotel (Munyonyo). Footage from the seventh location was never used.

Other short films made during the 4th Screenwriters Lab include On Time by Patricia Olwoch (Uganda) and Sins of the Parents by Judith Lucy Adong (Uganda).

==References==
 

 
 
 
 