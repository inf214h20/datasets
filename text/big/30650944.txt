Poster Girl (film)
{{Infobox film
| name           = Poster Girl
| image          =
| caption        =
| director       = Sara Nesson
| producer       = Mitchell Block and Sara Nesson
| writer         = 
| starring       = Robynn Murray
| music          = Miriam Cutler
| cinematography = Sara Nesson
| editing        = Geof Bartz
| distributor    = 
| released       =  
| runtime        = 38 minutes 
| country        = United States
| language       = English
| budget         = 
}}
Poster Girl is a 2010 documentary film about an American soldiers experience with posttraumatic stress disorder after returning from the Iraq War.  The film showed at the 37th Telluride Film Festival on September 3, 2010.  It was named as a nominee for the Academy Award for Best Documentary (Short Subject) at the 83rd Academy Awards on January 25, 2011  but lost to Strangers No More.   

The documentary short film is a production of Portrayal Films and was conceived and produced by Mitchell Block and directed and photographed by Sara Nesson.   

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
  
 
 
 