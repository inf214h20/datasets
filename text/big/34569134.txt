Zarafa (film)
{{Infobox film
| name           = Zarafa
| image          = Zarafa (2012 film) poster.jpg
| image size     = 
| border         = 
| alt            = 
| caption        = French poster
| director       = Rémi Bezançon   Jean-Christophe Lie
| producer       = Christophe Jankovic Valérie Schermann
| writer         = Rémi Bezançon Alexander Abela
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = Max Renaudin, Simon Abkarian, Ronit Elkabetz
| music          = Laurent Perez
| cinematography = 
| editing        = Sophie Reine
| studio         = Pathé, Prima Linea Productions
| distributor    = 
| released       =  
| runtime        = 78 min.
| country        = France
| language       = French
| budget         = $8,6 million
| gross          = $11,301,860 
}}

Zarafa is a French animated film directed by Rémi Bezançon and Jean-Christophe Lie. It was released on 8 February 2012 in France. 

==Plot== telling a story to a group of eager children. slavery by Muslim slave traders with his friend Soula. He escapes the villainous slave trader Moreno (voiced by Thierry Fremont) and comes across a young giraffe and its mother. Moreno catches up to Maki and kills the mother giraffe. Maki promises the calf’s mother that he’ll protect and nurture her. Just as Moreno is about to take him to his slave camp, Hassan, a Bedouin nomad (Simon Abkarian), intervenes and saves his life. Maki follows Hassan as soon as he takes the giraffe with him. Hassan names the giraffe Zarafa (Arabic for “giraffe”) and reluctantly agrees to take care of Maki and Zarafa. They come across a merchant, Mahmoud, who gives them two Tibetan cows, Mounh and Sounh. Maki discovers Soula being forced into slave labor by Moreno. When the evil man turns his attention on Maki, Soula hits him with a palm leaf, but before Moreno can beat her with his whip, Maki cries out for her and Hassan steps in. Maki thanks Hassan for saving him.
Hassan is on a mission to the Pasha of Egypt, Mehemet Ali, who wants to offer a young giraffe to the King of France, Charles X, to convince him to unite his country against the Turks besieging Alexandria. Maki and Hassan join together with the aeronaut Malaterre, who agrees to take Zarafa to Paris via a hot air balloon. Hassan convinces Maki to leave Zarafa, but Malaterre thinks otherwise, seeing Maki’s determination, and takes the boy with them. The basket gets heavy, so Hassan and Malaterre toss Maki and the cows over the side. Maki and the two cows land on a pirate ship, where they come across the pirate queen Bouboulina and her ragtag crew. Maki explains that he is in pursuit of a treasure of great value aboard the balloon. Instead of taking him prisoner, Bouboulina welcomes Maki to her crew. Meanwhile, Moreno is determined to hunt down Maki and arrives on shore with Soula in tow. Bouboulina and her crew rescue Maki and scare off Moreno and his henchmen. The group continues on their journey. During a perilous crossing of the mountains where the balloon crashed, one of the cows is taken by a pack of wolves. the city zoo, and Maki stays firm about returning the giraffe to her home. Moreno kidnaps him and forces him to work in his household. Hassan is ashamed that he had failed his mission and mortified to have lost Maki, so he sinks into despair and alcohol. As several years pass, Zarafa’s appearance causes “giraffe mania” and she grows up. 
Maki is reunited with Soula and they escape from Moreno’s clutches. They manage to find Malaterre and Hassan, but the nomad is unable to help them, since he becomes an alcoholic and they cannot take him with them. Maki rushes into the zoo with Soula. He discovers a hippopotamus, and, remembering an experience he had with one before meeting Hassan, tells Soula to hold up her parasol. Maki does the same and the hippo squirts a colossal pile of dung onto King Charles and his subjects, giving our heroes enough time to make a getaway. They rush to free Zarafa, but she is too large to fit into the balloon. Maki realizes that he must give up Zarafa and go with Soula. Moreno appears and prepares to kill Maki, but Hassan steps in to protect them and is shot. Aided by Malaterre, Maki and Soula escape in the balloon. Moreno gives chase, but the two friends push him off the basket and he falls into an enclosure where he is devoured by a polar bear. Maki and Soula return home, marry, and found a flourishing new village. Hassan, treated at the hospital, survives his wounds and falls in love with Bouboulina. As it turns out, the storyteller is actually Maki himself.

==Background and controversy==
The film was based on the historical event, of the Giraffe given to Charles X of France by Muhammad Ali of Egypt, and Rémi Bezançon wanted to make a film of it as soon as he heard about it, and was also keen to explore the issue of slavery in a film.  

The film was accused of distorting the historical facts about how the giraffe was treated, and the Museum dhistoire naturelle created a temporary exhibition entitled « The True story of Zarafa » to present its own version of history.  But mostly it received positive reviews from critics.

==Accolades==
{| class="wikitable"
|-
! Award !! Category !! Winner/Nominee !! Result
|- Annie Awards Directing in an Animated Feature Production Remi Bezancon, Jean-Christophe Lie
| 
|-
|}

==References==
 

==External links==
*   
* 

 
 
 
 
 
 