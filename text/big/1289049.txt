Millions
 
 
 
{{Infobox film name = Millions image =Millions DVD cover.jpg caption = Theatrical release poster director = Danny Boyle producer = Graham Broadbent Andrew Hauptman Damian Jones writer = Frank Cottrell Boyce starring = Alex Etel Lewis McGibbon James Nesbitt Daisy Donovan Christopher Fulford music = John Murphy cinematography = Anthony Dod Mantle editing = Chris Gill studio = Moving Picture Company BBC Films UK Film Council Pathé Pictures Ingenious Film Partners distributor =Pathé Distribution (UK) Fox Searchlight Pictures (USA) released =   runtime = 98 minutes language = English country = United Kingdom budget = gross = $11,782,282 
}} novel while Carnegie Medal. This is Danny Boyles only film not R rated by the MPAA.

==Plot==
Millions tells the story of 8-year-old Damian, a Catholic school boy, whose family moves to the suburbs of Widnes after the death of his mother. Soon after the move, Damians "hermitage" in a cardboard box by the train tracks is disturbed by a bag of money flung from a passing train. Damian immediately shows the money to his brother, Anthony, and the two begin thinking of what to do with it. Anthony wants the money all to himself. Damian, kind-hearted and religious, had recently overheard three Missionary (LDS Church)|Latter-day Saint missionaries lecture other members of the community on building foundations of rock rather than foundations of sand, an old Christian principle which dictates that self-worth should be based on the teachings of Christ rather than any other object of worship such as Money/Mammon or Power. The lecture inspires Damian, who looks for ways to give his share of the money to the poor; at one point he even stuffs a bundle of cash through the missionaries letter box, having heard about their modest lifestyle and deciding that they too must be poor.

Throughout the story, Damian commits small acts of kindness like buying birds from pet stores and setting them free and taking beggars to Pizza Hut, while Anthony bribes other kids at school into being his transportation and bodyguards, and looks into investing the money in real estate.
 The Bank pound (£) to the euro (€)- an event publicised as € Day. An assembly is held at Damians school to inform the children about the change, as well as to educate the children about helping the poor. Realizing that the money, which is in pounds, will be no good after a few days, Damian decides that the best thing to do would be to give it away before the conversion. He drops £1,000 into the donation can at the assembly. The woman collecting the money, Dorothy, is forced to report Damian; when questioned by the principal, Anthony lies that he and Damian stole the money from the Mormons. Damian and Anthony are grounded that night. When their father collects them from school he chats with Dorothy, and there is an obvious attraction between them.

After the donation, Anthonys friend informs them that a train carrying notes which were to be destroyed after the conversion had been robbed. One bag was stolen in a diversion, while the robber remained on the train disguised as one of the emergency staff, and the money had been dispersed by throwing it off of the train at various locations throughout the country to be collected by the robbers. The boys logically conclude that their money was stolen, and Damian, who thought the money was from God, feels terrible.

Around this time, a mysterious man comes snooping around the train tracks and asks Damian if he has any money. Damian thinks that the man is a beggar and tells him he has loads of money. However, Anthony realises he is one of the robbers, and gives the man a jar full of coins to cover Damians tracks.

The robber eventually finds out where Damian lives and ransacks his house. Damian had informed his father about the money just before they came home to their destroyed house. The robbery is then explained. The robbers boarded the train. They then escaped the police by dressing as football fans and joining a crowd of similarly dressed fans leaving a game. However, one man remained on the train. He began to throw the money off, to be collected later. The robber who came sneaking around hid in Damians room after ransacking it, much in the way the train robbery was carried out. Damians father, who had resolved to give the money back, decided that if the robbers were going to steal his familys Christmas, then he would steal the robbers money. The family, as well as Dorothy, go on a massive shopping spree on Christmas Eve.

That night, after they are asleep, their house is bombarded by beggars and charities begging for contributions, and seeing the confusion that results, Damian runs off to the train tracks to burn the money, deciding that it was doing more harm than good. While he is burning the money, he is visited by his dead mother, who tells him not to worry about her.

In the final scene, the audience sees Damians dream of the family flying a rocket ship to Africa and helping develop water wells, while Damian narrates over the scene that each family member but him had hidden a little bit of the money beforehand. Damian convinced them to spend this money on the wells he is dreaming about. Earlier in the movie this was shown to be the most crucial and cheapest way to drastically improve the quality of life for many African communities.

==Cast==
*Alex Etel as Damian Cunningham
* Lewis McGibbon as Anthony Cunningham
*James Nesbitt as Ronald Cunningham
*Daisy Donovan as Dorothy
*Christopher Fulford as The "Poor Man"
*Pearce Quigley as Community Policeman
*Jane Hogarth as Mum
*Alun Armstrong as Saint Peter
*Enzo Cilenti as Saint Francis of Assisi
* Nasser Memarzia as Saint Joseph
*Kathryn Pogson as Saint Clare of Assisi
*Harry Kirkham as Saint Nicholas Gonzaga
*Kolade Ambrosio
*Leslie Phillips as Himself

==Production==
In a 2014 interview, Boyle stated that, had he and Cottrell Boyce been more confident, they would have made the film as a musical, with the characters singing and dancing. Boyle was interested in having Noel Gallagher write original songs for the film. 

==Release==
===Box office=== War of the Worlds. Around £3,987,642.22 of the final box office was received in the UK alone. 

===Critical reception===
The film received very positive reviews, earning an 88% "Certified Fresh" approval rating on the review aggregate website Rotten Tomatoes. 

 |title=Millions|accessdate=18 April 2007
|date=18 March 2005}} 
 Ebert & Roeper, called it "One of the most stylish and eccentric films about childhood dreams and heartbreaks that Ive ever seen."

Leonard Maltin praised the film upon its DVD release, saying "Millions is a winning and unpredictable fable from England that will charm viewers both young and old." {{cite news|first=Leonard|last=Maltin|url=http://www.leonardmaltin.com/nucleus1.55/index.php?blogid=1&archive=2005-11|work=Leonard Maltins Video View]|title=Millions|accessdate=18 April 2007
|date=3 November 2005}} 

====Christian film critics====
Christian publications weighed in on the film, many adding stock to its religious message.

Catholic News Service s Harry Forbes wrote, "Boyles offbeat tale—with a clever script by Frank Cottrell Boyce—features good performances all around, especially by the remarkable Etel, who displays just the right innocence and religious fervor in delightful vignettes with the saints. The script dramatizes the themes of money and its complexities and the need for societal philanthropy without being heavy-handed, making this ideal entertainment for older adolescents and up."   

Sister Rose Pacatte, F.S.P. (AmericanCatholic.org) commented, "Millions engages, inspires and is just quirky enough to be charming."  She added, "Damiens familiarity with the saints and his recitation of their biographies is accurate and very funny." {{cite news|first=Rose|last=Pacatte|url=http://americancatholic.org/Messenger/Apr2005/Eye_On_Entertainment.asp#F2|title=Millions|work= |accessdate=26 April 2007
|year=2005}} 

However, although praising the film overall for its positive depiction of the role the Christian faith can play in a young boys life, there were details some felt marred its religious underpinning. As Harry Forbes wrote, "The film contains a couple of mildly crude expressions, some intense episodes of menace, a momentary sexual situation, religious stereotyping, and a brief scene where the brothers look, with boyish curiosity, at a web site for womens bras on a computer."  As such, he explained, "the USCCB Office for Film & Broadcasting classification is A-II – adults and adolescents." 

Johnathan Wooten of Christian Spotlight on Entertainment downplayed the significance, saying, "Those concerned about objectionable content will not find much to offend here though. There is very little violence (a short robbery scene, a very brief moment of a child in peril). Sexual content includes a glimpse of an unmarried couple in bed together as well as pre-pubescent boy viewing an Internet lingerie ad. When played out the latter scene actually has a strange wholesomeness to it considering his other viewing options. The only profanity is some mild British slang." 

===Accolades===
The film premiered at the 2004 Toronto International Film Festival on 14 September 2004.

2005 British Independent Film Awards 
*Won, Best Screenplay: Frank Cottrell Boyce
*Nominated, Most Promising Newcomer: Alex Etel

2006 Broadcast Film Critics Association Awards {{cite news|url=http://imdb.com/Sections/Awards/Broadcast_Film_Critics_Association_Awards/2006
|title=Broadcast Film Critics Association Awards – 2006|accessdate=19 October 2007}} 
*Nominated, Best Young Actor: Alex Etel

2005 Emden International Film Festival {{cite news|url=http://imdb.com/Sections/Awards/Emden_International_Film_Festival/
|title=Emden International Film Festival – 2005|accessdate=19 October 2007}} 
*Nominated, Emden Film Award: Danny Boyle

2005 Golden Trailer Awards 
*Nominated, Best Animation/Family
*Nominated, Best Foreign Independent Film

2005 Humanitas Prize 
*Nominated, Best Film

2005 Phoenix Film Critics Association 
*Won, Best Live Action Family Film
 Saturn Awards {{cite news|url=http://imdb.com/Sections/Awards/Academy_of_Science_Fiction_Fantasy_And_Horror_Films_USA/2006
|title=Academy of Science Fiction, Fantasy & Horror Films, USA – 2006|accessdate=19 October 2007}} 
*Nominated, Best Performance by a Younger Actor: Alex Etel

==Soundtrack== Muse and, Muse song is played, "Blackout (Muse song)|Blackout". It also includes "Hitsville UK" by The Clash, from their Sandinista album.
* The song playing in the scene after they descend from the sky and provide water in Africa is "Nirvana (Elbosco song)|Nirvana", by Elbosco on the Angelis album.
* The song "La Petite Fille de la mer" by Vangelis also appears in the film.
* Members of the Northwest Boychoir, directed by Joseph Crnko, sang on the soundtrack. 

==References==
 

==External links==
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 