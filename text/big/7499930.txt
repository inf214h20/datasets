The Secret of Santa Vittoria
{{Infobox film
| name           =  The Secret of Santa Vittoria
| image          = Original movie poster for the film The Secret of Santa Vittoria.jpg
| caption        = original film poster by Bob Peak
| director       = Stanley Kramer 
| producer       = Stanley Kramer Associate producer George Glass  William Rose and Ben Maddow
| based on       =  
| starring       = Anthony Quinn Virna Lisi Hardy Kruger Sergio Franchi  and Anna Magnani Ernest Gold
| cinematography = Giuseppe Rotunno, A.S.C.
| editing        = William A. Lyon, A.C.E. and Earle Herdan
| studio        = Metro-Goldwyn-Mayer
| distributor    = United Artists 
| released       =  
| runtime        = 139 minutes 
| country        = United States 
| language       = English
| budget         = $6.3 million Tino Balio, United Artists: The Company That Changed the Film Industry, Uni of Wisconsin Press, 1987 p 146 
| gross = $2.7 million (US/ Canada rentals) 
}}
 William Rose. Robert Crichton. Ernest Gold and the cinematography by Giuseppe Rotunno.

The film stars Anthony Quinn, Anna Magnani, Virna Lisi, Hardy Kruger, and Sergio Franchi. It also features  Renato Rascel, Giancarlo Giannini, and Eduardo Ciannelli; with Valentina Cortese making an uncredited appearance. It was almost entirely shot on location in Anticoli Corrado, Italy (near Rome).
 The Joey Bishop Show.  Army Archerd, Regis Philbin and Buddy Hackett interviewed Stanley Kramer, Anthony Quinn, Virna Lisi, and Sergio Franchi from Los Angeles.  The premiere was held to benefit the Reiss-Davis Child Study Center, with Gregory Peck as chairman. The event ended with a celebration at the Century Plaza Hotel. 

This was selected as the opening-night film for the 13th Annual San Francisco International Film Festival. The festival ran from October 23, 1969 until November 2, 1969. 

==Plot==
This story is set during World War II in the summer of 1943, in the aftermath of the fall of the Fascist government of Benito Mussolini. The German army uses the ensuing political vacuum to occupy most of the peninsula of Italy.

Italo Bombolini (Anthony Quinn), the mayor of the wine-making hill town of Santa Vittoria, learns that the German occupation forces want to take all of Santa Vittorias wine with them. The townspeople frantically hide a million bottles in a cave before the arrival of a German army detachment under the command of Sepp Von Prum (Hardy Kruger). 

The Germans are given a few thousand bottles, but Von Prum knows there is a lot more. The two very different men engage in a battle of wits. Finally, with time running out, a frustrated Von Prum threatens to shoot Bombolini unless the hidden wine is given up, but no one speaks. Not being a fanatic, Von Prum leaves without harming the mayor.

==Cast==
  
*Anthony Quinn as Italo Bombolini
*Virna Lisi as Caterina Malatesta
*Hardy Kruger as Sepp von Prum
*Sergio Franchi as Tufa
*And Anna Magnani as Rosa
 
*Co-starring Renato Rascel as Babbaluche
*Giancarlo Giannini as Fabio
*Patrizia Valturri as Angela
*Eduardo Ciannelli as Luigi
*Leopoldo Trieste as Vittorini
*Gigi Ballista as Padre Polenta
 

==Reception==
The film earned $6.5 million world wide, which was considered a disappointment considering the popularity of the novel. 

==Awards and nominations== Film Editing Best Music Score (Ernest Gold). It was nominated for an Eddie award by the American Cinema Editors, USA for best edited feature film.
 Best Motion Picture Comedy; and was nominated  by the Golden Globe Awards committee for Best Director (Stanley Kramer), Best Actor Comedy (Anthony Quinn), Best Actress Comedy (Anna Magnani), Best Original Score (Ernest Gold) and Best Original Song ("Stay", Ernest Gold and Norman Gimbel)

==The Secret of Santa Vittoria on Turner Classic Movies==
The Secret of Santa Vittoria was shown on April 30, 2015 on Turner Classic Movies as part of its "Star of the Month salute" to Anthony Quinn.

===Introductory comments=== Robert Crichton, no relation to Michael Crichton|Michael, by the way, The Secret of Santa Vittoria takes place in a northern Italian village during the Second World War. Anthony Quinn plays a colorful and affable sort, also a tipsy one, often looked down on, as the town drunk. But, through an odd set of circumstances — movie circumstances — hes appointed mayor. 

Soon after, the townspeople learn theyre about to be occupied by the Nazis and they go into a panic. But it isnt their lives they fear for — its their wine. So the new mayor hatches a plan to keep the vino out of German hands and off German lips. This is a spirited romp, as it sounds, even one of the Nazis is charming. Its set against the backdrop of the Italian countryside, with beautiful Technicolor cinematography by Giuseppe Rotunno. The man calling the shots on the film is Stanley Kramer, working for the first time with Anthony Quinn — they would collaborate again one year later on the 1970 drama R. P. M. Joining Quinn in the cast is Anna Magnani, Virna Lisa sic, Giancarlo Giannini and Hardy Kruger as that aforementioned charming Nazi scene-stealer. Pour yourself a glass of chianti and relax. From nineteen sixty-nine, heres Anthony Quinn in The Secret of Santa Vittoria." 

==References==
 

==External links==
 
* 
* 
* 
* 
*  at Rotten Tomatoes
*  at TV Guide (revised form of this 1987 write-up was originally published in The Motion Picture Guide)

 
 

 
 
 
 
 
 
 
 
 
 
 