Something Always Happens (1928 film)
{{Infobox film
| name           = Something Always Happens
| image          = 
| alt            = 
| caption        = 
| director       = Frank Tuttle
| producer       = Jesse L. Lasky Adolph Zukor
| screenplay     = Raymond Cannon Herman J. Mankiewicz Florence Ryerson Frank Tuttle  Neil Hamilton Sôjin Kamiyama Charles Sellon Roscoe Karns Lawrence Grant Mischa Auer
| music          =
| cinematography = J. Roy Hunt
| editing        = Verna Willis 
| studio         = Famous Players-Lasky Corporation
| distributor    = Paramount Pictures
| released       =  
| runtime        = 50 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =
}}
 comedy silent Neil Hamilton, Sôjin Kamiyama, Charles Sellon, Roscoe Karns, Lawrence Grant and Mischa Auer. The film was released on March 24, 1928, by Paramount Pictures. {{cite web|url=http://www.nytimes.com/movie/review?res=9403E7DB173CE73ABC4951DFB3668383639EDE|title=Movie Review -
  Don t Marry - THE SCREEN; The Old-Fashioned Impostor. - NYTimes.com|work=nytimes.com|accessdate=11 February 2015}}  

== Cast ==
*Esther Ralston as Diana Mallory Neil Hamilton as Roderick Keswick
*Sôjin Kamiyama as Chang-Tzo 
*Charles Sellon as Perkins
*Roscoe Karns as George
*Lawrence Grant as The Earl of Rochester
*Mischa Auer as Clark
*Noble Johnson as The Thing
*Vera Lewis	as Gräfin Agathe 

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 
 