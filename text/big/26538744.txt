Mr. Cinema
 
 
{{Infobox film name = Mr. Cinema image = caption = director = Samson Chiu producer = Henry Fong Ping writer = starring = Anthony Wong Teresa Mo Ronald Cheng Karen Mok music = Leon Ko cinematography = editing = studio = distributor = released =   runtime = 110 minutess country = Hong Kong language = Cantonese budget =
}} Hong Kong Anthony Wong, Teresa Mo, Ronald Cheng and Karen Mok.

==Plot== Anthony Wong) British colony HK transferred over to the Peoples Republic of China.  In the end Zhou realised he sacrificed everything for the communist cause, and his family is left with nothing.

==Cast== Anthony Wong as Zo Heung Kong
* Teresa Mo as Chan Sau-ying
* Ronald Cheng as Zo Chong
* Karen Mok as Luk Min
* Paw Hee-Ching as Lee Choi-ha
* John Shum as Luk Yau

==Production note==
The film has been criticised for its "selective history" for covering a long period of HKs history, but does not mention the   was only covered briefly, and Chinas support for its HK-based loyalists is never addressed.   The name of Anthony Wongs character Zhou Heung Kong is pronounced similar to "Left(ist) Hong Kong".

==Critical reception==
The film received mixed reviews. One of them, by Vivienne Chow of Muse (Hong Kong magazine)|Muse magazine, applauded Chiu for initiating the idea of telling a Hong Kong story from the perspective of the leftists for the first time, but deemed the movie ultimately overambitious. 

==See also==
* The True Story of Ah Q

==References==
 

==External links==
*  

 
 


 