Theruvu Narthaki
{{Infobox film
| name           = Theruvu Narthaki
| image          =
| image_size     =
| caption        =
| director       = N. Sankaran Nair
| producer       = S Kumar
| writer         =
| screenplay     = N. Sankaran Nair Geetha Anuradha Anuradha
| music          = Vijayabhaskar
| cinematography = Prabhakar
| editing        = G. Murali
| studio         = Sastha Productions
| distributor    = Sastha Productions
| released       =  
| country        = India Malayalam
}}
 1988 Cinema Indian Malayalam Malayalam film, Geetha and Anuradha in lead roles. The film had musical score by Vijayabhaskar.   

==Cast==
*Balan K Nair
*Bheeman Raghu Geetha
*Anuradha Anuradha
*Vincent Vincent

==Soundtrack==
The music was composed by Vijayabhaskar.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Anthiyil Vidarana || KS Chithra || Mankombu Gopalakrishnan ||
|-
| 2 || Muracherukkan Vannu || KS Chithra || Mankombu Gopalakrishnan ||
|}

==References==
 

==External links==
*  

 
 
 


 