Pareeksha
{{Infobox film 
| name           = Pareeksha
| image          =
| caption        =
| director       = P. Bhaskaran
| producer       = Vasu Menon
| writer         = TN Gopinathan Nair
| screenplay     = TN Gopinathan Nair Sharada Adoor Bhasi Thikkurissi Sukumaran Nair
| music          = MS Baburaj
| cinematography = EN Balakrishnan
| editing        = K Narayanan K Sankunni
| studio         = Rani Films
| distributor    = Rani Films
| released       =  
| country        = India Malayalam
}}
 1967 Cinema Indian Malayalam Malayalam film, directed by P. Bhaskaranand produced by Vasu Menon. The film stars Prem Nazir, Sharada (actress)|Sharada, Adoor Bhasi and Thikkurissi Sukumaran Nair in lead roles. The film had musical score by MS Baburaj.   

==Cast==
 
*Prem Nazir as Vijayan Sharada as Yamuna
*Adoor Bhasi as Ayyappan Pilla
*Thikkurissi Sukumaran Nair as Janardhanan Pilla
*Kottayam Santha
*P. J. Antony
*T. R. Omana
*BK Pottekkad
*Latheef
*Aranmula Ponnamma
*CA Balan Khadeeja
*Kuttan Pillai
*Panjabi
*Santo Krishnan
*Ramesh 
*P. Bhaskaran
 

==Soundtrack==
The music was composed by MS Baburaj and lyrics was written by P. Bhaskaran. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Annuninte Nunakkuzhi || K. J. Yesudas || P. Bhaskaran || 
|-
| 2 || Avidunnen Gaanam Kelkkaan || S Janaki || P. Bhaskaran || 
|-
| 3 || Chelil Thaamara   || S Janaki || P. Bhaskaran || 
|-
| 4 || En Praananaayakane || S Janaki || P. Bhaskaran || 
|-
| 5 || Oru Pushpam || K. J. Yesudas || P. Bhaskaran || 
|-
| 6 || Praanasakhi Njan || K. J. Yesudas || P. Bhaskaran || 
|}

==References==
 

==External links==
*  

 
 
 

 