Camp Blood 2

 
{{Infobox film
| name           = Camp Blood 2
| image          =
| writer         = Brad Sykes Jane Johnson Courtney Burr Tim Sullivan Brannon Gould Natasha Corrigan Leana Masiello Bret Ellington
| director       = Brad Sykes
| editing        = Jeff Leroy
| producer       = David S. Sterling
| distributor    = Sterling Entertainment
| released       = 2000
| runtime        = 75 min.
| country        = United States
| language       = English
| budget         = Ghost
| cinematography = Jeff Leory
| awards         =
|}}

Camp Blood 2 is a 2000 horror film, and sequel to Camp Blood. The film was directed by Brad Sykes and produced by David S. Sterling. It was followed in 2005 by Within the Woods.

==Plot== first movie film maker Worthy Milligan decides to shoot a film based upon the murders and hires Tricia, the traumatized sole survivor of the massacre, as a technical advisor. However, once Tricia, Milligan and the rest of the cast and crew trek into the woods, the nightmare becomes all too real as the clown reappears and begins to butcher the hapless crew members.

After most of the cast and crew have been killed over, Tricia is captured by the killer who reveals themself to be Adrienne, who is the sister of Harris, the perpetrator of the original killings, who blames Tricia for the death of Harris. As the two wrestle, Tricia uses a lighter to set the gasoline soaked Adrienne on fire. During their final struggle, Tricia retrieves Adriennes machete and slashes her neck open. Adrienne gives Tricia her mask before she dies of her wounds and Tricia flees carrying the mask and machete.

==Production==
Camp Blood 2 was released just one year after its original and was even more successful than its first. This time however, the movie within a movie approach was taken to not separate it from the first movie, but to also poke fun at the original as well.

==External links==
* 

 
 
 
 
 

 