Pilgrim Hill (film)
{{Infobox film
| name = Pilgrim Hill
| image = Pilgrim Hill 2013 movie poster.jpg
| image_size = 200px
| caption = the movie poster Gerard Barrett
| producer = Gerard Barrett
| writer = Gerard Barrett
| narrator =
| starring = Joe Mullins Muiris Crowley
| music =
| cinematography = Ian D. Murphy
| editing = Gerard Barrett
| distributor =  Element Pictures
| released = 12 April 2013 (Ireland) UK (September 2013) USA (October 2013) China (November 2013)
| runtime = 85 minutes (theatrical cut) Ireland
| English
| budget = Euro|€4,500
|          Euro|€15,000 (Post-Production)
| gross = Euro|€100,000 (Ireland)
| preceded_by =
| followed_by = studio = Irish Film Board Nine Films}}
 Gerard Barrett won the Rising Star Award at the 10th Irish Film & Television Awards. 

The film depicts the life of an Irish cattle farmer, living alone with his invalided father in a remote Irish location. The Irish Times (in a four-star review) wrote: "Barretts debut feature is a quietly stunning slice of rural naturalism. A masterful debut." 
==References==
 

== External links ==
*  
* 
* 
* 
* 
* 

 
 