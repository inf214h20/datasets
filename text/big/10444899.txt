TV Junkie
TV Sundance in 2006.  The film was the subject of a lawsuit by his ex-wife Tammie. 

Variety gave the film a middling review, finding the central figure unpleasant and unsympathetic and the production values inevitably low.    Film Threat found it "an unbelievably candid glimpse into the contradictions of cocaine addiction", and praised Kirkhams articulate and authentic self-portrait.  OutNow.CH found it had a certain fascination beyond what would be expected. 

==Rick Kirkham==
Rick Kirkham was a reporter for Inside Edition who appeared on a segment called "Inside Adventure". From the age of 14, he filmed more than 3,000 hours of a video diary; this included footage during his tenure on Inside Edition during which he was addicted to crack cocaine.  

He appeared on The Oprah Winfrey Show in 2007 to talk about his addiction, and clips of his video diary were shown.   On Oprah, Kirkham said he was a functional addict, and that he would smoke crack at night instead of sleeping, remaining high for about six hours, and report for work at Inside Edition two hours later.  Kirkham went days without sleep during this time.  He said he once interviewed President George H. W. Bush while high on crack; the subject was drug abuse.

==References==
 

==External links==
*  
 

 
 
 
 
 
 
 