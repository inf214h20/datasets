The Woman's Angle
{{Infobox film
| name           = The Womans Angle
| image          = 
| caption        = 
| director       = Leslie Arliss 
| producer       = 
| writer         = 
| starring       = 
| editing        = Edward B. Jarvis
| cinematography = 
| music          =  ABPC
| distribution   = Associated British-Pathé
| released       = 1952
| runtime        = 
| country        = United Kingdom English
| gross = £91,096 (UK) 
}} British drama film directed by Leslie Arliss and starring Edward Underdown, Cathy ODonnell and Lois Maxwell.  It is based on the novel Three Cups of Coffee by Ruth Feiner.

==Cast==
* Edward Underdown - Robert Mansell
* Cathy ODonnell - Nina Van Rhyne
* Lois Maxwell - Enid Mansell
* Claude Farell - Delysia Veronova Peter Reynolds - Brian Mansell
* Marjorie Fielding - Mrs. Mansell Anthony Nicholls - Doctor Nigel Jarvis
* Isabel Dean - Isobel Mansell John Bentley - Renfro Mansell
* Olaf Pooley - Fudolf Mansell
* Ernest Thesiger -  Judge
* Eric Pohlmann - Steffano
* Joan Collins - Marina
* Dana Wynter - Elaine
* Geoffrey Toone - Count Cambia
* Anton Diffring - Peasant
* Miles Malleson - A. Secrett
* Peter Illing - Sergei

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 


 