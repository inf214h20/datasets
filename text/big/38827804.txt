Janala
{{Infobox film
| name           = Janala
| image          = Janala Bengali movie poster.jpg
| alt            = 
| caption        = 
| director       = Buddhadeb Dasgupta
| producer       = 
| writer         = 
| starring       =
| music          = Biswadeb Dasgupta
| cinematography = 
| editing        = 
| studio         = Reliance Pictures
| distributor    = 
| released       =    
| runtime        = 
| country        = India
| language       = Bengali
| budget         = 
| gross          = 
}} Bengali film directed by Buddhadeb Dasgupta and produced under Reliance Pictures banner. The music of the film was composed by Biswadeb Dasgupta.    The film received best films title in the 54th Asia Pacific Film Festival in 2009.  The film was screened in the Marché du Film section of the 2009 Cannes Film Festival. 

== Plot ==
The film attempts to touch the nostalgia of childhood. The story of the film revolves around an urban couple Bimal and  Meera and a broken window of Bimals schools classroom. Bimal plans to visit his school where he spent many years of his childhood. When he visits the school, he finds the building in a very ill condition. He specially notices one broken window of a classroom corner where he used to sit daily and look outside through the window. Though Bimal has little money, he wants to donate something to school authority to repair that window. Since Bimal does not have sufficient money, he uses the money from his fiancée Meeras savings without informing her. When Bimal presents the window to the school authority, they reject it. Conditions get worse when Meera learns about it and she breaks relation with Bimal.    

== Cast ==
* Indraneil Sengupta as Bimal
* Swastika Mukherjee as Meera
* Tapas Paul
* Shankar Chakraborty

== Release and festivals ==
The film premiered in the Masters of World Cinema section at Toronto International Film Festival and has been invited in various other film festivals like– Telluride Film Festival, USA, London Film Festival, Mumbai Film Festival, Hong Kong Asian Film Festival, International Film Festival of India, Mahindra Indo – American Arts Council Film Festival, New York, Dubai International Film Festival etc.  The film released in India on 25 February 2011. 

== Awards ==
The film received best films title in the 54th Asia Pacific festival in 2009.      

== See also ==
* Waarish

== References ==
 

== External links ==
*  

 
 