Dolly Dearest
{{Infobox film
| name           = Dolly Dearest
| image          = T65314u7z3w.jpg
| writer         = Maria Lease Rod Nave  Peter Sutcliffe (story) Maria Lease (screenplay)
| starring       = Denise Crosby Sam Bottoms Rip Torn Chris Demetral Candace Hutson Lupe Ontiveros
| director       = Maria Lease
| studio         = Patriot Pictures Channeler Enterprises
| music          = Mark Snow
| editing        = Geoffrey Rowland
| cinematography = Eric D. Anderson
| producer       = Daniel Cady 
| distributor    = Trimark Pictures Image Organization
| released       =  
| runtime        = 94 minutes
| country        = United States English
}}

Dolly Dearest is an American 1991 horror film starring Denise Crosby and Rip Torn. The movie was initially supposed to be direct-to-video, but did get a limited theatrical release in the Midwest.   Variety 

==Plot==
  Mayan Cult. An archaeologist fascinated by the discovery ends up dead by attempting to break inside. He not only got himself killed from fallen debris, but released an evil spirit that has been trapped within for hundreds of years. The evil spirit then finds refuge inside many dolls within the factory. The death of the archaeologist does not cease the plan of the opening doll factory. Upon the Wades arrival from Los Angeles, they meet their solicitor  , Estrella, and escorts them to their new beautiful home. 

After getting acquainted with their new housekeeper, Camilla (Lupe Ontiveros), and home, Elliot and Estrella return to the vehicle. Curious about her fathers factory, seven-year-old daughter Jessica asks to go with. Elliot does not object and even encourages Jimmy to come with them. Marilyn is the only family member that stays behind on the very first trip. Due to the lack of activities and maintenance, the secluded building (in and out) was covered with spiderwebs, dust, and looked very fragile. While exploring the factory in disappointment, Estrella briefly discusses the factorys history. During the conversation, Jessica leaves the group and goes to a different room. On a high shelf lies many dolls in excellent condition, thanks to the sheet that was covering them up. Outside, Jimmy finds what looks like an entrance to a cave protected by a wired-fence from future intrusion, unaware an archaeologist recently died there. As Jimmy questioned about that area, his father warned him to never go back there again. But being a highly curious character he is, does not listen. 

On the first night of their new home, Marilyn tucks Jessica in bed. They made brief conversations. This was the last night Jessica was herself before being manipulated by her new "playmate" doll, which she was allowed to bring home with her. As she fell asleep, Dolly becomes to life and looks at her. The next day, everything seems normal until Marilyn finds a disturbing drawing that Jessica drew. Although curious, Marilyn does not confront her. As the day progresses, Marilyn begins to experience unusual activities and changes within her daughter. First, she mistook the sound of footsteps as Jessica when it was Dolly (although the movie indicates to the audience it is Dolly, but the Mother is unaware of it until later on). Second, Camilla has sent a Priest to bless the house. During the session, Jessica throws a tantrum while in the car with Marilyn and Jimmy. The outburst scares both her family. Upon reaching their home, the Priest is seen walking away as the Mother successfully calms her down. An awkward behavior, Jessica demands to have her Dolly (for comfort possibly) immediately. As the family walks inside the house, the daughter looks back and gives Camilla an evil look.
 watchman Luis, dead on the floor and flees. It turns out Luis died from a heart attack but in truth, the dolls had come to life and toyed with him for a while. Struggling to escape, his hand got caught in an active sewing machine. He was able to break free, however, but the sight of the wound and in state of shock was apparently too much for him that led to his fate. As he was dying, the dolls made haughty giggles nearby. 

While Jimmy explains his interest of the Mayan cult to his parents, few Sanzia facts such as "make an evil kid" and "devil child" suggests Jessicas doll had an uncanny connection with the archaeological dig. The Mother, still confused and distraught, now believes Jessica really is possessed and attempts to separate the two. The first try didnt go well as Jessica warned her to not touch the doll. And when the Mother did not listen and sternly tells shes going to take the doll despite the daughters demand, Jessica suddenly screams in a fit of rage and shouts, "I WILL KILL YOU!" And to make sure the Mother understands to never intervene again, her childs voice has turned into centuries old hoarse of the evil spirit proclaiming, "the kids mine!" The next day, Marilyn visits the dig for the first time and talks about the tribe with an archaeologist, Resnick (Rip Torn). After asking what is it that theyre looking for, Marilyn is told they are searching for the remains of the Sanzia devil child inside the tomb, which hasnt been opened yet. Resnick continues to explain the nature of the devil child being truly evil and that it was fed by a warm blood of slaughtered children. It was then killed by the tribe because the slaughter of children almost caused their extinction. She then explains Jessica is being controlled by her doll and visits Camillas sister, a nun at a convent. The nun already knew about her plight and even tells her its too late. Not giving up, Marilyn decides to take matters into her own hands. 

At home, Dolly shows its true nature to Marilyn. Both have few words and the battle begins from there. The dolls at the factory disconnect the telephone line and even one says, "its time to play." The mother goes back downstairs, loads ammo into the shotgun, returns to Jessicas room, and finds the doll gone. It becomes a cat and mouse chase briefly then gets wary. While the mother is battling, Jimmy strives to contact his father. When not getting through, he then calls the operator, but is unable to communicate because he doesnt speak Spanish language|Spanish, hangs up, and aides his mom. When Jessica becomes separated, the mother snatches her up and the family runs for the front door, only blocked by Dolly, who is holding the car key. Marilyn threatens Dolly to move out the way, but the Dolly calls to Jessica and says, "its time to play." Jessica attacks their mother. The shotgun slides on the floor toward Jimmy, who stands there, stunned. As the mother and daughter struggle, Dolly starts advancing towards them with a kitchen knife. Jimmy clutches the weapon, after figuring out how it works, he aims it at Dolly. Before pulling the trigger, he says, "play with this, bitch!" 

The impact of the bullets sent Dolly crashing through the door and Jessica becomes herself again. After regaining consciousness, Jessica embraces Marilyn and the family heads out to the factory to save their father. Resnick eventually opens the tomb, ignoring a mysterious womans plea whom he encountered the night before to close the tomb and found the legend to be true after all. Inside, lies an infant in skeleton with a head of an animal and evacuates to warn Elliot. Elliot barely survived the attack and makes it out alive with sustained injuries. With the aid of Resnick, both of them planted dynamite inside the factory. The dolls tried to disarm them, but failed. Meanwhile, Jimmy planted dynamite at the pit. The bombs explode, killing the dolls inside and the family and Resnick watches as the factory burns to the ground.

==Cast==
*Denise Crosby as Marilyn Wade, Eliots wife and Jessica and Jimmys mother. She is the protagonist.
*Sam Bottoms as Eliot Wade, Marilyns husband, Jessica and Jimmys father, and the toy maker and manufacturer of the "Dolly Dearest" toy factory. archaeologist who is trying to find the remains of the Sanzia Devil child.
*Lupe Ontiveros as Camilla, the housekeeper. She is later killed by Dolly.
*Candace Hutson as Jessica "Jessie" Wade, Eliot and Marilyns daughter and Jimmys younger sister. (Credited as Candy Hutson)
*Chris Demetral as Jimmy Wade, Jessicas older brother. 
*Ed Gale as the Dolly double. Serves as the films antagonist.

==Reception==
Critical reception for Dolly Dearest was mostly negative,  with DVD Verdict writing that "aside from a handful of fun moments, theres nothing much here to separate it from the lineup of Childs Play clones it dwells among."  Variety Magazine praised Hutsons performance but criticized the movies "clutzy dialogue".  DVD Talk panned the film, saying "in the "killer doll" subgenre of stupid horror flicks, Dolly Dearest may very well be the most moronic".  The Austin Chronicle also reviewed the movie, giving Dolly Dearest 2 1/2 out of 5 stars. 

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 