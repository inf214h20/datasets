Maniyan Pilla Adhava Maniyan Pilla
{{Infobox film
| name = Maniyan Pilla Adhava Maniyan Pilla
| image =
| image_size =
| caption =
| director = Balachandra Menon
| producer = EJ Peter
| writer = Balachandra Menon
| screenplay = Balachandra Menon Ambika
| music = G. Devarajan
| cinematography = K Ramachandrababu
| editing = G Venkittaraman
| studio = St.Martin Films
| distributor = St.Martin Films
| released =  
| country = India Malayalam
}}
 1981 Cinema Indian Malayalam Malayalam film, Ambika in lead roles. The film had musical score by G. Devarajan.   
 
==Cast==
  
*Maniyanpilla Raju as Maniyan Pilla 
*Venu Nagavally as Rahim 
*Jose Prakash  Ambika 
*Nanditha Bose Nithya
*Sankaradi
*Adoor Bhavani
*Balachandra Menon
*P. A. Latheef
*Jayashree
*Kaviyoor Ponnamma
 

==Soundtrack==
The music was composed by G. Devarajan. 
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF" align="center" 
| No. || Song || Singers ||Lyrics || Length (m:ss) 
|- 
| 1 || Aruthe Aruthe Enne Thallaruthe || P Madhuri, Krishnachandran || Poovachal Khader || 
|- 
| 2 || Manjurukunnu manassil || K. J. Yesudas || Poovachal Khader || 
|- 
| 3 || Mayilaanchiyaninju || P Madhuri || Poovachal Khader || 
|- 
| 4 || Rajakumari premakumari || K. J. Yesudas || Poovachal Khader || 
|}

==References==
 

==External links==
*   
*  

 
 
 


 