The Will (1939 film)
The the greatest Arabic language movies of all time, and has been voted the best Egyptian film of all time by Egypty.com, among other websites and magazines.

==Plot Synopsis== Egypt in the 1930s.

==Cast & Crew==

===Cast===
Hussein Sedki as Muhammad.

Fatma Rouchdi as Fatima.

===Crew===
Director: Kamal Selim

Cinematographer: Very Varkash

Music By: Abdul-Hamid Abdulrahman

Production Company: Egypt Film Studio

==Production Notes==
 
Director Kamal Selim took great care on the production design aspect of filming, in order to make it seem as if he had filmed live at the slums of Egypt. As a result, it is considered the first film to accurately and realistically display the slums of Egypt. 

==Themes==

===Portrayal of Slum Life===
The Will was one of the first films to accurately portray the slums of Egypt. It has been praised for its realistic depiction of life in the slums, and the struggles men and women go through within these slums. Released in 1939, The Will has carried on an enduring legacy, due in part to its sympathetic view on the lives of the average Egyptian people, who did not enjoy much luxuries at the time, and who often faced struggles with love and employment similar to the ones faced by the protagonist in the film, making the film a social commentary on its era.

==Critical Reception==
The Will is considered by many to be the greatest Egyptian film ever made, and is well respected, not only in Egypt, but in the international cinematic community as well. It was voted the #1 Egyptian film by egypty.com,  and is often cited as one of the greatest films ever made. 

==Legacy==
The Will is considered one of the first examples, or one of the precursors, of the Italian Neorealism movement in film, which would later expand to include films such as The Bicycle Thief and Pather Panchali. 

== References ==
 
 

== External links ==
*  

 

 
 
 
 
 
 