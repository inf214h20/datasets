Ondu Kshanadalli
{{Infobox film
| name = Ondu Kshanadalli
| image = Ondu Kshanadalli poster.jpg
| caption = Film poster
| director = Dinesh Babu
| producer = Jai Jagadish
| writer = Dinesh Babu
| dialogue writer = Dinesh Babu
| starring = Tarun Chandra Bhama Sanjjanaa
| music = Giridhar Diwan Goutham Shrivastav
| cinematography = Suresh Byrasandra
| editing =
| distributor =
| released =  
| runtime =
| country = India Kannada
| budget =
| gross =
}} Kannada romantic drama film  starring Tarun Chandra, Sanjjanaa and Bhama in the lead roles. The Story, screenplay, dialogues and Direction is by Dinesh Babu. Giridhar Diwan has composed music for the soundtrack, while Goutham Shrivastav has done the background score. Actor-producer Jai Jagadish has produced the venture. 

The film made its theatrical release on 5 October 2012 across Karnataka cinema halls.

==Plot==
The film has two heroines and the hero is not in love with either of them, he says, adding that the film is based on a story that happens on the spur of the moment towards the end of the first half. Tarun Chandra plays the role of Shyam who grows up in another city before he lands in Mysore. Shyams mother finds a match for him. But Shyam does not want to get hitched. He tries his best to get his marriage cancelled. 

== Cast ==
* Tarun Chandra as Shyam
* Sanjjanaa as Shilpa
* Bhama as Divya
* Bhaskar
* Sharan
* Umesh Jai Jagadish
* Sangeetha

==Critical reception==
Upon its release Ondu Kshanadalli received average to negative reviews from critics. The Times of India|TOI gave the film 3/5 star rating and praised the films script and narration.  However, Supergoodmovies criticized the film saying its baseless and meaningless giving it a rating of two out of five stars.  Srikanth Srinivasa of Rediff.com|Rediff gave the film 1.5/5 rating and wrote, "Ondu Kshanadalli is a somewhat slow and tedious film with some very insipid narration." 

== Soundtrack ==
{{Infobox album
| Name        = Ondu Kshanadalli
| Type        = soundtrack
| Artist      = Giridhar Diwan
| Cover       = 
| Alt         = 
| Released    =  
| Recorded    =
| Genre       = Film soundtrack
| Length      = 
| Label       = Anand Audio
}}
Giridhar Diwan has composed a total of 4 songs. All the songs lyrics are written by K. Ramnarayan. The Audio has been distributed by Anand Audio.

{|class="wikitable" width ="50%"
! Sl No. !! Song Title !! Singers
|-
| 1 || "Bisilethake" || Ajay Warrior, Anuradha Bhat
|-
| 2 || "Ee Kshana"  || Santhosh, Anuradha Bhat
|-
| 3 || "Mandarave Ninna" || Anuradha Bhat
|-
| 4 || "Shloka" || Ajay Warrior, Divya Raghavan
|-
|} 

==References==
 

 
 
 
 