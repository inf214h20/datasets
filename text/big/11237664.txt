Thicker than Water (2000 film)
{{Infobox Film
 | name           = Thicker Than Water
 | image          = Thicker Than Water (2000 film) poster.jpg
 | caption        = DVD cover
 | writer         = 
 | starring       = Raimana Boucher Saxon Boucher Timmy Curran Shane Dorian Brad Gerlach Jack Johnson Chris Malloy Emmett Malloy
 | producer       = 
 | distributor    = 
 | released       = May 12, 2000
 | runtime        = 45 minutes
 | language       = English
 | cinematography = 
 | editing        = 
 | music          = 
 | budget        = 
}}
 2000 documentary documentary surf Jack Johnson Chris Malloy. It shows surfing footage from different locations like Australia, Indonesia, Hawaii, India, and Ireland in combination with a wide range of styles of guitar music. Surfers in the film include Kelly Slater and Shane Dorian.

==Cast==
In alphabetical order:
*Raimana Boucher
*Saxon Boucher
*Timmy Curran
*Shane Dorian
*Brad Gerlach
*Conan Hayes Jack Johnson
*Noah Johnson
*Taylor Knox
*Rob Machado
*Chris Malloy
*Dan Malloy
*Emmett Malloy
*Keith Malloy
*Kelly Slater
*Benji Weatherly

==Soundtrack==
Released on November 25, 2003, the soundtrack to Thicker Than Water, which is scored by Johnson, collects 14 previously unavailable tracks by the likes of G. Love & Special Sauce, The Meters, Finley Quaye, and Johnson himself.

===Track listing=== Jack Johnson) Jack Johnson)
# "Even After All" - 3:55 (Finley Quaye)
# "Hobo Blues" - 2:44 (G. Love)
# "Relate to Me" - 1:34 (The Voyces) Jack Johnson) Jack Johnson)
# "Dark Water & Stars" - 4:59 (Natural Calamity)
# "My Guru" - 4:10 (Dan the Automator)
# "Honor and Harmony" - 3:36 (G. Love and Special Sauce)
# "Liver Splash" - 2:38 (The Meters)
# "Underwater Love" - 5:58 (Smoke City)
# "Thicker than Water" - 3:25 (Todd Hannigan)
# "Witchi Tai To" - 2:43 (Harpers Bizarre)

==Awards and honors==
Thicker than Water received   

==External links==
*  

==References==
 

 

 
 
 
 
 
 


 