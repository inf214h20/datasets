Lessons in Forgetting
{{Infobox film
| name           = Lessons in Forgetting
| image          =
| alt            =  
| caption        = 
| director       =Taimoor khan
| producer       = Prince Thampi
| screenplay     = Anita Nair
| based on       =  
| starring       = Adil Hussain Roshni Achreja Maya Tideman Raaghav Chanana
| music          = Ganesh Kumaresh
| cinematography = Viswamangal Kitsu
| editing        = Unni Vijayan Mandar Khanvilkar
| studio         = Arowana Studios
| distributor    = Arowana Studios
| released       =  
| runtime        = 110 minutes
| country        = India
| language       = English
| budget         = 1.2 million USD
| gross          = 
}}

Lessons in Forgetting  is a 2012 Indian film produced by Prince Thampi for Arowana Studios. It is based on the novel of the same name by Anita Nair and directed by Unni Vijayan. It stars Adil Hussain, Roshni Achreja, Maya Tideman and Raaghav Chanana. The film is shot in 35mm Cinemascope.
 2012 National Best Feature Film in English.

==Plot==

A renowned expert on cyclones J.A. Krishnamoorthy comes to India, when he learns that his daughter, Smriti, has met with a fatal freak accident. Determined to find out the truth behind his daughter’s accident, JAK starts to retrace his daughter’s path on the days that led to her accident in a small town in Tamil Nadu.

With a strong desire to know the truth, JAK embarks on his own investigation. He meets with the people who were close to Smriti. They give different versions of what they felt about her. JAK feels he is getting closer to the truth. But his hopes are shattered when he reaches a dead end.

Meera, a socialite, finds her life in chaos when her husband walks out on her leaving her with her two children, mother and her grandmother. Left to fend for herself, Meera takes up a job as an assistant to JAK. Inadvertently she leads him to a very important piece of the puzzle of the case.

Various events and coincidences, help him in getting closer to the truth. They find themselves back in Minjikapuram, the coastal town where it all began. Here JAK realises that, there was a bigger issue that Smriti was fighting against.

JAK must show the courage to confront the reality and forget the past.

==Cast==

* Adil Hussain as JAK
* Roshni Achreja as Meera
* Maya Tideman as Smriti
* Raaghav Chanana as Soman
* Karan Nair as Mathew
* Amey Wagh as Shivu
* Bhanu Prakash as Dr.Srinivasan
* Srilekha as Chinnataayi
* Anuja Vaidya as Nayantara
* Veena Sajnani as Meeras Mother
* Lakshmi Krishnamurthy as Meeras Grandmother
* Parthiv Shah as Nikhil
* Sukitha Aiyar as Vinnie
* Uttara Baokar as Kalachitti.

==Production==
Produced by Prince Thampi for Arowana Studios, the movie is based on the bestselling novel by the same title, written by Anita Nair. Prince Thampi is best known as the publisher of the Malayalam magazine Santham. Lessons in Forgetting is the first film to be produced  by Arowana Studios. Some of the staff members of Arowana Studios and the Parent Company Arowana Consulting Limited   also participated as extras in a few scenes.

===Filming locations===
 

Shot entirely in India across 
-Bangalore (Karnataka, India). 
-Pondicherry (Tamil Nadu,India). 
-Mahabalipuram (Tamil Nadu,India). 
-Coonoor/ Mettupalayam (Tamil Nadu,India).

==Crew==
{| class="wikitable"
| Director || Unni Vijayan
|-
| Screenplay || Anita Nair
|-
| Director of Photography || Vishawamangal Kitsu
|-
| Editing || Unni Vijayan/Mandar Khanvilkar
|-
| Sound Designer || Gissy Michael
|-
| Production Designer || Malay Bhattacharya
|-
| Makeup Designer || Roshan N.G
|-
| Music || Ganesh Kumaresh
|-
| Casting Director || Nilesh Maniyar
|-
| Costume Designer || Manoviraj Khosla
|-
| Executive Producer || Rajagopalan Achu
|}

==Awards==

* Unni Vijayan, Director - Outstanding Achievement In International Feature Filmmaking - Williamsburg International Film Festival,New York (2012)
* Adil Hussain : Best Actor - New Jersey Independent South Asian Cine Fest, New Jersey.(2012)
* Roshni Achreja : Best Supporting Actress - New Jersey Independent South Asian Cine Fest, New Jersey.(2012)
* Roshan N. G., Best Makeup Artist - Sunset International Film Festival, Los Angeles. (2012) 60th National Best Feature Film in English   

==Screenings==

* Rome International Film Festival 2012 (RIFF)
* Chicago South Asian Film Festival 2012 (CSAFF)
* Williamsburg International Film Festival, New York
* New Jersey Independent South Asian Cine Fest, New Jersey (2012)
* Sunset International Film Festival, Los Angeles (2012)

==References==
 
*  
* {{cite web
 | title =Arowana Studios  | url =http://www.arowanastudios.com/ | accessdate = May 28, 2013}}
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  
* 
* 

==External links==
 
*  
*  
* http://www.businessworld.in/bw/2010_02_09_Women_And_Womanhood.html 
*  
* http://timesofindia.indiatimes.com/topic/article/06lpfK8gUq4m7?q=Adil+Hussain 
* http://www.newkerala.com/news/world/fullnews-170168.html 
* 
*  
*  
*  

 
 
 
 