To an Unknown God
{{Infobox Film
| name           = A un dios desconocido
| image          = A un dios desconocido.jpg
| image_size     = 
| caption        = Theatrical release poster
| director       = Jaime Chávarri
| producer       = Elías Querejeta	
| writer         = Elías Querejeta Jaime Chávarri 
| narrator       = 
| starring       = Héctor Alterio Xabier Elorriaga Ángela Molina Mercedes Sampietro Rosa Valenty Mirtha Miller
| music          = Luis de Pablo
| cinematography = Teodoro Escamilla          
| editing        = 
| distributor    = 
| released       = 16 September 1977 Spain
| runtime        = 104 minutes
| country        = Spain
| language       = Spanish
| budget         = 
}}
 1977 Spain|Spanish film directed by Jaime Chávarri. The film is about an aging man coming to terms with his homosexuality and mortality.  It was a pioneer in his frank and mature examination of homosexuality.

==Plot ==
José, a middle age magician, is an elegant discreet homosexual who lives alone and has an occasional affair with Miguel, a young politician who finds it more convenient in Madrids high society to marry than assert his homosexuality. José is a man romantically possessed and obsessed by his childhood in Granada during the outbreak of the Spanish Civil War in the spring of 1936.

Now in his fifties, José returns to Granada and relives his childhood there. A time when he fell in love with García Lorca and had a youthful affair with one of Lorcas own lovers. Memories come flooding back to the mature José, of youthful sexual conquest, of Lorcas murder at the hands of Francisco Franco|Francos agents, and his own early homosexual affairs. Josés entire life is colored by his obsessions with García Lorca, his unknown God, to whom the film is dedicated.

José travels twice to Granada. First, he revisits a woman who is also obsessed with García Lorcas memory, and steals a photograph of the boy with whom he had his first sexual encounter; later, José returns to Madrid, to a party in search of his youth, and meets a pianist with whom he had sexual relations many years before but now does not remember.

When José returns to Madrid, he is a man tormented by his past, and in search of peace. Listening to a taped recording of García Lorcas famous "Ode to Walt Whitman", he desires nothing more than to face the rest of his life in loneliness, although his recent lover, Miguel has returned to his bed and wants to continue their affair. José realizes that he is really all alone in their world, alone with his God.

==Cast==	
* Héctor Alterio-  José
* Xabier Elorriaga - Miguel
* Maria Rosa Salgado - Adela
* Rosa Valenty - Clara
* Ángela Molina- Soledad
* Margarita Mas - older Soledad
* Mercedes Sampietro- Mercedes
* José Joaquin Boza - Pedro

==Awards==
The film was the Grand Prize winner at the Chicago International Film Festival of 1978 and was part of the American Film Institute series of Spanish films which traveled throughout North America in 1979-1980. Hector Alterio also won the Best actor award at the San Sebastian Film Festival in 1977 for his performance as José.

* 3 Awards at San Sebastián International Film Festival: Best Spanish Film, Best Actor and OCIC Award.

== References ==
* Schwartz, Ronald, The Great Spanish Films: 1950- 1990,Scarecrow Press, London, 1991, ISBN 0-8108-2488-4

== External links ==
*  

 
 
 
 
 
 
 
 
 