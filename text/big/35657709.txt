Leprechaun: Origins

{{Infobox film
| name           = Leprechaun: Origins
| image          = LeprechaunOrigins Poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Zach Lipovsky
| producer       = Chris Foss Michael Luisi
| writer         = Harris Wilkinson
| based on       =   Dylan "Hornswoggle" Postl Stephanie Bennett Teach Grant Bruce Blain Adam Boys Andrew Dunbar Melissa Roxburgh Brendan Fletcher
| music          = Jeff Tymoschuck
| cinematography = Mahlon Todd Williams
| editing        = Shawn Montgomery
| studio         = WWE Studios
| distributor    = Lionsgate Films
| released       =  
| runtime        = 90 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} reboot for the series.  WWE Studios President Michael Luisi has described the film as "a little darker, a little more traditional horror than the Warwick Davis ones that people remember".  
 VOD on 26 August and a DVD/Blu-ray release on September 30, 2014.  

==Plot==
 
Backpacking through the lush Irish countryside, two unsuspecting young couples discover a towns chilling secret. Ben (Dunbar), Sophie (Bennet), David (Brendan Fletcher) and Jeni (Roxburgh) quickly discover the idyllic land is not what it appears to be when the towns residents offer the hikers an old cabin at the edge of the woods. Soon, the friends will find that one of Irelands most famous legends is a terrifying reality. The four people find out that the town is really using them as sacrifices to the Leprechaun. The town stole the gold from the Leprechaun and lured people outside the village to be killed by the Leprechaun so the Leprechaun wouldnt kill them. During the night, two evil villagers were killed by the Leprechaun. At the end, Sophie becomes the last survivor and she makes it out. She manages to kill the Leprechaun, only to find out that there is more than one Leprechaun.

==Cast== Dylan "Hornswoggle" Postl as Lubdan/Leprechaun
* Stephanie Bennett as Sophie Roberts
* Teach Grant as Sean McConville
* Bruce Blain as Ian Joyce
* Adam Boys as Francois
* Andrew Dunbar as Ben
* Melissa Roxburgh as Jeni
* Brendan Fletcher as David
* Emilie Ullerup as Catherine

==Marketing==
 
On March 17, 2014, WWE Studios official YouTube channel premiered a clip from the film with an introduction by Dylan Postl, in light of Saint Patricks Day. 

==Reception==
  Red Clover, but what else do you expect from an unintelligible cash-in short on reveals, lacking on death scenes, and heavy on infuriating redundancy?" 

==References==
 

==External links==
*  
*  
 
 

 
 
 

 
 
 
 