Go (2007 film)
{{Infobox film name = Go image = go2007.jpg caption = Theatrical Poster writer = Arshad Sayed starring = Gautam Gupta Nisha Kothari Kay Kay Menon director = Manish Shrivastava producer = Ram Gopal Varma music = Sneha Khanwalkar released = 5 October 2007 country =  India language = Hindi
}}

Go is a Bollywood film starring Gautam Gupta, Nisha Kothari and Kay Kay Menon in the lead roles. It is directed by Manish Shrivastava and produced by Ram Gopal Varma. 

==Synopsis==
Abhay (Gautam Gupta) and Vasundhara (Priyanka Kothari) are neighbors who are in love. Due to strong opposition from their parents, they decide to elope to Goa. Meanwhile, Chief Minister Arjun Patil (Ravi Kale) has his Deputy Chief Minister Praveen Deshpande (Govind Namdeo) murdered, planning of which is audio-taped by his assistant Bihari. Bihari then decides to blackmail the Chief Minister (CM), whose men eventually kill Bihari but not before he drops the tape in Abhay and Vasundharas car. This leads to the couple being chased by CMs goons and Inspector Nagesh Rao (Kay Kay Menon), who wants to get hold of the tape in order to exhort money from the CM.

==Cast==
* Gautam Gupta as Abhay Narula
* Priyanka Kothari as Vasundhara Vasu Dave
* Kay Kay Menon as Nagesh Rao
* Rajpal Yadav as Kay Jagtap Tiwari
* Priyanka Sharma as Sanjana Joshi
* Ravi Kale as Chief Minister Arjun Patil
* Sherveer Vakil as T. S. Ranga Ranganathan
* Govind Namdeo as Deputy Chief Minister Praveen Deshpande

==References==
 

==External links==
*  

 

 
 
 
 

 