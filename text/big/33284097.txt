Danger Has Two Faces
 
 
{{Infobox film
| name           = Danger Has Two Faces
| image          = DangerHasTwoFaces.jpg
| alt            = 
| caption        = Film poster
| film name = {{Film name| traditional    = 皇家大賊
| simplified     = 皇家大贼
| pinyin         = Huáng Jiā Dà Zéi
| jyutping       = Wong4 Gaa1 Daai6 Caak2}}
| director       = Alex Cheung
| producer       = Mona Fong
| writer         = Alex Cheung Yuen Ka Chi
| starring       = Bryan Leung Paul Chu Carroll Gordon Liu Lai Ling Fei Xiang
| music          = Stephen Sing So Chan Hau
| cinematography = Lee San Yip
| editing        = Siu Fung Ma Chung Yiu Chiu Cheuk Man
| studio         = Shaw Brothers Studio
| distributor    = Shaw Brothers Studio
| released       =  
| runtime        = 90 minutes
| country        = Hong Kong
| language       = Cantonese
| budget         = Hong Kong dollar|HK$4,128,000
| gross          =
}} Hong Kong action film directed by Alex Cheung and starring Bryan Leung, Paul Chu and Fei Xiang. The film is produced and distributed by Shaw Brothers Studio.

==Plot==
Many years ago, Inspector Kam Chi Kin (Bryan Leung) killed an innocent man and decided to resign from the police force. He then became a professional killer, paid by Uncle Hung (Che Hung), and disguises as a pet store owner in order to earn money to raise his son. His best friend, Bobby Chow Fuk Cheung (Fei Xiang) recently returned from UK and works under Superintendent Lau Cheuk Sang (Paul Chu). Chow has been investigating recent murder cases that were done by Kam. Later, as one of his colleague Sam (Kirk Wong) is murdered, Chow discovered the mastermind behind these cases were actually Superintendent Lau. Lau then murders one of his subordinate Man (Cheung Ming Man), mistaken him to have discovered his identity. Later, Uncle Hung takes Kams son hostage and orders him to kill Chow. Kam later disguises Chows murder and they both confront Lau, Hung, the gang and two police officers Wai (Chan Chik Wai) and Chicken Blood.

==Cast==
*Bryan Leung as Kam Chi Kin
*Paul Chu as Superintendent Lau Cheuk Sang
*Carroll Gordon as Jenny
*Liu Lai Ling as Ling
*Fei Xiang as Inspector Bobby Chow Fuk Cheung
*Cheung Ming Man as Man
*Kirk Wong as Sergeant Sam Leung
*Pamela Pak as pet store customer
*Lam Fai Wong as robber
*Cheng Kei Ying as robber
*Charlie Cho as man impersonating cop
*Paul Che as armoured car driver
*Pak Sha Lik as man shot by Kam (flashback)
*Chan Chik Wai as Wai
*Leung Siu Wah as robber
*Wong Kim Fung as robber who fights with Bobby
*Kam Piu as man outside hotel
*Fung King Man as man meeting Sergeant Sam
*Kei Ho Chiu as robber
*Tam Tin Nam as tall man at bus stop
*Yuen Ling To as armoured car guard
*Hon Kong as man at police station
*Ko Hung as bodyguard
*Amy Au as wife of man shot by Kam
*Che Hung as Uncle Hung
*Fung Ming as man meeting Sergeant Sam
*Cheung Kwok Wah as truck driver

==Box office==
This film grossed Hong Kong dollar|HK$4,128,000 during its theatrical run from 31 May to 13 June 1985 in Hong Kong.

==External links==
* 
*  at Hong Kong Cinemagic
* 

 
 
 
 
 
 
 
 
 
 
 
 
 