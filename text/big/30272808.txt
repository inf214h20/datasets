Everyone Else
{{Infobox film 
| name           = Everyone Else
| image          = Everyone Else.jpg
| alt            = 
| caption        = 
| director       = Maren Ade
| producer       = Maren Ade Dirk Engelhardt Janine Jackowski
| screenplay     = Maren Ade
| starring       = Birgit Minichmayr Lars Eidinger
| music          = 
| cinematography = Bernhard Keller
| editing        = Heike Parplies
| studio         = Komplizen Film
| distributor    = The Cinema Guild (United States)
| released       =  
| runtime        = 119 minutes   
| country        = Germany
| language       = German
| budget         =
| gross          =
}}
Everyone Else ( ) is a 2009 German drama film written and directed by Maren Ade, a German director.

== Cast==
* Birgit Minichmayr as Gitti
* Lars Eidinger as Chris
* Nicole Marischka as Sana
* Hans-Jochen Wagner as Hans

==Awards and nominations==

=== Film Awards===
{| border="1" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 90%;"
|-  style="background:#b0c4de; text-align:center;"
! Year !! Film Festival !! Award !! Category
|- 2009 || Best Film  
|- 2009 ||  Berlin Film Festival || Silver Bear || Best Actress
|- 2009 ||  Berlin Film Festival || Femina Film Prize || Best Production Design
|}

===Submissions===
*Berlin Film Festival
**Golden Bear (nominated)
* German Film Awards
** Best Direction (nominated)
** Best Performance by an Actress in a leading role (nominated)
** Outstanding Feature Film (nominated)

==Critical reception==
The film received positive reviews from film critics. Review aggregator Rotten Tomatoes reports that 89% out of 35 professional critics gave the film a positive review, with a rating average of 7.7/10. 

==References==
 

==External links==
*  
*  , Kevin Lee, MUBI

 
 
 
 

 
 