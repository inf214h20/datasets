OSS 117: Cairo, Nest of Spies
 
{{Infobox film
| name           = OSS 117: Cairo, Nest of Spies
| image          = OSS_117,_Le_Caire_nid_despions_poster.jpg
| caption        = French promotional poster
| director       = Michel Hazanavicius
| producer       = Éric Altmeyer Nicolas Altmeyer
| writer         = Jean Bruce novel series Jean-François Halin Michel Hazanavicius
| starring       = Jean Dujardin Bérénice Bejo Aure Atika
| music          = Ludovic Bource Kamel Ech-Cheikh
| cinematography = Guillaume Schiffman
| editing        = Reynald Bertrand Gaumont
| released       =  
| runtime        = 99 minutes
| country        = France French Arabic Arabic
| budget         = $14,090,000 
| gross          = $37,823,018  
}}
OSS 117: Cairo, Nest of Spies (OSS 117 : Le Caire, nid despions, 2006) is a French comedy film directed by Michel Hazanavicius. It is a parody of the spy film genre. The film follows the exploits of a French secret agent, OSS 117, in Cairo in 1955.

==Plot==
 OSS agent, Soviet cargo Belgian spy Nazis from the beginning.

Throughout the film the main character has two main romantic interests. The first is an Egyptian princess Al Tarouk, who cant resist the charms of OSS 117. The second is the former assistant of Jack Jefferson, Larmina El Akmar Betouche, who at first shows no interest in the main character - and in fact temporarily becomes a secondary villain due to OSS 117s continued crass statements about her religion - but warms up to him in the end.

==Cast==
* Jean Dujardin as Hubert Bonisseur de La Bath, AKA OSS 117
 Philippe Lefebvre as Jack Jefferson
* Claude Brosset as Armand Lesignac
* Éric Prat as Gilbert Plantieux
* Aure Atika as Princess Al Tarouk
* Bérénice Bejo as Larmina El Akmar Betouche
* Constantin Alexandrov as Ieveni Setine
* Laurent Bateau as Nigel Gardenborough
* François Damiens as Raymond Pelletier
* Richard Sammel as Gerhard Moeller
* Said Amadis as Egyptien spokesman
* Youssef Hamid as the imam 
* Khalid Maadour as the man following OSS 117
* Arsène Mosca as Loktar, 
* Abdallah Moundy as Slimane
* Alain Khouani as the hotel receptionist

==Production==
The film is a continuation of the OSS 117 series of spy films from the 1950s and 1960s, which were in turn based on a series of novels by Jean Bruce, a prolific French popular writer. However, instead of taking the genre seriously, the film parodies the original series and other conventional spy and Eurospy films, most noticeably the early James Bond series right down to the cinematography, art direction, music and costume of the 1960s (although this is a slight anachronism as the film is stated in dialogue to be set in 1955, hence a sequence where OSS 117 briefly dances the twist is out of place). For example, driving scenes are all filmed with obvious rear projection, night scenes were clearly shot during the day with a blue filter and camera movements are simple, and avoid the three-dimensional Steadycam and crane movements that are easily accomplished today. The scene at the Cairo airport was filmed in the entrance hall of a campus of Panthéon-Assas University.  
 
 The Artist, a film that, like Cairo, Nest of Spies, pays tribute to a past genre of filmmaking.

==Reception==
The film won the  . 
 Get Smart, and the most lovingly detailed period pastiche since Todd Hayness Far from Heaven." 

==References==
 

==External links==
* 
* 
* 
* 
* 

 
 
 

 
 
 
 
 
 
 
 
 
 
 