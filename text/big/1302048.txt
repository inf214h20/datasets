PpilKu
{{Infobox film name           = Ppilku image          = Ppilku.jpg caption        = director       = You Jin-sun producer       = Kwak Jeong-hwan writer         = Youn Bo-hyun   Choi Seon-wu   Yu Ha-sun starring  Lee Min-woo   Kim Geum-yong  music          = Kang In-goo cinematography = Kim Nam-jin editing        = Park Soon-duk distributor    = released       =   runtime        = 97 minutes country        = South Korea language       = Korean budget         = film name      = {{Film name hangul         =   hanja          = 삘  rr             = Bilgu mr             = Ppilku}}
}}
Ppilku is a 1997 South Korean teen drama film.

==Plot==
Pil-gu is a high school athlete and the leader of an underground club called Shock. Along with the Shocks, the judo club and a girls club called Sexy Wave led by Hee-jeong cause plenty of trouble and mayhem in the school. The arrival of a pretty young teacher named Yoo Yuna gets the students in gear for another big scheme. Each club sets up a strategy to lure the new teacher. After beating numerous contenders, Pil-gu approaches Yuna.

==Cast==
  Lee Min-woo as Pil-gu
*Kim Geum-yong as Yoo Yuna
*Kim Ki-yeon as Hee-jeong
*Hong Il-kwon as Hong-ik
*Kim Seong-su as Do-sik
*Won Ki-joon as Ki-chul
*Lee Dae-ro as Principal
*Oh He-chan as Student-in-charge
*Cho Hak-ja as Guidance counselor
*Kim Gyeong-jin as Female teacher
*Ra Kap-sung as Seon-joo
*Hong Chung-gil as Sailor
*Kim Jeong-yeon as Dong-ja
*Lee Dong-yeop as young Pil-gu
*Kim Seong-cheol as Yong-gu
*Kil Do-tae-rang as Detective squad chief
*Hong Sung-young as Detective Hong
*Yoo Seong as Detective Yoo
*Park Jae-seok as Shock member
*Bae Yong-joon as Shock member
*Kim Yu-heung as Shock member
*Lee Sung as Shock member
*Lee Se-chang as Shock member
*Kim Mun-beom as Shock member
*Yu Su-mi as Sexy Wave member
*Oh Yeong-mi as Sexy Wave member
*Park Yu-mi as Sexy Wave member
*Im Hae-won as Sexy Wave member
*Park Hye-yeong as Sexy Wave member
 

==Production== Salut DAmour, KBS drama series. 

==References==
 

==External links==
* 
* 

 
 
 
 

 
 