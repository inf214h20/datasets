Ragada
{{Infobox film
| name           = Ragada
| image          = Ragada2010Poster.jpg
| caption        = Indian poster
| writer         = Veeru Potla    
| producer       = D. Siva Prasad Reddy
| director       = Veeru Potla
| starring       = Nagarjuna Akkineni Anushka Shetty Priyamani
| music          = S. Thaman
| cinematography = Sarvesh Murari
| editing        = Marthand K. Venkatesh
| studio         = Kamakshi Movies
| released       =  
| runtime        = 
| country        =   India
| language       = Telugu
| budget         =
| gross          =
}}  
 Tollywood Action action film produced by D. Siva Prasad Reddy on Kamakshi Movies banner, directed by Veeru Potla. Starring Nagarjuna Akkineni, Anushka Shetty, Priyamani in lead roles and music is composed by S. Thaman. The film dubbed into Hindi under same title & in Tamil as Vambhu.  The film recorded as Super Hit at box-office.  

== Synopsis ==
The movie starts out with Tanikelli Bharani trying to kill an innocent man, who is against Pedhanna. Tanikelli is killed by Jairam, who is one of Peddhannas followers. Peddhanna is the biggest goon in all of AP, and he has 3 major followers who murder people for him. The other henchman just kidnap the wanted. These followers are, Jairam, Bhagavan, and Nanda. The next scene introduces Satya, getting of a truck and getting involved in a fight, which is against GK, and Peddhanna. Satya helps GK, who makes him his partner. Here, they find out that Satya is very money minded. GKs love, Sirisha, who does not love GK back, gets captured by Satya, and falls in love with him. Brahmanandam is another important character in the story whose major role is flirting with Sirisha. Priyamani enters as Ashtalakshmi, being chased by rowdies. Obviously, Satya beats them up, and finds out that the rowdies are Pedhannas rowdies following Ashtalakshmi, for a reason she does not know of. Later, her whole family is introduced as a Brahmin family.

Satya tells good tactics to GK, to fight Peddhanna. In one fight, Jairam captures Sirisha, and GK captures Jairam. Not wasting any time, Satya kills Jairam. Once again, Sirisha falls deeper in love with him. Satya gets another warning from Bhagavan, Peddhanas follower, to join him; but Satya does not. Ashtalakshmi, then, also falls in love with him. Later, Sirisha takes Satya to meet his friends at a pub, and she talks with a tattooed girl, whose face is not shown to Satya. In the next scene, Satya takes Ashtalakshmi on a date with him, that was supposed to be his date with Sirisha. Satya is cornered and goes to Bhagavans house, and kills him, and his son.

Satya thinks about why he joined the goons gang. It is for money. The movie jumps to Satyas flashback, where they find out that Satya is an orphan, taken into care by a loving doctor, who is like Mother Teresa. The people of this City, Kadapa, worship this doctor. At that time, an election is taking place. The people want the Dr. to decide on who to vote for. Devyander, a political campaigner and who also has the support of Pedhanna, kidnaps Drs daughter, to make sure she tells everyone to vote for him. However, she does not. Satya enters on cue, and beats up Devyanders men. It is later found out that he has to get 72 crores to keep the Drs hospital running, as Devyanders dad donated the money to the Dr, to open the hospital. He also kills the doctor at one point of time. So, to get the money and to take revenge against the goons who killed the doctor, Satya enters into the city.

At this time, Satya is back in his house and is dealing with Ashtalakshmis parents who are weeping uncontrollably. He finds out that Peddhannas men long ago kidnapped Ashtalakshmis elder brother, and that is why they came to the city. But, as they can not find him, they ask Satya to help. Satya goes to the headquarters where her brother is held, and frees him. Then he finds out that Ashtalakshmi is not who she says she is, and actually robbed 180 crores from Peddhanna with the help of her brother.

The rest of the story is how Satya gets the money and figures out the link between GK and Pedhanna.

==Cast==
{{columns-list|3| Nagarjuna Akkineni as Satya Reddy
* Anushka Shetty as Shirisha
* Priyamani as Priya alias Ashta Lakshmi Pradeep Rawat as Peddhanna 
* Kota Srinivasa Rao as Gangaiah
* Dev Gill as G.K, opposition of Peddhanna
* Brahmanandam as Braham darling
* Tanikella Bharani as Devudu 
* Dharmavarapu Subramanyam
* Sushant Singh as Nanda
* Supreth as Bhagawan (son of Gangaiah)
* Sravan 
* Bharath Reddy (actor)|Dr.Bharat Reddy  
* Amith
* Satya Prakash 
* Benerjee 
* Raghu Babu as Hotel Server  Pruthvi Raj 
* Giridhar 
* Venu 
* Raghu 
* Sasidhar  
* Sandesh 
* Srikanth 
* Anand Bharathi 
* Narsingh Yadav  Jenny
* Master Bharath as Narayana 
* Giridhar as Govardhan
* Sandesh
* Sreekanth Reddy
* Charmy Kaur in an Item number Song Anitha  Vennirade Nirmala as Nirmalamma
* Sana 
* Srilalitha 
* Rajani
}}

==Soundtrack==
{{Infobox album 
| Name = Ragada
| Artist = S. Thaman
| Type = Soundtrack
| Cover = Ragada Audio CD.jpg
| caption = Original cover
| Released =  
| Recorded = 2010
| Genre = Film soundtrack
| Length =  
| Label = Aditya Music
| Producer = S. Thaman Brindavanam  (2010)
| This album = Ragada (2010)
| Next album = Mirapakaay|Mirapakay (2010)
}}

The music and background score was composed by S. Thaman. The audio was launched on 29 November 2010 amidst the fans at the Shilpa Kala Vedika in Hyderabad. Almost all the members of Akkineni family, including Akkineni Nageswara Rao, Nagarjuna, Naga Chaitanya, Akhil, Sumanth and Sushanth, besides hundreds of fans attended the function. Akkineni unveiled the audio CD and handed over the first piece to Nagarjuna. 

Music composed by S. Thaman. Lyrics written by Ramajogayya Sastry. Music released on ADITYA Music Company.
{{Track listing
| collapsed =
| headline =
| extra_column = Singer(s)
| total_length = 25:21
| all_writing =
| all_lyrics =
| all_music =
| writing_credits =
| lyrics_credits = 
| music_credits =

| title1 = Meesamunna Manmadhuda 
| extra1 = Shankar Mahadevan,Rita,Hima Bindhu 
| length1 = 4:46

| title2 = Shirisha Shirisha
| extra2 = Hariharan (singer)|Hariharan,Srivardhini Dhaman
| length2 = 4:07

| title3 = Okkadante Okkade
| extra3 = Ramya NS,Suchitra
| length3 = 3:20
 
| title4 = Bholo Ashta Lakshmi
| extra4 = Karthik (singer)|Karthik,Geetha Madhuri
| length4 = 4:14
 
| title5 = Ragada Ragada
| extra5 = Baba Sehgal,K. S. Chitra|Chitra,Rita
| length5 = 4:44
 
| title6 = Empillo Apple O
| extra6 = Karthik,Anuradha Sriram
| length6 = 3:53
}}

==References==
 

==External links==
*  
*  

 
 
 