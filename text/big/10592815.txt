Main Azaad Hoon
{{Infobox Film
| name           = Main Azaad Hoon
| image          = Main Azaad Hoon.jpg
| image_size     = 200px
| caption        = Film Poster
| director       = Tinnu Anand
| producer       = 
| writer         = Javed Akhtar
| narrator       = 
| starring       = Amitabh Bachchan Shabana Azmi
| music          = Amar Biswas Utpal Biswas	 
| cinematography = 
| editing        =
| distributor    = Nadiadwala Sons
| released       = 15 Dec 1989
| runtime        = 
| country        = India
| language       = Hindi
| budget         = 
}}
 1989 Hindi film, an Indian adaptation of 1941 Frank Capra film, Meet John Doe by Javed Akhtar about an opportunistic journalist who concocts a fictitious man in a fictitious article to boost newspaper sales, but when the article gets huge response, she finds an unemployed man to sit in as Azaad, "man of the masses". The film was directed by Tinnu Anand, and starred Amitabh Bachchan and Shabana Azmi.  

The film has only one song "Itne Baazu Itne Sar" written by Shabana Azmis real life father and noted poet Kaifi Azmi and sung by the lead actor himself, Amitabh Bachchan. 
 Best Dialogue for Javed Akhtar. 

==Plot==

The movie begins with a female journalist named Subhashini (Shabana Azmi) working for a daily newspaper and mostly in controversy due to her bold and open mouthed articles against corrupt politicians.

After a change in the ownership of the newspaper, she realizes that she stands to lose her job along with many others, especially who are not likely to subscribe to the new managements policies. Feeling betrayed she spews out her venom by penning her column with a fictitious letter ad verbatim,  which is supposedly written to her by someone named Azaad. This letter openly criticizes a cross-section of media and the establishment vocalizing unacknowledged and uncomfortable facts. The letter also says that the author of the letter would commit suicide from a high-rise building on 26 January, the Republic Day of India, if certain conditions are not met with.

In this new development, the new owner, Seth Gokulchand (Manohar Singh), senses an opportunity. He thinks of a scheme to promote his newspaper and coaxes Subhashini to establish a column in the name of Azaad and write about the ills of present society and administration. The task for them now is to find a face and character for Azaad, should the question arise about the real identity of the author of the column and the letter.

One day she come across a jobless and nameless vaudevillian character, Amitabh Bachchan), who happens to be drifting by Rajnagar along with another bum friend of his. Subhashini offers him a job asking him to pose as Azaad. The bum sees a chance in it to make a few quick bucks. With this in mind that he has nothing to lose, he accepts the job. Subhashini makes use of the whole propaganda machinery at her disposal to create a public figure for Azaad. Azaad is steadily introduced in the media and local issues, made to attend various public rallies. His native, down to the earth charm finds instantaneous appeal with the people of the town. This draws gullible public to his frequent public addresses. Word spreads like wild fire and soon people from the surrounding rural areas drain in to attend his public appearances. This gathers momentum and soon Azaad becomes a cult figure. Seth Gokulchand initiates the idea of making Azaad a much wider-publicised figure to Subhashini. She very efficiently works on the idea writing articles on Azaad, on Azaads ideas. This results into Azaad becoming a kind of a nation-wide hero, and is soon perceived as a threat by the local politicians as a potential national leader.

Later he learns through the media that he has been used however he decides to sacrifice for nation and prove that he had evolved as the imaginary character AZAD over a period of time. Azad becomes a leader of masses and corrupt politicians challenge him to prove his love for people and nation, he jumps from a 30 story structure under construction building to prove that and dies but before that he records a message for his supporters  and urges that AZAD should evolve in each one of them.

==Cast==
* Amitabh Bachchan - Azaad
* Shabana Azmi - Subhashni Saigal
* Anupam Kher - Dalchand Jain
* Manohar Singh - Gokuldas
* Ajit Vachani - Editor Sharma
* K.K. Raina - Newspaper editor
* Ram Gopal Bajaj - Rambhau Kaka (Village Sarpanch)
* Raja Bundela	- Anwar
* Avtar Gill - IG Nagu
* Annu Kapoor - Munna
* Anjan Srivastav - Rastogi - CMs PA
* Abha Parmar - Worker

==References==
 

==External links==
*  

 
 
 
 