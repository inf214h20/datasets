The Honorable Friend
{{Infobox film
| name           = The Honorable Friend
| image          =
| alt            = 
| caption        = 
| director       = Edward LeSaint
| producer       = Jesse L. Lasky
| screenplay     = Elizabeth McGaffey Eve Unsell
| starring       = Sessue Hayakawa Tsuru Aoki Raymond Hatton Goro Kino M. Matsumato William Elmer
| music          = 
| cinematography = Harold Rosson	
| editing        = 
| studio         = Jesse L. Lasky Feature Play Company
| distributor    = Paramount Pictures
| released       =  
| runtime        = 50 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =
}}
 drama silent film directed by Edward LeSaint and written by Elizabeth McGaffey and Eve Unsell. The film stars Sessue Hayakawa, Tsuru Aoki, Raymond Hatton, Goro Kino, M. Matsumato and William Elmer. The film was released on August 27, 1916, by Paramount Pictures.  

==Plot==
 

== Cast ==
*Sessue Hayakawa as Makino
*Tsuru Aoki as Toki-Ye 
*Raymond Hatton as Kayosho
*Goro Kino as Goto 
*M. Matsumato as Hana 
*William Elmer as Murphy

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 
 

 