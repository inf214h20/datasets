Delitto sull'autostrada
{{Infobox film
 | name =Delitto sullautostrada 
 | image = Delitto sullautostrada.jpg
 | caption =
 | director = Bruno Corbucci
 | writer = 
 | starring =  Tomas Milian
 | music =  Franco Micalizzi
 | cinematography =  Silvano Ippoliti
 | editing =  Daniele Alabiso
 | producer = 
 | language =  Italian
 | budget =
 }} 1982 Cinema Italian "Poliziotteschi|poliziottesco"-comedy film directed by Bruno Corbucci. It is the ninth  chapter in the Nico Giraldi film series starred by Tomas Milian.    

== Cast ==
* Tomas Milian: Nico Giraldi
* Viola Valentino: Anna Danti
* Bombolo: Venticello
* Olimpia Di Nardo: Angela
* Paco Fabrini: Rocky Giraldi
* Marcello Martana: Commissioner Trentini Tony Kendall: Tarquini
* Gabriella Giorgelli: Bocconotti Cinzia
* Giorgio Trestini: Andrea Carboni
* Adriana Russo: Miss Carboni
*Marina Hedman: Woman at the confessional
* Ennio Antonelli: Trainer  
* Enzo Andronico: Receiver of stolen goods
* Andrea Aureli: Mr. Mariotti

==References==
 

==External links==
* 

 
 
 
 
 
 
  

 
 