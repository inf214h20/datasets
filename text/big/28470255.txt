The Wind of Change (film)
{{Infobox film
| name           = The Wind of Change
| image          = Windofchange1961.jpg
| image_size     = 200px
| caption        = UK DVD cover
| director       = Vernon Sewell
| producer       = John Dark
| writer         = Alexander Doré John McLaren Johnny Briggs Ann Lynn
| music          = Norman Percival
| cinematography = Basil Emmott
| editing        = Peter Pitt
| distributor    = Bryanston Films (UK)
| released       = March 1961
| runtime        = 64 minutes
| country        = United Kingdom
| language       = English
}}
 1961 British Johnny Briggs Wind of Change" speech given by British Prime Minister Harold Macmillan in South Africa in February 1960, it is one of the earliest British films to tackle race as an issue, focussing on disenchanted working-class white youth finding an outlet for their frustrations in racial hatred.  The film is set around the coffee bars and homes of the Notting Hill district of West London, scene of the notorious 1958 Notting Hill race riots.

==Plot==
Frank (Briggs) is an unemployed, discontented and rebellious teenage Teddy Boy, living at home with his mild-mannered father (Pleasence), domineering mother (Hilda Fenemore) and sister Josie (Lynn).  Frank harbours a deep-seated resentment and hatred towards the black people he sees as flooding Notting Hill and taking all the jobs.  He spends his time hanging around with a gang of similar youths who all share his racist views.

After an evening spent wandering round the local coffee bars, the gang go looking for trouble and decide to beat up a black youth for kicks.  They are inflamed to see a black boy accompanied by a white girl, and they chase them through the dark streets before cornering them and launching a vicious assault on the couple using fists, feet, knives and bicycle chains.   The boy is so brutally beaten that he later dies from his injuries in hospital, and the girl is stabbed during the melee.  Frank is horrified when he realises that the injured girl is his sister.

As a police investigation begins, Franks family are shocked by his involvement and try to discover why he feels the way he does.  Josie in particular challenges his racist views and involvement in a gang culture of mindless violence towards those who have done him no harm.  The gravity of what has happened causes Frank to reconsider his attitudes, and he determines to leave behind his gang involvement and focus on finding a job.

==Cast==
* Donald Pleasence as Pop Johnny Briggs as Frank
* Ann Lynn as Josie
* Hilda Fenemore as Gladys
* Glyn Houston as Sgt. Parker
* David Hemmings as Ginger
* Angela Douglas as Denise
* Norman Gunn as Ron
* Bunny May as Smithy
* Patricia Garwood as Lina
* Topsy Jane as Peggy
* Antonita Dias as Sylvia
* Rosemary Frankau as Woman in Mews

==Reputation==
Although originally shot as a B-movie, The Wind of Change is considered to be historically significant by British film observers as an example of a smaller studio producing a film centred on subject matter which, at the time, larger studios would not have touched.  While some of the racial epithets and abuse used in the film may sound offensive to contemporary viewers, it is regarded as an accurate reflection of the attitudes of its time, and significant in highlighting what Eleni Llarou of the British Film Institute describes as "the underbelly of Macmillans affluent society, in which the delinquency of a teenage culture had more to do with educational failure, lack of occupational aspiration, the pall of boredom and the economic struggles of the English working class than any deep racial clash".

The film is studied alongside other British films of the era, such as Sapphire (film)|Sapphire (1959) and Flame in the Streets (1961), which confronted the subject of racial attitudes in Britain at a time when it was a hot political and social issue, but rarely tackled on screen.  The Wind of Change is seen as very much a product of its time, inasmuch as it is presented entirely from the perspective of its white characters.  Llarou notes: "Although they are portrayed in a positive light, black characters are very briefly presented. Only white people can speak for and about them, indicating that the wind of change the film was envisaging was still blowing in one direction." 

==References==
 

== External links ==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 