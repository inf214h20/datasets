Quasimodo d'El Paris
{{Infobox Film
| name           = Quasimodo dEl Paris
| image          = 
| image_size     = 
| caption        = 
| director       = Patrick Timsit
| producer       = 
| writer         = Jean-François Halin Raffy Shart Patrick Timsit
| narrator       =  Richard Berry Mélanie Thierry
| music          = Laurent Petitgirard
| cinematography = 
| editing        = 
| distributor    = 
| released       = 1999
| runtime        = 100 mins
| country        = France
| language       = French
| budget         = € 10 650 000
| gross          = € 14 000 565
| preceded_by    = 
| followed_by    = 
}} 1999 French comedic Adaptation adaptation of the novel The Hunchback of Notre Dame (Notre-Dame de Paris) by Victor Hugo.

==Plot==
The location is the town of El Paris. When ten-year-old boy Quasimodo shows signs of deformity, his well-to-do parents place him in the charge of the town’s mysterious evangelist, Frollo. In exchange, they adopt a Cuban girl, Esméralda, from a lower social class. Ten years later, El Paris is menaced by a serial killer, and Quasimodo is the prime suspect. 

==Cast==
* Patrick Timsit as Quasimodo Richard Berry Serge Frollo
* Mélanie Thierry as Agnès / Esméralda (The Hunchback of Notre Dame)|Esméralda Phoebus
* Didier Flamand as The Governor
* Patrick Braoudé as Pierre Gringoire|Pierre-Grégoire
* Axelle Abbadie as The Governors wife Trouillefou
* Albert Dray as Pablo
* Doud as Diego
* Lolo Ferrari as la fée
* Jean-François Halin as Le conducteur alléché
* Raffy Shart as Lhomme au chapeau
* François Levantal as Le psychopathe
* Cathy Guetta as a prostitute

==References==
 

==External links==
 
*  
* 

 

 
 
 
 

 