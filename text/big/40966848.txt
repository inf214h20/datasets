The Black Wolf (film)
{{infobox film
| title          = The Black Wolf
| image          =
| imagesize      =
| caption        =
| director       = Frank Reicher
| producer       = Jesse L. Lasky Margaret Turnbull (scenario)
| starring       = Nell Shipman Lou Tellegen
| cinematography = Dent Gilbert
| editing        =
| distributor    = Paramount Pictures
| released       = February 12, 1917
| runtime        = 5 reels
| country        = USA Silent English intertitles
}}
The Black Wolf is a 1917 silent film drama produced by Jesse L. Lasky, directed by Frank Reicher, starring Nell Shipman and Lou Tellegen, and distributed through Paramount Pictures. 

==Cast==
*Lou Tellegen - The Black Wolf
*Nell Shipman - Dona Isabel
*H. J. Herbert - Don Phillip
*James Neill - Count Ramirez
*Paul Weigel - Old Luis

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 


 