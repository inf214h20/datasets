Sankalp
 
{{Infobox film
| name = Sankalp
| image =
| image_size     = 
| caption = Cover Original Motion Pictures
| director = Ramesh Saigal
| producer = Ramesh Saigal
| writer =
| narrator = 
| starring = Arjun Bakshi Bipin Gupta Farida Jalal
| music = Khayyam & Mohd. Rafi Lyrics by Kaifi Azmi
| cinematography = 
| editing = 
| distributor = 
| released = 1975
| runtime        = 
| country =  
| language = Hindi
| budget = 
| gross = 
| preceded_by = 
| followed_by = 
| website = 
}}

Sankalp is a 1974 Bollywood film
directed by Ramesh Saigal. The song Tu Hi Sagar Tu Hi Kinara rendered by Sulakshana Pandit earned her the only Filmfare award. It was composed by
Khayyam & Mohd. Rafi.

== Plot ==

Rakesh lives with his widowed
businessman dad Harkishanlal; elder
brother Shyam, his wife, Kamla and
their son. He attends college, does
extremely well in his exams, and stands
first with only one other candidate, who
comes from a poor family. Rakesh also
has a sweetheart in fellow-collegian,
Geeta Sehgal, and both hope to get
married soon. Rakesh introduces Geeta
to Kamla, and gets instant approval.
But there is something troubling
Rakesh, he is appalled at the
inequalities and injustices in this world,
the power the rich have over the poor;
why God remains a silent spectator in
the light of atrocities, committed on his
very own creations. Rakesh sets out to
seek answers within his family, and is
shunned. His dad and brother want him
to lend a hand in their business, but
Rakesh does not want any part in it as
part of it is being run by black money,
surrounded by hypocrites. He stops
seeing Geeta, and when Kamla finds
this out, she follows him one day and
finds him with a group of half-naked,
drug-induced hippies. When confronted,
Rakeshs only explanation is that he
will not marry Geeta as he does not
want to get tied down and wants his
freedom – what exactly is the freedom
that Rakesh seeks – watch as he runs
away from home to seek lifes
meanings along with newfound hippie
friends.

== Cast ==

* Arjun Bakshi as Shyam
* Bipin Gupta as Harkishanlal (Shyams dad)
* Farida Jalal as Geeta Sehgal
* Jankidas as Sadhu
* Anjali Kadam as Kamla (Shyams wife)
* Leela Mishra as Rani of Bacharawa (Kamlas mom)
* Sulakshana Pandit as Poojaran
* Yunus Parvez as Kantilal
* Sukhdev as Rakesh

== Soundtrack ==

{{infobox album
|  
| Name = Sankalp
| Type = Album
| Artist = Khayyam & Mohd. Rafi
| Cover =
| Released = 1 December 1976
| Recorded = Feature Film Soundtrack
| Length =
| Label = Saregama
| Producer = Ramesh Saigal
}}

Music composed by Khayyam & Mohd. Rafi and lyrics
by Kaifi Azmi.

{|
|+ Original Motion Pictures
|-
! Track
! Song
! Singer (s)
! Music
|-
| 1
| Tu Hi Sagar Tu Hi Kinara, Part 1
| Sulakshana Pandit & Vinod Sharma
|
|-
| 2
| Dhan Tere Kaam Na Aayega Mukesh
|
|-
| 3
| Sab Thath
| Mukesh
| Khayyam
|-
| 4
| Zindagi Kya Hai
| Mahendra Kapoor
|
|-
| 5
| Tu Hi Sagar Tu Hi Kinara, Part 2
| Sulakshana Pandit
| Khayyam
|-
| 6
| Ghao Dil Ke
| Mahendra Kapoor
| Khayyam
|-
| 7
| Bhitar Bhitar Khaye Chalo
| Mukesh & Mahendra Kapoor
|
|}

== Awards & Nominations ==

* Filmfare Award for Best Female Playback Singer – Sulakshana Pandit for the song "Tu Hi Sagar Tu Hi Kinara"

 
 
 


 