Forgiveness (2004 film)
 
 
{{Infobox film
| name           = Forgiveness
| image          = Forgiveness movie poster.jpg
| caption        = The movie poster.
| director       = Ian Gabriel
| producer       = Cindy Gabriel
| writer         = Greg Latter
| narrator       = 
| starring       = Arnold Vosloo
| music          = Philip Miller
| cinematography = Giulio Biccari
| editing        = Ronelle Loots
| distributor    = California Newsreel
| released       =  
| runtime        = 92 min.
| country        = South Africa Afrikaans
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
Forgiveness is a 2004 South African drama film dealing with the effects of the apartheid system and the difficulty of reconciliation. It was directed by Ian Gabriel and stars Arnold Vosloo, Zane Meas, Quanita Adams and Denise Newman.

==Plot== Truth and ANC activist. Haunted by his brutal past, Coetzee travels to a West Coast fishing village to find the mans family and eventually ask their forgiveness.

==Cast==
* Arnold Vosloo as Tertius Coetzee
* Quanita Adams as Sannie Grootboom
* Christo Davids as Ernest Grootboom
* Zane Meas as Hendrik Grootboom

==Reception==
Forgiveness has won awards at both the Locarno International Film Festival and the Cape Town International Film Festival.

==External links==
* 
* 
*  at allmovie
*  at Rotten Tomatoes

 
 
 
 
 
 

 