Graduate (film)
{{Infobox film
| name = Graduate
| image = 
| caption = 
| director = Prasad Rayala
| producer = Shivaji Attaluri
| writer = Prasad Rayala
| starring = Akshay Rithika Sood Tashu Kaushik
| music = Sandeep
| cinematography = Murali
| editing = 
| studio = 
| distributor = Script Technologies Pvt Ltd.
| released =  
| runtime = 
| country = India
| language = Telugu
| budget =
| gross =
}}
Graduate is a 2011 Indian Telugu language romance film written and directed by Prasad Rayala. It stars debutants Akshay and Rithika Sood in the lead roles alongside Tashu Kaushik. Murali has handled the camera and Sandeep has composed the music. This film has been released in the first week of January 2011. 

==Plot==
This film is completely youth centric with romance as the backdrop.

==Cast==
* Akshay
* Tashu Kaushik
* Rithika Sood
* Manoj Chandra
* Ranjeet Somi

==Soundtrack==
The music of the film is composed by Sandeep. 
{{Track listing
| lyrics_credits = yes
| extra_column = Performer(s)
| title1 = Maruvaleni Nesthama || extra1 = Bunty, Siddhartha                   || lyrics1 = Desi Raju
| title2 = Gadichina Kashanamu || extra2 = Deepu, Revanth, Shravana Bhargavi   || lyrics2 = Ravi Eruvinti
| title3 = Ninnala Nenu Lenu   || extra3 = Anuj Gurwara, Shravana Bhargavi || lyrics3 = Ravi Eruvinti
| title4 = 1234                || extra4 = Geetha Madhuri, Siddhartha      || lyrics4 = Amjad
| title5 = Priyathama          || extra5 = Pranavi, Siddhartha                 || lyrics5 = Ravi Eruvinti
}}

==References==
 

 
 
 


 