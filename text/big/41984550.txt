The Rose Bowl Story
{{Infobox film
| name =  The Rose Bowl Story
| image =
| image_size =
| caption =
| director = William Beaudine
| producer = Richard V. Heermance    Walter Mirisch
| writer = Charles R. Marion 
| narrator =
| starring = Estelita Rodriguez   Warren Douglas   Mimi Aguglia     Leon Belasco  
| music = Marlin Skiles  
| cinematography = Harry Neumann 
| editing = Walter Hannemann  
| studio = Monogram Pictures
| distributor = Monogram Pictures
| released = August 24, 1952
| runtime = 73 minutes
| country = United States English 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}}
The Rose Bowl Story is a 1952 American romance film directed by William Beaudine and starring Marshall Thompson, Vera Miles and Richard Rober.  The film was made in Cinecolor. It follows the relationship between a college football player and his girlfriend.

==Cast==
* Marshall Thompson as Steve Davis 
* Vera Miles as Denny Burke 
* Richard Rober as Coach James Hadley 
* Natalie Wood as Sally Burke 
* Keith Larsen as Bronc Buttram 
* Tom Harmon as Himself 
* Ann Doran as Mrs. Addie Burke  James Dobson as Allie Bassler 
* Jim Backus as Michael Iron Mike Burke 
* Clarence Kolb as Gramps Burke 
* Barbara Woodell as Mrs. Mary Hadley 
* Bill Welsh as Himself

==References==
 

==Bibliography==
* Marshall, Wendy L. William Beaudine: From Silents to Television. Scarecrow Press, 2005.
* Umphlett, Wiley Lee. The Movies Go to College: Hollywood and the World of the College-life Film. Fairleigh Dickinson Univ Press, 1984.

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 

 