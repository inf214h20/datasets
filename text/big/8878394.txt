Golden Age (2006 film)
 
Golden Age is an adult animated documentary film which debuted as a web-series on Comedy Centrals broadband channel Motherload in 2006. Ten segments trace the sordid careers of oddball cartoon characters from throughout the history of animation. Notable characters include Marching Gumdrop, Lancaster Loon, and Kongobot. The film is produced by Augenblick Studios and directed by Aaron Augenblick. Golden Age was an official selection of the 2007 Sundance Film Festival. 

== See also ==
*The Golden Age of American animation

== References ==
{{reflist|refs=
 {{cite web
 |url=http://history.sundance.org/films/3826
 |title=Golden Age   Archives   Sundance Institute
 |publisher=Sundance Institute
 |location=Utah, USA
 |accessdate=29 August 2012
}} 
}}

== External Links ==
* 
*  
*  , Ryan Ball, Animation (magazine)|Animation magazine, May 8, 2006 
*  

 
 
 
 
 
 
 
 


 