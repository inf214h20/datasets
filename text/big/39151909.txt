Engal Swamy Ayyappan
{{Infobox film
| name           = Engal Swamy Ayyappan
| image          = Engal Swamy Ayyappan DVD cover.jpg
| image_size     =
| caption        = DVD cover
| director       = Dasarathan
| producer       = Ramani
| writer         = Dasarathan
| starring       =  
| music          = Dasarathan
| cinematography = Saari - Saga
| editing        = R. Devarajan
| distributor    =
| studio         = Deepa Hari Films
| released       =  
| runtime        = 140 minutes
| country        = India
| language       = Tamil
}}
 1990 Tamil Tamil devotional film directed by Dasarathan. The film, produced by Ramani, had musical score by Dasarathan and was released on 28 December 1990.  

==Plot==

An Ayyappans devotee (Dasarathan) tells five different stories of Ayyappans miracles.

The first story is about Rajaswamy (Hari Raj). Rajaswamy, an Ayyappans devotee, is married to Usha (Sindhu) and promises to make a pilgrimage if he gets a job. He finds a job as a car driver. While hes poor and an ardent devotee, his chief (Malaysia Vasudevan) is boastful and disrespectful. Before the pilgrimage, Rajaswamy must arrange a pooja for Ayyappan. His wife sells her jewels for organize the pooja. His chief arranges the same day the pooja with boasting. The lord Ayyappan, as a child, comes in Rajaswamys chief house and the chief insults the lord. Ayyappan comes to Rajaswamys house, Rajaswamy welcomes with joy and Ayyappan helps him in his pooja. Ayyappan forgives the chief.

The second story is about Swamy (Anand Babu). During his pilgrimage, Someone steals his things. Swamy begins to pray lord Ayyappan. An elephant retrieves Swamys things.

The third story is about Bhaskarswamy (Dilip (Tamil actor)|Dilip). He is married to Lakshmi (Anju). He puts the Ayyapans chain and he must eat vegetarian foods. His arrogant neighbour Sarasu doesnt care of it, cooks fishes and she feels superior. Lord Ayyappan teaches her a lesson and Bhaskarswamy forgives her.

The fourth story is about Prasanthswamy. He is married to Gowri (Kokila) and has a daughter Saumya. He was the witness of a murder. Some rowdies want to take revenge by kidnapping his daughter. In his dream, Lord Ayyappan encourages him to take with him his daughter for the pilgrimage. During the pilgrimage, Saumya gets lost and Salim Bhai (Nagesh), an old Muslim man, returns her to his house. The rowdies hurt Saumya and Salim Bhais grandson. To save the two children, a Christian man gives his blood.

The fifth story is about Vasuswamy (Parthiban). When he returns home after the pilgrimage, his father Sivalingam gives off him and his mother because his father has now a concubine. He finds a job in a butcher shop. The innocent Vasu is later arrested for killing his father and is sentenced to the capital punishment. Before his hanging, the real culprit is caught and Vasu is saved.

==Cast==

*Dasarathan
*Parthiban as Vasuswamy
*Anand Babu as Swamy Dilip as Bhaskarswamy Karthik in a guest appearance
*Hari Raj as Rajaswamy
*Malaysia Vasudevan
*Nagesh as Salim Bhai
*Sindhu as Usha
*Suryakanth
*Anju as Lakshmi
*Madhuri as Veni
*Kokila as Gowri
*Sivaraman
*Kullamani
*Chiranjeevi (archive footage)

==Soundtrack==

{{Infobox Album |  
| Name        = Engal Swamy Ayyappan
| Type        = soundtrack
| Artist      = Dasarathan
| Cover       = 
| Released    = 1990
| Recorded    = 1990 Feature film soundtrack |
| Length      = 20:54
| Label       = 
| Producer    = Dasarathan
| Reviews     = 
}}

The film score and the soundtrack were composed by film composer Dasarathan. The soundtrack, released in 1990, features 5 tracks with lyrics written by Dasarathan.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Track !! Song !! Singer(s) !! Duration
|-  1 || Asaiyoda Pooja || K. J. Yesudas || 5:13
|- 2 || Emmathamum Sammathamum || K. J. Yesudas || 4:39
|- 3 || Sangadam Pokkida || S. P. Balasubrahmanyam || 3:51
|- 4 || Mano || 2:51
|- 5 || Vanga Vanga Swamigalae || Paranthaman || 4:20
|}

==References==
 

 
 
 
 
 