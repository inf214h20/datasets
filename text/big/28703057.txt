The Corbett-Fitzsimmons Fight
{{Infobox film
| name           = The Corbett-Fitzsimmons Fight
| image          = Fitzsimmons Corbett 1897.jpg
| image size     = 275px
| alt            = 
| caption        = still photograph of the fight Fitzsimmons on left; Corbett on right
| director       = Enoch J. Rector
| producer       = William Aloysius Brady
| writer         = 
| screenplay     = 
| story          = 
| narrator       = 
| starring       = James J. Corbett Bob Fitzsimmons
| music          = 
| cinematography = Enoch J. Rector
| editing        = 
| studio         = 
| distributor    = Veriscope
| released       =  
| runtime        = c. 90-100 minutes
| country        = USA
| language       = 
| budget         = 
| gross          = $100,000-$750,000
}}
  longest film that had ever been released to date; as such, it was the worlds first feature film.  The technology that allowed this is known as the Latham loop, and Rector was a rival for claiming the invention of the device.  He used three such equipped cameras placed adjacently and filming on 63mm nitrate film.  Only fragments of the film survive today.  The known fragments were transferred in the 1980s from a print owned by Jean A. LeRoy of New York City, the transfer being done on a specially built optical printer to convert the film to 35mm film.

The film was also the first ever to be shot in   that Luke McKernan declared, "it was boxing that created the cinema." 

In 2012, the film was added to the National Film Registry at the Library of Congress as a "culturally, historically, or aesthetically significant film". 

==Synopsis==
The film no longer exists in its entirety; however, it is known from contemporary sources that the film included all fourteen rounds of the event, each round lasting three minutes.  This was not unusual for a boxing film, although each round would previously have been presented as a separate attraction.  What made this film exceptional is a five-minute introduction that showed former champion John L. Sullivan (whom Corbett defeated in 1892) and his manager, Billy Madden, introducing the event, the introduction of referee George Siler, and both boxers entering the ring in their robes. 

The film also caught the one-minute rest between each round and, when the film was reissued in Boston and many of its subsequent reissues, including in Dublin, included a ten-minute epilogue of the empty ring at the end of the fight, into which members of the audience eventually stormed.  Even with these approximate timings, the film ran a minimum of 71 minutes, and sources generally report that it exceeded 90 or 100 minutes.  The film climaxes with Fitzsimmons hitting Corbett in the solar plexus for a knockout, Corbett crawling outside the space of the camera so that he is not visible above the waist. 

==Production== Edison Kinetoscope as a separate peep show for a separate fee.   Some time after leaving the company, Rector arranged for the film with boxing promoter Dan Stuart (Condon, 137).  Stuart offered $10,000 to the winner of the bout in an agreement signed by both boxers on 4 January 1897.  Corbett, along with his fans, was eager to win back the title that he had lost to Fitzsimmons in Mexico.   Producer William Aloysius Brady got an agreement from Rector that 25% of the proceeds of the film would go to him and Corbett, and Fitzsimmons and his manager, Martin Julian, would receive $13,000.  Fitzsimmons was outraged upon learning of the deal, and the terms were renegotiated. Under the new terms, each boxer and his manager would take 25%  each, with Rector, Stuart, and Samuel J. Tilden Jr (who had left Kinetoscope with Rector in a battle over who invented the Latham loop) dividing the remaining 50%. 

The film was shot in widescreen format on 2 3/16 gauge film stock.   Rector brought 48,000 feet of film stock, the largest amount that had ever been brought on location, and exposed 11,000 feet of it. 
The night before the match, Stuart cut the ring down from 24 feet to 22 feet for the sake of the camera, but the referee noticed this and Stuart was forced to change it back. 

Wyatt Earp was a reporter for The New York World at the time, which published his commentaries on the fight on March 14 (p.&nbsp;15) and March 18.   He disagreed with referee George Silers decision when Fitzsimmons allegedly hit Corbett in the jaw, which should have resulted in a foul, coming after a knockout blow to Corbetts solar plexus.  The World heavily promoted the film, and the day after the films release, printed a statement from Fitzsimmons, "I dont believe there is a single picture in it that will substantiate those   published in The World." 

==Exhibition== New York Academy of Music and played into June, where it was presented with live running commentary.   In total, eleven companies toured with the film. (Musser, 199)

Local debuts:
*May 31, 1897 (Boston)
*June 6, 1897 (Chicago)
*June 7, 1897 (Buffalo, New York|Buffalo)
*June 26, 1897 (Philadelphia)
*July 3, 1897 (Pittsburgh)
*July 13, 1897 (San Francisco)
*July 27, 1897 (Portland, Oregon)
*September 27, 1897 (London) 
*April 1898 (Dublin) 

When the film was shown in Coney Island, it was advertised under the title Corbetts Last Fight. 

==Reception==

Brady estimated the films net profit at $750,000.  Film scholar Charles Musser claims that the film made a more modest $100,000. 

The film is also notable because that, at the time, women were essentially prohibited from viewing boxing matches, which were seen as a "Stag (disambiguation)|stag" activity, but they were not prohibited from viewing this film.  Much attention was given to the fact that Rose Julian Fitzsimmons, Bobs wife, viewed the live match from a box with other female companions, such as dancers Loïe Eiler and Ida Eiler, while women otherwise did not mix with the crowd.  As much as 60% of the Chicago audience was composed of women.   As Miriam Hansen put it, "it afforded women the forbidden sight of male bodies in seminudity, engaged in intimate and intense physical action."   She argues a connection between the female reception of this film and the large female audience for Rudolph Valentino two decades later, who was typically shown stripped to the waist and beaten in his films.

Dan Streibles article calls this into debate, and suggests that the size of the female audience is predominantly self-generated Boilerplate text|boilerplate.  The film had been strongly opposed by the Womens Christian Temperance Union, which tried to get legislation passed to prevent the films transmission by mail.   Their protests of fight films were second only to suffrage on their national agenda.  Several state and local authorities tried to ban the reproduction of pugilist films, but this did not come to a vote.   An editorial in The New York Times  declared, "It is not very creditable to our civilization perhaps that an achievement of what is now called the veriscope that has attracted and will attract the widest attention should be the representation of a prizefight."  Rector claimed that the film had "every defect known to photography" in the San Francisco Examiner  in attempt to quell the protests against a film falsely deemed unusable. Streible, 24.   Because the audience for prizefighting was "perceived to be a rowdy, less desirable class of patron  Veriscope recruited more genteel audiences.  Ladies were officially invited."   Promotion for the film avoided the term "prizefight" with its connotations of violence, and promoted it as a "sparring contest."   Veriscope was trying to run counter to press that had presented the story as feminine resistance to "stag" entertainment. 
 Gentleman Jack, which contributed to Corbetts reputation as a matinee idol for women, as the play was presented to mixed audiences.  Brady had honed Corbetts image as an educated gentleman in order to improve his appeal to bourgeois audiences.   Streible notes that this reputation as a matinee idol and "ladies man image," in addition to the bare-gluteus trunks, about which he could find no contemporary commentary, may have drawn women audiences to the film. 
 Olympia Theatre, she counted only sixty women in an audience of a thousand, and found the dress circle empty.  She observed that they were mostly "dressed down," and that all were escorted by men and appeared uninterestedly watching a bloodless spectacle.  She proceeded to describe the entire medium of motion pictures as "awful." 

On page 38, Streible reproduces a drawing that accompanied Rixs article depicting two women in attendance of the film.  One appears to be younger and leaning forward, watching the film with interest, while the other, apparently an older woman acting as chaperon, is turned away from the screen and uninterested in the film, even dismayed at the younger womans interest.  He notes that "respectable women" had been allowed to attend theater for only about a generation, and that Broadway did not actively court women or families as audience members until 1865.  The pre-war audience had primarily been men and prostitutes.   By 1897, women were only beginning to see theater as a legitimate social space.  Musser (200) notes that The Boston Herald went so far as to call the film the "proper" thing for ladies to see.  Streible, citing the research of Antonia Lant,  contrasts paintings of women in theater audiences by Mary Cassatt, Claude Monet, and Berthe Morisot with this drawing by making it appear that the fact that women were allowed to look was more important than that the act of looking being allowed to them.  That the younger woman is leaning indicates that what she is looking at is, in fact, what is important to her rather than the simple privilege of looking. 
 strongman photos by Thomas Waugh.  He concludes that prizefighting, as opposed to physical culture, was not associated with aesthetics or male beauty, Corbett excepted.  The aesthetics of the boxing scene were better known for broken jaws and cauliflower ears, such that ones sexual orientation probably had little bearing on ones appreciation of the film, and of a sport surrounded by homophobic press. 

Musser, in his discussion of subsequent feature-length fight films, that subsequent to The Corbett-Fitzsimmons Fight, no boxing film drew comparable audience numbers and that women stopped attending in significant numbers.,  reinforcing Streibles theories of hype and female interest in Corbett the matinee idol.

Denis Condons article, "Irish Audiences Watch Their First U.S. Feature:  The Corbett-Fitzsimmons Fight (1897)",  discusses how class, rather than gender, affected audience response to the film in Dublin.  The significance of the films reception in Ireland derives from the fact that Corbett was Irish by birth and often contrasted to the English-born Fitzsimmons, who himself was the son of an Irish blacksmith, a fact that no newspaper noted at the time.  He notes a surprising absence, in response to the film, of ethnic partisanship, in spite of the St. Patricks Day day of the fight, the Irish-English tension of 1898, and heavy antagonism of the Irish-American Corbett and the English Fitzsimmons, who is elsewhere described as Anglo-Australian.  Audiences put aside political fervors and suspended their knowledge, pretending that they were watching a live performance.   Irish women did not attend, possibly because The Lyric Hall, where the film was shown, often featured live boxing and sexually risqué material, and thus considered an inappropriate place for a respectable woman, while another theatre nearby was regarded as more family-friendly. 

==Legacy==
Quick to compete, Siegmund Lubin created a film the same year known as Reproduction of the Corbett and Fitzsimmons Fight, staged on a rooftop with two freight handlers from the Pennsylvania Railroad.  Each round was shot on only 50 feet of 35 millimeter film stock at a very slow speed.  Veriscope threatened to sue, but there was no law broken.   Audiences did not appreciate the facsimile, even though it was advertised as such.  The Arkansas Vitascope Company showed the film.  The June 1897 issue of Phonoscope  reprinted an article from The Little Rock Gazette that stated that the audience was so angered by Lubins film that it was turned off after the third round for lack of an audience.  The August–September issue of Phonoscope  noted that the manager of the opera house turned over his $253 profits to a state senator who, after time to deliberate, eventually refunded the patrons money.

Ramsaye  notes that The Corbett-Fitzsimmons Fight was the singular film that pushed the social status of film, then uncertain, into the Low culture|low-brow.  This is not consistent with the way that the film was actually marketed.  Prices for seats were set up to $1 for the wealthiest patrons, assuring middle and upper class attendance. 

Rector intended to go into long form dramatic films, but was dismissed as a crank, although he continued to be involved in the technical side of motion picture production. 

==See also==
*List of early wide-gauge films
*List of film formats
*List of 70 mm films

==References==
 

==External links==
* 
* 
* 
* 
* 

 
 
 
 
 
 
 