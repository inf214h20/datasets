Columbus on Trial
 

Columbus on Trial is a film directed by  . 

==Characters==
Columbus on Trial contains a variety of characters, all an important part of the message that the film conveys to its audience. The protagonist of this film is Christopher Columbus, who with the help of his Chicano defense lawyer tries to prove his innocence. The lawyer in charge of prosecuting Christopher Columbus is a Native man who stresses the importance of convicting Columbus, referred to as “The Great Assassin". The judge and defense lawyer are Chicanos loyal to their Spanish roots and believe Columbus was the discoverer of the Americas. The prosecutor, on the other hand, is of Indian origins and wants Christopher Columbus to be convicted. Christopher Columbus, for many,  was the explorer who discovered the Americas. For others, he was a murderer and a trespasser. This video is a parody of a present-day trial in a courtroom. In this make-believe trial we  see various aspects of coloniality of power, hegemony, and colonial modern gender system.

==Coloniality of power==
 Coloniality of power, according to Maria Lugones, introduces basic and universal classification of population in terms of race. Relations of superiority and inferiority, the legacy that the colonization left behind. Anibal Quijano, states that coloniality of power still exists in form of domination left over form political and economic colonialism, allows for continual notions of superiority to be bred under Euro-centric. An example of this can be seen in the film itself, the fact that the prosecuting Indian was wearing a suit shows an example of coloniality of power. Although he was from Indian descent, his style of clothing came from the colonizers. This is just one of the many examples in the film in which you  can see obvious forms of coloniality of power.

==Hegemony==
Antonio Gramsci describes hegemony as a form of rule in which dominant ideas, meaning, and values, permeate all of society without appearing to be imposed. Hegemonic ideologies are seen in the film by Lourdes Portillo when we see the acculturation of the defense lawyer and the Judge. These Chicanos believe that Columbus was the discoverer of the Americas without really doubting Columbus. Columbus discovering the Americas is the dominant idea and therefore do not challenge it.

==Colonial modern gender system==
 This film also presents a shift of what Maria Lugones describes as colonial modern Gender Systems. Lugones presents gender as a colonial introduction that brought upon an imposed strict gender binaries that were firmly based on heterosexuality principals. This film breaks gender binaries in which women are seen only as submissive caregivers. The film has a judge that is a woman. This is important in that a woman was given the job of an authoritarian character, that held the power to decide whether Columbus the discovered of America or invaded it and colonized its Indian population. Another example of this is seen at the end of the film when Columbus was declared free and clear to go, as he was leaving a young Chicana shot him down in cold blood. This also breaks the gender binary in that Lourdes Portillo creates a strong Latina who is powerful and an assassin which broke all gender binaries imposed by the colonial modern gender system.

==Related terminology==
* Eurocentrism
* Decolonization
* Deterritorialization
* Chicano

==Sources==
 *Lugones, M. “Heterosexualism and the Colonial/Modern Gender System.” Hypatia 22,
no. 1 (Winter 2007): 186-209.  
 *Quijano, A. “Coloniality and Modernity/Rationality.” Cultural Studies 21 (2-3)
(March/May 2007): 168-178.  
 
 

==References==
 

==External links==
  in the  

 
 