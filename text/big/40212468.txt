Sahkanaga
{{multiple issues|
 
 
}}
{{Infobox film
| name = Sahkanaga
| image = Sahkanaga.jpg
| director = John Henry Summerour
| writer = John Henry Summerour
| starring = Trevor Neuhoff
| country = United States
| language = English
| released =  
}}
Sahkanaga is a 2011 feature film written and directed by American filmmaker John Henry Summerour. The film uses the true events of the Tri-State Crematory incident as its setting and backdrop, though the central characters are fictional.

==Background and development==
Summerour began working on the film script after learning that over 300 bodies had been dumped in the woods near the Tri-State Crematory instead of being incinerated by the operators. A 15-minute short film was adapted from the full script and shot in 2007. The short production cast local, non-professional actors as a "litmus test" to determine if local talent (many of whom were directly affected by the incident) could be used without resulting in awkward and exaggerated performances. 

==Full-length feature==
Sahkanaga follows the story of Paul (played by Trevor Neuhoff),  the son of the owner of a funeral home in rural Northwest Georgia. Paul stumbles upon a decomposing corpse near the local crematory and uncovers a scandal that shakes the foundations of his small town. As the ones entrusted with the care of the deceased, Pauls family is met with hatred and blame by their neighbors. The tragedy for Paul is complicated by his budding romance with the granddaughter of first corpse that he finds. Questions of sin, secrets, forgiveness, and faith are explored as the close community tries to recover.

Many of the actors from the short film returned to participate in the feature. The entire cast was made up of local, non-professional talent, many of whom had little or no experience.  The film was shot in 21 days on a budget of $100,000. The filming was made possible by substantial community support and volunteers, though some locals disapproved of using the material as the subject of a film. 

==Screenings and awards==
The feature film has enjoyed the following screenings and awards:
*2013 PEACHTREE ROAD UMC SUMMER FILM SERIES
*BROOKLYNS RERUN THEATRE AND ATLANTAS HISTORIC PLAZA THEATRE : (IFP, Filmmaker Magazine, Filmwax and the Atlanta Film Festival)
*2012 SECRET CITY FILM FESTIVAL : (Winner of the Jury Award & Audience Award for Best Narrative Feature)
*2012 BRASILIA INTERNATIONAL FILM FESTIVAL
*2012 RAINIER INDEPENDENT FILM FESTIVAL : (Winner of the Rainier Llama Award for a Feature)
*2012 SOUTHERN CIRCUIT TOUR OF INDEPENDENT FILMMAKERS
*2012 SAN FRANCISCO INDEPENDENT FILM FESTIVAL : (Winner of the Jury Award for Best Narrative Feature)
*2012 EUROPEAN FILM MARKET : (“American Independents in Berlin” presented by IFP & the Sundance Institute)
*2011 CORONA CORK FILM FESTIVAL
*2011 NEW ORLEANS FILM FESTIVAL
*2011 DRIFTLESS FILM FESTIVAL
*2011 TACOMA FILM FESTIVAL : (Recipient of TheFilmSchools Great American Storyteller Prize)
*2011 WOODSTOCK FILM FESTIVAL
*2011 ROME INTERNATIONAL FILM FESTIVAL : (Winner of the Jury Award for Best Narrative Feature)
*2011 SIDEWALK MOVING PICTURE FESTIVAL : (Winner of the Koroni Film Award for Public Health)
*2011 NORTHSIDE FESTIVAL : (Special Presentation by IFP & L Magazine)
*2011 INDEPENDENT FILM FESTIVAL BOSTON
*2011 ATLANTA FILM FESTIVAL : (Audience Award Winner for Best Narrative Feature)

==References==
 

==External links==
* 
* 
* 
* 


 