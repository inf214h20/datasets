Wraiths of Roanoke
{{Infobox television film 
| name           = Wraiths of Roanoke
| image          = Wraithsofroanoke.PNG
| caption        = 
| genre          = Horror Mystery Sci–Fi
| director       = Matt Codd
| producer       = Dana Dubovsky Rafael Jordan Mark L. Lester Steven G. Kaplan
| writer         = Rafael Jordan
| starring       = Adrian Paul Frida Show Rhett Giles Michael Teh George Calil
| music          = John Dickson
| cinematography = Anton Bakarski 
| editing        = Robin Russell
| studio         = American World Pictures Rainstorm Entertainment
| distributor    = Allumination Filmworks American World Pictures 
| network        = Syfy 
| released       =  
| runtime        = 95 minutes
| country        = United States Bulgaria
| language       = English
| budget         = 
}} Sci Fi original movie, directed by Matt Codd and stars Adrian Paul, Frida Show, Rhett Giles, Michael Teh, and George Calil.
 Sci Fi October 13, rated TV–14.

==Plot== Indian escort. When the settlers arrive, they find the colony abandoned by the previous occupants, except for one body. They find a corpse hanging from a rope inside a small building, with the door bolted from the inside. The English governor sent with the settlers dismisses this as an intimidation tactic by the Spanish. The governor is forced to return to England to gather supplies, and names settler Ananias Dare (Adrian Paul) interim governor, backed with the strong arm of George Howe (Rhett Giles). That evening, Ananiass pregnant wife Eleanor Dare (Frida Show) is sleeping, when suddenly, she has a gruesome dream that her nightgown is covered in blood, with her baby taken from her womb. She runs out into the middle of the town square, and finds a ghost with her baby. She wakes up deeply shaken, and warns Ananias that it might be better for them to return to England. He dismisses this, but settlers begin dying in the woods one by one. Eleanor mysteriously gives birth prematurely, but the baby girl, whom they name Virginia, is thankfully born safely. Crops wont grow in the islands soil, and there are no animals in the forest. More settlers begin dying, and it soon becomes apparent that there is a supernatural presence on the island.

Eleanor continues to get disturbing dreams, and eventually the dreams reveal that the island was the location of a brutal execution of an innocent woman and a few other men by Vikings. Long before, a ship of Viking warriors suffered misfortune and blamed it on one of the women and a few of the men traveling on the ship with them. They took them to the island and tortured them to death, and because of this the souls of the evil Viking men and the other men and the single woman are still trapped on the island in a state between life and death. Ananias Dare must work with a local Indian chief and his own townspeople to find a way to send the evil wraiths out of this world and into hell where they belong.

In time, one of the colonists leads an attack on the Indian village, only to be repelled and most of the men killed; for ruining the Englishs chances of gaining the Indians trust, he is put in the stocks. Later that night, the wraiths suck out his soul and begin an attack on the colonists; they are fought back, but many colonists are killed. It is soon revealed that these wraiths feed on the souls of the living and are trying to kill Virginia because they require an innocent soul to pass on to the afterlife; it is also learned that their weaknesses are fire and water (water being a symbol of life). They are soon forced to devise a plan to defeat the wraiths; they set up a raft with a pile of wood and hay on it and wait for the moon to come. The wraiths arrive at night and due to a colonist panicking, they begin attacking them. In time, only Eleanor, Ananias, Virginia, and Howe are the only survivors. Unfortunately, Eleanor attacks one of the wraiths in an attempt to save Ananias but is quickly killed. After a few minutes, Howe and Ananias are mortally wounded, with the latter luring the wraiths onto the raft with baby Virginia. As they close in on Ananias and Virginia, Howe launches a fire arrow onto the raft, setting it aflame; as the wraiths cannot cross water, they are forced to suffer. Ananias looks at Virginia one last time before setting her adrift and dying. Later on, Virginia is found by the Indians and is to be raised as one of their one; Manteo orders the tribe to bury the colonists and set their colony on fire.

==Cast==
* Adrian Paul as Ananias Dare
* Frida Show as Eleanor Dare
* Rhett Giles as George Howe
* Michael Teh as Manteo
* Mari Mascaro as Elizabeth Viccars
* Alex McArthur as John White
* George Calil as Thomas Stevens
* Doug Dearth as Gregory Hemphill
* Atanas Srebrev as Samuel Fillon
* Hristo Mitzkov as Ambrose Viccars
* Velislav Pavlov as Gaius Callis
* Rafael Jordan as Christopher Harvie
* Jonas Talkington as William Stark
* Suzette Kolaga as Emme Merrimoth
* Terence H. Winkless as Father Jacob
* Alex Revan as Feldon
* Ivo Simeonov as Lee Bamber
* George Zlatarev as Simon Fernandes

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 
 
 