Blood of Dracula
{{Infobox film|
  name=Blood of Dracula|
  image= |
  director=Herbert L. Strock|
  caption=| Ralph Thornton |
  starring=Sandra Harrison Louise Lewis Gail Ganley Jerry Blaine Heather Ames|
  producer=Herman Cohen executive James H. Nicholson Samuel Z. Arkoff|
studio = Carmel Productions|
  distributor=American International Pictures|
  released= |
  runtime=68 mins.|
  language=English|
  }}
Blood of Dracula (UK title: Blood Is My Heritage) is a horror film starring Sandra Harrison, Louise Lewis and Gail Ganley released by American International Pictures (AIP) in November 1957. It is one of two follow-up films to AIPs box-office hit I Was a Teenage Werewolf released less than five months earlier, being released as a double bill with I Was a Teenage Frankenstein. Gary A. Smith, The American International Pictures Video Guide, McFarland 2009 p 29  The film is black and white. 

==Plot== corrective institution private preparatory school with a very good reputation.

Nancy is immediately harassed by her dormmates that night, and at breakfast the next morning, after Thorndyke officially introduces Nancy to the girls, Myra (Gail Ganley), their leader, tells Nancy that it was good thing that she did not mention anything about the way they acted the previous night.

Myra also tells Nancy about their secret club, "The Birds of Paradise," and introduces her to Eddie (Don Devlin), a young groundsman whom the “Birds” take turns dating. Myra is the assistant for Miss Branding (Louise Lewis), the school’s chemistry teacher, who is writing a thesis about her belief that there is a “terrible power,” “strong enough to destroy the world&nbsp;– buried within each of us.” If she can prove this is the case, she hopes that the scientific community will abandon their experiments with nuclear power and other weapons mass destruction. When Branding tells Myra that she is looking for a special girl on whom to experiment, Myra suggests Nancy.
 hypnotize her Carpathian Mountain region and is capable of healing, as well as destroying&nbsp;– and has the ability to release frightening powers. As Nancy gazes at the amulet, Branding hypnotizes her and instructs her to always obey her.

Later, Eddie and two local boys, Tab (Jerry Blaine) and Joe (Jimmy Hayes), climb up into the girls room as they are having Nancy’s initiation party. In a nearby building, Branding hears the noise and, despite the distance, is able to re-hypnotize Nancy, turning her into a vampire. The party is broken up by the disciplinarian Miss Rivers (Edna Holland), who then sends Nola to the basement to fetch supplies. While in the basement, Nola is attacked by something subhuman and killed.

The next morning, as police detective sergeant Stewart investigates the killing, Nancy is unable to wake up until Branding orders her and, when she relates a nightmare she had, Branding orders her to forget it.

At police headquarters, the coroner informs Lt. Dunlap (Malcolm Atterbury) that he found two puncture wounds in Nolas jugular vein and that the body was drained of blood. A young assistant to the coroner, Mike (Paul Maxwell) who shared a room in med school with “an exchange student from a small town in the Carpathian Mountains,” remembers his friends stories about vampires. Dunlap is unimpressed in the theory.

The girls later organize a Halloween scavenger hunt in the local cemetery, and Nancy is again transformed into a vampire and kills another girl, as well as Tab. The police subject all the girls in the scavenger hunt to a Lie detector test|lie-detector test, however, Branding is able to alter Nancys responses to the questions by remote hypnosis.

Back at the school, Nancy, confused and frightened by her transformations, begs for Brandings help, but Branding assures her that the experiment will soon be over and that she will be proud of her part in saving mankind from self-destruction. The state threatens to close the school over the unsolved murders, and consequently, Thorndyke asks Branding to take over some of her duties while she attempts to calm concerned parents. Nancy’s boyfriend from back home, Glenn, suddenly arrives at the school, alarmed at the news stories of the slaughters but Nancy acts cold toward him&nbsp;– afraid that she might turn and kill him.

Nancy then goes to Brandings laboratory and begs Branding to release her from the experiment and from her power, but the obsessed Branding refuses and hypnotizes her again. Nancy once more becomes a vampire, nevertheless this time she attacks Branding, strangling her to death with the amulet’s necklace. As they struggle, Branding pushes Nancy away, impaling her on a broken piece of furniture, as Glenn, Thorndyke and Myra break into the laboratory. After they discover that Brandings written thesis has been destroyed by acid, Thorndyke declares that “those who twist and pervert knowledge for evil only work out their own destruction.”

==Production notes==
The film bears a striking resemblance to AIP’s earlier summer box office hit, I Was a Teenage Werewolf. Arkoff, pp. 61–75  More or less a remake, and with the hero and villain roles now both played by females, Blood of Dracula could have easily been titled I Was a Teenage Vampire. With a story and screenplay credit by I Was a Teenage Werewolf writer Ralph Thornton (a pseudonym for AIP producer   and Paul Dunlap accompanied by a choreographed "ad-lib" dance number; hypnosis as scientific medical treatment; drug injections; specific references to Carpathian Mountains|Carpathia; hairy transformation scenes; and even some of the same dialogue. In addition, two prominent actors from I Was a Teenage Werewolf are also featured in Blood of Dracula, Malcolm Atterbury and Louise Lewis; with Lewiss villain, Miss Branding a practically perfect female version of Whit Bissels Dr. Brandon. However, few critics have drawn a connection between the two films, and while most reference works consider I Was a Teenage Frankenstein and How to Make a Monster as direct follow-ups to I Was a Teenage Werewolf, not even cinema guru Leonard Maltin speaks of Blood of Dracula as even being related to the trilogy. 

The film begins with a proto-feminist theme&nbsp;– the female scientist is driven by a need to assert herself in a male-dominated world and prove men of science and politics wrong. The film ends on a conservative anti-women message, however, suggesting that women who try to rise above their station ultimately become unbalanced.

==Cast==
*Sandra Harrison as Nancy Perkins
*Louise Lewis as Miss Branding, Chemistry
*Gail Ganley as Myra
*Jerry Blaine as Tab
*Heather Ames as Nola
*Mary Adams as 	Mrs. Thorndyke
*Edna Holland as Miss Rivers, Art
*Thomas B. Henry as Mr. Paul Perkins
*Jeanne Dean as Mrs. Doris Perkins
*Don Devlin as Eddie
*Malcolm Atterbury as Lt. Dunlap
*Richard Devon as Det. Sgt. Stewart
*Craig Duncan as Police Officer
*Carlyle Mitchell as Stanley Mayther
*Voltaire Perkins as Dr. Lawson
*Paul Maxwell as Mike, the young doctor
*Shirley De Lancey as Terry
*Michael Hall as Glenn
*Barbara Wilson as Ann
*Jimmy Hayes as Joe
*Lynn Alden as Linda

==Reception==
Upon its theatrical release, the United States Conference of Catholic Bishops described the film as a “Low-budget chiller ... in which a new student at a girls prep school turns into a murderous vampire after falling under the hypnotic spell of the schools feminist science teacher ... Stylized violence, hokey menace and sexual innuendo,” and gave the film an “Adults Only” rating.
 

For its DVD release, DVD Verdict wrote: "Blood of Dracula has nothing to do with Dracula, but rather taints the vampire legend into the fate of a cranky teenage girl. The film basically takes the same route as I Was a Teenage Werewolf, but never lives up to that effort, especially with Harrison’s monster turns kept to a bare minimum. But her wild bat make-up is memorable, looking closer to Nosferatu with big hair than anything else, and an impromptu musical number, Puppy Love is a hoot." DVD Drive-In.com Reviews: SAMUEL Z. ARKOFF COLLECTION: HOW TO MAKE A MONSTER (1958)/BLOOD OF DRACULA (1957)
EARTH VS. THE SPIDER (1958)/WAR OF THE COLOSSAL BEAST (1958)   

==References==
;Notes
 

;Bibliography
*  
*  

==External links==
*  
*  

 

 
 
 
 
 
 
 