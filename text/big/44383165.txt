Magda Expelled
{{Infobox film
| name =   Magda Expelled
| image =
| image_size =
| caption =
| director = Ladislao Vajda
| producer =  Ferenc Pless 
| writer =  Miklos Kadar (play)   László Kadar (play)   Károly Nóti   Imre Éri-Halász 
| narrator = Antal Páger   György Nagy 
| music = Andor Komáromy  
| cinematography = István Eiben 
| editing = Viktor Bánky 
| studio =    Harmónia Film 
| distributor = 
| released =   5 March 1938 
| runtime = 89 minutes
| country = Hungary Hungarian
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}} Antal Páger.  The film was based on a play. In 1940 it was remade in Italy as  Maddalena, Zero for Conduct with some changes (such as the lawyer being from Vienna rather than London).

==Synopsis==
A schoolgirl accidentally sends a love letter written by one of her female teachers to a handsome lawyer in London, leading to a series of misunderstandings which are eventually resolved.

==Cast==
*   Ida Turay as Lévay Magda 
* Klári Tolnay as Makray Erzsébet,a kereskedelmi levelezés tanárnõje   Antal Páger as Alfred Harvey  
* György Nagy as Horvay István,Harvey unokaöccse  
* Piri Peéry as Igatgatónõ 
* Sándor Góth as Kémiatanár  
* Gyula Gózon as Father  
* Márta Fónay as Student  
* Valéria Hidvéghy as Student 
* Gerö Mály as Pedellus 
* Erzsi Pártos as Student  
*  Kató Fényes 
* Judith Laszlo
* Veronika Radó
* Magda Révfalvy
* Lili Szász 
* István Dózsa 
* István Falussy
* Aranka Hahnel 
* Gusztáv Harasztos  Gyula Justh 
* Terus Kováts 
* Márta Lendvay 
* Sándor Pethes 
* Mária Román 
* Zsuzsa Simon 
* Irén Sitkey 
* Éva Somogyi 
* Mária Szemlér

== References ==
 

== Bibliography ==
* Reich, Jacqueline & Garofalo, Piero. Re-viewing Fascism: Italian Cinema, 1922-1943. Indiana University Press, 2002.

== External links ==
* 

 

 
 
 
 
 
 
 
 
 

 