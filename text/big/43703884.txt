My Piece of the Pie
 

{{Infobox film
| name     = My Piece of the Pie
| image    = 
| director = Cédric Klapisch
| producer = 
| Script = 
| starring = Karin Viard Gilles Lellouche
| cinematography = 
| music = 
| country = France French
| runtime = 109 minutes
| released =  
| budget = $7,800,000
| gross = $15,894,152	
}}

My Piece of the Pie ( ) is a 2011 French comedy film directed by Cédric Klapisch.

==Plot== global financial income inequality, the film opens in the French seaside town of Dunkirk. France (Karin Viard), a middle-aged divorcée, has just been laid off after 20 years at the same shipping company. She takes an overdose of pills but is rushed to hospital and makes a complete recovery. Desperate for employment to support her three children, she moves to Paris where she connects with Ahmed (Zinedine Soualem), the father of a former co-worker. France pretends to be a low-skilled immigrant to find menial work, but is then offered the chance to be the housekeeper for Stéphane Delarue (Gilles Lellouche), a wealthy French banker who has returned to Paris after 10 years in London.

The relationship between the pair is initially tense, but Stéphane eventually warms to France, especially after she proves herself an adept hand at taking care of his son for a month, an extra duty for which she is paid €200 a day. The relationship between France and Stéphane turns amorous on a trip to London to meet with partners of Stéphane’s firm. But the following morning France reveals how she came to his employ: she had lost her job when her shipping company moved many of its jobs to China. Stéphane, recognizing the company’s name, callously admits he was one of the bankers whose shorting of the company’s stock hastened the job losses. Leaving the hotel to play in the park with Stéphane’s son, France then overhears him boasting and laughing about his conquest the previous night. France abducts the boy and returns to Dunkirk, holing up in an auditorium thats hosting a dance performance. Stéphane arrives to see France arrested but the crowd, many of whom are former employees of the shipping company, are made aware of Stéphane’s identity and pursue him onto the beach. The mob gives chase as others form a blockade to prevent the police wagon from taking France away.

==Cast==
* Karin Viard - France
* Gilles Lellouche - Steve Delarue
* Audrey Lamy - Josy
* Jean-Pierre Martins - JP
* Zinedine Soualem- Ahmed
* Raphaële Godin - Mélody
* Marine Vacth - Tessa
* Tim Pigott-Smith - Mr. Brown Philippe Lefebvre - Le PDG dans la fête

==References==


 
	
==External links==
*  

 
 

 
 