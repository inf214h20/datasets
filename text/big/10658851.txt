Anbulla Rajinikanth
{{Infobox film|
| name = Anbulla Rajinikanth
| image = Meena
| director = K. Natraj
| writer = Thooyavan Meena Ambika Ambika Jaishankar Bhagyaraj Raadhika Sarathkumar
| producer = M. S. Akbar Durga Tamizhmani
| music = Ilaiyaraaja
| cinematography = Babu
| editing = R. Vittal
| studio = S. T. Combines
| distributor = G. V. C. Films
| released = 2 August 1984
| runtime = 180&nbsp;minutes
| country = India
| language = Tamil
| budget =
}}
 Ambika  Raadhika and Bhagyaraj in guest roles. It has positive reviews and praised the potrayal of child Artist Meenas performance and mainly Rajinikanths excellent acting.

==Plot==
Rosy (Meena Durairaj|Meena), is a wheelchair-using cancer patient, who lives in an orphanage along with other orphans. She is very rude and uncaring towards anyone. She even illtreats her caretaker (Ambika (actress)|Ambika) who is actually her mother.

When Rajinikanth, who acts as himself in the movie, visits the orphanage, Rosy also illtreats him, not knowing that he is Rajinikanth. But after watching a few movies of his, she becomes a big fan of Rajinikanth and the latter also develops a special bond with her, taking her out for his shooting and to many other places. Rosys attitude also undergoes a change due to this relationship as she becomes a kind and considerate girl.

When Rosy dies on Christmas Eve due to the cancer, she stills holds Rajinikanths Shirt, and Rajinikanth starts crying, such was their affection for each other.

==Cast==
* Rajinikanth as himself Meena (Child Artist) as Rosy Ambika as Mother of Rosy
* Rajkumar Sethupathi as Father of Rosy Tinku

===Supporting Roles===
* Jaishankar as himself Bhagyaraj as himself
* Raadhika Sarathkumar as herself Parthiban as K. Bhagyarajs assistant director

==Production== Meena appeared as one of the main characters. 

==Soundtrack==
The music composed by Ilaiyaraaja. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|- Vaali (poet) Vaali || 4.17&nbsp;min
|-
| 2 || Kadavul Ullame || Latha Rajinikanth, Chorus || 4.17&nbsp;min
|-
| 3 || Muthumani Chudare Vaa || K.J. Yesudas || Na. Kamarasan || 4.17&nbsp;min
|-
| 4 || Thaaththa Thaaththa || Malaysia Vasudevan || Kuruvikkarambai Shanmugam || 3.45&nbsp;min
|- Vaali ||  4.23&nbsp;min
|}

==Reception==
On 3 August 1984 The Hindu said, "For one making his debut as director Natraj deserves accolades for the near-to-the-heart treatment of the situations in which the performance of a six-member-group of orphans will make even elder artistes sit up" and concluded, "Babus camera embellishes the frames".  Cinemalead wrote, "Anbulla Rajinikanth made Rajini seal childrens heart forever, that was the impact of the movie. The story of a childs emotional bondage with a film actor and her fighting against cancer reached out to audience very well due to Rajinis reach and Meenas acting. The film also gives us some glimpse of Rajinis lifestyle and his real life character". 

==References==
 

 
 
 
 
 
 


 