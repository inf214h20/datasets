Sharaku (film)
 
{{Infobox film
| name           = Sharaku
| image          = 
| caption        = 
| director       = Masahiro Shinoda
| producer       = Masato Hara Frankie Sakai
| writer         = Midori Katakura Hiroko Minagawa Masatoshi Sakai Masahiro Shinoda
| starring       = Hiroyuki Sanada
| music          = Tōru Takemitsu Tatsuo Suzuki
| editing        = Hirohide Abe
| distributor    = 
| released       =  
| runtime        = 115 minutes
| country        = Japan
| language       = Japanese
| budget         = 
}}

Sharaku ( ) is a 1995 Japanese drama film directed by Masahiro Shinoda. It was entered into the 1995 Cannes Film Festival.   

==Cast==
* Hiroyuki Sanada - Tonbo
* Frankie Sakai - Tsutaya
* Shima Iwashita - Troupe Leader
* Tsurutarō Kataoka - Goro
* Shirô Sano - Utamaro
* Riona Hazuki - Hanasato
* Toshiya Nagasawa - Tetsuzo (the future Hokusai)
* Yasosuke Bando - Matsudaira Sadanobu
* Nakamura Tomijyuro V - Gosei
* Naoko Kato - Ohuji
* Masumi Miyazaki - Gohi
* Choichiro Kawarazaki - Santo Kyoden
* Naomasa Musaka - Manager Yohei

==References==
 

==External links==
* 

 

 
 
 
 
 


 