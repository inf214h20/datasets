Broadway Nights
{{Infobox film
| name           = Broadway Nights
| image          = Broadway Nights (1927) film poster.jpg
| border         = yes
| alt            = 
| caption        = Theatrical release poster
| director       = Joseph C. Boyle
| producer       = Robert Kane
| writer         = {{Plainlist|
* John W. Conway
* Forrest Halsey
}}
| story          = Norman Houston
| starring       = {{Plainlist| Lois Wilson Sam Hardy
}}
| music          = 
| cinematography = Ernest Haller    
| editing        = Paul F. Maschke
| studio         = Robert Kane Productions
| distributor    = First National Pictures
| released       =  
| runtime        = 7 reels; 6,765 feet
| country        = United States
| language       = {{Plainlist|
* Silent film
* English intertitles
}}
| budget         = 
| gross          = 
}} romantic drama Lois Wilson Sam Hardy. The film marked the debuts of Barbara Stanwyck and Ann Sothern who had small roles as fan dancers.   

The film is considered lost.

==Cast== Sam Hardy as Johnny Fay  Lois Wilson as Fanny Franchette 
* Louis John Bartels as Baron 
* June Collyer as Herself 
* Georgette Duval as Herself 
* Philip Strange as Bronson 
* Henry Sherwood as Himself 
* Sylvia Sidney as Herself 
* Francis "Bunny" Weldon as Nightclub producer 
* De Sacia Mooers as Texas Guinan 
* Barbara Stanwyck as Fan dancer 
* Ann Sothern as Fan dancer
* Lee Armstrong

==References==
 

==External links==
* 
* 
* 

 
 
 
 
 
 
 
 


 
 