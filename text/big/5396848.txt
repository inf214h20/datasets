Mythos (film)
{{Infobox television
| show_name            = Mythos
| image                =  
| caption              = Mythos (Collected Works of Joseph Campbell)
| show_name_2          = The Shaping of Our Mythic Tradition
| genre                = Documentary, Mythology
| creator              = Joseph Campbell
| developer            = Mythology, Ltd.
| writer               =  Robert Walter
| creative_director    = 
| presenter            = Susan Sarandon
| starring             = Joseph Campbell
| judges               = 
| voices               = 
| narrated             = 
| theme_music_composer = 
| opentheme            = 
| endtheme             = 
| composer             = 
| country              = United States
| language             = English
| num_seasons          = 3
| num_episodes         = 15
| list_episodes        = 
| executive_producer   = David M. Fox
| producer             = Bill Free
| editor               = 
| location             = 
| cinematography       = 
| camera               = 
| runtime              = 15 hrs
| company              = Joseph Campbell Foundation
| distributor          = Unipix/Acorn Media
| channel              = PBS
| picture_format       = NTSC
| audio_format         = 
| first_run            = 
| first_aired          = 
| last_aired           = 
| status               = Released
| preceded_by          = 
| followed_by          =  The Heros Journey
| website              = http://www.jcf.org/new/index.php?categoryid=83&p9999_action=details&p9999_wid=258
| production_website   = 
}}
Mythos is a three-part documentary that consists of a series of lectures given by Joseph Campbell. Campbell conceived of the original lectures, filmed over the last six years of his life, as a summation of what he had learned about the human mythic impulse, in terms of psychology, ethnology and comparative mythology—what he called "the one great story of mankind." 

==Transformations: A False Step==
After Campbells death and the posthumous celebrity brought by the airing in 1988 of The Power of Myth, the filmmakers who had recorded the lectures quickly cobbled together a much-abridged, hastily edited series for PBS entitled Transformations of Myth Through Time. An even-more-highly redacted version was briefly released under the title The World of Joseph Campbell.   and Mythos: from   

==Mythos Emerges==
Campbells estate, represented by his widow Jean Erdman and, eventually, by the Joseph Campbell Foundation (JCF), asked that these versions, which were unlicensed and did not accurately represent Campbells thoughts, be pulled from the market, and proposed the production of a twenty-hour television series in four parts that followed Campbells original vision more closely: Mythos.

Volume One of Mythos was released in 1999. Volume Two was released in 2000. Both parts are narrated by Susan Sarandon.

After these initial releases, the original distributor, Unipix, promptly went bankrupt, and production on the series halted. 

The JCF re-released the first two volumes in 2007 and 2008 in conjunction with Acorn Media as part of the Collected Works of Joseph Campbell series and the third volume was released in 2011. The decision was eventually made that the planned fourth volume dealing with James Joyces novels, was to be released in 2013 as a separate product.  This was motivated by the difficulties to make its contents fit with the overall format of Mythos.

The three volumes of Mythos were released together as Mythos - The Complete Series in September 2012.

==Mythos Episodes==

===Mythos: Vol. 1, The Shaping of Our Mythic Tradition (1999)===
*Mythos - 1.1: Psyche & Symbol  - The psychological impulse for and response to myth
*Mythos - 1.2: The Spirit Land  - How myths awakened American Indians to the mystery of life.
*Mythos - 1.3: On Being Human  - The emergence of myth in early hunter-gatherer societies
*Mythos - 1.4: From Goddesses to God  - The gradual shift from the Goddess to male, warlike deities
*Mythos - 1.5: The Mystical Life   - Non-biblical mythic strains that helped shape the Western spirit

===Mythos: Vol. 2, The Shaping of the Eastern Tradition (2000)===
*Mythos - 2.1: The Inward Path   - The core myths of the great Asian religions
*Mythos - 2.2: The Enlightend One  - The Buddha and enlightenment, East and West
*Mythos - 2.3: Our Eternal Selves  - Yoga and transcendence
*Mythos - 2.4: The Way to Illumination  - Kundalini yoga and the seven chakras
*Mythos - 2.5: The Experience of God  - Tibetan Buddhism and the spiritual journey that is death

===Mythos: Vol. 3, The Shaping of the Western Tradition (2011)===
*Mythos - 3.1: Love as the Guide - The Arthurian romances, including Tristan and Iseult
*Mythos - 3.2: The Path of the Heart - Parzival and the Grail Quest
*Mythos - 3.3: Beyond Time and Space - The Romantic philosophers
*Mythos - 3.4: Between Pairs of Opposites - Thomas Mann and The Magic Mountain
*Mythos - 3.5: Into the Well of Myth - The Joseph novels and modern myth

==Footnotes==
 

==External links==
* 
* 
* 
*  page on Joseph Campbell Foundation website
*  

 

 
 
 