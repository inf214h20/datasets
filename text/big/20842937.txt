A Touch of Grey
 
{{Infobox film
| name = A Touch of Grey
| image = 
| caption = Film Poster
| director = Sandra Feldman Ian D. Mah Tim Roberts(line producer)
| writer = Sandra Feldman Maria del David Wong
| music = Blackie and the Rodeo Kings Suzie Vinnick
| cinematography = Maya Bankovic
| editing = Nadia Tan 
| release =  
| language = English
| country = Canada
}} 
A Touch of Grey is a 2009 Canadian dramatic narrative film.  A Touch of Grey won Best Canadian Film at the 2010 Buffalo Niagara Film Festival, and lead Maria del Mar received a Canadian Comedy Award for her performance in the film. Written by Sandra Feldman and directed by herself and Ian D. Mah, A Touch of Grey is a coming of middle age film as 4 high school friends reuniting after 25 years, each facing a crossroads in their life as they ask themselves, "Is that all there is?"

== Plot ==
BARB (40s) takes a break from the stresses of work and home. With hopes of recapturing the optimism and enthusiasm she once had, Barb rents out a hotel suite to reunite with her former high school friends— PATTI, KAREN and LIZ . 

A knock on the door, and Patti and Karen arrive. Patti, a perky, but not so bright wife and mother, is excited to escape her domineering husband for a weekend. Karen, whose life seems in much better shape, is just happy to be with old friends. After initial pleasantries and lies, it seems everything is going well with each others lives. In an attempt to break from the ties to the lives they have left at home, Patti and Barb hide each others cell phones- freeing themselves for one night. The women head out to a local bar, but end up dragging back Patti. They tie her to a chair for aggressively hitting on a 20-year-old. The interrogations from Barb and Karen are interrupted by the arrival of LIZ, a newly divorced, single mother and career woman.

Patti confesses that as of late, she finds herself uncontrollably attracted to younger men. Unexpected at this time in her life, she is having a sexual awakening. With increasingly more wine on board,  Barb admits that she is unhappy with her life- having built a life for herself for all these years and realizing she hates what she has built. She seriously considers to leave her husband and family- to be set free. No more burden or responsibilities. Karen, always sensible, tries to reassure Barb to stay in the life she is in- to fix the problems without running away. Liz strongly disagrees, arguing that she should free herself from the suffocating life. Liz and Karen get into a heated argument, each justifying the choices Barb should make. To discredit Karen, Liz reveals that Karen’s husband is having an affair. Barb and Patti are shocked to learn that Karen has no intention of leaving her marriage despite her husbands continuing adultery. It is difficult for the friends to support Karen and her decision. Patti decides she is no longer taking advice from Karen and storms out to return to the bar in search of her 20-year-old.

As the hours pass, Barb, Liz, and Karen settle their differences over the contents of the hotel mini-bar. Liz confesses to Barb that being a single mother, middle-aged divorcee is not as fun and glamorous as it seems. it becomes apparent that each of their lives is a mess. Once Patti returns, the girls retire for the night-all relieved that Patti is safe and unharmed from the evening. Pattis life, however, has been shattered. Having gone back to seduce the 20 year old, she finds that her looks and charms dont work anymore. She is rejected by the 20 year old. A devastating discovery for her. For the first time in her life, Patti realizes she is old. Not knowing how to handle the truth, she has sex with a stranger in the alley. Patti begs Barb to help her keep this secret from Karen and Liz. To cover up Patti’s tears before the other women see them, Barb impulsively shoves a lemon in Patti’s eye.

The next morning, Karen, Liz and Patti get ready to go back to their lives. Liz sees that Barb is at a crossroads and advises her that she must choose a road- any road. After the friends leave, Barb gets a phone call from her husband. Barb makes her decision. She prepares a list of food that is needed for dinner that evening. Barb returns home, but its not to the same place.

== Crew ==
 Tim Roberts
*First Assistant Director: William Beauchamp
*Production Designer: Dan Lefebre
*Production Assistant: Brian Collins
*Assistant Editor: Daniel Warth

*Wardrobe: Ginger Martini
*Hair and Makeup: Ginger Martini, Rachel Ford, Shauna Lee

*Sound: Emiliano Paternostro
*1st Assistant Camera: Chuch Taylor
*2nd Assistant Camera: Peter Diaz
*Steadicam operator: Jason Vieira
*Gaffer: Gabrielle Nadeau
*Grip: Grant Wood
*Continuity: Daniel Warth

== External links ==
*  
*  
*  
*  
*  

 
 
 
 