Never Steal Anything Small
  Roger Smith, Cara Williams, Nehemiah Persoff, Royal Dano, and Horace McMahon. The film was based on The Devils Hornpipe by Maxwell Anderson and released by Universal-international.

==Production==
Filmed in color, this musical was directed by Charles Lederer. Its about racketeers infiltrating the labor movement. Cagney, as a dishonest but charming union boss, gets to sing and dance a little. This was Cagneys final musical film. Jones, who plays a happily married woman whom the labor leader wants to steal away, does a lively musical number, spoofing television commercials. One of the highlights is a duet between Cagney and Williams. The film has a running time of 94 minutes and has been released on home video, as well as featured on American Movie Classics.

==Cast==
* James Cagney as Jake MacIllaney
* Shirley Jones as Linda Cabot Roger Smith as Dan Cabot
* Cara Williams as Winnipeg Simmons
* Nehemiah Persoff as Pinelli
* Horace McMahon as O.K. Merritt
* Jack Albertson as Sleep-Out Charlie

==Sources==
*American Movie Classics
*Video Movie Guide, edited by Mick Martin and Marsha Porter (New York: Ballantine Books, 1996)

==External links==
* 

 

 
 
 
 
 
 
 


 