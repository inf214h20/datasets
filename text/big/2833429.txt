Manjil Virinja Pookkal
{{Infobox film
| name           = Manjil Virinja Pookkal
| image          = Manjil Virinja Pookkal.jpg
| caption        = Promotional Poster designed by P. N. Menon (director)|P. N. Menon Fazil
| writer         = Fazil Shankar Poornima Jayaram Mohanlal Nedumudi Venu
| producer       = Navodaya Appachan
| studio         = Navodaya
| distributor    = Navodaya Ashok Kumar
| editing        = T. R. Shekhar
| released       =     
| runtime        = 150 minutes
| country        = India
| language       = Malayalam
| music          = Jerry Amaldev Guna Singh
                   Bichu Thirumala (lyrics) 
| budget         =  15 lakhs
| gross          =  98 lakhs 
}}
 1980 Malayalam film produced by Navodaya Appachan and directed by Fazil (director)|Fazil, starring Shankar (actor)|Shankar, Poornima Jayaram, and Mohanlal in the main roles. This film was one of the biggest hits of the year and was a major landmark in the career of director Fazil. The songs were composed by Jerry Amaldev.

Manjil Virinja Pookkal was Mohanlals first release.  His first film Thiranottam was never released in theaters. The film was shot mainly from Kodaikanal, Tamil Nadu. The first dialogue of Mohanlal, which begins as "I am Narendran" was shot from the Astoria Hotel, near Kodaikanal busstand.  
 Sleeping with Agni Sakshi (1996), Koi Mere Dil Se Poochhe (2002); into Tamil as Aval Varuvala (1998); and into a Pakistani movie Khilona.

== Plot ==
The story is about Prem Kishan (Shankar) coming to the hills of Kodaikkanal on work, meeting and falling in love with Prabha (Poornima Jayaram) whom he knows little about. Later  he realises that she is married when her husband, Narendran (Mohanlal) comes into  the picture. Mohanlal plays the antagonist in this film, one of the many negative characters he has played in his career. The film ends on a sad note, with Prem Kishan committing suicide after killing Narendran, saddened by the loss of Prabha, who was killed by Narendran.

==Cast==
  as Narendran in the film]] Shankar as Prem Kishan
* Poornima Jayaram as Prabha Narendran
* Mohanlal as Narendran
* Prathapachandran as Sivasankara Panikkar
*Alummoodan as Kushalan
* Nedumudi Venu as Seythalavi

Manjil Virinja Pookkal was Mohanlals first release. His first film Thiranottam was only released in 1 center with difficulties. Before Mohanlal was cast in the role of the antagonist, other actors were considered for the part, including Bharath Gopi and Sukumaran. Sukumaran almost got the part but was turned down because the director felt that he looked too old. Mohanlal was 20 years old when in this film. And so was Shankar.

There are reports that Kamal Haasan was to play Shankars role and Sukumaran to play Mohanlals role. But the produer decided to make a shift to newcomers since dates of Kamal were not available.

==Credits== Fazil
* Fazil
* Produced by Navodaya Appachan
* Original Music by Jerry Amaldev (songs) and Gunasingh (score)
* Cinematography by Ashok Kumar
* Film Editing by T.R. Shekhar
* Second Unit Director or Assistant Director: Sibi Malayil
* Lyricist: Bichu Thirumala
* Singers: Vani Jairam, S. Janaki, K. J. Yesudas

==Soundtrack==
The evergreen hit songs of this movie was composed by Jerry Amaldev and penned by Bichu Thirumala.
{| class="wikitable"
|-
! Track !! Song Title !! Singer(s)
|-
| 1 || Manchadi Kunnil || K. J. Yesudas, Vani Jayaram
|-
| 2 || Manjani Kompil || S. Janaki
|-
| 3 || Mizhiyoram Nananjozhukum || K. J. Yesudas
|-
| 4 || Manjani Kompil (Pathos) || S. Janaki
|-
| 5 || Mizhiyoram Nilavalayo || S. Janaki
|-
|}

==Box office==
The movie was a slow starter but turned to be a blockbuster of the year. The movie which was made in a budget of  15 lakhs gained  98 lakhs.   


Shankars symbolism before kissing (moving his index finger from left chin to his upper lip and then downwards)  was a rage among the youth.

==Awards==
; 1980 Kerala State Film Awards   Best Popular Film - Manjil Virinja Pookkal Best Actress - Poornima Jayaram Best Music Director  - Jerry Amaldev Best Play Back Singer - K. J. Yesudas Best Play Back Singer - S. Janaki Best Background Music - Gunasingh

==References==
 

== External links ==
*  

 

 
 
 
 
 