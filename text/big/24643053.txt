Slander House
{{Infobox film
| name           = Slander House
| image          =File:Slander House poster.jpg
| image_size     =
| caption        =Film poster
| director       = Charles Lamont
| producer       = Ben Judell (producer) Melville Shyer (associate producer)
| writer         = John W. Krafft (writer) Gertrude Orr (writer) Madeline Woods (novel Scandal House)
| narrator       =
| starring       = See below
| music          =
| cinematography = M.A. Anderson
| editing        = S. Roy Luby
| distributor    =
| released       =  
| runtime        = 65 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}

Slander House is a 1938 American film directed by Charles Lamont.  The films producer was Ben Judell of Progressive Pictures, known for low-budget exploitation films with provocative titles; other films released by Progressive the same year included Rebellious Daughters and  Delinquent Parents.  

== Cast ==
*Adrienne Ames as Helen "Mme. Helene" Smith Craig Reynolds as Pat Fenton
*Esther Ralston as Ruth De Milo
*George Meeker as Dr. Herbert Stallings
*Pert Kelton as Mazie Mason William Newell as Terry Kent
*Dorothy Vaughan as Mrs. Horton Edward Keane as George Horton
*Vivien Oakland as Mrs. Conway
*Ruth Gillette as Mme. Renault
*Dot Farley as Mrs. Willis
*Bonita Weber as Mrs. Richards
*Blanche Payson as Hilda
*Mary Field as Bessie, an attendant
*Louise Squire as Olga
*Fay McKenzie as Anna
*Harry Depp as Higginbotham
*Robert Homans as The Irish Policeman

==References==
 

== External links ==
* 
* 
 

 

 
 
 
 
 
 
 
 
 


 