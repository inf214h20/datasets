The Broken Melody (1937 film)
 The Broken Melody}}
 
 
{{Infobox film
| name           = The Broken Melody
| image          =
| image_size     = 
| caption        = Theatrical release poster
| director       = Ken G. Hall
| producer       = Ken G. Hall Frank Harvey 
| based on       = novel by F. J. Thwaites
| narrator       = 
| starring       = Lloyd Hughes  Alfred Hill (special theme) George Heath
| editing        = William Shepherd
| studio         = Cinesound Productions
| distributor = British Empire Films (Aust) RKO (UK)
| released       = 17 June 1938 (Australia) 1938 (UK)
| runtime        = 89 mins
| country        = Australia
| language       = English
| budget = £20,000 (est.)
}}

The Broken Melody is a 1938 Australian drama film directed by Ken G. Hall and starring Lloyd Hughes, based on a best-selling novel by F. J. Thwaites.

==Synopsis==
John Ainsworth helps win a rowing race for Sydney University against Melbourne University. While celebrating at a nightclub, he demonstrates his skill with the violin with one of his original compositions. He also flirts with a young woman, Ann Brady, to the displeasure of a crook, Webster. A brawl results and John is expelled from university. Johns sheep farmer father – who is disdainful of culture and wants John to marry a rich girl – is furious and disowns his son.

The Depression is in full flight and John has difficulty obtaining work. He befriends a pickpocket, Joe, who invites John to live with him in the Sydney Domain. One night he comes across Ann trying to commit suicide under the Sydney Harbour Bridge. He stops her and she goes to live with John and Joe.

John gets hold of his old violin and starts playing for his fellow homeless vagrants in the Domain. He is overheard one night by a rich couple travelling through the area and soon becomes a well known violinist under the name "John Hilton", working with manager. He goes to London, taking Joe with him as his valet but leaving Ann behind. He meets a famous singer, Madame Le Lange, and rises to fame as a leading conductor and composer.

John returns to Australia intending to conduct his new opera in triumph. His father, whose property is greatly in debt, suffers a heart attack. Madame Le Lange throws a tantrum and refuses to appear. Anne steps in, Johns father recovers and the opera is a big success.

==Cast==
*Lloyd Hughes as John Ainsworth
*Diana Du Cane as Ann Brady
*Rosalind Kennerdale as Madame de Lange Frank Harvey as Jules de Latanac
*Alec Kellaway as Joe Larkin
*Harry Abdy as Sam Harris
*Rita Pauncefort as Bella
*Harold Meade as Michael Ainsworth
*June Munro as Nibs Ainsworth
*Ronald Whelan as Bullman
*Lionello Cecil as the tenor
*Letty Craydon as the maid
*Marshall Crosby as rowing trainer
*Gough Whitlam as man in nightclub

==Original novel==
{{infobox book |  
| name          = The Broken Melody
| title_orig    = 
| translator    = 
| image         = 
| caption = 
| author        = F. J. Thwaites
| illustrator   = 
| cover_artist  = 
| country       = Australia
| language      = English
| series        = 
| genre         = melodrama
| publisher     = Publicity Press
| release_date  = 1930
| english_release_date =
| media_type    = 
| pages         = 
| isbn          = 
| preceded_by   = 
| followed_by   = The Melody Lingers
}}
The script was adapted from the debut novel by F. J. Thwaites.

===Plot===
The novel was about Ted Jenkins, a farmers son, is kicked out of boarding school for addiction to dope; when his father finds out, he kicks Ted off their property as well. Ted moves to Sydney and becomes homeless; he thinks about killing himself when he sees a young woman, Carmol, thinking of killing herself, too. They decide not to do it and Carmol nurses Ted back to health. The two form a strong bond (though not a romantic one – Teds love is for Nibs, the daughter of Teds fathers best friend).
 Kings Cross, then moves to Thursday Island to improve his health, eventually running a pearling business for three years.

While on Thursday Island, Ted/Digby runs into Fay le Bretton, a world class pianist on holiday, who discovers his talent for the cello and persuades him to come to London. The night before Teds London debut he finds out that Carmol has been killed in a hit and run accident, but he still performs and is a big success.

Four years later, Ted returns to Australia a rich man; he buys his family property (on the market due to a drought) and is reunited with his dying father and Nibs. 

===Background===
Thwaites started writing the novel when he was seventeen, without having even written a short story beforehand.  He later claimed his motivation was to perpetuate the memory of his grandfather, Francis Jenkins, who settled on "Buckingbong" Station in 1827. Thwaites grandmother, Mrs. Lydia Jenkins, was born in 1839, and was claimed to have been the first white child born in the south-western portion of New South Wales. "It was my object to write a book around the Jenkins family so that the name of Jenkins would live again", said Thwaites. 

However Thwaites could not get anyone to publish it, so he decided to do it himself. "Then the problems started", he later recalled. "I travelled from Sydney to Townsville interviewing every librarian, bookseller and newsagent on the way and returned without selling one copy. I was about to give the game away but decided to try the Riverina first."   

He arrived in Wagga Wagga and introduced himself to the editor as the great grandson of a local property owner. The paper ran a story of him and the publicity enabled him to sell his book. 

===Reception===
Critical reception was not strong. One critic wrote that the book "betrays the prentice hand, for Mr. Thwaites does not yet know men and women, nor does he understand life. Neither fortunes nor girls are won as easily as he Imagines. But be has enterprise, and his novel has Interest." 

The book became a best seller.  By 1935 it was estimated to have sold 55,000 copies in Australia and 25,000 in England.  Thwaites wrote a sequel, The Melody Lingers (1935). By 1968 it had been reprinted 54 times and was estimated to have sold over a million copies. 

In 1933 it was announced that Thwaites was travelling to Hollywood to sell the book to the movies.  He was no fan of the Australian film industry at this time; according to a 1933 interview: On Our Harmony Row, The Sentimental The Squatters Daughter, in all of which there was at least one imbecile or half-wit. People abroad viewing these pictures could not be blamed for coming to the conclusion that about one person in every four or five in Australia was sub normal. The Efftee Studios in Australia were deserving of praise for their pioneering work, but surely it was possible to portray humour on the screen without associating it with lunacy.    
Ironically, The Broken Melody would be turned into a film by Ken G. Hall, who also directed On Our Selection and The Squatters Daughter.

==Production==
The movie was to be the first of five movies made by Cinesound Productions for a total cost of ₤100,000. 
 Ann Richards was not cast the film as she was exhausted after making three films back to back. 

Filming started in September 1937, with the elaborate cabaret scene shot first.  Future Australian Prime Minister Gough Whitlam was a law student at the time and appeared as an extra in this scene. Shooting wound up in November.

===Music=== Alfred Hill was especially commissioned to write an operetta for the movie.  The opera scenes were shot using the "play back" method. 

==Release==
Reviews praised the movie but criticised the melodramatic plot.  Hall gave the movie a sneak preview in the style of Hollywood studios which produced a strong response and box office performance was solid.   
 The Broken Melody (1934).

Despite the films success, this was Halls last drama for Cinesound. In 1938 the British government ruled that Australian films no longer counted as British for purposes of the local quota, and therefore could not be guaranteed release over there. Accordingly, Cinesounds next six movies were all comedies even though Hall preferred drama.

"It is so much cleaner", he said at the time. "There is no mess of pies and so forth to be swept from the studio, as there is after slapstick." 

F. J. Thwaites expressed pleasure with the film. 

==The Melody Lingers==
{{infobox book |  
| name          = The Melody Lingers
| title_orig    = 
| translator    = 
| image         = 
| caption = 
| author        = F. J. Thwaites
| illustrator   = 
| cover_artist  = 
| country       = Australia
| language      = English
| series        = 
| genre         = melodrama
| publisher     = Jackson and OSullivan
| release_date  = 1935
| english_release_date =
| media_type    = 
| pages         = 
| isbn          = 
| preceded_by   = The Broken Melody
| followed_by   = 
}}

In 1935, Thwaites published a sequel, The Melody Lingers. 

===Plot===
Dale Jenkins, heir to a rich property, tries to become a writer. 

==Radio Adaptation==
The novel was adapted for radio in 1961. 

==References==
 

==External links==
*  in the Internet Movie Database
*  at Australian Screen Online
*  at Oz Movies
*  at AustLit
*  at AustLit
*  at AusLit
 
 

 
 
 
 
 
 
 
 
 