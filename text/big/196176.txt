Straw Dogs (1971 film)
{{Infobox film
| name = Straw Dogs
| image = Straw dogs movie poster.jpg
| alt = 
| caption = Theatrical release poster
| director = Sam Peckinpah
| producer = Daniel Melnick
| screenplay = {{plainlist|
* Sam Peckinpah
* David Zelag Goodman
}}
| based on =  
| starring = {{plainlist|
* Dustin Hoffman Susan George
}}
| music = Jerry Fielding
| cinematography = John Coquillon
| editing = {{plainlist|
* Paul Davies
* Tony Lawson
* Roger Spottiswoode
}}
| studio = {{plainlist| ABC Pictures
* Talent Associates
}}
| distributor = Cinerama Releasing Corporation
| released =  
| runtime = 117 minutes   113 minutes    
| country = {{plainlist|
* United States
* United Kingdom
}}
| language = English
| budget = $2.2 million "ABC Motion Pictures|ABCs 5 Years of Film Production Profits & Losses", Variety (magazine)|Variety, 31 May 1973 p 3 
| gross = $8 million  (rentals)  
}} Susan George. Gordon M. Williamss 1969 novel The Siege of Trenchers Farm.    The films title derives from a discussion in the Tao Te Ching that likens the ancient Chinese ceremonial straw dog to forms without substance.
 A Clockwork The French Connection, and Dirty Harry, the film sparked heated controversy over the perceived increase of violence in cinema.     
 remake directed by Rod Lurie was released on September 16, 2011.

==Plot==
David Sumner, an American mathematician, comes to live with his young trophy wife, Amy, in her hometown, a small village in a remote part of Cornwall, England.

Amys return is of particular interest to her ex-boyfriend Charlie Venner and his cronies, Norman Scutt, Chris Cawsey and Phil Riddaway, who are immediately resentful of the outsider who has married one of their own. David hires the men to carry out repairs to the isolated farmhouse he and Amy have rented, Trenchers Farm. Tensions in the Sumners marriage soon become apparent—explicitly so when Amy stands topless in a window in full view of the workmen.

When Amy discovers their dead cat hanging by a light chain in their bedroom closet, she claims the workmen are responsible. She presses him to confront them, but he refuses. Later, the men invite David to go hunting in the woods with them.  During the hunting trip, the workmen take him to a remote forest meadow and leave him there with the promise of driving the birds towards him. Having ditched David, Charlie Venner returns to the couples farmhouse where he initiates sex with Amy. She at first resists but eventually appears to submit, repeatedly embracing and kissing him. This is the films most controversial scene due to the ambiguous nature of the encounter, there was much debate about whether it depicted a rape or consensual sex. Some censors complained that it showed a victim appearing to enjoy being violated, although the director insists this is not accurate (see "Reception" section below). As Amy and Charlie lay together, Norman Scutt enters silently and forces Venner at gunpoint to hold Amy down while he rapes her in a sequence far less ambiguous as Amy screams and struggles to break free, to no avail.

The next day, David, who is seemingly unaware of his wifes ordeal, fires the workmen. Later that week, the Sumners attend a church social where Amy becomes distraught after seeing the men who raped her. They leave the social early, and, while driving home through thick fog, accidentally hit the local village idiot Henry Niles, whom they take to their home. David phones the local pub about the accident. However, earlier that evening Niles had accidentally strangled a flirtatious young girl from the village, Janice Hedden, and now her father, the town drunkard, Tom, and the workmen looking for him are alerted by the phone call to Niless whereabouts.

Soon the drunken locals, including Amys rapists, are pounding on the door of the Sumners home. The local magistrate, Major Scott, arrives to deal with the situation, but is accidentally shot dead by Tom. David realises that he, Amy and Niles are now in mortal danger, and prepares to defend his household. 

In the ensuing mayhem all the attackers are either killed or incapacitated, as David is forced to confront the violence he has long repressed in himself.

==Cast==
* Dustin Hoffman as David Sumner Susan George as Amy Sumner
* Peter Vaughan as Tom Hedden
* T. P. McKenna as Major John Scott
* Del Henney as Charlie Venner Jim Norton as Chris Cawsey
* Donald Webster as Riddaway
* Ken Hutchison as Norman Scutt
* Len Jones as Bobby Hedden
* Sally Thomsett as Janice Hedden
* Robert Keegan as Harry Ware
* Peter Arne as John Niles
* Cherina Schaer as Louise Hood
* Colin Welland as Reverend Barney Hood David Warner as Henry Niles
* June Brown (deleted scenes) as Mrs. Hedden

==Production==
Sam Peckinpahs two previous films, The Wild Bunch and The Ballad of Cable Hogue, had been made for Warner Bros.-Seven Arts.  His connection with the company ended after the chaotic filming of Cable Hogue wrapped 19 days over schedule and $3 million over budget. Left with a limited number of directing jobs, Peckinpah was forced to travel to England to direct Straw Dogs. Produced by Daniel Melnick, who had previously worked with Peckinpah on his 1966 television film Noon Wine, the screenplay was based on Gordon Williams novel The Siege of Trenchers Farm. 
 adaptation of the novel drew inspiration from Robert Ardreys books African Genesis and The Territorial Imperative, which argued that man was essentially a carnivore who instinctively battled over control of territory. A significant difference between the novel and the film is the Sumner couple have a daughter who is also trapped in the farmhouse. Peckinpah removed the daughter and rewrote the character of Amy Sumner as a younger and more liberated woman.  The film was shot on location at St Buryan, Cornwall. 
 pacifist unaware of his feelings and potential for violence that were the very same feelings he abhorred in society.  Judy Geeson, Jacqueline Bisset, Diana Rigg, Helen Mirren, Carol White, Charlotte Rampling, and Hayley Mills were considered for the role of Amy before Susan George was finally selected.  Hoffman disagreed with the casting, as he felt his character would never marry such a "Lolita|Lolita-ish" kind of girl. Peckinpah insisted on George, an unknown actress at that time. 

==Reception==
  of The Chicago Sun-Times rated it 2/4 stars and described the film as "a major disappointment in which Peckinpahs theories about violence seem to have regressed to a sort of 19th-Century mixture of Kipling and machismo."   Vincent Canby of The New York Times called it "a special disappointment" that is "an intelligent movie, but interesting only in the context of his other works."   Variety (magazine)|Variety wrote, "The script (from Gordon M. Williams novel The Siege of Trenchers Farm) relies on shock and violence to tide it over weakness in development, shallow characterization and lack of motivation."   Entertainment Weekly wrote that the contemporary interpretation was that of a "serious exploration of humanity’s ambivalent relationship with the dark side", but it now seems an "exploitation bloodbath".   Nick Schager of Slant Magazine rated it 4/4 stars and wrote, "Sitting through Peckinpahs controversial classic is not unlike watching a lit fuse make its slow, inexorable way toward its combustible destination—the taut build-up is as shocking and vicious as its fiery conclusion is inevitable."                                       Philip Martin of the Arkansas Democrat-Gazette wrote, "Peckinpahs Straw Dogs is a movie that has remained important to me for 40 years. Along with Stanley Kubricks A Clockwork Orange, Straw Dogs stands as a transgressively violent, deeply 70s film; one that still retains its power to shock after all these years." 

===Box office===
The film earned rentals of $4.5 million in North America and $3.5 million in other countries. By 1973 it had recorded an overall profit of $1,425,000. 

===Controversy=== misogynistic Sadistic personality disorder|sadism, and male chauvinism,     especially disturbed by the scenes intended ambiguity—after initially resisting, Amy appears to enjoy parts of the first rape, kissing and holding her attacker, although she later has traumatic flashbacks. It is claimed that "the enactment purposely catered to entrenched appetites for desired victim behavior and reinforces rape myths".  Another criticism is that all the main female characters depict straight women as perverse with every appearance of Janice and Amy used to highlight excessive sexuality.  
 fascist celebration of violence and vigilantism. Others see it as anti-violence, noting the bleak ending consequent to the violence.  Dustin Hoffman viewed David as deliberately, yet subconsciously, provoking the violence, his concluding homicidal rampage being the emergence of his true self; this view was not shared by director Sam Peckinpah. 

The village of St Buryan was used as a location for the filming with some of the locals appearing as extras. Local author Derek Tangye reports in one of his books that they were not aware of the nature of the film at the time of filming, and were most upset to discover on its release that they had been used in a film of a nature so inconsistent with their own moral values. 

===Censorship=== R rating from the MPAA film rating system|MPAA. 
 Video Recordings Act, "because of Amys violent rape".  The film had been released theatrically in the United Kingdom, with the uncut version gaining an X rating in 1971 and the slightly cut US R-rated print being rated 18 in 1995. In March 1999 a partially edited print of Straw Dogs, which removed most of the second rape, was refused a video certificate when the distributor lost the rights to the film after agreeing to make the requested BBFC cuts, and the full uncut version was also rejected for video three months later on the grounds that the BBFC could not pass the uncut version so soon after rejecting a cut one.

On July 1, 2002, Straw Dogs finally was certified unedited on VHS and DVD.  This version was uncut, and therefore included the second rape scene, in which in the BBFCs opinion "Amy is clearly demonstrated not to enjoy the act of violation".  The BBFC noted that:  

==See also==
*List of films featuring home invasions

==References==
 

==External links==
* 
* 
* 
* 
*  for Criterion Collection
*  at Salon.com

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 