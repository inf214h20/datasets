The Virtuous Sin
{{Infobox film
| name           = The Virtuous Sin
| image          = TheVirtuousSinPoster.jpg
| caption        = Original poster
| director       = George Cukor Louis J. Gasnier
| producer       =
| writer         = Martin Brown Louise Long Based on a play by Lajos Zilahy
| narrator       =
| starring       = Walter Huston Kay Francis Kenneth MacKenna
| music          = Sam Coslow Ralph Rainger David Abel
| editing        = Otho Lovering
| studio         =
| distributor    = Paramount Pictures
| released       =  
| runtime        = 80 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
The Virtuous Sin is a 1930 American comedy-drama film directed by George Cukor and Louis J. Gasnier. The screenplay by Martin Brown and Louise Long is based on the play The General by Lajos Zilahy.

==Plot==
Marya is the wife of medical student Victor Sablin, who finds it impossible to deal with military life when he is inducted into the Russian army during World War I.  With her husband is sentenced to death by firing squad due to his insubordination, Marya offers herself to General Gregori Platoff in order to save him. When the two unexpectedly fall in love, Victor — not caring that his life has been spared — threatens to kill his rival.  His determination to eliminate the general falters when Marya confesses she is not in love with her husband — and never was.

==Cast==
*Walter Huston as Gen. Gregori Platoff
*Kay Francis as Marya Sablin
*Kenneth MacKenna as Victor Sablin

==Critical reception==
Mordaunt Hall of the New York Times called the film "a clever comedy with a splendid performance by Walter Huston" and added, "There is a constant fund of interest in this pictures action. It is one of those rare offerings in which youth takes a back seat. 

==George Cukors reflection in 1972==
In the book On Cukor, director   and Walter Huston, though." {{citation
|last1=Parish
|first1=James Robert
|last2=Mank
|first2=Gregory W.
|last3=Stanke
|first3=Don E.
|title=The Hollywood Beauties
|year=1978
|publisher=Arlington House Publishers
|location=New Rochelle, New York 
|isbn=0-87000-412-3
|page=73
}} 

==Preservation status==
A complete print of this film is held by the UCLA Film and Television Archive. However, the UCLA archives website says the print is too shrunken for projection. 

==References==
 

==External links==
* 

 
 

 
 
 
 
 
 
 
 
 
 