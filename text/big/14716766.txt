Where Were You When the Lights Went Out?
{{Infobox film
| name           = Where Were You When the Lights Went Out?
| image          = WhereWereYouWhentheLightsWentOutPoster.jpg
| caption        = Theatrical poster
| director       = Hy Averback
| producer       = Everett Freeman Martin Melcher
| writer         = Everett Freeman Karl Tunberg Original work: Claude Magnier Patrick ONeal Robert Morse Terry-Thomas Lola Albright Jim Backus
| music          = Dave Grusin
| cinematography = Ellsworth Fredericks
| editing        = Rita Roland
| studio         = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 89 minutes
| country        = United States English
| budget         =
| gross          = $7,988,000   
}} American comedy states lost electricity for French play Monsieur Masure by Claude Magnier. 

This was the penultimate film of Doris Days long career, being released two months before her final screen appearance in 1968s With Six You Get Eggroll.

==Synopsis== Broadway play for the night, she returns home unexpectedly and discovers her architect husband Peter (Patrick ONeal) being overly attentive to attractive reporter Roberta Lane (Lola Albright). Infuriated, she heads to the couples weekend house in Connecticut and takes a concoction to fall asleep.
 corporate embezzler Waldo Zane (Robert Morse), fleeing New York with an attache case full of money, develops car trouble near Margarets weekend house, he lets himself in and unwittingly takes some of the elixir himself, falling into a deep sleep beside her. 

Peter shows up, sees the two together and assumes his wife has been unfaithful. Despite their claims of innocence and ignorance, Peter believes neither of them and heads back to Manhattan.

Margarets agent Ladislaus Walichek (Terry-Thomas), anxious because she has announced her plan to retire, keeps her husbands jealousy burning in the hope their marriage will crumble and shell be forced to continue working to support herself. 

Margaret and Peter eventually reconcile, but new questions about what really happened when the lights went out arise when she gives birth exactly nine months after that fateful night.

==Production notes==
The films title tune was written by Dave Grusin and Kelly Gordon and sung by The Lettermen. George W. Davis and Urie McCleary were the films art directors, and costumes were designed by  Glenn Connelly. 

Morgan Freeman is seen briefly as a Grand Central Terminal commuter but does not receive on-screen credit.
 16th highest grossing film of 1968.

==Principal cast==
*Doris Day as Margaret Garrison Patrick ONeal as Peter Garrison
*Robert Morse as Waldo Zane
*Terry-Thomas as Ladislaus Walichek
*Lola Albright as Roberta Lane
*Steve Allen as Radio Announcer
*Jim Backus as Tru-Blue Lou
*Ben Blue as Man with a Razor
*Pat Paulsen as Conductor
*Dale Malone as Otis J. Hendershot, Jr.
*Robert Emhardt as Otis J. Hendershot, Sr.
*Harry Hickox as Detective Captain
*Parley Baer as Dr. Dudley Caldwell
*Randy Whipple as Marvin Reinholtz Earl Wilson as Himself
*Morgan Freeman as Grand Central Commuter (uncredited)

==Critical reception==
In her review in The New York Times, Renata Adler wrote, "a good part of the movie permits Miss Day to play an actress something like herself, and this might be fresh and almost poignant." 

Roger Ebert of the Chicago Sun-Times stated, "I dont find it funny at all." 

Variety (magazine)|Variety described it as "an okay Doris Day comedy, well cast with Robert Morse and Terry-Thomas . . . Averbacks comedy direction lifts things a bit out of a well-plowed rut, making for an amusing, while never hilarious, film." 

Time Out New York calls it "a sprightly comedy" and adds, "the performances are superb (Morse, ONeal and Albright, especially), and Averbacks comic timing is spot on." 

TV Guide describes it as "a trifle that starts out funny enough but sinks into predictability, made somewhat better by the adroit acting that triumphs over the lackluster script." 

 
==References==
 

==External links==
 
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 