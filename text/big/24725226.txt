Leap Year (2010 film)
{{Infobox film
| name           = Leap Year
| image          = Leap year poster.jpg
| alt            = Redhaired woman in a green dress with a man with stubbly beard wearing a grey top and blue jeans
| caption        = Theatrical release poster
| director       = Anand Tucker
| producer       = {{Plainlist|
* Gary Barber Chris Bender
* Roger Birnbaum
* Jonathan Glickman
* Jake Weiner
}}
| screenplay     = {{Plainlist|
* Harry Elfont
* Deborah Kaplan
}}
| starring       = {{Plainlist|
* Amy Adams
* Matthew Goode Adam Scott
* John Lithgow
}}
| music          = Randy Edelman
| cinematography = Newton Thomas Sigel Nick Moore
| studio         = {{Plainlist|
* Universal Pictures
* Spyglass Entertainment
* Optimum Releasing
}}
| distributor    = Universal Pictures Spyglass Entertainment (through Optimum Releasing)
| released       =  
| runtime        = 100 minutes
| country        = United States
| language       = English
| budget         = $19 million   
| gross          = $32,607,316   
}}
Leap Year is a 2010 American romantic comedy film directed by Anand Tucker, and starring Amy Adams and Matthew Goode. The film is about a woman who heads to Ireland to ask her boyfriend to accept her wedding proposal on leap day, when tradition supposedly holds that men cannot refuse a womans proposal for marriage. Her plans are interrupted by a series of increasingly unlikely events and are further complicated when she hires a handsome innkeeper to take her to her boyfriend in Dublin. The film premiered in New York City on January 6, 2010.   

==Plot==

Frustrated that her boyfriend of many years still has not proposed to her, Anna Brady travels to Dublin, Ireland to propose to her boyfriend Jeremy on February 29, leap day. According to Irish tradition, a man who is proposed to on leap day must accept the proposal. During the flight, a storm diverts the plane to Cardiff, Wales. Anna hires a boat to take her to Cork (city)|Cork, but the severity of the storm forces her to be put ashore on the Dingle Peninsula. She enlists the help of a surly Irish innkeeper, Declan OCallaghan, to taxi her across the country to Dublin to pull off the proposal in time.
 pub is threatened with foreclosure the next morning, he agrees to drive her for Euro|€500. Before they leave, Declan gets frustrated at Annas luggage and she snaps at him telling him to be careful, as her expensive Louis Vuitton luggage was a gift from her boyfriend. The two set out in Declans old car, but they quickly run into a herd of cows blocking the road. Anna shoos them away but steps in cow dung and leans on the car to remove it. As she leans on the car, it then rolls downhill into a lake. The two argue, and, angry at Declan, Anna walks away from him. She stops another car for a lift, but after offering to help her, three neer-do-wells take just her bags and drive away.

Anna and Declan eventually reach a pub where they discover the three thieves going through Annas luggage. Declan gets into a fight with them, and they are both kicked out by the landlord. Anna and Declan eventually reach a railway station on foot. The train arrives early, and Anna misses it. The two go to a bed and breakfast, where they are forced to pretend that they are married so that their conservative hosts will allow them to stay. Asked their married name, they simultaneously answer Brady and OCallaghan, quickly amending that to OBrady-Callaghan. During dinner, Anna and Declan are then forced to kiss, which causes some confusion for them. That night, they hesitantly sleep in the same bed. The next day, they take shelter from a hail storm in a church where a wedding is taking place, where, after a series of events, Anna gets drunk. At this point, Anna begins to question her intentions with Jeremy when she realizes she has an attraction for Declan.

The following day they arrive by bus in Dublin. On the way to the hotel, they stop by a park and Declan reveals that he was once engaged but that his ex-fiancée ran off with his best friend and his family ring to Dublin; Anna encourages him to get his ring back. When they arrive at Jeremys hotel, Jeremy surprises her and proposes to her, and she accepts after a hesitation—as Declan, dispirited, walks away.

Later, back in the USA at their engagement party, Anna learns that Jeremy decided to commit to her only in an effort to impress the manager of the expensive condominium the two were attempting to buy. Dismayed, Anna pulls the fire alarm and leaves after watching Jeremy grab all the electronics.

Anna goes back to the Dingle Peninsula in County Kerry, where Declan is successfully running his inn/restaurant. She proposes that they get together and not make plans, and Declan leaves the room. Anna interprets this as a rejection, so she rushes outside and ends up on a cliff overlooking the sea. Declan follows her and asks, Mrs. OBrady-Callaghan, where the hell are you going? and proposes to her with the family ring he retrieved from his ex-fiancée while in Dublin. She accepts and they kiss twice. Some time later, the two are shown driving in Declans car with a Just Married sign in the back window, while they playfully bicker about the expensive luggage, nicknamed Louis.

==Cast==
* Amy Adams as Anna Brady
* Matthew Goode as Declan OCallaghan Adam Scott as Jeremy Sloane
* John Lithgow as Jack Brady
* Kaitlin Olson as Libby
* Noel ODonovan as Seamus
* Tony Rohr as Frank
* Pat Laffan as Donal
* Alan Devlin as Joe
* Ian McElhinney as Priest
* Peter OMeara as Ron

==Production== Adam Scott was to play Jeremy Sloane, Annas long time boyfriend,    and that Kaitlin Olson would play Libby, Annas best friend.    Temple Bar, Georgian Dublin, Wicklow National Park and Olaf Street, Waterford.    On October 19, it was announced that Randy Edelman had been chosen to compose the films film score. The decision to choose Edelman came as a surprise, as Tucker had used Barrington Pheloung for two of his previous films, Hilary & Jackie and When Did You Last See Your Father?   

==Reception==
===Critical response===
Upon its release, the film received mostly negative reviews from critics. Review aggregation website Rotten Tomatoes gives the film a score of 21% based on reviews from 127 critics have given the film a positive review, with an average audience rating of 47%. {{cite web| url = http://www.rottentomatoes.com/m/leap_year_2010/| title=Leap Year (2010)
| publisher = Flixster| work = Rotten Tomatoes| accessdate = January 22, 2010}}
  The sites general consensus is that: "Amy Adams is as appealing as ever, but her charms arent enough to keep Leap Year from succumbing to an overabundance of clichés and an unfunny script." 
Metacritic, which assigns a weighted mean score out of 0–100 reviews from critics, has given the film a rating score of 33 based on 30 reviews. 

Roger Ebert of the Chicago Sun-Times described Leap Year as a full-bore, PG-rated, sweet rom-com. It sticks to the track, makes all the scheduled stops, and bears us triumphantly to the station.  Also, Owen Gleiberman of Entertainment Weekly gave the film a B- stating that the film could have used more pizazz. 

A. O. Scott of The New York Times saw it as so witless, charmless, and unimaginative, that it can be described as a movie only in a strictly technical sense.  Richard Roeper gave it a C-, stating that it had a Recycled plot, lame sight gags, Leprechaun-like stock Irish characters, adding that The charms of Amy Adams rescue Leap Year from Truly Awful status. 
 IRA men or twinkly rural imbeciles.   Matthew Goode, who co-stars in the film, admitted I just know that there are a lot of people who will say it is the worst film of 2010 and revealed that the main reason he signed on to the project was so that he could remain close to home and be able to see his girlfriend and newborn daughter. 

===Box office===
The film opened at the American box office at number 6, with a modest US$9,202,815, behind blockbusters  , as well as Daybreakers and Its Complicated (film)|Its Complicated.  The films final gross of US$25,918,920 in the United States against a production budget of US$19,000,000. In addition to this, the film made US$6,688,396 in international markets, for a final worldwide gross of US$32,607,316.

==Soundtrack==
An audio CD soundtrack for Leap Year was released on the Varèse Sarabande record label on January 12, 2010. That album contains only the original score, composed and conducted by Randy Edelman. The musical selections that were used, and credited at the end of the film are not, available on CD. Those include:
* "More and More of Your Amor" by  s production released 2009)
* "I Want You" by Kelly Clarkson
* "Ill Tell Me Ma|Ill Tell My Ma" by The Colonials featuring Candice Gordon
* "The Irish Rover" by The Colonials featuring Candice Gordon Eulogies
* "Waltz with Anna" by The Brombies
* "Patsy Fagan" by Dessie OHalloran and Sharon Shannon
* "Within a Mile of Home" by Flogging Molly
* "Buffalo Gals" by The Brombies
* "A Pint for Breakfast" by The Brombies
* "Leaping Lizards" by The Brombies
* "The Staunton Lick" by Lemon Jelly
* "Dream a Little Dream of Me" by The Mamas & the Papas
* "Only Love Can Break Your Heart" by Gwyneth Herbert Never Forget You" by Noisettes
* "You Got Me" by Colbie Caillat, over the closing scene/credits Just Say Yes" by Snow Patrol, used in the trailer

==Home media==
Leap Year was released on DVD in the United States on May 4, 2010.  It debuted at number 4 on the American DVD rentals chart, with a first week rental index of 56.63.  It placed 5th on the DVD sales chart, selling an estimated 159,843 units, and has sold almost 800,000 units in total to April 2013. 

==See also==

*Jab We Met

==References==
 

==External links==
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 