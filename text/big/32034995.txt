The Daughter of Dr. Jekyll
{{Infobox film
| name           = The Daughter of Dr. Jekyll
| image          = Daughter of Dr. Jekyll (1957) trailer 1.jpg
| image_size     =
| border         = yes
| caption        = Gloria Talbott and Arthur Shields in the film
| director       = Edgar G. Ulmer
| producer       =
| writer         =
| screenplay     = Jack Pollexfen
| story          =
| starring       = Gloria Talbott John Agar Arthur Shields
| music          =
| cinematography =
| editing        =
| studio         = Film Venturers Allied Artists
| released       =  
| runtime        = 71 minutes
| country        = United States
| language       = English
| budget         =
}}
 Allied Artists. The Cyclops. It features Gloria Talbott as Janet, the daughter of the infamous Strange Case of Dr Jekyll and Mr Hyde|Dr. Henry Jekyll, and John Agar as her fiancée. Janet learns that she may have inherited her fathers condition, and she begin to believe she may be guilty of murder when people are found horribly killed.  However, all is not what it seems. 

==Cast==
*Gloria Talbott as Janet Smith
*John Agar as George Hastings 
*Arthur Shields as Dr. Lomas 
*John Dierkes as Jacob
* Molly McCart as Maid Maggie 
*Martha Wentworth as Mrs. Merchant 
* Marjorie Stapp as Woman Getting Dressed 
* Reita Green as Young Woman
* Marel Page as Young Man

==Production==
In the films fight scene, the stuntman Ken Terrel stood in for Arthur Shields. The monster version of the girl was also played by a stunt double. To produce an unreal effect during this scene, the director set the action in a wooded region that had recently been burned by fire, then filmed it in ultraviolet light. 

==Reception==
American film critic Andrew Sarris noted that the film had a "scenario so atrocious that it takes forty minutes to establish that the daughter of Dr. Jekyll is indeed the daughter of D. Jekyll".  Yet film director Gary Don Rhodes suggests that the film "may be read as a critically significant text within the melodramatic crisis of female identity theater of the 1950s". He describes the film as an identity quest set in a dark fairy tale. 

==See also==
* The Son of Dr. Jekyll

==References==
{{reflist|refs=

   

   

   

   

   

   

}}

 
 

 
 
 
 

 