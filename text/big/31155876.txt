Virunnukari
{{Infobox film 
| name           = Virunnukari
| image          =
| caption        =
| director       = P. Venu
| producer       = P. Venu Panthalathu Sreedharan
| writer         = P. Venu P. J. Antony (dialogues)
| screenplay     = P. Venu Madhu Sheela Jayabharathi
| music          = MS Baburaj
| cinematography = TN Krishnankutty Nair
| editing        = G Venkittaraman
| studio         = Shanthasree
| distributor    = Shanthasree
| released       =  
| country        = India Malayalam
}}
 1969 Cinema Indian Malayalam Malayalam film, directed and produced by P. Venu . The film stars Prem Nazir, Madhu (actor)|Madhu, Sheela and Jayabharathi in lead roles. The film had musical score by MS Baburaj.   

==Cast==
 
*Prem Nazir as Madhavankutty Madhu as Sethu
*Sheela as Radha
*Jayabharathi as Shantha Ambika as Malathi
*K. P. Ummer as Surendran
*Adoor Bhasi as Swami
*Sankaradi
*Sreelatha Namboothiri as Sreelatha
*T. R. Omana as Surendrans mother
*T. S. Muthaiah as Panikkar
*Abbas (Old)
*Adoor Bhavani as Kalyani
*Kaduvakulam Antony as Paramu
*Lakshmi (Old) as Usha
*MS Namboothiri as Sankunni Menon
*Nellikode Bhaskaran as Raman Nair
*Vidhubala as Mohanam
*P. J. Antony as Raghava Menon master
*Sukumari as Kamakshiyamma
*P. R. Menon
 

==Soundtrack==
The music was composed by MS Baburaj and lyrics was written by P. Bhaskaran. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Ambaadi Pennungalodu || P. Leela || P. Bhaskaran || 
|-
| 2 || Chumalil Swapanathin || K. J. Yesudas || P. Bhaskaran || 
|-
| 3 || Innale Njaanoru  || S Janaki, CO Anto || P. Bhaskaran || 
|-
| 4 || Muttathemullathan || S Janaki || P. Bhaskaran || 
|-
| 5 || Pormulakkachayumaay || P. Leela || P. Bhaskaran || 
|-
| 6 || Vaasantha Sadanathin || P Jayachandran || P. Bhaskaran || 
|}

==References==
 

==External links==
*  

 
 
 

 