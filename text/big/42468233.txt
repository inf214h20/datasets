Spare Parts (film)
 
{{Infobox film
| name           = Spare Parts
| image          = Spare Parts poster.jpg
| alt            = 
| caption        = Theatrical release poster Sean McNamara
| producer       = {{Plainlist|
* David Alpert
* Rick Jacobs
* Leslie Kolins Small
* George Lopez
* Ben Odell}}
| writer         = Elissa Matsueda
| based on       =  
| starring       = {{plainlist |  
* George Lopez
* Jamie Lee Curtis Carlos PenaVega
* Marisa Tomei}}
| music          = Andrés Levin 
| cinematography = Richard Wong
| editing        = Maysie Hoy
| studio         = {{Plainlist|
* Brookwell-McNamara Entertainment
* Pantelion Films}} Lionsgate
| runtime        = 115 minutes 
| released       =  
| country        = United States
| language       = {{Plainlist|
* English
* Spanish}}
| budget         = 
| gross          = $3.6 million 
}} Sean McNamara Joshua Davis. The film was released on January 16, 2015 by Lions Gate Entertainment|Lionsgate. 

==Plot==
A group of four Carl Hayden High School students win first place over M.I.T. in a 2004 national robotics competition.  The team had only $800 and used car parts but won the competition over several highly funded university teams.    All four Mexican students were illegal immigrants from Mexico.      

==Cast==
* George Lopez as Fredi Cameron
* Jamie Lee Curtis as Principal Carlos PenaVega as Oscar Vazquez
* Esai Morales as Mr. Santillan
* José Julián as Lorenzo Santillan
* David Del Rio as Cristian Arcega
* Oscar Gutierrez as Luis Arranda Alexa PenaVega as Karla
* Alessandra Rosaldo as Mrs. Vazquez
* Marisa Tomei as Gwen

==Production==
Spare Parts is the first film under Lopezs recently announced film and TV deal with Pantelion Films and South Shore, the film and TV ventures between Lionsgate and Mexican media giant Televisa.      Much of the shooting of the film was done on location in Albuquerque, New Mexico.   

==Reception==
;Critical reaction
Spare Parts received mixed reviews from the critics. On Rotten Tomatoes, the film has a rating of 54% based on 24 critics.  On Metacritic, the film has a rating of 50 out of 100, based on 15 critics, indicating "mixed or average reviews". 

==References==
 

==External links==
*   
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 