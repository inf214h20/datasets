Be-Lagaam
 

 
Be-Lagaam is a 2002 film, directed by Ram Lakhan, and starring Pramod Moutho, Tanveer Zaidi and Rajeev Verma.     The film was written by Ayub M. Khan (story) and Kalim Rahi (dialogue).

== Cast ==

* Pramod Moutho - Ajit Pal (as Pramod Mauthoo)
* Tanveer Zaidi - Pramod Allahabadi ( LEAD ROLE )
* Kshitij Dhankar - Bali ( LEAD ROLE )
* Rajeev Verma  - Madhus Uncle
* Rakesh Bedi - Banarasi Babu
* Rana Jung Bahadur - Col. DSouza (as Ranajung Bahadur)
* Aryan Singh - Police Inspector ( LEAD ROLE )
* Nawab Shah - Rajeshwar ( LEAD ROLE )
* Zahida Parveen - Wife of Banarsi Babu ( Rakesh Bedi ) and lover of Pramod ( Tanveer Zaidi )
* Sneha Sangwan - Madhu,lover of Police Inspector Aryan Singh
* Mushtaq Khan - Police Constable
* Pinki Chinoi - Dancer

* Choreographer - Vicky Khan
* Stunt Director - Akbar Shareef
* Music Director - Amar-Akbar
* Director of Photography - Nazeer Khan
* Lyricist - Tauqeer Zaidi
* Story - Ayub M. Khan
* Producer - Dr.Anil K. Mehta
* Director - Ram Lakhan ( R. L. Mishra,associate to Gulzar )

==Story Background==
*The story revolves around the four bad guys; Nawab Shah,Tanveer Zaidi,Kshitij Dhankar,and Mahesh Desai who were involved in all types of crime in the sin city Mumbai, these four negative guys (BELAGAAM LADKE) were full of attitude, once they Kidnapped and raped a girl Sneha Sangwan, incidentally she died, they threw the body on a railway track. Sneha Sangwan was the girlfriend of a Police Inspector Aryan Singh, rest is the story of escape and revenge.There was another interesting sub plot in the movie,Rakesh Bedi is an impotent,his wife Zahida Parveen trap Tanveer Zaidi in her love with the permission of her husband,Zahida slept for a night with Tanveer Zaidi and later gives birth to a child.

==Premiere==
*The film premiered at Holland, Mumbai, Kanpur, Allahabad and Lucknow. The film was also critically acclaimed by the media. Belagaam received four stars by the TV and print media. The song "Tension Ko Thoker Maar" sung by Vinod Rathod, written by Tauqeer Zaidi and filmed on Tanveer Zaidi,Kshitij Dhankar, Mahesh Desai,and Nawab Shah was a chart buster on various T.V channels. Media reported that Kshitij Dhankar,Nawab Shah, Tanveer Zaidi, Aryan Singh were brilliant in their performances. Pramod Moutho. Rakesh Bedi, Zahida Parveen, Rana Jung Bahadur, Mushtaq Khan did their jobs very well. Kaleem Rahi was very good with his dialogue and screen play. Executive Producer and writer of the movie Ayub M. Khan received praises.
Satellite rights are with B4U movies, and Belagaam is a regular feature of this channel.
Video rights are with Captain Videos.

==References==
 

==External links ==
* 

 
 
 