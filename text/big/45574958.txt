Mistress and Maid (1910 film)
 
{{Infobox film
| name           = Mistress and Maid 
| image          = Mistress and Maid 1910 film.jpg
| caption        = A publicity film still for the film 
| director       =
| producer       = Thanhouser Company
| writer         = 
| narrator       =
| starring       = 
| music          =
| cinematography =
| editing        =
| distributor    = Motion Picture Distributing and Sales Company
| released       =  
| runtime        =
| country        = United States English inter-titles
}}
 silent short short drama Newfoundland dog is sent on a rescue message and delivers a message to the lifeguard. He arrives and rescues Nan from her jailer before hurrying back to the hotel, catching Susan in the act. Nan decides not to only dismiss instead of punish her maid because she her new-found love more than compensates the hardships she went through. The cast and credits for the film are unknown. The film was released on November 11, 1910, to mixed reviews in trade publications. The film is presumed lost film|lost.

== Plot == Newfoundland dog, who braves an angry sea, Nan sends a message to her lifeguard sweetheart, who comes instantly to her aid. After a spirited encounter with the keeper of the light, who is also Nans jailer, the young people make their escape in Jims boat. They reach the hotel in time to confront Susan, who cringes with fear when caught red-handed with her mistress belongings. Nan allows Susan to go unpunished, feeling that the happiness she has found in the proof of Jims unselfish love more than compensates her for all the hardships she has endured." 

== Production == George Middleton, Grace Moore, John W. Newfoundland dog who played the key role in foiling the plot and was promoted in advertising for the film. 

==Release and reception ==
The single reel drama, approximately 1,000 feet long, was released on November 1, 1910.    The film likely had a wide national release, theaters advertising the film are known in Pennsylvania,  Kansas,  Indiana,  and Montana.  In 1917, years after its widespread release, the Pennsylvania State Board of Censors of Moving Pictures would approve it for viewing without modification. 

Walton of The Moving Picture News praised the film, "The theme is not new, but the way it is handled, in this case, gives it a new interest. A film that grips and by skillful presentation must be popular. The lighthouse scenes and the dog won deserved applause."  The Moving Picture World was more specific in its praise. The reviewer stated, "There is life and animation enough to suit the most exacting, with good acting and clear photography as features of the picture."  The New York Dramatic Mirror contained a scathing review, "The young actress with the pretty face who played the part of the mistress in this impossible melodrama should learn to show us her back once in a while. Perhaps it is the director who is to blame; at any rate, this thing of constantly twisting the attitude so that one can face the front ruins the sense of reality that must be depended on to make motion picture acting effective, especially so in this film, which is so far-fetched in its melodramatic situations that no single element of the motion picture producing art should be dispensed with." 

== References ==
 

 
 
 
 
 
 
 
 