Magi (film)
 
{{Infobox film
| name           = Magi
| image          = 
| alt            =
| caption        = 
| film name      = 
| director       = Hasan Karacadağ
| producer       = Hasan Karacadağ
| writer         = Hasan Karacadağ
| screenplay     = 
| story          = 
| based on       =  
| starring       = Michael Madsen Stephen Baldwin Brianne Davis Dragan Micanovic Lucie Pohl Emine Meyrem Kenan Ece
| narrator       = 
| music          = 
| cinematography = Gabriel Campoy
| editing        = 
| studio         = Taff Pictures
| distributor    = Mars Dağıtım
| released       =  
| runtime        = 
| country        = Turkey
| language       = English
| budget         = 
| gross          =  
}} Turkish horror film directed and written by Hasan Karacadağ  and starring Lucie Pohl, Michael Madsen, Stephen Baldwin and Brianne Davis. 

==Plot==
Marla Watkins moved to Istanbul a few years ago. She now teaches English in a local language school. Her sister, Olivia, is a New York-based journalist. When informed about Marlas pregnancy, Olivia travels to Turkey to visit her sister. Marla lives in a fancy neighborhood. She was previously married to an Iranian artist, but the two have recently put an end to their relationship. Marla decided shed keep the baby and that is when sinister happenings began to occur.

== Cast ==
* Michael Madsen as Lawrence Irlam
* Stephen Baldwin as Burga   
* Brianne Davis as Marla Watkins
* Dragan Micanovic as Deyran  
*   as Olivia Watkins
* Emine Meyrem as Suzan
*   as Emir 

==References==
 

==External links==
*  

 
 
 
 
 

 
 