Revenge: A Love Story
 
 
{{Infobox film
| name           = Revenge: A Love Story
| image          = Revenge- A Love Story poster.jpg
| image size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Wong Ching-Po
| producer       = 
| writer         = 
| screenplay     = Wong Ching-Po
| story          = 
| based on       =  
| narrator       = 
| starring       = Juno Mak   Sola Aoi
| music          = Dan Findlay
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 111 minutes
| country        = Hong Kong
| language       = Cantonese
| budget         = 
| gross          = 
}}

Revenge: A Love Story ( ) is a 2010 Hong Kong film written and directed by Wong Ching-Po.  

==Cast==
* Juno Mak as Chan Kit (T: 陳 杰, P: Chén Jié, J: can4 git6)
* Sola Aoi as Cheung Wing (T: 張 穎, P: Zhāng Yǐng, J: zoeng1 wing6)
* Candy Cheung as Ling
* Siu-hou Chin
* Tony Ho
* Tony Liu

==Reception==

Catherine Shoard in The Guardian called it a baffling, grotesque horror that fails to validate its shocks and gave it one star. 

==References==
 

==External links==
 
*  

 

 
 
 


 