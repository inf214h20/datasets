Tradition Is a Temple
{{Infobox film
| name           = Tradition Is a Temple
| image          = Traditionisatemple-poster.jpg
| caption        = Theatrical poster
| director       = Darren Hoffman
| producer       = Darren Hoffman Kristen McEntyre Patrick Stafford
| writer         = Darren Hoffman
| narrator       = Chuck Perkins
| starring       = “The King of Treme” Shannon Powell Jason Marsalis Roland Guerin Topsy Chapman and Solid Harmony Lucien Barbarin Steve Masakowski Ed Petersen Chuck Perkins The Baby Boyz Brass Band The Treme Brass Band
| music          = 
| cinematography = James Laxton
| editing        = Darren Hoffman Benedict Kasulis Ryland Jones
| distributor    = Self Distributed
| released       =  
| runtime        = 96 minutes
| country        = United States
| language       = English
| budget         = Under United States dollar|US$ 100,000
| gross          = 
}}
Tradition Is a Temple is an American documentary film about New Orleans jazz culture and modernization’s effect on American traditions.

==Plot== New Orleans Jazz alive through the 70’s and 80’s. Asking the artists point blank, director Darren Hoffman explores the potential “death” of traditional jazz through modernization and marginalization and its preservation through mentorship and the continuation of traditions that intrigue and inspire young people to play the music of previous generations.

==Performances and Production==
In addition to in depth personal interviews, Tradition is a Temple is composed of various multi-camera professional studio recordings. The performances were recorded by Steve Reynolds (sound engineer) at the University of New Orleans School of Music. The production took place completely post-Hurricane Katrina beginning in August 2006.

==External links==
*  
*  

 
 
 
 
 
 

 