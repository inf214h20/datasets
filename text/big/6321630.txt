Only the Lonely (film)
{{Infobox film
| name           = Only the Lonely
| image          = Only the lonely ver1.jpg
| caption        = Theatrical release poster
| image_size     = 250px Chris Columbus John Hughes Hunt Lowry
| writer         = Chris Columbus
| starring       = John Candy Maureen OHara Ally Sheedy Anthony Quinn
| music          = Maurice Jarre
| cinematography = Julio Macat
| editing        = Raja Gosnell Peter Teschner
| distributor    = 20th Century Fox
| released       =  
| runtime        = 104 min.
| country        = United States
| language       = English
| budget         =
| gross          = $21,830,957 (USA)
}} romantic comedy-drama Chris Columbus and starring John Candy, Maureen OHara, Ally Sheedy and Anthony Quinn. The plot is similar to the earlier award-winning film Marty (film)|Marty.

==Plot== Irish mother, Sicilian and Polish American|Polish) only exacerbates the situation. 

Dannys brother Patrick (Kevin Dunn) tries to convince Danny to remain unmarried so that Danny and Rose can move down to Florida, where Danny can take care of her; Salvatore "Sal" Buonarte (James Belushi), one of Dannys married friends and fellow police officers, tries to tell Danny that he can do better and not to settle down just yet, as he did. Because of this, Danny begins to feel guilty about his relationship with Theresa, especially towards his mother. This leads to his interrupting dates with Theresa to check on his mother. 

When Theresa is finally introduced to Rose at a fancy dinner, Rose immediately begins to put her down. Theresa stands up to Rose, and complains to Danny as to why he didnt stand up for her. After Theresa leaves, Danny scolds his mother for being so cruel to Theresa, saying that her way of "telling it like it is" is really her attempts to hurt people. He then reminds her of how she had lost a $450,000 account for his late fathers company after calling his bosses racial slurs. Danny then tells Rose that he will propose to Theresa, whether she approves or not.
 Chicago fire truck. She says yes and the two are set to be married. However, even though Rose finally does approve of Theresa, on the night before the wedding, Danny calls to check on his mother in front of Theresa. Angered at the fact that they might never be alone, Theresa walks off. At the wedding, both Danny and Theresa fail to show up, thus, the two dont marry. A few weeks later, Dannys friends question what happened to make them not get married, but Danny avoids an answer. Then, when a friend of the family, Doyle (Milo OShea) passes away, alone with no wife or children, Danny realizes that he doesnt want to end up that way, and realizes that he cant let Theresa go.

Finally, the day Danny and Rose are scheduled to move to Florida, Danny tells Rose that he cant let Theresa go and by leaving her behind, hed be leaving behind the best thing that ever happened to him. Reluctant at first, Rose finally agrees to Dannys plan and goes to Florida without him, instructing her son to get married, have a family and be happy. Danny then goes to Lunas Funeral Home to look for Theresa.  However, her father tells him that she left for New York City by train. Danny contacts the railroad station manager, who agrees to stop the train at a suburban station outside the city. There, Danny apologizes to Theresa and proclaims his love for her. He tells her that he will move to New York with her and join the New York City Police Department. Having no more guilt about his mother, the two re-board the train for New York to live the rest of their lives together. 
 Greek neighbor, Nick Acropolis (Anthony Quinn), who encourages Danny to pursue Theresa, attempts to woo Rose. Rose is salty towards him in the beginning, but as she gradually softens her stance regarding Dannys relationship with Theresa, she ultimately warms up to Nick.

==Cast==
*John Candy as Officer Daniel "Danny" Muldoon, Chicago Police Department
*Maureen OHara as Rose Muldoon
*Ally Sheedy as Theresa Luna
*Anthony Quinn as Nick Acropolis
*James Belushi as Officer Salvatore "Sal" Buonarte, Chicago Police Department
*Kevin Dunn as Patrick Muldoon
*Macaulay Culkin as Billy Muldoon
*Kieran Culkin as Patrick Muldoon, Jr.
*Milo OShea as Doyle
*Bert Remsen as Spats
*Joe Greco as Joey Luna

==Production==

===Casting===
Chris Columbus wrote the part of Rose specifically for Maureen OHara, but did not know that she had retired from acting and was living in the Virgin Islands. Columbus contacted OHaras brother Charles B. Fitzsimons, a producer and actor in the film industry, to ask him to send OHara a copy of the script, which he did, telling her, "This you do!". OHara read the script and loved it. She was reported to have replied to Fitzsimons, "This I do!". However, she would not commit until she met co-star John Candy. The two formed an instant rapport and she quickly signed to do the film. 

Co-star Jim Belushi recounted this story: On the set of Only the Lonely, the producers stuck Maureen O’Hara in a tiny trailer. When John Candy complained on her behalf, he was told the budget was being spent on the picture, not on accommodations for old movie stars. Candy gave O’Hara his luxurious trailer and slept on a cot in cramped quarters for three days until the producers acquiesced.
 John Hughes co-produced the film. This movie marked Macaulay Culkins third film with Hughes and Candy (after Home Alone and Uncle Buck). Other than New Port South, it was the only film Hughes produced that he did not write.

===Filming=== Clark Street Gold Coast.
 Amtrak station in Niles, Michigan, which was renamed to Willoughby and decorated with Christmas lights for the filming.

===Music=== Someone Like You" by Van Morrison is played during one of Danny and Theresas dates. "Dreams to Remember" by Etta James is played, also in its entirety. Also, "Pachelbels Canon" is played briefly during the wedding scene.

==Reception==
Only the Lonely received mixed to positive reviews from critics and currently holds a 62% rating on Rotten Tomatoes based on 21 reviews.

==External links==
 
*  
*  
*  
*  
*  

 


 
 
 
 
 
 
 
 
 
 
 
 
 