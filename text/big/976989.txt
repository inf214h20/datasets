The Doom Generation
{{Infobox film
| name           = The Doom Generation
| image          = Doom generation.jpg
| image_size     = 215px
| alt            = 
| caption        = Theatrical release poster
| director       = Gregg Araki
| producer       = Gregg Araki Andrea Sperling
| writer         = Gregg Araki
| starring       = Rose McGowan James Duval Johnathon Schaech
| music          = Dan Gatto
| cinematography = Jim Fealy
| editing        = Gregg Araki Kate McGowan Union Générale Cinématographique
| distributor    = Trimark Pictures   Haut et Court  
| released       =  
| runtime        = 83 minutes  
| country        = United States France
| language       = English
| budget         =
| gross          = $284,785 
}} comedy Thriller thriller film punk drifter who become involved in a ménage à trois.

The film is the second of a trilogy of films known as the Teenage Apocalypse Trilogy, the first being Totally Fucked Up and the last Nowhere (film)|Nowhere. It is billed in the opening titles as "A Heterosexual Movie by Gregg Araki".

==Plot== disemboweled her children with a machete before committing suicide, thus, he concludes, removing any possibility of the trio being caught by the police.

Later that evening, Amy has sex with X, even though they do not get along. Eventually Jordan finds out, and things become tense as the two men develop a lingering sexual attraction for one another. As the trio journeys around the city of Los Angeles, they continue to get into violent situations due to people either claiming to be Amys previous lovers or mistaking her for such. The FBI has a meeting and declares it will find Amy and kill her (exactly the same sentiment is voiced by several other parties in the film). She is mistakenly identified by a fast food window clerk as "Sunshine" and later by a character played by Parker Posey as "Kitten".

Jordan, Amy and X spend the night in an abandoned warehouse, where they engage in a threesome. While Amy goes to urinate, Jordan and X are attacked by a trio of neo-Nazis, one of whom had previously mistaken Amy for his ex-girlfriend "Bambi". The gang proceeds to beat up X and then hold Jordan down as the aforementioned neo-Nazi ties up and rapes Amy on top of an American flag. The group finally severs Jordans penis with pruning shears and forces it into his mouth. After Amy breaks free, she kills the neo-Nazis with the shears and escapes with X, leaving Jordan for dead. The film ends with Amy and X driving aimlessly on the road with no communication as the film fades.

==Cast==
* Rose McGowan as Amy Blue
* James Duval as Jordan White
* Jonathan Schaech as Xavier "X" Red
* Dustin Nguyen as Win Coc Suc, the convenience store clerk
* Margaret Cho as Wins wife
* Parker Posey as Brandi Christopher Knight as TV anchorpeople
* Nicky Katt as Bartholomew, Carnoburger cashier
* Amanda Bearse as Barmaid
* Cress Williams as Peanut
* Skinny Puppy as Gang of goons
* Perry Farrell as Stop n Go clerk
* Heidi Fleiss as Liquor store clerk Khristofor Rossianov as Don

==Response==
The Doom Generation received mixed reviews, with critics often comparing the film both favorably and unfavorably to Oliver Stones Natural Born Killers. Film website Rotten Tomatoes, which compiles reviews from a wide range of critics, gives the film a score of 47% based on 34 reviews.  Roger Ebert famously gave the film "zero stars".  Ricky da Conceição of Sound on Sight named the film the best of Arakis "Teenage Apocalypse Trilogy" and said it "represented a major artistic leap forward" for Araki, who "creates a twisted pastiche of science fiction, nihilistic road movie and teen angst filtered with dead pan comedy and his own unique commentary on the depravity of modern America." He praised the set design, lighting, score and actress Rose McGowan, who "steals the show as the foul mouthed, morally aimless femme fatale on crystal meth and Diet Coke." 

==Home media==
In March 2012, the UK company Second Sight Films released a DVD with amamorphic widescreen and director/cast commentary.  Previous releases up until this point lacked the commentary, with many lacking the widescreen format.

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 