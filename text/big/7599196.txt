The Ghost Talks (1929 film)
 
{{Infobox film
| name           = The Ghost Talks
| image          =
| caption        =
| director       = Lewis Seiler
| producer       =
| writer         =
| starring       = Helen Twelvetrees Carmel Myers
| music          =
| cinematography = George Meehan
| editing        =
| distributor    = Fox Film Corporation
| released       =  
| runtime        =
| country        = United States
| language       = English
}}

The Ghost Talks is a 1929 comedy genre film, directed by Lewis Seiler; based on a Max Marcin and Edward Hammonds Broadway play.

According to the New York Times review, this was the second all-talking feature from Fox, featuring various plot devices, like a lisping heroine, to show off the technology. This film is regarded as lost.

==Cast==
* Helen Twelvetrees Charles Eaton
* Carmel Myers
* Stepin Fetchit
* Earle Foxe
* Henry Sedley Joe Brown
* Clifford Dempsey
* Baby Mack
* Arnold Lucy
* Bess Flowers
* Dorothy McGowan
* Mickey Bennett

==External links==
* 
* 

 

 
 
 
 
 
 
 
 
 


 