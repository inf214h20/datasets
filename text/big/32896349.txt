Hills of Hate
 
{{Infobox film
| name           = Hills of Hate
| image          = 
| image_size     =
| caption        = 
| director       = Raymond Longford
| producer       = 
| writer         = E. V. Timms
| based on       = novel by E. V. Timms
| narrator       = Dorothy Gordon
| music          =
| cinematography = Arthur Higgins
| editing        = 
| studio         = Australasian Films A Master Picture
| distributor    = 
| released       = 27 November 1926 
| runtime        = 6,000 feet
| country        = Australia
| language       = Silent film  English intertitles
| budget         = 
| preceded_by    = 
| followed_by    = 
}}

Hills of Hate is a 1926 Australian silent film directed by Raymond Longford, based on the debut novel by E. V. Timms, who also did the screenplay. It is considered a lost film.

==Synopsis== Dorothy Gordon). Matters are complicated by Sam Ridgeways villainous overseer, Cummins (Big Bill Wilson).

==Cast==
*Dorothy Gordon as Ellen Ridgeway
*Gordon Collingridge as Jim Blake
*Big Bill Wilson as Black Joe Cummins
*Clifford Toone as Jim Blake Snr
*Kathleen Wilson as Peggy Blake
*Stanley Lonsdale as Stanley Ridgeway

==Original Novel==
E. V. Timms original novel was published in 1925.

==Production==
Shooting began in March 1926 and went for around five weeks, mostly on location in Gloucester, New South Wales. 
 Dorothy Gordon, For the Term of His Natural Life (1927).  She and later became a radio commentator and newspaper columnist under the name of Andrea.  
 Tall Timber (1927). 

Raymond Longfords son Victor served as associate producer. "Raymond Longford", Cinema Papers, January 1974 p51 

==Reception==
The Northern Times said Collingridge played his role "with a skill remarkable in such a young actor, whilst Dorothy Gordons portrayal is a powerfully competing proof of her ability." 

The film was not a success at the box office – although it was screening in cinemas as late as 1933  –  and it was several years before Longford managed to direct another feature, The Man They Could Not Hang (1934). This turned out to be his last movie as director.

==References==
 

==External links==
* 
*  at National Film and Sound Archive
*  at AustLit
*  at AustLit
*  at Australian Womans Weekly in 1936
*Serialised version of story from 1927 –  ,  ,  ,  ,  ,  ,  ,  ,  
 
 

 
 
 
 
 
 