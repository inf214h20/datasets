Dick Barton: Special Agent
 
 
{{Infobox film
| name           = Dick Barton: Special Agent
| image          = 
| caption        = 
| director       = Alfred J. Goulding
| producer       = Henry Halstead
| writer         = Alfred J. Goulding Alan Stranks
| based on = the BBC radio serial
| starring       = Don Stannard George Ford
| music          = 
| cinematography = Stanley Clinton
| editing        = Eta Simpson
| studio = Hammer Film Productions
| distributor    = Exclusive Films
| released       = 10 May 1948
| runtime        = 71 mins
| country        = UK
| language       = 
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
Dick Barton: Special Agent is a 1948 British film about special agent Dick Barton. It was the first of three films that Hammer Film Productions made about the British agent, followed by Dick Barton at Bay and Dick Barton Strikes Back. 

==Plot==
Dick Barton (Don Stannard) and his friends Snowey and Jack are investigating smuggling when attempts are made on his life. It turns out there is a neo-Nazi plot to contaminate Great Britains water supply...

==Cast==
*Don Stannard as Dick Barton
*George Ford as Snowey
*Jack Shaw as Jack
*Gillian Maude as Jean
*Beatrice Kane as Mrs Horrock
*Ivor Danvers as Snub
*Geoffrey Wincott as Dr Caspar
*Arthur Bush as Schuler
*Alec Ross as Tony

==Release==
The films success prompted Hammer to make a number of movies based on radio and/or TV shows. It was released in the USA as Dick Barton, Detective. 

==Reception==
Dick Barton: Special Agent  currently holds an average two and a half star rating (4.8/10) on IMDb.

==References==
 

==External links==
* 
 

 
 
 