Speak to Me of Love
 
 
{{Infobox film  
| name           = Speak to Me of Love
| image          = Parlez-moi damour.jpg
| border         = yes
| caption        = Theatrical release poster
| producer       = Alain Sarde
| director       = Sophie Marceau
| writer         = Sophie Marceau
| starring       =  
| music          = Éric Neveux
| cinematography = Emmanuel Machuel
| editing        = Claudine Merlin
| distributor    = Mars Distribution
| released       = 29 August 2002
| runtime        = 98 minutes
| country        = France
| language       = French
}}
Speak to Me of Love ( ) is a 2002 French drama film written and directed by Sophie Marceau and starring Judith Godrèche, Niels Arestrup, and Anne Le Ny. The first feature-length motion picture directed by actress Sophie Marceau, the film is about the breakup of a long-term relationship.    Speak to Me of Love was filmed on location in New York City and Paris.    In 2002, the film received the Montréal World Film Festival Award for Best Director (Sophie Marceau) and was nominated for the Grand Prix des Amériques.   

==Plot==
Justine and Richards fifteen-year relationship ends in separation due to irreconcilable differences with Justine maintaining custody of their three boys. Her new life means having to deal with being a single parent but at the same time, she comes to terms with her own parents divorce and finds a common bond with her long-suffering mother. Richard, a renowned author, deals with the situation by devoting all his attention to his writing. Both are forced to confront their uncertain futures, while examining what led to the breakdown of their marriage.

==Cast==
* Judith Godrèche as Justine
* Niels Arestrup as Richard
* Anne Le Ny as Amélie
* Laurence Février as Justines mother
* Jean-Marie Frin as Justines father
* Aurélien Wiik as William
* Daniel Isoppo as Hubert
* Christelle Tual as Josée
* Chantal Banlier as Christine
* Isabelle Olive as Carole
* Jimmy Baudrand as Constantin
* Louis-Alexandre Lucotte as Jérémy
* Jules Boudier as Jacob
* François Chattot as French producer
* Ariane Seguillon as Corinne
* Fleur Michels as Traffic warden
* Léa Unglick as Justine (5 years old)
* Lilly-Fleur Pointeaux as Justine (9–12 years old)
* Yoann Denaive as Friend suburb
* Nastasia Demont as Clémentine (8 years old)
* James Gorter as Clément (8 years old)
* Annelise Hesme as Elsa
* Enric Arquimbau as Opera manager
* Christine Campion as Opera managers assistant
* Jacques Le Forestier as Delivery man
* Oury Milshtein as A man in the office
* Julien Marion as Justines brother (14 years old)   

==Awards and nominations==
* 2002 Montreal World Film Festival Award for Best Director (Sophie Marceau)
* 2002 Montreal World Film Festival Award Nomination for Grand Prix des Amériques (Sophie Marceau) 

==References==
 

==External links==
*  

 

 
 
 
 
 
 


 