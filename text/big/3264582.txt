Evil (2003 film)
 
{{Infobox film
| name           = Evil
| image          = Evil 2003.jpg
| caption        = Region 1 DVD cover
| director       = Mikael Håfström
| producer       = Ingemar Leijonborg Hans Lönnerheden
| screenplay     = Hans Gunnarsson Mikael Håfström
| based on       =  
| starring       = Andreas Wilson Henrik Lundström Gustaf Skarsgård
| music          = Francis Shaw
| cinematography = Peter Mokrosinski
| editing        = Darek Hodor
| studio         = Moviola Columbia TriStar
| released       =  
| runtime        = 113 minutes
| country        = Sweden
| language       = Swedish
| budget         = $3,000,000
| gross          = $12,469,000
}} novel with the same name  from 1981, and starring Andreas Wilson, Henrik Lundström and Gustaf Skarsgård. The film is set in a private boarding school in the 1950s with institutional violence as its theme.
 Best Film.

==Plot==
Erik Ponti, a 15 year old boy, lives with his mother and sadistic stepfather in Stockholm. At home, his stepfather beats him every day after dinner. His docile mother ignores her husbands sadistic nature and allows the violence to proceed. At school, Erik is violent and frequently engages in fights, as a result of his violent upbringing. After a particularly vicious fight, Erik is expelled. The headmaster labels him vicious and accuses him of being pure evil. In an attempt to provide her son with a fresh start he sorely needs, his mother sells of some of her possessions and sends Erik to a boarding school.

Upon arriving in Stjärnsberg, realising the boarding school is his last chance at reaching Sixth Form, Erik attempts to forgo his violent tendencies. At the prestigious school twelve members of the Sixth Form form a Students Council. They exercise a sadistic rule over the school and punish disobeying students physically and psychologically. When Erik refuses to obey the ludicrous requests of two councilmen, Silverhielm and Dahlen, he becomes the target of relentless bullying. His refusal to demean himself by obeying their humiliating punishments lands him a number of weekend detentions. Erik befriends his intellectual roommate Pierre, who flies below the radar in order to avoid bullying.

Whilst in the school kitchens one weekend, after a particularly grueling day of labour, Erik meets Marja, a pretty cafeteria worker. The two begin a romantic relationship as Marja admires Eriks resistant and just personality. Meanwhile, Erik joins the swimming team. A swimming match approaches and Erik is determined to win, but soon realises that in order to win he must defeat the current school champion and son of a donor to the school. He knows that winning would make him more of a target than ever, but his fair and dedicated swim coach assures him that it is a matter of honour and he must not lose. Erik wins, breaking a number of school records and humiliating a number of Sixth Formers.

For Christmas break Erik goes home. His stepfather beats him mercilessly, whilst his mother plays the piano to disguise the sounds of the cane. When he arrives back at school, the student council begins targeting Eriks intellectual friend Pierre. Pierre does not stand up for himself. Pained to see his friend humiliated Erik leaves the swim team. But that doesnt seem to be enough. A while later, Erik is called up to the council president, Silverhielms room. There Pierre has been made to strip and Dalen threatens to put out a cigarette on his chest, but Erik volunteers instead and unflinchingly endures the pain. The next day, Pierre is challenged to fight the councilmen. He gets beaten up but does not obey their requests.

The following day, Erik is ambushed when walking back from detention. They tie him to the ground and pour boiling water over him followed by cold water and leave him outside to freeze. However, he is rescued by Marja. The two sleep together and Erik returns to his room to find Pierre has left the school. Erik, bitter and fed up, challenges Dahlen and von Schenken to a fight. He quickly defeats both, and then goes in search of Marja, who has left for Finland after being fired.

The headmaster intercepts a letter from Marja to Erik and expels Erik. Erik finds Silverhielm in the woods and threatens to kill him. As Silverhielm begs for his life on his knees Erik assures him he wont kill him because he is not like him. Erik returns to the school with a lawyer and threatens to expose the sadistic measures of discipline and loose law on bullying in the school. He is reinstated. The school year ends and Erik returns home to find his mother has been beaten by his stepfather. His stepfather tries to beat him again, but Erik warns him that it is over. He tells his mother that it is the last time there will be violence in the household and closes the door behind himself as he prepares to get payback for years of violence. He reconciles with Pierre and sets out on realising his dream of becoming a lawyer.

==Cast==
*Andreas Wilson as Erik Ponti
*Henrik Lundström as Pierre Tanguy
*Gustaf Skarsgård as Otto Silverhielm
*Linda Zilliacus as Marja
*Jesper Salén as Gustaf Dalén
*Peter Eggers as Karl von Rosen
*Filip Berg as Johan
*Johan Rabaeus as Eriks stepfather
*Marie Richardson as Eriks mother
*Magnus Roosmann as Tosse Berg, gymnastic teacher
*Ulf Friberg as Tranströmer, biology teacher
*Lennart Hjulström as Lindblad, headmaster of Stjärnsberg
*Mats Bergman as Melander, history teacher
*Kjell Bergqvist as Gunnar Ekengren, lawyer
*Björn Granath as headmaster
*Fredrik af Trampe as von Schenken
*Petter Darin as von Seth

==Production==
Being based on one of the best selling Swedish books in modern time, there had been discussions about a film adaptions for many years, including several failed attempts, before Mikael Håfström was offered to direct the material. It was originally conceived as a television series, but Håfström felt he was not ready for such an extensive production. Instead he waited a few years and eventually convinced the producers to make it a feature film. Håfström brought in Hans Gunnarsson, with whom he had worked before, as a co-writer, and the writing process as well as the financing went ahead quickly. Carnevale, Rob (2005-11-30) " ." Indie London. Retrieved on 2009-07-14.  The budget was 20 million Swedish kronor. 

The casting of the supporting actors proceeded without any significant difficulties, but despite auditions for over 120 applicants, a lead actor had still not been found when only two weeks remained before filming was intended to begin.  Finally the director recalled Andreas Wilson, a young male model with very limited acting experience whom he had previously met briefly at a birthday party. Håfström contacted people he knew had been at the party to get Wilsons phone number, after which he called him and asked him to come over. Håfström was immediately sure that he had found the right person, and after a few physical tests, Wilson was given the role.  Before filming started Wilson had swimming training to learn how to  . Retrieved on 2009-07-14. 

  north of Stockholm served as the school buildings exterior.]]

Filming took 30 days to finish, from early October to November 2002.    The school Stjärnsberg in the film is based on Solbacka boarding school, a real school attended by the author of the novel. The school was closed in 1973.  The original building of Solbacka still exists as a recreation centre for golfers, but had been renovated to such a degree that the director felt it could not be used as a believable filming location.  Instead most exterior shots were made around Görvälns slott in Jakobsberg, north of Stockholm.  The dining area in the film was built in a studio as a replica of the original schools dining hall, based on photographs from the time.  The pool scenes were shot at the school Gubbängsskolan in southern Stockholm. 
 Peggy Sue" Buddy Holly & The Crickets. 

==Release== Toronto International Film Festival in September the same year.  The Swedish premiere followed on 26 September. Evil became a huge commercial success in Sweden with 959,223 admissions in total.  On 24 June 2005 it was released in the United Kingdom, and on 10 March 2006 as a limited release in the United States.

==Accolades== Best Direction, Best Actor Best Supporting Actor.  

==References==
 

==External links==
* 
*  
* 

 
 
 

 
 
 
 
 
 
 
 
 
 