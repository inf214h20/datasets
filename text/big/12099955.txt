Filth and Wisdom
{{Infobox film
| name           = Filth and Wisdom
| image          = Filth and Wisdom Poster.jpeg
| producer       = Nicola Doring Madonna
| writer         = Madonna Dan Cadan
| starring       = Eugene Hütz Holly Weston Vicky McClure Richard E. Grant Olegar Fedoro
| music          = 
| cinematography = Tim Maurice Jones
| editing        = Russell Icke Semtex Films, HSI London
| distributor    = IFC Films
| released       =  
| runtime        = 81 minutes
| country        = United Kingdom|
| language       = English Russian
| budget         = 
| gross          = $354,628 
}}
Filth and Wisdom is a 2008 film directed by Madonna (entertainer)|Madonna, starring Eugene Hütz, Holly Weston, Vicky McClure and Richard E. Grant. It was filmed on location in London|London, England, from 14 to 29 May 2007. Locations included two actual strip clubs in Hammersmith and Swiss Cottage; both owned by the   chain. Additional scenes were shot in July 2007.
 Semtex Films.

==Plot==
Described as a comedy/drama/musical/romance, the story revolves around a Ukrainian immigrant named A.K. (Hütz) who finances his dreams of rock glory by moonlighting as a cross-dressing dominatrix and his two female flatmates: Holly (Weston), a ballet dancer who works as a stripper and pole-dancer at a local club and Juliette (McClure), a pharmacy assistant who dreams of going to Africa to help starving children.

The Gypsy punk band that appears in the film is portrayed by real-life Gypsy punk band, Gogol Bordello, who also contributed three songs to the films soundtrack. The bands lead singer, Eugene Hütz, portrays the main character – a character with a philosophical attitude towards life. Madonna allowed additional dialogue written by Hütz himself to be included in the film.

==Reception== The Telegraph described the film as "not an entirely unpromising first effort" but went on to say "Madonna would do well to hang on to her day job."  Peter Bradshaw of The Guardian wrote, "Well, it had to happen. Madonna has been a terrible actor in many, many films and now – fiercely aspirational as ever – she has graduated to being a terrible director."  Jonathan Romney of Screen International called the film "a good-humoured, averagely average vanity project" and "a cheap and cheerful comedy," adding that "Madonna simply cannot direct actors."  The New Yorker s Anthony Lane panned the film, saying that "in technical terms, more professional productions than this are filmed and cut on iMovie, by ten-year-olds, a thousand times a day" and that "if the actors were paid according to their talents, they cannot have cost more than forty bucks." 

==Cast== Ade – DJ
*Olegar Fedoro – A.K.s Father
*Eugene Hütz – A.K.
*Holly Weston – Holly
*Vicky McClure – Juliette
*Richard E. Grant – Professor Flynn Stephen Graham – Harry Beechman
*Inder Manocha – Sardeep
*Shobu Kapoor – Sardeeps wife
*Elliot Levey – Businessman
*Clare Wilkie – Chloe
*Hannah Walters – Businessmans wife

==Production credits==
*Directed by: Madonna
*Produced by: Nicola Doring
*Executive Producer: Madonna
*Screenplay: Madonna & Dan Cadan
*Director of Photography: Tim Maurice Jones
*Editor: Russell Icke
*Production Designer: Gideon Ponte
*Costume Designer: B
*Running Time: 81 minutes
*Format: 35&nbsp;mm, 1:1.85 aspect ratio Semtex Films (in association with HSI London)

==References==
 

==External links==
 
 
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 

 