The Babadook
 
{{Infobox film
| name           = The Babadook
| image          = The-Babadook-Poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Jennifer Kent
| producer       = Kristina Ceyton Kristian Moliere
| writer         = Jennifer Kent
| starring       = Essie Davis Noah Wiseman Daniel Henshall Hayley McElhinney Barbara West Ben Winspear
| music          = Jed Kurzel
| cinematography = Radek Ladczuk
| editing        = Simon Njoo
| studio         = Causeway Films
| distributor    = Cinetic Media eOne Films International IFC Films
| released       =  
| runtime        = 94 minutes 
| country        = Australia
| language       = English
| budget         = $2 million
| gross          = $4.9 million 
}}

The Babadook is a 2014 Australian psychological horror film, written and directed by Jennifer Kent, in which a woman and her son are tormented by an evil entity. The film stars Essie Davis and Noah Wiseman, while Daniel Henshall, Hayley McElhinney, Barbara West, and Ben Winspear appear in supporting roles. The Babadook was produced by Causeway Films and is based on the short film Monster (2005), also written and directed by Kent. The film was shown at the 2014 Sundance Film Festival and received widespread acclaim.  

==Plot==
Amelia, a widowed orderly, has raised her 6 year old son Samuel alone after her husband Oskar was decapitated in an accident driving Amelia (who was in labour) to the hospital. Sam begins displaying erratic behavior: he rarely sleeps through the night and is preoccupied with an imaginary monster, which he has built weapons to fight. Amelia is forced to take her son out of school due to his behavioural problems.

One night, Sam asks his mother to read from a mysterious Pop-up book|pop-up storybook he finds on his shelf. The story, Mister Babadook, is about a supernatural creature—once someone is made aware of its existence, the monster torments that person indefinitely. Amelia is disturbed by the books contents, while a traumatised Sam becomes convinced that the Babadook is stalking them in their home.

Strange events begin occurring throughout the house: doors open and close on their own, Amelia finds glass in her food, and strange sounds are heard with no apparent source. Amelia attributes the events to Sams behaviour, but he insists that the Babadook is responsible. Amelia rips up the Mister Babadook book and disposes of it.

At his cousin Ruby’s birthday party, Sam pushes Ruby out of a treehouse and breaks her nose, after she refuses to believe in the Babadook and bullies Samuel for not having a father. Amelia’s sister, Claire, admits she cannot bear to be around Sam, and suspects that Amelia feels the same way. On the drive home from the party, Sam has another vision of the Babadook and suffers a febrile seizure.

Amelia convinces a doctor to prescribe sedatives for Sam to help him sleep, hoping that she will also be able to sleep as a result. The following morning, Amelia hears knocking at the door and finds the Mister Babadook storybook, reassembled, on the front step. The book taunts Amelia, claiming that the more she denies the Babadooks existence, the stronger the monster will get, growing inside her. The book contains new popups of Amelia strangling her dog, killing Sam, and slitting her own throat. Terrified, Amelia burns the book on an outdoor barbecue grill. After receiving an eerie phone call, Amelia tries to convince the police that she is being harassed by a stalker, but  when the police officer asks for the storybook she replies that she has burned it. This, combined with Chalk on Amelias fingers, leads the police officer to suspect that she is the culprit, and notify child protective services, who threaten to remove her custody of Sam.

Amelia begins to see the Babadook wherever she goes, and does not sleep for several consecutive nights. With her mental health deteriorating, Amelia becomes hostile and shows signs that she intends to harm Sam. After Sam phones a neighbour for help, Amelia becomes enraged, cutting the phone lines. She then encounters an apparition of Oskar in the cellar, who asks her to bring Sam to him. After Amelia refuses and runs away from the cellar, the Babadook materializes and gives chase to her, revealing its true and horrifying form; it then attacks and possesses Amelia.  In a fit of madness she kills the family dog and pulls out one of her teeth. During an altercation, Sam stabs Amelia in her leg and lures her back into the cellar, which he has booby trapped to combat the Babadook. Sam manages to incapacitate his mother, and pleads with her to fight the monster inside her. While nearly choking Sam, Amelia momentarily comes to her senses when Sam tenderly caresses her face with love and with a great force of effort, she vomits out the malevolent entity onto the floor of the cellar. Sam then reiterates that "You cant get rid of the Babadook", and is dragged upstairs by an unseen force. In a final confrontation, Amelia fearlessly stakes their claim on the house and subsequently subdues the Babadook until it retreats into the cellar.

After these events, Amelia and Sam celebrate Sams birthday in their backyard, and they spend time together as a normal and happy family. Amelia tells Sam to stay outside while she goes inside and feeds worms from the garden to the Babadook, who now lives in their cellar.

==Cast==
*Essie Davis as Amelia Vannick
*Noah Wiseman as Samuel Vannick
*Daniel Henshall as Robbie
*Hayley McElhinney as Claire
*Barbara West as Mrs. Roach
*Benjamin Winspear as Oskar
*Cathy Adamek as Prue
*Craig Behenna as Warren
*Adam Morgan as Sergeant
*Peta Shannon as Mother #2
*Hachi as Bugsy

==Production==

===Development===
Kent studied at the National Institute of Dramatic Art (NIDA)—where she learned acting alongside Davis—and graduated in 1991.  She then worked primarily as an actor in the film industry for over two decades. Kent eventually lost her passion for acting by the end of the 1990s and sent a written proposal to Danish filmmaker Lars von Trier, asking if she could assist on the film set of von Triers 2003 drama film, Dogville, to learn from the director. Kents proposal was accepted and she considers the experience her film school, citing the importance of stubbornness as the key lesson she learned.    

Prior to Babadook, Kents first feature film, she had completed a short film, titled Monster, and an episode of the television series Two Twisted. Kent explained in May 2014 that the origins of Babadook can be found in Monster, which she calls "baby Babadook".   

The writing of the screenplay began in around 2009 and Kent has stated that she sought to tell a story about facing up to the darkness with ourselves, the "fear of going mad" and an exploration of parenting from a "real perspective". In regard to parenting, Kent further explained in October 2014: "Now, I’m not saying we all want to go and kill our kids, but a lot of women struggle. And it is a very taboo subject, to say that motherhood is anything but a perfect experience for women."    In terms of the characters, Kent said that it was important that both characters are loving and lovable, so that "we   really feel for them"—Kent wanted to portray human relationships in a positive light.  In total, Kent completed five drafts of the script.   

Kent drew from her experience on the set of Dogville for the assembling of her production team, as she observed that von Triers was surrounded by a well-known "family of people". Therefore, Kent sought her own "family of collaborators to work with for the long term." Unable to find all of the suitable people within the Australian film industry, Kent hired Polish director of photography (DOP) Radek Ladczuk, for whom Babadook was his first-ever English-language film, and American illustrator Alexander Juhasz.  In terms of film influences, Kent cited 1970s and 80s horror—including The Thing (1982 film)|The Thing, Halloween (1978 film)|Halloween, Eyes Without a Face|Les Yeux Sans Visage, The Texas Chain Saw Massacre|The Texas Chainsaw Massacre, The Shining (film)|The Shining and Let the Right One In (film)|Let The Right One In—as well as Vampyr and Nosferatu. 

Although the process was challenging and she was forced to reduce her total budget,    Kent managed to secure funding of around A$2.5 million from government bodies Screen Australia and the South Australian Film Corporation|SAFC; however, she still required an additional budget for the construction of the film sets. To attain the funds for the sets, Kent and Causeway Films producer Kristina Ceyton launched a Kickstarter crowdfunding campaign in June 2012, with a target of US$30,000. Their funding goal was reached on 27 September 2012 through pledges from 259 backers raising $30,071.  

===Filming===
The film was primarily shot in Adelaide, South Australia, with most of the interior shots filmed on a soundstage in the Australian city—as the funding was from the South Australian state government, this was a requirement that Kent needed to meet.  However, Kent explained to the Den Of Geek website that she is not patriotic and didnt want the film to be "particularly Australian":

 
I wanted to create a myth in a domestic setting. And even though it happened to be in some strange suburb in Australia somewhere, it could have been anywhere. I guess part of that is creating a world that wasnt particularly Australian ... Im very happy, actually, that it doesnt feel particularly Australian.  

To contribute to the universality of the films appearance, a Victorian terrace-style house was specifically built for the film, as there are very few houses designed in such a style in Adelaide.  A script reading was not done due to Noah Wisemans age at the time—six years old—and Kent focused on bonding, playing games, and lots of time spent with the actors in which they became more familiar with one another. Pre-production occurred in Adelaide and lasted three weeks and, during this time, Kent conveyed a "kiddie" version of the narrative to Wiseman. 

Kent originally wanted to film solely in black-and-white, as she wanted to create a "heightened feel" that is still believable. She was also influenced by pre-1950s B-grade horror films, as it was "very theatrical", in addition to being "visually beautiful and terrifying". Kent later lost interest in the black-and-white idea and worked closely with production designer Alex Holmes and Radek to create a "very cool", "very claustrophobic" interior environment with "meticulously designed" sets.   The films final colour scheme was achieved without the use of gels on the camera lenses or any alterations during the post-filming stage.  Kent cited filmmakers David Lynch and Roman Polanski as key influences during the filming stage. 

Kent described the filming process as "stressful" because of Wisemans age. Kent explained "So I really had to be focused. We needed double the time we had." Wisemans mother was on set and a "very protective, loving environment" was created.  Kent explained after the release of the film that Wiseman was protected throughout the entire project: "During the reverse shots where Amelia was abusing Sam verbally, we had Essie   yell at an adult stand-in on his knees. I didnt want to destroy a childhood to make this film—that wouldnt be fair."  Kents friendship with Davis was a boon during filming and Kent praised her former classmate in the media: "To her credit, shes   very receptive, likes to be directed and is a joy to work with." 

In terms of the Babadook monster and the scary effects of the film, Kent was adamant from the outset of production that a low-fi and handmade approach would be used. She cites the influence of Georges Méliès, Jean Epstein’s The Fall of the House of Usher (1928 French film)|The Fall of the House of Usher and Hexen.  Kent used stop-motion effects for the monster and a large amount of smoothening was completed in post-production. Kent explained to the Empire publication: "There’s been some criticism of the lo-fi approach of the effects, and that makes me laugh because it was always intentional. I wanted the film to be all in camera."   

==Release==
The films global premiere was in January 2014 at the Sundance Film Festival. The film then received a limited theatrical release in Australia in May 2014,  followed by a screening in April 2014 at the Stanley Film Festival. 

In Singapore, the film was released on 25 September 2014.    The film opened in the United Kingdom for general release on 17 October 2014, and in the United States on 28 November 2014. 

The U.S. Blu-ray and DVD release date is scheduled for 14 April 2015, and the special edition will also be available on that date.     The special edition features Kents short film, Monster, and the comic novel, Creating The Book, by Juhasz. {{cite news|author1=John Lui|title=
‘The Babadook’ Gets Special Edition Release!|url=http://bloody-disgusting.com/home-video/3330205/babadook-gets-collectors-edition-release/|accessdate=21 November 2014|work=The Strait Times|date=24 September 2014}}  The UK Blu-ray Disc features the short documentary films "Illustrating Evil: Creating the Book", "There’s No Place Like Home: Creating the House" and "Special Effects: The Stabbing Scene". {{cite news|author1=John Lui|title=
‘The Babadook’ Gets Special Edition Release!|url=http://www.dreadcentral.com/news/90336/riddle-teases-hidden-content-uk-home-video-release-babadook/|accessdate=21 November 2014|work=The Strait Times|date=24 September 2014}} 

==Reception==

===Box office===
The Babadook opened in North America on a limited release basis in three theaters and grossed US$30,007, with an average of $10,002 per theater. The film ranked in the 42nd position at the box office, and, as of 1 February 2015, has grossed $913,720 in the U.S. and $3,917,000 internationally. To date, the films worldwide box office takings is $4,830,720, which is more than double the estimated production budget of $2 million. 

===Critical response===
The Babadook received acclaim from critics and has a "Certified Fresh" score of 98% on  , based on 32 critics indicating "universal acclaim" 

Dan Schindel from Movie Mezzanine said that "The Babadook is the best genre creature creation since the big black wolf-dog aliens from   gave The Babadook 9 out of 10. 
 The Exorcist) stated on his Twitter profile, "Psycho (1960 film)|Psycho, Alien (film)|Alien, Les Diaboliques (film)|Diabolique, and now THE BABADOOK."  Friedkin also added, "Ive never seen a more terrifying film. It will scare the hell out of you as it did me."  Prominent British film critic Mark Kermode named The Babadook his favourite film of 2014. 

==Symbolism==
Writing for the Daily Beast, Tim Teeman contends that grief is the "real monster" in The Babadook, and that the film is "about the aftermath of death; how its remnants destroy long after the dead body has been buried or burned". Teeman writes that he was "gripped" by the "metaphorical imperative" of Kents film, with the Babadook monster representing "the shape of grief: all-enveloping, shape-shifting, black". Teeman states that the films ending "underscored the thrum of grief and loss at the movie’s heart", and concludes that it informs the audience that grief has its place and the best that humans can do is "marshal it". 

Egyptian national film critic Wael Khairy wrote in his "Film Analysis" on 22 November 2014 that The Babadook "taps into something real, a real human fear".    Khairy argues that what the Babadook "stands for is up for debate", but writes:

 
The malevolent Babadook is basically a physicalized form of the mother’s trauma ... I believe, the Babadook embodies the destructive power of grief. Throughout the film, we see the mother insist nobody bring up her husband’s name. She basically lives in denial. Amelia has repressed grief for years, refusing to surrender to it.  

Khairy concluded that the film is "based on something very real" and "feels unusually beautiful and even therapeutic." 

An analysis by Syvology on the MIshka NYC "Bloglin" site examines the film from a psychoanalytical viewpoint, whereby Kent considers "the dark complexities of the familial unit". According to Syvology, the Babadook "mediates" issues such as "incest taboo and fantasies of maternal filicide" that "Amelia can’t bear to think about". Syvology writes that, without a father, Sam assumes the role of his mothers "supreme protector" and the "two of them form a dreadful libidinal circuit." Syvology also discusses "the notion of paternal estrangement vis-à-vis the mother-child dyad" evident in the film, as the fathers literal death at the same time that the son is born is symbolic of the "death inherent in fatherhood". Although the Babadook "gives body to much of what is fundamentally discomfiting about reproduction and family life", the films ending reminds the viewer that such matters continually exists in the "basement" of the unconscious and are "dealt with as-needed." 

==Accolades==
{| class="wikitable"
|-
! Award
! Category
! Subject
! Result
|- AACTA Awards  (4th AACTA Awards|4th)  AACTA Award Best Film 1   Kristina Ceyton
| 
|- Kristian Moliere
| 
|- AACTA Award Best Direction Jennifer Kent
| 
|- AACTA Award Best Original Screenplay
| 
|- AACTA Award Best Actress Essie Davis
| 
|- AACTA Award Best Editing Simon Njoo
| 
|- AACTA Award Best Production Design Alex Holmes
| 
|- AACTA Awards|AACTA International Awards   (4th AACTA International Awards|4th)  AACTA International Best Actress Essie Davis
| 
|-
|Critics Choice Movie Award|Critics Choice Award Best Young Actor Noah Wiseman
| 
|- Detroit Film Critics Society Awards Best Actress Essie Davis
| 
|- Best Breakthrough Jennifer Kent
| 
|- Empire Awards  (20th Empire Awards|20th)  Empire Award Best Female Newcomer Essie Davis
| 
|- Empire Award Best Horror
|The Babadook
| 
|- 2014 New New York Film Critics Circle Awards Best First Feature Kristina Ceyton
| 
|- Kristian Moliere
| 
|- Online Film Critics Society Awards Online Film Best Actress Essie Davis
| 
|- San Francisco Film Critics Circle Award San Francisco Best Actress
| 
|- Saturn Awards  (41st Saturn Awards|41st)  Saturn Award Best Horror Film Kristina Ceyton
| 
|- Kristian Moliere
| 
|- Saturn Award Best Actress Essie Davis
| 
|- Saturn Award Best Performance by a Younger Actor Noah Wiseman
| 
|-
|}

 1  Shared award with The Water Diviner

==References==
 

==External links==
*  
*  
*  
*   at Fliks.com.au
*  at  

 
 

 
 
 
 
 
 
 
 
 
 
 
 