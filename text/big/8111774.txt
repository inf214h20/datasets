Deadline: Sirf 24 Ghante
{{Infobox film|
 name = Deadline: Sirf 24 Ghante|
 image =Deadline-Movie-Poster.gif|
 director = Tanveer Khan|
 writer = | Irfan Khan Sandhya Mridul|
 producer = Ravi Agrawal|
 distributor = Percept Picture Company|
 cinematography = |
 editing = |
 released =  |
 language = Hindi|
 country = India|
 music = |
}} 2006 featuring Irfan Khan and Rajit Kapur in the lead roles. The film is a Bollywood adaptation of the Charlize Theron-Kevin Bacon film Trapped (2002 film)|Trapped, which was based on the Greg Iles novel 24 Hours.

== Synopsis ==

Heart surgeon Dr. Viren Goenka (Rajit Kapur), his wife Sanjana (Konkona Sen Sharma) and their seven-year-old daughter Anishka (Princey Shukla) live in Mumbai. Their joys have just multiplied with Viren being honored with a prestigious award for his contribution to the medical world. 
 Irfan Khan) in her house. Krish tells her that Anishka will be fine as long as Sanjana and Dr. Goenka follow his instructions. 
 Zakir Hussain) holds Anishka at a remote location. The kidnappers keep strict half-hour checks with each other by cell phones and set the ransom amount at Rs. 3 crores. 

It doesnt take much for Viren and Sanjana to realize that abiding by the kidnappers rules is the best thing that they could do to save their daughters life. Viren mortgages everything possible, including his dream hospital project and his house, and somehow manages to collect Rs. 3 crores in just 24 hours. 

But the kidnappers flee without any signs of their daughter and take away the ransom money as well. Both Sanjana and Viren are uncertain about what couldve happened and fear for the life of their beloved daughter. Viren wants his daughter back at any cost and thus, tries to exploit all his resources. The police searches for clues, but theres no breakthrough. 

Finally Sanjana and Viren realize that this case is not for money but for the revenge. And the end, the kidnappers return their daughter and all money and ends happily. But why they kidnapped Dr.Virens Daughter teaches Dr.Viren lesson what is doctors profession is not a profession but a noble profession. 

== Cast ==
*Konkona Sen Sharma ... Sanjana Irfan Khan ... Krish Vaidya
*Rajit Kapur ... Dr. Viren Goenka
*Sandhya Mridul ... Roohi
*Jhanak Shukla ... Anishka Zakir Hussain ... Kabir

== References ==
 

==External links==
* 

 
 
 