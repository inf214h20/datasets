Brother Sun, Sister Moon
{{Infobox film
| name           = Brother Sun, Sister Moon
| image          = Brother sun sister moon.jpg
| image size     =
| caption        = Theatrical release poster
| director       = Franco Zeffirelli
| producer       = Kenneth Ross
| narrator       =
| starring       = Graham Faulkner Judi Bowker
| music          = Riz Ortolani Donovan (songs)
| cinematography = Ennio Guarnieri
| editing        = Reginald Mills
| distributor    = Paramount Pictures (USA), Cinema International Corporation (non-U.S.A.)
| released       =   
| runtime        = 135 /122 min.
| country        = Italy / United Kingdom
| language       = English
| budget         =
| gross          = $1,200,000 (US/ Canada rentals) 
| preceded by    =
| followed by    =
}}
 Brother Sun, Sister Moon ( ) is a 1972 film directed by Franco Zeffirelli and starring Graham Faulkner and Judi Bowker. The film is a biopic of Saint Francis of Assisi.

==Plot==
Francesco, the spoiled son of Pietro Bernardone, a wealthy textile merchant, returns home from fighting in the war between Assisi and Perugia. Struck by a feverish illness that has forced him to leave the war, Francesco lies on his bed tormented by visions of his past when he was a boisterous, arrogant youth. During a long recovery process, he slowly finds God in poverty, chastity and obedience, experiencing a physical and spiritual recovery.

Healthy again, Francesco returns to his normal life as a rich young man. However, to the consternation of his parents, he begins to spend most of his time surrounded by nature, flowers, trees, animals and poetry as he becomes more and more reluctant to resume his prior life style.  Pietros obsession with gold now fills Francesco with revulsion, creating an open confrontation between Francesco and Pietro.  Francesco rebuffs offers to take over the family business and throws the textiles out of the window.  Pietro, frustrated, beats Francesco and humiliates him in front of the citys Bishop and population.  Francesco renounces all his worldly possessions and his "noble" family name Bernardone and leaves Assisi naked and free from his past to live an ascetic and simple life as a man of God and nature.
 San Damiano, where he hears Gods voice asking him to "restore My church on earth".  Much to the dismay of his family, friends and the local bishop, Francesco gradually gains a following amongst the poor and the suffering.  His friend Bernardo happily joins him after returning from a Crusade that left him in sorrow and emptiness.  Two other friends, Silvestro and Giocondo, admiring Francescos new vocation, help to rebuild the chapel of San Damiano.

Clare of Assisi|Clare, a beautiful young woman also from a wealthy family, serves and cares for lepers of the community.  She joins the brothers in their life of poverty.  Meanwhile in Assisi, the high classes of nobility, including the Bishop, protest against Francesco and his group, worried about them "corrupting" the whole of Assisis youth and they command Francescos friend Paolo to hinder and stop the so-called "minor brothers".

One day the rebuilt chapel is set on fire, and one of Francescos followers is killed.  Francesco blames himself but cannot understand what he has done wrong  and decides to walk to the Vatican in Rome to seek  answers from Pope Innocent III.  In Rome, Francesco is stunned by the enormous wealth and power that surrounds the throne of St. Peter.  When granted an audience with the Pope, Francesco breaks from reciting Paolos carefully prepared script and calmly protests against pomp and worldliness, reciting the scriptures (which was not allowed) to protest that Christs teachings are totally opposite to Romes obsession with wealth.  Francesco and his friends are expelled and finally accepting his admiration toward Francesco, Paolo decides to join them.

Apparently convinced by the sincerity of Paolos conversion, the Pope orders Francesco and his friends to be brought back. The Pope says that in our obsession with original sin we have forgotten original innocence and to everyones astonishment, kisses Francescos feet and blesses him and his companions, wishing for them a long world-wide society of men and women willing to serve God in just that way.  Some of the final lines from Rome (in juxtaposition to the well-established tone of the film) place the sincerity of the Popes response in question when an unnamed observer says:  "Dont be alarmed, his holiness knows what he is doing.  This is the man who will speak to the poor, and bring them back to us." The film finishes with the sight of Francesco slowly walking alone into the distance in the countryside.

==Cast==
The film features a cast of newcomers and screen veterans: Francesco di Bernardone (St Francis of Assisi) Clare Offreduccio Bernardo di Quintavalle Paolo
* Lee Montague - Pietro di Bernardone
* Valentina Cortese - Pica di Bernardone
* Alec Guinness - Pope Innocent III
* Michael Feast - Silvestro
* Nicholas Willatt - Giocondo John Sharp - Bishop Guido
* Adolfo Celi - Consul
* Francesco Guerrieri - Deodato

==Production== Romeo and Juliet (1968). The film attempts to draw parallels between the work and philosophy of Saint Francis and the ideology that underpinned the worldwide counterculture movement of  the 1960s and early 70s. The film is also known for the score composed by Riz Ortolani.
 Best Art Direction (Lorenzo Mongiardino, Gianni Quaranta, Carmelo Patrono).   

==Reception==
Brother Sun, Sister Moon received mixed reviews, as the film holds a 44% rating on Rotten Tomatoes from 18 critics.

==Soundtrack== Brother Sun, Sister Moon was released exclusively on iTunes Store.

The composer Leonard Bernstein and lyricist Leonard Cohen were originally commissioned to provide a score, but after working on the project for about three months in Italy, they withdrew.  However Bernstein used "A Simple Song", originally written for the film, in his Mass (Bernstein)|Mass.   Paul Simon was also approached for music and lyrics, but he too declined.   However, a quatrain he wrote while considering the commission was later presented to Leonard Bernstein for use in his Mass. 

==See also==
*List of historical drama films

==References==
 

==External links==
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 