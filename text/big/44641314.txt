In the Power of the Hypnotist
{{infobox film name = In the Power of the Hypnotist image =  caption =  director = Sidney Olcott producer = Gene Gauntier Feature Players writer = Gene Gauntier starring = Gene Gauntier Jack J. Clark Sidney Olcott distributor = Warners Features cinematography = 
| released =  
| runtime = 3000 ft
| country = United States language = Silent film (English intertitles) 
}}

In the Power of the Hypnotist is a 1913 American silent film produced by Gene Gauntier Feature Players and distributed by Warners Features. It was directed by Sidney Olcott with himself, Gene Gauntier and Jack J. Clark in the leading roles.

==Cast==
* Sidney Olcott - Gondorza
* Gene Gauntier - Gondorzas Daughter
* Jack J. Clark - The Detective

==Production notes==
* The film was shot in Jacksonville, Fla.

==External links==
* 
*    website dedicated to Sidney Olcott

 
 
 
 
 
 
 
 


 