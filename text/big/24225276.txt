Saddle Mountain Roundup
{{Infobox film
| name           = Saddle Mountain Roundup
| image          = Saddle Mountain Roundup (film poster).jpg
| image_size     =
| caption        = film poster
| director       = S. Roy Luby
| producer       = Anna Bell Ward (associate producer) George W. Weeks (producer)
| writer         = William L. Nolte (story) Earle Snell (writer) John Vlahos (writer)
| narrator       =
| starring       = See below
| music          =
| cinematography = Robert E. Cline
| editing        = S. Roy Luby
| distributor    =
| released       = 29 August 1941
| runtime        = 59 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}

Saddle Mountain Roundup is a 1941 American film directed by S. Roy Luby, one of the Range Busters series.

== Plot summary ==
 

== Cast ==
*Ray Corrigan as Crash Corrigan
*John Dusty King as Dusty King
*Max Terhune as Alibi Terhune
*Lita Conway as Nancy Henderson
*Jack Mulhall as Dan Freeman (lawyer)
*Willie Fung as Fang Way John Elliott as Magpie Harper
*George Chesebro as Foreman Blackie Stone
*Jack Holmes as Sheriff Steve Clark as Jack Henderson
*Carl Mathews as Bill (henchman at shack)
*Elmer as Elmer, Alibis Dummy
*Harold Goodman as Cousin Harold (singing / comic ranch hand

== Soundtrack ==
 

== External links ==
* 
* 

 

 
 
 
 
 
 
 
 


 