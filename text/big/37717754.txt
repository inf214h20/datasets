Table No. 21
 
 
 
{{Infobox film
| name = Table No. 21
| image = Table No 21 official poster.jpg
| alt =
| caption = Theatrical release poster
| director = Aditya Datt
| producer = Vicky Rajani Sunil Lulla
| screenplay =Jimmy-Sen
| story =Deepak Behera
| starring = Paresh Rawal Rajeev Khandelwal Tena Desae Dhruv Ganesh Article 21 of Indian Constitution   
| music = Gajendra Verma Background Score Amar Mohile
| lyrics = Aseem Ahmed Abbasee
| cinematography = Ravi Walia
| editing = Devendra Murdeshwar
| studio = Next Gen Films
| distributor = Eros International
| released =  
| runtime = 108 mins   
| country = India
| language = Hindi English 
| budget   =  
| gross =  (Nett)
}}
 Hindi psychological thriller film| Psychological Drama mystery thriller film directed by Aditya Datt and produced by Eros International. It is named for Article 21 of the Indian Constitution, which particularises the Protection of Life and Personal Liberty.  The movie features Paresh Rawal, Rajeev Khandelwal and Tena Desae, and touches upon the pertinent social issue of ragging. The films soundtrack was composed by Gajendra Verma, with lyrics penned by Aseem Ahmed Abbasee. The film was a moderate success at the box office. 

==Plot==
Vivaan (Rajeev Khandelwal) and Siya Agasthi (Tena Desae) strive 
to make ends meet. The couple wins a trip to the beautiful island nation
of Fiji in a lucky draw; the holiday is fully sponsored, with 
luxurious hotel accommodation and fine dinners. The duo fly to Fiji to 
celebrate their wedding anniversary and encounter the charming Mr. Khan 
(Paresh Rawal) at the resort. Mr Khan invites the couple to 
participate in a live game show called Table 21. He tells them that 
the winner of the game bags a staggering amount of ₹210&nbsp;million as prize money. He outlines the rules: eight 
personal questions are asked, which must be answered truthfully, and 
following this, one must complete a task related to the question. The 
couple decides to enter the game show. At first, the questions seem 
easy, but as the game progresses, the tasks become increasingly 
horrific.  The gruesome nature of the tasks makes Vivaan think back to 
his time at college. Vivaans final assignment is to murder an 
individual. He is led to a room, wherein he is to face his target. When 
he sees the person he must kill, he recognises him from his past. A 
flashback shows Vivaan and his friends ragging a boy, the target, 
named Akram (Dhruv Ganesh). The severe bullying made Akram [[intellectual disability| mentally 
challenged]]. Back in the present, it is revealed that Akram is  show Vivaan and 
Siya the damage inflicted on him by their actions]]. Mr. Khan tells them that they are free to go but their sins will follow them  everywhere. The movie ends as Vivaan and Siya are still shocked - crying and  regretting their choices. The end credits show upsetting cases of ragging.

==Cast==
* Paresh Rawal...Abdul Razaq Khan
* Rajeev Khandelwal...Vivaan Agasthi
* Tena Desae...Siya Agasthi
* Dhruv Ganesh...Akram
* Asheesh Kapur as Bittu
* Sana Sheikh ...Neeti
* Hanif Hilal...Ghouse (Khans Bodyguard)

==Marketing==
The promotional poster was released online on 8 November 2012, and the official theatrical trailer was unveiled a few days later on 23 November.

==Release==
The film released on 4 January 2013, and received an extremely good response at the Box Office.

==Reception==
Critics have praised the story but have criticised the way the issue of 
ragging is kept under wraps.  . 
Zeenews.india.com.  

Indiaglitz.com says that "Table No. 21 keeps you engaged right from 
start to the finish. If the beginning portions are frothy, middle 
portions turn thrilling, post interval is dramatic and ultimately the 
narrative turns dark before reaching a shocking 
end."  . Indiaglitz.com (5 January 
2013). 

Ankur Pathak of Rediff.com says that "Table No 21 should be watched 
for the reactive social commentary that it is, and should not be 
misconceived as a vigilante film." Rated it 3 out of 5 
stars.  . 
Rediff.com (4 January 2013). 

Madhureeta Mukherjee of The Times of India rated the film 3 
out of 5 
stars.  .
 Timesofindia.indiatimes.com. 

Rajeev Masand of CNN-IBN|IBNLive.com says that "Table No 21 
squanders its potential. The films ending is bold, but little else is 
consistent or 
gripping"  . Ibnlive.in.com (5 January 
2013). 

Mansha Rastogi of Nowrunning.com says that "Table No. 21, although may 
not be a completely out of the box, never before concept but its the 
execution of the story and the acting that makes this film a one time 
watch."  . 
Nowrunning.com (4 January 2013). 

Prasanth of Movieorange.com says that "Table No 21 is an excellent 
thriller, with a good message." Rated it 8 out of 
10.  Salis Afaque of Salis Magazine rated it with 2 stars out of 5.

==Box office== Box Office in its first week. It collected ₹157.5&nbsp;million net over the weekend, while the four-day collection was approximately ₹67.5&nbsp;million net.  The movie earned ₹101.0&nbsp;million in the first week of its release,  and after a two-week-long run, 2013′s first release stood at a total of ₹121.0&nbsp;million at the Indian Box Office. 

==References==
 

== External links ==

*  

 
 
 
 
 
 
 
 
 