The Gentle Sex
 
{{Infobox film
| name = The Gentle Sex
| image = "The_Gentle_Sex"_(1943).jpg Leslie Howard
| producer = Derrick de Marney
| writer = Moie Charles    
| starring = Joan Gates Jean Gillie Joan Greenwood Joyce Howard Rosamund John Lilli Palmer Ronald Shiner
| narrator = Leslie Howard
| music = John D. H. Greenwood
| cinematography = Robert Krasker Charles Saunders
| production_company = Derrick De Marney Productions Two Cities Films
| distributor = General Film Distributors
| released = 15 April 1943
| runtime = 92 minutes
| country = United Kingdom
| language = English
}}
 directed and Leslie Howard. produced by Concanen Productions, Two Cities Films and Derrick de Marney.  The Gentle Sex was Howards last film before his death. 

==Synopsis== Leslie Howard provides narration through the course of the film.  

==Cast==
* Joan Gates as Gwen Hayden
* Jean Gillie as Dot Hopkins
* Joan Greenwood as Betty Miller
* Joyce Howard as Anne Lawrence
* Rosamund John as Maggie Fraser
* Lilli Palmer as Erna Debruski
* Barbara Waring as Joan Simpson
* John Justin as Flying Officer David Sheridan
* Mary Jerrold as Mrs. Sheridan
* John Laurie as Scots Corporal
* Elliott Mason as Mrs. Fraser
* Harry Welchman as Captain Ferrier
* Miles Malleson as Train Guard
* Jimmy Hanley as 1st Soldier
* Meriel Forbes as Junior Commander Davis
* Rosalyn Boulter as Telephonist
* Tony Bazell as Ted
* Noreen Craven as Convoy Sergeant
* Frederick Peisley as 2nd Soldier
* Ronald Shiner (as Ronnie Shiner) as the Racing Punter in the Pub

==Critical reception==
TV Guide noted, "some lucid and funny moments in a capable and intelligent production for its time";  and Billy Mowbray wrote, for Film 4, "if only social history was this good at school. Funny, fascinating and probably unlike any film youve seen before, The Gentle Sex is a bona fide cultural treasure."  

==References==
 

==Further reading==
* Lejeune, C. A. (1947) Chestnuts in her Lap. London: "Mädchen in Uniform: The Gentle Sex", pp.&nbsp;95–96

==External links==
*  
*  
*  
*  
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 


 
 