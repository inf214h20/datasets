Failan
{{Infobox film
| name           = Failan
| image          = Failan film poster.jpg
| caption        = Theatrical poster
| film name      = {{Film name
| hangul         =  
| rr             = Pairan
| mr             = P‘airan}}
| director       = Song Hae-sung
| producer       = Hwang Woo-hyun   Hwang Jae-woo   Ahn Sang-hoon
| writer         = Ahn Sang-hoon   Song Hae-sung   Kim Hae-gon
| based on       =  
| starring       = Choi Min-sik Cecilia Cheung
| music          = Lee Jae-jin
| cinematography = Kim Young-chul
| editing        = Park Gok-ji
| distributor    = Tube Entertainment
| released       =  
| runtime        = 115 minutes
| country        = South Korea
| language       = Korean
| budget         =  
}} 2001 South Japanese novel Love Letter by Jirō Asada.  It stars Choi Min-sik and Hong Kong actress Cecilia Cheung.

==Plot==
After losing both her parents, Failan (Cecilia Cheung) immigrates to Korea to seek her only remaining relatives. Once she reaches Korea, she finds out that her relatives have moved to Canada well over a year ago. Desperate to stay and make a living in Korea, Failan is forced to have an arranged marriage through a match-making agency. Kang-jae (Choi Min-sik) is an old and outdated gangster who has no respect from his peers. Short on money, Kang-jae decides to take on the arranged marriage. Having nothing more than a picture of Kang-jae, Failan spends her days dreaming and wishing that Kang-jae would come to visit her. Failan often writes to Kang-jae in sorrow about how much she misses and thinks about him, but never has the nerve to give the letters to Kang-jae. Things take a turn when Kang-jae is asked by his boss to take the fall for a murder in exchange for some money. The only hope in his worthless life is the wife he never met.

== Cast==
* Cecilia Cheung as Failan
* Choi Min-sik as Kang-jae
* Son Byung-ho
* Gong Hyung-jin Kim Ji-young
* Min Kyung-jin
* Ji Dae-han
* Kim Hae-gon
* Kim Kyung-ae
* Kim Kwang-sik
* Gong Yoo-seok
* Jung Dae-hoon
* Kim Su-hyeon

== Awards ==
2001 Blue Dragon Film Awards  
* Best Director: Song Hae-sung
* Best Actor: Choi Min-sik

2001 Busan Film Critics Awards
* Best Actor: Choi Min-sik

2001 Directors Cut Awards
* Best Director: Song Hae-sung
* Best Actor: Choi Min-sik

2002 Deauville Asian Film Festival
* Lotus dOr (Prix du Jury) ("Jury Prize"): Failan
* Lotus du Meilleur Réalisateur ("Best Director"): Song Hae-sung
* Lotus du Meilleur Acteur ("Best Actor"): Choi Min-sik
* Lotus du Public (Prix du Public) ("Audience Choice Award"): Failan

2002 Grand Bell Awards
* Best Director: Song Hae-sung
* Jury Prize

== References ==
 

== External links ==
*  
*  
*  
*   at Koreanfilm.org
*   at MediaCircus.net
*   at LoveHKFilm.com
*   at AsianMovieWeb

 

 
 
 
 
 
 
 