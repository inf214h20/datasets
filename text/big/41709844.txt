2030 (film)
 
{{Infobox film
| name           = 2030
| image          = Nuoc 2030 Movie Poster.jpg
| caption        = Film poster
| director       = Minh Nguyen-Vo|Nghiem-Minh Nguyen-Vo
| producer       = Đặng Tâm Chánh  Nguyễn Thế Thanh Nghiem-Minh Nguyen-Vo Bao Nguyen
| writer         = Nghiem-Minh Nguyen-Vo
| starring       = Quynh Hoa  Quy Binh
| music          = Inouk Demers
| cinematography = Bao Nguyen
| editing        = Julie Béziau
| studio         = Saigon Media
| distributor    = 
| released       =  
| runtime        = 98 minutes
| country        = Vietnam
| language       = Vietnamese
| budget         = 
}}
 science fiction romance drama film written and directed by Minh Nguyen-Vo|Nghiem-Minh Nguyen-Vo. The film was in selected as the opening night film in the Panorama section at the 64th Berlin International Film Festival in February 2014.       It was awarded the Tribeca Sloan Filmmaker Award from the Tribeca Film Institute and the Alfred P. Sloan Foundation.    

==Plot==
In the year 2030, water levels have risen due to global climate change. South Vietnam is one of the regions worst affected by climate change, which causes as much as half the farmland to be swallowed by water. To subsist, people have to live on houseboats and rely solely on fishing with a depleting supply. Huge multinational conglomerates compete to build floating farms equipped with desalination and solar power plants floating along the coastline to produce the needed vegetables that have become highly priced commodities. A young woman is on a journey to find out the truth about the murder of her husband whom she suspects has been killed by the people of a floating farm. In the process, she discovers the secret of that floating farm; it employs genetic engineering technology to cultivate vegetables that can be grown using salt water thus can be produced much cheaper. This untested technology can have dangerous health consequences for the consumers that the farm wants to keep as a secret. It turns out that the chief scientist of the floating farm in question; the main suspect of her husbands death was her ex-lover. She ends up finding out different versions of the “truth” about her husbands death and has to make a dramatic decision without knowing the absolute truth.. {{cite web|url=http://english.vov.vn/CultureSports/Arts/Vietnamese-film-enthralls-Berlin-audiences/273025.vov
 |title=Vietnamese film enthralls Berlin audiences |date=2014-02-08 |accessdate=2014-02-12 |work=english.vov.vn}} 

==Cast==
* Quỳnh Hoa as Sáo
* Quý Bình as Giang
* Thạch Kim Long as Thi
* Hoàng Trần Minh Đức as Thủy
* Hoàng Phi as Thành

==References==
 

==External links==
*  
*  
*  

 
 
 
 