Help, My Bride Steals
{{Infobox film
| name           = Help, My Bride Steals
| image          = 
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Werner Jacobs
| producer       = Herbert Gruber
| writer         = 
| screenplay     = Janne Furch
| story          = 
| based on       =  
| narrator       =  Peter Alexander Cornelia Froboess
| music          = Johannes Fehring
| cinematography = Sepp Ketterer
| editing        = Arnfried Heyne
| studio         = 
| distributor    = 
| released       =  
| runtime        = 83 min
| country        = Austria West Germany
| language       = German
| budget         = 
| gross          = 
}} West German Peter Alexander, Cornelia Froboess and Gunther Philipp. A man marries a woman soon after meeting her, unaware that she is a kleptomaniac.

==Cast== Peter Alexander – Valentin Haase 
* Cornelia Froboess – Elisabeth Schöner 
* Gunther Philipp – Gustav Notnagel 
* Elfriede Irrall – Tessy 
* Fred Liewehr – Generaldirektor Schöner, Elisabeths Vater 
* Guggi Löwinger – Champagnermizzi 
* Kurt Heintel – Direktor Bensberg 
* Rudolf Carl – Ober Franz 
* Guido Wieland – Ein Herr 
* Peter Gerhard – Juwelier 
* Raoul Retzer – Wirt 
* Elisabeth Stiepl – Ältere Dame 
* Rudolf Vogel – Psychotherapeut 

==External links==
* 

 

 
 
 
 
 
 
 

 
 