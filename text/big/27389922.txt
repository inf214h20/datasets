George Harrison: Living in the Material World
 
 
{{Infobox film
| name = George Harrison: Living in the Material World
| image = livinginthematerialworldposter.jpg
| caption = 
| director = Martin Scorsese
| producer = Olivia Harrison Martin Scorsese Nigel Sinclair
| writer =
| starring = George Harrison
| music = Robert Richardson Martin Kenzie
| editing = David Tedeschi
| distributor = HBO
| released =  
| runtime = 208 min
| country = United States
| language = English
| budget =
}} Beatles member Emmy Awards for Outstanding Nonfiction Special and Outstanding Directing for Nonfiction Programming.

==Plot== Olivia and Dhani Harrison, friends, and many others.

==Appearances==

 
*Neil Aspinall
*John Barham
*Jane Birkin
*Pattie Boyd
*Eric Clapton
*Ray Cooper
* Bob Dylan
*Terry Gilliam
*Mukunda Goswami
*Dhani Harrison
*Olivia Harrison
*Harry Harrison
*Irene Harrison
*Louise Harrison
*Pauline Harrison
*Peter Harrison
*Damon Hill
*Eric Idle Arthur Kelly
*Jim Keltner
*Astrid Kirchherr
*Paul Lanzanic
*Jeff Lynne
*George Martin
*Paul McCartney
*Gary Moore
*Gordon Murray
*Yoko Ono
* Roy Orbison
*Tom Petty
*Billy Preston
*Ken Scott
*Ravi Shankar
*Phil Spector
*Ringo Starr
*Jackie Stewart
*Joan Taylor
*Klaus Voormann
*Gary Wright
*George Harrison
 

==Production==
After Harrisons death in 2001, various production companies approached his widow Olivia about producing a film about her late husbands life. She declined because he had wanted to tell his own life story through his video archive. Upon meeting Scorsese, she gave her blessings and signed on to the film project as a producer.

According to Scorsese, he was attracted to the project because "That subject matter has never left me...The more youre in the material world, the more there is a tendency for a search for serenity and a need to not be distracted by physical elements that are around you. His music is very important to me, so I was interested in the journey that he took as an artist. The film is an exploration. We dont know. Were just feeling our way through." 
 Shutter Island and the documentary.

The documentary premièred at the Foundation for Art and Creative Technology in Liverpool on 2 October 2011.  It was shown on HBO in two parts on 5 and 6 October 2011 in the United States and Canada   and as a two part Arena (TV series)|Arena special on BBC Two on 12 and 13 November 2011 in the United Kingdom. 

==Deluxe Edition CD==
 
All songs written by George Harrison, except where noted.

#"My Sweet Lord" (Demo)&nbsp;– 3:33 Run of the Mill" (Demo)&nbsp;– 1:56
#"Id Have You Anytime" (Early Take) (George Harrison, Bob Dylan)&nbsp;– 3:06
#"Mama, You Been on My Mind|Mama, Youve Been on My Mind" (Demo) (Bob Dylan)&nbsp;– 3:04 Let It Be Me" (Demo) (Gilbert Bécaud, Mann Curtis, Pierre Delanoë)&nbsp;– 2:56
#"Woman Dont You Cry for Me" (Early Take)&nbsp;– 2:44
#"Awaiting on You All" (Early Take)&nbsp;– 2:40
#"Behind That Locked Door" (Demo)&nbsp;– 3:29 All Things Must Pass" (Demo)&nbsp;– 4:38
#"The Light That Has Lighted the World" (Demo)&nbsp;– 2:23

== Awards == Primetime Emmy Awards, Outstanding Nonfiction Special and Outstanding Directing for Nonfiction Programming for director Martin Scorsese. It also earned nominations for Outstanding Cinematography, Picture Editing, Sound Editing, and Sound Mixing. 

==References==
 

==External links==
*  
*  
*  
 
 
 

 
 

 
 
 
 
 
 
 
 