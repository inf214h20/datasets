Four Girls in Town
{{Infobox film
| name           = Four Girls in Town
| image          = Four Girls in Town.jpeg
| caption        = 
| director       = Jack Sher
| producer       = Aaron Rosenberg
| based on       = 
| screenplay     = Jack Sher
| starring       = George Nader Julie Adams Elsa Martinelli John Gavin Gia Scala
| music          = Alex North
| cinematography = Irving Glassberg
| editing        = Fredrick Y. Smith
| studio = Universal Pictures|Universal-International
| distributor    = 
| released       =  
| runtime        = 85 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = $1 million (US) 
}}
Four Girls in Town is a 1957 film about four girls trying to be a movie star. Curtis Mister Cory Crashes High Society
Scheuer, Philip K. Los Angeles Times (1923-Current File)   14 Mar 1957: C12.  

==Cast==
*George Nader as Mike Snowmen
*Julie Adams as Katny Conway Marianne Cook (Marianne Koch) as Ina Schiller
*Elsa Martinelli as Maria Antonelli
*Gia Scala as Vicki Dauray
*Sydney Chaplin as Johnny Pryor
*Grant Williams as Spencer Farrington, Jr.
*John Gavin as Tom Grant
*Herbert Anderson as Ted Larabee
*Hy Averback as Bob Trapp
*Ainslie Pryor as James Manning
*Judson Pratt as William Purdy 

==References==
 

==External links==
*  at IMDB
*  at TCMDB
*  at New York Times

 
 
 
 
 
 
 

 