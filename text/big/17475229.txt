Lucky Cisco Kid
{{Infobox film
| name           = Lucky Cisco Kid
| image          = Lucky Cisco Kid 1940 poster.jpg
| caption        = 1940 US Theatrical poster
| director       = H. Bruce Humberstone
| producer       = Sol M. Wurtzel
| writer         = 
| starring       = Cesar Romero Mary Beth Hughes Dana Andrews
| music          = Cyril J. Mockridge
| cinematography = Lucien N. Andriot
| editing        = 
| distributor    = 20th Century Fox
| released       =  
| runtime        = 67 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Lucky Cisco Kid is a 1940 western film directed by H. Bruce Humberstone and starring Cesar Romero, Mary Beth Hughes and Dana Andrews.

==Plot summary== William Robertson), are committing crimes and blaming it on the Cisco Kid (Cesar Romero), in McQuades attempt to drive the settlers off the land and buy it himself. The Cisco Kid and Gordito (Chris-Pin Martin) eventually stop the scheme, and the Kid falls in love with widow Emily Lawrence (Evelyn Venable).

==Cast==
*Cesar Romero as The Cisco Kid
*Mary Beth Hughes as Lola
*Dana Andrews as Sergeant Dunn
*Evelyn Venable as Emily Lawrence
*Chris-Pin Martin as Gordito Willard Robertson as Judge McQuade
*Joe Sawyer as Bill Stevens
*Johnny Sheffield as Tommy Lawrence
*William Royle as Sheriff

== External links ==
*  

 
 
 
 
 
 
 


 