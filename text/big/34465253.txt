The Host (2013 film)
{{Infobox film
| name           = The Host
| image          = The Host Poster.jpg
| image_size     = 220px
| alt            =  
| caption        = Theatrical release poster
| director       = Andrew Niccol
| producer       = {{Plainlist| 
* Stephenie Meyer Nick Wechsler Steve Schwartz Paula Mae Schwartz
}}
| screenplay     = Andrew Niccol
| based on       =  
| starring       = {{Plainlist| 
* Saoirse Ronan
* Max Irons  
* Jake Abel
* Frances Fisher 
* Chandler Canterbury
* Diane Kruger
* William Hurt
* Boyd Holbrook
* Scott Lawrence
* Shawn Carter Peterson
* Lee Hardee
* Phil Austin
* Raeden Greer
* J.D. Evermore
* Emily Browning 
* Mustafa Harris
* Bokeem Woodbine Alex Russell David Lichtenstein
}} Antonio Pinto
| cinematography = Roberto Schaefer
| editing        = Thomas J. Nordberg Nick Wechsler Productions Silver Reel
| distributor    = {{Plainlist| 
* Open Road Films
* Universal Pictures
* ChockStone Pictures
* ShowMaker Works
}}
| released       =  
| runtime        = 125 minutes     
| country        = United States
| language       = English
| budget         = $40 million 
| gross          = $63,327,201 
}}
 romantic science science fiction thriller film adapted from novel of the same name. Written and directed by Andrew Niccol,  the film stars Saoirse Ronan, Max Irons, Jake Abel, William Hurt, and Diane Kruger.  It was released in theaters on March 29, 2013.

==Plot==
  
The human race has been taken over by small parasitic aliens called "Souls". The Souls travel to distant planets inserting themselves individually into a host body of that planets dominant species. They are able to access the hosts memories, and occupied hosts are identifiable by silver rings that form in the hosts eyes.

Melanie Stryder is captured and infused with a soul called "Wanderer." Wanderer is asked by Seeker to access Melanies memories in order to discover the location of a pocket of unassimilated humans. Surprisingly, Melanies consciousness has not been eliminated, and it struggles for control of her body. Melanie and Wanderer carry out an internal conversation with each other, forming something of a friendship.

Wanderer shares with Seeker that Melanie was traveling with her brother, Jamie, and her boyfriend, Jared Howe, to find Melanies uncle Jeb in the desert. Wanderer admits that Melanie is still present, so Seeker decides to be transferred into Melanies body to get the information herself. With the help of Melanie, Wanderer escapes and makes her way to the desert, where she is found by Jeb, who takes her to a series of caves hidden inside a mountain where the humans (including Jared and Jamie) are hiding.

Wanderers presence in the shelter is met with hostility by all but Jeb and Jamie. Seeing this, Melanie instructs Wanderer not to tell anyone she is still alive, since it would merely provoke them, though she later allows her to tell Jamie. Wanderer begins interacting with the humans and slowly begins to gain their trust, forming a bond with Ian OShea. 

Seeker leads a search party into the desert to find Wanderer. They intercept one of the shelters supply teams, and in the ensuing chase, Aaron and Brandt commit suicide to avoid capture. During the chase, Seeker accidentally kills another Soul, leading her superiors to call off the search.

Returning to the caves, a vengeful Jared and Kyle move to kill Wanderer, causing Jamie to reveal that Melanies consciousness is alive. Jeb and Ian accept this, but Jared refuses to believe it until he attempts to determine the truth by kissing Wanderer, provoking Melanie to slap him. Kyle tries to kill Wanderer but endangers his own life and ends up being saved by Wanderer. Ian believes that Kyle attacked Wanderer and tells her that he loves her. Wanderer admits that, while occupying Melanies body, she must love Jared, but she has feelings of her own, and the two kiss.

Sometime later, Wanderer enters the communitys medical facility and discovers that Doc has been experimenting with ways to remove Souls and allow the hosts mind to regain control, resulting in the deaths of many Souls and Hosts from his failed experiments. After isolating herself for several days, Wanderer learns that Jamie is critically ill with an infection in his leg. She infiltrates a Soul medical facility to steal some of their alien medicine, saving Jamies life.

Seeker has continued looking for Wanderer on her own, but Jeb captures her and imprisons her in the caves. Wanderer offers to show Doc the proper method of removing Souls, on the condition that he remove her from Melanies body and let her die. Doc uses the technique to remove Seeker from her host, with both Host and Soul surviving. Wanderer takes the Seeker alien to a Soul space-travel site, where she sends it to a planet far enough from Earth that it can not return for numerous Generation#Familial generation|generations. 

Wanderer makes Doc promise to let her die when she is removed and not tell anyone. Their friends intervene with Doc, who then inserts Wanderer into Pet, a human who was left brain-dead after the Soul inside her was removed. Now with a body of her own, Wanderer forms a relationship with Ian, while Melanie reunites with Jared.

A few months later, while on a supply-gathering trip, Wanderer, Melanie, Ian and Jared are captured. They discover that their captors are humans, who reveal that there are several other human groups as well. They also learn that a Soul with this group has sided with the human resistance, as Wanderer has, and they may not be the last Souls to do so.

==Cast==
 
 
* Saoirse Ronan as Melanie Stryder / Wanderer "Wanda" 
* Max Irons as Jared Howe 
* Jake Abel as Ian OShea   
* Frances Fisher as Magnolia "Maggie" Stryder 
* Chandler Canterbury as Jamie Stryder 
* Diane Kruger as The Seeker / Lacey 
* William Hurt as Jebediah "Jeb" Stryder
* Boyd Holbrook as Kyle OShea   
* Scott Lawrence as Doc
* Shawn Carter Peterson as Wes
* Lee Hardee as Aaron     
* Phil Austin as Charles
* Raeden Greer as Lily
* J.D. Evermore as Trevor Stryder
* Emily Browning as Pet / Wanderer "Wanda"
* Mustafa Harris as Brandt
* Bokeem Woodbine as Nate Alex Russell as Seeker Burns
* David Lichtenstein as Benjamin Wallen
 

==Production==

===Development=== Nick Wechsler, Nick Wechsler, Steve Schwartz, and Paula Mae Schwartz the main producers.  Andrew Niccol was hired to write the screenplay and to direct the film. In February 2011, Susanna White was hired to replace Niccol as director, but he later resumed the role in May 2011.

Saoirse Ronan was also cast in May as Melanie Stryder/Wanderer. On June 27, the release date was set for the film for March 29, 2013, and it was also announced that principal photography would begin in February 2012, in Louisiana and New Mexico.    

==Release== trailer was The Hunger Games. 

===Home media===
The Host was released on DVD and Blu-ray on July 9, 2013. 

== Reception ==

===Box office===
The film grossed $63,327,201 worldwide, of which $26,627,201 was from North America, and  $36,700,000 from other territories. It opened at #6 at the US box office, and for its opening weekend grossed $10,600,112; screened at 3,202 theaters it averaged $3,310 per theater. 

===Critical response===
  
The Host received negative reviews from critics. Review aggregation website Rotten Tomatoes gives an 8% rating based on 118 reviews, with an average score of 3.6/10. The sites consensus states: "Poorly scripted and dramatically ineffective, The Host is mostly stale and tedious, with moments of unintentional hilarity."    
At Metacritic, which assigns a normalised rating out of 100 based on reviews from critics, the film has a score of 35 based on 28 reviews, indicating "generally unfavorable reviews".   

CinemaScore polls reported that the average grade audiences gave the film was a "B-" on a scale of A+ to F. 

Manohla Dargis of The New York Times gave the film an unfavorable review, calling it "a brazen combination of unoriginal science-fiction themes, young-adult pandering and bottom-line calculation".  Todd McCarthy of The Hollywood Reporter felt that "its cloaked in yawningly familiar teen-romance terms and cries out for even a little seasoning of wit, irreverence, political smarts and genre twists that, given the well-trod terrain, seem like requisites when presenting visions of the near future." 

Roth Cornet of IGN gave it a "mediocre" score of 5/10, stating that the film is "unintentionally laughable" and "frustratingly absurd". Cornet says that it could have made an interesting story but that the contradictions of the peaceful but parasitic Souls were not fully explained, in the case of the character Seeker only given a shallow unsatisfying explanation. She praises Ronan for her performance and blames a "fundamentally flawed" script.  Time Out magazine blames the source material, crediting Niccol for making the best of it, but primarily blaming the high-definition–video cinematography saying it makes "what once would have been a lush, grand-scale blockbuster appear cheap and televisual." 

The Host was the penultimate film to be reviewed by film critic Roger Ebert before his death on April 4, 2013, and the last major film to be published in his lifetime. He rated the film 2.5/4 stars, saying "The Host is top-heavy with profound, sonorous conversations, all tending to sound like farewells. The movie is so consistently pitched at the same note, indeed, that the structure robs it of possibilities for dramatic tension." 

==See also==
* Invasion of the Body Snatchers
* Twilight (series)|Twilight (series)
* Parasyte

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 