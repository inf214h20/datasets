American High School (film)
{{Infobox film
| name           = American High School
| image          = AHSdvdBOX.gif
| caption        = DVD cover
| director       = Sean P. Cannon
| producer       = Raquel Tolmaire Sean P. Cannon
| starring       = Jillian Murray Aubrey ODay Talan Torriero Martin Klebba Davida Williams Brian Drolet Alex Murrel
| music          = James E. Foley
| cinematography = Joe Passarelli
| editing        = Michael Darrow
| distributor    = Anchor Bay Entertainment
| released       =  
| runtime        = 86 minutes
| country        = United States Canada
| language       = English
| budget         = 
| gross          =
}} film written and directed by Sean Patrick Cannon and starring Jillian Murray, Aubrey ODay, Talan Torriero and Martin Klebba. It was released on April 7, 2009 in the US.     Trini Lopez makes a guest appearance as the performer at the Prom.

==Plot==
Its the final week of senior year at American High School and Gwen Adams (Murray) considers divorcing her new husband Holden Adams (Torriero), whom she married because her rival Hilary Weiss (ODay) is also trying to be his girlfriend. She faces the problems created by Hilarys plot to become Prom Queen and decides to run for Prom Queen herself. The married couple are caught having sex by Principal Mann (Klebba). The principal faces forced resignation and hatches a plan with Gwen, another teacher and some other students, to get her job back from Ms. Apple (Ziering). She is later crowned Prom Queen (albeit by shredding Hilarys votes earlier), she dumps her husband and decides she is leaving town and so is now finally free of Hilary. As the film ends, Hilary is shown getting rejected by the male students because of her promiscuous behaviour.

==Cast==
* Jillian Murray as Gwen Adams
* Talan Torriero as Holden Adams
* Aubrey ODay as Hilary Weiss
* Martin Klebba as Principal Mann
* Nikki Schieler Ziering as Ms. Apple
* Brian Drolet as Jonny Awesome
* Alex Murrel as Dixie
* Pat Jankiewicz as Mr. Seuss
* Hoyt Richards as Kip Dick
* Ashley Ann Cook as Zoey
* James E. Foley as Matt Mysterio
* Davida Williams as Trixie
* Maxie J. Santillan Jr. as Tee-Pee

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 