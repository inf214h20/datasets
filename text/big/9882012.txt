Q Planes
{{Infobox film
| name           = Q Planes
| image          = Q Planes film poster.jpg
| image_size     =
| caption        = British quad cinema poster
| director       = Tim Whelan Arthur B. Woods
| producer       = Irving Asher (producer) Alexander Korda (executive producer) Brock Williams Jack Whittingham Ian Dalrymple
| narrator       =
| starring       = Ralph Richardson Laurence Olivier Valerie Hobson
| music          =
| cinematography =
| editing        =
| studio         = Irving Asher Productions Denham Studios
| distributor    = Columbia Pictures
| released       = 2 March 1939
| country        = United Kingdom
| language       = English
| runtime        = 82 minutes
| budget         =
}} First World The Thief of Bagdad) who was then in Britain working for Korda at Denham Studios. The film was released in the US as Clouds Over Europe.

Q Planes mixes the genres of spy thriller and comedy romance and was intended to be a star vehicle for Olivier, but Richardson dominates much of the screen with a sardonic take on a spy, either working for Scotland Yard or British Military Intelligence.  Released just months before the United Kingdom declared war on Germany in 1939, the film is a precursor to the "strongly nationalistic, anti-German films that would reach their zenith in Britain during the war years". 

==Plot==
Before the Second World War, advanced British aircraft prototypes carrying experimental and highly secret equipment being developed under government contract are regularly vanishing with their crews on their test flights. No one can deduce the problem, not even spymaster Major Hammond (Ralph Richardson) or his sister Kay (Valerie Hobson), a newspaper reporter who is working undercover in the works canteen used by the crews at the Barrett & Ward Aircraft Company.
 George Merritt), George Curzon), is a foreign agent and "mole", but Jenkins is killed by unseen gunmen before he can give up the names of his contacts. 

The mystery remains unsolved when McVane returns to the aircraft factory, determined to make the next test flight. His aircraft, like the others, is brought down by a powerful ray beamed from the S.S. Viking, a mysterious salvage ship manned by a foreign crew.   Along with his aircraft, McVane and his flight crew are taken hostage on the ship, where he discovers many other missing airmen have suffered the same fate. Gathering up weapons, McVane leads the British survivors in an attempt to take control of the ship. Major Hammond learns the truth and directs a Royal Navy ship ( ) to come to their rescue. 

In a short coda, Kay and McVane get together and Hammond learns, to his chagrin, that his longtime lady friend has married.

==Cast==
* Laurence Olivier as Tony McVane
* Ralph Richardson as Major Hammond
* Valerie Hobson as Kay Hammond George Curzon as Jenkins George Merritt as Mr. Barrett, the company president 
* John Laurie as Newspaper editor

 
==Production==
Period airports and aircraft including the Airspeed Envoy and de Havilland Tiger Moth are featured in the aerial scenes. The Brooklands racetrack, which also was an important aeronautical centre, was used as a backdrop for the aerial sequences on the ground. 

==Reception==
Considered a pre-war harbinger of a conflict with Germany and the role of spies in combating foreign agents, when screened in the United States, under the intriguing title of Clouds Over Europe, the film received a very positive review by Frank S. Nugent of The New York Times. Initially thinking the film may be a documentary, with newsreel footage of London starting the film, the scenes soon turn into "... one of the wittiest and pleasantest comedies that have come a capering to the American screen this season..." Variety reviewers also considered it had a "...refreshing tongue-in-cheek attitude; whole thing is bright, breezy and flavorsome."  
 The Avengers, according to Brian Clemens, one of its creators. 
 film version.

==Home release==
The film was released on video by Carlton Home Entertainment in 1991, and on DVD in April 2007.

==References==
===Notes===
 
===Citations===
 
===Bibliography===
 
* Aldgate, Anthony and Jeffrey Richards. Britain Can Take it: British Cinema in the Second World War. Edinburgh: Edinburgh University Press, 2nd Edition, 1994. ISBN 0-7486-0508-8.
* Barr, Charles, ed. All Our Yesterdays: 90 Years of British Cinema. London: British Film Institute, 1986. ISBN 0-85170-179-5.
* Chapman, James. Saints and Avengers: British Adventure Series of the 1960s (Popular TV Genres). London: I. B. Tauris, 2002. ISBN 978-1-86064-754-3.
* Coleman, Terry.  . London: Henry Holt and Co., 2006, First edition 2005. ISBN 0-7475-7798-6.  
* Anthony Holden|Holden, Anthony.  . London: Weidenfeld, 2008. ISBN 1-904435-89-0. 
* Murphy, Robert. British Cinema and the Second World War. London: Continuum, 2000. ISBN 0-8264-5139-X.
* Donald Spoto|Spoto, Donald. Laurence Olivier: A Biography. New York: HarperCollins, 1992. ISBN 0-06-018315-2.
 

==External links==
*  
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 