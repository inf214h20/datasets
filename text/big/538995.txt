Shaolin Soccer
 
 
 
{{Infobox film
| name           = Shaolin Soccer
| image          = ShaolinSoccerFilmPoster.jpg
| caption        = Promotional Poster (for Chinese market)
| film name      = {{Film name| traditional    = 少林足球
| simplified     = 
| pinyin         = Shàolín Zúqiú
| jyutping       = Siu3Lam4 Zuk1Kau4}}
| director       = Stephen Chow
| producer       = Yeung Kwok-Fai
| writer         = Stephen Chow Tsang Kan-Cheung
| starring       =   Raymond Wong Kwong Ting-Wo
| editing        = Kai Kit-Wai
| studio         = Star Overseas Ltd   Universe Entertainment Ltd
| distributor    = Universe Entertainment Ltd.
| released       =  
| runtime        = 112 minutes
| country        = Hong Kong
| language       = Cantonese
| budget         = US$10,000,000 
| gross          = US$42,776,760   
}} 2001 Cinema Hong Kong martial arts comedy film Shaolin monk reunites his five brothers, years after their masters death, to apply their superhuman martial arts skills to play soccer and bring Shaolin kung fu to the masses.

== Plot == Shaolin kung fu, whose goal in life is to promote the spiritual and practical benefits of the art to modern society. He experiments with various methods, but none bear positive results. He then meets "Golden Leg" Fung (Ng Man Tat), a legendary Hong Kong soccer star in his day, who is now walking with a limp, following the treachery of a former teammate Hung, now a rich businessman.

Sing explains his desires to Fung who offers his services to coach Sing in soccer.  Sing is compelled by the idea of promoting kung fu through soccer and agrees to enlist his former Shaolin brothers to form a team under Fungs management.
Sing and Fung attempt to put together an unbeatable soccer team. Fung invites a vicious team (some of which Sing previously encountered) to play against them and the thugs proceed to give the Shaolin team a brutal beating. When all seems lost, the Shaolin disciples reawaken and utilise their special powers, dismantling the other teams rough play easily. The thugs then give up, and ask to join Sings team.

 
Sing meets Mui (Zhao Wei), a baker with severe acne who uses Tai chi to bake and even takes her to look at very expensive dresses at a high-end department store after hours. She soon forms an attachment to Sing and even gets a makeover in an attempt to impress Sing. However, this backfires and when Mui reveals her feelings to him, he tells her he only wants to be her friend. This revelation, coupled with the constant bullying from her overbearing boss, leads Mui to disappear.

  of Team Evil prepares for his most powerful attack.]]
Team Shaolin enters the open cup competition in Hong Kong, where they chalk up successive and often ridiculous one-sided victories. They end up meeting Team Evil in the final. Team Evil, helmed by none other than Fungs old nemesis, Hung, who assembled a squad of players who have been injected with an American doping (sports)|drug, granting them superhuman strength and speed, making them practically invincible. Team Shaolin, which had steamrolled their earlier opponents, are brought back to reality when Team Evils amazing capabilities prove more than a match for them. At a critical moment, Mui, who has shaved her hair and gotten rid of her acne, reappears to keep goal for Team Shaolin.
 striker leaps into the sky and turns the ball into a glowing orb. When he kicks the fiery ball towards Mui, she and Sing combine their martial skills and rocket the ball down field, which tears the ground and sucks up everything in its path. The ball plows through Team Evils goal post and destroys half of the stadium. Sing is then thrown into the air in celebration as the trophy is presented to him and his team.

A newspaper article then shows Hung being stripped of his title of soccer chairman and sent to jail for five years, while Team Evil players are permanently banned from playing soccer professionally. With people all over the world practicing kung fu in their daily lives, Sings dream is finally fulfilled; and Sing and Mui also become a famous kung fu couple.

==Cast and characters==

===Major===
*Iron Head (Yat-fei Wong): the eldest Shaolin brother, he specializes in headers during match; before joining Sing he worked in a club ground tumbling boxing. 
*Iron Shirt (Tin Kai-Man): the third brother, "Iron Shirt" can absorb all kinds of blunt force attacks with little to no injuries, as well as siphoning and shooting the ball with his abdomen. 
*Empty Hand, (lightning hand) ( .
*"Mighty Steel Leg" Sing (Stephen Chow): the fifth brother and Forward striker for Team Shaolin, and the main protagonist of the film. 
*Light Weight Vest (Lam Chi-chung): the sixth brother "Light Weight" is obese and gluttonous after being diagnosed to have a pituitary disease.
*"Golden Leg" Fung (Ng Man Tat): a player from the 1980s, he became crippled after being beaten after a match, which he deliberately lost after being bribed
*Mui (Zhao Wei): A baker and Sings love interest
*Hung (Patrick Tse): The coach of Team Evil, Fungs nemesis from past till now

===Minor===
*Team Evils   and soar into the sky to kick a fiery ball to its target.
*Team Evils Goalkeeper No. 21 (Cao Hua): He is able to guard with one hand in his pocket.  His incredibly strong hands are capable of crushing a thick metal crossbar.
*Team Dragon Players No. 7 & No. 11 (Cecilia Cheung and Karen Mok): They work as a team and can run so fast that it appears as if they are flying inches above the ground. 
*Team Rebellion Captain (  and steel mallet as weapons. 
*Team Tofu Captain (Vincent Kok): Captain of the team that Team Shaolin faces in preliminaries.

==Production==

===Inspiration=== Premiere Magazine, Chow stated,

 

===Casting=== Danny Chan dance choreographer hired to design the "Michael Jackson dance number" that followed Sing and Muis first meeting early in the film. Chow comments he made Chan wear Bruce Lees Game of Death#The yellow-and-black tracksuit|yellow-and-black tracksuit because only the goalkeeper "can wear a special uniform." Tin Kai Man (Iron Shirt) had been Chows production manager on several movies, but had acted in numerous minor roles in previous films.  Triad member King of briefly appear as Team Dragon Players 7 & 11 in Shaolin Soccer, had major roles in King of Comedy. Chow defends his decision to hire non-actors, saying, "In terms of finding talent, I try to bring out the funniest thing I notice about them during casting, if it made us laugh at the casting, it will also do on the big screen." 

Vicki Zhao, who played the Mandarin-speaking Mui, said it was a different step for her to star in a Hong Kong production. However, Zhao admitted that she was not impressed with her look with less makeup because she is easily recognisable for her beautiful appearance.
 King of Comedy and the Four Eyes Clerk who beats up both Sing and Bone when they make fun of him on the bus in Kung Fu Hustle. 

==Home video releases==

===Hong Kong===
In Hong Kong, the film was released on DVD  and Video CD in 14 September 2001.  The DVD release was shortened by 10 minutes, with the option for viewers to access the deleted scenes in the middle of the film. The scenes deleted from the DVD version are the dance sequence in front of Muis bakery, much of the conversation over Muis makeover and the blooper reel before the end credits. Viewers can also access the making of key special effects scenes as well.
 UMD format Sony PSP on 23 December 2005. 

===United States===
The 2004 US DVD release by Miramax Films deleted 23 minutes of footage from the original cut; the omitted footage includes "Golden Leg" Fungs flashback opening sequence and Sings interactions with Mui. This version features an English dub with Chow dubbing his own voice and Bai Ling as the voice of Mui. In addition, the DVD gives viewers the option to play the original Hong Kong version. 

===United Kingdom===
In the UK, the film was released on Blu-ray disc by Optimum Releasing on 26 January 2010. 

===Japan===
The Japanese version of the film was released by Pioneer LDC on 22 November 2002.  It was reissued by The Clockworks Group on 21 December 2003. 

===Italy===
The Italian dub of the film features the voices of professional footballers Damiano Tommasi, Vincent Candela, Marco Delvecchio, Sinisa Mihajlovic, Giuseppe Pancaro and Angelo Peruzzi.

==Box office and reception==
Shaolin Soccer did well at Hong Kong box office eventually grossing HK$60,739,847, making it the highest grossing film in the regions history at the time. It held the record until 2004 when it was topped by Stephen Chows next feature  Kung Fu Hustle. Shaolin Soccer earned a worldwide gross of US$$42,776,760. 

Shaolin Soccer received highly positive reviews from film critics; review aggregator Rotten Tomatoes reported that 91% of critics had given the film positive reviews based on 90 reviews.

===China ban===
Chinas State Administration of Radio, Film and TV rejected Shaolin Soccer from theatrical and DVD/VCD release, because Stephen Chow did not apply for Chinese permission for public screenings in Hong Kong. 

==Awards & Nominations==

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 2px #aaa solid; border-collapse: collapse; font-size: 90%;" 
|- bgcolor="#CCCCCC" align="center"
! colspan="4" style="background: LightSteelBlue;" | Awards & Nominations
|- bgcolor="#CCCCCC" align="center" 
! Event
! Category
! Nominee
! Result
|-
|Blue Ribbon Awards Best Foreign Language Film
|
| 
|-style="border-top:2px solid gray;"
|-
|rowspan=3|2nd Chinese Film Media Awards Best Film
|
| 
|-style="border-top:2px solid gray;"
|- Best Actor Stephen Chow 
| 
|-style="border-top:2px solid gray;"
|- Best Actress Zhao Wei
| 
|-style="border-top:2px solid gray;"
|-
|rowspan=2|38th Golden Horse Awards Best Action Choreography
|Siu-Tung Ching 
| 
|-style="border-top:2px solid gray;"
|- Best Visual Effects Centro Digital Pictures Ltd. 
| 
|-style="border-top:2px solid gray;"
|-
|rowspan=3|7th Golden Bauhinia Awards Best Picture
|
| 
|-style="border-top:2px solid gray;"
|- Best Director Stephen Chow
| 
|-style="border-top:2px solid gray;"
|- Best Supporting Actor
|Yat-Fei Wong 
| 
|-style="border-top:2px solid gray;"
|-
|rowspan=14|21st Hong Kong Film Awards Best Picture
|
| 
|-style="border-top:2px solid gray;"
|- Best Director Stephen Chow
| 
|-style="border-top:2px solid gray;"
|- Best Young Director Stephen Chow
| 
|-style="border-top:2px solid gray;"
|- Best Screenplay Stephen Chow Kan-Cheung Tsang 
| 
|-style="border-top:2px solid gray;"
|- Best Actor Stephen Chow
| 
|-style="border-top:2px solid gray;"
|- Best Supporting Actor Wong Yat-Fei
| 
|-style="border-top:2px solid gray;"
|- Best Action Choreography
|Siu-Tung Ching 
| 
|-style="border-top:2px solid gray;"
|- Best Cinematography
|Pak-Suen Kwan Ting Wo Kwong 
| 
|-style="border-top:2px solid gray;"
|- Best Costume & Make Up Design Yim Man Choy  
| 
|-style="border-top:2px solid gray;"
|- Best Editing
|Kit-Wai Kai  
| 
|-style="border-top:2px solid gray;"
|- Best Sound Effect Kinson Tsang 
| 
|-style="border-top:2px solid gray;"
|- Best Visual Effect Frankie Chung Ken Law Ronald To 
| 
|-style="border-top:2px solid gray;"
|- Best Original Film Score
|Ying-Wah Wong   
| 
|-style="border-top:2px solid gray;"
|- Best Original Film Song Jacky Chan (composer) Andy Lau (lyricist/performer)  
| 
|-style="border-top:2px solid gray;"
|-
|rowspan=1|Hong Kong Film Critics Society Awards Best Picture
|
| 
|-style="border-top:2px solid gray;"
|-
|}

==Media adaptations==

===Comic books===

====Chinese====
The first of a four volume Shaolin Soccer manhua was published in Hong Kong roughly nine months after the film originally premiered in 2001. The characters were drawn with large manga-like eyes and cartoonish bodies, but the artists were careful to retain the likenesses of each actor who portrayed them.  

====American====
  film corporation bought the American film rights to Shaolin Soccer before its release in China,  so they helped publish the comic book along with two Chinese film companies who originally produced the film. Seto, Andy. Shaolin Soccer (Vol. 1). Fremont, CA: ComicsOne Corp., 2003 (ISBN 1-58899-318-3)  Volumes 1 (ISBN 1-58899-318-3) and 2 (ISBN ISBN 1-58899-319-1) were released in August and November 2003 and sold for United States dollar|US$13.95 each. Their suggested reading level was age 13 and above.  

Seto worked to make the novel as faithful to the film as possible but he admits that Stephen Chows brand of Mo lei tau comedy does not translate well into illustrations.  He stated in an interview that "the Shaolin Soccer comic is 80% movie adaptation with 20% new content."  This new content includes a backstory about Steel Legs training in Shaolin before the death of his master, as well as completely rewriting entire sections of the movie. For example, in the film a group of bar thugs beat up Sing and Iron Head after listening to their Lounge music|lounge-style tribute to Shaolin kung fu. The following day, Sing seeks out the group and uses his Shaolin skills to beat the thugs using a soccer ball. Fung sees the brawl and comes up with the idea of fusing kung fu and soccer. However, in the comic book, Sing is meditating in the park when he gets hit in the head with a soccer ball. The cocky players mock him and destroy a stone statue of his deceased master. Sing proceeds to use the soccer ball as a weapon.

Another example is the fact the characters are visually different from the film. All of their comic book personas look to be in their twenties to thirties, with highly toned athletic physiques (with the exception of Light Weight); even Iron Head, who was the eldest of the six brothers, appears younger than he should.

== Reception ==
Several online reviews have criticised the American adaptation for its apparent lack of story line coherence, mixture of realistic and cartoonish drawing styles, and bad Chinese-to-English translation, among other issues. In regards to the translation, one reviewer stated, "Its almost as if the book was translated with a first-year English student referencing a Chinese-to-English dictionary, with strangely assembled sentences and strange bursts of dialogue peppering the pages."  Another common complaint was that the comics seemed to be geared towards those people who had previously seen the movie. Without this familiarity, a newcomer would lose track of the storyline because of the overcrowded pages and rapidly shifting plot.     

==In popular culture==
  animated television series, stated in an interview that "Shaolin Soccer is one of our favorite movies. It has tons of fantastic action and lots of funny moments. Some of the effects provided inspiration for how bending (the art of controlling the elements) might look on the show." 
 Keroro Gunso had a soccer theme which parodied this movie.

The music video for the American R&B singer-songwriter and rapper Lumidee song "Dance" launched for the 2006 FIFA World Cup album soundtrack has scenes of the movie.

== Notes and references ==
 
*  

== External links ==
*  
*  
*  
*  
*  
*  
*  
*   at LoveHKFilm.com
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 