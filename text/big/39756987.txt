The County Fair (1934 film)
 

{{Infobox Hollywood cartoon|
| cartoon_name = The County Fair
| series = Oswald the Lucky Rabbit
| image = Countyfair1934.jpg
| caption = The boy beagle wakes Oswald up with rain.
| director = Walter Lantz Bill Nolan
| story_artist = 
| animator = Ray Abrams Fred Avery Cecil Surry Jack Carr Ernest Smythe Merle Gilson
| voice_actor = 
| musician = James Dietrich
| producer = Walter Lantz
| studio = Walter Lantz Productions
| distributor = Universal Pictures
| release_date = February 5, 1934
| color_process = Black and white
| runtime = 6:13 English
| preceded_by = The Candy House
| followed_by = The Toy Shoppe
}}

The County Fair, also called The Country Fair in some reissues, is a short animated film distributed by Universal Pictures, and among numerous ones that feature Oswald the Lucky Rabbit. The cartoon shares a very similar title with Kounty Fair, another Oswald short released four years prior.

==Plot==
Oswald is walking merrily on the road, heading towards the county fair. On his way, the boy beagle is expelled out of the house by a disgruntled father. The father dog roughs up the boy beagle some more before returning indoors. Feeling sorry for his little friend, Oswald decides to take the little dog along.

At the fair, Oswald and the boy beagle disguise themselves as a single customer in an attempt to just pay for one ticket. But their cover is blown when they stumble, prompting the ticket seller to shoo them. Annoyed by that employee, the boy beagle assaults the ticket seller, eventually knocking the latter out of the ticket booth.

Going further into fair, the boy beagle wishes to board the roller coaster. But when a guard demands for a ticket which he has none, the little dog decides to just harass the guard, momentarily knocking that worker using the spinning bars. Oswald, who tries to take him, would also be knocked down by those bars.

Oswald chases the boy beagle into the trousers of a hefty tourist. While the little dog is able to get out of the pants, the rabbit gets stuck. The boy beagle begins pelting Oswald in the head with some baseballs.

Oswald then pursues the boy beagle into a high striker which the little dog climbs up. Oswald strikes the lever with a mallet but the boy beagle is able to dodge the bell-hitting mechanism. Oswald strikes the lever once more but harder. The impact of the second hit was so powerful that the high striker collapses. The rabbit then captures the boy beagle in a way that the tiny canine cannot escape.

Returning to the house, Oswald hurls the boy beagle back inside. In no time the house starts to rattle violently, giving Oswald the notion that the little dog is getting battered more by the father. It then appears that the one getting the abuse is the father dog who runs away in terror. The angry boy beagle shows up holding a short 2x2 lumber. Frightened, Oswald runs as far as he could. The boy beagle tosses the lumber which lands on Oswalds head, putting the rabbit dazed on the ground.

==External links==
*  at the Big Cartoon Database

 

 
 
 
 
 
 
 
 
 


 