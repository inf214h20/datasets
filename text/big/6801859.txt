La Fièvre Monte à El Pao
{{Infobox film
| name        = La fièvre monte à El Pao
| image       = Fievremonteelpao.jpg
| caption     = La fièvre monte à El Pao poster
| writer      = Luis Alcoriza Luis Buñuel
| starring    = Gérard Philipe María Félix Jean Servais Miguel Ángel Ferriz
| director    = Luis Buñuel
| producer    = Gregorio Walerstein Raymond Borderie
| movie_music =
| distributor = Films Borderie, Terra Films, Cormoran Films and others
| released    = 5 December 1959 (France)
| runtime     = 97 min. French
| music       =
| awards      =
| budget      =
}}
La fièvre monte à El Pao ("Fever Mounts at El Pao", also known in English as Republic of Sin)  is a 1959 film by director Luis Buñuel.  Gerard Philipe died during the filming. This was his last film and scenes had to be shot using a double, or rewritten to complete the picture.

==Plot==
On the remote Caribbean island Ojeda an agitated population kills their despotic ruler Mariano Vargas. His secretary Ramón Vázquez takes over and tries to reinstate public order. Meanwhile Alejandro Gual, leader of a special military unit, tries to take the place of Ramón Vázquez. Knowing that Ramón Vázquez had an affair with the dictators wife Inés, he tries to turn the widow against her lover.

==Selected Cast==
* Gérard Philipe as Ramón Vázquez
* María Félix as Inés Rojas
* Jean Servais as Alejandro Gual
* Miguel Ángel Ferriz as Gouvernor Mariano Vargas
* Raúl Dantés as Lieutenant García
* Domingo Soler as Professeur Juan Cárdenas
* Víctor Junco as Indarte

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 


 
 