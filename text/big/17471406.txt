Wild Blood (2008 film)
{{Infobox film
| name           = Wild Blood
| image          = Wild Blood.jpg
| caption        = Sanguepazzo theatrical poster.
| director       = Marco Tullio Giordana
| producer       = Angelo Barbagallo 
| screenplay     = Leone Colonna Marco Tullio Giordana Enzo Ungari
| story          = Marco Tullio Giordana
| starring       = Luca Zingaretti Monica Bellucci
| music          = Franco Piersanti
| cinematography = Roberto Forza
| editing        = Roberto Missiroli
| distributor    = 
| released       =  
| runtime        = 150 minutes
| country        = Italy
| language       = Italian
| budget         = 
| gross          = $1,296,450 
}} Italian film directed in 2008 by Marco Tullio Giordana.

==Plot==
This film tells the story of two renowned actors of Fascist cinema, Luisa Ferida and Osvaldo Valenti, who were supporters of the Salò Republic. Accused of collaboration and torture, they were shot by the Partisans after the country was liberated.

The movie was included in the "uncategorized" group at Cannes Film Festival in 2008

==Cast==
*Luca Zingaretti as Osvaldo Valenti
*Monica Bellucci  as  Luisa Ferida
* Alessio Boni as  Golfiero/Taylor
* Maurizio Donadoni as  Vero Marozin 
* Alessandro Di Natale as  Dalmazio 
* Luigi Diberti as  Cardi
* Tresy Taddei as  Irene
* Mattia Sbragia as the film director
* Luigi Lo Cascio as the partisan 
* Sonia Bergamasco as the prisoner

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 


 
 