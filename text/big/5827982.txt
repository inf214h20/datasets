The Love of Sunya
{{Infobox film
| name           = The Love of Sunya
| image          = The Love of Sunya.jpg
| image_size     =
| caption        = Albert Parker
| producer       = Gloria Swanson
| writer         = Earle Browne (adaptation) Cosmo Hamilton (titles) Lenore J. Coffee (uncredited)
| based on       =   John Boles Pauline Garon
| music          = William P. Perry (1970s re-issue) Robert Martin George Barnes (uncredited)
| editing        =
| distributor    = United Artists
| released       =  
| runtime        = 78 minutes
| country        = United States
| language       = Silent English intertitles
}}
 silent drama Albert Parker, John Boles and Pauline Garon.  A copy of The Love of Sunya still survives and is kept in the Paul Killiam collection.   

==Plot==
The film depicts a young woman (Swanson) granted the ability to see into her future, including her future with different men.

==Cast==
*Gloria Swanson - Sunya Ashling John Boles - Paul Judson
*Pauline Garon - Anna Hagan
*Ian Keith - Louis Anthony
*Andres de Segurola - Paola deSalvo
*Anders Randolf - Robert Goring Hugh Miller - The Outcast
*  - Henri Picard
*Ivan Lebedeff - Ted Morgan
*John Miltern - Asa Ashling
*Raymond Hackett - Kenneth Ashling
*Florence Fair - Rita Ashling

==Production background== in 1919 Albert Parker, who had directed the 1919 film in the hopes that the production would be quicker as Parker was already familiar with the material. 
 George Barnes was eventually secured though he is given no screen credit.

==Reception== Roxy Theatre in New York City on March 11, 1927. Swanson later wrote that the film received a standing ovation.  Despite this initial good reception and decent reviews from critics,  the film performed poorly at the box office, and barely recouped its budget.  Swanson felt it was terrible.  Due to its failure, producer Joseph M. Schenck convinced Swanson to come back to Hollywood and to film something more commercial. Swanson agreed but ended up filming the more controversial Sadie Thompson (1928) instead which became her most successful independent production.

==References==
 

==External links==
*  
*  
* 

 

 
 
 
 
 
 
 
 
 
 
 