Sandwich (2011 film)
{{Infobox film
| name           = Sandwich
| image          = Sandwich_2011film.jpg
| caption        = Film poster
| alt            =
| director       = M. S. Manu
| producer       = M.C.Arun   Sudeep Karat 
| writer         = Ratheesh Sukumaran   Ananya   Richa Panai  
| music          = Jayan Pirasharady 
| cinematography = Pradeep Nair 
| editing        =
| studio         =
| distributor    =
| released       =  
| runtime        =
| country        = India
| language       = Malayalam
| budget         =
| gross          =
}} Ananya in the lead roles. The movie was released on October 14, 2011 to mixed response.

==Plot==
Sai (Kunchacko Boban), a high-profile software engineer who is currently acknowledged as the best in their payrolls of his big MNC. Betrothed to another techie Shruthy, Sai and his friends lead a happy-go lucky life. After a small party on a Friday, Sai rushes home to join his parents for dinner, but on the way happens to hit another car, killing the driver. Very soon Sai realizes that he has actually killed a dreaded criminal of the city who was the main accused in many police cases. The goons younger brother Murugan (Vijyakumar) now starts to follow Sai in the belief that he is one of the men of their enemy Andipetty Naykkar (Suraj Venjaramood) and has knowingly killed his brother. Naykkar, on the other hand warmly welcomes Sai and starts protecting him and even offers to marry him to his only daughter (Ananya (actress)|Ananya). Sandwiched between two goons and two girls, Sai plans to escape from the difficult situation is the rest of the story.
 
==Cast==
* Kunchacko Boban as Sai Ananya as Kanmani
* Richa Panai as Shruthi
* Lalu Alex as Kunchacko Bobans Father
* Suraj Venjaramood as Andipetti Nayiker
* Vijay Kumar as Murugan
* Indrans
* P. Sreekumar Shari as Kunchacko Bobans Mother

==Production==
Kunchacko Boban appears in the lead of this debut film by M S Manu, who was an associate to director Shaji Kailas. The film has Richa Panai (of Bhima Jewellery advertisement) as the heroine, along with Ananya (actress)|Ananya.

==References==
 

==External links==
*  
*  
*  
*  
*  

 
 
 
 
 