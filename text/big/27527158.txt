Perfect Alibi (1995 film)
{{Infobox film
| name           = Perfect Alibi
| image          = Perfect Alibi Movie Poster.jpg
| caption        = 
| director       = Kevin Meyer
| producer       = William Hart
| writer         = Rochelle Majer Krich Kevin Meyer 
| starring       = Teri Garr Hector Elizondo Gedde Watanabe Charles Martin Smith Lydie Denier Kathleen Quinlan Alex McArthur
| music          = Amotz Plessner
| cinematography = Doyle Smith 
| editing        = David H. Lloyd    
| distributor    = Rysher Entertainment
| released       = 1995
| runtime        = 
| country        = United States
| language       = English
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
Perfect Alibi is a 1995 American crime film directed by Kevin Meyer and starring Teri Garr and Hector Elizondo.      It is based on the 1990 novel Wheres Mommy Now? by Rochelle Majer Krich. 

==Plot==
An adulterous husband and a homicidal French nanny seek to murder his rich wife for her money. The wifes best friend and a police detective attempt to bring the culprits to justice. During the films conclusion, the nanny attempts to poison her opponents tea. However, the woman apparently switches the drinks, which gradually leads to the truth being revealed.

== References ==
 

==External links==
* 

 
 
 