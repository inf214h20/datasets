Stars and Stripes Forever (film)
{{Infobox film
| name           = Stars and Stripes Forever
| image= Stars and Stripes Forever VideoCover.png
| image_size     = 
| caption        = 
| director       = Henry Koster
| producer       = Lamar Trotti
| writer         = Ernest Vajda (story) John Philip Sousa  (book)
| narrator       = 
| starring       = Clifton Webb Debra Paget Robert Wagner Ruth Hussey Alfred Newman
| cinematography = Charles G. Clarke James B. Clark
| distributor    = 20th Century Fox
| released       =  
| runtime        = 90 minutes
| country        = United States
| language       = English
| gross          = $3 million (US rentals) 
}}
Stars and Stripes Forever is a 1952 American biographical film about late-19th-/early-20th-century composer John Philip Sousa, played by Clifton Webb.  Sousa is best known for his military marches, of which "The Stars and Stripes Forever" is arguably the best known.

==Plot==
While loosely based on Sousas autobiography Marching Along, the film takes considerable liberties and dramatic license, often expanding  and examining themes and passages in the book. 
 Marine Corps after his enlistment expires to form his own band because he is not paid enough to provide for his wife Jennie (Ruth Hussey) and their children. As a favor for his splendid service, he is allowed to take along Private Willie Little (Robert Wagner), who is credited with designing the Sousaphone and naming it after his mentor (in real life, Sousa himself designed the instrument). 

Willie persuades Sousa to go with him to a "concert" where some of Sousas songs will be performed. In fact, it is a music hall, where Willies girlfriend, Lily Becker (Debra Paget), is one of the performers. When the police raid the place for indecency (by 1890s standards), the trio barely manage to get away.

Sousa forms his band and is very successful. A mention in the book that Sousa discouraged the married men in the band from bringing their wives on tour is expanded into a subplot where Willie and Lily get married in secret so they can continue touring together with Sousa. An episode shows Sousas Band playing at the Atlanta, Georgia Cotton States and International Exposition (1895), despite the attempt by Colonel Randolph (Finlay Currie) to cancel their contract. Sousa has his musicians play "Dixie (song)|Dixie" as they march up, putting the crowd in a cheerful mood. A listing of his song list for the performances includes "Dixie" as every second one. The crowd cheers, and Randolph welcomes Sousa. This stays relatively close to fact.

Sousa tours the world, and is honored by the crowned heads of Europe. Late one night, he spots Willie sneaking into Lilys train compartment late; Sousas wife has to defuse his indignation by letting him into the secret.
 USS Maine is sunk by a suspicious explosion, precipitating the Spanish-American War, both Willie and Sousa reenlist. However, Sousa is kept from the actual fighting and instead sent on a sea voyage by an outbreak of typhoid. The inspiration for the title march is depicted with a voiceover of Webb quoting Sousas actual description of the event while at sea (however, the sea voyage in real life was due to Sousa and his wife rushing back to the U.S. from a vacation in Europe upon the sudden death of his manager). 
 El Capitan, title march in public for the first time. (In real life, the march was first played publicly at the Academy of Music in Philadelphia on May 14, 1897, much earlier than depicted.) The film then cuts away to modern Washington, D.C., where the ghostly spirit of Sousa leads a real marching band.

==Music==
The "Presidential Polonaise", a Sousa composition, may be heard during the White House scene in which the President is hounded by a senator about a postmaster appointment. President Benjamin Harrison sends a request for a more lively piece of music to speed up the reception line, and "Semper Fidelis" is played.  Both pieces were specifically written by Sousa for White House functions.  The "Presidential Polonaise" for indoor events, "Semper Fidelis" for outdoors.

During the El Capitan rehearsal, while Sousa reads of the friendly fire
incident, two rarely heard lyrics can be heard: one to "El Capitan", the march, and a lovely ballad.

During the overture over the title credits, there are excerpts from many marches. The drum solo is a shortened version of the "Semper Fidelis" solo.

==Cast==
*Clifton Webb as John Philip Sousa
*Debra Paget as Lily Becker
*Robert Wagner as Willie Little
*Ruth Hussey as Jennie Sousa
*Finlay Currie as Col. Randolph
*Roy Roberts as Maj. Houston
*Thomas Browne Henry as David Blakely (as Tom Browne Henry)
*Casey Adams as Narrator (uncredited)

==References==
 

==External links==
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 