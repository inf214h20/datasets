Dr. Cook's Garden
{{Infobox play name       = Dr Cooks Garden image      = image_size = caption    = writer     = Ira Levin chorus     = characters = mute       = setting    = premiere   = September 18, 1967 place      = New York orig_lang  = English series     = subject    = genre      = web        =
}}
Dr Cooks Garden is a play by Ira Levin.

==Plot==
A young doctor returns to his New England home town after a long absence. He visits with the towns kindly old physician, Dr. Cook, a man he has admired since childhood. However, he soon finds out that the old doctor is keeping a mysterious secret.

==Original Production==
The play premiered on Broadway in 1967 with a cast including Burl Ives and Keir Dullea. George C. Scott was meant to direct News of the Rialto: So Many Busy People So Many Busy People
By LEWIS FUNKE. New York Times (1923-Current file)   27 Aug 1967: D1.  but was replaced during rehearsals by Levin. 

==Film==
{{Infobox film
| name           = Dr. Cooks Garden
| image          =
| image_size     =
| caption        =
| director       = Ted Post
| producer       =
| writer         = Art Wallace
| based on       = play by Ira Levin
| starring       = Bing Crosby Frank Converse Blythe Danner
| music          =
| cinematography =
| editing        =
| studio         = Paramount Television
| released       =  
| runtime        =
| country        = United States
| language       = English
| budget         =
| gross          =
}}
The play was adapted for television in 1971 with Bing Crosby in the title role.

==References==
 

==External links==
*   
*  

 
 
 
 
 


 