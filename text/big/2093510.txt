The Dream Team (film)
{{Infobox film
| name           = The Dream Team
| image          = Dream_team_poster.jpg
| caption        = Theatrical release poster
| director       = Howard Zieff
| producer       = Christopher W. Knight
| writer         = Jon Connolly David Loucka
| starring       = Michael Keaton Christopher Lloyd Peter Boyle Stephen Furst Lorraine Bracco David McHugh
| cinematography = Adam Holender
| editing        = Carroll Timothy OMeara
| studio         = Imagine Entertainment
| distributor    = Universal Pictures
| released       =  
| runtime        = 113 minutes
| country        = United States
| language       = English
| budget         = $15 million
| gross          = $28,890,240 (USA)
}}
The Dream Team is a 1989 comedy film directed by Howard Zieff and produced by Christopher W. Knight for Imagine Entertainment and Universal Pictures.  It stars Michael Keaton, Christopher Lloyd, Peter Boyle and Stephen Furst as mental-hospital inpatients who are left unsupervised in New York City during a field trip gone awry.  Jon Connolly and David Loucka wrote the screenplay.

==Plot==
Dr. Jeff Weitzman (Dennis Boutsikaris) is a psychologist working in a sanitarium in New Jersey. His primary patients are Billy, Henry, Jack and Albert.  Billy (Keaton) is the most normal of the group and their unofficial leader, though he is a pathological liar with delusions of grandeur and violent tendencies.  Henry (Lloyd) is obsessive/compulsive and he has deluded himself into thinking he is one of the doctors at the hospital, often walking around with a clipboard, lab coat and stethoscope.  Jack (Boyle) is a former advertising executive who believes he is Jesus Christ.  Finally, Albert (Furst) is a man-child who only says things he hears during baseball games, particularly from former ball player and commentator Phil Rizzuto.

Convinced that his patients need some fresh air and some time away from the sanitarium, Dr. Weitzman persuades the administration to allow him to take them to a baseball game at  , only for the parishioners to come to their senses and expel him (without any clothes), and the other three patients get Jack new (albeit garish) clothes from an army surplus store.

After Dr. Weitzmans beating and coma, it is up to the patients to save their doctor from being murdered by the crooked cops.  They end up having to both use and overcome their delusions and disorders in order to save the only man who ever tried to help them, with both the police and the killers looking for them.  Three revisit scenes from their pasts: Billy (former girlfriend Riley, played by Lorraine Bracco), Henry (his wife & daughter), and Jack (his former employer). As each patient does so individually, they each behave in a sane, clear manner, Henry genuinely missing his family, Billy wishing to pursue a stronger relationship, and Jack appealing to his boss that he and his friends are in trouble (but the boss reports Jack to the police).

Throughout the film there are minor scenes showing the interaction between the two crooked police officers (Philip Bosco and James Remar) and what their plans are in framing the patients for the murder of Officer Alvarez earlier in the film.

==Cast==
* Michael Keaton as Billy Caufield
* Christopher Lloyd as Henry Sikorsky
* Peter Boyle as Jack McDermott
* Stephen Furst as Albert Ianuzzi
* Dennis Boutsikaris as Dr. Jeff Weitzman
* Lorraine Bracco as Riley
* Milo OShea as Dr. Newald
* Philip Bosco as OMalley
* James Remar as Gianelli
* Michael Lembeck as Ed, Rileys boyfriend
* Jack Duffy as Bernie
* Larry Pine as Canning
* Ted Simonett as a yuppie John Stocker as Murray
* Lizbeth MacKay as Henrys wife
* Olivia Horton as Henrys daughter Ron James as Dwight
* Freda Foh Shen as a TV newscaster
* Dennis Parlato as TV newscaster
* Donna Hanover as a field reporter
* Jihmi Kennedy as the tow truck driver

==Reception== One Flew Over the Cuckoos Nest" that you might begin to wonder when Jack Nicholson will show up.   may suggest that "Dream Team" is a weak, derivative, somehow disreputable movie, which is somewhat true. If you compare it to its obvious source, it has a coy, flip attitude toward illness, skating over the surface of tragedy, dementia and pain without breaking the ice. The union of four oddballs--rebel-writer, obsessive noodge, religious fanatic and couch potato--is almost too schematic, as if the writers were somehow trying to define 80s dissidence. But even though you can predict virtually everything that happens from the first five minutes on, the director and actors manage to hook you in."  It currently holds a 53% rating on Rotten Tomatoes.

===Box office=== Paramount film, Major League.  It went down from that position in subsequent weeks.

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 