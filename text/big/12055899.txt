Like a Virgin (film)
{{Infobox film
| name           = Like a Virgin
| image          = Like a Virgin film poster.jpg
| film name      = {{Film name
| hangul         =   
| hanja          =   마돈나
| rr             = Cheonhajangsa Madonna
| mr             = Chŏnhachangsa Madonna}}
| director       = Lee Hae-jun Lee Hae-young
| producer       = Cha Seung-jae Kim Mi-hee Kim Moo-ryoung Im Choong-ryul Yun Sang-oh
| writer         = Lee Hae-jun Lee Hae-young
| starring       = Ryu Deok-hwan Baek Yoon-sik Lee Eon Lee Sang-ah Kim Yoon-seok
| music          = Kim Hong-jib
| cinematography = Jo Yong-gyu
| editing        = Nam Na-yeong
| distributor    = CJ Entertainment
| released       =  
| runtime        = 116 minutes
| country        = South Korea
| language       = Korean
| budget         =  
| gross          =   
}} 2006 South Madonna song of the same name.

== Plot ==
Chubby high school student and Madonna (entertainer)|Madonna-devotee Oh Dong-ku is a trans woman living with her abusive, alcoholic father. Dong-ku works part-time to save money for the sex reassignment surgery she craves. Despite being told that she has the perfect physique for ssireum, Dong-ku has no interest in taking up sports — but when she finds out about an upcoming ssireum tournament with a large cash prize going to the winner, she changes her mind and signs up for the team.

== Cast ==
* Ryu Deok-hwan as Oh Dong-ku
* Baek Yoon-sik as Coach
* Lee Sang-ah as Dong-kus mother
* Lee Eon as Park Joon-woo
* Moon Se-yun as Big guy 1
* Kim Young-hoon as Big guy 2
* Yoon Won-seok as Big guy 3
* Park Young-seo as Jong-man
* Tsuyoshi Kusanagi as Japanese teacher
* Kim Yoon-seok as Dong-kus father
* Kim Kyeong-ik as Jin-chul
* Oh Yoon-hong as Company presidents wife
* Choi Jung-woo as Company president
* Kwak Min-seok as Priest
* Kim Do-yeon as Large female employee
* Kim Won-sik as Oh Dong-chul
* Kwon Oh-jin as Worker
* Jang Nam-yeol as Homerooom teacher
* Lee Jae-gu as Employee at labor office
* Kim Tae-joon as Choir member at concert hall
* Kwon Jae-won as Lawyer

==Awards and nominations==
{| class="wikitable"
|-
! Year
! Award
! Category
! Recipient
! Result
|-
| rowspan=10| 2006 || rowspan=2| 7th Busan Film Critics Awards || Best New Director || Lee Hae-jun, Lee Hae-young ||  
|-
| Best New Actor || Ryu Deok-hwan ||  
|-
| rowspan=3| 27th Blue Dragon Film Awards  || Best Screenplay || Lee Hae-jun, Lee Hae-young ||  
|-
| Best New Director || Lee Hae-jun, Lee Hae-young ||  
|-
| Best New Actor || Ryu Deok-hwan ||  
|-
| rowspan=4| 5th Korean Film Awards || Best Actor || Ryu Deok-hwan ||  
|-
| Best Screenplay || Lee Hae-jun, Lee Hae-young ||  
|-
| Best New Director || Lee Hae-jun, Lee Hae-young ||  
|-
| Best New Actor || Moon Se-yun ||  
|-
| 9th Directors Cut Awards || Best New Actor || Ryu Deok-hwan ||  
|-
| rowspan=10| 2007 || 4th Max Movie Awards || Best New Actor || Ryu Deok-hwan ||  
|-
| 20th Singapore International Film Festival  || NETPAC Award || Like a Virgin ||  
|-
| rowspan=3| 43rd Baeksang Arts Awards  || Best Screenplay || Lee Hae-jun, Lee Hae-young ||  
|-
| Best New Director || Lee Hae-jun, Lee Hae-young ||  
|-
| Best New Actor || Ryu Deok-hwan ||  
|-
| 11th Fantasia International Film Festival  || Best Actor || Ryu Deok-hwan ||  
|-
| rowspan=2| 44th Grand Bell Awards  || Best Screenplay || Lee Hae-jun, Lee Hae-young ||  
|-
| Best New Actor || Ryu Deok-hwan ||  
|-
| 1st Korea Movie Star Awards || Best Young Star || Ryu Deok-hwan ||  
|- Best Actor || Ryu Deok-hwan ||  
|-
|}

==References==
 

== External links ==
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 