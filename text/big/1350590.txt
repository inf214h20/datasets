Fight Back to School
 
 
{{Infobox film
| name           = Fight Back to School
| image          = Fight Back to School DVD cover.jpg
| image_size     = 190px
| caption        = DVD cover (South Korean version)
| director       = Gordon Chan
| producer       = Wong Jing
| writer         = Barry Wong Gordon Chan
| narrator       =
| starring       = Stephen Chow Cheung Man Ng Man-Tat Roy Cheung Barry Wong
| music          =
| cinematography = Cheng Siu-Keung
| editing        =
| distributor    = Wins Movie Production & I/E Co. Ltd.
| released       = 1991
| runtime        = 100 minutes
| country        = Hong Kong Cantonese  Mandarin
| budget         =
| gross          =
}}

Fight Back To School ( ) is a 1991 Hong Kong comedy film directed by Gordon Chan and starring Stephen Chow. The cast includes Cheung Man and Ng Man-Tat. Owing to this success, the film spawned two successful sequels; Fight Back to School II,  and Fight Back to School III, as well as a spin-off Truant Heroes, which retained much of the cast.

This movie is set in a school in Hong Kong, Shatin Shatin College|College.

==Plot==
Star Chow (  to recover the senior officers missing revolver. 

The undercover operation is made complicated when Star is partnered with Tat - an aging, incompetent police detective (Ng Man-Tat). However, Star still manages to fall in love with Ms Ho (Cheung Man), the schools guidance counselor, as well as disrupting a gang involved in arms-dealing.

==Cast==
* Stephen Chow - Star Chow / Chow Sing-Sing
* Cheung Man - Miss Ho
* Ng Man-tat - Tso Tat-Wah / Uncle Tat
* Roy Cheung - Brother Teddy Big
* Barry Wong - Police Commissioner
* Gabriel Wong - Turtle Wong Paul Chun - Lam
* Dennis Chan
* Peter Lai
* Nicholas Laletin Roger Thomas
* Tsang Kan-Wing
* Karel Ng
* Kingdom Yuen

 {{Cite web |url=http://www.imdb.com/title/tt0103045/ |title=Fight Back to School 
 |accessdate=29 June 2010 |publisher=imdb.com}}    

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 

 
 