Silver Tongues
 
{{Infobox film
| name           = Silver Tongues
| image          = 
| caption        = 
| director       = Simon Arthur
| producer       = Jared Moshe Leda Nornang
| writer         = Simon Arthur
| starring       = Lee Tergesen Enid Graham 
| music          = Enis Rotthoff 
| cinematography = Josh Silfen
| editing        = 
| studio         = 
| distributor    = Stick Pictures 
| released       = 
| runtime        = 90 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}

Silver Tongues is a 2011 dark drama film. Director Simon Arthur received the "someone to watch" award at the Independent Spirit Awards for his work directing. 

==Plot==
Gerry and Joan (Tergesen and Graham) are a middle-aged couple who travel from town to town under false personas to mutilate and change the lives of any and all unsuspecting victims in their paths. Using their immense acting skill, the duo start to fall apart when their relationship strains under the pressure of their performances.

==Cast==
* Lee Tergesen as Gerry
* Enid Graham as Joan
* Tate Ellington as Alex
* Emily Meade as Rachel
* Adam LeFevre as Police Chief
* Harvey Kaplan as John Roberts

==External links==
*  

 
 
 
 
 
 
 
 
 
 


 
 