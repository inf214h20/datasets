The Happy Cricket and the Giant Bugs
 
{{Infobox film
| name           = The Happy Cricket and the Giant Bugs
| image          = The Happy Cricket and the Giant Bugs.jpg
| alt            =  
| caption        = 
| director       = Walbercy Ribas and Rafael Ribas
| producer       = Juliana Ribas
| writer         = Walbercy Ribas
| starring       = 
| music          = Ruriá Duprat
| cinematography = 
| editing        = 
| studio         = Start Desenhos Animados
| distributor    = 20th Century Fox
| released       =  
| runtime        = 80 minutes
| country        = Brazil
| language       = Portuguese
| budget         = 
| gross          = 
}}
The Happy Cricket and the Giant Bugs ( ) is a 2009 Brazilian computer-animated childrens fantasy film directed by Walbercy Ribas and Rafael Ribas and produced by Start Desenhos Animados. The film is the sequel of The Happy Cricket, released in 2001.

The Happy Cricket and the Giant Bugs was nominated for best movie in the 2009 Chicago International Childrens Film Festival."  In 2010, the film won the Academia Brasileira de Cinemas prize for best animation and best childrens feature film.

==Cast==
*Luiz Amorim - Professor Vareta
*Rodrigo Andreatto - Rafael
*Jair Assumpcao - Trambika
*Marcello Braccesi - Caradura
*Carlos Capeletti - Sakana
*Giovanni Delgado - Sebastião
*Renato Dobal - Kakatus
*Julia Duarte - Pétala
*Vagner Fagundes - Grilo Feliz
*Bel García - Pétala - SINGER
*Milto Levy Jorge - Camelô
*Marcelo Leal - Grilo Feliz - SINGER
*Jonas Melo - Montanha
*Krissos Michellepis - Camelô
*Fatima Noya - Bituquinho
*Leonardo Patriani - Locutor
*Leticia Quinto - Juliana
*Sarah Regina - Sapa
*Rafael Ribas - Netao
*Marcos Tumura - Verdugo

==References==
 

==External links==
*    
*  
*  
*    

 
 
 
 
 
 

 