Alvarez Kelly
{{Infobox film
| name           = Alvarez Kelly
| image          = Alvarez Kelly poster.jpg
| caption        = Theatrical release poster
| director       = Edward Dmytryk
| producer       = {{plainlist|
* Sol C. Siegel
* Ray David (uncredited)
}}
| writer         = {{plainlist|
* Franklin Coen
* Elliott Arnold (uncredited)
}}
| starring       = {{plainlist|
* William Holden
* Richard Widmark
}}
| music          = Johnny Green
| cinematography = Joseph MacDonald
| editing        = Harold F. Kress
| distributor    = Columbia Pictures Corporation
| released       =  
| runtime        = 116 minutes
| country        = United States
| language       = English
| gross          = $1.4 million (est. US/ Canada rentals) 
}}
 Confederate Major General Wade Hampton III.

==Plot== cattle drive, Confederate raiders led by Colonel Tom Rossiter (Richard Widmark). The Confederacy desperately needs the beef to feed its soldiers besieged in Richmond, Virginia|Richmond.
 Patrick ONeal). As revenge, Kelly arranges passage for Rossiters discontented fiancée, Liz Pickering (Janice Rule), on a blockade runner leaving the besieged city.

==Cast==
* William Holden as Alvarez Kelly
* Richard Widmark as Colonel Tom Rossiter
* Janice Rule as Liz Pickering Patrick ONeal as Major Albert Steadman Victoria Shaw as Charity Warwick
* Roger C. Carmel as Captain Angus Ferguson, a blockade runner
* Richard Rust as Sergeant Hatcher
* Arthur Franz as Capt. Towers
* Don Red Barry as Lt. Farrow (billed as Donald Barry)
* Duke Hobbie as John Beaurider
* Harry Carey Jr. as Cpl. Peterson
* Howard Caine as McIntyre
* Mauritz Hugo as Ely Harrison
* Barry Atwater as Gen. Kautz (as G.B. Atwater) Robert Morgan as Capt. Williams

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 

 