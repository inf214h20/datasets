The Party Is Over
{{Infobox film
| name = The Party Is Over
| image = 
| caption =
| director = Leopoldo Torre Nilsson
| producer = Néstor R. Gaffet Juan Sires Leopoldo Torre Nilsson
| writer = Beatriz Guido Ricardo Luna Leopoldo Torre Nilsson
| starring = Arturo García Buhr
| music =
| cinematography = Ricardo Younis
| editing = José Serra
| distributor =
| released =  
| runtime = 104 minutes
| country = Argentina
| language = Spanish
| budget =
}}

The Party Is Over ( ) is a 1960 Argentine drama film directed by Leopoldo Torre Nilsson. It was entered into the 10th Berlin International Film Festival.    The film depicts the political corruption in Argentina in the 1930s, a period known as the Infamous Decade.

==Cast==
* Arturo García Buhr as Mariano Braceras
* Lautaro Murúa as Guastavino
* Graciela Borges as Mariana Braceras
* Leonardo Favio as Adolfo Peña Braceras
* Elena Tritek as The Prostitute
* Osvaldo Terranova
* Lydia Lamaison
* María Principito
* Idelma Carlo
* Hilda Suárez
* Leda Zanda
* Juan Carlos Galván
* Santángelo
* Emilio Guevara
* Ricardo Robles

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 