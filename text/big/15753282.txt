Work In Progress (2000 film)
{{Infobox film
| name           = Work in Progress
| image          =Work_in_progress.png
| caption        = Title sequence
| director       =Tom Bertino
| producer       = Christian Kubsch
| music             =Jimmie Haskell
| distributor    = Industrial Light & Magic
| released        =November 21, 2000
| runtime          = 5 minutes
| country        =   United States
}}
 ILM animated computer animated short film directed by Tom Bertino

==Plot== Richard Wilson) is in a large workshop with his dumpy, cigar smoking assistant (voiced by Tony Haygarth) working on designing and building a huge gorilla. The conversation between them explains to the user that the pair have created all of the animals on their world, but only with the help of a small girl; except for one, the chihuahuaraffe, a chihuahua body with a giraffe neck and head, which has severe stability issues resulting in it tripping over constantly.

They then fire up the gorilla and bring it to life. The gorilla promptly sits up and smashes through a wooden platform causing the assistant to be flung across the workshop. The gorilla looks very rough with exposed metal and large sewing marks along its chest. It sits there and chews on some of the wood from the platform.

The short then cuts to the small girl working across a beach and through a forest towards the workshop. This scene demonstrates a number of CGI technologies, including: particle simulation (shown with clouds, water and sand) cloth simulation (with the girls clothing),  fur/hair simulation (with the girls hair and the fur on a chipmunk) and foliage simulation.
 cymbal monkey causing them to change their minds and congratulate themselves on how well it works, completely ignoring the girl. They then proceed to walk off together discussing their next animal design while the gorilla walks off with the girl.

== Cast == Richard Wilson
*Tony Haygarth

== Crew ==
*Directed by Tom Bertino
*Produced by Christian Kubsch
*Co-Producer: Jill Brooks
*Executive Producers: Patricia Blau, Jim Morris
*Music by Jimmie Haskell
*Editor: Steve Bloom
*Story Supervisor: Anthony Stacchi
*Production Designer: Erik Tiemens
*Character Designer: Carlos Huante
*Layout Supervisor: Scott Farrar
*Visual Effects Supervisor: Erik Mattson
*Supervising Animators: Chris Armstrong, Sean Curran, Tim Harrington
*Sound Designer: Tom Myers

== External links ==
* 
* 

 
 
 
 
 
 
 
 
 
 
 