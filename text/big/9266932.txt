Def-Con 4
 
 

{{Infobox film
| name           = Def-Con 4
| image          = Def-Con4.jpg
| caption        = Theatrical release poster Paul Donovan   Tony Randel (uncredited) Maura OConnell
| writer         = Paul Donovan
| starring = {{Plainlist|
* Lenore Zann
* Maury Chaykin
* Kate Lynch
* Kevin King
* John Walsch
* Tim Choate
}} Chris Young
| cinematography = Douglas Connell Les Krizsan
| editing        = Todd C. Ramsay
| studio         = Salter Street Films
| distributor    = New World Pictures
| released       = March 15, 1985
| runtime        = 88 minutes
| country        = Canada
| language       = English
| budget         = 
| tagline        = 
}}Def-Con 4 is a 1985 post-apocalyptic film, portraying three astronauts who survive World War III aboard a space station and return to earth to find greatly changed circumstances.

==Synopsis==
Two months after the exchange of nuclear weapons on earth the spacecrafts guidance system is mysteriously reprogrammed, forcing the crews return to earth.  In order to survive, the crew must escape to the radiation free zones while avoiding cannibal terminals and a military school student turned evil despotic ruler.

The spacecraft lands, considerably off-course, on a beach in eastern Canada. Jordan (Kate Lynch) is knocked unconscious on impact. Walker (John Walsch) exits first and is quickly killed by terminals - humans crazed by disease. Several hours later, in the middle of the night, Howe (Tim Choate) ventures out in search of help and a way to escape. He soon encounters Vinny (Maury Chaykin), a survivalist who has fortified his house with barbed wire and booby traps. Vinny effectively saves him from the terminals and makes him his prisoner. As the plot develops, Vinny, JJ (Lenore Zann) (another prisoner), and Howe are captured and taken in chains to a makeshift fortress built out of junk. They must escape before a malfunctioning nuclear warhead explodes in sixty hours.

==See also== The Aftermath

==External links==
* 
*  
*  
 
 

 
 
 
 
 
 
 
 
 
 


 
 