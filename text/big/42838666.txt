Maybe This Time (2014 film)
{{Infobox film
| name           = Maybe This Time
| image          = Maybe This Time 2014.jpg
| caption        = Maybe This Time theatrical movie poster
| director       = Jerry Lopez Sineneng
| producer       = 
| writer         = Melai Monge and Anton Santamaria  . ABS-CBN News. Retrieved 2014-05-22. 
| starring       = {{plainlist|
* Sarah Geronimo
* Coco Martin
* Ruffa Gutierrez
}}
| music          = 
| cinematography = 
| editing        = 
| studio         = Star Cinema; Viva Films
| distributor    = Star Cinema
| released       =  
| runtime        = 
| country        = Philippines English
| budget         = 
| gross          = PHP 134,638,610 
}} Filipino romantic comedy film directed by Jerry Lopez Sineneng starring Sarah Geronimo and Coco Martin. The film, produced by Star Cinema and Viva Films and distributed by Star Cinema officially premiered in the Philippines on May 28, 2014.  . Star Cinema. Retrieved 2014-05-22. 
 pesos on the first day of its release.  . Star Cinema. Retrieved 2014-04-29. 

==Synopsis==
"Steph Asuncion (Sarah Geronimo) and Tonio Bugayong (Coco Martin) were once in love. Back then, she was a young girl who wanted a simple life and he was older, more ambitious than she was. Tonio was a small town guy who wants to board a ship to provide for his family’s furniture business. Sarah was a Manila girl who spends the summer in the province for community service. What might have been a sweet relationship ended sourly when Tonio left without saying goodbye. Steph was heartbroken and it taught her to dream bigger to be worthy of love. Will their paths cross again? Will they overcome the pains of the past to give love a second chance? This is a story between two people who will be reminded about the importance of being true to one’s self in order for true love to happen." - Star Cinema 

==Cast==

===Main cast===
*Sarah Geronimo as Stephanie "Steph"/ "Teptep" Asuncion
*Coco Martin as Antonio "Tonio" Bugayong
*Ruffa Gutierrez as Monica T. Valencia

===Supporting cast===
{{Columns-list|2|
*Dennis Padilla as Erning
*Buboy Garovillo as Butch
*Sharmaine Buencamino as Lenny
*Tony Mabesa as Pancho
*Ogie Diaz as Mama Mae
*Marlann Flores as Mels
*Zeppi Borromeo as Jans
*Alex Castro as Patrick
*Minnie Aguilar as Elma
*Kathleen Hermosa as Clara
*Garlic Garcia as Kooks
*Cecil Paz as Susan
*Devon Seron as Abby
*Aaron Junatas as Hyro
*Sofia Millaresas Kimberly
*Bea Basa as Porky
*Marco Antonio Masaas Danno
*Gaby Dela Mercedas Liana
*Marina Benipayo as Carmen
*Dante Ponce as Andrew
}}

==Development==

===Casting and production===
The announcement of the main casts for Star Cinemas upcoming film was announced on February 8. The details for the coming film was first discussed through a meeting on January 21. Although there are no official announcement for the movie yet, it was already publicized that Geronimo and Martin will be the leads, together with Ruffa Gutierrez, and will be directed by Jerry Lopez Sineneng. 

===Promotions===
On March 1, 2014, the official theme song of the movie accompanied by a music video is released.  The song entitled "Maybe This Time" is released by producer Star Cinema and performed by singer-actress Sarah Geronimo. Few weeks later, the whole team had a grand press conference on May 15 at the Dolphy Theaters to talk about the film and their experiences.   The next day, the first official full-length trailer is released on YouTube, on May 16. 

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 