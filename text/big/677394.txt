23 (film)
 
{{Infobox film
| name           = 23
| image          = 23MovieFrenchDVDCover.jpg
| image_size     = 215px
| alt            =
| caption        = French DVD cover
| director       = Hans-Christian Schmid
| producer       = Jakob Claussen Thomas Wöbke
| writer         = Hans-Christian Schmid Michael Gutmann Michael Dierking
| starring       = August Diehl Fabian Busch Dieter Landuris Jan Gregor Kremp Norbert Jürgen Schneider
| cinematography = Klaus Eichhammer
| editing        = Hansjörg Weißbrich
| studio         = Claussen & Wöbke Filmproduktion GmbH
| distributor    = Buena Vista International
| released       =  
| runtime        = 99 minutes
| country        = Germany
| language       = German English Russian
}} drama Thriller thriller film hacker Hagbard Karl Koch, who died on 23 May 1989, a presumed suicide. It was directed by Hans-Christian Schmid, who also participated in screenwriting. The title derives from the protagonists obsession with the number 23 (numerology)|23, a phenomenon often described as apophenia. Although the film was well received by critics and audiences, its accuracy has been vocally disputed by some witnesses to the real-life events on which it was based. Schmid subsequently co-authored a book that tells the story of the making of 23 and also details the differences between the movie and the actual events. 

==Plot== Karl Koch (August Diehl) finds the world around him threatening and chaotic. Inspired by the fictitious character Hagbard Celine (from Robert Anton Wilson and Robert Sheas Illuminatus! Trilogy), he starts investigating the backgrounds of political and economic power and discovers signs that make him believe in a worldwide Conspiracy theory|conspiracy.
 global data network—which is still, at this point, in its early stages—and their belief in social justice propels them into espionage for the KGB. Driven by contacts with a drug dealer—and by increasing KGB pressure to hack successfully into foreign systems—Karl spirals into a cocaine dependency and grows increasingly alienated from David.

In a drug-addled state, Karl begins to sit in front of his computer for days at a time. Perpetually sleepless, he also grows increasingly delusional. When David publicly reveals the espionage activity in which the two men have been engaged, Karl is left alone to face the consequences. Collapse soon follows. Karl is taken to a hospital to deal with his drug addiction and mysteriously dies afterward.

==Cast== Karl Koch
* Fabian Busch as David
* Dieter Landuris as Pepe
* Jan Gregor Kremp as Lupo
* Stephan Kampwirth as Jochen Maiwald
* Zbigniew Zamachowski as Sergej
* Peter Fitz as Brückner
* Burghart Klaußner as Weber
* Lilly Tschörtner as Beate
* Patrick Joswig as Alex
* Arnulf Schumacher as Seybert

==See also==
* 23 Enigma
* The Number 23
* The Cuckoos Egg – another account of the Hanover Hacker case, written by one of the system administrators who discovered the break-ins
* List of films featuring surveillance

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 