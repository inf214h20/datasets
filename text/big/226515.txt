Jason and the Argonauts (1963 film)
{{Infobox film
| name           = Jason and the Argonauts
| image          = Jason and the argounauts.jpg
| caption        = Theatrical release film poster by Howard Terpning
| director       = Don Chaffey
| producer       = Charles H. Schneer
| writer         = Apollonios Rhodios
| based on       = The Argonautica
| screenplay     = Beverley Cross Jan Read
| starring       = Todd Armstrong Nancy Kovack Honor Blackman Gary Raymond
| music          = Bernard Herrmann
| cinematography = Wilkie Cooper
| editing        = Maurice Rootes
| studio         = Morningside Productions 
| distributor    = Columbia Pictures
| released       = June 19, 1963
| runtime        = 101 minutes
| country        = United States United Kingdom
| language       = English
| budget         = $1 million
| gross          = $2,100,000 (US/ Canada) 
}}
 fantasy Greek mythical Greek Mysterious Island and The 7th Voyage of Sinbad. The working title was Jason and the Golden Fleece.

==Plot== King Aristo and killing him; but learns a prophecy that he will be overthrown by a child of Aristo wearing one sandal. In an attempt to thwart the prophecy, Pelias kills one of Aristos daughters, Briseis (Davinia Taylor), which angers the goddess Hera (Honor Blackman) because the murder profaned her temple. Before Briseis is killed, she places the infant daughter of Aristo, Philomela--her fate left unknown--into the arms of the statue of Hera. Meanwhile, the infant Jason has been rescued by a soldier of Aristos to be raised to manhood to fulfill the prophecy. Zeus, angered by Pelias attempt to confound his designs, determines that Pelias shall fall and the infant son of Aristo shall be his instrument.

Twenty years later ("but an instant of time on Olympus"), Jason (Todd Armstrong), Aristos son grown to manhood, saves Pelias from drowning during a "chance" encounter (orchestrated by Hera), but loses a sandal in the river so that Pelias recognises him. Upon learning that Jason means to obtain the legendary Golden Fleece, Pelias — concealing his identity — encourages him, hoping he will be killed in the attempt.

Jason is taken to Mount Olympus by the god Hermes (Michael Gwynn) to speak with Zeus and Hera.  Hera wishes him well, but tells him that Zeus has decreed that he can only call upon her aid five times.  She directs him to search for the Fleece in the land of Colchis.  Zeus offers his direct aid to Jason, but Jason declares that he can organize the voyage, build the ship, and select a crew of the bravest and most able men in all of Greece by holding an Olympics.  Zeus, observing that those most worthy of the aid of the gods are those who least call upon it, is pleased and sends Jason back to Earth.
 Hercules (Nigel Green), Hylas (John Cairney) and Acastus (Gary Raymond), the son of Pelias, sent by his father to sabotage the voyage.

When supplies run low, Hera guides Jason to the Isle of Bronze, but warns him to take nothing but provisions. While exploring the island, however, Hercules steals a brooch pin the size of a javelin from a treasure chamber surmounted by a statue of Talos, which comes to life and attacks the Argo. Jason again turns to Hera, who tells him to open a cylindrical plug on the back of Talos heel, releasing his ichor. Talos falls to the ground, crushing Hylas and hiding his body. Feeling responsible, Hercules refuses to leave until he ascertains what happened to his friend. The other Argonauts refuse to abandon Hercules, so Jason calls on Hera again. She informs Jason this is the last time she can help him; she confirms that Hylas is dead and that Hercules is not fated to continue with the others. 
 Phineas (Patrick Clashing Rocks, Triton rises from sea foam and holds the rocks apart long enough for the Argo to pass. They pick up three survivors of the other ship, among them Medea (Nancy Kovack).

At Colchis, Acastus and Jason disagree on how to approach the King of Colchis, and eventually fight. Disarmed, Acastus jumps into the sea to escape. Believing him dead, Jason and his men accept an invitation from King Aeëtes (Jack Gwillim) to a feast, where they are captured and imprisoned. Acastus has warned Aeëtes of Jasons quest. However, Medea, enamoured of Jason, helps him and his men escape.
 skeletal warrior from each. When Medea is wounded by an arrow in the resulting battle, Jason uses the Fleece to heal her. He and two of his men hold off the skeletons to enable the others to reach the ship.  When his two companions are killed, Jason jumps off a cliff into the sea. He, Medea, and the surviving Argonauts begin their return to Thessaly.  In Olympus, Zeus tells Hera that in due time he will call upon Jason again.

==Cast==
In credits order
* Todd Armstrong as Jason
* Nancy Kovack as Medea
* Gary Raymond as Acastus Argus
* Niall MacGinnis as Zeus
* Michael Gwynn as Hermes
* Douglas Wilmer as Pelias King Aeëtes
* Honor Blackman as Hera
* John Cairney as Hylas
* Patrick Troughton as Phineus
* Andrew Faulds as Phalerus
* Nigel Green as Hercules
* Ennio Antonelli as Dmitrius (uncredited) John Crawford Polydeuces (uncredited)
* Aldo Cristiani as Lynceus (uncredited) Castor (uncredited)
* Davina Taylor as Briseis (uncredited) Triton (uncredited)

==Musical score== Journey to the Center of the Earth.

Contrasting with Herrmanns all-string score for Psycho (1960 film)|Psycho, the soundtrack to Jason and the Argonauts was made without a string section. This leaves the brass and percussion to perform the heroic fanfares, and the woodwinds along with additional instruments (such as the harp) to dominate in the more subtle and romantic parts.

In 1995, Intrada released a re-recording of the original score. The new version was conducted by American composer/conductor Bruce Broughton, and performed by the Sinfonia of London.

==Differences from classical mythology==
The film differs from the traditional telling in Greek mythology in several ways.
 Queen Europa.

* In the film, Hylas was killed when the crumbling remains of Talos crushed him. However, in mythology, Hylas as actually kidnapped by a water nymph who fell in love with him as he took a drink from a spring. When Hercules couldnt find him, he stayed behind on the island to search for him (this part was accurately portrayed in the film: when Hylas was crushed, Hercules believed him to still be alive, and stayed behind to look for him).

* The harpies were not caught in a net or caged, but were chased away by the   (goddess of the rainbow), promised the Harpies would not bother Phineas anymore. Phineas told the Argonauts how to safely pass the clashing rocks by releasing a dove. If the bird makes it through, he tells them to row with all their might and, according to Apollonius, the goddess Athena gave the Argo the extra push needed to clear them, "the Argo darted from the rocks like a flying arrow", whereas in the film he gives Jason an amulet. Yet in Homers Odyssey, Circe tells Odysseus, "One ship alone, one deep-sea craft sailed clear, the Argo, sung by the world, when heading home from Aeetes shores. And she would have crashed against those giant rocks and sunk at once if Hera, for her love of Jason, had not sped her through." 

* Jason was not betrayed by Acastus in the classical tale. Jason openly told King Aeëtes that he had come for the Fleece. The king promised Jason could have it if he performed three tasks, knowing full well they were impossible. However Jason was able to complete the tasks with the help of Medea. It was Jason himself who sowed the dragons teeth in the ground, not Aeëtes. Jason defeated the skeleton army (the spartoi) by making them fight amongst themselves and destroy each other, rather than the Argonauts battling them.

*One of the two Argonauts killed by the skeletons is Castor and Pollux|Castor, who in Greek mythology would perish much later as the result of a feud with Idas and Lynceus (Argonaut)|Lynceus. The other is Phalerus, who in mythology would also survive the adventures of the Argonauts.

* Medea killing her own brother, Absyrtus, to help Jason and the Argonauts escape, is omitted from the film,  as are the episodes with Cyzicus and the Gegeines and the Argonauts stay on the isle of Lemnos. Ancient mythology suggests King Aristo and Pelias were half brothers; each sharing a common mother Tyro, where the former is a son of her husband Cretheus and the latter, a son of the god Poseidon.

* King Aeetes is Medeas own father and Jason does not kill his uncle Pelias, instead Medea uses her wit and powerful magic to accomplish the task.

* The film ends on a high note with plotlines unresolved and ignores the tragedy and bloodshed which marks the end of the myth.

==Legacy==
In April 2004, Empire Magazine|Empire magazine ranked Talos as the second best film monster of all time, after King Kong. {{cite news | title=King Kong tops movie Monster poll | date=April 3, 2004| publisher=BBC | url=
http://news.bbc.co.uk/1/hi/entertainment/film/3596551.stm}} 
 1992 Academy Awards actor Tom Hanks deemed Jason and the Argonauts to be "the greatest movie of all time". 

Ray Harryhausen regarded this as his best film. Jason and the Argonauts. Culver City: Columbia TriStar Home Video, 1998.   Previous Harryhausen films had been generally shown as part of double features in "B" theatres. Columbia was able to book this film as a single feature in many "A" theatres in the United States. The skeletons shields are adorned with designs of other Harryhausen creatures, including an octopus and the head of the Ymir from 20 Million Miles to Earth.

The film was nominated for AFIs 10 Top 10#Fantasy|AFIs Top 10 Fantasy Films list. 
It currently holds a 96% fresh rating on film review site Rotten Tomatoes, based on 26 reviews. 

==Release== audio commentaries, one by Peter Jackson and Randall William Cook, the other by Harryhausen in conversation with his biographer Tony Dalton. 

==See also==
* Jason and the Argonauts (TV miniseries)|Jason and the Argonauts (TV miniseries)
* List of stop-motion films
* Sword and sandal
* List of historical drama films
* Greek mythology in popular culture
* Argonautica by Apollonius Rhodius third century BC

==References==
 

==External links==
 
*  
*  
*  
*  
*  
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 