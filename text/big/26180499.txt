Dead Cert (1974 film)
{{Infobox film
| name           = Dead Cert
| image          = Dead_Cert_(film).jpg
| caption        = 
| director       = Tony Richardson
| producer       = Neil Hartley
| writer         = John Oaksey   Tony Richardson
| based on       =  
| narrator       = 
| starring       = Scott Antony
| music          = John Addison
| cinematography = Freddie Cooper John Glen
| studio         = Woodfall Film Productions
| distributor    = United Artists Films   MGM Home Entertainment
| released       =  
| runtime        = 99 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
}}  novel of the same name by Dick Francis. 

==Plot==
 

A young jockey investigates on strange deadly accidents that involve his race horse.

==Cast==
* Scott Antony as "Alan York"
* Geoffrey Bateman as "Everest"
* John Bindon as "Walter"
* Joseph Blatchley as "Joe Nantwich"
* Judi Dench as "Laura Davidson"
* Mark Dignam as "Clifford Tudor"
* Mick Dillon as "Jockey" (as Mickey Dillon)
* Bill Fraser as "Uncle George"
* Frank Gentry as "Gipsy"
* Julian Glover as "Lodge" Ian Hogg as "Bill Davidson"
* Sean Lynch as "Sid"
* Hideo Saito as "Yuko"
* Nina Thomas as "Penny Brocker" Michael Williams as "Sandy Mason"

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 


 
 