Looping the Loop
{{Infobox film
| name           = Looping the Loop
| image          = 
| image_size     = 
| caption        = 
| director       = Arthur Robison
| producer       = Gregor Rabinovitch
| writer         = Robert Liebmann   Robert Reinert   Arthur Robison
| narrator       = 
| starring       = Werner Krauss   Jenny Jugo   Warwick Ward   Gina Manès
| music          = Artur Guttmann   Hugo Riesenfeld
| editing        = 
| cinematography = Carl Hoffmann UFA
| distributor    = Parufamet (Germany)    Paramount Pictures (US)
| released       = 15 September 1928
| runtime        = 
| country        = Germany Silent  German intertitles
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}} German silent silent thriller film directed by Arthur Robison and starring Werner Krauss, Jenny Jugo and Warwick Ward.

==Cast==
* Werner Krauss - Botto, ein berühmter Clown 
* Jenny Jugo - Blanche Valette 
* Warwick Ward - Andre Melton, Artist 
* Gina Manès - Hanna, Kunstschützin 
* Sig Arno - Sigi, Hannas Partner 
* Max Gülstorff - Blanches Verwandter 
* Lydia Potechina - Blanches Verwandte 
* Gyula Szöreghy - Ein Agent 
* Harry Grunwald

==Bibliography==
* Bergfelder, Tim & Bock, Hans-Michael. The Concise Cinegraph: Encyclopedia of German. Berghahn Books, 2009.
* St. Pierre, Paul Matthew. E.A. Dupont and his Contribution to British Film: Varieté, Moulin Rouge, Piccadilly, Atlantic, Two Worlds, Cape Forlorn. Fairleigh Dickinson University Press, 2010.

==External links==
* 

 

 
 
 
 
 
 


 
 