Hunt the Man Down
{{Infobox film
| name           = Hunt the Man Down
| image          = Hunt_the_Man_Down_film_poster.jpg
| image_size     = 
| alt            = 
| caption        = Theatrical release poster
| director       = George Archainbaud 
| producer       = Lewis J. Rachmil
| screenplay     = DeVallon Scott
| based on       = 
| narrator       = 
| starring       = Gig Young
| music          = Paul Sawtell
| cinematography = Nicholas Musuraca
| editing        = Samuel E. Beetley
| distributor    = RKO Pictures
| released       =  
| runtime        = 69 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Hunt the Man Down is a 1951 American film noir directed by George Archainbaud starring Gig Young. 

==Plot==
Public defender Paul Bennett (Gig Young) dedicates himself to defending a destitute man accused of murder. Some-time piano player Richard Kinkaid (James Anderson) was brought to trial for the crime 12 years earlier, but he was sure he would be found guilty and escaped before the jury reached a verdict. Now recaptured, hes due to be put on trial again. Bennett is faced with the task of tracking down the seven witnesses to a fight that Kinkaid had with the murder victim before the crime. In the years since the first trial, World War II has come and gone and the lives of the witnesses have undergone major changes, most of them for the worse.

==Cast==
 
 
* Gig Young as Paul Bennett
* Lynne Roberts as Sally Clark Mary Anderson as Alice McGuire / Peggy Linden
* Willard Parker as Burnell Brick Appleby
* Carla Balenda as Rolene Wood
* Gerald Mohr as Walter Long James Anderson as Richard Kincaid / William H. Jackson John Kellogg as Kerry Lefty McGuire Harry Shannon as Wallace Bennett
* Cleo Moore as Pat Sheldon
* Christy Palmer as Mrs. Joan Brian
 
* Iris Adrian as Marie, McGuires Neighbor (uncredited)
* Vince Barnett as Joe, Pool Player (uncredited) 
* Al Bridge as Ulysses Grant Sheldon (uncredited) 
* Frank Cady as Show Box Puppeteer (uncredited)
* Dick Elliott as Happy, Bar Owner (uncredited)  William Forrest as J.P. Knight, Public Defender (uncredited)
* Paul Frees as Packard Packy Collins (uncredited)
* William Haade as Bart (uncredited)  Al Hill as Pete Floogle - Hit Man (uncredited)
* James Seay as Prosecutor (uncredited) 
 

==References==
 

==External links==
*  
*  
*  
 
 
 
 
 
 
 
 
 
 

 