Who Goes Next?
  British war film directed by Maurice Elvey and starring Barry K. Barnes, Sophie Stewart and Jack Hawkins.  During the First World War, a number of captured British officers attempt to escape a prisoner-of-war camp. The story was inspired by the real-life escape of 29 officers through a tunnel from Holzminden prisoner-of-war camp in Lower Saxony, Germany, in July 1918. 

==Cast==
* Barry K. Barnes ...  Maj. Hamilton 
* Sophie Stewart ...  Sarah Hamilton 
* Jack Hawkins ...  Capt. Beck  Charles Eaton ...  Capt. Royde 
* Andrew Osborn ...  F / O Stevens 
* Frank Birch ...  Capt. Grover 
* Roy Findlay ...  Lt. Williams 
* Alastair MacIntyre ...  Lt. Mackenzie 
* Meinhart Maur ...  Commandant

==References==
 

==External links==
* 

 

 
 
 
 
 

 