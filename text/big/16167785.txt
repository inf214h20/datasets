Julie Ganapathi
{{Infobox film
| name = Julie Ganapathi
| image = JulieGanapathyfilm.JPG
| caption = Audio cassette Cover
| director = Balu Mahendra
| based on =  
| writer = Balu Mahendra
| starring = Jayaram Saritha Ramya Krishnan
| producer =
| music = Ilaiyaraaja
| cinematography = Balu Mahendra
| editing = Balu Mahendra
| released = 14 February 2003
| runtime =
| country = India
| language = Tamil
}}

Julie Ganapathi is a 2003 South Indian motion picture. It is a remake of the 1990 American horror film Misery (film)|Misery, itself an adaptation of the Stephen King Misery (novel)|novel. 

==Plot==
Julie Ganapathi (Saritha) is a staunch fan of a popular television show, Manga. Over the years, Julie grows to identify herself obsessively with the main character of the show. The author of the series, Tenkasi Balakumaran (Jayaram), leaves his home for a few days to be alone to write the last few episodes of Manga. On his way back home after completing his work, his vehicle gets into an accident which leaves him seriously injured and crippled. Julie rescues Balakumaran and takes him home but is revealed to be emotionally unstable, and deliriously obsessed with the character in Balas story. Julie asks Bala to allow her to read the scripts for the concluding episodes that he has just completed.

Over the next few days she reads the story and finds the ending not to her liking. She forces him to re-write the ending by various means, forcing him to escape his captors clutches before it is too late. Soon after, Bala becomes aware of how Julie has not informed anyone of Balas situation and is not at all planning to release him anytime soon. As a result, he tries to escape her home. In this, he is caught and suspended to remain in his room. She also disables him from getting up.

Afterwards, a police investigating the disappearance of Bala decides to go to the house of Julie since she has made her love for Manga known around the village. When investigating her home, he discovers that Bala was in fact being kept there. Julie then draws a weapon and kills him. She then informs Bala that they will then die together. Just to get him some time he uses the excuse of having to finish his new version of the last Manga script. With that, he finishes, tricks Julie into getting him a weapon, and with a fight, manages to kill Julie and escape. The film forwards to Bala with his family, discussing to the press his experiences, stating that she was in fact an inspiration to him.

==Cast and crew==
* Jayaram
* Saritha
* Ramya Krishna
* Amarasigamani
* Delhi Ganesh
* Associate director: VetriMaran
* Assistant directors: B. Ravikumar, SenthilKumar

==Soundtrack==
The music was composed by Ilaiyaraja.
* "Minmini Paarvaigall" - K.J. Yesudas
* "Enakku Piditha Paadal" (Female version) - Shreya Ghoshal.
* "Kaakka Kaakka-Anitha"
* "Enakku Piditha Paadal" (Male version) - Vijay Yesudas
* "Idhayamae Idhayamae" - Shreya Ghoshal
* "Thanni Konjam Yeri Irukku" - Prasanna

==References==
 

==External links==
 
*  

 

 
 
 
 
 
 
 
 
 