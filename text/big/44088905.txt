Rakthamillatha Manushyan
{{Infobox film 
| name           = Rakthamillatha Manushyan
| image          =
| caption        =
| director       = Jesey
| producer       = JJ  Kuttikkad
| writer         = VT Nandakumar A. Sheriff (dialogues)
| screenplay     = A. Sheriff Jose Manavalan Joseph
| music          = M. K. Arjunan
| cinematography = Anandakkuttan
| editing        =
| studio         = Kuttikkattil Films
| distributor    = Kuttikkattil Films
| released       =  
| country        = India Malayalam
}}
 1979 Cinema Indian Malayalam Malayalam film, Jose and Manavalan Joseph in lead roles. The film had musical score by M. K. Arjunan.   

==Cast==
*Jayabharathi as Rukmini
*Adoor Bhasi as Ramalinga Chettiyar Jose as Baby
*Manavalan Joseph as Antrayose
*Kanchana as Yamuna
*MG Soman as Shivan Meena as Sumathis mother
*Vidhubala as Sophie
*Veeran as Sumathis father Shubha as Sumathi
*Sankaradi as Appayya/Mathai
*Sukumari as Kamala Ambal
*Jose Prakash as Menon

==Soundtrack==
The music was composed by M. K. Arjunan and lyrics was written by Sathyan Anthikkad. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Etho kinaavinte || Vani Jairam || Sathyan Anthikkad || 
|-
| 2 || Ezhaamkadalinakkareyakkare || Ambili, Chorus || Sathyan Anthikkad || 
|-
| 3 || Thirakal || K. J. Yesudas || Sathyan Anthikkad || 
|}

==References==
 

==External links==
*  

 
 
 

 