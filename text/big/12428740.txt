Investigation Held by ZnaToKi
{{Infobox television film
| bgcolour =
| name = Investigation Held by ZnaToKi
| image = 
| caption = 
| format = Detective
| runtime = 
| creator = 
| director = 
| producer = 
| writer = 
| starring = Georgy Martinyuk Leonid Kanevsky Elza Lezhdey
| music = 
| country = Soviet Union  
| language = Russian
| network = 
| released =
| first_aired = 1971
| last_aired = 2003
| num_episodes = 24
| preceded_by =
| followed_by =
| tv_com_id = 
}} Soviet detective TV-series with two Russian series (2002 and 2003). Total episodes released - 24.

Main characters - investigator Pavel Znamenski, detective Alexandr Tomin and laboratory analyst Zinaida Kibrit were acting together under a group name ZnaToKi (translated as "Experts").

Song by Mark Minkov based on the lyrics Invisible Battle (Nezrimiy Boi - Our mission is both dangerous and difficult, and most invisible at first glance...) by Anatoly Gorokhov features in almost all series. It became an unofficial hymn of the Soviet Militia.

==Original Cast Members==
* Georgy Martinyuk as Pavel Znamenski, Senior investigator
* Leonid Kanevsky as Aleksandr Tomin, Senior Inspector-detective
* Elza Lezhdey as Zinaida Kibrit, laboratory analyst
* Lidia Velezheva as Kitaeva, laboratory analyst (since 2002, after Elza Lezhdeys death)
* Vera Vasilyeva as Margarita Nikolayevna Znamenskaya, Znamenskis mother
* Semen Sokolovskiy as Colonel Skopin, Chief of Investigation department, Znamenskis boss
* Lev Durov as Afanasiy Filippov, Senior Inspector of GAI (traffic police)
* Anatoly Grachov as Mikhail Tokarev «Reserve To», Senior Inspector of OBKhSS (financial police)

===Non-constant cast===
* Boris Ivanov		
* Yuri Katin-Yartsev
* Leonid Bronevoy
* Grigory Lampe as «Surgeon» Kovalski
* Georgy Menglet
* Valery Nosik
* Stanislav Chekan
* Nikolai Karachentsov
* Leonid Markov
* Iya Savvina
* Leonid Kuravlyov
* Armen Dzhigarkhanyan
* Alexander Kaidanovsky

==TV episodes==
* Sledstvie vedut znatoki. Case #1: Black Broker / Chyornyy makler (1971)
* Sledstvie vedut znatoki. Case #2: What Is Your True Name / Vashe podlinnoe imya (1971)
* Sledstvie vedut znatoki. Case #3: Red-handed / S polichnym (1971)
* Sledstvie vedut znatoki. Case #4: A Fault Confessed... / Povinnuyu golovu... (1971)
* Sledstvie vedut znatoki. Case #5: Dinosaur / Dinozavr (1972)
* Sledstvie vedut znatoki. Case #6: Blackmail / Shantazh (1972)
* Sledstvie vedut znatoki. Case #7: Accident / Neschastnyy sluchay (1972)
* Sledstvie vedut znatoki. Case #8: The Escape / Pobeg (1973)
* Sledstvie vedut znatoki. Case #9: Witness / Svidetel (1974)
* Sledstvie vedut znatoki. Case #10: Strike Back / Otvetnyy udar (1975)
* Sledstvie vedut znatoki. Case #11: At Any Price / Lyuboy tsenoy (1977)
* Sledstvie vedut znatoki. Case #12: Bouquet Is Standing By / Buket na priyome (1977)
* Sledstvie vedut znatoki. Case #13: Before The Third Gunshot / Do tretyego vystrela (1978)
* Sledstvie vedut znatoki. Case #14: Herdsboy With A Cucumber / Podpasok s ogurtsom (1979)
* Sledstvie vedut znatoki. Case #15: Went And Never Returned / Ushel i ne vernulsya (1980)
* Sledstvie vedut znatoki. Case #16: From The Life Of A Fruits / Iz zhizni fruktov (1981)
* Sledstvie vedut znatoki. Case #17: Hes Somewhere Here / On gde-to zdes (1982)
* Sledstvie vedut znatoki. Case #18: Midday Thief / Poludennyy vor (1985)
* Sledstvie vedut znatoki. Case #19: The Fire / Pozhar (1985)
* Sledstvie vedut znatoki. Case #20: Boomerang / Bumerang (1987)
* Sledstvie vedut znatoki. Case #21: Without The Knife And A Knuckleduster / Bez nozha i kasteta (1988)
* Sledstvie vedut znatoki. Case #22: Mafia / Mafiya (1989)
* Sledstvie vedut znatoki. Case #23: Arbitrator / Treteyskiy sudiya (2002)
* Sledstvie vedut znatoki. Case #24: Pood Of Gold / Pud zolota (2003)

==External links==
*  ,  ,  ,  ,  ,  ,  ,  ,
*  ,  ,  ,  ,  ,  , 
*  ,  ,  ,  ,  ,  ,  ,  ,   at IMDB

 
 
 
 
 
 