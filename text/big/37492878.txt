Ambara
{{Infobox film
| name            = Ambara
| director        = Sen Prakash
| producer        = Anand Raj Yogesh Bhama
| music           = Abhiman Roy
| cinematography  = Soori
| dialogues       = yogesh shankar narayan
| choreography    = harsha kali malur
| released        =  
| studio          = Aura Cinemas
| editing         = Ravikumar
| language        = Kannada
| country         = India
| budget          = 
}} Yogesh and Bhama in the lead roles. Anand Raj has produced the film under Aura Cinemas banner. Abhiman Roy has scored the music and Soori is the cinematographer. 

==Cast== Yogesh as Ajay
* Bhama as Arundathi
* Harish Raj
* Tilak as Vilas
* Vishwa
* Sadhu Kokila
* Bank Janardhan
* Jai Jagadish
* Ramakrishna
* Padma Vasanthi
* Jayasheela
* Vinayak Joshi
* Sudha Belawadi

==Production==

===Filming===
Ambara is the maiden venture of Aura cinemas headed by Anandraj Bilishivale. Actor Yogesh plays a young college student in the film. Actress Bhama is the romantic interest and a fellow college mate. The film was launched on 23 May at Anjaneya Temple, Bilishivale. Aravind Limbavali MLA and former minister was among the chief guests.

The filming mainly took place in Sri Sivakumara Swamy Technical University. Director, Sen Prakash, with 14 long years of experience in television, documentary and also the director of the film Navika tries a different presentation. Yogish is a college student in the first stage and later reaches to a new height through a friend.  The shooting will also take place in Haridwar and Rishikesh during the final schedule  

==Soundtrack==
Music director Abhiman Roy has composed 7 tracks for the movie.

* "Lovey Thrillingu" - Tippu (singer)|Tippu, Supriya Mohith
* "Saniha Banda mele" - Sonu Nigam, Yashi
* "Hai Hai Re Hai" -
* "Vande Mataram" (bit) - 
* "Poli Poli" -
* "Kaanade Kaanade" -
* "Saniha Banda Mele" (remix) -

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 


 