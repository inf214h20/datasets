The Apocalypse (2007 film)
 
{{Infobox Film name           = The Apocalypse image          = Poster of The Apocalypse (film).jpg caption        = director       = Justin Jones producer       = David Michael Latt David Rimawi Paul Bales screenplay     = David Michael Latt story          = Carlos De Los Rios starring       = Rhett Giles Jill Stapley Kristen Quintrall Tom Nagel music          = cinematography = editing        = distributor    =The Asylum released       =   runtime        = 85 minutes country        = United States language       = English budget         = gross          =
}} American direct-to-DVD Deep Impact, however the storyline is drastically different, a recurring theme of some The Asylums mockbusters.

== Plot ==
The film takes place in the United States in what is presumably the present day. A large, colossal meteorite the size of Texas is heading towards the Earth, and threatens to wipe out the human race in a manner similar to the extinction of the dinosaurs.

Whilst the Earth begins a series of efforts to divert and later destroy the meteorite, two parents – Jason (Rhett Giles) and Ashley (Jill Stapley) – search for their missing child, who is in Los Angeles, while attempting to overcome various catastrophes somehow triggered by the arrival of the meteorite.

Jason and Ashley eventually realize that it is not advanced technology that is required to find their child or divert the meteorite but faith in religion. The daughter is eventually reunited with her father, just at the time the meteorite is about to hit. They tell each other that they will see each other soon, and the film ends with the meteorite colliding with the earth, destroying all life.

== Cast ==
 
* Rhett Giles as Jason
* Jill Stapley as Ashley
* Kristen Quintrall as Lindsay
* Tom Nagel as Andrew
* David Shick as Don
* Amol Shah as Khalil
* Shaley Scott as Amanda
* Kelsey Higgs as Lucia Kim Little as Lynn
* Carissa Bodner as Audrey
* Dean N. Arevalo as Motel Manager
* Sarah Lieving as Janis
* Erica Roby as Laura
* Leigh Scott as Nick
* Jason S. Gray as Banker
 

== Production ==
The Apocalypse was originally written as a straightforward disaster film, but buyers asked for a religious film.  It was rewritten to include faith-based elements, and The Asylum created the Faith Films label to distribute it. 

== See also ==
* 2012 Doomsday, another Asylum film with a similar plot.
* H. G. Wells War of the Worlds (2005 film)|H. G. Wells War of the Worlds, an earlier Asylum film also starring Rhett Giles.

== References ==
 

== External links ==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 

 
 