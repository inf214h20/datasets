Patriotism (film)
{{Infobox Film
| name           = Patriotism
| image          = Patriotism.jpg
| image_size     =
| caption        = Cover of The Criterion Collection DVD
| director       = Yukio Mishima
| producer       = Yukio Mishima Hiroaki Fujii 
| writer         = Yukio Mishima
| narrator       = 
| starring       = Yukio Mishima Yoshiko Tsuruoka
| music          = Richard Wagner (Tristan und Isolde)
| cinematography = Kimio Watanabe
| editing        =
| distributor    =
| released       = April 12, 1966 (Japan)
| runtime        = 28 mins.
| country        = Japan
| language       = Japanese
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
}}

  is a 1966 Japanese short film directed by Yukio Mishima. The English-language release was originally titled The Rite of Love and Death and the French-language release was originally titled  . It is based on Mishimas short story  , published in 1961.   Patriotism, AMC Movie Guide, accessed January 12, 2013 

==Plot==
 Ni Ni Roku Incident of February 1936, Lieutenant Shinji Takeyama has been given orders to execute some of his fellow mutineers. He decides to commit seppuku, and his wife Reiko vows to kill herself with him. They make passionate love, then commit suicide.

==Style==

Patriotism is a silent, thirty minute black-and-white film with long expository intertitles elaborating on the story and its historical background. It contains visual references to Noh theatre, as Mishima admired the traditional style and wrote several plays in the genre. Set in a single room, it is composed of static wide shots and lingering close-ups, most of which obscure Mishimas eyes.   Tom Mes, Midnight Eye, accessed January 12, 2013 

==Cast==

* Yukio Mishima as Lieutenant Shinji Takeyama 
* Yoshiko Tsuruoka as Reiko

==Notes==

On November 25, 1970 Mishima actually committed seppuku, after delivering a speech intended to inspire a coup détat.   Michiko Kakatani, Mishima: Film Examines an Affair with Death, accessed January 12, 2013 

After Mishimas suicide his widow Yōko requested that all existing copies of the film be destroyed. But in 2005 the original negatives were discovered in perfect condition, in a tea box at a warehouse at their home in Tokyo.   Stephanie Goodman, Arts Briefly, accessed January 12, 2013  The film was released on DVD in Japan in 2006, and then in the US by the Criterion Collection in 2008.   Dave Kehr, New DVDs: Mishima and Framed, accessed January 12, 2013 

==References==
 

==External links==
* 
* 
* Essay by Tony Rayns  

 
 
 
 
 
 
 
 
 

 