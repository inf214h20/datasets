You're an Education
 
{{Infobox Hollywood cartoon
| cartoon_name = Youre An Education
| series = Merrie Melodies
| image = Youre_An_Education_reissue_card.png
| caption = Title card for reissue version
| director = Frank Tashlin
| story_artist = Dave Monahan
| animator = A.C. Gamer
| voice_actor = Mel Blanc
| musician = Carl Stalling
| producer = Leon Schlesinger
| distributor = Warner Bros. Pictures
| release_date = November 5, 1938 (USA)
| color_process = Technicolor
| runtime = 7:47 (original version), 6:58 (Blue Ribbon reissue) English
}}
 1938 Merrie Melodies cartoon directed by Frank Tashlin.

== Plot ==

 
The brochures in a travel agency spring to life in this "its-midnight-and-everything-comes-to-life" cartoon, this time in a travel agency with first a bunch of tableaux, followed by a big song, then a crime story.

We see a spinning globe, then the front of the agency store, where we see several displays of banners and posters of different countries. Then as we see one poster/banner for each country, we hear a song tied to the country or a pun on the name. For example, with a picture of Bombay harbor, we see exploding bombs! Theres a little tour of the world at first, with appropriate songs, which then stray into puns about food. "Foods an Education," so we go to Hungary, Turkey, the Sandwich Islands, Hamburg, Chili, Oyster Bay, Twin Forks and Java. The Thief of Bagdad uses the Florida Keys to break into the Kimberly Diamond Mine, and then pawns them with the Pawnee Indians. He is chased by the soldiers and police of different nations, but gets away by forming an "unusual alliance" with the Lone Stranger: "Well, youre not alone now, Beeg Boy!"

== Trivia ==
This was Frank Tashlins last cartoon with WB for five years; he left the studio to become a gag writer with Disney, then moved to Screen Gems to direct. He would return in 1943.

The Blue Ribbon reissue of this cartoon is missing about 45 seconds of footage. The print used on   is the reissue print. The original is presumed to be lost. However, it is not included on the Region 2 release.

This is the third and final entry in Tashlins Books-Come-To-Life trilogy released in 1937–1938.

== External links ==
*  
*  

 
 
 
 
 
 


 