Uran Khatola (film)
{{Infobox Film
| name           = Uran Khatola
| image          = 
| image_size     = 
| caption        = 
| director       = S. U. Sunny
| producer       = Naushad
| writer         = Azm Bazidpuri
| narrator       = 
| starring       = Dilip Kumar Nimmi
| music          = Naushad
| cinematography = Jal Mistry
| editing        = Vasant Borkar
| distributor    = 
| released       = 1955
| runtime        = 
| country        =   India
| language       = Hindi 
| budget         = 
| preceded_by    = 
| followed_by    = 
}}

Uran Khatola is a 1955 Hindi movie produced by music director Naushad and directed by S. U. Sunny. The film stars Dilip Kumar, Nimmi, Jeevan and Tun Tun. The films music is by Naushad. Songs are written by Shakeel Badayuni.
 Tamil and released as Vaanaratham ( ). 

==Plot==
Kashi travels by an ill-fated plane, which crashes on the outskirts of an isolated city that is ruled by women, who worship Sanga, their God. Kashi is rescued by pretty Soni and taken to her home, where she lives with her widowed dad, and brother, Hira. Since the roads are blocked, Kashi is unable to return home, and in order to continue to stay here, he must first obtain permission from the Raj Rani, the ultimate ruler. He meets with her and she finds him attractive and charming, and invites him to stay with her at her palace and sing for him, which he does. Kashi and Soni have given their hearts to each other, they meet secretly, with Soni disguised as a man, Shibu. Watch what happens when Raj Rani finds out that a Kashi is ignoring her love for a mere peasant.

==Cast==
* Dilip Kumar         	 ...	Kashi
* Nimmi         	 ...	Soni / Shibu
* Surya Kumari	 ...	Raj Rani
* Jeevan (actor)       	 ...	Shanu
* Roopmala		
* Amar		
* Tun Tun        	 ...	Hiras girlfriend (as Tuntun)
* Baba		
* Nawab		
* Syedjan		
* Agha (actor)       	 ...	Hira

==Music==
{{Infobox album   
| Name = Uran Khatola
| Type = soundtrack 
| Artist = Naushad 
| Cover = 
| Released = 1954
| Recorded =  Feature film soundtrack 
| Length = 
| Label =  
| Producer =
| Music Director = Naushad 
| Reviews = 
| Last album = Shabaab (film)|Shabaab (1954)
| This album = Uran Khatola (1955)
| Next album = Mother India (1957)
}}
The films music was by maestro Naushad. Vocalists were Mohammed Rafi and Lata Mangeshkar. Some songs were originally recorded by Sudha Malhotra who was replaced by Lata later.
# Chale Aaj Tum - Mohammed Rafi
# Na Ro Ae Dil - Lata Mangeshkar
# Haal-E-Dil Main Kya Kahoon - Lata Mangeshkar
# Na Toofan Se Khelo - Mohammed Rafi
# Ghar Aaya Mehman Koi - Lata Mangeshkar
# Mera Salam Le Ja - Lata Mangeshkar
# Hamare Dil Se Na Jana - Lata Mangeshkar
# Mohabbat Ki Rahon Mein - Mohammed Rafi
# More Saiyan Ji Utrenge Paar - Lata Mangeshkar

== External links ==
*  

 
 
 

 