Paradise (1991 film)
 
 
{{Infobox film
| name = Paradise
| image = Paradiseposter.jpg
| image_size =
| caption = Promotional poster
| director = Mary Agnes Donoghue
| producer = Scott Kroopf Patrick J. Palmer
| writer = Mary Agnes Donoghue
| starring = Melanie Griffith Don Johnson Elijah Wood David Newman
| cinematography = Jerzy Zielinski
| editing = Eva Gardos Debra McDermott
| studio = Touchstone Pictures Interscope Communications Buena Vista Pictures
| released =  
| runtime = 111 minutes
| country = United States
| language = English
| budget =
| gross = $18,634,643 (Domestic)
}} David Newman.

Melanie Griffith and Don Johnson (at the time married to each other) play Lily and Ben Reed, a young couple torn apart by a family tragedy. It would take a miracle to rekindle their love and a miracle arrives in the form of a summer guest - Willard Young (Elijah Wood).

It is a remake of the French film Le Grand Chemin by Jean-Loup Hubert.

==Plot==
Willard Young is a 10-year-old boy who is sent by his mother to stay with her best friend Lily Reed, who lives in the Delta shrimp-fishing country in a town called Paradise. Lily and her husband Ben have been living in an unmentioned emotional vacuum since the death of their own three-year-old boy. Willie makes friends with the local 9-year-old tomboy Billie Pike, who teaches him to be comfortable with himself. When Willard gains a handle on his own emotions, he can now help Lily and Ben to connect, overcome grief and rediscover themselves.

==Cast==
* Melanie Griffith .... Lily Reed
* Don Johnson .... Ben Reed
* Elijah Wood .... Willard Young
* Thora Birch .... Billie Pike
* Sheila McCarthy .... Sally Pike
* Eve Gordon .... Rosemary Young
* Louise Latham .... Catherine Reston Lee
* Greg Travis .... Earl McCoy
* Rick Andosca .... Ernest Parkett

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 


 