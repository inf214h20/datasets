Mookilla Rajyathu
 
{{Infobox film
| name     = Mookkilla Rajathu
| image    = Mookkilyarajyathu.jpg
| caption  = A screenshot from the movie.
| director = Ashokan-Thaha
| producer = Rohini Arts
| writer   = B. Jayachandran Mukesh Siddique Siddique  Jagathy
| editing  = G. Venkitaraman
| studio   = Rohini Arts
| distributor = Rohini Arts
| music    = Ouseppachan
| released =  
| runtime  =
| country  = India
| language = Malayalam
}}
  Jagathy in The Dream Team.

==Plot==

The film starts by displaying an antic of Keshavan (Thilakan) who is an inmate of the Kakkanad Mental Hospital. Two other inmates are Benny (Mukesh) and Krishnankutty (Jagathy Sreekumar), along with the female inmate played by Philomena. The late Hakeem Rawther too plays as an inmate in a cameo role. By profession Benny is an artist, Krishnankutty is an auto-mechanic and Keshavan had retired from military service. The trio chance to come across a newspaper report that the Hindi film star Amitabh Bachchan is visiting Ernakulam City for film shooting, which is near the place they stay. They are soon joined by Venu (Siddique), acting as the brother of Balan (N.N.Balakrishnan) who takes him to the mental hospital. Venu is a good singer but suffers from a compulsive obsession to singing.

Driven by a desire to see Amitabh Bachchan the four of them manage to escape from the mental hospital that night by outsmarting the cell warden Ameen (Mala Aravindan). They reach Ernakulam city in a bus, but are disappointed to learn that Amitabh Bachchan had gone back after the film shooting which took place a month ago, and they had in fact seen an old newspaper report. They decide not to return to the mental hospital but start living in the city by finding suitable employments. The film portrays their many eccentric pranks after their arrival in the city (even though ironically they display normal and even intelligent behavior too much of the time). The ostracization and stigmatization faced by mental patients in the society too is brought to fore in a couple of emotional scenes. The protagonist Benny meets his former college mate and lover Leena (Vinaya Prasad) too there imparting glamour to the plot. Paravoor Bharathan plays as father of Leena, who is unwilling to give his daughter in marriage to Benny, and even unsuccessfully hires a goon Bheem Singh (Krishnan Kutty Nair) to get rid of him.

The story enters a new phase with the arrival of two shady characters Vasu (Kuthiravattom Pappu) and Abdullah (Rajan P Dev) under the pseudo names Sundaran Pillai and Gireesh Puri, in the disguise of a TV serial producer and director respectively. Their intention is to rob a bank near to the home rented by the four, by making a tunnel under their home. For that they hire the four supposedly for their TV serial and send them to learn various skills like martial arts, rock climbing, tree climbing, break dance etc. to keep them away from the house during the day time while the excavation is going on under the supervision of another gang member Bruno (Kunchan). But the Police officer played by Jagadish is tracking their activities. In the end their vicious scheme is busted up by the police officer, assisted by our four heroes who wake up to the reality at last, and the bank robbers end up in police custody leading to a merry ending.

==Cast== Mukesh  - Benny
* Siddique (actor)|Siddique- Venu
* Thilakan - Keshavan / Keshu Jagathy - Krishnankutty
* Vinaya Prasad - Leena
* N.N.Balakrishnan - Balan Krishnan Kutty Nair - Bheem Singh Pappu  - Vasu
* Rajan P. Dev - Abdullah Kunchan - Bruno
* Philomina - Insane Lady
* Oduvil Unnikrishnan - Doctor
* Mala Aravindan - Ameen Suchitra - Dance Teacher
* Jagadish - Police Inspector
* Hakim Rawther - man at mental hospital

== Credits ==
* Directed by Ashokan-Thaha
* Scripted by B. Jayachandran
* Singers: M. G. Sreekumar

==References==
 

==External links==
*  

 
 
 
 