Zatoichi's Pilgrimage
{{Infobox film
| name = Zatoichis Pilgrimage
| image =
| film name      =  {{Infobox name module
| kanji          = 座頭市海を渡る	
| romaji         = Zatōichi umi o wataru
}}
| director = Kazuo Ikehiro
| producer = Ikuo Kubodera
| writer = Kaneto Shindo
| based on       =  
| starring = Shintaro Katsu Michiyo Okusu Isao Yamagata
| music = Ichirō Saitō
| cinematography = Senkichiro Takeda
| editing        = Toshio Taniguchi
| studio         = Daiei Studios
| distributor    =
| released =  
| runtime = 82 minutes
| country = Japan
| language = Japanese
}} Daiei Motion Picture Company (later acquired by Kadokawa Pictures).

Zatoichis Pilgrimage is the fourteenth episode in the 26-part film series devoted to the character of Zatoichi. It has also been known as Zatoichis Ocean Voyage

==Plot==
  88 shrines on Shikoku.  On the road, a man (Igawa) attacks Zatoichi but is killed by him. Zatoichi follows the mans horse back to his home.

==Cast==
* Shintaro Katsu as Zatoichi
* Michiyo Okusu as Okichi
* Isao Yamagata as Boss Tohachi
* Hisashi Igawa as Eigoro
* Masao Mishima as Gonbei
* Kunie Tanaka as storyteller 

==Reception==
===Critical response===
Thomas Raven, in a review for freakengine, wrote that " his film represents another major step forward for the series.  Director Kazuo Ikehiros touch is exactly what Ichis stories need and since this was his third Zatoichi picture, hed honed his skills to a fine point.  It certainly helps that the script is so crisp, as is the inventive cinematography and art direction.  This is certainly one of the best looking of the first fourteen films."   

==References==
 

==External links==
* 
*  
* 
*  review by D. Trull for Lard Biscuit Enterprises 
*  review by Steve Kopian for Unseen Films (16 February 2014)

 

 
 
 
 
 
 
 
 
 

 