A Star Is Born (1976 film)
 
{{Infobox film
| name = A Star Is Born
| image = AStarisBorn1976.jpg
| caption = Theatrical release poster
| director = Frank Pierson
| producer = Jon Peters Barbra Streisand Robert Carson John Gregory Dunne Joan Didion
| starring = Barbra Streisand Kris Kristofferson Gary Busey Paul Williams Barbra Streisand
| editing =  Robert Surtees
| studio = First Artists
| distributor = Warner Bros.
| released =  
| runtime = 140 minutes
| country = United States
| language = English
| budget = $6 million
| gross = $80,000,000 
}} 1937 version drama starring 1954 version was a musical film starring Judy Garland and James Mason. This version was the highest-grossing of the three films.

==Plot==
Esther Hoffman, an aspiring singer/songwriter, meets John Norman Howard, a famous, successful and self-destructive singer/songwriter, whom, after a series of coincidental meetings, she finally starts dating. Believing in her talent, John gives her a helping hand and her career begins to eclipse his.

Writer and director Frank Pierson, in his New West magazine article "My Battles With Barbra and Jon" summarized it this way:

"An actress is a little more than a woman, an actor a little less than a man (Oscar Wilde) ... The woman in our story is ambitious to become a star, but it is not necessary: it can make her happier and richer, but she could give it all away and not be a better or worse person. With stardom she is only a little more than a woman. For the man, his career is his defense against a self-destructive part of himself that has led him into outrageous bursts of drunkenness, drugs, love affairs, fights and adventures that have made him a legend. His career is also what gives him his sense of who he is. Without it, he is lost and confused; his demons eat him alive. Thats why he is a little less than a man. And it is not that her success galls him, or that she wins over him; the tragedy is that all her love is not enough to keep alive a man who has lost what he measures his manhood with; his career." 

And so the conclusion is measured by the theme. He takes his life in the mistaken belief that he will then not drag her down with him.

==Cast==
* Barbra Streisand as Esther Hoffman
* Kris Kristofferson as John Norman Howard
* Gary Busey as Bobbie Ritchie
* Paul Mazursky as Brian Wexler
* Joanne Linville as Freddie Lowenstein
* Oliver Clark as Gary Danziger
* Venetta Fields as One (of the Oreos)
* Clydie King as Two (of the Oreos)
* Sally Kirkland as Photographer
* Marta Heflin as Quentin
* Rita Coolidge as Herself
* Tony Orlando as Himself
* M. G. Kelly as DJ Bebe Jesus
* Uncle Rudy as Mo
* Susan Richardson as Groupie (uncredited)
* Robert Englund  as Marty (uncredited)
* Maidie Norman as Justice of the Peace (uncredited)
* Martin Erlichman as Manager (uncredited)

==Production== Robert Carson with additional contributions by Pierson, John Gregory Dunne and Joan Didion. It also features Gary Busey and Sally Kirkland. Venetta Fields and Clydie King perform as Streisands backing vocalists "The Oreos". Kristoffersons wife Rita Coolidge and Tony Orlando appear briefly as themselves.
 Hollywood film Academy Award, while the 1976 version depicted the heroine winning a Grammy Award instead.

A Star Is Born was co-produced by Streisand and her then-partner  , insisted that Elvis have top billing and asked for a substantial sum of money for the role, even though he had not had an acting role since 1969, and people were unsure of what kind of box office draw he would be, which effectively ended Elviss involvement with the project. Parker also did not want to have Elvis portrayed as having a show business career that was in decline.  This in fact was far from the truth, with Elvis playing to packed auditoriums wherever he toured in the States. Diamond, who knew Streisand and had attended high school with her at Erasmus Hall High School in Brooklyn, was also very seriously considered but had to decline due to his extensive concert commitments - he was at the time, just as he is today, one of the biggest concert performers in the world - and Kristofferson later got the part of John Norman Howard.

The film cost around $6 million to produce. Its soundtrack album was also an international success reaching number 1 in many countries and selling nearly 15 million copies worldwide. It featured the ballad "Evergreen (Love Theme from A Star Is Born)", which became one of the biggest hits of Streisands career, spending three weeks at number one in the United States, and peaking at number three in the United Kingdom. The filming locations included many in Arizona such as downtown Tucson, Arizona|Tucson, Tucson Community Center, Sonoita and Tempe, Arizona|Tempe. 

Streisands characters (Esther Hoffman Howard) clothing was straight out of Streisands own closet. The actual credit reads:  Miss Streisands clothes from... her closet.
 David Winters of West Side Story fame, who worked closely with Streisand to perfect the movies dancing sequences.  

==Reception==

===Critical reception===
A Star Is Born maintains a 31% "Rotten" rating on Rotten Tomatoes based on 16 reviews.  Roger Ebert gave the film two and 1/2 stars. 

===Awards=== Academy Award Best Original Paul Williams, Best Cinematography Robert Surtees), Best Sound Robert Glass Original Music Score (Roger Kellaway).   
 Golden Globe Best Motion Best Actress Best Actor Best Original Best Original Song, (Streisand and Williams for "Evergreen").
 Song of the Year) and an Oscar.

According to at least one Streisand biography , unhappy with a few of Frank Piersons scenes, Streisand later directed them herself (a claim also made for 1979s "The Main Event"), adding to the rumors that she and Pierson clashed constantly during production.

==Remakes== 1937 film. The film was remade in Bollywood as Aashiqui 2 in 2013.

==Home media==
In 2006, the Region 1 DVD was released in North America in Dolby Digital 5.1 sound with extras including a full length commentary by Barbra Streisand, 16 minutes of never before seen and additional footage and the original wardrobe test. In 2007 the Region 2 DVD with the same extras was released in Germany. In 2008 the Region 4 DVD was released in Australia, the content of which appears to be the same as the Region 1 edition. The DVD has yet to be released in any other region.

Warner Bros. released the film worldwide on the Blu-ray format on February 6, 2013.

==Soundtrack==
{{Infobox album  
 | Name = A Star Is Born
 | Type = Soundtrack
 | Artist = Barbra Streisand
 | Cover =
 | Caption =  
 | Released = November 1976
 | Recorded = 1975 to 1976 Classic pop
 | Length = 41:31 Columbia
 | Producer = Barbra Streisand and Phil Ramone
 | Last album  = Classical Barbra   (1976)
 | This album  = A Star Is Born (1976)
 | Next album  = Streisand Superman (1977)
 | Misc = {{Singles
  | Name           = A Star Is Born
  | Type           = soundtrack
  | single 1       = Evergreen (Love Theme from A Star Is Born)  | single 1 date  = November 16, 1976 }}
}}
{{Album reviews rev1 = Allmusic rev1score =    rev2 = The Village Voice rev2Score = D+   
}}
 Release Me. The import version of the CD adds the Spanish version of Evergreen as a bonus track.

The track listing of the album is as follows: Paul Williams, Kenny Ascher) – 3:49
# "Queen Bee" (Rupert Holmes) – 3:55
# "Everything" (Rupert Holmes, Paul Williams)– 3:50
# "Lost Inside of You" (Barbra Streisand, Leon Russell)– 2:54
# "Hellacious Acres" (Paul Williams, Kenny Ascher) – 2:58
# "Evergreen (Love Theme from A Star Is Born)" (Barbra Streisand, Paul Williams) – 3:04
# "The Woman in the Moon" (Paul Williams, Kenny Ascher) – 4:49
# "I Believe in Love" (Music by Kenny Loggins, Lyrics by Alan Bergman,  Marilyn Bergman)– 3:13
# "Crippled Crow" (Donna Weiss)– 3:30
# "Finale: With One More Look at You/Watch Closely Now" (Paul Williams, Kenny Ascher) – 7:43
# "Reprise: Evergreen (Love Theme from A Star Is Born)" – 1:46
# "Evergreen (Love theme from A Star Is Born)"- (Spanish version)- 3:05-(Import version only)

It should be noted that the songs that appear on the soundtrack are in some cases alternate (live) and studio recordings that do not appear in the film.

 Charts and sales 
{| class="wikitable"
!Year
!Chart
!Position
!Sales
!Certifications
|- 1977
|Billboard 200 1
|4,000,000 4 x Platinum   
|- UK Album Chart 1 
|300,000 Platinum
|- Australian Album Chart 3
|140,000 2 x Platinum   
|- Canadian Album Chart 1
|500,000 5 x Platinum   
|- Norwegian Album Chart 10
|
|
|-
|}

 
{{succession box Wings
  | title  = Billboard 200|Billboard 200 Number-one albums of 1977 (USA)|number-one album
  | years  = February 12 – March 25, 1977
  | after =    by Fleetwood Mac
}}
{{succession box The Muppet Show by The Muppets number one album 
  | years  = July 2–9, 1977
  | after =  The Johnny Mathis Collection  by Johnny Mathis
}}
 

==References==
 

== External links ==
 
*  
*  
*  
*  
*   Barbra Streisand Archives
*  , New West magazine article by director Frank Pierson
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 