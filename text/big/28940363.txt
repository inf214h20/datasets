Nightfur
 

{{Infobox film
| name           = Nightfur
| image          = NightfurPosterWiki.jpg
| alt            =  
| caption        = Theatrical Release Poster
| director       = Jason Corgan Brown
| producer       = Jeter Rhodes Jason Corgan Brown
| screenplay     = Jason Corgan Brown
| story          = Jason Corgan Brown Jeter Rhodes
| starring       = Jeter Rhodes Creighton Barrett Jana Danae
| music          = Band Of Horses The Parson Redheads The Stevenson Ranch Davidians Lucy Langlas The Karabal Nightlife
| cinematography = 
| editing        = 
| studio         = Corgan Pictures
| distributor    = Vanguard Cinema
| released       = 2011
| runtime        = 90 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} Jeter Rhodes, Creighton Barrett, and Jana Danae. The feature film blends new wave cinema, romance, and mysticism with science fiction and fantasy.  Nightfur features music by Band Of Horses, The Parson Redheads, The Stevenson Ranch Davidians, Lucy Langlas, The Karabal Nightlife, and more.

==Plot== Jeter Rhodes) Jana Danae) from the metaphysical side of the fence, has been taken in by the doctor. She is found to be sensitive to a mysterious signal coming from the depths of space. The unexplainable phenomena that occur force Frank and Dr. Roberts to look at life from a different, unscientific, angle. When the womans forest dwelling spiritual guide enters the mix they find that jealousy, logic, and love become easily entangled in the presence of the unknown.

==Cast==
* Jeter Rhodes as Frank
* Creighton Barrett as John Moon
* Jana Danae as Helen
* Callan Curtis as Judy
* Phillip Brooks as Roadside Salesman
* Sarah Penrod as Girl in the Park
* Matt Groller as Yellow Face Man
* Meredith Brooks as Young Helen

==Reception==
Nightfur received mostly positive reviews when it was initially released in 2011.  The feature was quickly picked up by Vanguard for a December 20, 2011 North American release.

Jeremy Biltz of DVD Talk said "Nightfur is like a pleasant dream... The direction is confident. Writer / director Jason Brown definitely knows where hes going and what he wants to say... Nightfur is recommended for anyone looking for a film very different from what usually screens at the multiplex." 

Dustin Jansick of Way Too Indie wrote that Nightfur is "An enjoyable as well as entertaining romantic sci-fi."

==References==
 

==External links==
* 
* 

 
 
 
 
 
 