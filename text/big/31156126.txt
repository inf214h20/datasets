Makane Ninakku Vendi
{{Infobox film 
| name           = Makane Ninakku Vendi
| image          =
| caption        =
| director       = EN Balakrishnan
| producer       =
| writer         = Parappurathu
| screenplay     = Parappurathu
| starring       = Prem Nazir Sheela Kaviyoor Ponnamma Adoor Bhasi
| music          = G. Devarajan
| cinematography = GV Ramani
| editing        = 
| studio         = Premier Pictures
| distributor    = Premier Pictures
| released       =  
| country        = India Malayalam
}}
 1971 Cinema Indian Malayalam Malayalam film, directed by EN Balakrishnan . The film stars Prem Nazir, Sheela, Kaviyoor Ponnamma and Adoor Bhasi in lead roles. The film had musical score by G. Devarajan.   

==Cast==
 
*Prem Nazir as Sam, Thomachan (double role)
*Sheela as Chinnamma
*Kaviyoor Ponnamma as Thomachans mother
*Adoor Bhasi as Kuruvilla Ashaan
*Thikkurissi Sukumaran Nair as Mammachan Prema as Saramma
*T. S. Muthaiah as Peelippochan
*Arathi as Sophia Khadeeja as Kalyani
*Kottarakkara Sreedharan Nair as Mathachan
*Philomina as Achamma
*Ushanandini as Mary
*TR Omana as Eali Meena as Martha
*Jose Prakash as Pappachan
*Nellikode Bhaskaran as Chandikunju
*Sreelatha Namboothiri as Mariakutty
*Shobha as Young Sophia
 

==Soundtrack==
The music was composed by G. Devarajan and lyrics was written by Vayalar Ramavarma. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Baavaakkum Puthranum || P Susheela, Renuka || Vayalar Ramavarma || 
|-
| 2 || Irunooru Pournami || K. J. Yesudas || Vayalar Ramavarma || 
|-
| 3 || Maalaakhamaar || P Susheela || Vayalar Ramavarma || 
|-
| 4 || Ponmaane || P Jayachandran || Vayalar Ramavarma || 
|-
| 5 || Sneham Virunnu Vilichu || P. Madhuri || Vayalar Ramavarma || 
|}

==References==
 

==External links==
*  

 
 
 

 