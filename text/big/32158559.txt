Álom.net
{{Infobox film
| name           = Álom.net
| image          =
| director       = Gábor N. Forgács
| producer       = József Cirkó Edina Salla
| writer         = Gábor N. Forgács
| starring       =  Lilla Labanc Kinga Czifra Ádám Csernóczki
| music          = Zsolt Kárpáti Zoltán Makai
| cinematography = Zsolt Tóth
| editing        = Gergely Roszik
| studio         = HCC Media Group Parabel Studio
| released       =  
| runtime        = 90 minutes
| language       = Hungarian
}} Hungarian romantic comedy film written and directed by Gábor N. Forgács.

==Synopsis==
Regina, the once popular girl has to make new friends at her new, conservative school. Problems arrive when she becomes enemies with Lívia, the schools queen bee, and falls in love with Márk, a musician. If these werent enough, she decides to organize a cheerleader team.

==Cast==
*Lilla Labanc        ... Regina
*Kinga Czifra        ... Lívia
*Ádám Csernóczki     ... Márk
*Attila Jankóczky    ... Dávid
*István Széni        ... Gábor
*János Szücs         ... Tomi
*Zoli Ádok         ... Geri
*Arnold Tarsoly      ... Pityu
*Dorottya Farsang    ... Vivien
*Eszter Iszak        ... Szandra
*Ottília Lerch       ... Laura
*Petra F. Tóth       ... Andrea
*Laura Marsi         ... Orsa
*Kristóf Steiner     ... Krisztián
*Ildikó Incze        ... Katalin
*Pál Oberfrank       ... András
*István Tamási       ... Ricsi
*Bence Balázs        ... Niko
*Richárd Reiter      ... Szabi
*Szandra Szabó       ... Detti
*Zsófi Komáromi      ... Alíz
*Réka Nádai          ... Krisztina
*Gábor Reviczky      ... School principal
*Károly Rékasi       ... Mr. Károly
*Éva Mészáros        ... Auntie Ica
*Ágnes Bánfalvy      ... Márta
*László Balog        ... Student 1.
*Erika Dankai        ... French teacher
*Csaba Horváth       ... Waiter
*Katalin Koller      ... Vanda
*Szilvia Laborczy    ... Secretary
*Roland Patai        ... Roland
*Dominika Potyó      ... Dominika
*David Szebeni       ... Assistant commentator
*Tamás Szebeni       ... Commentator
*Zoltán Szujó        ... Commentator

==Reception==
Álom.net received extremely lot and widely different reviews in Hungary.

==See also==
*List of films considered the worst

== External links ==
*  

 
 
 
 

 