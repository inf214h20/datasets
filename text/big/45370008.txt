Uncle Tom's Cabin (1910 Thanhouser film)
 
 
{{Infobox film
| name           = Uncle Toms Cabin 
| image          = Uncle Toms Cabin TC1.jpg
| caption        = A surviving film still.
| director       = 
| producer       = Thanhouser Company
| writer         = 
| narrator       =
| starring       =
| music          =
| cinematography =
| editing        =
| distributor    =
| released       =  
| runtime        =
| country        = United States English inter-titles
}} silent short short drama Uncle Toms Cabin. This prompted the Thanhouser Company to advertise against the Vitagraph film by referring to the other as being overly drawn out. The film garnered mixed, but mostly positive reception in trade publications. The film is presumed lost film|lost. 

== Plot ==
Though the film is presumed  . But Uncle Tom cannot resist a mothers pleading, and when Eliza entreats him to give her back her child he does so and aids her to escape with him."   

"For this deed he is beaten by Legree and forced to join the bloodhounds in which Legree institutes to recover the slave. Eliza, with her boy in her arms, escapes over the Kentucky border to Ohio, a free state, making a perilous crossing on one block of ice to another on the Ohio River. Terribly overcome by the cold and faint from exposure, Eliza is carried unconscious to the home of Senator Bird of Ohio. Tracked down by the purchaser, Simon Legree, to Birds home, Mr. Bird out of goodness buys the boy and, giving him his freedom, gives him to his mother. Uncle Tom is not fortunate enough to find another purchaser and is taken by Legree to the plantation in Mississippi, finding on the trip that the new owner has taken a dislike to him and treats him with great brutality." 

During his journey, while waiting for a Mississippi steamboat, Uncle Tom first meets little Eva, who with her father is also taking the boat south. Tom is at once attracted to the beautiful little girl, and she in turn talks to the kindly old darkey. While looking at the boats, the little one accidentally falls into the swiftly flowing river and escapes drowning only through the bravery of Uncle Tom. He of all the crowd has the courage to jump in and rescue the little girl. Evas father to reward Tom for his bravery, buys him from Legree, and once more Tom knows what it is to be treated kindly. He lives happily as little Evas special bodyguard until the little one is seized with a sudden sickness and dies. She had become greatly attached to Uncle Tom, and the last act of her life was to present him with a little locket containing her picture. Once more Uncle Tom is sold and again falls into the hands of Simon Legree. He is taken to Legrees plantation in Mississippi, where he is overworked and ill treated to the point of death. Just before he dies he presses to his lips the locket with the picture of his beloved little girl and in a vision sees her in the clouds holding out her arms to him that he, too, may enter with her the pearly gates, inside of which all souls are equal, and all free. The comedy of the story is furnished by little Evas Aunt Ophelia, a queer old lawyer named Marx, and his stubborn donkey, to say nothing of Topsy, a wicked little colored girl, who Aunt Ophelia tries hard to convert." 

== Cast ==
*Frank H. Crane as Uncle Tom  
*Anna Rosemond as Eliza   
*Marie Eline as Little Eva   
*Grace Eline as Topsy  

== Production ==
  Romeo and Juliet.   

 .  The 1914 Minot Films Inc. release was not produced by Thanhouser. 

== Release and reception ==
The one reel drama, approximately 1000 feet long, was released on July 26, 1910.  The release of the film fell on the same date of   is cited in the The Complete Index to Literary Sources in Film.    According to Bowers, most Thanhouser posters of this era were of one-sheet size, but an additional six-sheet format poster was created for Uncle Toms Cabin.  The film likely had a wide national release, but specific identification of the Thanhouser film is muddied by the prevalence of Vitagraphs production and stage plays. Two advertisements in Indiana and Kansas specifically note that the film to be shown will be the Thanhouser production.  

By the time of the films release, the quality of the Thanhouser films in general stood out amongst the Independent producers. An editorial by "The Spectator" in the Mirror contained specific praise for Thanhouser productions by stating, "...practically all other Independent American companies, excepting Thanhouser, show haste and lack of thought in their production. Crude stories are crudely handled, giving the impression that they are rushed through in a hurry - anything to get a thousand feet of negative ready for the market. Such pictures, of course, do not cost much to produce, but they are not of a class to make reputation. The Thanhouser company, alone of the Independents, shows a consistent effort to do things worthwhile..."    The editorial was written by Frank E. Woods of the American Biograph Company, a Licensed company, and like the publication itself had a considerable slant to the Licensed companies.  

Still, the film received mixed praise from trade publications. The Mirror commended the film for the clever adaptation which preserved coherency in the story, the good acting and adequate costuming despite a few flaws in the production. Among the errors highlighted by the Mirror was Elizas ice scene and another scene where the black actors wore inaccurate attire such as a laborer who wore a fine shirt and tie.  The The Morning Telegraph reviewer did not care for the production much, finding issues with the adaptation, the acting and the scenes.  The Moving Picture News was more positive in its review, praising the photography and the sensible adaptation of the book to the film format. Three different reviews in The Moving Picture World were published, each of which were positive. In contrast to the fault found in the Mirror, one of the reviewers specifically praised the ice scene by stating, "This part of the picture seems to have been conceived and carried out with extraordinary realism. You see the snow falling, the ice floes cracking and moving, you felt the danger that the woman and child were running as they made their escape across the ice, pursued by their remorseless enemy through the blinding snowstorm. This is one of the finest effects I have seen on the moving picture screen, and the whole story, so far as I have seen it, is worked out with wonderful realism, effect and verisimilitude. Its as good a rendering of the subject as I have ever seen, and I have seen Uncle Toms Cabin played several times."   The Leavenworth Times of Leavenworth, Kansas had an article reviewing the production and offered much praise for adaptation. 

==Notes==
 

== References ==
 

 
 
 
 
 
 
 
 
 