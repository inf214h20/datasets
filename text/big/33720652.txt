Goon (film)
{{Infobox film
| name           = Goon
| image          = Goonfinalposter.jpg
| caption        = Theatrical release poster
| alt            =
| director       = Michael Dowse
| producer       = {{Plain list |
* Don Carmody
* David Gross
* Jesse Shapira
* André Rouleau
* Jay Baruchel
* Ian Dimerman
}}
| writer         = {{Plain list |
* Jay Baruchel
* Evan Goldberg
}}
| starring       = {{Plain list |
* Seann William Scott
* Jay Baruchel
* Alison Pill
* Marc-André Grondin
* Kim Coates
* Eugene Levy
* Liev Schreiber
}}
| music          = Phil Costello   Edith Butler
| cinematography = Bobby Shore
| editing        = Reginald Harkema
| studio         = No Trace Camping   Caramel Film   Don Carmody Productions   Inferno Pictures Inc.
| distributor    = Alliance Films (Canada)   Magnet Releasing (US)
| released       =  
| runtime        = 92 minutes
| country        = United States Canada
| language       = English
| budget         =
| gross          = $6,985,158   
}} 2011 Canadian-American enforcer for a minor league ice hockey team.

A sequel is in development. In 2012 Michael Dowse was announced as director for Goon 2.   

==Plot== fight and one of their players climbs into the stands, calling him a homosexual slur. Doug, whose brother is gay, steps in and easily beats up the opposing player. Soon after, Doug gets a phone call from the coach of his hometown team who offers him a job as an enforcer (ice hockey)|enforcer.
 concussed the Halifax Highlanders, to protect Laflamme and be his roommate.

The Highlanders experience success with Doug as their enforcer, and he quickly becomes popular among fans and teammates, much to the chagrin of his parents and Laflamme, who loses ice time and the Alternate captain|alternate-captaincy to Doug. Doug becomes romantically involved with Eva (Alison Pill), a hockey fan with a penchant for players.

With four games left on their schedule, the Highlanders need two wins to secure a playoff spot. On a road game in Quebec, after an opposing player concusses Laflamme with a heavy hit, Doug savagely beats the player unconscious and is suspended for the next game against Rhea and the St. Johns, Newfoundland|St. Johns Shamrocks. Doug encounters Rhea at a diner, where Rhea dismisses Dougs claim that he is a hockey player, calling him a goon. Though Rhea acknowledges Dougs physical prowess and gives Doug his respect, Rhea warns him that if they ever meet on the ice, he will "lay   the fuck out." The Highlanders, with Doug suspended and Laflamme hospitalized, lose to the Shamrocks.

Doug reaches out to Laflamme, and promises him he will always have his back on the ice. In their next game, the Highlanders lead 1–0 thanks to strong teamwork between Doug and Laflamme. In the final seconds, Doug blocks a slapshot with his face and his ankle is injured in the ensuing scramble. The Highlanders win, but need a win against Rhea and the Shamrocks in their last game for a playoff spot.
 natural hat trick, giving the Highlanders a 3–2 lead. As the game enters its final minute, the final scene has Eva comforting Doug in the locker room as he comments, "I think I nailed him."

==Cast==
* Seann William Scott as Doug "The Thug" Glatt
* Jay Baruchel as Pat
* Alison Pill as Eva
* Liev Schreiber as Ross "The Boss" Rhea
* Marc-André Grondin as Xavier Laflamme
* Eugene Levy as Dr. Glatt
* David Paetkau as Ira Glatt
* Kim Coates as Coach Ronnie Hortense
* Jonathan Cherry as Goalie Marco "Belchie" Belchior
* Ricky Mabe as John Stevenson David Lawrence as Richard
* Ellen David as Mrs. Glatt
* Geoff Banjavich as Brandon Mike Smith from Trailer Park Boys make a cameo appearance as Pats production team
* Nicholas Campbell as Rollie Hortense
* Richard Clarkin as Highlanders Captain Gord Ogilvey
* Karl Graboshas as Oleg
* George Tchortov as Evgeni
* Georges Laraque as Huntington

==Production notes== Doug Smith. Footage from Smiths career as an enforcer is shown during the films credits, and Smith said in an interview with Grantland.com that he is happy with the finished film.  The book was discovered by Jesse Shapira and his producing partner David Gross. Along with Baruchel and Goldberg, they developed the script and then proceeded to package and independently finance the movie. It was the first film under their No Trace Camping banner. 
 NHL enforcer Georges Laraque has a small role as an enforcer for the Albany Patriots. His character fights both Glatt and Rhea over the course of the film. He draws with Glatt and the outcome of his fight with Rhea is not shown. When Laraques character fights Glatt, the dialogue closely resembles the dialogue used by Georges Laraque in a fight against Raitis Ivanāns. 

Goon was filmed in Brandon, Manitoba|Brandon, Portage la Prairie and Winnipeg, Manitoba.  Most of the hockey scenes were filmed at the Portage Credit Union Centre in Portage la Prairie, even though the Halifax Metro Centre was shown as the home of the Halifax Highlanders.  Some scenes were also filmed at the MTS Centre in Winnipeg and the Keystone Centre in Brandon.    
 an incident in the NHL when Marty McSorley of the Boston Bruins slashed Donald Brashear of the Vancouver Canucks in the head from behind. McSorley was suspended 23 games for the incident, which was extended to a full year after he was convicted of assault and sentenced to 18 months of probation. McSorley would never play another NHL game. Due to the slash and the fall on the ice, Brashear suffered a concussion, the same injury Laflamme suffered at the hands of Rhea in an unrelated incident.
 beat a fan with the fans own shoe. Milbury was suspended for six games for his involvement in the altercation. His teammates Terry OReilly and Peter McNab were suspended for eight and six games respectively, while the fan was sentenced to six months in jail. All of them were also punished with $500 fines.  It was also similar to an event where a fan jumped into the penalty box with Tie Domi of the Toronto Maple Leafs.

The teams Doug Glatt plays for over the course of the film both reference the Philadelphia Flyers, an NHL team that became infamous for physically intimidating their opponents during the 1970s to go along with their high level of skill. Glatts home team, the Orangetown Assassins, wear a uniform similar to the ones the Flyers wore during the 1980s, including the orange and black color scheme. The Halifax Highlanders employ a logo that is structured as the letter H with a circle signifying a puck on the right and wings on the left side of the letter; the Flyers logo has a similar design but with the letter P. The Highlanders team colors and name, however, are similar to those of the New York Islanders.
 Detroit Red Vancouver Canuck in the back of the neck, effectively ending his NHL career. Later during the press conference, Bertuzzi tearfully apologized for his actions that had him fined $500,000 and suspended for 17 months.   

==Marketing==
A Trailer (film)#Rating cards|red-band trailer for the film was released on IGN. 

In Toronto and Montreal, prior to its premiere, posters for the film were removed from city bus shelters after several complaints from the public due to Baruchel making a "sexually suggestive gesture with his tongue and fingers."  

 

==Critical reception==
The film has received positive reviews. Rotten Tomatoes has rated the film "Certified Fresh" with a score of 82% based on reviews from 103 critics.  Metacritic gives the film a score of 64 based on reviews from 21 critics. 

Stephen Holden, writing for The New York Times gave a positive review that credits all the major performances. 
Goon was nominated for three awards at the   for Achievement In Direction, Jay Baruchel and Evan Goldberg for Best Adapted Screenplay, and Jay Baruchel and Kim Coates, both for Performance By An Actor In A Supporting Role.   

The timing of the film’s release was considered controversial by some as the previous summer featured the deaths of three NHL enforcers – Derek Boogaard, Rick Rypien and Wade Belak – all three of whom suffered from depression and head trauma (both of which were caused by their role as an enforcer) that are believed to be factors in their deaths.   

==Sequel==
Baruchel is in the process of writing the sequel with Jesse Chabot. Michael Dowse will return to direct and Evan Goldberg will produce the sequel.   

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 