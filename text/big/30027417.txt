Vater Morgana
{{multiple issues|
 
 
}}

{{Infobox film
| name = Vater Morgana
| image = Vater Morgana.jpg
| writer = Till Endemann Daniel Schwarz
| producer = Douglas Welbat
| director = Till Endemann
| released =  
| runtime = 93 minutes
| country = Germany
| language = German
| music = Daniel Welbat
}}
Vater Morgana is a 2010 German comedy film by director Till Endemann with the actors Christian Ulmen, Michael Gwisdek and Felicitas Woll in the central characters – produced by the movie Company and Warner Bros. Film Productions Germany, released by Warner Bros.

== Plot ==
Lutz is actually happy with his life and wants to propose to his girlfriend Annette, but suddenly his father Walther emerges. Always when he surfaced in Lutz life, he played havoc and just made problems. And also now, he confuse everything and unfortunately Lutz has to notice, that his father has Alzheimers.

== Cast ==
* Christian Ulmen as Lutz Stielike
* Michael Gwisdek as Walther Stielike
* Felicitas Woll as Annette Diercks
* Marc Hosemann as Lothar Rehberg
* Ulrike Krumbiegel as Britta
* Michael Lott as Holger
* Heinz Hoenig as Guenther Diercks
* Hans Peter Hallwachs as Edgar Moebius
* Aykut Kayacik as Fazeli
* Friederike Kempter as Kerstin
* Hans Werner Olm as Spliss
* Annalena Schmidt as Frau Dr. Beer
* Alexander Stamm as Axel
* Kyra Mladeck as Frau von Markwirtz
* Hans-Peter Korff as Juwelier Jensen
* Harald Maack as Eberhard

== Production ==
The principal filmings was in August and September 2009 in Hamburg, Germany. The producer, Douglas Welbat, also produced 7 Dwarves – Men Alone in the Wood.

== External links ==
*  

 
 
 
 
 


 
 