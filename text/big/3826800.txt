L'Infermiera
{{Infobox film
| name           = LInfermiera
| image          = LInfermiera poster.jpg
| caption        = Spanish original film poster
| director       = Nello Rossati
| producer       = Carlo Ponti
| writer         = Claudia Florio Nello Rossati
| starring       = Ursula Andress Duilio Del Prete Jack Palance Luciana Paluzzi
| music          = Gianfranco Plenizio
| cinematography = Ennio Guarnieri
| editing        = 
| distributor    = Interfilm Italia   Mid Broadway United States-1979
| released       =   
| runtime        = 105  minutes
| country        = Italy
| language       = Italian 
| budget         = 
| preceded by    = 
| followed by    =

}}

LInfermiera is a 1975 commedia sexy allitaliana film starring Ursula Andress, Jack Palance and Luciana Paluzzi, also known by the titles I Will If You Will, The Nurse, The Sensuous Nurse and The Secrets of a Sensuous Nurse.

==Plot==

As an aging widower begins suffering from heart trouble, his greedy heirs hope to speed him on his way by hiring a seductive nurse (Andress) to get his pulse racing. Their plan eventually backfires as the young beauty begins to fall in love with the old man.

== Cast ==
*Ursula Andress as Anna
*Jack Palance as Mr. Kitch
*Luciana Paluzzi as Jole Scarpa
*Duilio Del Prete as Benito Varotto
*Mario Pisu as Leonida Bottacin
*Daniele Vargas as Gustavo Scarpa
*Carla Romanelli as Tosca Floria Zanin
*Marina Confalone as Italia Varotto
*Stefano Sabelli as Adone
*Lino Toffolo as Giovanni Garbin
*Attilio Duse as Doctor Pavan

==External links==
* 

 
 
 
 
 
 
 


 
 