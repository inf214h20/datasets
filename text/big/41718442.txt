Cross My Heart (1946 film)
{{Infobox film
| name =  Cross My Heart 
| image =
| image_size =
| caption = John Berry
| producer = Harry Tugend 
| writer = Louis Verneuil  (play)   Georges Berr (play)   Claude Binyon   Charles Schnee   Harry Tugend
| narrator = Rhys Williams   Ruth Donnelly 
| music = Robert Emmett Dolan   Stuart Thompson
| editing = Ellsworth Hoagland  
| studio = Paramount Pictures
| distributor = Paramount Pictures
| released = December 18, 1946
| runtime = 85 minutes
| country = United States English 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}} John Berry Rhys Williams. It was a remake of the 1937 film True Confession, which was itself based on an earlier French play. 

==Synopsis==
A secretary is wrongly accused of murdering her boss.

==Cast==
* Betty Hutton as Peggy Harper 
* Sonny Tufts as Oliver Clarke   Rhys Williams as Prosecutor  
* Ruth Donnelly as Eve Harper  
* Al Bridge as Det. Flynn  
* Iris Adrian as Miss Baggart  
* Howard Freeman as Wallace Brent  
* Lewis L. Russell as Judge  
* Michael Chekhov as Peter  

==References==
 

==Bibliography==
* Fetrow, Alan G. Feature films, 1950-1959: a United States Filmography. McFarland & Company, 1999. 

==External links==
* 

 
 
 
 
 
 
 
 
 
 
 

 