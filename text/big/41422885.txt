As the Light Goes Out
{{Infobox film
| name           = As the Light Goes Out
| image          = As the Light Goes Out poster.jpg
| border         = 
| alt            = 
| caption        = Hong Kong poster
| director       = Derek Kwok
| producer       = {{plainlist|
*Albert Lee
*David Chan
*Zhao Jun }}
| writer         = {{plainlist|
*Derek Kwok
*Jill Leung
*Philip Yung }}
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = Nicholas Tse   Shawn Yue   Simon Yam   Hu Jun
| music          = {{plainlist|
*Teddy Robin
*Tommy Wai}}
| cinematography = Jason Kwan 
| editing        = {{plainlist|
*Wong Hoi
*Matthew Hui }} Media Asia Films   Zhujiang Film Group
| distributor    = Emperor Motion Pictures   Huaxia Film Distribution
| released       =   
| runtime        = 116 minutes 
| country        = Hong Kong   China 
| language       = Cantonese   Mandarin   English
| budget         = 
| gross          = US$15,973,348 
}}
As the Light Goes Out (Chinese: 救火英雄) is a 2014 Hong Kong-Chinese disaster film directed by Derek Kwok and starring Nicholas Tse, Shawn Yue, Simon Yam and Hu Jun. 

==Cast==
* Nicholas Tse as He Yong Sen (Sam)
* Shawn Yue as You Bang Chau
* Simon Yam as Lee Pei Dau
* Hu Jun as Hai Yang
* Michelle Bai as Yang Lin
* William Chan as Chang Wen Jian
* Andy On as Ye Zhi Hui Patrick Tam as Mr Wan
* Liu Kai-chi as Tam Sir
* Deep Ng as Ben Sir
* Michelle Wai as Power plant employee
* Kenny Kwan as Power plant employee
* Alice Li as Emily
* Jackie Chan as Himself (cameo) 
* Andrew Lau as Director of Fire Services (cameo) 
* Siu Yam-yam as Wife of wine brewer owner (cameo) 

==Release==
As the Light Goes Out was released in Hong Kong on January 2 2014 and in China on January 3 2014.    At the end of the run, the film has grossed United States dollar|$11.92 million in China.   

==Reception==
Film Business Asia gave the film a six out of ten rating, stating that the film "lacks human drama and real scope" and that "isnt especially bad as a genre movie, its also not especially good, and is certainly no threat to Johnnie Tos Lifeline (1997) as the premier Hong Kong firefighting film." 

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 
 

 