Super Capers
{{Infobox Film
| name           = Super Capers
| image          = Poster of the movie Super Capers.jpg
| image_size     = 
| caption        =  Ray Griggs
| producer       = Michael Kim Binder
| writer         = Ray Griggs Tommy "Tiny" Lister Michael Rooker Adam West Tom Sizemore
| music          = Nathan Lanier	
| cinematography = Martin Rosenberg
| editing        = Stacy Katzman
| distributor    = Roadside Attractions Lionsgate (DVD only)
| released       = March 20, 2009
| runtime        = 98 mins.
| country        = United States English
| imdb_title     = 1161064
| budget         = $2 million 
| gross          = $30,955 
}}
 action comedy Ray Griggs who also starred as one of the misfit superheros.

==Premise==
Would-be superhero, Ed Gruberman, who possesses no super powers must join a team of misfit heroes-in-training known as The Super Capers. Having only faith, Gruberman must travel through time to uncover an evil plot involving some gold bullion, a fiery femme fatale, and a criminal mastermind with a dark secret about Eds past.

==Story==
The story begins with a woman (Christine Lakin) in a red outfit being followed through a dark alleyway by a mysterious man who clearly has criminal intent. As she corners herself in a dead end, Ed Gruberman (Justin Whalin) arrives to save the day. The woman, calling herself simply "Red" reveals to both of the men that she has super powers, disabling the robber whom Ed strikes with a 2x4, sending him through the window of a law office. Red kisses Gruberman passionately before disappearing into the night, and he is left standing as the police come to arrest him.
 Tommy Lister) Ray Griggs) Terminator whose primary invention was an RV fashioned after the DeLorean DMC-12 time machine from the Back to the Future films, complete with a flux capacitor, which they cannot tell actually works because the RV cannot get up to 88 miles per hour.

The group is called out on a mission to stop Captain Sludge (Jon Polito) a diminutive super villain with powers of super speed and his minotaur minion, and in a show of panic, Gruberman proclaims he has the power of "Prayer" and supposedly forces a lamppost to fall onto the minotaur, allowing the heroes to be victorious and earning Felicias eye. However, Grubermans world begins to fall apart, when he and Brainard attempt to follow the suspicious judge incognito, and Ed is set up for stealing millions in Gold Bullion and sending them to a Swiss Bank account, which the number was found conveniently in his pocket. He is sent to visit the Judge, who reveals himself to both be Grubermans father AND the Dark Winged Vesper, and that the stunt that killed his parents was orchestrated by him, (but not intended to be fatal). Red, his accomplice makes an appearance and the pair attempt to lure Gruberman to the dark side (ala Return of the Jedi). Gruberman manages to escape, and in an ensuing chase, he drives the RV off the Mount Rushmore national monument, and when telling God that he will soon be with him, the vehicle reaches 88 miles per hour, sending him back to the date of his first mission 1 week prior (as Will Powers had put the date into the time circuits to mark the event). He travels to the bridge, and learns that should he encounter his past self (before that he saw the judge and imitates the voice of Vito Corleone and the catchphrase "it was not personal, it was only business"), one of them would spontaneously combust. He manages to meet the Super Capers, and in the process of revealing the Judges plans encounters his younger self. After giving away the events to follow in the week ahead, the Judge is arrested and Sludge and his minotaur surrender while Red is defeated and Freeze admits her attraction to Gruberman. The confusion of two Grubermans is resolved, however when the past version combusts, leaving only the one from the future and the day appears to be saved. The Judge, however, escapes in a post-credits scene.

==Cast==
* Justin Whalin as Ed Gruberman Samuel Lloyd as Herman Brainard
* Ryan McPartlin as Will Powers
* Danielle Harris as Felicia Freeze Ray Griggs as Puffer Boy Tommy "Tiny" Lister as Sarge Chris Owen as Igniter Boy
* Oliver Muirhead as Herbert Q
* Michael Rooker as Judge / Dark Winged Vesper
* Christine Lakin as Red
* Jon Polito as Captain Sludge
* Clint Howard as Mugger
* Tom Sizemore as Roger Cheatem
* Adam West as Manbat / Cab Driver Doug Jones as Special Agent Smith #1
* Isaac C. Singleton Jr. as Special Agent Smith #2
* Taylor Negron as Chauffeur
* June Lockhart as Mother
* Pat Crawford Brown as Gertrude
* Eva Marcille as News Reporter
* George Stults as Police Officer #2
* Steve Braun as Agent Guard
* Brian Cummings as Robot (voice)

==External links==
*  
*  
*  
*  

==References==
 

 
 
 
 
 
 
 
 
 
 