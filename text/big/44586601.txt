Dry Cleaning (film)
{{Infobox film
| name           = Dry Cleaning
| image          = 
| caption        =	
| director       = Anne Fontaine
| producer       = Philippe Carcassonne Alain Sarde
| writer         = Anne Fontaine Gilles Taurand 
| starring       = Miou-Miou Charles Berling Stanislas Merhar Mathilde Seigner
| music          = Edouard Dubois 
| cinematography = Caroline Champetier 			 
| editing        = Luc Barnier 
| studio         = Cinéa Les Films Alain Sarde Maestranza Films
| distributor    = AMLF 
| released       =  
| runtime        = 95 minutes
| country        = France
| language       = French
| budget         = €4.1 million 
| gross          = 
}}

Dry Cleaning ( ) is a 1997 French drama film directed by Anne Fontaine and written by Fontaine and Gilles Taurand. The film won the Golden Osella for Best Screenplay at the 54th Venice International Film Festival. 

== Cast ==
* Miou-Miou as Nicole Kunstler 
* Charles Berling as Jean-Marie Kunstler
* Stanislas Merhar as Loïc 
* Mathilde Seigner as Marylin  
* Nanou Meister as Yvette 
* Noé Pflieger as Pierre 
* Michel Bompoil as Robert 
* Christopher King as Steve 
* Gérard Blanc as Bertrand

==Accolades==
{| class="wikitable plainrowheaders sortable"
|- style="background:#ccc; text-align:center;" Award / Film Festival Category
! Recipients and nominees Result
|- 54th Venice International Film Festival Golden Lion  Anne Fontaine
|  
|- Golden Osella Anne Fontaine and Gilles Taurand 
|  
|- 23rd César Awards Best Actor Charles Berling 
|  
|- Best Actress
|Miou-Miou 
|  
|- Best Supporting Actress
| Mathilde Seigner 
|  
|- Best Screenplay
| Anne Fontaine and Gilles Taurand 
|  
|- Most Promising Actor
| Stanislas Merhar 
|  
|-
|Lumières Award Best Actress
|Miou-Miou 
|  
|-
|}

== References ==
 

== External links ==
*  

 

 
 
 
 
 
 
 
 
 
 
 

 