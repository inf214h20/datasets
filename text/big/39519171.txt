The Local (film)
  }}
{{Infobox film
| name           = The Local
| image          = 
| caption        = 
| director       = Dan Eberle
| producer       = 
| writer         = Dan Eberle
| starring       = Dan Eberle   Maya Ferrara   Karl Herlinger   Beau Allulli   David F. Nighbert   Paul James Vasquez   James Alba   Paul Bowen
| music          =
| cinematography = 
| editing        = 
| studio         = Insurgent Pictures   New Core Productions
| distributor    = Vanguard Cinema 
| released       =  
| runtime        = 90 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =
}}

The Local is a 2008 action film|action-drama film directed, written by and starring Dan Eberle. It also stars Maya Ferrara, Karl Herlinger, Beau Allulli, David F. Nighbert, Paul James Vasquez, James Alba, and Paul Bowen. The film follows Noname, a small-time drug trafficker hired by a wealthy out-of-towner to help free his drug-addicted daughter.

==Plot==

Noname (Dan Eberle) is a mysterious, lowly drug courier in Brooklyn, New York looking for escapism from his tortured and violent past. He rents out the basement of a married couple, struggling to stay clean from drugs and make ends meet with no job or money, which has him resort to working for psychotic drug dealers, particularly Sig (George Tchortov) and Big Black (Paul Bowen)

Under surveillance by wealthy out-of-towner, Frank (David F. Nighbert), and his associate, Joe (Paul James Vasquez), Noname confronts them in their car, where he is offered $5000 in return for his cooperation, later revealed they want him to emancipate Frank’s heroin-addicted, estranged daughter Claire (Maya Ferrara) from the drug house of his boss, Big Black.

During his errands, Noname meets drug dealer, Blueboy (Beau Allulli), and elderly leukemic woman, Anne Thomson (Janet Panetta), both who befriend him. However, some of his trafficking stops turn up botched, landing him in trouble with his dealers, as he is kidnapped and nearly killed by one of Sig’s enforcers.

Later, Blueboy is busted for drug possession and Anne dies from her illness. Barely scraping by, Noname robs money from a businessman. When his tenant Joe (Torben Brooks) discovers his life by finding a syringe in his basement, he is evicted from the premises and sleeps at the train station. Blueboy soon finds him at a restaurant, explaining he was bailed out of jail, but is being sent to rehab. Before they go their separate ways, Blueboy helps out Noname by giving him some cash.

Delaying the task long enough, Noname takes back up the $5000 offer by Frank and Joe. He kills Big Black after taking out one of his right-hand men Horse (James Alba). The other man Rottweiler (Karl Herlinger) escapes, while Noname removes a drugged Claire from the apartment. They board a train at the station that Rottweiler is a passenger on. Once Claire is taken into care by Joe during a train stop, Noname confronts Rottweiler, killing him in the process. Outside the station, Noname helps Frank reconcile with Claire and set her on a path to get clean. He tells Claire that Frank is family and will take care of her. Noname receives his $5000 owing and in the final shot, he watches on from the bridge as Frank, Joe, and Claire drive away.

==External links==
*  
*  

 
 
 
 
 
 
 
 