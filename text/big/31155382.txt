Bharthavu
{{Infobox film
| name = Bharthavu
| image =
| caption =
| director = M. Krishnan Nair (director)|M. Krishnan Nair
| producer = TE Vasudevan
| writer = TE Vasudevan Kanam EJ (dialogues)
| screenplay = Kanam EJ
| starring = Sheela Kaviyoor Ponnamma Ramesh T. S. Muthaiah
| music = V. Dakshinamoorthy MS Baburaj
| cinematography = NS Mani
| editing = TR Sreenivasalu
| studio = Jaya Maruthi
| distributor = Jaya Maruthi
| released =  
| country = India Malayalam
}}
 1964 Cinema Indian Malayalam Malayalam film, directed by M. Krishnan Nair (director)|M. Krishnan Nair and produced by TE Vasudevan. The film stars Sheela, Kaviyoor Ponnamma, Ramesh and T. S. Muthaiah in lead roles. The film had musical score by V. Dakshinamoorthy and MS Baburaj.   

==Cast==
  
*Sheela 
*Kaviyoor Ponnamma 
*Ramesh
*T. S. Muthaiah 
*Prathapachandran 
*Adoor Pankajam  Baby Vinodini 
*Bahadoor 
*Chandran
*KB Kurup
*PA Krishnan 
*Simhalan 
*T. K. Balachandran 
*Vijayakumar
*Alex 
 

==Soundtrack==
The music was composed by V. Dakshinamoorthy and MS Baburaj and lyrics was written by P. Bhaskaran. 
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF" align="center" 
| No. || Song || Singers ||Lyrics || Length (m:ss) 
|- 
| 1 || Bhaaram Vallaatha Bhaaram || K. J. Yesudas || P. Bhaskaran || 
|- 
| 2 || Kaakkakuyile Cholloo || K. J. Yesudas, LR Eeswari || P. Bhaskaran || 
|- 
| 3 || Kaneerozhukkuvaan Maathram || Gomathy || P. Bhaskaran || 
|- 
| 4 || Kollaam Kollaam || MS Baburaj, Uthaman || P. Bhaskaran || 
|- 
| 5 || Naagaswarathinte || LR Eeswari, Chorus || P. Bhaskaran || 
|- 
| 6 || Orikkaloru Poovalankili || P. Leela || P. Bhaskaran || 
|- 
| 7 || Swargathil Pokumbol || AP Komala, Uthaman || P. Bhaskaran || 
|}

==References==
 

==External links==
*  

 
 
 


 