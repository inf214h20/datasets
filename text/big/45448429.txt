27, Memory Lane (film)
{{Infobox film
| name           = 27, Memory Lane
| image          = 
| alt            =
| caption        = 
| director       = Luke Hupton
| produced by    = Luke Hupton
| screenplay by  = Luke Hupton
| starring       = James Clay Michael Maughan Dani Harrison Edwina Lea Hazel Mrozek Diona Doherty Mario Babic Jane Leadbetter Samantha Mesagno Lucas Smith
| music          = Mark Daniel Dunett
| cinematography = Ed Lambert
| casting by     = Felicity Jackson
| studio         = Lupton Films
| set decoration = Hughie Erskine Andrea Hupton Sarah Hupton
| costume design = Eleanor Kelsall
| sound          = Alex Dunn Kyle Martyn-Clarke 
| visual effects = Chris Cooke
| makeup department = Katie Eddison Rebecca Amor Michael Anthony Vera Fenlon Katie Longshaw
| production management = Chris Baker Viki James Rebecca Macdonald
| second unit director = Imogen Howkings 
| art department = Geraldine Sharrock
| camera         = Peter Nance George Short
| casting department = Beth Kelsall Liane Robertson
| costume department = Anna Dakin
| editorial department = Doug Garside
| music department = Shaun Chasin Nadine Sarhan Emma Zarobyan
| production assistand = Daisy Howkings
| production coordinator = Helena Rochester
| location scout = Sue West
| released       =  
| country        = United Kingdom
| language       = English
}}

27, Memory Lane is a British fantasy romance drama film, written and directed by Luke Hupton. 

==Plot==
 

==Cast==
* James Clay as August Pennyworth 
* Eileen Page as Elise Babineau
* Mario Babic as Dr. Djikstra 
* Kelly Marie Autumn as Nurse
* Diona Doherty as Niamh Healy - 17 
* Mark Tristan Eccles as Tom Shead
* Dani Harrison as Page Healy
* Samantha Mesagno as Keira Healy
* Hazel Mrozek as Evelyn Frost - 51
* Michael Maughan as August Pennyworth - 52
* Graham Cheadle as Frank Collingwood
* Lucas Smith as Henry Whistler
* Jason Redshaw as Burglar
* Graham Parrington as Herb Pennyworth
* Claire Louisa Cummings - Party Student
* Charles O`Neil as Carrie`s Solicitor
* Edwina Lea as Evelyn Frost - 26
* James Whitehurst as Mark
* Thomas Grande as Tom`s Best Man
* Alexander Wolfe as Tobias
* Helen Birdo as Party Student
* Jane Bancroft as Niam Healy
* Helena Rochester as Village Local
* Sarah Burill as Verity
* Sophie MacWhanell as Marion Pennyworth
* Laura-Marie Ring as Avril
* Caroline Vella as Carrie Winthrop-Pennyworth
* Laura Heathcote as Wedding Guest
* Patricia Drakley as Esther Collingwood
* Suzanne Duthie as Wedding Guest
* Dave Wake as Jason
* Louise Priding as Georgia Pennyworth
* Geraldine Sharrock as Paige`s Neighbour
* Philip Svejnoha as August`s Solicitor
* Francesca Totti as Wedding Guest
* Sarah Nelson as Wedding Guest
* Georgia Boe-Robertson as Make-Up Artist 
* Carole Bardsley as Tom`s Mother
* Rachelle Shaw as Wedding Guest
* Ravi Gopaul as Party Student
* Louis Macdonald as August Pennyworth - 10
* Lisa Blake as Wedding Guest
* Harold Shephard as Wedding Guest
* Julie Devigne as Wedding Guest
* Steven Kelly as Wedding Guest
* Charlotte Green as Paige`s Friend
* Daisy Howkings as Wedding Guest
* Pamela Taylor as Jude
* John Allwork as Wedding Guest
* Kate Cunliffe as Wedding Guest
* Sarah O`Rourke as Wedding Guest
* Alex Wareing as Wedding Guest
* Vanessa Sibanda as Wedding Guest
* David Hyndman as Wedding Guest
* Kelly Jane Hupton as Photographer
* Penelope Allwork as Wedding Guest
* Samantha Elwell as Party Student
* Danny Effy as Wedding Guest
* Michael Whewell as Bryan
* Jack McKiernan as Wedding Guest
* Sue Bardsley as Delivery Women
* John Rothwell as Party Student
* Mark Parr as Wedding Guest
* Brenda Catherall as Reverend
* John Chambers as Elderly Man in Cemetry

===Filming Locations===
Filming took place at Natura Skin Clinic in Cheshire, England, UK, Dam House in Astley, Lancashire, England, UK, Cheadle Hulme, Cheshire, England, UK, Salford Museum and Art Gallery, Salford, Greater Manchester, England, UK, Trafford General Hospital, Davyhulme, Greater Manchester, England, UK, Macdonald Kilhey Court Hotel, Standish, Wigan, Lancashire, England, UK, Atherton, Lancashire, England, UK, Millthrop, Cumbria, England, UK,  Forgotten Flowers Antique in Sedbergh, Cumbria, England, UK, Rivington Castle, Rivington, Lancashire, England, UK and at Chowbent Chapel in Atherton, Lancashire, England, UK. 

===Casting===
Casting took place at Surving Actors: Manchester 2013 featured casting. 

== Release ==
The film has been released on April 26, 2014. 

== References ==
 

== External links ==
*  
*  

 


 