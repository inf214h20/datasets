Göta kanal 2 – Kanalkampen
{{Infobox film
| name           = Göta kanal 2 – Kanalkampen
| image          =
| image_size     =
| caption        =
| director       = Pelle Seth
| producer       = Anders Birkeland Göran Lindström
| writer         = Bengt Palmers (writer)
| narrator       =
| starring       =
| music          =
| cinematography = Jens Fischer
| editing        = Darek Hodor Katarina Wiklund
| distributor    =
| released       =  
| runtime        = 94 minutes
| country        = Sweden
| language       = Swedish
| budget         =
| gross          =
}}

Göta kanal 2 &ndash; Kanalkampen is a 2006 Swedish film directed by Pelle Seth.

It is a sequel to  .

== Cast ==
*Morgan Alling as Röde Börje
*Pino Ammendola as Dario
*Kim Anderzon as Lena
*Sigge Avander as Vaitor / Man with hound in car
*Danilo Bejarano as Sergio
*Mats Bergman as Farmer
*Kjell Bergqvist as Sluiceguard Janne Loffe Carlsson as Janne Andersson
*Görel Crona as Sluiceguard-wife
*Daniella Dahmén as Female officer 1
*Henrik Dorsin as Host
*Julia Dufvenius as ICA-cashier
*Rafael Edholm as Benito
*Lena Endre as Vonna Jigert
*Ola Forssmed as Glider
*Svante Grundberg as Canoer
*Peter Haber as excavator-operator
*Magnus Härenstam as Peter Black
*Pia Johansson as Rita
*Nadine Kirschon as Fia
*Maria Langhammer as Female officer 2
*Peter Lorentzon as Patrik
*Regina Lund as Twin
*Henrik Lundström as Basse
*Claes Malmberg
*Claes Månsson as Sailor
*Johan Rabaeus as Isidor Hess
*Eva Röse as Petra Andersson
*Allan Svensson as Sluiceguard
*Linus Wahlgren as Filip
*Johan Wahlström as EU-bureaucrat
*Christopher Wollter as MC-officer

== External links ==
* 
* 

 

 
 
 
 
 
 
 


 
 