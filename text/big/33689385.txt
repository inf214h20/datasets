Challenge of a Lifetime
{{multiple issues|
 
 
 
}}

{{Infobox film
| name           = Challenge of a Lifetime
| image          = 
| image_size     =
| caption        =
| director       = Russ Mayberry
| producer       = Robert M. Sertner
| writer         =  Peachy Markowitz
| narrator       = 
| starring       = Penny Marshall Jonathan Silverman
| music          = Mark Snow
| cinematography = Héctor R. Figueroa 
| editing        = Robert Florio Michael J. Sheridan
| studio         = ABC
| released       = February 14, 1985
| runtime        = 93 minutes USA
| English
| budget         =
| gross          = 
| preceded_by    = 
| followed_by    = 
}} 1985 television film directed by Russ Mayberry. Starring Penny Marshall and Jonathan Silverman, the film focuses on a 35-year-old woman entering a Triathlon.

== Plot ==
Since her divorce from her husband, 35-year-old Nora Schoonover (Penny Marshall) has relocated from New York to Los Angeles and has fallen into a depression. She is working a low salary job and has gotten into great debts. Will Brodsky (Richard Gilliland) is assigned to help her work off her debts, and they quickly fall for each other. One day, her estranged 16-year-old son Steven (Jonathan Silverman), who has been living with his father since the divorce and is supposed to be on computer camp, shows up on her doorstep. A television interview with former class mate and recent Triathlon winner Mary Garritee (Mary Woronov), inspires Nora to enter and complete the Hawaiian Triathlon as a way of starting a new life. Steven, who has no interest in returning to his father in New York, promises to help and train her.

Norma quickly wants to give up, and claims to be too sick to enter the Triathlon, as well as taking care of Steven. Steven feels that she is making up excuses and reminds her of the fact that she never finishes a project. Inspired by her sons devotion of helping her, Norma continues her training. Simultaneously, she grows closer to Will, who has reported her car stolen, even though he is of knowledge that she sold it to tourists. Life gets better for both Nora and Steven, until John (Paul Gleason) shows up one day demanding for his son to return to New York. For one moment, Nora considers blowing it off again, lying to Will that she is seeing another man and advising Steven to go with his father. Steven does not take her advice, though, and accompanies her to Hawaii.

In Hawaii, Nora gets acquainted with her competition, including Mary and Mark Wilson (Brad Conner), while Steven attracts the attention from a young blonde. During practice of the Triathlon, Nora completes swimming, but falls off her bicycle and gets injured on her arm. She is determined to complete the Triathlon, though the accident causes her not qualify for the race. Her only hope now is a lottery, which gives some unqualified contestants a second chance. Nora loses fate in the competition, until she gets picked in the lottery, with Will showing up to support her.

During the actual Triathlon, Nora finishes 58th in the swimming category, which is led by Wilson. This is followed by a 5 hour bike ride, in high temperatures and with a backfiring wind. Nora struggles with the heat, though stays focused and finished 31st overall. Steven, extremely proud of his mother, expresses his interest in moving in with her, before she starts running in the final category. By the time Mark has already finished, Nora is still in the race, but starts to lose ground and threatens to reach her physical limits. With the final mile in sight, Nora is pushed to the finish by contestant Donald Turnquist (James Bergeson), who suffers from a lung disease. Nora collapses right before the finish, but stands up and completes the race in 13 hours and 32 minutes on the 217th place, winning her sons respect.

==Cast==
*Penny Marshall as Nora Hoffer-Schoonover
*Jonathan Silverman as Steven Schoonover
*Richard Gilliland as Will Brodsky
*Mary Woronov as Mary Garittee
*Paul Gleason as John Schoonover
*James Bergeson as Donald Turnquist
*Bart Conner as Mark Wilson
*Cathy Rigby as Virginia
*Mark Spitz as Brad Harris
*Jim Lampley as Announcer

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 