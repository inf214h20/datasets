The Navy
 
{{Infobox Hollywood cartoon|
| cartoon_name = The Navy
| series = Oswald the Lucky Rabbit
| image = Oswald Navy Scene.jpg
| caption = Oswald says to the admiral, "Well, Ill be seeing ya." Bill Nolan Bill Nolan
| animator = Clyde Geronimi Manuel Moreno Ray Abrams Fred Avery Lester Kline Pinto Colvig
| voice_actor = Pinto Colvig as Oswald 
| musician = James Dietrich
| producer = Walter Lantz
| distributor = Universal Pictures
| release_date = November 3, 1930
| color_process = Black and white
| runtime = 7 min. English
| preceded_by = The Fowl Bowl
| followed_by = Mexico (cartoon)|Mexico
}}

The Navy is a short animated film produced by Walter Lantz and as part of the Oswald the Lucky Rabbit cartoons.

==Plot summary==
Oswald is a sailor who works on the ship of the admiral, and is wearing shoes for the first time. {{cite web
|url=http://lantz.goldenagecartoons.com/1930.html
|title=The Walter Lantz Cartune Encyclopedia: 1930
|accessdate=2011-04-24
|publisher=The Walter Lantz Cartune Encyclopedia
}}  One day when work is done for the day, he looks forward to find a date at a lighthouse.

Minutes afterward, Oswald arrives at the lighthouse. To get the attention of his special someone, Oswald sings while a couple of sparrows play an accordion. Looking out the window of the lighthouse is a girl cat who is entertained by Oswalds performance. To get inside, Oswald springs himself upward using the accordion. Oswald then notices that the girl cat is having an affair with the admiral who orders him to return to the ship.
 tabbies and at the same time scrub the floor with brushes attached to his feet as he gets pulled. This strategy turned out to be very difficult to control as it results in Oswald accidentally stripping paint off some life boats as well as knocking down the admiral.

The admiral is most dissatisfied and literally kicks Oswald out of the ship. The kick is so powerful that Oswald is sent airborne for a few miles. The rabbit ends up landing in knickers that are hanged on a clothesline. At the end of the clothesline lies the window of the lighthouse which is right next to Oswald. Once more the girl cat shows up and is happy to see Oswald again. The two trade kisses.

==See also==
*Oswald the Lucky Rabbit filmography

==References==
 

==External links==
*  at the Big Cartoon Database

 

 
 
 
 
 
 
 
 
 


 