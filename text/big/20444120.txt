The Irrefutable Truth about Demons
 
 
{{Infobox film
| name = The Irrefutable Truth About Demons
| image = The Irrefutable Truth About Demons DVD cover.jpg
| alt =  
| caption = DVD cover
| director = Glenn Standring
| producer = {{Plainlist|
*Dave Gibson
*Chris Tyson}}
| writer = Glenn Standring
| starring = {{Plainlist|
*Karl Urban
*Katie Wolfe
*Jonathon Hendry
*Sally Stockwell}}
| music = {{Plainlist|
*Victoria Kelly
*Joost Langeveld}}
| cinematography = Simon Baumfield
| editing = Paul Sutorius
| studio =
| distributor =
| released =   
| runtime = 90 minutes
| country = New Zealand
| language = English
| budget =
| gross =
}}
The Irrefutable Truth about Demons is a New Zealand horror film released in 2000. It was directed by Glenn Standring and stars Karl Urban, Katie Wolfe, and Jonathon Hendry.

The films UK DVD title is The Truth About Demons.

==Plot==
Haughty anthropology professor Harry Ballard (Karl Urban) receives a sinister videotape showing a cult called the Black Lodge ranting about a demonic plot. As it turns out, Harrys brother, Richard, killed himself a few months earlier under mysterious circumstances, possibly related to this cult; in any event, the loss has been preying on Harrys mind, sending his relationship with his girlfriend (Sally Stockwell) into a tailspin. Meanwhile, a seemingly schizophrenic young woman named Benny (Katie Wolfe), who has a penchant for lighting sparklers in alleyways for no good reason, follows Harry around and snatches him from the jaws of doom after he falls into the cults hands. The devilish leader, Le Valliant (Jonathan Hendry), apparently has big plans in store for Harry, and soon the protagonists grip on reality slips as the cult targets him for an upcoming ritual.

== Critical reception == AllMovie gave the film a positive review, calling it "a clever, gleefully ludicrous flick". 

== References ==
 

==External links==
 
* 
* 
* 

 
 
 
 
 


 
 