Dreamland (2009 film)
{{Infobox film name           = Dreamland image          = Draumalandid.png caption        = director       = Þorfinnur Guðnason Andri Snær Magnason producer       = Sigurður Gísli Pálmason writer         =  cinematography = Þorfinnur Guðnason Guðmundur Bjartmarsson Hjalti Stefánsson Bergsteinn Björgúlfsson editing        = Eva Lind Höskuldsdóttir distributor    =  released       =   runtime        = 90 minutes country        = Iceland language       = Icelandic budget         =  gross          = 
}}
Dreamland ( ) is a 2009   by Andri Snær Magnason. The films soundtrack is composed by Valgeir Sigurðsson.

== Content ==
The documentary Dreamland is about the question, in how far the unspoiled, unique nature of Iceland should be preserved or if it is more important to build up enormous dams to produce hydro-electronic. It shows, how the aspiration after "green energy" more and more threatens the natural wonders of Iceland just to provide aluminum industries with cheap energy. 
Through interviews with economists, psychologists, historians, poets, editors, managers and so forth, the audience gets insight of different point of views. 
Further it deals with the statement, that fear is a powerful emotion and a way of controlling. For example the fear of unemployment often leads to a limited view of other possible alternatives. To develop the countryside of Iceland and to preserve it from unemployment, the Icelandic government decided to make Iceland the biggest aluminum manufacture. It seemed to be the simplest way. 

The second theme the documentary deals with is the fact, how the world makes business with war. The military base in Keflavík increased the economy of this region, but when it got more and more irrelevant for the American government, the people feared about their jobs and the government tried desperately to convince the American military to stay. 

During the whole documentary, pictures of a beautiful, untouched nature are shown, followed by gigantic hydro towers and dams which destroy the picture of the wideness and beauty of a landscape, which units volcanoes, glaciers, mountains, waterfalls and so much more. 

==See also==
*Kárahnjúkar Hydropower Plant
* 

==External links==
* 
* 
*  
 
 
 
 
 
 
 
 

 
 