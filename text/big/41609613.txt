Si Gomar
{{Infobox film
| name           = Si Gomar
| image          = Poster si gomar.jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = Advertisement
| director       = Tan Tjoei Hock
| producer       = The Teng Chun
| screenplay     =
| narrator       = 
| starring       ={{plain list|
*Hadidjah
*Tan Tjeng Bok
*Mohamad Mochtar
}}
| music          = Mas Sardi
| cinematography = 
| editing        = 
| studio         = Java Industrial Film
| distributor    = 
| released       =  
| runtime        = 
| country        = Dutch East Indies
| language       = Indonesian
| budget         = 
| gross          = 
}}
Si Gomar is a 1941 film from the Dutch East Indies which was written and directed by Tan Tjoei Hock and produced by The Teng Chun. Starring Hadidjah, Mohamad Mochtar, and Tan Tjeng Bok, the movie follows a brother and sister who are separated by robbers and almost marry before their cousin recognises them.

==Plot==
After a run-in with robbers, Badjoeri and his son Soebardja are set adrift on a river. Badjoeris wife and daughter, Ramina and Mariani, are captured by the bandits. Though they escape with the help of Wirama, Ramina dies soon afterwards. Badjoeri also dies, soon after leaving Soebardja with Mansur.

Years pass, and Soebardja and Mariani are set to marry. As they have been raised separately, by different people, they do not realize that they are brother and sister. The marriage is only called off after their cousin Ismail realizes the true relationship of the would-be bride and groom. 

==Production==
Si Gomar was written and directed by Tan Tjoei Hock for Action Film, a subsidiary of Java Industrial Film (JIF). The film was produced by The Teng Chun, owner of the company,  who had signed Tan in 1940 after seeing him at Prinsen Park (now Lokasari).  Artistic arrangement was handled by Hajopan Bajo Angin. Music was provided by Mas Sardi, whereas sound was handled by The Teng Chuns brother TS The. 

Hadidjah, Mohamad Mochtar, and Tan Tjeng Bok starred in Si Gomar; the film also featured Bissu, Aysah, M. Sani, and Said Thalib. Hadidjah and Tan Tjeng Bok both took a dual role; Hadidjah played Ramina and Mariani, whereas Tan Tjeng Bok played Badjoeri and Soebardja.  Filming of this black-and-white production had been completed by August 1941. 

==Release and reception==
Si Gomar was released by September 1941. An anonymous review for Pertjatoeran Doenia dan Film praised the movie, particularly Tan Tjeng Boks acting in his double role and the visual effects&nbsp;– such as those showing an erupting volcano and a forest fire. 
 Japanese occupation began in March 1942, causing all but one film studio to be shut down. 

Si Gomar was shown as late as December 1943.  It is now likely lost film|lost, as are all Indonesian films from before 1950 according to American visual anthropologist Karl G. Heider.  Movies produced in the Indies were shot on highly flammable nitrate film, and after a fire destroyed much of Produksi Film Negaras warehouse in 1952, old films shot on nitrate were deliberately destroyed.  However, Kristanto records several as having survived at Sinematek Indonesias archives, and film historian Misbach Yusa Biran writes that several Japanese propaganda films have survived at the Netherlands Government Information Service. 

==References==
 

==Works cited==
 
* {{cite journal
  | title = Action Production: Si Gomar
  | language = Indonesian
  | work = Pertjatoeran Doenia dan Film
  | location = Batavia
  |date=September 1941
 |page=47
  | ref =  
  }}
*{{cite news
 |title=Berita Djawa no. 19 sebagai Pendahoeloean Film Si Gomar di Kranggan
 |trans_title=Berita Djawa No. 19 as a Prelude to Si Gomar at Kranggan
 |language=Indonesian
 |work=Soeara Asia
 |location=Yogyakarta
 |page=2
 |date=27 December 1943
 |ref= 
 |url=http://niod.x-cago.com/maleise_kranten/article.do?code=Niod084&date=19431227&id=084-19431227-002012&words=Si%20Gomar%20si%20gomar
}}
*{{cite book
 |title= 
 |trans_title=History of Film 1900–1950: Making Films in Java
 |language=Indonesian
 |last=Biran
 |first=Misbach Yusa
 |author-link=Misbach Yusa Biran
 |publisher=Komunitas Bamboo working with the Jakarta Art Council
 |year=2009
 |isbn=978-979-3731-58-2
 |ref=harv
}}
*{{cite book
 |title=Indonesia dalam Arus Sejarah: Masa Pergerakan Kebangsaan
 |trans_title=Indonesia in the Flow of Time: The Nationalist Movement
 |language=Indonesian
 |last=Biran
 |first=Misbach Yusa
 |chapter=Film di Masa Kolonial
 |trans_chapter=Film in the Colonial Period
 |author-link=Misbach Yusa Biran
 |publisher=Ministry of Education and Culture
 |year=2012
 |volume=V
 |pages=268–93
 |isbn=978-979-9226-97-6
 |ref=harv
}}
*{{cite book
 |url=http://books.google.ca/books?id=m4DVrBo91lEC
 |title=Indonesian Cinema: National Culture on Screen
 |isbn=978-0-8248-1367-3
 |author1=Heider
 |first1=Karl G.
 |year=1991
 |publisher=University of Hawaii Press
 |location=Honolulu
 |ref=harv
}}
* {{cite web
  | title = Kredit Si Gomar
  |trans_title=Credits for Si Gomar
  | language = Indonesian
  | url = http://filmindonesia.or.id/movie/title/lf-s007-41-611559_si-gomar/credit
  | work = filmindonesia.or.id
  | publisher = Konfiden Foundation
  | location = Jakarta
  | accessdate = 11 January 2014
  | archiveurl = http://www.webcitation.org/6MXVuy0yp
  | archivedate = 11 January 2014
  | ref =  
  }}
* {{cite web
  | title = Si Gomar
  | language = Indonesian
  | url = http://filmindonesia.or.id/movie/title/lf-s007-41-611559_si-gomar
  | work = filmindonesia.or.id
  | publisher = Konfiden Foundation
  | location = Jakarta
  | accessdate = 27 July 2012
  | archiveurl = http://www.webcitation.org/69SSYDzAC
  | archivedate = 27 July 2012
  | ref =  
  }} (also available in  )
* {{cite web
  | title = Singa Laoet
  | language = Indonesian
  | url = http://filmindonesia.or.id/movie/title/lf-s010-41-227946_singa-laoet
  | work = filmindonesia.or.id
  | publisher = Konfiden Foundation
  | location = Jakarta
  | accessdate = 27 July 2012
  | archiveurl = http://www.webcitation.org/69SOYQuIA
  | archivedate = 27 July 2012
  | ref =  
  }}
* {{cite web
  | title = Tan Tjoei Hock
  | language = Indonesian
  | url = http://www.jakarta.go.id/web/encyclopedia/detail/3221/Tan-Tjoei-Hock
  | work = Encyclopedia of Jakarta
  | publisher = Jakarta City Government
  | location = Jakarta
  | accessdate = 26 September 2012
  | archiveurl =http://www.webcitation.org/6AxmFRrkO
  | archivedate = 26 September 2012
  | ref =  
  }}
* {{cite journal
  | title = Warta Studio
 |trans_title=News from the Studios
  | language = Indonesian
  | work = Pertjatoeran Doenia dan Film
  | location = Batavia
  |date=August 1941
 |page=28
  | ref =  
  }}
 
 

 
 