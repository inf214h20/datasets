Warchild (film)
Warchild German and Serbo-Croatian Cyrillic|Serbo-Croat.  It won two awards and was nominated for a further two.

==Plot==
Labina Mitevska stars as Senada, a young mother whose only daughter Aida was removed from Bosnia-Herzegovina during the worst years of the war and presumably adopted into a Western European family. Searching for her after the war, Senada enters illegally into Germany, where she discovers through a social worker the harsh truth of postwar adoption: Aida is alive and well and living happily with a German family. Dark secrets emerge, leaving no one unscathed in this expertly crafted, superbly performed drama.

==The theme of war==
Warchild is set against the background of the Bosnian War. However, Wagner (Wallers last Trip/Transatlantis/Ghetto Kids) wanted to tell a universally recognisable story that would bring home to viewers the impact of war on ordinary people’s lives. It is a story about a mother’s search for her child after the war but it is also a story about dislocation, as adults and children everywhere face the consequences of separation, relocation and Human migration|migration.

==Balkan Blues Trilogy==
Warchild belongs to German filmmaker Christian Wagner’s Balkan Blues Trilogy, which begins with a short film, Zita, and continues with Alcatrash, currently in production. The three films are connected to the Balkans and reflect Wagner’s interest in the movement of peoples throughout Europe.

==Awards and Screenings== Before the Rain) and Katrin Sass (Good Bye, Lenin!). It had its international premiere at the 2006 Montreal World Film Festival. It won the award for best screenplay.

Warchild has won a number of other awards, including the Bavarian Film Awards (Special Jury Prize), Best Screenplay Montreal 2006, Audience Award Portoroz, and the Golden Olive Tree & Special Audience Award 2007 at the European Filmfestival/Lecce.

Shown at about 50 international film festivals, the film is a German and Slovenian co-production by Wagnerfilm, Munich and Studio Maj, Ljubljana. It was funded by MFG/FilmFernsehFonds Bayern/BKM/Filski Sklad Slovenia/EURIMAGES.

==Notes==
 

==External links==
*  
*  
*  , accessed 2008-08-09.

 
 
 
 


 