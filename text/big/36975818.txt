Puff the Magic Dragon in the Land of the Living Lies
{{Infobox television film name     = Puff the Magic Dragon genre  Family Short Short
|director Fred Wolf producer = Charles Swenson Peter Yarrow Romeo Muller Kevin Hunter (executive) Robert L. Rosen (executive) writer   = Romeo Muller starring = Burgess Meredith Peter Yarrow Robert Ridgely Joan Gerber Ike Eisenmann music    = Peter Yarrow David Richard Cambell editing  = Rich Harrison studio   = Murakami Wolf Swenson Yarrow/Muller-My Company network  ABC
|released =   runtime  = 30 minutes country  =   language = English
}}
 animated television Puff the Magic Dragon. The title character is voiced by Burgess Meredith.

==Plot==
The film begins with Puff acting out the lies of a girl named Sandy, who has developed a persistent habit of making up absurd lies on most occasions, and shows how this has alienated most of her friends and leaving only her dog as a companion. Puff moves to intervene when she causes a household accident and falsely blames her innocent dog who is sent away as punishment.

The dragon meets Sandy and forcibly takes her to the Land of Living Lies, leaving her claiming that she preferred to be there anyway because her house is "broken." Once there, she encounter famous liars like The Boy who Cried Wolf, and Baron Munchausen and various representations of metaphors of imagination. Along the way, Puff explains to Sandy the difference between purposefully deceptive lies and the harmless description of honest figments of imagination.

Unfortunately, the impossibility of living in a such a world where nothing expressed can be believed is reinforced when Sandy is presented with strange laws about not taking any of the non-existent flowers and apples. She is then maneuvered by a talking rock that claims its a flower into picking him up, only to accuse her of stealing him, leads to her being arrested by Pinocchio.  She is then subject to a bizarre trial in the Caverns in the Living Lies by the denizens of the Land, where she panics and falsely implicates Puff for her crime.

Afterward, she visits him as he sits chained, pleading for him to use his magic to send them back to the real world. Puff tells her that would be impossible unless she tells the truth, which causes the natives to growl in pain. Regardless of the situation, Sandy confesses that she cant truly go home, but cant bring herself to explain how her home is truly "broken." At Puffs urging, she confesses that she makes up her lies because she doesnt want to live with her perceived truth that she considers herself responsible for her parents divorce. At that statement, the natives chortle with delight at what Puff explains is a self-deceptive lie and makes her realize that she had nothing to do with her parents break-up and they still love her.

At this liberating truth, Puff and Sandy are freed, bring the walls of the cavern down with their celebratory song and return to the real world. At that, Puff prepares to leave, but not before suggesting Sandy she can see him again in spirit by using her vibrant imagination in more productive ways like writing fiction. Sandy immediately takes that advice after reconciling with her parents and begins her first story, which Puff confidently notes is sure to be a classic.

==Voice cast==
*Burgess Meredith as Puff
*Peter Yarrow as Father
*Robert Ridgely as Baron Munchausen/Snake/Cowboy/Attorney
*Joan Gerber as Pinocchio/Rock #2 /Little Girl/Mother
*Ike Eisenmann as The Boy Who Cried Wolf/Umpire
*Gene Moss as Old Fisherman /Judge/Bailiff
*Mischa Bond as Sandy
*Alan Barzman as Rock #1

==Sequel==
The film was followed by the made-for-TV sequel Puff and the Incredible Mr. Nobody (1982).

==External links==
* 

 
 
 
 
 
 
 