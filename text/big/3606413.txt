Impostor (film)
 
{{Infobox film
| name           = Impostor
| image          = Impostor.jpg
| image_size     =
| caption        = Theatrical poster
| alt            =
| director       = Gary Fleder
| producer       = Gary Fleder Marty Katz Daniel Lupi Gary Sinise
| based on       = "Impostor (short story)|Impostor" by Philip K. Dick
| story          = Scott Rosenberg  (adaptation) 
| screenplay     = Caroline Case Ehren Kruger David Twohy
| starring       = Gary Sinise Madeleine Stowe Vincent DOnofrio Mekhi Phifer
| music          = Mark Isham
| editing        = Armen Minasian Bob Ducsay
| cinematography = Robert Elswit
| distributor    = Dimension Films
| released       =  
| runtime        = 102 minutes
| country        = United States
| language       = English
| budget         = $40 million http://www.the-numbers.com/movies/2002/IMPOS.php  
| gross          = $8,145,549 
}}
 short story of the same name by Philip K. Dick. The film starred Gary Sinise, Madeleine Stowe, Vincent DOnofrio and Mekhi Phifer.

==Plot== military government is established to effect the war and the survival of humans.
 replicant created by the aliens. The ESA intercepted an alien transmission which cryptanalysts decoded as programming Olhams target to be the Chancellor, whom he was scheduled to meet. Such replicants are perfect biological copies of existing humans, complete with transplanted memories, and do not know they are replicants. Each has a powerful "u-bomb" in their chest, which can only be detected by dissection or a high-tech medical scan, since it only assembles itself when it gets in proximity to its target. Detection via the special scan works by comparing against a previous scan, if there was one.

Major Hathaway begins interrogating Olham. As hes about to drill out his chest to find the bomb, Olham breaks loose and escapes, accidentally killing his friend Nelson in the process. With the help of underground stalker Cale, Olham avoids capture and sneaks into the hospital where his wife Maya is an administrator to get the high-tech scan redone and prove hes not a replicant. But the scan is interrupted by security forces before it can deliver the answer.

Olham and his wife are eventually captured by Hathaways troops in a forest near an alien crash site near the spot where they spent a weekend. Inside the ship they discover the corpses of the real Maya and Spencer, who were indeed killed on their weekend picnic. At that moment Olham realizes aloud that he really is a replicant and the secondary trigger (his awareness of who he was) detonates his bomb, destroying himself, Maya, Hathaway, and everything in a wide area. In the final scene, the news announces that Hathaway and the Olhams were killed in an enemy attack, as if the government was covering up the true resolution or didnt know it. Cale wonders if he really knew Olhams true identity.

==Cast==
* Gary Sinise  as Spencer Olham
* Madeleine Stowe  as Maya Olham
* Vincent DOnofrio  as Hathaway
* Mekhi Phifer as Cale
* Tony Shalhoub as Nelson Gittes
* Tim Guinee as Dr. Carone
* Gary Dourdan as Captain Burke
* Lindsay Crouse as Chancellor
* Clarence Williams III as Secretary of Defense (uncredited) Elizabeth Pena  as Midwife
* Shane Brolly  as Lt. Burrows
* Golden Brooks  as Cales Sister Ted King  as RMR Operator
* Rachel Luttrell  as Scan Room Nurse

==Production== Isaac Asimovs Donald A. Matthew Robbins. film of the same name, but with a different script.

The short was originally written by Scott Rosenberg, with revisions by Mark Protosevich and Caroline Case. When it was decided to expand the short into a feature-length film, additional scenes were written by Richard Jeffries, Ehren Kruger, and David Twohy.

Burn areas in  ) 

The movie was made on an estimated $40 million budget. 

==Reception==
=== Critical response ===
 
Impostor received negative reviews from critics. Rotten Tomatoes gives the film a score of 22% based on 91 reviews. {{cite web
| title = Imposter (2002)
| url = http://www.rottentomatoes.com/m/impostor/
| work = Rotten Tomatoes
| publisher = Flixster
}}
 
Metacritic gives the film a score of 33% based on 26 reviews. 
http://www.metacritic.com/movie/impostor
Metacritic
CBS
 

James Berardinelli of ReelViews gave the film two and a half stars (out of four), saying "there are a few moderately diverting subplots and the storyline eventually gets somewhere," but added that "Impostor wears out its welcome by the half-hour mark, and doesnt do anything to stir things up until the climax. You could spend the entire midsection of this movie in the bathroom and not miss much." 
William Arnold of the Seattle Post-Intelligencer gave the film a mildly positive review, praising lead actor Gary Sinises ability to "hold the film together and provide a strong, sympathetic human focus. The movies atmosphere has a very definite Blade Runner feel." 
Maitland McDonagh of TV Guide gave the film three stars out of four, saying it packed "a real emotional wallop," but suggested that it would have worked better as the 40-minute short film it was originally intended to be. 

Keith Phipps of The Onions The A.V. Club|A.V. Club gave the film a negative review, saying that "it essentially uses the setup of   as a bookend to one long, dull chase scene." {{cite web
| date = March 29, 2002
| author = Keith Phipps
| title = Imposter
| url = http://www.avclub.com/articles/impostor,20556/
| work = The A.V. Club
| publisher = The Onion
}}
 
Robert Koehler of Variety (magazine)|Variety also criticized the film, calling it "a stubbornly unexciting ride into the near future." {{cite web
| date = January 2, 2002
| author = Robert Koehler
| title = Also Playing: Impostor
| url = http://www.variety.com/review/VE1117916683.html
| work = Variety (magazine)|Variety
}}
 

A. O. Scott of The New York Times offered a sardonic view of the movies "dark view of the future" ("a badly lighted one, that is"), of the editing ("pointlessly hyperkinetic"), and of the "twist" ending ("meant to be clouded with ambiguity, but really it is unequivocally happy because it means the movie is over"). 

=== Box office ===
The film earned a little over $6 million at the box office in the United States and Canada, with the estimated worldwide of over $8 million, thus making it a box office failure. {{cite web
| title = Imposter (2002)
| url = http://www.boxofficemojo.com/movies/?id=impostor.htm
| work = Box Office Mojo
| publisher = Amazon.com
}}
 

==References==
 

==External links==
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 