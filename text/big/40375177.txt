Breathe In (film)
 
{{Infobox film
| name           = Breathe In
| image          = Breathe In 2013 Poster.jpg
| image_size     = 220px
| border         = yes
| alt            = 
| caption        = Theatrical release poster
| director       = Drake Doremus
| producer       = Steven M. Rales Mark Roybal Jonathan Schwartz Andrea Sperling
| writer         = Drake Doremus Ben York Jones
| starring       = Guy Pearce Felicity Jones Amy Ryan
| music          = Dustin OHalloran
| cinematography = John Guleserian
| editing        = Jonathan Alberts Indian Paintbrush QED International Super Crispy Entertainment
| distributor    = Cohen Media Group
| released       =  
| runtime        = 97 minutes  
| country        = United States
| language       = English
| budget         = 
| gross          = $298,445 
}}
Breathe In is a 2013 American romantic drama film co-written (with Ben York Jones) and directed by Drake Doremus and starring Guy Pearce, Felicity Jones, and Amy Ryan. The film premiered at the Sundance Film Festival on January 19, 2013—the directors third film to play at the festival.   

== Plot ==
The film is about a high school music teacher who has an affair with a foreign exchange student from England who is his daughters age, due to his disillusion with his life and their shared love of music. 

Keith Reynolds is a high school music teacher married to Megan and living in a small town an hour and half from New York City. He has given up a career as a musician following the arrival of their daughter. Lauren is very nearly 18 and is a champion swimmer. In his spare time, he substitutes for a cello player in an orchestra and is applying for a permanent role. Keith finds his life frustrating, resents his job as a teacher, and his wifes dismissal of his music playing. He wants to move back to the city and work as a musician but his wife Megan refuses to consider this as she disliked the uncertainty of the income and the lifestyle.   

The family is to host a foreign exchange student, Sophie, for one semester. When Sophie arrives she is disappointed by the distance from Manhattan which she wanted to visit. Sophies mother died when she was very young and she was raised by her Uncle and Aunt. Her Uncle introduced her to the piano but has recently died, affecting Sophie badly. Sophie tells her aunt that she is not practising her music saying that she doesnt know who she is playing for any more.

Sophie reveals to Keith that she is an accomplished musician and they bond over their mutual interest. While they are alone at the house, she tells Keith about the death of her Uncle and that she wants to choose to play music. Sophie is unsure and has lost her purpose following her Uncles death so doesnt play while Keith has chosen not to play for the sake of his family.

Keith and Sophie embark on a love affair, spending time together talking about running away together. Lauren discovers this and reveals her knowledge to Sophie who panics and tells Keith she must leave. Keith tells her he will run away with her. They plan to meet in New York City after a performance, packing their things.

Lauren, angry at her Father and having been spurned by Aaron, gets drunk and crashes her car. At the same time, Megan discovers Keith and Sophies affair and their plan to run away. While she is smashing things around the house in anger, she gets a call telling her Lauren is hurt and in hospital.

Following his performance, Keith meets up with Sophie. They load their things in the car and are smiling at each other in happiness. Suddenly Keith receives a text message from Megan telling him that something has happened to Lauren. He appears at the hospital where Megan tells him she doesnt know how Lauren is.

The film closes with the family having a photo shoot. Lauren is fine but has a scar near her eye. Keith has decided to return to his wife and Sophie has gone.

==Cast==
* Guy Pearce as Keith Reynolds
* Felicity Jones as Sophie
* Amy Ryan as Megan Reynolds
* Mackenzie Davis as Lauren Reynolds 
* Ben Shenkman as Sheldon
* Alexandra Wentworth as Wendy Sebeck Hugo Becker as Clément
* Brendan Dooling as Ryan
* Kyle MacLachlan as Peter Sebeck
* Lucy Davenport as Sophies mom
* Jenny Anne Hochberg as Swim Team Member
* Elise Eberle as Angela
* Nicole Patrick as Theresa
* Brock Harris as Paul
* Stephen Sapienza as Swim Team Member

==Production==
===Casting===
Breathe In is the third film from director Drake Doremus to play at the Sundance Film Festival. Doremus won the festivals dramatic grand jury prize in 2011 with Like Crazy, his first film with leading lady Felicity Jones, who took the special jury prize for acting that year.  
Discussing his casting of Jones in his early film Like Crazy, Doremus remembers, "She sent me a tape she made in her flat. She did the ending of the movie, actually in her shower. And it was like, OK, wow. I cast her without even meeting her." After they completed Like Crazy, they began talking about working together again. "I felt like the journey wasnt complete yet, and that we had some more exploring to do." 

===Filming===
Using the same technique as on Like Crazy, Doremus and co-screenwriter Ben York Jones prepared a detailed outline for each scene without dialogue, and then rehearsed with the cast for several weeks while they improvised the words.  Doremus explains:
 

Doremus films are known for their "intimate" style.  His use of hand-held cameras, often right in the actors faces, and his ability to draw out performances from his actors that are  natural and unaffected, creates the illusion that were "invading private moments" in the lives of the characters. 

Breathe In was filmed on location in upstate New York and Terminal 4 in JFK International Airport in Queens, New York. 

==Reception==
===Box Office===
Breathe in" received a limited release in The United States opening in 18 theatres and grossing $15,324 with an average of $851 per theatre. The films widest release was 41 theatres and it ended up making $89,661 domestically and $208,784 elsewhere for a total of $298,445. 
 

===Critical response===
The film scored generally mixed to positive reviews. On Metacritic, which assigns a rating out of 100 based on individual reviews, the film received an average score of 60 ("generally favorable") based on 21 reviews.  

As of September 2014, it holds an overall approval rating of 55% (out of 73 reviews) on the film review aggregator Rotten Tomatoes; the consensus states: "Breathe In s plot never quite sparks the way it should, but it remains thoroughly watchable thanks to strong performances from Felicity Jones and Guy Pearce." 

In his review for Collider.com, Adam Chitwood gave the film an A– rating, noting that it is "rare to find a film of this kind that is genuinely moving without feeling overly manipulative or sappy".    Chitwood continued:

 
Chitwood thought the entire cast was "fantastic", singling out Pearce who "turns in one of the best performances of his career".  Chitwood concludes, "With impeccable performances, inspired direction, beautiful cinematography, and a devastating story, Breathe In marks one of the best family dramas in recent years and a promisingly mature leap forward for director Drake Doremus." 

In his review for CinemaBeach, Bryan Thompson called the film "an adult masterpiece that carries Doremus’s thoughtful and methodic style into something both beautiful and tragic".    Thompson also praised the "subtle but beautiful performances by the films cast", singling out Guy Pearce who is "particularly stellar" at creating the "complex and sympathetic" character of Keith.
 

While pointing out that the film is as "predictable as melodrama comes", Thompson also believes the film "shines ... in the smaller moments". Thompson notes, "As Keith and Sophie explore their taboo relationship, the film can easily fall into the trap of cliché, but it skirts the edges thanks to a realism and honesty framed through fully formed characters and sequences that are quiet but speak volumes, building on relationships and emotion rather than story."  Thompson concludes that despite the films weaknesses—particularly the last act crisis that "feels forced"—the finer moments of Breathe In make up for the flaws, and that Doremus and company are "fast becoming filmmaking virtuosos". 

In his first look review for The Guardian, Jeremy Kay gave the film four out of five stars, calling it a "finely calibrated piece of work from one of the more talented US film-makers to emerge in recent years".    Kay notes that while the story is predictable and nothing new, Doremus "makes it all utterly captivating" and "mines just the right amount of drama and spontaneous comedy from each moment and the foreshadowing is perfectly weighted".  Kay also praises the acting performances: 
 

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 