Forever (1921 film)
for the 1935 film of this story starring Gary Cooper see Peter Ibbetson
{{Infobox film
| name           = Forever
| image          = Forever-1921.jpg
| image size     =
| caption        = Thearical poster
| director       = George Fitzmaurice
| producer       = Famous Players-Lasky
| writer         = Ouida Bergere (scenario)
| based on       =  
| starring       = Elsie Ferguson Wallace Reid Montague Love George Fawcett Elliott Dexter
| cinematography = Arthur C. Miller
| art direction  = Robert M. Haas
| editing        =
| distributor    = Paramount Pictures
| released       =  
| runtime        = 60+ minutes (at 7236 feet)
| country        = United States Silent
}}
 Peter Ibbetson, made into a play by John N. Raphael.

Once an extant film, with the sole remaining copy held until the 1970s by Wallace Reids widow Dorothy Davenport, who donated it for a proposed museum/archive, the film is lost film|lost.   

==Plot==
 
Peter Ibbetson (Reid) is an orphan raised by his uncle, Colonel Ibbetson. When the Colonel insults his dead mother, Peter attacks him and is ordered from the house. Then the young man runs into his childhood sweetheart, Mimsi (Ferguson), and their romantic feelings are rekindled.

Unfortunately, Mimsi has married, but they carry on a love affair in their dreams. Their dream-affair continues over the years, even after Peter kills her husband, the Duke of Towers, and gets a life prison sentence.

==Cast==
* Wallace Reid as Peter Ibbetson
* Elsie Ferguson as Mimsi
* Montagu Love as Colonel Ibbetson
* George Fawcett as Duquesnois	
* Dolores Cassinelli as Dolores
* Paul McAllister as Seraskier
* Elliott Dexter as Pasquier Charles Eaton as child Gogo
* Jerome Patrick as Duke of towers

==Proposed film== Ethel and John to Paramount for five years, Ethel had been under contract to Metro Pictures but Ethels contract was ending that same year. Lionel freelanced in and out of Metro to companies like Paramount and First National. If produced the film would have united all three Barrymore siblings in their second film but also in the same scenes. A previous silent film National Red Cross Pageant (1917) had all three siblings but not in the same scenes. Never produced, the Peter Ibbetson project met the screen in the film known as Forever with Wallace Reid and Elsie Ferguson. 

==See also==
*List of lost films

==References==
 

==External links==
 
* 
* 
*  still of Elsie Ferguson and Wallace Reid from the film, University of Washington
* 

 

 
 
 
 
 
 
 
 
 