Mausam (1975 film)
 
{{Infobox film
| name           = Mausam
| image          = Mausam1975.jpg
| caption        = DVD cover Gulzar
| producer       = P. Mallikharjuna Rao
| based on       =   Gulzar
| Sanjeev Kumar Dina Pathak Om Shivpuri Satyen Kappu C.S.Dubey Lily Chakravarty
| music          = Songs:   
| cinematography = K. Vaikunth
| editing        = Waman B. Bhosle Gurudutt Shirali
| distributor    = 
| released       =  
| runtime        = 156 minutes
| country        = India Hindi
| budget         =
}}
Mausam ( ;  ) is a  .

The film was remade in Tamil as Vasandhathil or Naal with Sivaji Ganesan. 

==Plot==
Mausam is a dramatic love story of Dr. Amarnath Gill, who falls for Chanda, the daughter of a local healer, Harihar Thapa, when visiting Darjeeling for his medical exams. Amarnath is called back and promises Chanda to return, though he never keeps his promise. Twenty-five years later, he returns as a wealthy man and searches for Chanda and her father. He learns that Harihar has died and that Chanda was married to a crippled old man. She gave birth to a daughter, became insane and died. Finding Chandas daughter, Kajli, he sees that she closely resembles her mother and later discovers that after having been molested by her mothers brother-in-law, she ended up at a brothel. Amarnath had no choice but to buy her from the brothel and he takes Kajli home and tries to change her into well refined woman to make up for what he did to Chanda. Unaware that Amarnath is indirectly responsible for her mothers death, Kajli begins to fall in love for him.

==Cast==
* Sharmila Tagore  as  Chanda Thapa/Kajli
* Sanjeev Kumar  as  Dr. Amarnath Gill
* Dina Pathak  as  Gangu Rani (Brothel madame)
* Om Shivpuri  as  Harihar Thapa

==Production==
The film written simultaneously along with Aandhi (1975), together by Kamleshwar, Bhushan Banmali and Gulzar, and even shot together, with Sanjeev Kumar playing the lead of an old man in the films.  Though Aandhi was released first, it ran into political controversy and portions of it has to be reshot, meanwhile Mausam was completed and released.     While the song, "Meri ishq ke lakhon jhatke" was being shot with Sharmila Tagore, choreographer Saroj Khan was also in the studios for another film, that is when Gulzar requested her to teach a few moves to Tagore. 

==Music== Late Madan Mohan after his demise on 14 July 1975. The songs of the movie were penned by Gulzar. Mausam is one of those two movies directed by Gulzar the songs of which were composed by Madan Mohan. The other one is Koshish. 

The song Dil Dhoondta Hai, by Bhupinder Singh and Lata Mangeshkar, featured at 12th position on the Annual list of the year-end chart toppers of Binaca Geetmala for Binaca Geetmala annual list 1976|1976. 
{| class="wikitable"
|-
! Sr. No. !! Song !! Singers !! Filmed on
|- Bhupinder Singh|| title track
|-
| 2 || Chhadi Re Chhadi|| Mohammad Rafi, Lata Mangeshkar|| Sanjeev Kumar, Sharmila Tagore
|- Bhupinder Singh & Lata Mangeshkar|| Sanjeev Kumar, Sharmila Tagore
|-
| 4 || Mere Ishq Mein|| Asha Bhosale|| Sanjeev Kumar, Sharmila Tagore
|-
| 5 || Ruke Ruke Se Kadam|| Lata Mangeshkar|| Sharmila Tagore
|}

==Awards==
 

=== Won === National Award Best Actress - Sharmila Tagore
* National Film Award for Second Best Feature Film - Mausam Filmfare Best Movie - Mausam Filmfare Best Director - Gulzar

=== Nominated === Filmfare Best Actor - Sanjeev Kumar (Won by Sanjeev Kumar for Aandhi) Filmfare Best Actress - Sharmila Tagore (Won by Raakhee for Tapasya (1976 film)|Tapasya) Filmfare Best Supporting Actress - Dina Pathak (Won by Kajri for Balika Badhu (1976 film)|Balika Badhu) Filmfare Best Story - Kamleshwar (Won by Balachand Mukherjee for Arjun Pandit) Filmfare Best Khayaam for Kabhi Kabhie) Filmfare Best Lyricist - Gulzar for Dil Dhoondta Hai (Won by Sahir Ludhiyanvi for Kabhi Kabhie Mere Dil Main from the movie Kabhi Kabhie)

==References==
 

==External links==
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 