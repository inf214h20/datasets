The Cabin Crew
{{Infobox film
| name     = The Cabin Crew
| image    = 
| caption  =  traditional = 後備空姐
|                          simplified = 后备空姐
|                              pinyin = Hoùbeì Kōngjiě}}
| director = Guan Xiaojie
| producer = Huang Jianan Gao Munan Chen Huanzhang
| writer   = Guan Xiaojie
| starring = Zhao Yihuan Liu Yuting Qin Hanlei Wen Zhuo
| music    = Chenhuang Yinan
| cinematography = Zhao Ming
| editing  = He Jianyu
| studio   = IFG
| distributor = IFG
| released =  
| runtime = 107 minutes
| country = China
| language = Mandarin
| budget = 
| gross = 
}}
The Cabin Crew is a 2014 Chinese inspirational film directed and written by Guan Xiaojie, starring Zhao Yihuan, Wen Zhuo, Liu Yuting, and Qin Hanlei. The film was released in China on International Workers Day. 

==Cast==
* Zhao Yihuan as Zhao Xiaofan
* Wen Zhuo as Yu Zhi
* Liu Yuting as Liu Zhengzheng
* Qin Hanlei as Wei Junzhi
* Zou Yang as Tang Guo
* Kingdom Yuen as Teacher Yuen
* Li Manyi as President Lingyan
* Teddy Chin as Chen Xueyou
* Jiang Shan as Senior
* Xie Jiayu as Jiali
* Ma Sise as Taozi
* Gu Yue as Xiao Xiena
* Yin Zhe as Jin Mimi
* Huang Zhenyan as Huimei
* Steve Yap as the plane captain
* Tani Lin as a stewardess
* Christina Tseng as a stewardess
* Cathy Shyu as a stewardess
* Candy Chen as a stewardess

==Production==
Filming took place in Kota Kinabalu and Shanghai.   

==References==
 

==External links==
*  

 

 
 
 
 
 