Amanush 2
{{Infobox film
| name           = Amanush 2
| image          = File:Amanush 2 poster.jpg
| alt            = 
| caption        = film poster
| film name      =  
| director       = Rajib Kumar Biswas producers       =  
| writer         =  
| screenplay     = 
| story          = 
| based on       =  
| starring       =  
| narrator       =  
| music          = Arindom
| cinematography = 
| editing        = 
| studio         = Shree Venkatesh Films
| distributor    =  
| released       =   
| runtime        = 
| country        = India Bengali
| budget         = 
| gross          = 
}}
 Bengali thriller Tamil film Naan (film)|Naan which starred Vijay Antony in the lead. The film is released on 14th April 2015. 

As early as July 2014, director Biswas spoke toward actress Payal Sarkar being a major character in the film.     Filming took place in Chennai.   

==Plot==
Raghu (Soham Chakraborty) impersonates his acquaintances and kills anyone who tries to discover his true identity. A good student in school, his problems begin when his father commits suicide. Seeking revenge, he sets his house on fire, killing his mother and ending up in a juvenile home. Upon his release, he heads to Chennai to start anew and befriends Saleem, a medical student. But Saleem dies when the bus they are travelling in meets with an accident. Seeing an opportunity, Raghu impersonates Saleem and joins the medical college. There, he also makes new friends, Suresh and Ashok, and has a relationship with Riya (Payal Sarkar). One day, the gang comes across the juvenile home warden, who recognizes Raghu and reveals his real identity. 

==Cast==
* Soham Chakraborty  as Raghu
* Payal Sarkar  as Riya
* Anindya Chatterjee as Ashok Rajesh Sharma
* Surajit Sen

==Production==
===First look and trailer===
The first look was released on The Times of India on 11 October 2014,   and the trailer was released on 14 Mar 2015 on Shree Venkatesh Filmss official YouTube channel and on Sangeet Bangla.   

===Promotion===
The film was promoted on two anticipated shows of the Bengali television, Dadagiri Unlimited and Didi No. 1.

==Critical Reception==
Madhushree Ghosh of The Times of India reviewed "There are two reasons why you can spend one evening watching Amanush 2 and not regret later — Sohams impressive return after last years Golpo Holeo Sotti and a gripping storyline that keeps you glued to your seats till the end. Thankfully, like most commercial Bengali films, Amanush 2 steers clear of the typical song and dance routine, an arm-candy heroine or a ruthless villain with a countrys supply of weapons at his disposal. And if these dont excite you enough, the film has a brilliant Anindya Chatterjee (as Raghus friend Ashok), who leaves a lasting impression once again after Chotushkone. However, this film too has its share of flaws. It is predictable to a great extent, which takes away the charm of a thriller. Paayel Sarkar, as Ashoks girlfriend Riya, looks worn out. Even her heavy make-up, trendy clothes couldnt camouflage her disinterested performance. Otherwise, it is a good one-time watch." 

==References==
 

 
 
 


 