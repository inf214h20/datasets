The Smile Wins
 
{{Infobox film
| name           = The Smile Wins
| image          = 
| caption        = 
| director       = Robert F. McGowan
| producer       = Hal Roach
| writer         = Hal Roach H. M. Walker Robert A. McGowan
| starring       = 
| music          = 
| cinematography = 
| editing        = Richard C. Currier
| studio         = Hal Roach Studios
| distributor    = Pathé Exchange
| released       =  
| runtime        = 20 minutes
| country        = United States  Silent English intertitles
| budget         = 
}}
 short silent silent comedy film directed by Robert F. McGowan.       This was the 72nd Our Gang short subject released, and the last Our Gang short that Hal Roach released through Pathé Exchange.   

==Cast==
===The Gang===
* Allen Hoskins as Farina
* Jannie Hoskins as Mango
* Joe Cobb as Joe
* Jackie Condon as Jackie
* Jay R. Smith as Jay
* Harry Spear as Harry
* Pete the Pup as Himself

===Additional cast===
* Johnny Aber as Kid throwing tomato
* Mildred Kornman as Toddler on the gangs merry-go-round
* George B. French as Simon Sleazy
* Florence Hoskins as Farinas mother
* Jean Darling as Undetermined role
* Bobby Hutchins as Undetermined role
* Budd Fine as Undetermined role
* Lyle Tayo as Undetermined role

==See also==
* Our Gang filmography

==References==
 

==External links==
* 
* 


 
 
 
 
 
 
 
 
 
 
 
 