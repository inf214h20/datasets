Különös házasság
{{Infobox film
| name           = Különös házasság
| image          = 
| caption        = 
| director       = Márton Keleti
| producer       = 
| writer         = Gyula Háy Kálmán Mikszáth
| starring       = Gyula Benkő
| music          = 
| cinematography = Barnabás Hegyi
| editing        = Sándor Zákonyi
| distributor    = 
| released       = 18 February 1951
| runtime        = 112 minutes
| country        = Hungary
| language       = Hungarian
| budget         = 
}}

Különös házasság is a 1951 Hungarian drama film directed by Márton Keleti. It was entered into the 1951 Cannes Film Festival.   

==Cast==
* Gyula Benkő - Count Párdányi Buttler János
* Miklós Gábor - Bernáth Zsiga
* Lajos Rajczy - Baron Dőry István
* Hédi Temessy - Mária, daughter of Baron Dőry
* Sándor Tompa - Horváth Miklós
* Éva Örkényi - Piroska, daughter of Horváth
* Artúr Somlay - Archbishop Fischer
* Sándor Pécsi - Medve Ignác, doctor
* Sándor Szabó (actor)|Sándor Szabó - Vicar Szucsinka
* Tamás Major - Jesuit
* Hilda Gobbi - Mrs. Szimácsi
* Tivadar Uray - Fáy István főispán
* Gábor Rajnay - Advocate Pereviczky
* László Kemény - Professor Kövy
* József Bihari - Csősz

==References==
 

==External links==
* 

 
 
 
 
 
 
 


 
 