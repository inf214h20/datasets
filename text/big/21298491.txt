Ang Panday (2009 film)
{{Infobox film
| name           = Ang Panday
| image          = Ang Panday.jpg
| caption        = Theatrical film poster
| director       = Rico Gutierrez Mac Alejandre
| producer       = Annette Gozon-Abrogar Jose Mari Abacan Marlon Bautista
| screenplay     = R.J. Nuevas
| story          = Carlo J. Caparas R.J. Nuevas Ramon "Bong" Revilla Phillip Salvador Iza Calzado
| music          = Von de Guzman
| cinematography = Regiben Romana
| editing        = Augusto Salvador
| studio         = Imus Productions GMA Films
| distributor    = GMA Films
| released       =  
| runtime        = 110 minutes
| country        = Philippines
| language       = Filipino
| budget         = 
| gross          = MMFF Gross (2weeks): P99.8M
}} action Fantasy fantasy film Ramon "Bong" Revilla, Phillip Salvador and Iza Calzado. It was released in December 25, 2009.
 Ang Panday (2005).

==Plot==
Long ago, the evil wizard Lizardo (Phillip Salvador) sent an army of monsters to subjugate the land and its people. Lizardo succeeded, but a prophecy tells of a comet that will fall to Earth, and a man who will wield a weapon that will free the people from Lizardo’s tyranny. Flavio (Bong Revilla Jr.) is a blacksmith content with living a quiet, uneventful life in a town mostly untouched by Lizardo’s evil. But when the comet of prophecy lands on the outskirts of town, Flavio’s destiny is immediately made clear. Around the peace-loving but brave Flavio. His arch-enemy Lizardo attempts to ruin the peace and harmony of their dwelling place, affecting the inhabitants. Moreover, the evil warlord challenges Flavio by capturing his beautiful lady love Maria (Iza Calzado). A series of events take place, bringing the blacksmith (panday) at the forefront of a full-blown war against Lizardo’s troops.

==Cast== Ramon "Bong" Revilla as Flavio
* Phillip Salvador as Lizardo 
* Iza Calzado as Maria
* Geoff Eigenmann as Celso
* Robert Villar as Bugoy 
* Rhian Ramos as Emelita
* Sheena Halili as Dahlia
* Carlene Aguilar as Teresa / Mananaggal George Estregan Jr. as Apoykatawan
* Mike "Pekto" Nacua as Pipi
* Dan Fernandez 
* Matt Ranillo III
* Benjie Paras  as Zanboro
* John Lapus  as Kruma
* Paulo Avelino as Pepe 
* Stef Prescott as Ditas 
* Bj Forbes as Popoy 
* Luz Valdez as Lola Mameng
* Joonee Gamboa as Lolo Isko Carlos Morales as Diego
* Gladys Guevarra as Babang 
* JB Magsaysay  as Benito
* King Gutierrez as Kubada
* Dindo Arroyo as Cruzaldo
* Marissa Sanchez as Miling
* John Feir  as  Utal
* Carlo Lacana as Young Flavio
* Ace Espinosa as Flavios father
* Rey Sagum as Hugo
* Banjo Romero as Kumar
* Anne Curtis as Dyosa Adora/ Special Participation
* John Michael Este Mapa as Kumag

==35th Metro Manila Film Festival Awards==
{|| width="90%" class="wikitable sortable"
|-
! width="10%"| Year
! width="25%"| Category
! width="25%"| Recipient
! width="10%"| Result
|- 2009  Best Picture
| align="center"| Ang Panday
|  
|- Best Actor
| align="center"| Bong Revilla
|  
|- Best Supporting Actor
| align="center"| Phillip Salvador
|  
|- Best Child Performer
| align="center"| Robert Villar
|  
|- Best Original Theme Song
| align="center"| Ogie Alcasid ("Dahil Tanging Ikaw" - performed by Regine Velasquez)
|  
|- Best Visual Effects
| align="center"| Jay Santiago 
|  
|- Best Production Design
| align="center"| Richard Somes
|  
|}

==Media Release==
The series was released onto DVD-format and VCD-format by VIVA Video. The DVD contained the movie plus the full-length trailer of the movie. The DVD/VCD was released in 2010.

==Sequel==
  Ramon "Bong" Revilla, Iza Calzado and Phillip Salvador and with the new character who fulfilled by Marian Rivera will be release in December 25, 2011. 

==References==
 

== External links ==
*  

 
 
 
 
 
 

 
 
 
 
 