Ponirah Terpidana
{{Infobox film
| name           = Ponirah Terpidana
| image          = Ponirah Terpidana.jpg
| image_size     = 
| border         = 
| alt            = 
| based on       = VCD cover
| director       = Slamet Rahardjo
| producer       = {{plainlist|
*Manu Sukmajaya
*A. Gunawan
}}
| screenplay     = Slamet Rahardjo
| story          = Slamet Rahardjo
| starring       = {{plainlist|
*Nani Vidia
*Slamet Rahardjo
*Ray Sahetapy
*Christine Hakim
}}
| music          = Eros Djarot
| cinematography = Tantra Surjadi
| editing        = George Kamarullah
| studio         = Sukma Putra Film
| distributor    = 
| released       =  
| runtime        = 107 minutes
| country        = Indonesia
| language       = Indonesian
| budget         = 
| gross          = 
}}
Ponirah Terpidana (literally Ponirah is Convicted) is a 1984 Indonesian drama film directed by Slamet Rahardjo. Starring Nani Vidia, Rahardjo, and Ray Sahetapy, it follows a young woman named Ponirah who becomes a prostitute and is arrested for the murder of a rich businessman. The film, which combined traditional and contemporary elements, was a critical success in Indonesia. It won three Citra Awards at the 1984 Indonesian Film Festival, from a total of eleven nominations.

==Plot== died while prostitute to support herself and Ponirah; the two live at the brothel.
 trafficker tasked with luring young women to work as prostitutes. Although he falls in love with Ponirah and refuses to do this task, Ponirah says she wants to be the most expensive prostitute in the city. They part.

As Ponirah is picked up by a rich man named Franky Darling (Teguh Karya), Jarkasi meets Guritno and the two decide to save Ponirah. When they assault Frankys apartment, Ponirah&nbsp;– who has grown to hate men and intends to kill Franky&nbsp;– mistakes Guritno for her client and stabs her uncle with a pair of scissors, killing him. When the police come, Jarkasi tells them that he killed Guritno and prepares to be punished. However, the investigation reveals that Ponirah had delivered the killing blow and she is thus incarcerated.

==Production== on camera editing by George Kamarullah.  Rahardjos brother Eros Djarot handled the musical arrangement,  while Suparman Sidik handled sound. Benny Benhardi handled artistic direction. 
 Cinta Pertama (First Love; 1973).  Hermanto had been in cinema for over thirty years, rising to fame after starring in D. Djajakusumas Harimau Tjampa (Tiger from Tjampa) in 1953.  Meanwhile, Vidia was a new actor, making her feature film debut with Ponirah Terpidana. 

==Themes==
Nauval Yazid of The Jakarta Post writes that Ponirah Terpidana has a "women-who-suffer-continuously theme", one common in Indonesian films. He compares the later critical hit Jamila dan Sang Presiden (Jamila and the President; 2009) to the film, noting that the latter film&nbsp;– following a prostitute who murders a rich government official&nbsp;– was reminiscent of Ponirah Terpidana.  Gotot Prakosa, another writer for The Jakarta Post describes it as an art film, combining traditional and contemporary elements. 

==Release and reception==
Ponirah Terpidana received a wide release in 1984, although in 1983 it was screened at the Three Continents Festival in France.  According to Prakosa, foreign reviews criticised a scene at the end which showed a red-light district in Jakarta.  Eleanor Mannikka of allRovi gave the film three and a half out of five stars. 

A 35&nbsp;mm and VHS copy is stored at Sinematek Indonesia in Jakarta. 

==Awards== Best Leading Actress, by Sjumandjajas Budak Nafsu (Slave to Lust; 1984),  while Arifin C. Noers Pengkhianatan G30S/PKI (Betrayal of G30S/PKI; 1984) took Best Screenplay.  The film also won a special jury prize at the 1983 Three Continents Festival. 

{| class="wikitable plainrowheaders" style="font-size: 95%;"
|-
! scope="col" | Award Year
! scope="col" | Category
! scope="col" | Recipient
! scope="col" | Result
|-
! scope="row" | Three Continents Festival 1983
|Special Jury Prize
| 
| 
|-
! scope="row" rowspan="11" | Indonesian Film Festival
| rowspan="11" | 1984 Citra Award Best Film
| 
| 
|- Best Director
| Slamet Rahardjo
| 
|-
| Best Screenplay
| Slamet Rahardjo
|  
|- Best Leading Actor
|  Ray Sahetapy
|  
|- Best Leading Actress
| Christine Hakim
|  
|-
| Best Supporting Actor
| Bambang Hermanto
|  
|-
| Best Supporting Actor
| Slamet Rahardjo
|  
|-
| Best Cinematography
| Tantra Surjadi
|  
|-
| Best Artistic Direction
| Benny Benhardi
|  
|-
| Best Editing George Kamarullah
|  
|-
| Best Musical Direction
| Eros Djarot
|  
|}

==Footnotes==
 

==Works cited==
 
*{{cite web
 |title=Bambang Hermanto
 |language=Indonesian
 |url=http://filmindonesia.or.id/movie/name/nmp4b9bad981f10f_bambang-hermanto
 |work=filmindonesia.or.id
 |publisher=Konfiden Foundation
 |location=Jakarta
 |accessdate=15 February 2013
 |archiveurl=http://www.webcitation.org/6ERgf0qYc
 |archivedate=15 February 2013
 |ref= 
}}
*{{cite news
 |title=Eros Djarot: Makes a comeback with new album
 |url=http://www.thejakartapost.com/news/2009/01/25/eros-djarot-makes-a-comeback-with-new-album.html
 |work=The Jakarta Post
 |location=Jakarta
 |first=Tifa
 |last=Asrianti
 |date=25 January 2009
 |accessdate=15 February 2013
 |archiveurl=http://www.webcitation.org/6ERfs6EjU
 |archivedate=15 February 2013
 |ref= 
}}
*{{cite web
 |title=Kredit Cinta Pertama
 |language=Indonesian
 |url=http://filmindonesia.or.id/movie/title/lf-c012-73-037390_cinta-pertama/credit#.UR3t32dPP1I
 |work=filmindonesia.or.id
 |publisher=Konfiden Foundation
 |location=Jakarta
 |accessdate=15 February 2013
 |archiveurl=http://www.webcitation.org/6ERgmAXbG
 |archivedate=15 February 2013
 |ref= 
}}
*{{cite web
 |title=Kredit Ponirah Terpidana
 |language=Indonesian
 |url=http://filmindonesia.or.id/movie/title/lf-p016-83-835103_ponirah-terpidana/credit
 |work=filmindonesia.or.id
 |publisher=Konfiden Foundation
 |location=Jakarta
 |accessdate=15 February 2013
 |archiveurl=http://www.webcitation.org/6ERfPOUS8
 |archivedate=15 February 2013
 |ref= 
}}
*{{cite web
 |title=Ponirah Terpidana
 |language=Indonesian
 |url=http://www.allmovie.com/movie/ponirah-terpidana-v158277
 |work=allmovie
 |last=Mannikka
 |first=Eleanor
 |publisher=allRovi
 |accessdate=15 February 2013
 |archiveurl=http://www.webcitation.org/6ERiCbs4y
 |archivedate=15 February 2013
 |ref= 
}}
*{{cite web
 |title=Nani Vidia
 |language=Indonesian
 |url=http://filmindonesia.or.id/movie/name/nmp4c40238539e43_nani-vidia/filmography
 |work=filmindonesia.or.id
 |publisher=Konfiden Foundation
 |location=Jakarta
 |accessdate=15 February 2013
 |archiveurl=http://www.webcitation.org/6ERgOpv1d
 |archivedate=15 February 2013
 |ref= 
}}
*{{cite web
 |title=Penghargaan Budak Nafsu
 |trans_title=Awards for Budak Nafsu
 |language=Indonesian
 |url=http://filmindonesia.or.id/movie/title/lf-b016-83-877986_budak-nafsu-fatima/award
 |work=filmindonesia.or.id
 |publisher=Konfidan Foundation
 |location=Jakarta
 |accessdate=14 November 2012
 |archiveurl=http://www.webcitation.org/6CAWflQae
 |archivedate=14 November 2012
 |ref= 
}}
*{{cite web
 |title=Penghargaan Pengkhianatan G-30-S PKI
 |trans_title=Awards for Pengkhianatan G-30-S PKI
 |language=Indonesian
 |url=http://filmindonesia.or.id/movie/title/lf-p022-82-358646_pengkhianatan-g-30-s-pki/award
 |work=filmindonesia.or.id
 |publisher=Konfidan Foundation
 |location=Jakarta
 |accessdate=25 December 2012
 |archiveurl=http://www.webcitation.org/6DAEWDOnc
 |archivedate=25 December 2012
 |ref= 
}}
*{{cite web
 |title=Penghargaan Ponirah Terpidana
 |trans_title=Awards for Ponirah Terpidana
 |language=Indonesian
 |url=http://filmindonesia.or.id/movie/title/lf-p016-83-835103_ponirah-terpidana/award
 |work=filmindonesia.or.id
 |publisher=Konfiden Foundation
 |location=Jakarta
 |accessdate=8 January 2013
 |archiveurl=http://www.webcitation.org/6ERcw1i8Q
 |archivedate=8 January 2013
 |ref= 
}}
*{{cite web
 |title=Ponirah Terpidana
 |language=Indonesian
 |url=http://filmindonesia.or.id/movie/title/lf-p016-83-835103_ponirah-terpidana
 |work=filmindonesia.or.id
 |publisher=Konfiden Foundation
 |location=Jakarta
 |accessdate=15 February 2013
 |archiveurl=http://www.webcitation.org/6EReIm9BD
 |archivedate=15 February 2013
 |ref= 
}}
*{{cite news
 |last=Prakosa
 |first=Gotot
 |title=Slamet wants to bring Marsinah back from the grave
 |url=http://www.thejakartapost.com/news/2000/12/24/slamet-wants-bring-marsinah-back-grave.html
 |work=The Jakarta Post
 |date=24 December 2000
 |accessdate=15 February 2013
 |archiveurl=http://www.webcitation.org/66b0tPIsf
 |archivedate=15 February 2013
 |ref= 
}}
*{{cite news
 |last=Yazid
 |first=Nauval
 |title=A very irresistible Jamila
 |url=http://www.thejakartapost.com/news/2009/05/03/a-very-irresistible-jamila039.html
 |work=The Jakarta Post
 |date=3 May 2009
 |accessdate=1 April 2012
 |archiveurl=http://www.webcitation.org/66b0tPIsf
 |archivedate=1 April 2012
 |ref= 
}}
 

==External links==
* 

 
 

 
 