A Walk in the Sun (1945 film)
{{Infobox film
| name           = A Walk in the Sun
| image          = Walk in the Sun poster.jpg
| image_size     = 225px
| caption        = theatrical poster
| director       = Lewis Milestone
| producer       = Lewis Milestone Samuel Bronston (uncredited) Harry Brown (novel) Robert Rossen (screenplay)
| narrator       = Burgess Meredith
| starring       = Dana Andrews
| music          = Score:   Earl Robinson
| cinematography = Russell Harlan
| editing        = W. Duncan Mansfield
| studio         = Superior Productions
| distributor    = 20th Century Fox
| released       =  
| runtime        = 117 minutes English
| country        = United States
| budget         = $800,000 (est.)
| gross          =
}}
 Harry Brown, Liberty Magazine in October 1944.
 John Ireland, Norman Lloyd, Herbert Rudley and Richard Benedict, with narration by Burgess Meredith.

==Plot== Texas Division landing barge carries them to their objective during the pre-dawn hours, and the increasing danger of their situation is demonstrated when their young platoon commander, Lieutenant Rand, is wounded by a shell fragment that destroys half of his face.  Platoon Sergeant Pete Halverson takes over command and orders Sgt. Eddie Porter (Herbert Rudley) to lead the men to the beach while he tries to find the Captain and confirm their orders.
 First aid man McWilliams (Sterling Holloway) remains with Rand, and the rest of the men hit the beach and dig in while trying to elude the shelling and machine-gun fire.  Sgt. Bill Tyne (Dana Andrews) wonders what they will do if Halverson does not return, and after the sun rises, the sergeants send the men into the woods to protect them from enemy aircraft.  Tyne remains on the beach to wait for Halverson, but learns from McWilliams that both Rand and Halverson are dead.  Soon after, McWilliams is shot by an enemy airplane.

Tyne walks to the woods, and there discovers that three other men have been hit, including Sgt. Hoskins who was the senior surviving NCO.  Hoskins wound means he cannot continue and Porter as senior NCO is forced to take command.  Hoskins warns Tyne as he is leaving to keep an eye on Porter because he suspects Porter is going to crack under the pressure of command.

Porter, Tyne and Sgt. Ward (Lloyd Bridges) then lead the men in three squads along a road toward their objective, a bridge that they are to blow up that is near a farmhouse.  Porter knows that the six-mile journey will be a dangerous one, and grows agitated.   He warns the men to watch out for enemy tanks and aircraft.  As they walk, the men shoot the breeze and discuss their likes and dislikes, the nature of war and the food they wish they were eating.  Enemy aircraft appear and one of them strafes the platoon as they run for cover in a ditch.  Some of the men are killed.  Porter grows increasingly agitated.

Afterwards Porter is distracted when two retreating Italian soldiers surrender to the platoon and confirm that they are on the right road.  The Italians warn them that the area is controlled by German troops, and soon after, the platoon meets a small reconnaissance patrol of American soldiers.  After the patrols motorcycle driver offers to ride to the farmhouse and report back, Porter becomes even more edgy as minutes pass without the drivers return.  Finally Tyne tells the men to take a break while he sits with Porter.  As machine gunner Rivera (Richard Conte)  and his pal, Jake Friedman, razz each other, Porter begins to break down and tells Ward (also called Farmer) that he is putting Tyne in charge.  Porter has a complete breakdown when a German armored car approaches, but Tynes quick thinking prevails and the men blast the car with grenades and machine-gun fire. The bazooka men, who Tyne had sent ahead to search for tanks, blow up two tanks and another armored car, but expend all of their bazooka ammunition.
 John Ireland), a calm, introspective soldier suggests circling around the farm via the river and blowing up the bridge without first taking the house.  Tyne sends two patrols, headed by Ward and Windy, to accomplish the mission, then orders Rivera to strafe the house while he leads a column of men in an attack on the house, which he hopes will distract the Germans.  The remaining men nervously wait for their comrades to reach the bridge, until finally Rivera opens fire and Tyne and his men go over the stone wall and into the field. Tynes sight blurs as he crawls toward the house, and when he comes across the body of Rankin (Chris Drake), one of the fallen men, still cradling his beloved Tommy-gun, the platoons constant refrain, "Nobody dies," resounds through his head.

The bridge is blown up, and despite heavy losses, the platoon captures the house.  Then, at exactly noon, Windy, Ward and the remaining men wander through the house as Farmer fulfills his dream of eating an apple and Tyne adds another notch to the butt of Rankins pet Tommy-gun.

==Characters==
  
* Dana Andrews as Staff Sgt. Bill Tyne
* Richard Conte as Pvt. Rivera
* George Tyne as Pvt. Jake Friedman John Ireland as PFC Windy Craven
* Lloyd Bridges as Staff Sgt. Ward
* Sterling Holloway as McWilliams Norman Lloyd as Pvt. Archimbeau
* Herbert Rudley as Staff Sgt. Eddie Porter  
* Richard Benedict as Pvt. Tranella
* Huntz Hall as Pvt. Carraway
 
* James Cardwell as Sgt. Hoskins
* George Offerman Jr. as Pvt. Tinker Steve Brodie as Pvt. Judson
* Matt Willis as Sgt. Pete Halverson
* Chris Drake as Rankin
* Alvin Hammer as Johnson
* Victor Cutler as Cousins
* Jay Norris as James
* John Kellogg as Riddle
 

==Production==
Actor  , alongside that studios production of A Bell for Adano.
 34th Infantry Division in the North African campaign, where he and his regiment were captured by the Germans at the Battle of Kasserine Pass. Drake had recently been exchanged as a prisoner by the Germans due to his ill health, returning to the States in 1944.
 American half-track German half-track, and a P-51 plays the role of an "enemy" aircraft (probably intended to be either a C.205 or Bf-109).  Later, P-38s (as American aircraft) engage a radial engined "enemy" plane (an T-6 Texan posing as a FW-190) during the films climax.

In January 1945, Milestone showed the film to the U.S. Army for their approval. The Army was pleased with the film but requested two changes. They suggested that a remark be placed in the film explaining why the bazooka was not used during the attack on the farmhouse. Milestone complied with this request by shooting a scene where the bazooka crew reported that they used up all their shells in a battle with enemy tanks. The Army also requested a briefing scene at the films beginning to explain the platoons mission. They believed the film gave the impression that the platoon meandered about without an objective. Milestone authorized the shooting of such a scene but whether it was filmed but later edited out of the release no one is sure; however, a brief scene in the landing craft has the platoon sergeant explaining to the men, and the audience, that they had been briefed on their mission.
 Kenneth Spencer, replaced much of composer Freddie Richs original instrumental score. The ballad in A Walk in the Sun predates the ballad in High Noon, which also accompanied the films narrative.

Though several film companies showed strong interest in acquiring the film, 20th Century Fox acquired the film for release in July 1945 so as not to compete with Foxs A Bell for Adano released earlier. However, when Japan surrendered, Foxs head of production, Darryl F. Zanuck, stopped production of all war films. The film was released in June 1946 to critical and popular acclaim but also a strong critique of the film from director Samuel Fuller, which he sent in the form of a letter to Milestone.

Robert Rossens screenplay follows Browns book very closely. Milestone also recommended that Brown become a screenwriter in Hollywood, which led to a prolific career.

==Re-release==
A Walk in the Sun was reissued by Realart Pictures in 1951 as Salerno Beachhead. In the 1980s the film was released on VHS tape.

==Notes==
 

==External links==
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 