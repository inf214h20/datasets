Courte tête
{{Infobox film
| name           = Courte tête
| image          = 
| caption        = 
| director       = Norbert Carbonnaux
| producer       = Intermondia Films (France)
| writer         = Michel Audiard Norbert Carbonnaux
| starring       = Fernand Gravey Louis de Funès
| music          = Jean Prodomidès
| cinematography = 
| editing        = 
| distributor    = J. Arthur Rank
| released       = 10 October 1956 (France)
| runtime        = 88 minutes, 110 (USA)
| country        = France
| awards         = 
| language       = French
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
 French Comedy comedy film from 1956, directed by Norbert Carbonnaux, written by Michel Audiard, starring Fernand Gravey and Louis de Funès. The film is known under the title: "Photo Finish" (USA), "Short Head" (UK). 

== Cast ==
* Fernand Gravey: Olivier Parker, a notorious crook. posing as an equestrian coach
* Louis de Funès: Father Graziani, a fake monk Jean Richard: Ferdinand Galiveau, poultry dealer and racegoer
* Jacques Duby: Amédée Lucas aka fake jockey "Teddy Morton" 
* Darry Cowl: the receptionist at the hotel "Lutécia"
* Micheline Dax: Lola dHéricourt, a venturer
* Max Révol: the general agant for starch
* Robert Murzeau: the taylor, also a racegoer
* Jacques Dufilho: the stable-boy

== References ==
 

== External links ==
*  
*   at the Films de France

 
 
 
 
 
 
 


 