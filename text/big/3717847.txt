Bruce Lee: A Warrior's Journey
{{Infobox film
| name           = Bruce Lee: A Warriors Journey
| image          = BruceLeeAWarriorsJourney.jpg
| caption        = Hong Kong DVD cover
| film name = {{Film name| traditional    = 李小龍：死亡遊戲之旅
 | simplified     = 李小龙：死亡游戏之旅
 | pinyin         = Lǐ Xiǎolóng : Sǐwáng Yóuxì zhī Lǚ 
 | jyutping       = Lei5 Siu2lung4 : Sei2mong4 Jau4hei3 zi1 Leoi2}} John Little Bruce Lee   (G.O.D. footage) 
| producer       = Chris Ennis Lee Taek-Yong John Little Bruce Lee   Concord Production Inc.|(co-producer of G.O.D. footage) 
| writer         = John Little Bruce Lee   (material)   Bey Logan   (additional material)   John Little Dan Inosanto
| music          = Wayne Hawkins
| cinematography = 
| editing        = Brad Kaup
| studio         = 
| distributor    = Warner Home Video
| released       =  
| runtime        = 110 minutes
| country        = United States Hong Kong English Standard Cantonese
| budget         = 
| gross          = 
}}
{{Chinese
|t=李小龍：勇士的旅程
|s=李小龙：勇士的旅程
|mi= 
|myr=Lǐ Syǎulúng : Yǔngshr̀ de Lyǔchéng
|y=Leíh Síu Lùhng : Yúhng sih dī Leuíh chìhng
|ci= 
}} documentary on English and Cantonese dubbing as part of the documentary. Most of the footage which was shot is from what was to be the centre piece of the film. 

== Enter the Dragon: Special Edition == racial problems which he faced in United States|America.

== Cast ==
* Bruce Lee
* Kareem Abdul Jabbar
* Dan Inosanto
* Ji Han Jae
* John Little 
* Linda Lee Cadwell
* Taky Kimura

==External links==
* 
* 

==References==
 

 

 
 
 
 
 
 
 

 
 