Grandma (film)
{{Infobox film
| name           = Grandma 
| image          = 
| alt            = 
| caption        =  Paul Weitz
| producer       = Paul Weitz Terry Dougas Paris Kasidokostas Latsis Andrew Miano
| writer         = Paul Weitz	
| starring       = Lily Tomlin Julia Garner Marcia Gay Harden
| music          = Joel P. West
| cinematography = Tobias Datum
| editing        = Jon Corn
| studio         = 1821 Pictures
| distributor    = Sony Pictures Classics
| released       =  
| runtime        = 78 minutes
| country        = United States
| language       = English
| budget         = 
}}
 comedy drama drama film, Paul Weitz.  The film premiered at 2015 Sundance Film Festival on January 30, 2015 and served as the closing night film. 
 Big Business. It is the second collaboration between Tomlin and Weitz, who previously directed her in his 2013 film Admission (2013 film)|Admission.    The film is scheduled to be released on August 21, 2015, by Sony Pictures Classics.

==Plot==
Ellie, a lesbian poet whose long term life partner has died recently. On arrival of her 18 years old grand daughter Sage, Ellie goes on a road trip with her, as they come to terms with their troubles.

==Cast==
*Lily Tomlin as Elle Reid
*Julia Garner as Sage
*Marcia Gay Harden as Judy
*Judy Greer as Olivia
*Laverne Cox as Deathy
*Elizabeth Peña as Carla
*Nat Wolff as Cam
*John Cho as Chau
*Sam Elliott as Karl
*Mo Aboul-Zelof as Ian
*Sarah Burns

==Production==

===Development===
Paul Weitz had the story idea for many years but it never really fully formed until he met Lily Tomlin, saying that "After meeting Lily, the voice and the character really clicked, I had thought about it for years, so I had a lot of it worked out in my head, and then I just went to a coffee shop and wrote it longhand." 

===Filming===
Filming took place in Los Angeles in spring of 2014 and completed in 19 days. 

==Marketing and promotion==
Sony Pictures Classics acquired the distribution rights of the film in United States after its premiere at Sundance.  A scene from the film, featuring Tomlin and Garner, released on January 22, 2015. 

==Reception==
 .]]
Grandma received mostly positive reviews upon its premiere at the 2015 Sundance Film Festival, with Tomlin and Elliots performances receiving critical acclaim. The film has a "certified fresh" score of 80% on Rotten Tomatoes, based on 5 reviews, with an average rating of 5.8 out of 10. 
 Variety called it "An initially breezy family comedy about mothers, daughters and abortions that slowly sneaks up on you and packs a major wallop."  Brian Moylan of The Guardian gave the film three out of five stars and said that it is "Possibly the greatest thing about Grandma is that it passes the Bechdel test with flying colours, better than any film Ive seen recently."  Edward Douglas in his review for ComingSoon praised the film that it is "A perfectly pleasant mix of humor and drama that will remind you how much Lily Tomlin is missed as a regular fixture in movies."  While David Rooney of The Hollywood Reporter gave the film a positive review by calling it "mall-scale but consistently funny and poignant comedy-drama" and praising Tomlin said that it is "A sublime match of performer and role." 

==Release==
The film is scheduled to be released on August 21, 2015, by Sony Pictures Classics. 

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 