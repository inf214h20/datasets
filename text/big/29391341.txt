Based Down South
{{Infobox film
| name           = Based Down South
| image          = BasedDownSouthFilmPoster.jpg
| alt            = 
| caption        = Theatrical Poster
| director       = Martina Priessner
| producer       = Claudia Wolf
| writer         = Martina Priessner
| starring       = 
| music          = 
| cinematography = Anne Misselwitz
| editing        = Bettina Blickwede
| studio         =
| distributor    =
| released       =  
| runtime        = 88 minutes
| country        = Germany  Turkey
| language       = German  Turkish
| budget         = 
| gross          = 
}}
Based Down South ( ) is a 2010 German-Turkish documentary film written and directed by Martina Priessner about four Turks born in Germany but who now live in Istanbul. The film was selected for the 29th International Film Festival Istanbul, where it premiered, 53rd International Leipzig Festival for Documentary and Animated Films, 16th London Turkish Film Festival and the 13th Istanbul International 1001 Documentary Film Festival. Based Down South was nominated for the Grimme Award in 2011. 

== Synopsis ==
What Buelent, Murat, Fatos and Ciğdem share are memories of their childhood and youth in Germany. Now all of them live in Istanbul, three against their will. Fatos and Murat were forced to go to Turkey by their parents, Buelent was deported five years ago. Only Cigdem, the young manager with a German passport freely opted for a life in Istanbul. But even after decades in their parents country of origin, the other three have never really managed to make it their home. Insteas, they have constructed a surrogate Germany for themselves.

==Release==
===Festival screenings===
* 29th International Film Festival Istanbul (April 3–18, 2010)   
* Documentarist: Documentary Days of Istanbul (June 22–27, 2010)
* 16th Film Presentation Baden-Württemberg (December 1 – 5, 2010)
* 16th London Turkish Film Festival (November 4–18, 2010)   
* 13th Istanbul International 1001 Documentary Film Festival
* 53rd International Leipzig Festival for Documentary and Animated Films (Oktober 18–24, 2010)

==See also==
* 2010 in film
* Turkish films of 2010

==References==
 

==External links==
*  
*  

I 9

 
 
 
 
 
 
 
 
 

 