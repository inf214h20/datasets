Premante Idera
{{Infobox film
| name           = Premante Idera
| image          =
| caption        = DVD cover for the Hindi version titled Dulhan Dilwale Ki
| director       = Jayanth C. Paranjee
| writer         = Paruchuri Brothers  
| story          = Deenraj
| screenplay     = Jayanth C. Paranjee Venkatesh Preity Zinta 
| producer       = Burugupalli Siva Rama Krishna,   K.Ashok Kumar Raj  
| studio         = Sri Lakshmi Venteswara Art Films
| distributor    =
| editing        = Marthand K. Venkatesh
| cinematography = Jayanan Vincent
| released       = 30 October 1998
| runtime        = 2:47:41
| language       = Telugu
| budget         =
}} Telugu movie, 1998 produced by Burugupalli Siva Rama Krishna & K.Ashok Kumar on Sri Lakshmi Venteswara Art Films banner, directed by Jayanth C. Paranjee. Starring Daggubati Venkatesh|Venkatesh, Preity Zinta in lead roles and music composed by Ramana Gogula its his first film as music director. The film is recorded as Super Hit at box-office. It was dubbed into Hindi as Dulhan Dilwale Ki in 2001. It was remade in Kannada as O Premave with Ravichandran.

==Plot==
Murali (Daggubati Venkatesh|Venkatesh) and his friends go to a village to attend their friend`s marriage. There Murali meets a girl Shailu (Preity Zinta) bridegrooms friend they begin to have fun and join in with the festivities and Murali is captivated by her attitude and beauty. Soon both fall in love but Murali soon finds out that she is to be married to a police officer Muralidhar (Srihari).

Murali puts on a brave face for the ceremony but hopes that something or someone will be able to stop the ceremony. He does everything he can to stop the marriage, he even tries to convince Shailus joint family. Finally the movie ends with the approval of Venktramayya (Ranganath (actor)|Ranganath) for their marriage.

==Cast==
{{columns-list|3| Venkatesh as Murali
* Preity Zinta as Shailaja / Shailu Satyanarayana as Shailus grandfather 
* Srihari as Muralidhar 
* Brahmanandam as Avadhani Ali as Ram Pandu Chandra Mohan as Subba Rao  Ranganath as Venkatramayya 
* Giri Babu as Muralis father
* Prasad Babu as Suryam
* Narra Venkateswara Rao as Satyams father 
* Raghunath Reddy as Muralis Uncle
* Maharshi Raghava  as Muralis brother
* Sivaji Raja as Satyam  Sivaji as Sivaji
* Prasanna Kumar 
* Naveen as Sarath
* Gadiraju Subba Rao Lakshmi as Muralis mother
* Rama Prabha as Subbammatta
* Sana as Shailus aunty
* Rajitha as Shailus aunty
* Bangalore Padma as Satyams mother
* Madhurisen as Lakshmi
* Harika as Muralis sister-in-law
* Neelima 
* Tarangini as Shailus friend 
* Madhu Mani as Shailus friend
}}

==Soundtrack==
{{Infobox album
| Name        = Premante Idera
| Tagline     = 
| Type        = film
| Artist      = Ramana Gogula
| Cover       = 
| Released    = 1998
| Recorded    = 
| Genre       = Soundtrack
| Length      = 32:17
| Label       = Aditya Music
| Producer    = Ramana Gogula
| Reviews     =
| Last album  = Premante Idera   (1998)
| This album  = Thammudu (film)|Thammudu   (1999)
| Next album  = Yuvaraju   (2000)
}}

Music composed by Ramana Gogula. All songs are blockbusters. Music released on ADITYA Music Company.
{{Track listing
| collapsed =
| headline =
| extra_column = Singer(s)
| total_length = 32:17
| all_writing =
| all_lyrics =
| all_music =
| writing_credits =
| lyrics_credits = yes
| music_credits =

| title1  = Nizam Babulu Chandrabose 
| extra1  = Mano (singer)|Mano, Swarnalatha 
| length1 = 4:32

| title2  = Nalo Unna Prema	 Sirivennela Seetarama Sastry  SP Balu, Chitra
| length2 = 4:43

| title3  = Vayasa Chusuko  Sirivennela Seetarama Sastry SP Balu, Chitra
| length3 = 4:59

| title4  = Manase Edhuru Thirige Sirivennela Seetarama Sastry SP Balu, Chitra
| length4 = 4:49

| title5  = Yemo Ekkadundho Sirivennela Seetarama Sastry SP Balu, Chitra 
| length5 = 2:12 

| title6  = O Meri Bul Bul Thara Chandrabose 
| extra6  = Ramana Gogula
| length6 = 5:10

| title7   = Bombai Bomma Chandrabose 
| extra7   = Ramana Gogula
| length7  = 4:10

| title8   = Theme Music
| lyrics8  = Instrumental 
| extra8   = Ramana Gogula
| length8  = 1:01
}}

==External links==
*  

 

 
 
 
 

 