Dream Lover (1986 film)
{{Infobox film
| name           = Dream Lover
| image          = Dream lover 1986.jpg
| image_size     =
| caption        = Theatrical release poster
| director       = Alan J. Pakula
| producer       = Jon Boorstin
| writer         = Jon Boorstin
| narrator       =
| starring = {{Plainlist|
* Kristy McNichol
* Ben Masters
* Paul Shenar
}}
| music          = Michael Small
| cinematography = Sven Nykvist
| editing        = Angelo Corrao Trudy Ship
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 104 minutes
| country        = United States English
| budget         =
| gross          =
}}

Dream Lover is a 1986 thriller film about a woman who undergoes sleep deprivation therapy after being attacked in her apartment - with unexpected results. The film was directed by Alan J. Pakula, and stars Kristy McNichol, Ben Masters, and Joseph Culp. 

The film was not well received by critics. Movie historian Leonard Maltin called it "Abysmal...an intriguing concept is torpedoed by snaillike pacing and sterile atmosphere. What Alfred Hitchcock could have done with this!" 
 song of the same title is not used in the movie.

==Cast==
*Kristy McNichol as Kathy Gardner
*Ben Masters as Michael Hansen
*Paul Shenar as Ben Gardner
*Justin Deas as Kevin McCann
*John McMartin as Martin
*Gayle Hunnicutt as Claire
*Joseph Culp as Danny Matthew Penn as Billy
*Paul West as Shep Matthew Long as Vaughn Capisi
*Jon Polito as Dr. James Ellen Parker as Nurse Jennifer

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 


 