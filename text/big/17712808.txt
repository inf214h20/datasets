A Family Affair (1937 film)
 
{{Infobox film
| name           = A Family Affair
| image          = AFamilyAffair1937Poster.jpg
| caption        = Film Poster
| director       = George B. Seitz James Dugan (assistant) Samuel Marx
| based on       =  
| writer         = Kay Van Riper Hugo Butler
| starring       = Lionel Barrymore Cecilia Parker Spring Byington Eric Linden Charley Grapewin David Snell
| cinematography = Lester White
| editing        = George Boemler
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 69 minutes
| country        = United States
| language       = English
| budget         = $178,000 Scott Eyman, Lion of Hollywood: The Life and Legend of Louis B. Mayer, Robson, 2005 p 324 
| gross          = $502,000 
}}

A Family Affair (1937) is the first entry in the sixteen Andy Hardy film series, though Mickey Rooney has a secondary role as the son of Judge Hardy, played by Lionel Barrymore. Lewis Stone and Fay Holden replaced Lionel Barrymore and Spring Byington as Judge and Mrs. Hardy in the subsequent films of the series. 

The highly respected judge has to deal with family and political problems. The film was based on the play Skidding by Aurania Rouverol.

 

==Plot summary==

Judge Hardy (Lionel Barrymore) hopes to be re-elected, but his campaign is put in jeopardy by his opposition of a wasteful public works program. 
In Carvel (a small, idealized American town) lives the Hardy family of Judge James K Hardy, wife Emily and teenage son Andy (Mickey Rooney).  Judge Hardy (Lionel Barrymore)is well admired and respected. However his chances for being reelected as Judge are threatened when he blocks the construction of a $30,000,000 aqueduct.

Spurned, contractor Hoyt Wells and newspaper publisher Frank Redmond swear to block his reelection campaign.  Frank agrees to use his paper, The Carvel Star to publish disparaging stories about the family.

That evening Hardy daughter Marion returns home from college. Older daughter Joan Hardy Martin moves in as well, after a secret separation from her husband Bill. The family throws a party for returning Marion.  At the Party they are warned by a Star gossip columnist that only negative stories are going to be published about the family
Later that night Teenaged Andy Hardy reluctantly takes his childhood sweetheart Polly to a party, and is pleasantly surprised at what a beautiful woman she has grown into.
Marion has also found love in Wayne Trent, an engineer who has come to town to work on the aqueduct.  Facing the possibility of her boyfriend losing her job, she questions her father’s decision to block the construction.

Meanwhile Joan confesses to her father that she and Bill are separated after she went to a roadhouse with another man.  Although the encounter was innocent, Bill was enraged, and they soon separated.

The Carvel Star publishes an article stating the people want are calling for Judge Hardy’s impeachment.  Judge Hard attempts to bring contempt of court proceeding against the Star.

==Cast==
* Lionel Barrymore as Judge James K. Hardy
* Cecilia Parker as Marion Hardy
* Eric Linden as Wayne Trent III
* Mickey Rooney as Andy Hardy
* Charley Grapewin as Frank Redmond
* Spring Byington as Mrs. Emily Hardy
* Julie Haydon as Joan Hardy Martin
* Sara Haden as Aunt Milly Forrest
* Allen Vincent as William Booth Martin
* Margaret Marquis as Polly Benedict
* Selmer Jackson as Hoyt Wells

==Original Play==
{{Infobox play
| name       = Skidding
| image      = 
| image_size = 
| caption    = 
| writer     = Aurania Rouverol
| characters = 
| setting    = The living room of Judge Hardy in a certain town in Idaho
| premiere   = 21 May 1928 Bijou Theatre
| orig_lang  = English
| subject    = 
| genre      = comedy
}}

The movie was based on the play Skidding by Aurania Rouverol. 

==Production==
The film was made in the wake of the success of Ah, Wilderness! (film)|Ah, Wilderness! (1936). Many of the same cast from that movie returned. 

==Reception==
The film made a profit of $153,000. 

==See also==
* Lionel Barrymore filmography

==References==
 

==External links==
*  
*  
*  
*  
*   at Andy Hardy Films
*  }}

 
 

 
 
 
 
 
 
 
 
 
 