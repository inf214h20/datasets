Firestarter (film)
 
{{Infobox film
| name           = Firestarter
| image          = Firestarterposter84.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Mark L. Lester
| producer       = {{Plainlist|
* Frank Capra, Jr. Martha Schumacher}}
| screenplay     = Stanley Mann
| based on       =  
| starring       = {{Plainlist|
* David Keith
* Drew Barrymore
* Freddie Jones
* Heather Locklear
* Martin Sheen
* George C. Scott
* Art Carney
* Louise Fletcher}}
| music          = Tangerine Dream
| cinematography = Giuseppe Ruzzolini
| editing        = {{Plainlist|
* David Rawlins
* Ronald Sanders}} Dino de Laurentiis Company Universal Pictures
| released       =  
| runtime        = 114 minutes  
| country        = United States
| language       = English
| budget         = $15 million
| gross          = $17,080,167 (US) 
}} science fiction thriller film The Shop Chimney Rock, and Lake Lure, North Carolina.
 the Sci-Fi Channel. 

==Plot==
Andy McGee met his future wife, Vicky Tomlinson, in college while they were earning money by participating in an experiment in which they were given a dose of a low-grade hallucinogen called LOT-6. The experiment grants Vicky the ability to read minds; Andy can make people do and believe what he wants, but the effort gives him nosebleeds (the novel revealing them to be "pinprick" hemhorrages). Andy and Vicky went on to get married, and they now have a 9-year-old daughter named Charlene "Charlie" McGee, who can start fires with just one glance, and also predict the near future.
 The Shop"), was checking on them. The government wants to capture Charlie and harness her powerful firestarting ability as a weapon. Andy rescues Charlie from abduction by agents of The Shop. He makes the agents blind and for the next year they are on the run.

To protect themselves, Andy writes letters to major newspapers, but mailing them reveals their location. The Shop sends the one-eyed agent John Rainbird to capture them and stop the mail. At the Shops facility, father and daughter are kept separated. Andy is medicated, and subjected to tests, which show his powers have decreased. Meanwhile, Rainbird takes the role of "John the friendly orderly" to befriend Charlie and encourage her to submit to tests.

Charlies demonstrated powers increase exponentially, and she continually demands to see her father. Andy stops swallowing his drugs and slowly recovers his power, which he uses to influence Captain Hollister to arrange an escape from the facility. Charlie tells "John" about the escape, and he makes sure to be there. On his way to rendezvous with Charlie at the facilitys stables, Andy learns about Rainbirds ruse, and reveals it to her. Andy forces Hollister to shoot at Rainbird; Rainbird kills Hollister and fatally wounds Andy, then is burned to death by Charlie. With his dying breath, Andy tells Charlie to destroy the facility and run, and she does, leaving the facility up in flames, killing many people. Shortly afterwards, Charlie (who has returned to the Manders farm) heads to New York City to tell her story to the media.

==Cast==
* David Keith as Andrew "Andy" McGee
* Drew Barrymore as Charlene "Charlie" McGee
* Freddie Jones as Dr. Joseph Wanless
* Heather Locklear as Victoria "Vicky" Tomlinson/McGee
* Martin Sheen as Captain Hollister
* George C. Scott as John Rainbird
* Art Carney as Irv Manders
* Louise Fletcher as Norma Manders
* Moses Gunn as Dr. Pynchot
* Antonio Fargas as Taxi Driver

==Reception==
Firestarter received mixed reviews from critics. It has a 39% rating on Rotten Tomatoes. Rotten Tomatoes page: " ." 

==Soundtrack==
{{Infobox album
| Name = Firestarter
| Type = Soundtrack
| Artist = Tangerine Dream
| Cover = Firestarter (soundtrack).png
| Caption = 1984 LP album cover
| Released = 1984
| Recorded = 1984
| Genre = Electronic music
| Length = 41:39 MCA
| Producer =
| Last album = Wavelength (soundtrack)|Wavelength   (1984)
| This album = Firestarter   (1984)
| Next album = Flashpoint (soundtrack)|Flashpoint   (1984)
}}
Allmusic rated this soundtrack four out of five stars.  The score is composed by electronic music group Tangerine Dream.

{{tracklist
|title1=Crystal Voice
|length1=3:07
|title2=The Run
|length2=4:50
|title3=Testlab
|length3= 4:00
|title4=Charly the Kid
|length4=3:51
|title5=Escaping Point
|length5= 5:10
|title6=Rainbirds Move
|length6= 2:31
|title7=Burning Force
|length7= 4:17
|title8=Between Realities
|length8= 2:53
|title9=Shop Territory
|length9= 3:15
|title10=Flash Final
|length10=5:15
|title11=Out of the Heat
|length11= 2:30
}}

;Personnel
* Edgar Froese– keyboards, electronic equipment, guitar
* Christopher Franke– synthesizers, electronic equipment, electronic percussion
* Johannes Schmoelling– keyboards, electronic equipment

==References==
 

==External links==
 
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 