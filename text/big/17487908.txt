Unfair: The Movie
Unfair: drama Unfair (drama)|Unfair, and still starring Ryoko Shinohara in the main role. Its a "Die Hard"-like movie,  where the heroine, a police officer, must free her daughter trapped in a hospital taken by terrorists asking for a ransom.

==Synopsis==
Hard-drinking female detective Yukihira Natsumi (Shinohara) has the best record in the police force, but her independent personality and penchant to not go by the book has alienated her from colleagues, while her obsession with work has already cost her her marriage and custody of her eight-year-old daughter. Natsumi has no time for misgivings when there is crime to be fought. She is forced to reconsider everything she knows, however, when terrorists take over the hospital where her daughter is being treated. 

==Cast==
* Ryoko Shinohara ...  Natsumi Yukihira
* Kippei Shiina ...  Kuniaki Goto
* Hiroki Narimiya ...  Toda
* Sadao Abe ...  Yuji Kokubo Mari Hamada (濱田マリ) ...  Anna Hasumi
* Rosa Kato ...  Hiroko
* Mion Mukaichi ...  Mio
* Masaya Kato ...  Kaoru Mikami
* Ren Osugi ...  Assistant Director Irie
* Susumu Terajima ...  Tetsuo Yamazaki
* Yosuke Eguchi ...  Jin Saiki

==References==
 

==External links==
* 

 
 
 
 
 


 
 