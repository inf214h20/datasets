Yugandhar
 
{{Infobox film
| name           = Yugandhar
| image          = Yugandhar (1979).JPG
| image size     =
| caption        =
| director       = K. S. R. Das
| producer       = P. Vidya Sagar
| writer         = Salim-Javed
| narrator       =
| starring       = N.T. Rama Rao,  Jayasudha,  Kongara Jaggayya|Jaggaiah,  Sheela,  Chalam
| music          = Original songs and score:  
| cinematography = M C Shekar
| editing        = P Venkateswara Rao
| distributor    = Sri Gajalakshmi Arts
| released       = 30 November 1979
| runtime        = 159 min.
| country        = India Telugu
| budget         =
| preceded by    =
| followed by    =
}}
 1979 Cinema Indian feature directed by K. S. R. Das and produced by P. Vidya Sagar, starring N.T. Rama Rao, Jayasudha, Kongara Jaggayya|Jaggaiah, Sheela and Chalam. Jayamalini does a dance number in the film.  The director was K. S. R. Das, who stated that this film became a hit. 
 Don came out first. The films score and soundtrack was composed by Ilaiyaraaja.

==Plot==

Yugandhar (N.T. Rama Rao) is the most wanted criminal and killer. He is brave as well as intelligent, so the police force always fails at nabbing him.  He kills Ramesh, who is one of his gang members, when he finds out that hes also an informant to the police.  His fiancee Kamini (Jayamalini) tries to avenge his death, but Yugandhar kills her.  Thereafter, his sister Jaya (Jayasudha) takes judo and karate lessons and infiltrates the mob, in order to avenge her brothers death.  But before she gets a chance and unbeknownst to her, Yugandhar is fatally shot during a police encounter and later he dies. The Police head is soon forced to bring a look alike of Yugandhar in order to arrest Yugandhars gang members.

==Cast==

*N.T. Rama Rao as Yugandhar
*Jayasudha as Jaya
*Kaikala Satyanarayana
*Jaggayya
*Prabhakar Reddy
*Sheela
*Chalam
*Jayamalini as Kamini

==Trivia==

The Hindi song Khaike Pan Banaraswala was also remade into Telugu as Orabba Esukunna Killi. 
 China Town starring Shammi Kapoor. 

In 2009, a second Telugu remake of Hindi film Don (1978 film)|Don titled Billa (2009 film)|Billa starring Prabhas, Anushka Shetty and Krishnam Raju was released.   Jayasudha played a guest role, different from the role she played in "Yugandhar."

==References==
 

==External links==
*  

 
 

 
 
 
 
 
 


 