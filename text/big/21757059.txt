Dive Bomber (film)
{{Infobox film
| name           = Dive Bomber
| image          = File:Dive Bomber - Poster.jpg
| image_size     = 225px
| caption        = original theatrical Poster
| director       = Michael Curtiz
| producer       = Hal B. Wallis
| writer         = Frank Wead Frank Buckner Based on an original story by Wead
| starring       = Errol Flynn Fred MacMurray
| music          = Max Steiner
| cinematography = Bert Glennon Winton C. Hoch Charles A. Marshall
| editing        = George Amy
| distributor    = Warner Bros.
| released       = August 12, 1941 
| runtime        = 133 minutes 
| country        = United States
| language       = English
| budget         = $1.7 million
| gross          = over $3 million Charles Tranberg, Fred MacMurray: A Biography, Bear Manor Media, 2014 
}}

Dive Bomber is a 1941 American aviation film directed by Michael Curtiz  and starring Errol Flynn and Fred MacMurray. The film is notable for both its Technicolor photography of pre-World War II United States Navy aircraft and as a historical document of the U.S. in 1941,  including the aircraft carrier  , one of the best known World War II U.S. warships.
 documentary style, the film contains elements of true events that were involved in period aeromedical research, as well as real contemporary medical equipment. 
 Oscar for Best Color Cinematography at the 14th Academy Awards in 1942. The movie is dedicated to the flight surgeons of the U.S. armed forces "in recognition of their heroic efforts to solve the immensely difficult problems of aviation medicine." The film was a big hit at the box office, rounding out as the 19th highest-grossing film of 1941.

==Plot==
During pre-war operations from an aircraft carrier off Hawaii, the VB-3 dive bombing squadron (bearing the "High Hat" emblem of Bombing Squadron Four) arrives in a wingover approach to Honolulu; one of its pilots blacks out during the high speed dive and crashes. At the base hospital in Honolulu, Lt. Cmdr. Joe Blake (Fred MacMurray) is concerned that Lt. "Swede" Larson (Louis Jean Heydt) will not survive.  U.S. Navy Doctor, Lt. Doug Lee (Errol Flynn), convinces the Senior Surgeon (Moroni Olsen) to operate but the pilot dies on the operating table. After Blake blames Lee for rushing the surgery, the doctor decides to become a flight surgeon, and winds up being trained at the U.S. Naval Air Station in San Diego by a number of instructors, including his nemesis, Blake. A sub-plot involving the romantic adventures of Blake, Lee and a group of mechanics, introduces Mrs. Linda Fisher (Alexis Smith) as a love interest for the two rivals, Blake and Lee.

On completion of his flight training, Lt. Lee is posted as an assistant to a senior Navy surgeon, Cdr. Lance Rogers (Ralph Bellamy), who is working to find a solution for altitude sickness that affects pilots in dive bombers. Lee flies with Blake as his pilot in a camera-equipped aircraft and observes Blake blacking out. He experiments with a pneumatic belt that will keep blood above the heart and successfully flight tests it himself, although he disobeys regulations in flying by himself. Even though he has qualified as a pilot, Lee is still not trusted, considered a "grandstander" and a "vulture", always there when someone crashes. His judgment over pilots ability to fly is further resented when he grounds a pilot, Lt. Tim Griffin (Regis Toomey), who is suffering from chronic fatigue. In anger, Griffin quits the U.S. Navy, and joins the Royal Air Force in Canada but visits his old squadron when he is ferrying a new fighter from the Los Angeles factory. On his return flight, Griffin suffers from fatigue and dies attempting to land at an emergency field, completely misjudging his approach. 

Blake finally accepts that the flight surgeon is trying to help pilots survive dangerous high altitude flying, and volunteers as a "guinea pig" pilot for aerial experiments. The first flight test of a pressurized cabin nearly ends in disaster when the aircraft ices up and Blake passes out, forcing Dr. Lee to take over. After ground testing of a new invention jointly developed by Lee and Blake, a pressure suit, Blake is told that he did not pass his most recent physical and will be grounded. Taking off without permission, Blake carries out the aerial testing of the new suit anyway, but when the oxygen regulator fails, he loses consciousness and fatally crashes. His notes are salvaged from the wreckage, however, and mass production of the suit can begin. In the final scene, Blakes self-sacrifice is acknowledged while Rogers and Lee are honored for their pioneering work in protecting pilots flying at high altitude.

An ongoing motif involving cigarettes in National Air Races cases that each of the "High Hats" squadron pilots carries, continues into the final sequence where Lee throws Blakes cigarette case out over the Pacific as a final tribute.

 s of VFA-14|VB-4 in formation, as seen in the opening scenes]]
 

==Cast==
*Errol Flynn as Lt. Douglas S. "Doug" Lee, Medical Corps, USN
*Fred MacMurray as Lt. Cmdr. Joe Blake, USN
*Ralph Bellamy as Lt. Cmdr. Lance Rogers, Medical Corps, USN
*Alexis Smith as Mrs. Linda Fisher Robert Armstrong as Art Lyons
*Regis Toomey as Lt. Tim Griffin, USN
*Allen Jenkins as Hospital Corpsman 2nd Class "Lucky" James Craig Stevens as Pilot Trainee, Ens. John Anthony, USN
*Herbert Anderson as Lt. "Slim" Markham, Medical Corps, USN
*Moroni Olsen as Cmdr. Martin, Medical Corps, USN
*Dennie Moore as Ex-wife of "Lucky" James
*Louis Jean Heydt as Lieutenant "Swede" Larson, USN
*Cliff Nazarro as Double-talking Corpsman

==Production==
===Development=== Pearl Harbor Hardwick and Schnepf 1989, p. 58.  Based on a screenplay adapted from an original story by U.S. Navy Commander (retired) Frank "Spig" Wead, Dive Bomber features spectacular flying scenes (as in other films with a scenario written by Wead) interwoven into a storyline of medical research undertaken to combat the effects of high g-force|"G" combat maneuvers.  

Upon the first reading of the proposed script, the Department of the Navy realized the potential of the film to be a showcase of U.S. naval aviation and lent its full support to the production.  Dive Bomber s working title was "Beyond the Blue Sky". 

In December 1940, Warner Bros. originally intended to star  . Retrieved: February 7, 2014.   By February 1941, however, the script had been revised and the project reconfigured as an Errol Flynn film, with a new director.  Filming was given priority over Flynns other planned vehicle, They Died with Their Boots On (1941) because the U.S. Navy requested Dive Bomber be made as soon as possible. 

Fred MacMurray was borrowed from Paramount to star opposite Flynn. In exchange, Paramount borrowed Olivia de Havilland from Warners for Hold Back the Dawn (1941). 


Pre-production planning began in January 1941, and once the generous $1.7 million budget was established, a tight schedule of two months was determined for principal photography on location.  Although the U.S. Navy had agreed to cooperate, providing over 1,000 officers and enlisted men along with access to some of its most highly secured facilities, world events had dictated a full war-readiness, which placed additional pressures on the film cast and crew. With all naval air and sea forces committed to training and war exercises, Curtiz was notified in advance so that he could take advantage of the ongoing activities and set up his camera crews accordingly.  

===Filming=== Orriss 1984, p. 27.  

One bizarre incident came about when a formation of aircraft flew over the film set at a time when all the cameras were being reloaded. Curtiz reacted immediately by standing up and waving them off when he realized that the camera crews were not in position. The pilots, who were setting out on a daily exercise, simply ignored the gesticulating and screaming Curtiz below. The film crews were mightily amused by Curtizs declarations of "No, no. Go back!" as if the flight crews could actually hear him. The incident was symptomatic of the many outbursts and exchanges on the set when the autocratic Curtiz helmed a film. 
 NAS North Farmer 1989, p. 81.   

When principal photography commenced, the initial aircraft scenes featured U.S. Navy aircraft in their pre-war colorful schemes, set off by the Technicolor process, making this a vibrant document, unique for its time.  
 Orriss 1984, p. 28. 
 Orriss 1984, p. 25.  
 The Adventures of Robin Hood. Flynn did taxi a dive bomber to allow closeups of him in the cockpit during a critical scene when his character "is testing a G-resistant belt." 

The bulky Technicolor cameras caused considerable problems in the scenes filmed in the air; special mounts were built to allow one of the two cameras in the chase aircraft to be moved back and forth.  With the preponderance of actual aerial footage shot from a bevy of camera platforms, both on the ground and mounted to aircraft, Dive Bomber was notable in the restrained use of special effects. During the two months of studio post-production, footage of scale models and closeup "blue screen" effects were matched up with the aerial sequences. 

===Aircraft featured=== N3N Canary trainers were the primary type in use at Naval Air Station North Island and are featured prominently in the flight training sequence. 
 USS Enterprise was flown to NAS North Island with a single example appearing prominently in the penultimate "pressure suit" scenes both on the ground and in the air. All the aerial closeup work with the cast was photographed later in the studio using a series of realistic mock-ups. Some of the aircraft types used in Dive Bomber were engaged a few months later in combat with the Japanese aerial and naval forces, up to and including the Battle of Midway, while other types were declared obsolescent and relegated to home use when the U.S. geared up for war in earnest. Enterprise is the films aircraft carrier, and went on to be one of the most famous ships of World War II.   
 SB2C of Coral Sea, Battle of Midway|Midway, and other World War II engagements. A rare pair of shots are the takeoff from and landing on the Enterprise filmed from an aircraft.

==Reception== SBD Dauntless dive bomber to be displayed in conjunction with film screenings at principal cities, and set up recruiting booths by the theaters.  The film was one of Warners biggest hits of 1941, generating a profit in excess of $1 million.  It was listed as the sixth most popular film of 1941. 

Critically reviewed, Dive Bomber was praised for its colorful subject matter, but the plot as conceived by the screenwriting team of Frank Wead and Robert Buckner was considered "fanciful" and a "necessary evil" by Bosley Crowther of The New York Times. 

==In popular culture== Dizzy Heights.

==References==
Notes
 

Bibliography
 
* Dive Bomber. Los Angeles: Warner Home Video, 2007.
* Dive Bomber: Keep Em in the Air. Atlanta: Turner Entertainment Company, 2005.
* Dolan, Edward F. Jr. Hollywood Goes to War. London: Bison Books, 1985. ISBN 0-86124-229-7.
* Farmer, James H. "Hollywood Goes to North Island NAS." Air Classics, Volume 25, No. 9, September 1989.
* Hardwick, Jack and Ed Schnepf. "A Viewers Guide to Aviation Movies". The Making of the Great Aviation Films, General Aviation Series, Volume 2, 1989.
* Kinnard, Roy and R.J. Vitone. The American Films of Michael Curtiz. Metuchen, New Jersey: Scarecrow Press, 1986. ISBN 0-8108-1883-3.
* Orriss, Bruce. When Hollywood Ruled the Skies: The Aviation Film Classics of World War II. Hawthorne, California: Aero Associates Inc., 1984. ISBN 0-9613088-0-X.
* Robertson, James C. The Casablanca Man: The Cinema of Michael Curtiz. London: Routledge, 1994. ISBN 978-0-415-11577-3.
 

==External links==
*  
*  

 

 

 
 
 
 
 