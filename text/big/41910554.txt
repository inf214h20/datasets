Vaniusha and The Giant
 
{{Infobox film
| name           = Vaniusha and The Giant Ванюша и космический пират
| image          = 
| image_size     = 
| caption        = 
| director       = Vladimir Danilevich
| producer       =  Roman Kachanov,Jr (credit as R. Gubin)
| narrator       = Vsevolod Larionov
| starring       = Aleftina Yevdokimova,  Svetlana Kharlap,  Boris Novikov,  Ludmila Gnilova, Rogvold Sukhoverko,  Vladimir Ferapontov
| music          = Yevgeny Botiarov
| cinematography = Vladimir Sidorov
| editing        = Galina Filatova 
| distributor    = 
| released       =  
| runtime        = 10 min
| country        = Russia
| language       = Russian
| budget         = 
| gross          = 
}} Soyuzmultfilm studio.  The film is about The Friendly Newcomer from another planet.  The film is The Fourth Film of the tetralogy, which tells about the adventures of The Newcomer Vaniusha and his friends. Other three films called "The Newcomer in The Cabbage", "Vaniusha The Newcomer" and "Vaniusha and The Space Pirate".

== Plot summary ==
The Friends go to the distant past, many-many centuries ago. There They help The Good-Natured Giant. The Evil Knights and The Dragons hurt The Good-Natured Giant. Vaniusha with His Friends help The Giant and take Him in Their Village, in The Our Century.

In The Plot of This Film are mixed The Elements of The Folk Tales and The Science Fiction Stories.

== Creators ==
{| class="wikitable"
|-
! !! English !! Russian
|-
| Director 
| Vladimir Danilevich 
| Владимир Данилевич
|-
| Writer  Roman Kachanov,Jr (credit as R. Gubin)
| Роман Качанов-младш. (псевдоним Р. Губин)
|-
| Art Director 
| Ekaterina Mikhailova
| Екатерина Михайлова
|-
| Animators  
| Tatiana Molodova,  Sergei Kositsyn
| Татьяна Молодова,  Сергей Косицын
|-
| Puppets and decor 
| Anna Vetukova,  Alexander Maximov,  Nadezhda Lyarskaya,  Sergei Popov,  Natalia Barkovskaya,  Valery Petrov,  Victor Grishin,  Vladimir Alisov,  Youry Aksenov,  Nina Moleva
| Анна Ветюкова,  Александр Максимов,  Надежда Лярская,  Сергей Попов,  Наталья Берковская,  Валерий Петров,  Виктор Гришин,  Владимир Алисов,  Юрий Аксенов,  Нина Молева   
|-
| Camera 
| Vladimir Sidorov
| Владимир Сидоров
|-
| Music 
| Yevgeny Botiarov
| Евгений Ботяров
|-
| Sound
| Boris Filchikov
| Борис Фильчиков
|-
| Executive Producer 
| Alina Vlasova
| Алина Власова
|-
| Voice Actors 
| Aleftina Yevdokimova,  Svetlana Kharlap,  Boris Novikov,  Vsevolod Larionov,  Ludmila Gnilova, Rogvold Sukhoverko,  Vladimir Ferapontov
| Алефтина Евдокимова,  Светлана Харлап,  Борис Новиков,   Всеволод Ларионов,  Людмила Гнилова,  Рогволд Суховерко,  Владимир Ферапонтов
|-
| Artist
| T. Bogdanova
| Т. Богданова
|-
| Editor
| G. Filatova
| Г. Филатова
|-
| Script Editor
| Natalia Abramova
| Наталья Абрамова 
|}

==See also==
*"The Newcomer in The Cabbage"
*"Vaniusha The Newcomer" 
*"Vaniusha and The Space Pirate"

==External links==
*  at Animator.ru
*  at Kinopoisk.ru

 
 
 
 


 
 