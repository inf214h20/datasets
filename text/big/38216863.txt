Do Be Quick
{{Infobox film
| name           = Do Be Quick
| image          = 
| caption        = 
| director       = Stanislav Strnad
| producer       = 
| writer         = Arno Kraus
| starring       = Zdenek Hradilák
| music          = 
| cinematography = Jan Novák
| editing        = 
| distributor    = 
| released       =  
| runtime        = 77 minutes
| country        = Czechoslovakia
| language       = Czech
| budget         = 
}}

Do Be Quick ( ) is a 1977 Czech drama film directed by Stanislav Strnad. It was entered into the 10th Moscow International Film Festival.   

==Cast==
* Zdenek Hradilák as Frantisek Kabát
* Marie Drahokoupilová as Tereza Kabátová
* Ivan Lutansky as Ivan Kabát
* Martin Ruzek as Emil Martinec
* Karolina Slunécková as Eva Martincová
* Radoslav Brzobohatý as Jirí Voník
* Milena Dvorská
* Adolf Filip as nadporucík VB
* Grazyna Sahatqiu as Alena Martincová
* Jana Gýrová as Ruzena Voníková
* Milos Willig as Václav Houdek

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 