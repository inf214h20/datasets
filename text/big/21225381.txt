Green Magic
 
{{Infobox film
| name           = Green Magic
| image          = 
| caption        = 
| director       = Gian Gaspare Napolitano
| producer       = Mário Aldrá Leonardo Bonzi
| writer         = Mário Aldrá Gian Gaspare Napolitano Alfredo Palácios
| narrator       = Carlos Montalbán Bret Morrison
| starring       = 
| music          = 
| cinematography = Mario Craveri Giovanni Raffaldi
| editing        = José Cañizares Mario Serandrei
| distributor    = 
| released       =  
| runtime        = 85 minutes
| country        = Italy
| language       = Italian
| budget         = 
}}

Green Magic ( ) is a 1953 Italian documentary film directed by Gian Gaspare Napolitano.

==Cast==
* Carlos Montalbán as Narrator, US version (voice)
* Bret Morrison as Narrator, US version (voice)
* Leonardo Bonzi as Himself (Expedition Member)
* Gian Gaspare Napolitano as Himself (Expedition Member)
* Mario Craveri as Himself (Expedition Member)
* Giovanni Raffaldi as Himself (Expedition Member)
* Jose Docarmo as Himself (Pilot)

==Awards==
;Won
*     

;Nominated
*     

==References==
 

==External links==
* 

 
 
 
 
 
 


 
 