Vampire Blvd.
{{Infobox film
| name           = Vampire Blvd.
| image          = Vampire_Blvd.jpg
| caption        = Vampire Blvd. Video Box Cover
| director       = Scott Shaw
| producer       = Scott Shaw
| writer         = Scott Shaw
| starring       = Scott Shaw  Kevin Thompson   Joe Estevez   Adrienne Lau
| music          = 
| cinematography = Donald G. Jackson   Scott Shaw   Hae Won Shin Jake Blade
| distributor    = 
| released       =  
| runtime        = 90 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} Jill Kelly can also be seen in this film performing a small role.

This film is set in 1970s Hollywood, California. This film follows two primary characters, Elijah Starr and Blaze Jones, played by Shaw and Thompson. These two characters are Hollywood Private investigators who attempt to help a newly arrived actress from Hong Kong who is being tracked down by a cult of Vampires. This character is played by Hong Kong pop singer Adrienne Lau.

Similar to other Scott Shaw films, this movie follows an unexpected and non-linear storyline, with obvious references to Blacksploitation Cinema, and is full of Music video style references and edits where the central characters leave behind the storyline and interact solely by the presentation of visual images in association with Techno music.

==Hollywood==
Similar to many Scott Shaw films Vampire Blvd. utilities Hollywood, California as a cinematic backdrop. Hollywood landmarks such as the Hollywood sign and images of Hollywood Blvd. are commonly seen in his films. This is also the case with Vampire Blvd. where various character interactions take place in Hollywood.

Bronson Cave, which is located in the Hollywood Hills is also used as a location in this film.  This cave was used in many of the film and television Westerns that were filmed in the 1940s and 1950s and was commonly used as a location for the television series Power Rangers.  This cave is perhaps most well known as being the location of the Bat Cave from the 1960s televisions series Batman.

==Zen Film==
This film is considered a Zen Film in that it was created in the distinct style of filmmaking formulated by Scott Shaw known as Zen Filmmaking.  In this style of filmmaking no scripts are used.

==See also==
*Vampire film

==External links==
*  
* 
*  

 
 


 