The Virgin Suicides (film)
{{Infobox film
| name           = The Virgin Suicides
| image          = VirginSuicidesPoster.jpg
| caption        = Theatrical release poster
| director       = Sofia Coppola
| producer       = Francis Ford Coppola Julie Costanzo Dan Halsted Chris Hanley
| writer         = Sofia Coppola
| based on       =  
| narrator       = Giovanni Ribisi
| starring       = James Woods Kathleen Turner Kirsten Dunst Josh Hartnett A. J. Cook
| music          = Jean-Benoît Dunckel Nicolas Godin
| cinematography = Edward Lachman James Lyons
| studio         = American Zoetrope
| distributor    = Paramount Classics (US) Pathé (UK)
| released       =  
| runtime        = 97 minutes
| country        = United States
| language       = English
| budget         = $6.1 million
| gross          = $10,409,377 
}}
 drama written and directed by Sofia Coppola,    produced by her father Francis Ford Coppola,  starring James Woods, Kathleen Turner, Kirsten Dunst, Josh Hartnett, and A. J. Cook. 
 novel of the same name by Jeffrey Eugenides, the film tells of the events surrounding the lives of five sisters in an upper-middle class suburb of Detroit during the 1970s. After the youngest sister makes an initial attempt at suicide, the sisters are put under close scrutiny by their parents, eventually being put into near-confinement, which leads to increasingly depressive and isolated behaviour.

==Plot==
The story takes place in   that fill the boys conversations and dreams.

The film begins with the suicide attempt of the youngest sister, Cecilia, as she slits her wrist in a bath. After her parents throw a chaperoned party intended to make her feel better, Cecilia excuses herself and jumps out her bedroom window, dying when she impales herself on an iron fence. In the wake of her act, the Lisbon parents begin to watch over their daughters even more closely, further isolating the family from the community and heightening the air of mystery about them.

At the beginning of the new school year in the fall, Lux forms a secret relationship with Trip Fontaine (Josh Hartnett), the school heartthrob. Trip comes over one night to the Lisbon residence to watch television and persuades Mr. Lisbon to allow him to take Lux to the Homecoming Dance by promising to provide dates for the other sisters, to go as a group. After being crowned Homecoming Queen and King, Lux and Trip have sex on the football field. Lux falls asleep, and Trip abandons her immediately. Lux wakes up alone and has to take a taxi home.

Having broken curfew, Lux and her sisters are punished by a furious Mrs. Lisbon by being taken out of school and sequestered in their house. Unable to leave the house, the sisters contact the boys across the street by using light signals and sharing songs over the phone as a means of sharing their feelings.

During this time, Lux begins to have anonymous sexual encounters on the roof of the house late at night; the boys watch from across the street. Finally, after months of confinement, the sisters leave a note for the boys, presumably asking for help to escape. When the boys arrive that night, they find Lux alone in the living room, smoking a cigarette. She invites them to wait for her sisters, while she goes to wait in the car. The boys briefly imagine the group of them driving blissfully away on a sun-soaked country road.
 sticking her left the car engine running in the sealed garage. But no one knew why they did it.

Devastated by the suicides of all their children, Mr. and Mrs. Lisbon quietly flee the neighborhood, never to return. Mr. Lisbon had a friend sell off the family belongings, especially those belonging to the girls, in a yard sale; whatever didnt sell was put in the trash, including the family photos, which the neighborhood boys kept as mementos of the mysterious girls. Soon after, a young couple from Boston purchases the Lisbons house. Seemingly unsure how to react, the adults in the community go about their lives as if nothing ever happened or that the Lisbons ever even lived there. The boys never forgot about the Lisbon sisters, however much they tried, though everyone else did. The dead girls forever remain a source of mystery and grief for the boys, who cannot forget them. No one knew what drove the sisters to suicide and the film ends with one of the boys acknowledging in voice over that they will spend the rest of their lives trying to put together the unsolvable mystery of the Lisbon sisters.

==Cast==
  
* James Woods as Ronald Lisbon
* Kathleen Turner as Mrs. Lisbon
* Kirsten Dunst as Lux Lisbon
* Josh Hartnett as Trip Fontaine
* Michael Paré as Adult Trip Fontaine
* A. J. Cook as Mary Lisbon
* Hanna R. Hall as Cecilia Lisbon
* Leslie Hayman as Therese Lisbon
* Chelse Swain as Bonnie (Bonaventure) Lisbon
* Jonathan Tucker as Tim Winer
  Noah Shebib as Parkie Denton
* Anthony DeSimone as Chase Buell
* Lee Kagan as David Barker
* Robert Schwartzman as Paul Baldino
* Scott Glenn as Father Moody
* Danny DeVito as Dr. E. M. Horniker
* Hayden Christensen as Jake Hill Conley
* Kristin Fairlie as Amy Schraff
* Giovanni Ribisi as the Narrator
 

==Reception==
The film was generally well received by critics; it has a 76%  . Retrieved 2011-03-11.  The New York Post heaped praise on the film; "Its hard to remember a film that mixes disparate, delicate ingredients with the subtlety and virtuosity of Sofia Coppolas brilliant The Virgin Suicides."  The Philadelphia Inquirer outlined its attributes: "Theres a melancholy sweetness here, a gentle humor that speaks to the angst and awkwardness of girls turning into women, and the awe of boys watching the transformation from afar." 

== Score ==
 

== Soundtrack == Steely Dan, Head East, Air (one previously recorded; one composed for the film).

Mentioned in the credits (chronologically): Sloan (album Navy Blues, 1998)
* "Cant Face Up" (credited "How many times") by Sloan (One Chord to Another, 1996)
* "The Air That I Breathe" by The Hollies (Hollies (1974 album)|Hollies, 1974) Steely Dan (Cant Buy a Thrill, 1972) Heart (Dreamboat Annie, 1976) Heart (Dreamboat Annie, 1976) Face the Music, 1975) Head East Flat as a Pancake, 1975)
* "Alone Again (Naturally)" by Gilbert OSullivan (Gilbert OSullivan discography#Himself (Aug 1971)|Himself, 1971) So Far Away" by Carole King (Tapestry (Carole King album)|Tapestry, 1971)
* "The Lines You Amend" (credited "End It Peacefully") by Sloan (One Chord to Another, 1996)
* "A Dream Goes on Forever" by Todd Rundgren (Todd (album)|Todd, 1974) Air (Moon Safari, 1998)
* "How Can You Mend a Broken Heart?" by Al Green (Lets Stay Together (album)|Lets Stay Together, 1972)
* "Everything Youve Done Wrong" by Sloan (One Chord to Another, 1996)
* "The Good in Everyone" by Sloan (One Chord to Another, 1996)
* "Im Not in Love" by 10cc (The Original Soundtrack, 1975)
* "Hello Its Me" by Todd Rundgren (Something/Anything?, 1972) Run To To Whom It May Concern, 1972)

==Cultural references==
* A passage from the narration and a ticking clock sound are sampled at the end of the song "Doors Closing Slowly" from the album Journal for Plague Lovers by Manic Street Preachers. The quoted passage is "In the end we had pieces of the puzzle but no matter how we put them together gaps remained; oddly shaped emptiness mapped by what surrounded them, like countries we couldnt name".

==References==
 

==External links==
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 