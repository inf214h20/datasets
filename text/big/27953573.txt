The Shrine (film)
{{multiple issues|
 
 
}}
{{Infobox film
| name           = The Shrine
| image          = The Shrine.jpg
| caption        = 
| director       = Jon Knautz
| producer       = J. Michael Dawson 
| screenplay     =  
| story          =  
| starring       =  
| music          = Ryan Shore
| cinematography = James Griffith
| editing        = Matthew Brulotte
| studio         = Brookstreet Pictures
| distributor    = Brookstreet Pictures
| released       =   
| runtime        = 93 minutes
| country        = Canada
| language       = English
| budget         = $1,500,000 CAD (estimated)
| gross          = 
}}
The Shrine is a Canadian horror film produced by Brookstreet Pictures. The film was directed by Jon Knautz  and stars Aaron Ashmore, Cindy Sampson, Meghan Heffern, and Trevor Matthews. The screenplay was written by Jon Knautz, Brendan Moore, and Trevor Matthews.  Ryan Shore received a 2012 Grammy Award nomination for Best Score Soundtrack for his score.

==Plot==
The film starts with a man tied to a ceremonial table. Another man then kills him with a sledgehammer to the face.

A journalist, Carmen (Cindy Sampson), is having problems with her relationship with her boyfriend, Marcus (Aaron Ashmore) who is a photographer. Carmen never seems to stop thinking about work and tends to ignore him.
 Polish village of Alvania (the letter v is not part of the modern Polish alphabet). The last person to go missing was Eric Taylor (Ben Lewis), the man killed at the beginning of the film. She also mentions that the luggage of all the missing tourists ends up being found all around rural parts of central Europe. Her boss isnt interested in another missing persons case, saying that people go missing all the time and does not give her permission to go. Instead, he would rather have her investigate why a farmers bees go missing.

Carmen and her intern Sara (Meghan Heffern) visit Erics mother, Laura. Laura tells them that she has tried to get the help of both the Polish and the American Police. However, they are unwilling or too busy to help her. Carmen asks Laura if Erics luggage has been returned to her. Laura says that it has and Carmen borrows Erics journal.

Later in her home while reading Erics journal, she has a dream of Eric with bloody eyes who tells her "leave me alone". Carmen, wanting to mend her relationship with Marcus as well as her job, urges him to come with her and Sara to Alvania. Upon reaching Alvania, they find the village people to be secretive, unfriendly and unwelcoming, just as described in the last entry in Erics journal. Although English is taught in the villages school, not all the villagers know how (or are willing) to speak it.

They see a man named Henryk butchering a pig then find a girl named Lidia picking flowers and approach her. They show her a picture of Eric and ask if she has seen him. The girl reacts at the sight of Eric in the picture but is hesitant to answer. Before she even can, Henryk calls to her and sends her off. Carmen, Sara and Marcus explore the village. They notice that the villagers treat their religious leaders like royalty. They then spot a strange dense area of fog which seems to be concentrated only in one portion of the forest, also mentioned in Erics notes. They are threatened by Henryk and the villagers and told to leave the village when they attempt to move toward the fog.

Leaving the village initially, Carmen tells Marcus that they should go back and investigate the fog. Marcus initially disagrees and insists that they leave. Carmen manages to convince Marcus to return by admitting her editor knows nothing about the trip and that her career will be over if she returns with no story. They leave their car and walk into the forest where they eventually find the area with the fog. Carmen, Sara and Marcus find it strange that the fog seems to never move and is quite dense, therefore preventing Marcus from taking pictures of anything inside the fog.

Sara enters the fog and disappears into it. Carmen and Marcus wait for Sara to return but after a while, they lose their patience. Marcus insists that they leave but they cant leave Sara behind. Carmen enters the fog to find Sara. Moments after she enters the fog, Sara somehow gets out and is found by Marcus. Sara appears scared and unfocused. Carmen seems to have gotten lost in the fog and comes across a statue of a demon holding a heart. Carmen takes a picture of it. She moves to the side of the statue in order to take another picture. Looking up from the camera she sees that the statues head has turned its head to face Carmen. The statues eyes bleed and the heart in its hand starts to beat. This frightens Carmen. As she backs away from the statue, she finds herself out of the fog and is found by Marcus and Sara. Sara and Carmen talk about seeing the statue, both a bit disoriented, also having heard strange whispering voices.

As they make their way back to their car, they come across Lidia who claims to know the whereabouts of Eric and takes them to a hidden sacrificial shrine, where they discover the bodies of people (including Eric) that the village people have executed. Each body has a metal mask deeply embedded in its face, making it look as though the villagers are cultists and are practicing some kind of ritual black magic on the tourists who come to Alvania. Marcus, Sara and Carmen notice that Lidia is gone and find the door to the shrine barred.

Luckily, Marcus is able to open it but they are suddenly surrounded by the villagers and flee, hiding in a nearby barn. They are found and struggle to escape but one of the villagers knocks Carmen out. Henryk shoots Saras leg with a crossbow. The villagers knock her out with chloroform but not before she sees one of the villagers face turn into a demon. Marcus runs into the forest and is chased by Henryk. Henryk catches up with him. Marcus fights Henryk but the latter gains the upper hand and knocks him out with chloroform.

As they wake up, they see Henryk talking to Lidia. Lidia walks off and the villagers bring them back to the entrance of the sacrificial shrine where the head of the villages church, Arkadiusz, decides to sacrifice both Sara and Carmen. Sara and Carmen are taken inside the shrine while Marcus is led away by two villagers and is forced to dig a grave at gunpoint. After one of them leaves, Marcus uses the shovel to disarm the remaining villager with the gun and knock him unconscious. He takes the gun and runs off to rescue Sara and Carmen.

At the shrine, cultists strip Carmen and Sara and dress them with ceremonial gowns. The cultists place Sara on the same ceremonial table where Eric was killed while Carmen is placed in a prison cell. The cultists secure her arms, legs and head. They then deeply lacerate her arms and sever her Achilles tendons. While this is being done to her, she sees the faces of the cultists and Arkadiusz turn into demons. The same mask earlier found on the corpses is placed over Saras head. It has two spikes inside of it which are meant to pierce Saras eyes and embed the mask on her face. Arkadiusz takes a sledgehammer and uses it to embed the mask on Sara, executing her in the process.

One of the cultists looks at Carmen and she sees that the cultists face has become that of a demon. They bring her out to be sacrificed but Marcus rescues her. He locks the cultists in the shrine and he and Carmen make their escape.

They enter a villagers house to steal the keys to a truck. Marcus sees a woman, Emilia, and her son, Dariusz, in the kitchen. The woman calls for her husband, Aleks, but is unable to do anything since Marcus still has the gun. Marcus asks them for the keys to the truck but they cant understand English. When they see Carmen, Emilia expresses fear and panic. Marcus takes the family to their living room and ties up the couple.

While Marcus tries to get them to give him the keys, Carmen starts to experience voices. She sees the objects in the room shake. She sees Dariusz turn into a demon. She sees Emilia and Aleks turn demonic, untie themselves and crawl on the floor. Marcus turns to Carmen and he too has become a demon. But in truth all of them are still human, Emilia and Aleks are still tied up and none of them are even looking at her.

Carmen hides in another room where she continues to see and hear things. Dariusz, who understands and speaks English, leads Marcus back to the kitchen and gives him the truck keys. Not wanting to risk Dariusz going out and calling for help, Marcus also ties him up. The boy begs for Marcus to let them go and warns him that Carmen is evil since she has seen the statue. Carmen is then possessed by a demon while she is alone in the room and lets out a very loud shriek. The cultists, who have gotten free and are looking for Carmen, hear the shriek and are alerted of Carmens location.

Marcus hears Carmen calling out to him. He enters the room she went inside through a door from the kitchen. As he enters the room, another door leading back to the living room closes. Marcus hears Aleks and Emilia scream in fear and pain, followed by footsteps going to the kitchen. Marcus then hears Dariusz screaming. As he looks out from the door, he sees the boy being dragged away. The door then closes and Dariuszs screaming stops. The door leading to the living room slowly opens. He goes to the living room and finds the entire family brutally killed. A possessed Carmen then jumps on him and pins him on the floor. Just as she is about to kill Marcus, she is confronted by the cultists. She kills several of them. Arkadiusz fights back with prayers and holy water. He calls for the mask but the cultist holding the mask is stunned in fear, giving the possessed Carmen the opportunity to impale Arkadiusz. Carmen turns her attention back to Marcus.

Before Arkadiusz dies, he passes on his duties to Henryk. Henryk chants prayers while splashing Carmen with holy water. Henryk stabs Carmen. The prayers weaken her, allowing Henryk to slowly set her on the floor. Two more cultists help. They impale Carmens hands and pin her to the floor. Henryk calls for the mask and this time, the cultist holding the mask complies. They position the mask over Carmens face while Henryk readies the sledgehammer. However Carmen still struggles and the mask cant be properly placed over her eyes.

Marcus now realizes what is going on and understands why it was necessary to kill Sara and Carmen. Sara and Carmen saw the statue which is why the villagers had to kill them but not Marcus. The cultists werent sacrificing anyone. They were trying to prevent demonic possessions from taking place. Knowing this, Marcus assists Henryk and the others by holding Carmens head still. With the mask now in place, Henryk readies the sledgehammer to embed the mask. Carmen returns to normal and asks Marcus for help but Marcus knows it is just a trick. Henryk sends the sledgehammer down and embeds the mask on Carmen, killing her.

Marcus then looks at Henryk and the remaining men, wondering what will happen to him since he knows too much. One of the villagers asks Henryk what they will do with Marcus. In the end, Marcus is set free by the villagers. Henryk offers to have one of the villages men take him to where he needs to go. Marcus looks over to the forest where the fog is seen. Not knowing what is really inside the fog, he asks Henryk about it. He is told that the fog and the statue are a curse left long ago on the village that cannot be undone. The devilish statue is shown once more and the screen goes black.

==Cast==
* Aaron Ashmore as Marcus
* Cindy Sampson as Carmen
* Meghan Heffern as Sara
* Laura DeCarteret as Laura Taylor
* Ben Lewis as Eric Taylor
* Trevor Matthews as Henryk
* Connor Stanhope as Dariusz
* Monica Bugajski as Emilia
* Paulino Nunes	as Henchmen #5
* Jasmin Geljo as Purple Cloaked Man #2
* Danijel Mandic as Henchmen #7
* Neil Davison as Henchman #3
* Stefen Hayes as Aleksy
* Voytek Skrzetaa as Henchmen #4
* Ray Kahnert as Clerk
* David Tompa as Henchmen #1
* Julia Debowska as Lidia
* Wally Michaels as Purple Cloaked Man #1
* Alexander Krsticha as Henchmen #2
* Vieslav Krystyan as Arkadiusz

==Production== Toronto  and finished in September in Vaughan, Ontario.  The Brookstreet Pictures production budget was set at 1.5 million dollars.  Jon Knautz wrote the story with actor Trevor Matthews and Brendan Moore.  Matt Brulotte worked as editor on the film.  The Fantasia Festival screening was hosted by co-writer and director Jon Knautz and the lead actor Aaron Ashmore. 

==Soundtrack==
Ryan Shore scored the official soundtrack.  Ryan Shore received a 2012 Grammy Award nomination for Best Score Soundtrack for his score.

==Release== FanTasia on 25 July. 

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 
 
 