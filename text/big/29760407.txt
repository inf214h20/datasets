High Powered
{{Infobox film
| name           = High Powered
| image_size     =
| image	=	High Powered FilmPoster.jpeg
| caption        =
| director       = William Berke
| producer       = William H. Pine (producer) William C. Thomas (producer)
| writer         = Milton Raison (screenplay) Milton Raison (story) Maxwell Shane (screenplay)
| narrator       = Robert Lowery Phyllis Brooks Mary Treen Alexander Laszlo
| cinematography = Fred Jackman Jr.
| editing        = Henry Adams Howard A. Smith
| distributor    = Pine-Thomas Productions
| released       = February 23, 1945
| runtime        = 62 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}

High Powered is a 1945 American film directed by William Berke. It is also known as High Man.

==Plot Summary==

Marian Blair and Cassie McQuade are running a small lunch establishment in their trailer. When they move the trailer from Los Angeles to the building site outside San Francisco, they pick up a hitchhiker along the road. His name is Tim Scott, and he is on his way to a job picking grapes at the vineyards.

At one point, Tim takes the wheel, and accidentally drives into the curb, unhinging the trailer, sending it down the road and crashing into the sheriffs car. All three get arrested and sent to a local jail. Tim gets to share cells with a man named Spike Kenny. When Spikes boss Farrell comes to bail him out, he recognizes Tim from a high riser construction site and bails him out too.

Tim is offered a job a the new plant being built at the construction site, and Tim accepts the offer, but only to impress Marian, whom he has taken a liking to. Spike has taken an interest in Cassie.

Tim starts working as a chipper - a metal worker - and meets his work mate Bill Madden, a man who blames Tim for his brothers death. Bills brother fell down from construction in a high rise, and pulled Tim with him in the fall. Tim only survived because he landed on top of bills brother.

Marian and Cassie are quite successful with their lunch trailer, and both women are invited to the Boilermans Ball, by Rod and Spike respectively. Marian dares Tim to go up in one of the plant towers, fully aware he has trouble with heights, and Rod has to escort him down again. When Tim explains how he obtained his vertigo, Marian warms up to him.

One day there is a gas leak at the plant and Tim manages to save Jeffs life when the gas is ignited by a spark from his car. Tim still gets the blame for the leak, but Marian stands up for him and takes him out on a date. They take a liking to each other and when Tim asks her to the ball she accepts.

During the ball, Bill happens to overhear Marian when she explains she is only daring Tim out of pity. To get back at Tim, Bill tells him what he heard. Tim is furious and picks a fight with Bill, and the fighting spreads among the rest of the workers. Soon the sheriff arrives and arrests all of the people involved in the fighting.

This time Rod is reluctant to bail the other workers out of jail, but continues the work with a hand full of workers left. He changes his mind and decides to bail out Tim when he hears that Bill was the one responsible for the fight. However, both Tim and Spike has escaped jail already and are on their way to get work somewhere else.

There is an accident at the construction site, in which Rod is trapped under a heavy metal cap, by a cable that has snapped feom the weight. Tim sees Rod in trouble, overcomes his fear of heights, climbs up the tower and saves Rod. Rod decides to bail out everyone else from jail, Tim and Marian get married and go away on a honeymoon together. 

==Cast== Robert Lowery as Tim Scott
*Phyllis Brooks as Marian Blair
*Mary Treen as Cassie McQuade
*Joe Sawyer as Spike Kenny Roger Pryor as Rod Farrell
*Ralph Sanford as Sheriff Billy Nelson as Worker
*Edward Gargan as Boss
*Vince Barnett as Worker

==External links==
* 
* 

==References==
 

 
 
 
 
 
 
 
 