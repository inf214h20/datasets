Law of the Wolf
{{Infobox film
| name           = Law of the Wolf
| image_size     =
| image	=	Law of the Wolf FilmPoster.jpeg
| caption        =
| director       = Bernard B. Ray
| producer       = Bernard B. Ray (associate producer) Harry S. Webb (producer)
| writer         = Carl Krusada Carl Krusada (story)
| narrator       =
| starring       = See below
| music          =
| cinematography = Edward A. Kull
| editing        = Frederick Bain
| distributor    =
| released       = 1939
| runtime        = 55 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
 Rin Tin Tin III. The film is also known as Law of the Wild (American reissue title), not to be confused with The Law of the Wild a 1934 serial starring Rin Tin Tin Jr.

==Plot==

After being falsely accused of his brother Harrys murder Carl Pearson escapes from prison with the aid of another prisoner Duke Williams. Meanwhile wealthy aircraft manufacturer Roger Morgan makes plans to adopt Harrys son Bobby who is currently in the care of Ruth Adams, who is also Carls fiancee. With his father dead young Bobby is the legal owner of valuable aircraft plans that Morgan wants to acquire. Carls son is Johnny who is the owner of Rinty a prize tracking dog. The Police borrow Rinty in order to try to track Carl and Duke; but Rinty is less than cooperative in this effort. After a car accident leaves Ruth temporarily incapacitated, young Bobby wanders off. A canoe rescue and an encounter with a wild cougar follow. Arriving at the Pearsons cabin Carl recovers the plans, but Duke steals them in order to sell them. Eventually everyone else ends up at the cabin and the real killer is revealed. The killer tries to escape followed by Carl and Rinty in a climactic chase.

== Cast == Dennis Moore as Carl Pearson
*Luana Walters as Ruth Adams
*George Chesebro as Duke Williams Steve Clark as John Andrews Jack Ingram as Roger Morgan
*Robert Frazer as Lt. Franklin
*Jimmy Aubrey as Uncle Jim
*Martin Spellman as Johnny Robert Gordon as Bobby   Rin Tin Tin III as Rinty

== External links ==
* 
* 

 
 
 
 
 
 
 
 
 


 