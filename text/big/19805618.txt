Gift (1966 film)
{{Infobox film
| name           = Gift
| image	=	Gift FilmPoster.jpeg
| caption        = A poster bearing the films alternative U.S. title, Venom
| director       = Knud Leif Thomsen
| producer       = Bo Christensen Knud Leif Thomsen
| writer         = Knud Leif Thomsen
| starring       = Søren Strømberg
| music          = 
| cinematography = Arne Abrahamsen Claus Loof
| editing        = Birger Lind
| distributor    = ASA Film
| released       = 24 March 1966
| runtime        = 96 minutes
| country        = Denmark 
| language       = Danish
| budget         = 
}}

Gift is a 1966 Danish drama film directed by Knud Leif Thomsen and starring Søren Strømberg. In the United States this film is also known as Venom. 

==Cast==
* Søren Strømberg - Per
* Sisse Reingaard - Susanne
* Poul Reichhardt - Henrik Steen
* Astrid Villaume - Hjørdis Steen
* Judy Gringer - Sonja, the Maid
* Grethe Mogensen - Frau Jacobsen, the Secretary
* Karl Stegger - Caretaker
* Per Goldschmidt - Teenager
* Jess Kølpin - Teenager
* Vic Salomonsen - Teenager
* Tine Schmedes - Teenager

== References ==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 