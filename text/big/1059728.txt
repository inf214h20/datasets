Deathtrap (film)
 
{{Infobox film
| name           = Deathtrap
| image          = Deathtrap_imp.jpg
| caption        = Theatrical poster by Bill Gold
| director       = Sidney Lumet
| producer       = Burtt Harris
| writer         = Ira Levin (play)   Jay Presson Allen (screenplay)
| starring       = Michael Caine   Christopher Reeve   Dyan Cannon
| music          = Johnny Mandel
| cinematography = Andrzej Bartkowiak
| editing        = Jack Fitzstephens
| distributor    = Warner Bros.
| filmed = March 1981
| released       =  
| runtime        = 116 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = $19,282,134
}} thriller based play of the same name, directed by Sidney Lumet from a screenplay by Levin and Jay Presson Allen, starring Michael Caine and Christopher Reeve. 

==Plot==
  

Famed playwright Sidney Bruhl debuts the latest in a series of Broadway flops and returns to his opulent Long Island home and his wife, Myra. Although their financial situation is not dire, Sidney is hungry for a hit. They are starting to feel the limit of his wifes fortune, so he shares with her a plan. He has received a manuscript written by one of his students, Clifford Anderson, that he considers near perfection. Clifford recently attended one of Sidneys writing workshops and now asks for input on his play Deathtrap. Myras weak heart is not improved when her husband does not rule out eliminating Clifford and producing the play as his own. He invites Clifford to their secluded Long Island home to discuss it.

Clifford arrives by train. Myra tries desperately over the course of an evening to convince Sidney to work with Clifford as equal partners, but to no avail; Sidney attacks Clifford, strangling him with a chain. 

Sidney removes the body but still has to convince Myra to conspire with him. She reveals nothing when they receive an unexpected visit from the psychic Helga Ten Dorp, a minor celebrity who is staying with the Bruhls neighbors. But Helga senses pain and death in the house; before she leaves she warns Sidney about a man in boots who will attack him.

As she prepares for bed, Myra is managing to come to terms with Sidneys diabolical deed. All is calm until Clifford bursts through the bedroom window and beats Sidney with a log. Clifford chases Myra through the house until her heart gives out; she collapses and dies. Sidney calmly descends the stairs, uninjured, and sidles unperturbed to Cliffords side. They exchange a few words about what to do with Myras body, then exchange a passionate kiss.
 sociopath and he lies about the solvency of Myras estate as an excuse to assist Clifford while he contemplates a solution.

A few days later, Helga stops by, ostensibly for candles in anticipation of a predicted thunderstorm. Almost immediately after meeting Clifford, she warns Sidney that Clifford is the man in boots.

Sidney double crosses Clifford by asking him to arm himself with an axe to demonstrate a bit of stage business, then producing a gun to dispose of Clifford and Deathtrap. But Clifford has loaded the bullets into a different gun, and he secures Sidney to a chair with manacles, unaware they were once the property of Harry Houdini. Sidney easily releases himself, grabs a crossbow and knocks out Clifford with a single Quarrel (projectile)|shot.

Before Sidney can dispose of the body the storm hits with full force, plunging him into darkness. A flash of lightning illuminates the living room and a fleeting figure scurries through. It is Helga, aware of the mortal danger at the house.

Sidney finds a knife while Helga grabs a gun. Clifford regains consciousness and trips Helga. The gun goes flying and a struggle for it ensues that ... 

... culminates on a stage with actors before a full house, where "Clifford" stabs "Sidney" and both die, leaving "Helga" victorious. The opening night audience erupts in thunderous applause, and at the back of the house stands Helga Ten Dorp, now the happy author of a hit Broadway play called Deathtrap.

== Cast ==
* Michael Caine as Sidney Bruhl
* Christopher Reeve as Clifford Anderson
* Dyan Cannon as Myra Bruhl
* Irene Worth as Helga Ten Dorp Henry Jones as Porter Milgrim
 Jeffrey Lyons and Joel Siegel have cameo appearances as themselves.

== Reception ==
Deathtrap has an aggregate rating of 75% at Rotten Tomatoes.  Critic Roger Ebert gave it three stars, calling it "a comic study of ancient and honorable human defects, including greed, envy, lust, pride, avarice, sloth, and falsehood." 
 Golden Raspberry Award for "Worst Supporting Actress" for her performance.  Mad Magazine parodied the film as Deathcrap. 
 Tom Smith song " ".

==Home video release== Night Shift. Warner Home Video released Deathtrap on Blu-ray Disc on November 20, 2012, as part of the Warner Archive Collection. 

==See also==
* List of American films of 1982

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 