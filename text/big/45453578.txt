Secret Defense (2008 film)
{{Infobox film
| name     = Secrets of State
| image    =
| director = Philippe Haïm
| producer = 
| writer   = 
| starring = Gérard Lanvin   Vahina Giocante
| cinematography = 
| music    = 
| country  = France
| language = French
| runtime  = 100 min
| released =  
}}
Secrets of State is a 2008 French thriller film directed by Philippe Haïm.

== Cast ==
* Gérard Lanvin - Alex
* Vahina Giocante - Diane / Lisa
* Nicolas Duvauchelle - Pierre
* Mehdi Nebbou - Ahmed
* Rachida Brakni - Leïla / Chadia
* Simon Abkarian - Al Barad
* Aurélien Wiik - Jérémy
* Katia Lewkowicz - Aline
* Kamel Belghazi - Aziz

== External links ==
* 

 
 

 