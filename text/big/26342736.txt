Once Fallen
{{Infobox film
| name = Once Fallen
| image = Once Fallen.jpg
| caption = 
| director = Ash Adams
| producer =  
| writer = Ash Adams
| starring =  
| music = Jeff Beal
| cinematography = Tarin Anderson
| editing =  
| studio = Emmett/Furla/Oasis Films|Emmett/Furla Films
| distributor =  
| released =  
| runtime = 93 minutes
| country = United States
| language = English
| budget = 
| gross =  
 |publisher= 
 |accessdate= 
}}
Once Fallen is a 2010 crime film starring Brian Presley, Taraji P. Henson, and Ed Harris. Filming took place in Los Angeles, California.

==Plot==
Chance (Brian Presley) returns home from a half decade in jail determined to change his life. He tries to make peace with his father (Ed Harris), the head of the Aryan Brotherhood at the prison where he is serving a life sentence for murder. Chances release is quickly marred when he has to help his best friend with a debt to a local mobster.
When Chance gets home to his ex-girlfriends house, he finds out that he has a son named August. Then he meets Pearl, a friend of his ex-girlfriend. From then on, Chance sets himself on the path of changing his life around.

==Cast==
* Brian Presley as Chance
* Taraji P. Henson as Pearl
* Ed Harris as Liam
* Chad Lindberg as Beat
* Amy Madigan as Rose
* Peter Weller as Eddie
* Ash Adams as Rath

==Music==

The soundtrack to Once Fallen was released on November 9, 2010.

{{Track listing
| collapsed       = no
| extra_column    = Artist
| total_length    = 43:20

| title1          = Once Fallen
| length1         = 2:24
| extra1          = Jeff Beal

| title2          = Prison Fight
| length2         = 1:43
| extra2          = Jeff Beal

| title3          = Lets Leave Town
| length3         = 1:45
| extra3          = Jeff Beal

| title4          = Water
| length4         = 2:03
| extra4          = Jeff Beal

| title5          = There Are No Rules
| length5         = 1:22
| extra5          = Jeff Beal

| title6          = Liam Explodes
| length6         = 1:50
| extra6          = Jeff Beal

| title7          = Rath Gets the Coke
| length7         = 2:44
| extra7          = Jeff Beal

| title8          = Deal
| length8         = 1:06
| extra8          = Jeff Beal

| title9          = Pearl / Meeting with Liam
| length9         = 2:33
| extra9          = Jeff Beal

| title10         = Body in the Car
| length10        = 1:08
| extra10         = Jeff Beal

| title11         = Beat and Liam
| length11        = 1:36
| extra11         = Jeff Beal

| title12         = The Kidnap
| length12        = 2:20
| extra12         = Jeff Beal

| title13         = If I Go, You Go
| length13        = 4:17
| extra13         = Jeff Beal

| title14         = The Kiss
| length14        = 2:01
| extra14         = Jeff Beal

| title15         = Funny How It Goes
| length15        = 0:57
| extra15         = Jeff Beal

| title16         = Beat Dies
| length16        = 2:06
| extra16         = Jeff Beal

| title17         = Father and Son
| length17        = 1:48
| extra17         = Jeff Beal

| title18         = Reunion
| length18        = 0:55
| extra18         = Jeff Beal

| title19         = To Chance
| length19        = 2:56
| extra19         = Jeff Beal

| title20         = A Gift from Beat / End Credits
| length20        = 5:46
| extra20         = Jeff Beal

}}

===Musicians===
** "Eddies" Jazz Band
**** Peter Weller - Trumpet
**** Rick DePiro -  Piano
**** Ryan Cross -  Bass
**** Peter Harris -  Guitar

==External links==
*  

 
 
 
 
 
 
 
 


 