Patalghar
{{Infobox film
| name           = Patalghar
| image          = PATALGHAR.jpg
| caption        = DVD cover of Patalghar
| director       = Abhijit Chaudhuri
| producer       = Niti Sonee Gourisaria
| writer         = Shirshendu Mukhopadhyay
| starring       = Soumitra Chatterjee Mita Vasisht
| music          = Debojyoti Mishra
| cinematography = Abhik Mukhopadhyay
| editing        = Arjun Gourisaria 
| distributor    = 
| released       =  
| runtime        = 141 minutes
| country        = India
| language       = Bengali
| budget         = 
}} Bengali science fiction film based on the story of same name by Shirshendu Mukhopadhyay and directed by Abhijit Chaudhuri. 
{{Cite news
|url=http://articles.timesofindia.indiatimes.com/2003-02-19/kolkata/27268865_1_bengali-films-cinema-halls-satyajit-ray
|title=Rays presence dominates Patalghar-Kolkata -Cities-The Times of India
|publisher=indiatimes.com
|accessdate=2008-12-17
|last=
|first=
|date=19 February 2003
}}
  
{{Cite web
|url=http://ftvdb.bfi.org.uk/sift/title/720747?view=release
|title=PATALGHAR (2002)
|publisher=ftvdb.bfi.org.uk
|accessdate=2008-12-17
|last=
|first=
}}
 

==Plot==
150 years in the past, Aghor Sen, a genius scientist living in the village of Nischintipur  invents a device that generates sound waves which can put any living being to sleep. The alien Vik exiled from the planet Nyapcha, lands with his space shuttle on the outskirts of Nischintipur. He gets to know about the machine & tries to steal it but Aghor puts him to sleep. With Aghors death the instrument is lost. 

150 years later i.e. in the present day Dr. Bhootnath Nondy finds Aghor Sens diary mentioning this device. Bhootnath deciphers the diary at a science seminar announcing his plea to search for it. Begum, a gang leader, sends her goons after Bhootnath to get that machine. Aghar Sens device is in his laboratory - Patalghar. To reach there, one must solve the mysterious rhymes mentioned in the diary. Suddenly foreigners wanting to buy houses assail Nischintipur. At this juncture, Begum and Bhootnath work together in search of the machine. 

Kartik, a very intelligent & brave boy, lives with his uncle (Mama) Subuddhi. A lawyer reports to them that Kartik is the only legal heir of Aghor Sens laboratory at Nischintipur. Thus the duo reached there. Begum asks them to sell their house but they bluntly refuse. Bhootnath develops a friendship with Karthik. 

Meanwhile Vik wakes up and starts running from pillar to post looking for the device which had put him to sleep. Subuddhi joins the theatre chief of the village and searches for a unique character who could depict Mahommadi Beg in the play. He meets Vik in the forest and selects him for the character. 
          
Finally Bhootnath & Karthik unravel the path for the entrance of Patalghar and rediscover the machine. The Begum & her goons die in a duel with the alien Vik and finally Vik is once again put to sleep by the joint efforts of Bhootnath & Karthik by using the musical machine. Bhootnath flies of to Nyapcha with the unconscious Vik & Karthik remains on the earth inheriting the glorious legacy of Aghor Sen & Bhootnath Nandi. The great machine ultimately gets destroyed amidst all the chaos and havoc.

==Cast==
* Sourav Bandopadhay     ...     Kartik
* Biplab Chatterjee    ...     Vik
* Soumitra Chatterjee    ...     Aghor Sen
* Ketaki Dutta    ...     Pishima (Aghor Sens Aunt)
* Monu Mukherjee    ...     Gobinda Biswas
* Sunil Mukherjee    ...     Lawyer
* Kharaj Mukhopadhyay    ...     Subuddhi
* Joy Sengupta    ...     Dr. Bhootnath Nandy
* Mita Vasisht    ...     Begum

==Crew==
* Director Abhijit Chaudhuri
* Cameraman Abhik Mukhopadhyay
* Producer Niti Sonee Gourisaria
* Music Director Debojyoti Mishra
* Lyrics Rangan Chakraborty
* Editor Arjun Gourisaria
* Art Director Indranil Ghosh
* Executive Producer Bauddhayan Mukherji

==Awards== National Film Award
** Indira Gandhi Award for Best First Film of a Director-Abhijit Chaudhuri Best Cinematography-Abhik Mukhopadhyay

==See also==
* Science fiction film of India

==References==
 

==External links==
* 

 
 

 
 
 
 
 
 
 