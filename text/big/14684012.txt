Space Ship Sappy
{{Infobox Film |
  | name           = Space Ship Sappy
  | image          = Merrymix.JPEG
  | caption        = 
  | director       = Jules White Jack White
  | starring       = Moe Howard Larry Fine Joe Besser Benny Rubin Doreen Woodbury Emil Sitka Lorraine Crawford Harriette Tarler Marilyn Hanold
  | cinematography = Henry Freulich 
  | editing        = Saul A. Goodkind 
  | producer       = Jules White
  | distributor    = Columbia Pictures|
  | released       =  
  | runtime        = 16 15"
  | country        = United States
  | language       = English
}}

Space Ship Sappy is the 178th short subject starring American slapstick comedy team the Three Stooges. The trio made a total of 190 shorts for Columbia Pictures between 1934 and 1959.

==Plot==
The Stooges meet up with eccentric Professor A.K. Rimple (Benny Rubin) and his daughter (Doreen Woodbury) who ask the trio to help them with a space mission. The mission lands on the planet Sunev (Venus spelled backwards), where the Stooges are taken in by three attractive female aliens. At first, sparks fly (literally) when the girls kiss the boys. But then the ladies turn cannibalistic, and are about the suck the Stooges blood. However, the boys are able to escape, as a huge lizard appears on the horizon, causing the women to run away. The three jump back in the rocket ship, knocking the Professor and his daughter out cold, and fly back to Earth. They are then shown relating the story of their adventure to an assembled group. When they finish, the "Liars Club" presents them with the award for being the biggest liars in the world.
 )]]

==Production notes== heart attack shortly after filming began. Production of the Stooge shorts was put on hold for several weeks while Besser recovered.    

Space Ship Sappy features Moe and Larrys more "gentlemanly" haircuts, first suggested by Joe Besser. However, these had to be used sparingly, as most of the shorts with Besser were remakes of earlier films, and new footage had to be matched with old. 

== References ==
 

== External links ==
*  
*  

 

 
 
 
 
 
 
 
 
 