The Princess of the Ursines
{{Infobox film
| name =   The Princess of the Ursines 
| image =
| image_size =
| caption =
| director = Luis Lucia
| producer = Joaquín Cuquerella  Carlos Blanco   Luis Lucia 
| narrator =
| starring = Ana Mariscal
| music = José Ruiz de Azagra  
| cinematography = José F. Aguayo   Alfredo Fraile  Juan Serra
| studio = CIFESA
| distributor = CIFESA
| released =7 November 1947
| runtime = 
| country = Spain Spanish 
| budget =
| gross =
| preceded_by =
| followed_by =
}}
The Princess of the Ursines (Spanish:La princesa de los ursinos) is a 1947 Spanish historical film directed by  Luis Lucia and starring Ana Mariscal.  It was made by CIFESA, Spains largest studio at the time. The film is loosely based on real events that took place in the eighteenth century reign of Philip V of Spain. 

==Cast==
* Mariano Alcón as Secretario de Goncourt 
* Emilio Alonso  
* Mariano Asquerino as Torcy 
* Julio Rey de las Heras as Ministro 
* Manuel Dicenta as Capitán de control 
* Adriano Dominguez as Jefe de corchetes  
* Juan Espantaleón as Cardenal Portocarrero  
* Eduardo Fajardo as Capitán emisario  
* Félix Fernandez (actor)|Félix Fernandez as Cochero  
* Luis García Ortega as Conde de la Vega 
* César Guzman 
* José Isbert as Maese Pucheros 
* María Isbert as Lidia  
* José Jaspe as Truhán  
* José María Lado as Embajador Goncourt  
* Ana Mariscal as Ana María de la Tremouille 
* Arturo Marín as DArmegnon  
* Julia Pachelo as Dama 
* José Prada as Ortuzar  Manuel Requena as Ventero 
* Fernando Rey as Felipe V 
* Roberto Rey as Luis Carvajal  
* Santiago Rivero as Souville  
* Conrado San Martín as Capitán de frontera  
* Pilar Santisteban as La reina  
* Isidro Sotillo as Secretario del cardenal  
* Antonio Vadillo as Bronquista  
* Aníbal Vela as Ministro de la Guerra

== References ==
 
 
==Bibliography==
* Mira, Alberto. The A to Z of Spanish Cinema. Rowman & Littlefield, 2010. 
* Triana-Toribio, Núria. Spanish National Cinema. Routledge, 2012.

== External links ==
* 

 
 
 
 
 
 
 
 
 

 