Junior Bonner
 
{{Infobox film 
| name           = Junior Bonner
| image          = Juniorbonnerposter.jpg
| caption        = A promotional film poster for Junior Bonner.
| director       = Sam Peckinpah
| producer       = Joe Wizan
| writer         = Jeb Rosebrook Robert Preston Ben Johnson
| music          = Jerry Fielding
| cinematography = Lucien Ballard
| editing        = Frank Santillo Robert L. Wolfe
| distributor    = Cinerama Releasing Corporation
| released       =  
| runtime        = 100 min.
| country        = United States
| language       = English
| budget         = $3.2 million 
| gross          = $2.8 million "ABCs 5 Years of Film Production Profits & Losses", Variety, 31 May 1973 p 3 
}} Robert Preston and Ida Lupino. The film focuses on a veteran rodeo rider as he returns to his hometown of Prescott, Arizona to participate in an annual rodeo competition and reunite with his brother and estranged parents. Many critics consider it to be the warmest and most gentle of Sam Peckinpahs films. 

==Plot==
Junior "JR" Bonner is a rodeo rider who is slightly "over the hill". Junior is first seen taping up his injuries after an unsuccessful ride on an ornery bull named Sunshine.
 Independence Day parade and rodeo. When he arrives, the Bonner family home is being bulldozed by his younger brother Curly, an entrepreneur and real-estate developer, in order to build ranch homes.

Juniors womanizing, good-for-nothing father Ace and down-to-earth, long-suffering mother Elvira are estranged. (Note: both Preston and Lupino were born in 1918, making them just twelve years older than McQueen.) Ace dreams of emigrating to Australia to rear sheep and mine gold, but he fails to obtain financing from Junior, who is broke, and refuses to ask Curly for it.

After flooring his arrogant brother with a punch, Junior bribes rodeo owner Buck Roan to let him ride Sunshine again, promising him half the prize money. Buck thinks he must be crazy but Junior actually manages to pull it off this time, going the full eight seconds on the bull.

Junior walks into a travel agents office and buys his father a one-way, first-class ticket to Australia. The films final shot shows JR leaving his hometown, his successful ride on Sunshine continuing to put off the inevitable end of his career.

==Cast==
*Steve McQueen as Junior J.R. Bonner Robert Preston as Ace Bonner
*Ida Lupino as Elvira Bonner Ben Johnson as Buck Roan
*Joe Don Baker as Curly Bonner
*Barbara Leigh as Charmagne Mary Murphy as Ruth Bonner
*Bill McKinney as Red Terwiliger
*Dub Taylor as Del Sandra Deel as Nurse Arlis
*Don Red Barry as Homer Rutledge
*Charles H. Gray as Burt

== Themes ==
The story explores one of Sam Peckinpahs favorite themes - the end of a traditional form of honor and the arrival of modern capitalism on the western frontier. In a memorable scene, Ace and Junior escape from the rodeo parade on Juniors horse, ending up at a deserted railway station where they drink and despair at the state of the world and their indigency. The film has enjoyed a resurgence of popularity in the mid-2000s because of retrospectives and revival screenings of Sam Peckinpahs work and the screenplays predictions regarding capitalist development. 

== Production == Straw Dogs in England, Sam Peckinpah returned to the United States to begin immediate work on Junior Bonner. The lyrical screenplay by Jeb Rosebrook, depicting the changing times of society and binding family ties, appealed to Peckinpahs tastes. He accepted the project, concerned with being typed as a director of violent action (at the time, The Wild Bunch was his most renowned film and Straw Dogs was in preparation to be released to theaters). Junior Bonner would be his final attempt to make a low-key, dramatic work in the vein of Noon Wine (1966) and The Ballad of Cable Hogue (1970).

Filmed on location in Prescott, Arizona, Peckinpah utilized many colorful locales and residents as extras in the film. 

==Reception==
Due to a glut of rodeo-themed films released at that time, including The Honkers (1972), J.W. Coop (1972) and When the Legends Die (1972), Junior Bonner fell through the cracks and performed poorly at the box office.     It earned rentals of $1.9 million in North America and $900,000 in other countries, recording an overall loss of $2,820,000. 
 Walter Hills The Getaway, which they would film months after completing Junior Bonner. The second collaboration proved to be a financially successful one, as the action film would become one of the biggest box office successes of their careers.   

== References ==
 

== External links ==
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 