Pain of Love
{{Infobox film
  | name = Pain of Love
  | image =PainofLoveDVDcover.jpg
  | caption = Front cover of the Danish DVD box
  | director = Nils Malmros
  | producer = Per Holst
  | writer = Nils Malmros John Morgensen
  | starring = Anne Louise Hassing Søren Østergaard Birthe Neumann Waage Sandø
  | music = Gunner Møller Pedersen
  | cinematography = Jan Weincke
  | editing = Birger Møller Jensen
  | distributor = Egmont Entertainment
  | released =  
  | runtime = 115 minutes
  | country = Denmark Sweden
  | language = Danish
  | budget = 
    }}
 Danish dramatic tragedy written and directed by Nils Malmros. It stars Anne Louise Hassing and Søren Østergaard in a beautiful but bitter story about a young college student whose small setbacks in school and relationships lead her toward an inexorable descent into suicidal depression.

==Cast==
*Anne Louise Hassing ... Kirsten
*Søren Østergaard ... Søren
*Birthe Neumann ... Kirstens mor
*Waage Sandø ... Kirstens far
*Anni Bjørn ... Inge-Lise
*Peder Dahlgaard ... Anders
*Kamilla Gregersen ... Julie
*Finn Nielsen ... Lasse Ove Pedersen ... Psychology Examiner
*Mads-Peter Neumann ... Psychology Monitor
*Victor Marcussen ... Psychology Monitor
*Karin Flensborg ... Examiner

==Awards== Robert for Best Film and became the only film in Danish cinema to sweep the Bodil Awards with wins for Best Danish Film and for all four acting categories. It was also nominated for the Golden Bear at the 43rd Berlin International Film Festival.   

 
==References==
 

==External links==
* 
* 
* 
* 

 

 
 
 
 
 
 
 


 