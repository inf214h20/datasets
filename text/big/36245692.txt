Officer Down
{{Infobox film name           = Officer Down image          = Officer Down poster.jpg border         =  alt            = caption        = director  Brian A. Miller producer       = writer         = John Chase starring       = Stephen Dorff Dominic Purcell David Boreanaz AnnaLynne McCord Souljaboy Stephen Lang James Woods Walton Goggins Tommy Flanagan Oleg Taktarov Elisabeth Röhm music          = Jerome Dillon cinematography = editing        = Bob Mori studio         = distributor    = released       =   runtime        = country        = United States language       = English budget         =  gross          = $91,463
}} Brian A. Miller.

==Plot==
The story revolves around a corrupt police officer (Dorff) and his road to redemption.

==Cast==
*Stephen Dorff as Detective David Callahan
*Dominic Purcell as Royce Walker
*David Boreanaz as Detective Les Scanlon
*AnnaLynne McCord as Zhanna Dronov
*Soulja Boy Tell Em as Rudy
*Stephen Lang as Lieutenant Jake LaRussa
*James Woods as Captain John Verona
*Elisabeth Röhm as Alexandra Callahan
*Walton Goggins as Detective Nicholas Logue Tommy Flanagan as Father Reddy
*Oleg Taktarov as Oleg Emelyanenko Johnny Messner as McAlister
*Kamaliya as Katya
*Bea Miller as Lanie Callahan 
*AK Debris as James

==Development==
The film was first announced on May 3, 2011. Filming began later that week in Connecticut. The films screenplay was written by John Chase and it is directed by Brian A Miller.  Locations where the movie was filmed include Roberto’s Restaurant on State St, Fairfield Ave and inside a new apartment complex also located on Fairfield Ave, in Bridgeport CT.   The first images from the set were revealed on May 6, 2011. 

At the 2012 Cannes Film Festival, it was announced that the film would be distributed by Anchor Bay.   The films official poster was also revealed at the Cannes Film Festival. 

==Reception==
The film received negative reviews. 

==References==
 

==External links==
*  
*  

 
 
 
 
 