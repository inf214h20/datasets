Swargam
{{Infobox film
| name           = Swargam
| image          =
| caption        =
| director       = Unni Aranmula
| producer       =
| writer         = Unni Aranmula
| screenplay     = Unni Aranmula Mukesh Urvashi Urvashi Thilakan
| music          = Gopan
| cinematography = Vipin Mohan
| editing        = K Rajagopal
| studio         = Divya Movies
| distributor    = Divya Movies
| released       =  
| country        = India Malayalam
}}
 1987 Cinema Indian Malayalam Malayalam film, Urvashi and Thilakan in lead roles. The film had musical score by Gopan.   

==Cast==
 
*Jagathy Sreekumar Mukesh
*Urvashi Urvashi
*Thilakan
*Adoor Bhasi
*Sankaradi
*Unni
*Aranmula Ponnamma
*Bobby Kottarakkara
*K. P. Ummer
*Kanakalatha
*Karamana Janardanan Nair
*MG Soman
*Mala Aravindan Meena
*Ravi Menon
*Shelly
*Shivaji
*Umesh
 

==Soundtrack==
The music was composed by Gopan and lyrics was written by Unni Aranmula. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Eerezhu Pathinaalu || K. J. Yesudas || Unni Aranmula || 
|-
| 2 || Ezhu Nirangalil Ethu Niram || K. J. Yesudas || Unni Aranmula || 
|}

==References==
 

==External links==
*  

 
 
 

 