Edge of Doom
{{Infobox film
| name           = Edge of Doom
| image          = Edge of Doom movie poster.JPG
| alt            =
| caption        = Theatrical release poster
| producer       = Samuel Goldwyn
| director       = Mark Robson
| screenplay     = Philip Yordan
| based on       =  
| narrator       = Dana Andrews Joan Evans
| music          = Hugo Friedhofer
| cinematography = Harry Stradling
| editing        = Daniel Mandell
| studio         = Samuel Goldwyn Productions
| distributor    = RKO Radio Pictures
| released       =    |ref2= }}
| runtime        = 99 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}} Joan Evans. 

==Plot== Catholic priest, who slighted him, by beating him with a heavy crucifix.  Later, another young priest, Father Roth (Dana Andrews), suspects the young man, now arrested for another crime, for the killing.

==Cast==
* Dana Andrews as Father Thomas Roth
* Farley Granger as Martin Lynn Joan Evans as Rita Conroy, Martins girlfriend Robert Keith as Lieutenant Mandel Paul Stewart as Craig
* Mala Powers as Julie
* Adele Jergens as Irene, Craigs girlfriend
* John Ridgely as 1st Detective
* Douglas Fowley as 2nd Detective
* Harold Vermilyea as Father Kirkman
* Mabel Paige as Mrs. Pearson
* Ellen Corby as Mrs. Jeanette Moore
* Robert Karnes as George, a Priest narrated to

==Reception==

===Critical response===
When the film was released, the staff at Variety (magazine)|Variety magazine gave the film a positive review, writing, "A grim, relentless story, considerably offbeat, gives some distinction to Edge of Doom. It is played to the hilt by a good cast and directed with impact by Mark Robson."   The New York Times wrote, "Robsons direction gives flashes of high tension to the film, for he has made effective use of street scenes and noises and has skillfully reflected the oppressive atmosphere of poverty and squalor, but his actors run more to types than to real people." 

===Awards===
Wins
* National Board of Review of Motion Pictures: NBR Award - Top Ten Films; 1950.

==References==
 

==External links==
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 