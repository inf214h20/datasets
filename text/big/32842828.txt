Riders of the Sage
{{Infobox film
| name           = Riders of the Sage
| image          = 
| image_size     = 
| caption        = 
| director       = Harry S. Webb
| producer       = Harry S. Webb (producer)  (uncredited)
| writer         = Forrest Sheldon (story) Carl Krusada (screenplay)
| narrator       = 
| starring       = See below
| music          = 
| cinematography = Edward A. Kull
| editing        = Frederick Bain
| studio         = 
| distributor    = 
| released       = 1939
| runtime        = 57 minutes
| country        = USA
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}}

Riders of the Sage is a 1939 American film directed by Harry S. Webb.

== Plot summary ==
 

== Cast == Bob Steele as Bob Burke
*Claire Rochelle as Mona Halsey
*Ralph Hoopes as Buddy Martin
*Jimmy Aubrey as Steve Reynolds
*Carleton Young as Luke Halsey
*Earl Douglas as Hank Halsey Ted Adams as Poe Powers Dave OBrien as Tom Martin Frank LaRue as Jim Martin
*Bruce Dane as Rusty, the Singer
*Jerry Sheldon as Herb
*Reed Howes as Sam Halsey
*Bud Osborne as Sheriff

==See also==
*Bob Steele filmography

== External links ==
* 
* 

 

 
 
 
 
 
 
 
 