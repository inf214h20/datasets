Ed Gein: The Butcher of Plainfield
{{Infobox film 
| name           = Ed Gein: The Butcher of Plainfield
| image          = Ed Gein- The Butcher of Plainfield.jpg
| image_size     =
| caption        = DVD Cover
| director       = Michael Feifer
| producer       = Barry Barnholtz	
| writer         = Michael Feifer
| starring       = Kane Hodder Adrienne Frantz Michael Berryman Priscilla Barnes Shawn Hoffman
| music          = Glenn Morrissette
| cinematography = Roberto Schein
| editing        = Bryan Roberts
| distributor    = Feifer Worldwide
| released       = March 6, 2007
| country        = United States
| runtime        = 90 minutes English 
}}
 2007 United States|U.S.-American direct-to-video horror film starring Kane Hodder, Adrienne Frantz, Michael Berryman, Priscilla Barnes and Shawn Hoffman. It is based upon the crimes of serial killer Ed Gein. 

==Synopsis==
In the late 1950s, a quiet town in rural Wisconsin is rocked by a series of gory murders and grave robberies committed by a man who has descended into insanity. 

==Plot== traumatized by his mother, a religious fanatic who taught him that sex was evil and that all women (herself excluded) were sinful.

When he kidnapped 58-year-old Bernice Worden, the police came to Geins farmhouse and found her dismembered and disemboweled corpse, as well as parts of at least 15 other corpses.

The movie plot centers on Gein kidnapping and slaughtering Vera Mason, the mother of Bobby Mason, a sheriffs deputy, and his girlfriend. The ensuing action focuses on on the race to catch her, before its too late. These events are not true events.

== Cast ==
*Kane Hodder as Ed Gein
*Adrienne Frantz as Erica
*Michael Berryman as Jack
*Priscilla Barnes as Vera Mason
*Shawn Hoffman as Bobby Mason
*Timothy Oman as Sheriff
*Caia Coley as Sue Layton John Burke as Rick Layton
*Matteo Indelicato as Deputy Coley
*Stan Bly as Deputy Lyle

==Release== Black Dahlia, and Saw III.

==References==
 

== External links ==
*  
*  

 
 
 
 
 

 