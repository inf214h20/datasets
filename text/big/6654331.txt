Barsaat Ki Raat
 
{{Infobox film
| name           = Barsaat Ki Raat
| image          = BarsaatKiRaat FilmPoster.jpg
| director       = P. L. Santoshi Mumtaz Begum K. N. Singh
| producer       = R. Chandra Roshan Sahir Ludhianvi (lyrics)
| released       = 1960
| runtime        = 142 min.
| language       = Urdu, Hindi
|}} Mumtaz Begum and K.N. Singh. It was directed by P. L. Santoshi. 
 qawwali songs and was one of the biggest hits at the box-office in 1960. This is music director Roshans first big success as almost all the songs were hit. Barsaat Ki Raat was also one of the last films to star celebrated actress Madhubala.

==Story==
The story features a number of innovative themes while maintaining the basic form of a love story. It has particularly strong female characters who are independent-minded and choose their own loves and destiny.  The conflicts are not so much between the wishes of the parents and children about whom to marry, as is a common theme in Indian movies, but it is on the more complex level of the conflicts among the main characters and the duplicitous signals men and women send each other. The movie glorifies the lives of "singing girls" not often regarded highly in Indian society.  Although it is set with Muslim characters, the movie seamlessly shows the universality of sensual love.

==Music==
The song and dance does not occur randomly as is often the case in Indian movies; instead it is an integral part of the story-line, which involves a poet and singer as well as poetry competitions that were once common.

Female singers: Asha Bhosle, Kamal Barot, Lata Mangeshkar, Sudha Malhotra, Suman Kalyanpur 
Male singers: S. D. Batish, Balbeer, Bande Hasan, Manna Dey, Mohammad Rafi 
{{Track listing
| extra_column = Singer(s) Roshan
| all_lyrics = Sahir Ludhianvi
| lyrics_credits  = yes
| title6 = Na To Karvaan Ki Talash Hai
| extra6 = Mohammad Rafi, Sudha Malhotra, Asha Bhosle, Manna Dey
| length6 = 12:03
| title5 = Nigah-e-Naaz Ke Maron Ka Haal-Qawwali
| extra5 = Asha Bhosle, Sudha Malhotra, Shankar Shambhu, S. D. Batish & Chorus
| length5 = 8:43
| title1 = Zindagi Bhar Nahi Bhoolegi
| extra1 = Mohammad Rafi
| length1 = 4:26
| title2 = Garjat Barsat Sawan Aayo Re
| extra2 = Suman Kalyanpur & Kamal Barot
| title3 = Main Ne Shayad
| extra3 = Mohd. Rafi
| title4 = Mujhe Mil Gaya Bahana
| extra4 = Lata Mangeshkar
| title7 = Yeh Hai Ishq Ishq
| extra7 = Mohd. Rafi, Manna Day, Sudha Malhotra, S. D. Batish & Chorus
| title8 = Ji Chahta Hai Choom Loon - Qawwali
| extra8 = Asha Bhosle, Sudha Malhotra, Balbir, Bande Hasan & Chorus
| title9 = Zindagi Bhar Nahin Bhoolengi
| extra9 = Lata Mangeshkar & Mohd. Rafi
}}

==Notes==
 

==External links==
*  
*  

 
 
 
 


 