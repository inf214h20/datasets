Don Juan's Three Nights
{{Infobox film
| name           = Don Juans Three Nights
| image          =
| caption        = John Francis Dillon Henry Hobart
| writer         = Clara Beranger (adaptation & scenario) Gerald Duffy (intertitles)(as Gerald C. Duffy)
| based on       = novel, Lajos Biro Shirley Mason Malcolm McGregor
| cinematography = James Van Trees
| editing        =
| distributor    = First National
| released       = September 4, 1926(premier) October 3, 1926(wide release)
| runtime        = 76 minutes
| country        = USA
| language       = Silent..English intertitles
}} John Francis Shirley Mason Henry Hobart and distributed through First National Pictures.  

It is preserved in the Library of Congress collection and at the Wisconsin Center for Film and Theatre Research(Madison).   


==Cast==
*Lewis Stone - Johann Aradi Shirley Mason - Ninette Cavallar
*Malcolm McGregor - Giulio Roberti
*Myrtle Stedman - Madame Cavallar
*Betty Francisco - Madame de Courcy
*Kalla Pasha - Monsieur de Courcy
*Alma Bennett - Carlotta
*Natalie Kingston - Vilma Theodori
*Mario Carillo - Count di Bonito
*Jed Prouty - Lippi
*Madeline Hurlock - Louise Villate
*Gertrude Astor - Baroness von Minden

==Reference==
 


==External links==
* 
* 

 

 
 
 
 
 
 
 
 
 

 
 