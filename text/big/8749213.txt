Dracula and Son
{{Infobox film
| name           = Dracula and Son
| image          = Draculapere.jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = French film poster
| director       = Edouard Molinaro Browning, 2010. p.72 
| producer       = Alain Poire 
| writer         = Alain Godard Edouard Molinaro Jean-Marie Poiré 
| screenplay     = 
| story          = 
| based on       =      
| narrator       = 
| starring       = Christopher Lee
| music          = Vladimir Cosma 
| cinematography = Alain Levent 
| editing        = Monique Isnardon Robert Isnardon 
| studio         = 
| distributor    = 
| released       =     Hallenbeck, 2009. p.218 
| runtime        = 96 minutes 
| country        = France 
| language       = French  
| budget         = 
| gross          = 
}}

Dracula and Son ( ) is a 1976 French comedy and horror film directed and written by Edouard Molinaro. The film is about a vampire father and son.

==Plot== night watchman in Paris, France where he falls for a girl.  Naturally, tensions arise when father and son are reunited and both take a liking to the same girl.

== Cast ==
* Christopher Lee as Le prince des Ténèbres
* Bernard Menez as Ferdinand Poitevin
* Marie-Hélène Breillat as Nicole Clement
* Catherine Breillat as Herminie Poitevin
* Bernard Alane as Jean
* Jean-Claude Dauphin as Cristéa
* Raymond Bussières as Lhomme âgé à lANPE
* Mustapha Dali as Khaleb
* Xavier Depraz as Le majordome

==Release==
Dracula and Son was released in France on September 16, 1976.  It was released in 1979 in the United States.  The American distributor of the film cut many scenes in the film and replaced them with different gags.   

==Reception==
Allmovie gave the film a rating of two stars out of five, but noted that "this was a very witty film prior to its decimation by an uncaring American distributor.  A review in TV Guide gave a positive review of three stars out of four, noting that the film "actually works because it treats its subject with respect and doesnt degrade it for cheap, campy laughs." while noting that the film has a "poor dubbing job" that made the character Ferdinand Poitevin sound like a cross between Woody Allen and Austin Pendleton. 

==Notes==
 

==See also==
 
* Christopher Lee filmography
* List of comedy films of the 1970s
* List of French films of 1976
* List of horror films of 1976

==References==
*{{cite book
 | last= Browning
 | first= John Edgar
 |author2=Picart, Caroline Joan 
 | title= Dracula in Visual Media:Film, Television, Comic Book and Electronic Game Appearances, 1921-2010 McFarland
 |year= 2010
 |isbn= 0786433655
}}
*{{cite book
 | last= Hallenbeck
 | first= Bruce G.
 | title= Comedy-Horror Films:A Chronological History, 1914-2008 McFarland
 |year= 2009
 |isbn= 0786433329
}}

== External links ==
*  

 
 

 
 
 
 
 
 
 

 
 