Pocahontas (1994 film)
{{multiple issues|
 
 
}}

:This article is for the animated film from Jetlag Productions and GoodTimes Home Video. For the Disney film, see Pocahontas (1995 film)
{{Infobox Film |
  name	= Pocahontas |
  image	= Pocahontas_1995.jpg |
  size	= 250px |
  director	= Toshiyuki Hiruma Takashi |
  producer	= Mark Taylor |
  writer	= Jack Olesker (based on a true story) |
  starring	= | GoodTimes Home Video |
  released	= October 19, 1994 (USA)  August 6, 2002 (DVD release) (USA) |
  runtime	= 45 minutes |
  language	= English, Japanese |
  country	=   Japan   United States |
  budget	= |
  music	= Andrew Dimitroff Nick Carr Ray Crossley Joellyn Copperman (lyrics) 
 |
  awards	= |
}}
 GoodTimes Home Alogonquian maiden Pocahontas.  The film was re-released on DVD on August 6, 2002   by  GoodTimes Entertainment as part of their extensive "Collectible Classics" line that included the works of other animation companies such as Golden Films and Blye Migicovsky Productions.

== Plot summary ==
 Captain John Smith, and the two become fast friends.  Captain Smith is a friendly man and desires peace, but neither he nor Pocahontas can prevent a growing hatred between the two groups.  Some of the Indians, feeling that Powhatan has grown weak, decide to abandon the tribe and start their own, with new laws.  The ones that remain allow themselves to be taught many things by the white men, including new sports and useful everyday things.  As winter begins to approach, the two groups face difficult problems; the Indians believe that the white men are purposely hunting all of their animals and that they may soon be left to starve, while the white men fear they may not survive unless theyre invited to spend the winter safely in this new land, rather than sail back to England.  The wisdom and good heart of young Pocahontas becomes the key to the collaboration between the two different groups of people in order to survive those difficult times.

==Cast==
*Tony Ail
*Nathan Aswell
*Cheralynn Bailey
*Kathleen Barr
*Garry Chalk
*Lilliam Carlson
*Ian James Corlett
*Michael Donovan
*Kent Gallie
*Phil Hayes
*Roger Kelly
*Ellen Kennedy
*Terry Klassen
*Joanne Lee
*Andrea Libman
*Tom McBeath
*Lois McLean
*Scott McNeil
*Jesse Moss
*Doug Newell Richard Newman
*Crystaleen OBray Doug Parker
*Gerard Plunkett
*Susan Sciff
*Raouel Shane
*Scott Swanson
*Venus Terzo
*Louise Vallance
*Wanda Wilkinson
*Dale Wilson

==Songs==
As a common rule in Jetlag Productions films, Pocahontas featured three original songs:

* "Land of Pocahontas": The new world is described as "an unsullied land across the sea" in this first opening and closing song.  Sung by Wendy K. Hamilton-Caddey.
* "(Are They) Enemy or Friend?": The tribe struggles to understand the true purpose and intention of the white men. Sung by Wendy K. Hamilton-Caddey and a male vocal. 
* "Princess of Peace": Pocahontas life is described in this closing song and referred to as the "princess of peace". Sung by a mens choir.

== References ==
 

== External links ==
*  
*  
*   at the Big Cartoon Database

 

 
 
 
 
 
 
 
 
 
 
 