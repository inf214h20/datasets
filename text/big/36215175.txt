Republik Twitter
{{Infobox film
| name           = Republik Twitter
| image          = Republik Twitter.jpg
| imagesize      =
| director       = Kuntz Agus
| producer       = {{plainlist|
*Ajish Dibyo
*Faizan Zidni
*Isaac Zikri
*Eva Fadillah
}}
| eproducer      = Ridwan Hakim
| writer         = E.S. Ito
| starring       = {{plainlist|
*Laura Basuki
*Abimana Aryasatya
*Enzy Storia
*Ben Kasyafani
*Tio Pakusadewo
}}
| music          = 
| cinematography = Ujel Bausad
| editing        = {{plainlist|
*Aline Jusria
*Bayu Sulistyo
}}
| distributor    =  {{plainlist|
*Amalina Pictures
*RupaKata Cinema
}}
| released       =  
| runtime        = 90 minutes
| country        = Indonesia
| awards         =
| language       = Indonesian
| budget         =
| gross          =
}}

Republik Twitter (stylised as #republiktwitter; meaning The Twitter Republic) is an Indonesian romantic drama directed by Kuntz Agus released in 2012. Starring Laura Basuki and Abimana Aryasatya, it follows a university student in his quest to be with a beautiful journalist.

Work on Republik Twitter began in June 2011, when screenwriter Eddri Sumitra Ito began preparing a script. Based on one of his earlier blog posts, pre-production for the story took four months, with another fourteen days shooting and four months post-production. Its themes of social media use in Indonesia were generally well received.

==Plot==
Sukmo (Abimana Aryasatya), a student based in Yogyakarta, spends much of his time on the computer, especially on Twitter. He falls in love with another user, Hanum (Laura Basuki), a beautiful journalist whom he has never met face-to-face. Sukmo considers the internet his main way of life, while Hanum finds it an escape from her daily schedule. Finally Sukmo decides to go to Jakarta with his friend Andre (Ben Kasyafani) and meet Hanum, but is stunned by her beauty and, when he sees her speaking to another man, leaves without introducing himself to her.

Before Sukmo can return to Yogyakarta, he is met by Kemal (Tio Pakusadewo), a businessman who is helping politicians better use Twitter during their campaigns. With the help of Andre and his girlfriend Nadya (Enzy Storia), Sukmo rises to Twitter stardom and becomes a powerful voice in the political campaigns. With his newfound status, Sukmo is able to approach Hanum and they begin seeing each other.

==Production== documentaries and drawing inspiration from the stylings of Tim Burton and the Coen brothers. Republik Twitter was his first feature film. 

The script was written by Eddri Sumitra Ito, who had a background in writing historical novels.  Ito wrote the screenplay concurrently with his third novel, drawing inspiration from political campaigning in Indonesia and one of his blog posts about Twitter addiction.  The film was produced by Kuntz friend Ajish Dibyo, a manager of the Jogja-NETPAC Asian Film Festival, while the production studios Amalina Pictures and Rupakata Cinema handled other aspects. 

Pre-production took four months, beginning when the script was written in June 2011.  During the research phase of production, Kuntz and the crew spoke with numerous marketing managers and other professionals who used Twitter for financial gain. They researched the use of fake accounts for marketing, and Twitter celebrities for information on the  financial benefits of  having thousands of followers. Citra Award-winning actress Laura Basuki was cast in the main role of Hanum; Kuntz decided to make the character a journalist because he found the career "sexy because they tell everything on Twitter except stuff about their personal lives".  At the time of the films release, Indonesia had 20 million Twitter users, making it the fifth most-represented country on the website. 

The film underwent fourteen days of shooting and another four months of editing.  Krisna Purna composed the films music, while Khikmawan Susanto and Auvarahma Triangga served as sound designer and sound mixer, respectively. The theme song, "Jatuh Hati" ("Fall in Love"), was written by Ipang Lazuardi, while Potenzio wrote "Twitter Dunia" ("Twitter the World"). Both songs were released as singles in January 2012. 

==Themes==
Kuntz described Republik Twitter as a film about the Indonesian people and their interconnections, not Twitter or social media.  He considered Twitter changing the way Indonesians interacted, but not enough to cause social change. Instead, he said, the medium was mainly used for self-promotion.  Jenée Tibshraeny, in a review for The Jakarta Post, wrote that the film explores "overlaps between the online and offline world", resulting in viewers contemplating social issues in the country. 

==Release and reception==
Republik Twitter was released on 16 February 2012. It was the second Indonesian film centred on social media, after I Know What You Did On Facebook in 2010. 

Tibshraeny described the film as must-see, "if not for a snapshot of Indonesian pop culture, then for a good laugh at yourself for being a part of it."  Irma Manggia, writing for the Semarang-based Suara Merdeka, described the film as light and easy to understand, despite the several schemes shown in the plot. 

==References==
Footnotes
 

Bibliography
 
*{{cite news
 |last=Desita
 |first=Prima
 |title=My Jakarta: Kuntz Agus, Film Director
 |url=http://www.thejakartaglobe.com/myjakarta/my-jakarta-kuntz-agus-film-director/495945
 |date=6 February 2012
 |work=Jakarta Globe
 |accessdate=22 June 2012
 |archivedate=22 June 2012
 |archiveurl=http://www.webcitation.org/68bqWRciK
 |ref= 
}}
*{{cite news
 |title=Love a Click Away in Indonesia’s Twitter Republic
 |url=http://www.thejakartaglobe.com/entertainment/love-a-click-away-in-indonesias-twitter-republic/497374
 |date=11 February 2012
 |work=Jakarta Globe
 |accessdate=22 June 2012
 |archivedate=22 June 2012
 |archiveurl=http://www.webcitation.org/68bqFLh8G
 |ref= 
}}
*{{cite news
 |last=Manggia
 |first=Irma M.
 |title=Ada Cinta dalam Twitter
 |trans_title=Theres Love on Twitter
 |language=Indonesian
 |url=http://m.suaramerdeka.com/index.php/read/cetak/2012/02/19/177598
 |date=19 February 2012
 |work=Suara Merdeka
 |accessdate=22 June 2012
 |archivedate=22 June 2012
 |archiveurl=http://www.webcitation.org/68c1xqLyA
 |ref= 
}}
*{{cite news
 |last=Sawitri
 |first=Adisti Sukma
 |title=E.S. Ito: In fiction, reality works better
 |url=http://www.thejakartapost.com/news/2012/03/18/es-ito-in-fiction-reality-works-better.html
 |date=18 March 2012
 |work=The Jakarta Post
 |accessdate=22 June 2012
 |archivedate=22 June 2012
 |archiveurl=http://www.webcitation.org/68boWo1jt
 |ref= 
}}
*{{cite news
 |last=Siregar
 |first=Lisa
 |title=Is Twitter-Themed Indonesian Film Trending-Topic Good?
 |url=http://www.thejakartaglobe.com/entertainment/is-twitter-themed-indonesian-film-trending-topic-good/497507
 |date=12 February 2012
 |work=Jakarta Globe
 |accessdate=22 June 2012
 |archivedate=22 June 2012
 |archiveurl=http://www.webcitation.org/68bovndPF
 |ref= 
}}
*{{cite news
 |last=Tibshraeny
 |first=Jenée
 |title=Republik Twitter: The real love
 |url=http://www.thejakartapost.com/news/2012/02/12/republik-twitter-the-real-love.html
 |date=12 February 2012
 |work=The Jakarta Post
 |accessdate=22 June 2012
 |archivedate=22 June 2012
 |archiveurl=http://www.webcitation.org/68bg9xJFv
 |ref= 
}}
 

==External links==
* 

 
 
 
 