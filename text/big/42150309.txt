The Other Love
{{Infobox film
| name           = The Other Love
| image          = The Other Love 1947 Poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = André de Toth David Lewis
| screenplay     = {{Plainlist|
* Ladislas Fodor Harry Brown
}}
| based on       =  
| starring       = {{Plainlist|
* Barbara Stanwyck
* David Niven
* Richard Conte
}}
| music          = Miklós Rózsa
| cinematography = Victor Milner
| editing        = Walter A. Thompson Enterprise Productions
| distributor    = United Artists
| released       =  
| runtime        = 95–97 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 Harry Brown based on the story "Beyond" by Erich Maria Remarque, the film is about beautiful concert pianist who is sent to a sanatorium in Switzerland to treat a serious lung illness. Although she is attracted to her doctor, she ignores his instructions to rest and spends her time in Monte Carlo with a playboy racecar driver.

In his review for The New York Times, Bosley Crowther called the film "a typical artificial romance on the heart-rending theme of Camille".   

==Cast==
* Barbara Stanwyck as Karen Duncan
* David Niven as Dr. Anthony Stanton
* Richard Conte as Paul Clermont
* Gilbert Roland as Croupier
* Joan Lorring as Celestine Miller
* Lenore Aubert as Yvonne Dupré
* Maria Palmer as Huberta
* Natalie Schafer as Dora Shelton
* Edward Ashley as Richard Shelton
* Richard Hale as Professor Linnaker

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 