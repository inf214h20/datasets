100 Days with Mr. Arrogant
{{Infobox film
| name           = 100 Days with Mr. Arrogant
| image          = 100 Days With Mr Arrogant poster.jpg
| writer          = 
| starring       = Ha Ji-won Kim Jaewon
| director       = Shin Dong-yeop
| producer       = Jun Ji-hyun
| editing        = Go Im-pyo
| cinematography = Hwang Cheol-hyeon
| distributor    = Cinema Service
| released       =  
| runtime        = 95 minutes
| country        = South Korea
| language       = Korean
| music          = Jo Byeong-seok
| budget         = 
| film name      = {{Film name
 | hangul         =  
 | rr             = Nae sarang ssagaji
 | mr             = Nae sarang ssagaji}}
}}
100 Days with Mr. Arrogant (aka My Love Ssagaji) is a 2004 South Korean romantic comedy film.

==History== Do Re Mi Fa So La Ti Do (도레미파솔라시도), A Millionaires First Love (백만장자의 첫사랑), Romance of Their Own (늑대의 유혹), and He Was Cool (그놈은 멋있었다) were also derived from internet fiction) This popular internet fiction was later released in the form of a four part book series that was later turned into a movie. Although there are many scenes that were taken out and also added in place, it was a big hit around that time to create movies that derived from internet fiction stories and writers.

==Plot==
After being dumped by her boyfriend just before their 100 day anniversary, Ha-Yeong (Ha Ji-won) meets a college guy named Hyung-Jun (Kim Jaewon) when she kicks a can that accidentally hits him in the face and causes him to scratch his Lexus. He demands she pay him $3000 on the spot. She escapes from him, leaving her wallet behind.

Hyung-Jun stalks her, demanding money to pay for his car. Since she is a poor high school student Hyung-Jun writes up a "Enslavement Agreement" for Ha-Yeong in order to pay for the damage to his car. Ha-Yeong is thrown into a nightmarish slave life for 100 days, running his errands, i.e.: cleaning his house, carrying his shopping, and cleaning his car.

By accident she finds out that the damage to Hyung-Juns car costs only $10. She then takes her revenge by damaging his car and his reputation. But Hyung-Jun takes revenge by becoming her new tutor this brings them close to each other and they realize they love one another. Hyung-Jun frees Ha-Yeong from slavery and kiss her. Ha-Yeongs moms sees this and threatens Hyung-Jun to stay away from her daughters life and brings a new tutor to teach Ha-Yeong.Ha-Yeong says to Hyung-Jun that she wants to marry him but he says that he only toyed with her. Hyung-Jun leaves his apartment which makes Ha-Yeong more vulnerable.She studies hard so she can get into Hyung-Juns college. After the exams she finds out she was not selected to enter the same college as Hyung-Jun,her tutor takes her to a place where she finds Hyung-Jun telling her that she was selected into his college but he wanted to give her a surprise. after a long time we see Ha-Yeong driving while a high school kid kicks a can that accidentally hits her in the face and cause her to scratch her Lexus. And it starts again!! 
.
The original Korean title can be literally translated as "my love, the asshole," or, more roughly, as "my love, the no-manners".

==Cast==
* Kim Jaewon ...  Ahn Hyung-jun
* Ha Ji-won ...  Kang Ha-yeong Kim Tae-hyun ...  Yeong-eun
* Han Min ...  Hyeon-ju
* Kim Chang-wan ...  Ha-yeongs Father
* Hong Ji-Yeong
* Kim Ji-yu
* Kim Min-kyeong
* Lee Eung-kyung ...  Ha-yeongs Mother
*Kim Yong-gun - Hyung-juns father

==References==
 

==External links==
*   at LoveHKfilm.com
*  
*  

 
 
 
 
 
 