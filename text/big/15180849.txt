Action (1921 film)
 
{{Infobox film
| name           = Action
| image          = Action_(1921_film)_ad.jpg
| caption        = Ad for the film in the Casper Daily Tribune (Wyoming), October 21, 1921
| director       = John Ford
| producer       =
| writer         = J. Allan Dunn Harvey Gates Peter B. Kyne
| starring       = Hoot Gibson
| cinematography = John W. Brown
| editing        = Universal Film Manufacturing Company
| released       =  
| runtime        = 50 minutes
| country        = United States
| language       = Silent with English intertitles
| budget         =
| gross          =
}} Western film Short Stories, February 1921. 

==Cast==
* Hoot Gibson as Sandy Brouke Francis Ford as Soda Water Manning
* J. Farrell MacDonald as Mormon Peters
* Buck Connors as Pat Casey
* Clara Horton as Molly Casey
* William Robert Daly as J. Plimsoll
* Dorothea Wolbert as Mirandy Meekin
* Byron Munson as Henry Meekin Charles Newton as Sheriff Dipple
* Jim Corey as Sam Waters Ed Jones as Art Smith (credited as Ed "King Fisher" Jones)

==See also==
* Hoot Gibson filmography
* List of lost films

==References==
 

==External links==
* 
* 

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 