Dead and Deader
{{Infobox television film
| name = Dead And Deader
| image = DeadAndDeader.jpg
| caption = DVD release poster
| director = Patrick Dinhut
| producer = {{plainlist|
* Mark A. Altman
* Mark Gottwald
* Chuck Speed
}}
| writer = {{plainlist|
* Mark A. Altman
* Steven Kriozere
}}
| starring = {{plainlist|
* Dean Cain
* Guy Torry
* Susan Ward
* John Billingsley
* Armin Shimerman
* Colleen Camp
}}
| music = Joe Kraemer
| cinematography = Raymond Stella
| editing = Alan Pao
| studio = Mindfire Entertainment
| distributor = The Sci-Fi Channel
| released =  
| runtime = 89 minutes
| country = United States
| language = English
| budget =
}}
Dead And Deader is a zombie horror film directed by Patrick Dinhut and stars Dean Cain, Guy Torry, Peter Greene, and Susan Ward, with cameos from Armin Shimerman, John Billingsley, and Dean Haglund.  It was originally intended to be a third film in the House of the Dead film series but dropped that title before begin released straight-to-DVD in 2007.

==Plot==
 
A small U.S. special-forces squad travels to Cambodia to follow up on the case of a small medical outpost that has ceased communications, and why the first group sent to the outpost did not report back. The squad soon finds itself confronted by flesh-eating zombies seemingly created by a strange species of scorpion. The squad clears away the zombies, but is then attacked by an infected researcher who attempts to commit suicide with a grenade. Second-in-command Lieutenant Bobby Quinn (Dean Cain), initially survives and radios for a medivac airlift, but soon falls unconscious.

Later, Quinn wakes up on an exam table at Ft. Preston Army Base, to find he is being prepped for postmortem. The coroner, Dr, Flutie (Armin Shimerman), explains that he arrived in a body-bag the previous evening and had been pronounced DOA, as he has no vital signs. Quinn begins to get dressed and asks about the fate of his squad, which Flutie reveals are all dead. Quinn is suddenly stricken by an agonizing pain in his right arm, and grabs a scalpel to slice it open. Gushing greenish blood, the wound reveals one of the strange scorpions, which he promptly crushes. The incision rapidly heals without a trace of scarring.

Quinn and Flutie soon relate their findings to Dr. Boyce, who is determined to keep Quinn quarantined. Quinn, however, is driven to find out what happened to himself and his squad, and resists her directives. She tries to have him sedated, but the sedative has no effect, and in the course of the attempt, Quinn reveals superhuman strength to go with his rapid-healing and altered metabolism, and she finally reveals that one of the other members of his squad, Sgt. Cruz, is also in the morgue scheduled for cremation, and the other bodies were being taken elsewhere. Quinn demands to see him, and she takes him to Dr. Langdon (John Billingsley) to find his whereabouts when he turns up missing. With Quinns heightened senses, the trio tracks Cruz to the bases kitchen, where he is attacking the cook, Judson (Guy Torry). In the process, both Boyce and Langdon are infected, and Quinn and Judson have to destroy them. Right after, Quinn is stricken by intense pangs of hunger that can only be dulled by ingesting raw, red meat. The two of them are then taken into custody by Major Bascom (Kirk B.R. Woller), who refuses to believe Quinns story (though he does take notice of the unusually deep dent that Quinn made in his table after angrily slamming down his fist).

Quinn and Judson promptly escape, determined to track down the remaining squad-members before they can wreak havoc. They manage to find the truck used to transport the bodies, finding it wrecked and covered in blood. While following the trail, they stop at a small road-side bar, where they meet Holly, a smart U.C San Bernardino film-student who works as a bartender part-time as she studies for her degree. Unfortunately for Quinn and Judson, the local news has already aired a report about the "killings" at Fort Preston, and the bar-patrons lock them inside the cooler. When a group of infected soldiers attack the bar, Holly retreats to the cooler, and the trio manages to eliminate the zombified soldiers and patrons before they can get out. Having been called by the patrons, the police arrive to arrest Quinn and Judson. Quinn manages to break free of his handcuffs, Holly nabs the police cruiser and the trio escape yet again.

On the trail of the final body, Bill Sanderson, the trio change their clothes and general appearance in order to better disguise themselves (the African-American Judson humorously dressing in clothes similar to those worn by Michael Jackson in Michael Jacksons Thriller (music video)|Thriller), and head to L.A to intercept Sanderson. However, a visit to Sandersons home turns up nothing, and Quinn is stricken by another wave of hunger and has to raid the fridge to ease it. As he eats, a woman walks in and they mistake her for one of the family members. She reveals that she is Mrs. Wisteria from next door, and that she has been watching the house for the Sandersons who are on vacation, and that the wake is not until the following day. They try to satisfy her by saying they need the info for a "secret government mission" but she insists upon helping, threatening to call the police otherwise. The trio grudgingly agree, and Mrs. Wisteria drives them to the Everview Funeral Home where Sanderson was taken, and flirts openly with Judson to his dismay. When they arrive, the director (Dean Haglund) tells them that Sandersons body had already been claimed the previous evening. Frustrated, the group makes to leave, but they are intercepted when a van pulls up and the occupants stop them at gunpoint.

Their captors are soon revealed to be mercenaries under the hire of a Dr. Scott (Peter Greene), and his partner Dr. Adams (Ellie Cornell), who have Sanderson chained up in their hidden lab. Scott states that the Cambodians call the scorpion-like creature a "jindu", and that its sting revives dead tissue. It had been a legend until some relief workers stumbled upon the species some years back. It was Scott who sent the mercenaries that killed the workers at the relief-station, and who had themselves ended up dead. He goes on to say that the sting of the creature represents everlasting life if properly utilized, and that it would be of great benefit to the people of the world "for the right price". When Quinn counters that the infection must be stopped before it can spread, Scott angrily confesses that he is dying of cancer and thus seeks the secret for his own benefit first. He demands a sample of Quinns blood, and when Quinn refuses, he brings in Mrs. Wisteria and threatens to feed her to Sanderson. Quinn relents, but Scott gives Wisteria to Sanderson anyway, and they watch as she is bitten, dies, and transforms into one of the zombies, and Scott confirms that the second-generation zombies are always mindless killers: only those stung by the jindu have the potential to retain their personalities, so long as the creature does not travel straight to their heart. The female merc destroys Wisteria with a shot to the head. Despite being weakened by hunger, a furious Quinn destroys Sanderson as well, before being knocked out cold.

When Quinn awakens, Scott and Adams are preparing to leave for Fort Preston. They take him back to the holding cell, and chain him to Holly while taking Judson with them as a hostage and as a guide. Holly manages to evade the ravenous Quinn until he leaps at her and misses, shattering the two-way mirror that leads into Adams lab. Inside, he stumbles over the mini-fridge where the raw meat is kept for Quinn, and he quickly devours it, and comes back to his senses. Scott and his group reach Fort Preston, which is now overrun with zombies, thanks to the body of one of the mercs that had been brought back from Cambodia. Scott is on the hunt for that particular zombie, since he will be the only one with the jindu inside. The group proceeds within, destroying attacking zombies as they go, but do not realize that Quinn and Holly are hot on their trail. Scott has Judson open the security door where one of the officers had managed to trap most of the infected, and then Scott takes Judsons keycard and leaves him trapped outside the security doors, where a horde of zombies descend upon him.

Inside, the group eventually finds the morgue containing the original zombie, and shut themselves inside with a mass of undead hammering away from outside. Doctor Scott manages to restrain the zombie, and cuts into his chest cavity. Quinn and Holly reach the security doors, which Quinn breaks down with a flying kick. As Scott extracts the jindu, the zombies begin to break inside, wounding the female merc who accidentally shoots Dr. Adams, before being shot by her partner. The zombies stream in, attacking the remaining merc. Scott drops the jindu as Quinn and Holly burst inside. Quinn crushes the jindu and begins to destroy the zombies within, while Dr. Scott is attacked and literally "disarmed" by a pair of zombie soldiers before Quinn and Holly can shoot them down. Quinn approaches the bleeding Dr. Adams and asks after Judson, and she claims he was killed. As the pair leave the room, Holly shoots Adams before she can turn into one of the infected.

Leaving, they then find Judsons crucifix necklace and fear he has been killed, but spy a trash chute and open it to reveal Judson cowering inside. Happy to see him alive, they help him out, and make plans to finish off the infected for good. Quinn takes some C4 from the bases armory, and wires it to explode beneath the ammo-dump so as to take out the entire base. As he moves to find a vehicle for the group to escape with, he has Judson and Holly bait the zombies as close to the ammo-dump as possible. Quinn struggles to find a vehicle that is not damaged, as Judson and Holly narrowly escape a group of hungry undead they have drawn towards the ammo-dump, and activate the two-minute timer. They narrowly escaping, the base goes up in flames, and afterward, the trio scour the wreckage to make sure all the zombies are truly out of commission. With the undead finished, Quinn, Holly, and Judson walk away to plan their futures, with Holly saying, "this could be the beginning of a beautiful friendship."

==Cast==
* Dean Cain as Lt. Robert "Bobby" Quinn
* Guy Torry as Judson
* Susan Ward as Holly
* Peter Greene as Dr. Scott
* Ellie Cornell as Dr. Adams
* Kirk B. R. Woller as Major Bascom
* Colleen Camp as Mrs. Wisteria
* Armin Shimerman as coroner Flutie
* John Billingsley as Langdon
* Dean Haglund as "funeral home director"
* Ho-Sung Pak as "superstar merc"

==Production== Buffy the Vampire Slayer, where he played principal Snyder, the successor to a principal named Flutie), and a debate on the merits of various versions of James Bond by the soldiers Pvt. Connery and Pvt. Lazenby that mirrors dialogue by director and writer Kevin Smith. 

==Release==
The film made its debut on the Sci-Fi channel on December 16, 2006, and was later released to DVD in the US on April 10, 2007. In Australia it was released via Starz Home Entertainment in 2007, and had a theatrical release in 2008 in the UK via Fluid Entertainment. 

==Reception==
Travis Estvold of Boise Weekly wrote, "It probably wouldve done better as a 45-minute episode of The Outer Limits rather than as a feature film, except it may have even given that show a bad name."   Jon Condit of Dread Central rated it 2/5 stars and wrote, "By the time the closing credits rolled, Dead and Deader felt less like a new movie and more like a rejected mid-80s TV pilot".   Bloody Disgusting rated it 3.5/5 stars and wrote, "Truth be told for a low budget feature that made its debut on the Sci-Fi Channel it definitely has its moments."     Scott Weinberg of DVD Talk rated it 2.5/5 stars and wrote, "Its nothing but a low-budget, tongue-in-cheek mixture of Blade, 48 HRS., 28 Days Later, and any angry commando action flick you can imagine – but its also kinda fun, too."   Writing in The Zombie Movie Encyclopedia, Volume 2, academic Peter Dendle said, "The films promising opening deteriorates into buffoonery and yawny punch-lines." 

==References==
 

==External links==
*  
 

 
 
 
 
 
 
 
 
 
 
 