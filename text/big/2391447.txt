One of Our Dinosaurs Is Missing
{{Infobox film
| name =One of Our Dinosaurs is Missing
| image    = one_of_our_dinosaurs_is_missing_movie_poster.jpg
| caption  = Theatrical release poster Robert Stevenson Bill Walsh
| based on = The Great Dinosaur Robbery&nbsp;by  
| writer   = Bill Walsh
| starring = Peter Ustinov Helen Hayes Clive Revill Derek Nimmo
| music    = Ron Goodwin
| cinematography = Paul Beeson
| editing  = Peter Boita Walt Disney Productions Buena Vista Distribution
| released =  
| runtime  = 100 minutes
| country  = United States
| language = English
| budget   = 
}} Natural History Walt Disney David Forrest (pseudonym of David Eliades and Robert Forrest Webb).

==Plot==
Escaping from China with a microfilm of the formula for the mysterious "Lotus X", Lord Southmere, a Queens Messenger, is chased by a group of Chinese spies.
 Natural History Museum. Chinese spies follow him, so he hides the microfilm in the bones of one of the large dinosaur skeletons. He is relieved to meet his former nanny, Hettie, in the museum, and asks her to retrieve the microfilm. Southmere then faints and is captured by the Chinese, who tell Hettie and Emily (another nanny) that they are taking him to a doctor.

Hettie and Emily enlist other nannies to help them search. They hide in the mouth of the blue whale display until after closing time and then begin looking over the skeleton of an Apatosaurus (referred to in the film as a Brontosaurus, a synonym that was in popular use at the time). They are unsuccessful, and most have to return home to care for their children, but Hettie, Emily and their friend Susan remain to continue with the search. They are captured and taken to the spies London headquarters, underneath a Chinese restaurant in Soho. The nannies are locked up in the dungeon, with Lord Southmere. Fortunately, the nannies are able to outwit their captors and escape.
 steam lorry, Daimler limousine, but the nannies drive into a railway goods yard, onto a flat wagon at the back of a train, and are carried off to safety.

The nannies fail to find the microfilm on the skeleton. Meanwhile, back in London, Hetties two young charges, Lord Castlebury and his younger brother, Truscott, have been captured by the spies. They are taken to the museum and the chief spy retrieves the microfilm from the other large dinosaur, a Diplodocus skeleton. The two boys are allowed home and tell Nanny Hettie the news.

Realising that Lord Southmere is now in danger, Hettie organises a rescue. Hettie and her team of nannies invade the Chinese restaurant base and battle with the spies over Lord Southmere. Meanwhile, Emily and Susan return with the Apatosaurus and the lorry and bring the fight sequence to a shattering conclusion. Everything ends well and the secret of the mysterious "Lotus X" is finally revealed. It turns out that Lotus X is actually a recipe to Wonton soup, to which Southmere says that he tried to tell Wan that he was a businessman. Han then advertises the recipe and makes peace with the nannies.

==Cast==
 
 
*Derek Nimmo as Lord Southmere
*Hugh Burden as Haines
*Bernard Bresslaw as Fan Choy
*Helen Hayes as Hettie
*Joan Sims as Emily
*Deryck Guyler as Harris
*Peter Ustinov as Hnup Wan
*Clive Revill as Quon
*Molly Weir as Scots nanny
*Andrew Dove as Lord Castleberry
*Max Harris as Truscott
*Max Wall as Juggler
*Natasha Pyne as Susan
*Joss Ackland as B.J. Spence
*Arthur Howard as Thumley
*Roy Kinnear as Superintendent Grubbs
*Leonard Trolley as Inspector Eppers
*Joe Ritchie as Cabbie
 
 Percy Herbert as Mr. Gibbons
*Joan Hickson as Mrs. Gibbons
*John Laurie as Jock
*Angus Lennie as Hamish
*Jon Pertwee as Colonel
*Kathleen Byron as Colonels wife Lucy Griffiths as Amelia
*Aimée Delamain as Millicent
*John Bardon as Bookmaker
*Jane Lapotaire as Miss Prescott Richard Pearson as Sir Geoffrey
*Michael Elwyn as Haycock
*Anthony Sharp as Home Secretary
*Wensley Pithey as Bromley Frank Williams as Dr. Freemo Peter Madden as Sanders
*Erik Chitty as Museum guard
*Amanda Barrie as Mrs. B.J. Spence
 

==About the book==
The book from which the film was taken, The Great Dinosaur Robbery, was aimed at an adult audience by its authors, Robert Forrest Webb and David Eliades, and was set in New York. The authors, both very experienced UK national journalists and best-selling authors, extensively researched material in New York and were greatly assisted by the American Museum of Natural History which is situated alongside Central Park, and by the New York Police Department responsible for that area. The authors were disappointed that the humour of the film was aimed at a very much younger audience than that in the book which had been published, in several languages, extremely successfully throughout Europe, and also in Australia, New Zealand and the USA.

==Production==
 
*This film was made entirely on location in England at Elstree Studios and Pinewood Studios, along with some location shoots around Windsor, Berkshire.
*The Diplodocus skeleton model was later used in  , in the opening scenes in the Tunisian desert.

==External links==
*  
*  
*  
*  
*   - Time Out Film

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 