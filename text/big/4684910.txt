The Last Shot
 
{{Infobox Film
| name           = The Last Shot
| image          = Last shot poster.jpg
| image_size     = 
| caption        = Theatrical Release Poster
| director       = Jeff Nathanson
| producer       = Larry Brezner  David Hoberman
| writer         = Jeff Nathanson
| narrator       =  Evan Jones Tom McCarthy Glenn Morshower Jon Polito Troy Winbush
| music          = Rolfe Kent
| cinematography = John Lindley
| editing        = David Rosenbloom
| studio         = Touchstone Pictures Buena Vista Pictures
| released       = September 24, 2004
| runtime        = 93 minutes
| country        = USA English
| budget         = $8 million
| gross          = $464,000
| preceded_by    = 
| followed_by    = 
}}
 2004 comedy film starring Matthew Broderick, Alec Baldwin, Toni Collette, Tim Blake Nelson, Joan Cusack (uncredited), Tony Shalhoub, Buck Henry, Ray Liotta, Calista Flockhart and Ian Gomez. The movie is written and directed by Jeff Nathanson, who wrote Catch Me If You Can and The Terminal.

==Plot==

FBI agent Joe Devine (Alec Baldwin) has been assigned to coming up with an elaborate scheme to take down infamous mob boss John Gotti. He assumes the role of a Hollywood producer and tells all the right lies to enlist a stooge to help execute his sting. He finds unsuspecting wannabe writer/director Steven Schats (Matthew Broderick), whod do just about everything to get the chance to direct a feature. Schats falls for the pitch, but what Devine doesnt tell Schats is that the movie will never be made. 

Though Schats screenplay is titled Arizona, and the main character is supposed to euthanize herself in a Hopi cave at the end of the movie, he is so desperate to make the film that Devine convinces him to film it in Rhode Island. Devines target there is Tommy Sanz, who muscles in on the production. Devine records Sanz accepting a bribe for the International_Brotherhood_of_Teamsters|Teamsters approval of the production. Instead of ending the investigation at that point as the FBI expects, Devine plows ahead with the film production, because he has fallen in love with the movie business.

Devines mania leads him to pitch a three picture deal to his FBI superiors. He is convinced that he can ensnare more mobsters with a similar scheme, while also producing actual films. The FBI agrees to the idea, and Devine throws himself into production full tilt. Just as filming begins, the FBI arrests Gotti and puts an end to the production, against Devines wishes. The film jumps forward two years to the premiere of a movie based on the sting operation. Schats is working as a manager at a movie theater. Devine visits him and apologizes. He reveals that he has been working on a screenplay, and Schats gets excited about the pitch.

==Background== FBI sting Nasty Boys (Which, ironically, was about undercover police officers).  The grip truck ran into an elderly ladys car the first day of shooting in New Orleans and the crew wound up paying off the lady with hundreds peeled from a roll of bills.  The operation eventually led to indictments against five individuals, several of whom were convicted. The movie itself was shut down before serious filming started. Lewk and Levy have yet to make a full-length feature film.  They were, however, associate producers on The Last Shot and had cameo roles as Hollywood Boulevard Types. 

==Cast==
* Matthew Broderick as Steven Schats
* Alec Baldwin as Joe Devine
* Toni Collette as Emily French
* Calista Flockhart as Valerie Weston
* Ray Liotta as Jack Devine
* Tim Blake Nelson as Marshal Paris
* James Rebhorn as Abe White
* Tony Shalhoub as Tommy Sanz
* Stanley Anderson as Howard Schats (Ben Cartwright)
* W. Earl Brown as Willie Gratzo
* Ian Gomez as Agent Nance
* Buck Henry as Lonnie Bosco Evan Jones as Troy Haines Tom McCarthy as Agent Pike
* Glenn Morshower as Agent McCaffrey
* Jon Polito as Wally Kamin
* Troy Winbush as Agent Ray Dawson
* Russell Means as Himself
* Pat Morita as Himself
* Joan Cusack as Fanny Nash (uncredited) Robert Evans as Himself (uncredited)
* Judy Greer as Girl with Emily French at Movie Premiere (uncredited)
* Eric Roberts as Himself (uncredited)
* Shoshannah Stern as Stevens Girlfriend

==Reception==
 
At IMDb, the movie received 5.7 points out of 10 from over 3504 reviewers. 

==External links==
* 
*  
* 

==References==
 

 
 
 
 
 
 
 
 
 
 