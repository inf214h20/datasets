Burn!
{{Infobox film
 | name = Burn!
 | image = Original movie poster for the film Burn!.jpg
 | image_size = 220px
 | caption = English language theatrical poster
 | director = Gillo Pontecorvo
 | producer = Alberto Grimaldi
 | writer = Franco Solinas Giorgio Arlorio
 | starring = Marlon Brando Evaristo Márquez
 | music = Ennio Morricone
 | cinematography = Marcello Gatti Giuseppe Ruzzolini
 | editing = Mario Morra
 | studio = Europee Associate SAS Produzioni Europee Associati/Les Productions Artistes Associés
 | distributor = United Artists
 | released = 1969
 | runtime = 112 minutes (United States) 132 minutes (Restored) 
 | country = Italy France
 | language = Italian Spanish Portuguese
 | budget =
 | gross = 
 }} Portuguese colony William Walker, the famous United States filibuster (military)|filibuster, and while based on issues that Walker symbolically represented, the film itself is not based on the life of Walker. It was filmed in Cartagena de Indias. Music was composed by Ennio Morricone.

==Plot== Portuguese colony black slaves Great Britain wants to get economic control of the island, as it is an important sugar cane producer.
 latifundists friendly to Britain. To realize this project, William Walker persuades the black slaves to fight for their liberation from slavery.

José Dolores (Márquez) becomes the leader of the rebellion, while white political leaders assassinate the governor and establish a provisional government. After the overthrow of the Portuguese regime, British interests establish a corrupt puppet government, while Dolores is marginalized.  Though slavery had formally ended and the former slaves in theory had rights, a legal and property system was established that forced them to continue to work in the sugar cane plantations in even worse conditions than before.

William Walker leaves the island after the revolution. He comes back to Queimada 10 years later, this time to destroy the black political movement he had helped create. José Dolores has taken Walkers ideas to heart and is now leading a rebel army against the British puppet regime in Queimada. Walker is now working for the Royal Sugar Company, which organizes its own army and manipulates Queimada politics directly, including ordering the execution of one of its puppet presidents. After this, British troops land on the island, contributing artillery and crack infantry for fighting the rebels. Their main strategy is setting fire to the forests and sugar-cane fields to draw out the rebels—a strategy that achieves its goal but also destroys the reason for Britains interest in the island.

Eventually, the rebel army is defeated and Jose Dolores is captured. Dolores is offered his freedom in return for renouncing the rebellion. However, Dolores turns down this offer and is hanged, willingly sacrificing himself. Soon after that Walker is killed by a man in the street, seemingly as revenge for Doloress death.

==Cast==
* Marlon Brando as Sir William Walker
* Evaristo Márquez as José Dolores
* Renato Salvatori as Teddy Sanchez
* Dana Ghia as Francesca
* Valeria Ferran Wanani as Guarina
* Giampiero Albertini as Henry Thompson
* Carlo Palmucci as Jack
* Norman Hill as Shelton Thomas Lyons
* Joseph P. Persaud as Juanito
* Álvaro Medrano as Soldier
* Alejandro Obregón as English Major
* Enrico Cesaretti

==Themes== William Walker filibuster who led a privately backed invasion of Nicaragua in the 1850s. He was overthrown when his government threatened the interests of railroad magnate Cornelius Vanderbilt.  The chief protagonists name and personality are oblique references to this person, although they are of different nationalities. 
The plot is loosely based on events in the history of Guadeloupe. 
 The Arrangement once again with Elia Kazan, but chose instead to work on this film. He also had to turn down a major role in Ryans Daughter because of this films production problems. In his autobiography Brando claims, "I did some of my best acting in Burn!". 

==Reception==
The film received critical acclaim in the U.S. and abroad. Based on 11 reviews collected by Rotten Tomatoes, the film has an overall approval rating from critics of 82%.  By comparison, its 2004 re-release was given an average score of 72 out of 100, based on 4 reviews, by Metacritic, which assigns a rating based on top reviews from mainstream critics. 

Natalie Zemon Davis reviewed the film from a historians perspective and gave it high marks, arguing that it merges historical events that took place in Brazil, Cuba, Santo Domingo, Jamaica, and elsewhere. Natalie Zemon Davis, Slaves on Screen: Film and Historical Vision (2002) ch 3 

==See also==
* List of films featuring slavery

==References==
 

==Further reading==
* Davis, Natalie Zemon. Slaves on Screen: Film and Historical Vision (2002) ch 3
* Martin, Michael T., and David C. Wall, "The Politics of Cine-Memory: Signifying Slavery in the History Film,"  in Robert A. Rosenstone and Constantin Parvulesu, eds. A Companion to the Historical Film (Wiley-Blackwell, 2013), pp. 445-467.

==External links==
*  
*   by Amy Taubin
*   by John Bellamy Foster

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 