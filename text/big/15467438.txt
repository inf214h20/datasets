Giorgino
{{Infobox film| name = Giorgino
  | image =
  | caption =
  | director = Laurent Boutonnat
  | producer = Laurent Boutonnat
  | writer = Laurent Boutonnat   Gilles Laurent Christopher Thompson   Christian Gazio   Su Elliott   Janine Duvitski   Richard Claxton   John Abineri   Anne Lambton   Valérie Kaplanová
  | music = Laurent Boutonnat
  | cinematography =
  | editing =
  | distributor =
  | released = October 5, 1994 (France)
  | runtime = 177 min. English
  | budget = €12,000,000
  | followed_by =
    }}
 French film directed by Laurent Boutonnat. Giorgino   (Retrieved January 28, 2008) 

==Production==
* Title : Giorgino
* Direction : Laurent Boutonnat
* Screenplay : Laurent Boutonnat, Gilles Laurent
* Production : Laurent Boutonnat
* Music : Laurent Boutonnat
* Photo : Jean-Pierre Sauvaire
* Film editing : Laurent Boutonnat, Agnès Mouchel
* Costume : Carine Sarfati
* Country : France
* Format : Colours   - sound Dolby - 35&nbsp;mm
* Genre : Drama
* Running time : 177 minutes
* Released :  France : October 5, 1994

==Cast==
* Mylène Farmer : Catherine Degrâce
* Jeff Dahlgren : Giorgio Volli
* Jean-Pierre Aumont : Sébastien Degrâce, Catherines father
* Joss Ackland : The priest Glaise
* Louise Fletcher : The innkeeper
* Frances Barber : Marie
* Albert Dupontel : The crippled nurse Christopher Thompson, Christian Gazio : The young captain
* Su Elliott : Marthe
* Janine Duvitski : Josette
* Richard Claxton : Raoul
* John Abineri : Dr. Jodel
* Anne Lambton : Mother Raoul
* Valérie Kaplanová : The old woman

==Formats==
 
 

; Soundtrack (released on December 3, 2007) :
;; Edition Super Jewel Box
# "Ouverture" (3:52)
# "Le Vent et la Neige" (1:14)
# "La Route de Chanteloup" (1:05)
# "LOrphelinat"
# "Les Montagnes noires" (2:11)
# "En calèche" (2:19)
# "À Catherine" (1:26)
# "Giorgino theme" (3:37)
# "Levée du corps" (0:46)
# "LAbbé Glaise" (1:45)
# "Giorgio et les Enfants" (3:09)
# "La Nursery" (3:09)
# "Retour à lorphelinat" (2:30)
# "LArmistice" (1:42)
# "La Valse des baisers"
# "Giorgio et Catherine" (3:49)
# "Docteur Degrâce" (4:31)
# "Morts pour la France" (2:28)
# "Les Femmes dans léglise" (1:41)
# "Les Funérailles" (2:37)
# "Petit Georges" (1:21)
# "Sombres Souvenirs" (2:34)
# "Le Christ et les Cierges" (1:41)
# "Menteur" (3:30)
# "Le Marais" (1:50)
# "Final" (7:43)

 

; Collector Box Limited edition (300 ex)  - 2 DVD with 10 promotional post-card (released on December 5, 2007) :

* DVD 1 : Remasterised film
# Original version with subtitles (in English)
# French version
# French subtitles

* DVD 2 : Bonus
# Making of : interviews of Mylène Farmer, Jeff Dahlgren, Laurent Boutonnat
# Drawings of production
# Photos
# Teaser and the 1994 trailer + the 2007 unpublished trailer
# Booklet (48 pages) : 1994 interviews of Jeff Dahlgren, Mylène Farmer, Laurent Boutonnat, unpublished photos, various documents

; DVD (released in September 2008) 

 

==References==
 

 

 
 
 


 