Terror Among Us
{{Infobox Film
| name           = Terror Among Us
| image          = 
| caption        = 
| director       = Paul Krasny
| producer       = David Gerber, James H. Brown
| writer         = Dallas Barnes, JoAnne Barnes
| starring       = Don Meredith, Sarah Purcell, Jennifer Salt
| music          = Allyn Ferguson
| cinematography = Robert B. Hauser
| editing        = Richard Freeman
| distributor    = Columbia Pictures 1981
| runtime        = 95 min
| country        = United States English
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
 1981 made-for-TV movie directed by Paul Krasny. It first aired January 12, 1981.  The script was co-written by Dallas and JoAnne Barnes.  The story bears many similarities to Dallas Barnes 1976 book Yesterday Is Dead.

==Plot==

A police sergeant and a parole officer endeavor to stop a rapist-on-parole before he can follow through his threats on five women who testified against him years earlier.
(The movie has become a popular cult film among bondage enthusiasts due to the lengthy scenes involving tightly cleave-gagged and bound women.) 
==Cast ==
*Don Meredith as Sergeant Tom Stockwell
*Sarah Purcell as Jennifer
*Jennifer Salt as Connie Paxton
*Kim Lankford as Vickie Stevens
*Sharon Spelman as Sara Kates
*Rod McCary as Gates
*Elta Blake as Beth
*Pat Klous as Cathy
*Jim Antonio as Doctor
*Virginia Paris as Mrs. Quinn Tracy Reed as Barbara
*Ted Shackelford as Delbert Ramsey
*Stephen Keep as Roger Shiel

==References==
 
 

==External links==
* 
* 

 
 
 
 
 


 