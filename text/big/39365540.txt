Prometheus (1998 film)
{{Infobox film image           = File:Prometheus (1998 film) poster.jpg caption         = Poster of the film
| name           = Prometheus
| writer         = Tony Harrison
| screenplay     = Tony Harrison
| based on       = 
| budget         = 1.5 million pounds. 
| released       = 1998
| runtime        = 130 minutes
| country        = Britain
| language       = English and Ancient Greek
}} English poet and playwright Tony Harrison, starring Walter Sparrow in the role of Prometheus. The film-poem examines the political and social issues connected to the fall of the working class in England, amidst the more general phenomenon of the collapse of socialism in Eastern Europe, using the myth of Prometheus as a metaphor for the struggles of the working class and the devastation brought on by political conflict and  unfettered industrialisation. It was broadcast on Channel 4 and was also shown at the Locarno Film Festival. It was used by Harrison to highlight the plight of the workers both in Europe and in Britain. His film-poem begins at a post-industrialist wasteland in Yorkshire brought upon by the politics of confrontation between the miners and the government of Margaret Thatcher.       {{cite news|title=Beats of the heart|url=http://www.guardian.co.uk/books/2007/mar/31/poetry.tonyharrison|accessdate=12 May 2013|date=31 March 2007|author=
Maya Jaggi|newspaper=The Guardian}}           It has been described as "the most important artistic reaction to the fall of the British working class" at the end of the twentieth century.

==Influence== Prometheus Unbound. Harrison actually started the planning of his film at the Baths of Caracalla in Rome so that he could be in the same city as Shelley when he was writing his drama. 

==Plot==
Walter Sparrow plays the role of an elderly, emphysema-laden Yorkshire miner who is about to retire. In a bleak, post-industrialist landscape, the old miner meets a boy who is reciting a poem about Prometheus. The youth is played by Jonathan Waintridge. The miner eventually ends up in a depleted local cinema and, though sick with emphysema, he defiantly lights-up a cigarette. With the old miner at the theatre, the run-down projection equipment suddenly comes to life and projections appear on the old screen.    

The person directing these projections is   from Prometheus Bound.  Eventually, the old man starts seeing a huge golden statue of Prometheus, nicknamed Goldenballs in the film.  It is revealed that  the statue was made by melting the bodies of Yorkshire miners.    

The film traces the trip the statue takes in the back of a truck through Eastern Europe,  revisiting the horrors of World War II European History in places such as Auschwitz, Dresden and the Polish industrial city of Nowa Huta. As they pass from these places, Hermes tries to have the locals denounce Prometheus for their condition and all the ills he has brought them with his gift, rather than Zeus. Sometimes he succeeds, but not in Nowa Huta where the locals refuse to denounce Prometheus much to the chagrin of Hermes, who then lashes out at them through rhyming verses.     In the end, the old coal miner, who represents the spirit of Prometheus in the film,  arrives in Greece where he speaks about the powerful impact of fire on humankind.    

==Analysis and reception==
In Reception Studies it is mentioned that Harrison in his film has metaphorically "linked the gnawed liver of Prometheus to the eagle of genocide and the blackened lungs of the coalminers" and it was the bodies of the Yorkshire coalminers which were consumed by fire, as in Émile Zolas Germinal (novel)|Germinal, that were used in the casting of the statue of Harrisons Prometheus. 

Edith Hall has written that she is convinced that Harrisons Prometheus is "the most important artistic reaction to the fall of the British working class" at the end of the twentieth century,   and considers it as "the most important adaptation of classical myth for a radical political purpose for years" and Harrisons "most brilliant artwork, with the possible exception of his stage play The Trackers of Oxyrhynchus". 
 concrete to abstract labor, and thence to symbolic capital" highlights Harrisons use of imagery which involves capitalist and Marxist economic theories of the twentieth century. 

Edith Hall also compares the similarities between Harrisons work and Theo Angelopoulos Ulysses Gaze in that both works feature nonlinear-chronological movement through Eastern European places characterised by "monumental statuary" and classifies Harrisons Prometheus as a threnody although certain elements relating to the imagery of the film and its adaptation of classical themes also bring it closer to the film techniques of Michelakis. According to Lorna Hardwick, the film-poem attempts a new type of redemptive effect by using art as a "mirror to horrors" saving the audience from turning to stone, quoting Rowlands comments about Harrisons film-poem The Gaze of the Gorgon. 

Helen Morales remarks that the Yorkshire coal miner "embodies the spirit of Prometheus" and that his dialogues with Hermes, "Zeus cruel henchman", are "brilliant articulations of oppression and defiance". 

Carol Dougherty mentions that in Harrisons 1995 play The Labourers of Herakles there is a scene where Hercules, while lying on his own funeral pyre, delivers a speech about the power of fire in a manner reminiscent of Harrisons treatment of the same theme during his 1998 film Prometheus.   

==References==
 

==External links==
*  

 
 
 
 
 
 