The Private Life of Louis XIV
{{Infobox film
| name = The Private Life of Louis XIV
| image =
| image_size =
| caption =
| director = Carl Froelich
| producer = Carl Froelich  
| writer =  Karl Peter Gillmann   Wolfgang Hoffmann-Harnisch   Carl Froelich
| narrator =
| starring = Renate Müller   Eugen Klöpfer   Maria Krahn   Maly Delschaft
| music = Alois Melichar 
| cinematography = Reimar Kuntze 
| editing =  
| studio = 
| distributor = 
| released = 8 August 1935 
| runtime = 
| country = Nazi Germany  German 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}}
The Private Life of Louis XIV or Liselotte of the Palatinate (German:Liselotte von der Pfalz) is a 1935 German historical film directed by Carl Froelich and starring Renate Müller, Eugen Klöpfer and Maria Krahn.  The films English language release title is a reference to the hit British film The Private Life of Henry VIII (1933).
 Louis XIV. She was also the subject of a 1966 biopic in which she was played by Heidelinde Weis.

==Cast==
*  Renate Müller as Liselotte von der Pfalz 
* Eugen Klöpfer as Kurfürst Karl Ludwig, ihr Vater 
* Maria Krahn as Die Kurfürstin, ihre Mutter 
* Maly Delschaft as Luise von Degenfeld, die Raugräfin  
* Edith Oß as Anneliese von Degenfeld  
* Ida Wüst as Sophie, Herzogin von Hannover  
* Renate Howe as Prinzessin Charlotte  
* Maria Seidler as Fräulein von Uffeln  
* Eduard Bornträger as Haushofmeister des Kurfürsten 
* Michael Bohnen as Ludwig XIV., König von Frankreich  
* Hans Stüwe as Philipp von Orleans, sein Bruder  
* Else Ehser as Maria Theresa, Königin von Frankreich  
* Petra Unkel as Der Dauphin  
* Dorothea Wieck as Madame de Maintenon  
* Lothar Körner as Kriegsminister Louvois  
* Alexander Golling as General Mélac  
* Hans Adalbert Schlettow as Befehslhaber der französischen Truppen in Heidelberg  
* Aribert Wäscher as Chevalier de la Lorraine  
* Heinz von Cleve as Roland de Saint Cour 
* Maria Reisenhofer as Die Äbtissin von Maubuisson  
*  Valy Arnheim 
* Hilde Hildebrand as Duchesse de Montespan 
* Franz Klebusch 
* Fritz Lafontaine
* Maria Meissner as Marquise de la Valliere  
*Anneliese Würtz

== References ==
 

== Bibliography ==
* Hake, Sabine. Popular Cinema of the Third Reich. University of Texas Press, 2001.

== External links ==
*  

 

 
 
 
 
 
 
 
 
 
 
 
 


 