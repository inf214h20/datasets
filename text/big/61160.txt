The Barretts of Wimpole Street (1934 film)
 
{{Infobox film
| name           = The Barretts of Wimpole Street
| image          = WimpoleStreet.jpg
| image size     =
| caption        = Theatrical release poster Sidney Franklin
| producer       = Irving Thalberg	 Rudolf Besier
| starring       = Norma Shearer   Fredric March   Charles Laughton
| music          = Herbert Stothart
| cinematography = William H. Daniels	
| editing        = Margaret Booth	
| distributor    = Metro-Goldwyn-Mayer
| released       = September 21,  
| runtime        = 110 minutes
| country        = United States
| language       = English
| budget         = $820,000 Glancy, H. Mark "When Hollywood Loved Britain: The Hollywood British Film 1939-1945" (Manchester University Press, 1999) 
| gross          = $1,258,000 (Domestic earnings)  $1,085,000 (Foreign earnings)   	
| preceded_by    = 
| followed_by    = 
}}
 Elizabeth Barrett Sidney Franklin.

This film was based upon the famous 1930 play, The Barretts of Wimpole Street, starring Katharine Cornell.

==Plot==
The bulk of the story takes place in the lavish home of Edward Barrett (Charles Laughton) and his adult children. Upstairs, Elizabeth (Norma Shearer), called "Ba," the oldest girl, consults with her doctor. She is recovering from an undisclosed illness and is extremely weak - standing and walking are painful - but the doctor advises that a full recovery is possible.
 porter  which she has been advised by the doctor to take is making her feel worse, the doctor takes her off it and puts her on hot milk instead, but Edward forces her to continue drinking porter. His tyranny over the boys is more sketchily shown, but clearly, they are just as terrified of him as the girls.

Meanwhile, Henrietta is interested in marrying her brothers friend Surtees (Ralph Forbes), who has a promising career in the military. But she discourages him. She cannot see any way around her insanely  possessive father, who has forbidden any of his children - including his six boys - to marry.

Robert Browning (Fredric March) arrives in a snowstorm, and immediately sweeps Ba off her feet. Her poetry has caused him to fall madly in love with her. When she expresses her fear that death may be at hand, he laughs it off and encourages her to seize the day. When he leaves her room, she rises from her settee for the first time and drags herself to the window so she can see him as he departs.

Months pass; Ba is able to walk slowly and to go downstairs to see Robert. Edward warns her not to overdo it and tells her its just a temporary recovery. The doctors prescribe a trip to Italy for the winter. Edward is considering it, when chatty Cousin Bella (Marion Clayton) spills the beans that Bas relationship with Robert isnt just a meeting of minds. Edward immediately vetoes the trip and leaves the house, saying hes got another idea that may help her get the fresh air and sunshine she needs without having to leave the country. While hes out, Robert and Ba meet in Kensington Gardens. He assures her that he will take her himself to Italy and that she should be ready by the end of the month. She says shell think about it.

Edwards plan turns out to be a scheme to get Ba out of London, away from friends and activity (all for the good of her health, of course). He writes, bidding her tell her siblings that hes about to sell the house and move them all out to Surrey, six miles from the nearest railway station. Ba relays the message but doesnt tell Henrietta, who is now firmly committed to Surtees.

  with Norma Shearer and Fredric March.]]
Unexpectedly, Edward returns early, catching Henrietta and Surtees modeling his dress uniform for Ba in her room. Brutally grasping her wrists, he forces Henrietta to confess her secret affair. Denouncing her as a whore, he makes her swear on the Bible never to see Surtees again and to lock herself in her room. Ba witnesses all of this. When Edward starts in on the blame game against her, for aiding and abetting Henriettas illicit relationship, she reveals her true feelings, smashing the facade that has allowed her father to keep a dictatorial control over every minute of her waking life - she says that, far from obeying him out of love, she hates him, and denounces him as a tyrant. Unrepentant, her father walks out of the room, saying she can send for him when she has repented of her sins.

Ba conspires with her maid Wilson to let Robert know she will elope with him and Wilson is coming along. Henrietta, when set free, runs to Ba and exclaims that she will break her Bible oath, lie to her father if necessary, and run away with Surtees if she must.

Edward enters and dismisses Henrietta to speak to Ba alone. He opens up to her and confesses his real feelings and the motivation for his "dragon" behavior. Edward apparently thinks of himself as having a sex addiction, and although the language in this scene is extremely euphemistic, we can gather that he tyrannized his wife as well, and that some of the children may actually have been conceived through marital rape. Edward now suppresses all his desires, equating all sex with sin, and he wants his children never to fall prey to carnal passion. As he goes into detail about how he wants Ba all to himself, to have her confide in him all her thoughts and feelings, he embraces her and actually comes close to making a sexual pass. Horrified by his inhuman behavior, Ba repulses him, and cries out that he must leave her. He apologizes and leaves, saying hell pray for her.

Ba summons Wilson, puts on her cloak and hat, takes her little dog Flush and departs. As the two sneak down the stairs, we hear Edward saying grace over dinner. A few moments later, we hear the hysterical laughter of Bas sister Arabel (Katharine Alexander). The boys rush upstairs, followed by Henrietta, to find that Ba has left one letter for each of the siblings and Edward. Edward reads his letter and staggers to the window. As if drunk, he insanely mutters "Ill have her dog," and bids his son Octavius take Flush to the vet and have her killed. Octavius cries out that it is unjust, and Henrietta triumphantly drives the final blow; "In her letter to me Ba writes that she has taken Flush with her..." The film closes with a brief scene of Elizabeths and Roberts marriage, with Wilson as a witness and Flush waiting patiently by the church door.

==Cast== Elizabeth Barrett
*Fredric March as Robert Browning
*Charles Laughton as Edward Moulton-Barrett
*Maureen OSullivan as Henrietta Barrett
*Katharine Alexander as Arabel Barrett
* Vernon Downing as Octavius Barrett
*Ralph Forbes as Captain Surtees Cook
*Marion Clayton as Bella Hedley
*Ian Wolfe as Harry Bevan
*Ferdinand Munier as Dr Chambers Una OConnor as Wilson
*Leo G. Carroll as Dr Waterlow

==Depiction of the Brownings courtship==
Although the names of the individuals involved are correct in the play and films, by definition motivations of individuals cannot be known. The numerous love letters that Robert and Elizabeth exchanged before their marriage, however, can give readers a great deal of information about this famous courtship in their own words. The correspondence was well underway before they ever met in person, he having admired the collection Poems that she published in 1844. He opens his first letter to her, I love your verses with all my heart, dear Miss Barrett, and a little later in that first letter he says I do, as I say, love these books with all my heart—and I love you too (January 10, 1845). 
 Edward Barretts behavior in disinheriting any of the children who married seems bizarre, there is no evidence of his being sexually aggressive toward any of the family members.   While all overt suggestions of incest were removed from the script, Charles Laughton, who played Edward, said "They can censor it all they like, but they cant censor the gleam out of my eye". 

==Reception==
The film was a big hit at the box office. 

===Box Office===
According to MGM records the film earned $1,258,000 in the US and Canada and $1,085,000 elsewhere resulting in a profit of $668,000.  . 

==Adaptations==
 Keith Baxter in his film debut. 

Both of the films were released by Metro-Goldwyn-Mayer|MGM.

==References==
 

==External links==
 
* 
*  
*  

 

 
 
 

 

 
 
 
 
 
 
 
 
 
 
 