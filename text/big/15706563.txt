The Tiger's Trail
 
 
{{Infobox film
| name           = The Tigers Trail
| image          = Ruth Roland 1919.jpg
| caption        = Ad for film Robert Ellis Paul Hurst
| producer       = 
| writer         = Charles Logue Arthur B. Reeve Frank Leon Smith
| starring       = Ruth Roland George Larkin
| cinematography = 
| editing        =  Astra Films
| released       =  
| runtime        = 15 episodes
| country        = United States 
| language       = Silent (English intertitles)
| budget         = 
}}
 adventure Serial film serial Robert Ellis, Paul Hurst. A "fragmentary print" from the serial survives.   

==Plot==
As described in a film magazine,  Grim Gordon (Strong) is in possession of the Tiger Idol, stolen from a religious sect of Hindu Tiger Worshipers on an East Indian island which he and Peter Strong and Colonel Boyd visited years earlier. The two latter men were killed, but Belle Boyd (Roland), daughter of the Colonel, is alive and has part of the "Pact of Three," a document torn into three parts which shows the location of a treasure discovered during the expedition. Gordon has a pitchblende mine in the western United States, and among the workers are Hindus and Tiger Worshipers. Upon her arrival from an eastern school, Belle Boyd, ward of Gordon, is attacked by a gang of outlaws headed by Bull Shotwell (Kohler), but her life is saved by Jack Randall (Larkin), a mining engineer. Jack is employed by Gordon, but helps the heroine Belle in outwitting the evil forces surrounding her that are attempting to obtain her portion of the torn Pact of Three. In one episode Belle is put into a cage with a live Bengal tiger, and in others she is the subject of several kidnapping attempts.

==Cast==
* Ruth Roland as Belle Boyd
* George Larkin as Jack Randall
* Mark Strong as Randolph Gordon / "Grim" Gordon
* Harry Moody as Tiger Face
* Fred Kohler as "Bull" Shotwell George Field as Salonga
* Easter Walters as Hilda, the Spy
* Bud Osborne as a henchman
* Chet Ryan as a juvenile
* Rose Dione as a Dance Hall Queen

==Production== Hindu tiger worshiping sect and western outlaws, was based on The Long Arm by C. A. Logue.    (SilentEra.com states that the serial was adapted by Gilson Willets from The Long Arm by Arthur B. Reeve). 

Real tigers were used in filming.  The serial includes the "famous scene" of a human chain formed from the roof of a train, which enables the criminals to steal a valuable parcel from Ruths compartment. 

==Chapter titles==
# The Tiger Worshippers
# The Glowing Eyes
# The Human Chain
# Danger Signals
# The Tiger Trap
# The Secret Assassin
# The Flaming Waters
# Danger Ahead
# The Raging Torrent
# Bringing In The Law
# In The Breakers
# The Two Amazons
# The False Idol
# The Mountain Hermit
# The Tiger Face

==References==
 

==External links==
* 
* 

 
 

 
 
 
 
 
 
 
 
 