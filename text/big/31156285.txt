Naadan Premam
{{Infobox film 
| name           = Naadan Premam
| image          =
| caption        =
| director       = Crossbelt Mani
| producer       = N.Vishweshwaraiah PS Gopalakrishnan
| writer         = SK Pottakkad Thoppil Bhasi (dialogues)
| screenplay     = Thoppil Bhasi Madhu Sheela Prema
| music          = V. Dakshinamoorthy
| cinematography = P Ramaswami
| editing        = Chakrapani
| studio         = Sakthi Enterprises
| distributor    = Sakthi Enterprises
| released       =  
| country        = India Malayalam
}}
 1972 Cinema Indian Malayalam Malayalam film, Prema in lead roles. The film had musical score by V. Dakshinamoorthy.   

==Cast== Madhu
*Sheela
*Adoor Bhasi Prema
*Sankaradi
*T. R. Omana
*Bahadoor
*K. P. Ummer Ragini
*S. P. Pillai

==Soundtrack==
The music was composed by V. Dakshinamoorthy and lyrics was written by P. Bhaskaran. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Cheppum Panthum || K. J. Yesudas || P. Bhaskaran || 
|-
| 2 || Kanni Nilaavu || P Susheela || P. Bhaskaran || 
|-
| 3 || Mayangaatha Ravukalil || LR Eeswari || P. Bhaskaran || 
|-
| 4 || Paaril Sneham || K. J. Yesudas || P. Bhaskaran || 
|-
| 5 || Panchaarakkunnine || K. J. Yesudas || P. Bhaskaran || 
|-
| 6 || Undanennoru Raajaavinu || P Jayachandran || P. Bhaskaran || 
|}

==References==
 

==External links==
*  

 
 
 

 