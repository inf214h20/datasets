Harvey (1996 film)
{{Infobox film
| name           =Harvey
| image          =George Schaefers Harvey.jpg
| image_size     =240px
| caption        = George Schaefer
| producer       = Mary Chase (play),  Joseph Dougherty (teleplay)
| starring       =Harry Anderson Leslie Nielsen Swoosie Kurtz 
| music          =
| cinematography =
| editor       =
| distributor    =
| released       = 1996
| runtime        = 120 minutes
| country        = United States
| language       = English
| budget         =
}} American television George Schaefer. Mary Chases play of pooka named Harvey—in the form of a six-foot rabbit only he can see.  Anderson plays the role of the eccentric Elwood P. Dowd. The 1996 television version for CBS was adapted for television by Joseph Dougherty.

==Cast==
*Harry Anderson  as Elwood P. Dowd
*Leslie Nielsen as Dr. Chumley
*Swoosie Kurtz  as Veta Simmons
*Jonathan Banks as C.J. Lofgren, cab driver
*Jessica Hecht  as Miss Ruth Kelly
*William Schallert  as Judge Gaffney
*Lisa Akey as Myrtle Mae Simmons
*Jim OHeir 	as Duane Wilson
*Robert Wisden 	as Dr. Sanderson
*Lynda Boyd as Mrs. Chumley
*Alex Ferguson as Zabladowski
*Sheila Moore  as Mrs. Chauvenet
*Sheelah Megill	as Miss Tewksbury
*Robin Kelly as Mrs. Greenwalt
*Ingrid Tesch as Girl Reporter

==Production==
The film was shot in Vancouver, British Columbia, Canada.

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 


 
 