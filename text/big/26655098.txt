Three Men in a Boat (1933 film)
{{Infobox film
| name = Three Men in a Boat
| image =
| image_size =
| caption =
| director = Graham Cutts
| producer = Basil Dean 
| writer = Jerome K. Jerome (novel)   Reginald Purdell   D.B. Wyndham-Lewis 
| narrator = William Austin   Edmund Breon   Billy Milton   Davy Burnaby 
| music = Ord Hamilton Robert Martin
| editing = 
| studio = Associated Talking Pictures  Associated British 
| released = May 1933 
| runtime = 60 minutes
| country = United Kingdom English 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}} British comedy William Austin, Edmund Breon, Billy Milton and Davy Burnaby.  It is based on the novel Three Men in a Boat by Jerome K. Jerome which depicts three men and a dogs adventure during a boat trip along the River Thames.

==Cast== William Austin as Harris
* Edmund Breon as George
* Billy Milton as Jimmy
* Davy Burnaby as Sir Henry Harland
* Iris March as Peggy
* Griffith Humphreys as Sergeant
* Stephen Ewart as Doctor
* Victor Stanley as Cockney
* Frank Bertram as Fisherman
* Sam Wilkinson as Police Constable
* Winifred Evans as Lady Harland

==References==
 

==Bibliography==
* Low, Rachael. Filmmaking in 1930s Britain. George Allen & Unwin, 1985.
* Perry, George. Forever Ealing. Pavilion Books, 1994.

==External links==
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 


 