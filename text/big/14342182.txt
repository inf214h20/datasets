Strike (2006 film)
{{Infobox film
| name               =Strike
| image              =Strike (2006 film) poster.jpg
| writer             =Sylke Rene Meyer Andreas Pflüger
| producer           =Jürgen Haase
| starring           =Katharina Thalbach Andrzej Chyra Dominique Horwitz
| director           =Volker Schlöndorff
| distributor        =Provobis Film/Berlin
| released           =September 11, 2006 (Toronto Film Festival) February 19, 2007 (Germany) February 23, 2007 (Poland)
| runtime            =111 minutes
| country            =Poland
| language           =Polish
| budget             =
| music              =
}}
 German group, directed by formation of Solidarity. labor organizing Lenin Shipyard in Gdańsk, Poland.
 communist Poland of the early Sixties (DVD chapters 1-4), then following events leading to the Polish 1970 protests (chapters 5-10), and finally the early Eighties including the dedication of the Monument to the Fallen Shipyard Workers of 1970, the Gdańsk Agreement, and Martial law in Poland (chapters 11-15).

The character of Agnieszka is loosely based on at least two women, the crane operator Anna Walentynowicz and the diminutive shipyard nurse Alina Pienkowska with invented or distorted facts.    

== Plot ==
Agnieszka Kowalska is a hardworking welder in the Lenin Shipyard in Gdansk. She is honored as a heroic worker, “activist of the Year” award for being a superquota worker. This is her tenth time winning the award, so she is given a television. She even gives a speech of how she came to the shipyard in 1950 with nothing, and now she has a profession and a home. She truly seems thankful for what the shipyard has given her. However after the official ceremony, other women workers confront her for working so hard and they are clearly not even trying to beat the quotas set forth by the Party.

At a meeting of Party and union members, Agnieszka tries to argue for a longer lunch break but the officials refuse to budge, especially Sobecki. Agnieszka is clearly frustrated by the intractability of the officials and their lack of compassion for tough working conditions at the shipyard. Independently, she organizes bringing soup to workers so they will have enough time to eat. Sobecki confronts her, but she mocks him after he threatens to report her.

Agnieszka decides that she is willing to take a pay cut and switch jobs with Mateusz, a crane operator. She learns to read and write, growing closer with her new neighbor Kazimierz. She eventually passes the exam, becoming the first female crane operator. 

A few months later, Agnieszka is diagnosed with kidney cancer. She is distraught, unsure of what will happen to her young son Krystian after she dies. Kazimierz agrees to raise Krystian as his own son, and Kazimierz soon marries Agnieszka. However on their honeymoon, Kazimierz suddenly dies of a heart attack, leaving Agnieszka a widow.
During a difficult time, a Party official wants Agnieszka to ask the workers to go even faster to meet the shipyard deadlines. She refuses, because she knows everyone is being overworked and is terribly exhausted. Everyone has been doing overtime for weeks. Agnieszka is so frustrated, she even tells the official to kiss her ass. The official tells all of the workers to get back to work, even threatening to fire them if they do not keep going. Due to the exhaustion, there is an accidental fire in which multiple men are badly burned. Agnieszka does her best to help pull men out of the fire with her crane but there are 21 workers killed including her friend Mateusz. Refusing to accept culpability, the shipyard director decides to withhold all pensions or payments to the surviving dependents. The shipyard and its union even forge timecards to hide the extreme overtime they were making workers do. However, they offer to give Agnieszka her overtime if she drops talk of the forged timecards. She renounces Bochnan and curses him. 

Krystian is growing up;he has a girlfriend, and is doing well enough in school to go to a Polytechnic. However, Agnieszka’s defiance of the Party and shipyard union is hurting her family politically and may affect her son’s ability to get into a good school. It is revealed that Sobecki is in fact Krystian’s father. Agnieszka goes to his office and begs Sobecki for help. He agrees, if he can meet his son. Out of desperation, her son has enlisted so that afterwards he may go to a polytechnic. 

The next scene takes place during the 1970 strikes, in which workers were killed. Agnieszka ia arrested and beaten. She returns home to find her son in the uniform of those who had just been mistreating her. Krystian reveals he knows about his true father, and Agnieszka tells him the story of how she came to know Sobecki. 

The film fast forward to 1978. Sobecki’s secretary recruits Agnieszka to join an underground newspaper, Robotnik. Very quickly after this, Pope John Paull II, a Pole, is elected as the new pope. Agnieszka helps to write and then distribute these underground newsletters to try and educate workers about what is really going on in the shipyards. The shipyard officials do not approve of her new activities, so she is put on lockdown during her shifts so she cannot distribute more pamphlets. They finally find a convincing reason for firing her, theft for not returning her deceased husband’s trumpet, and so they fire her.In response to her firing, the shipyard workers all go on strike. The strikes snowball, with many other shipyards and even streetcar drivers striking in solidarity with the Lenin shipyard workers. Talks are held, and Leszek agrees top simple compromises that only affect the Lenin shipyard. Agnieszka, makes a speech and convinces Leszek to lead the workers in continuing another strike in solidarity with the other workers who had helped them win the concessions. Archive footage is played of the 21 Accrods, and Solidarity is born. 


== Cast ==
* Katharina Thalbach − Agnieszka Kowalska-Walczak
* Andrzej Chyra −  Leszek
* Andrzej Grabowski − Henryk Sobecki
* Wojciech Pszoniak − Kamiński
* Rafael Remstedt − Krystian
* Dominique Horwitz − Kazimierz Walczak
* Dariusz Kowalski − Bochnak
* Krzysztof Kiersznowski − Mateusz
* Wojciech Solarz − Krystian
* Maria Maj-Talarczyk − Chomska
* Ewa Telega − Mirka
* Marta Straszewska − Maria
* Barbara Kurzaj − Elwira
* Joanna Bogacka − Szymborska Henryk Gołębiewski − Marek
* Magdalena Smalara − Renata
* Wojciech Kalarus − Nauczyciel
* Bogusław Spodzieja − Andrzej

==External links==
* 
* 
* 
*  , a documentary film by Sylke Rene Meyer (2002)    

==References==
 

 

 
 
 
 
 
 
 
 
 


 
 