After the Fox
 
{{Infobox film
| name           = After the Fox
| image          = After the fox544.jpg
| image_size     = 225px
| caption        = Theatrical release poster by Frank Frazetta
| director       = Vittorio De Sica John Bryan
| writer         = Neil Simon Cesare Zavattini
| starring       = Peter Sellers Britt Ekland Lydia Brazzi Paolo Stoppa Victor Mature Tino Buazzelli Vittorio De Sica 
| music          = Burt Bacharach Piero Piccioni 
| cinematography = Leonida Barboni Russell Lloyd
| distributor    = Delgate / Nancy Enterprises United Artists
| released       =  
| runtime        = 103 min
| country        = Italy United Kingdom English Italian Italian
| budget         = 
| gross          = $2,296,970  (rentals)  
| preceded_by    = 
| followed_by    = 
}}

After the Fox ( ) is a 1966 British–Italian comedy film  starring Peter Sellers and directed by Vittorio De Sica. The screenplay is in English, by Neil Simon and De Sicas longtime collaborator Cesare Zavattini. 
 Hindi as Tees Maar Khan.

==Plot==
The story begins in   is Aldo Vanucci (Peter Sellers), also known as The Fox, a master criminal with a talent for disguise.

But Vanucci is in prison. He knows about the smuggling contract but is reluctant to accept it because he does not want to disgrace his mother and young sister Gina (Britt Ekland). But when his three sidekicks inform him that Gina has grown up and does not always come home after school, an enraged, over-protective Vanucci vows to escape. He succeeds by impersonating the prison doctor and convincing the guards that Vanucci has tied him up and escaped. The guards capture the real doctor and bring him face to face with Vanucci, who flees with the aid of his gang. Vanucci returns home where his mother tells him that Gina is working on the Via Veneto. Vanucci takes this to mean that Gina is a prostitute. Disguised as a priest, Aldo sees Gina, who is provocatively dressed, flirting and kissing a fat, middle-aged man. Aldo attacks the man, but it turns out that Gina, who aspires to be a movie star, is merely acting in a low-budget  film. Aldo’s actions cost her the role, but he realizes that the smuggling job will make his family’s life better. He makes contact with Okra and agrees to smuggle the gold into Italy for 50 percent of the take. Meanwhile, two policemen are constantly on Vanucci’s trail, and he uses several disguises and tricks to throw them off. After seeing a crowd mob an over-the-hill American matinee idol, Tony Powell (Victor Mature), it strikes Vanucci that movie stars and film crews are idolized and have free rein in society. This idea forms the basis of his master plan.
 Italian neo-realist director named Federico Fabrizi. He plans to bring the gold ashore in broad daylight as part of a scene in an avant-garde film. To give the picture an air of legitimacy, he cons the vain Tony Powell to star in the film, which is blatantly titled The Gold of Cairo (a play on The Gold of Naples, a film De Sica directed in 1954). Powell’s agent, Harry (Martin Balsam) is suspicious of Fabrizi, but his client wants to do the film. Fabrizi enlists the star struck population of Sevalio, a tiny fishing village, to unload the shipment. However, when the boat carrying the gold is delayed, Fabrizi must actually shoot other scenes for his faux film to keep up the ruse. The ship finally arrives and the townspeople unload the gold. But Okra double-crosses Vanucci and, using a movie smoke machine for cover, drives off with all of the gold. A slapstick car chase ensues, ending with Okra, Vanucci and the police crashing into each other. Vanucci, Tony Powell, Gina, Okra, and the villagers are accused of being co-conspirators. As evidence against them, Vanucci’s "film" is shown in court. An Italian film critic leaps to his feet and proclaims the disjointed footage to be a masterpiece. Vanucci suffers a crisis of conscience and confesses his guilt in court, thereby vindicating the villagers, but proclaiming that he will escape from prison once again.

The films final scene shows Vanucci escaping from prison by impersonating the prison doctor again. This time, however, he ties the doctor up and walks out of the prison in his place. As he attempts to remove the fake beard that is part of his disguise, he discovers that the beard is real, and realizes that the “wrong man" has escaped from prison.

==Cast==
 
*Peter Sellers as Aldo Vanucci / Federico Fabrizi
*Victor Mature as Tony Powell
*Britt Ekland as Gina Vanucci / Gina Romantica
*Martin Balsam as Harry Granoff
*Akim Tamiroff as Okra
*Maria Grazia Buccella as Okras sister
*Maurice Denham as The Chief of Interpol
*Paolo Stoppa as Polio
*Tino Buazzelli as Siepi
*Mac Ronay as Carlo
 
*Lydia Brazzi as Mamma Vanucci
*Lando Buzzanca as The Police Chief
*Tiberio Murgia as 1st Detective
*Francesco De Leone as 2nd Detective
*Pier Luigi Pizzi as Prison Doctor
* Enzo Fiermonte as Raymond
* Carlo Croccolo as Salvatore
* Nino Vingelli as a judge
* Vittorio De Sica as himself
 

==Production== art house films such as Last Year at Marienbad and Michelangelo Antonionis films, but the story evolved into the idea of a film-within-a-film.  Aldo Vanucci brings to mind the fast-talking cons of Phil Silvers and the brilliant dialects of Sid Caesar.  This is probably no coincidence since Simon wrote for both on television. 

In his 1996 memoir Rewrites, Simon recalled that an agent suggested Peter Sellers for the lead, while Simon preferred casting "an authentic Italian" such as Marcello Mastroianni or Vittorio Gassman. Sellers loved the script, however, and it was he who asked Vittorio De Sica to direct. 

De Sicas interest in the project surprised Simon, who at first dismissed it as a way for the director to support his gambling habit. But De Sica said he saw a social statement to be made, namely how the pursuit of money corrupts even the arts. Simon believed De Sica also relished the opportunity to take potshots at the Italian film industry. De Sica insisted that Simon collaborate with Cesare Zavattini. Since neither spoke the others language, the two writers worked through interpreters. "He had very clear, concise, and intelligent comments that I could readily understand and agree with", Simon wrote. Still, Simon worried that inserting social statements into what he considered a broad farce would not do justice to either. Yet After The Fox does touch on themes found in De Sicas earlier work, namely disillusionment and dignity. 
 John Bryan, a former production designer. It was also their last production, as Sellers and Bryan had a rift over De Sica. Sellers complained that the director "thinks in Italian, I think in English", and wanted De Sica replaced. But Bryan resisted for financial and artistic reasons. De Sica, meanwhile, grew impatient with his petulant star and did not like Sellers performance or Simons screenplay. 

  (in which he plays an aging football star) appears in the film.

According to Neil Simon, Sellers demanded that his wife, Britt Ekland, be cast as Gina, the Foxs sister. Eklands looks and accent were wrong for the role, but to keep Sellers happy De Sica acquiesced. Still, Simon recalled, Ekland worked hard on the film.  Sellers and Ekland made one other film together, The Bobo (1967).

Also featured are Akim Tamiroff as Okra, the mastermind of the heist in Cairo; Martin Balsam as Tonys agent, Harry; Maria Grazia Buccella as Okras voluptuous accomplice; Lydia Brazzi as Mama Vanucci; and Lando Buzzanca as the chief of police in Sevalio. Simon recalled that the Italian supporting cast learned their lines phonetically.  Tamiroff had been working on and off for Orson Welles filming Don Quixote, playing Sancho Panza. The film was never finished. Buccella was a former Miss Italy (1959) and placed third in the Miss Europe pageant. She was considered for the role of Domino in Thunderball (film)|Thunderball.  The wife of actor Rossano Brazzi, Lydia Brazzi was hand-picked by De Sica for the role of the Foxs mother, over her protestation that she was not an actress. 

The budget for the film was $3 million, which included location shooting in the village of Sant Angelo on Ischia in the Bay of Naples as well as the construction of an exact replica of Romes most famous street, the Via Veneto, on the Cinecittà lot. The Sevalio sequences were shot during the height of the tourist season. Reportedly the villagers of Sant Angelo were so busy accommodating tourists that they had no time to appear as extras in the film. The extras were brought in from a neighboring village. 
 Lawrence of Arabia. 

Simon summed up his opinion of the film: "To give the picture its due, it was funny in spots, innovative in its plot, and was well-intentioned. But a hit picture? Uh-uh...Still today, After the Fox remains a cult favorite." 

Burt Bacharach composed the score, and with lyricist Hal David wrote the title song for the film. For the Italian release, the score was composed by Piero Piccioni.  The title song "After the Fox" was recorded by The Hollies with Sellers in August 1966  and released by United Artists as a single (b/w "The Fox-Trot"). 

==Release==
After the Fox was released in Great Britain, Italy, and the US in December 1966. 

The film received mixed reviews in its day. New York Times critic Bosley Crowther summed up his review, "Its pretty much of a mess, this picture. Yes, youd think it was done by amateurs."  The Variety reviewer thought "Peter Sellers is in nimble, lively form in this whacky comedy which, though sometimes strained, has a good comic idea and gives the star plenty of scope for his usual range of impersonations."  Opinion continues to be divided. After the Fox is rated 6.5/10 on IMDB, an average of more than 2,500 user ratings.  The review aggregator website Rotten Tomatoes reported an 83% approval rating with an average rating of 5.7/10, based on six reviews; the Rotten Tomatoes audience score was 68%. 
 trailer for After The Fox proclaimed, "You Caught The Pussycat...Now Chase The Fox!".  The poster art for both films was illustrated by Frank Frazetta. 

==Influence==
The device in which robbers use a movie set to cover a robbery is also in Woody Allens Take the Money and Run (1969).  In the TV series Batman (TV series)|Batman, it is used in the 1968 episode titled "The Great Train Robbery".
 Austin Powers talks to Foxxy Cleopatra through the Nathan Lane character. 

The Bollywood movie Tees Maar Khan (2010 in film|2010) is a remake of After the Fox. 

==References==
{{Reflist|refs=
 "Big Rental Films of 1967", Variety (magazine)|Variety, 3 January 1968, p. 25. 
   
   
   
   
   
 Kevin Thomas (film critic)|Thomas, Kevin (7 December 1966). "Victor Mature Hits Stride". Los Angeles Times (1923–Current File). p. D15. 
   
   
   
   
   
   
   
   
   
}}

==External links==
 
*  
*  
*  

 

 

 
 
 
 
 
 
 
 
 
 
 
 
 