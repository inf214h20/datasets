Peruvannapurathe Visheshangal
 
 
 
{{Infobox film
| name           = Peruvannapurathe visheshangal
| image          = Peruvannapurathe-visheshangal.jpg
| image size     =
| caption        = Poster Kamal
| producer       = Castle Kamal  Ranjith
| Parvathi Jagathi Innocent Philomina Oduvil Unnikrishnan Mohanlal  (cameo) 
| lyrics         = Johnson
| cinematography = Vipin Mohan	
| editing        = K. Rajagopal	 	
| distributor    = Century Films
| studio         = Castle Productions 
| released       = 1989
| runtime        = 120 minutes 
| country        = India
| language       = Malayalam
| budget         =
| gross          =
| preceded by    =
| Year           = 1989
| followed by    =
}} Kamal and cameo role.

==Synopsis==
Kunju Lakshmi (Parvathy Jayaram|Parvathi) is the youngest daughter of the aristocratic Kavumpattu family in Peruvannapuram. She is pampered by her brothers and is a little arrogant. The family owns the local college.

Sivasankaran (Jayaram) comes to the college as a peon to replace the Padmanabhan (Jagathy Sreekumar) who fails to give the promised donation to the Kavumpattu family in return for the job. Padmanabhan is despondent at the loss of his job. People in the village goad him to make life miserable for the new peon so that he would leave the job and go leaving the post vacant for Padmanabhan. Padmanabhan starts exhorting money from Sivasankaran. Sivasankaran on the other hand finds out that his job includes helping the Kavumpattu family in household chores. When he takes lunch for Kunju, she laughs at him and taunts him. In the meantime Sivasankar stands up to Padmanabhan and refuse to give him anymore of his money.

Padmanabhan resorts to stealing coconuts from the Kavumpattu family estate to earn some money. Sivasankar catches them and in the scuffle he is accused of being the thief. He is let off on the intervention of the grandmother of the family. In the meantime a love note that was written for Kunju by a classmate ends up in her book and she accuses Sivasankar of writing her the letter. Kunju gets into a fight with Sivasankaran. Her brothers join the fight and Sivasankaran declares that he will marry Kunju in 15 days. Although Kunju hates Sivasankaran at that point, she later falls in love. Her brothers try to pressurize her to marry their maids son Achu (Mohanlal) who is actually the son of the late Vamadeva Kurupu of Kavumpattu. Ultimately, Sivasankaran and Kunju marry with  Achus help.

==Cast==
*Jayaram ...  Sivasankaran 
*Mohanlal ...  Achutha Kurup (Special Appearance) Parvathi ...  Kunju Lakshmi 
*Jagathi Sreekumar ...  Keeleri Padmanabhan 
*Philomina ...  Kunju Lakshmis Grandmother  Innocent ...  Adiyodi  Shivaji ...  Kurup  Siddique ...  Antappan  Kalpana ...  Mohini
*Kuthiravattam Pappu  ... Pushpangadan
*Oduvil Unnikrishnan ...Appunni Nair
* Mammukoya ...P. C. Peruvannapuram
* V. K. Sreeraman Santhosh
* Kaviyoor Ponnamma ... Devaki Amma
* K. P. A. C. Lalitha ... Madhavi Amma
* Sankaradi ... Principal
* Jagadish
* Kundara Johny
* Idavela Babu ...Suresh
* Paravoor Bharathan as Kalari Gurukkal

== External links ==
*  

 

 
 
 
 
 

 