Tom and Jerry: The Fast and the Furry
{{Infobox film
| name           = Tom and Jerry: The Fast and the Furry
| image          = Tom and Jerry The Fast and the Furry cover.jpg
| caption        = 
| director       = Bill Kopp
| producer       = Stephen Fossatti
| screenplay     = Bill Kopp
| story          = Joseph Barbera
| based on       = Tom and Jerry by William Hanna & Joseph Barbera
| music          = Nathan Wang Charles Adler Billy West Stephen Root Thom Pinto Neil Ross Vicki Lewis
| studio         = Turner Entertainment Warner Bros. Family Entertainment
| distributor    = Kidtoon Films   Warner Home Video
| released       =  
| country        = United States
| runtime        = 75 minutes
| language       = English
| budget         = 
| gross          = 
}} The Fast and the Furious, and is the second production from Warner Bros. Animation to spoof the phrase (the first was Fast and Furry-ous in 1949, the Looney Tunes short that introduced the Wile E. Coyote and The Road Runner.) It was released theatrically in selected cities of the United States by Kidtoon Films in September 2005 and again in June 2006. The film was released on DVD on October 11, 2005,    and on Blu-ray on April 5, 2011.   

==Plot== Tom and Jerry have wrecked the house they lived in during a frantic Tom-Jerry-esque chase, so they enter a race/reality show titled the "Fabulous Super Race", which offers the race-winner a luxurious mansion. Tom and Jerry, having built their own customized vehicles from scrap materials in a junk yard, present themselves to Globwobbler Studios, who are hosting the race. 

Jerry then prepares to continue racing to Borneo, where J.W. has assigned the finish line to be. Also, Grammy is brought back when the whale spits her out into point. Tom is then brought back up with the assistant of the producer flying to Australia to give him Cardiopulmonary resuscitation|CPR. Tom then repairs his car and cuts the continent in half using a laser, in an attempt to get into first place. However, angry because Tom has destroyed half, the boxing kangaroo comes and attacks Tom. Jerry and Grammy are able to survive.

The next leg of the race involves them modifying their cars for air travel to Borneo, which all three do with balloons, causing them to fly through the air slowly. However, Tom pops Grannys balloon with a harpoon, causing their car to fall. Tom and Jerry are shocked at this, meaning they have only five minutes to go to the opposite side of the world! But they decide to do it anyway for the mansion and the time restrictions. The main boss appears and kills him by cremating him for having a family friendly attitude. The main boss, subsequently makes Irving the head of Globwobbler Studios. At last, all is well with Tom and Jerry now sharing the mansion peacefully until the owner of their previous house shows up and orders Tom to get rid of Jerry.

==Voice Cast==
* Jess Harnell as Buzz Blister, Film Director Billy West as Biff Buzzard, President of Hollywood, Squirty
* John DiMaggio as J.W., Spike
* Rob Paulsen as Irving, Dave
* Charlie Adler as Grammy
* Tom Kenny as Gorthan, Whale
* Tress MacNeille as Soccer Mom (Malorie McDougal), Tour Girl, Lady
* Jeff Glen Bennett as Steed Dirkly, TV Announcer
* Grant Albercht as Clown-O, Security Guard
* Neil Ross as Dr. Professor, Director
* Thom Pinto as Computer Voice, Guard
* Bill Kopp as Tom, Frank
* Dee Bradley Baker as Jerry

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 

 