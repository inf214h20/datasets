Ripley Under Ground (film)
{{Infobox film
| name           = Ripley Under Ground
| image_size     =
| image          = Ripley Under Ground FilmPoster.jpeg
| caption        =
| director       = Roger Spottiswoode
| producer       =
| writer         = W. Blake Herron Donald E. Westlake
| based on       =    Patricia Highsmith 
| narrator       =
| starring       = Barry Pepper Jacinda Barrett Tom Wilkinson Willem Dafoe Alan Cumming Claire Forlani Ian Hart
| music          = Jeff Danna
| cinematography = Paul Sarossy
| editing        = Michel Arcand
| distributor    = Fox Searchlight Pictures
| released       = 6 November 2005
| runtime        = 101 minutes
| country        = Germany, France, UK 
| language       = English
| budget         =
| gross          =
}} French crime second novel Tom Wilkinson in supporting roles. Ripley Under Ground was produced during July and August 2003,  but was only released two years later. It was shown on the 2005 AFI Fest by American Film Institute, receiving a low-profile wide theatrical release.

== Plot summary ==
After his friend, a successful young artist, is killed in a car accident, Tom Ripley (Pepper) and his friends hide his body and concoct a scheme in which they forge his paintings, eventually making a great deal of money. When an art collector (Dafoe) complains that a painting he bought from the gallery is a fake, Ripley must use his inimitable talents to defuse the problem by whatever means necessary.

== Cast ==
*Barry Pepper as Tom Ripley
*Willem Dafoe as Neil Murchison
*Alan Cumming as Jeff Constant Tom Wilkinson as John Webster
*Jacinda Barrett as Héloïse Plisson
*Claire Forlani as Cynthia
*Ian Hart as Bernard Sayles
*Douglas Henshall as Derwatt
*François Marthouret as Antoine Plisson

== Release and reception ==
 wide theatrical release on 6 November 2005,  as well as being shown on the AFI Fest film festival in Los Angeles. It was released on DVD on 24 July 2007 in the Netherlands.
 The Talented Mr. Ripley and Ripleys Game (film)|Ripleys Game, its all in the casting, and the talented Mr. Barry Pepper is not capable of pulling off the demonically complicated and murderous con artist." 

WorldsGreatestCritic.com, however, gave a positive review, praising Barry Peppers "impeccable performance" and the film for capturing "the fun of Ripley, a fun that few others have captured from Highsmiths prose." According to the reviewer J.C. Maçek III, this is the first portrayal of Ripley to earn Patricia Highsmiths approval.  As of 1 February 2010, Ripley Under Ground has a user rating of 6.1 out of 10, based on 809 votes, on The Internet Movie Database. 

==References==
 

== External links ==
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 

 
 