Hare-um Scare-um
{{Infobox Hollywood cartoon cartoon name=Hare-um Scare-um
|series=Merrie Melodies 250px
|caption=The rabbit setting a trap for the hunter`s dog.
|director=Ben Hardaway Cal Dalton
|producer=Leon Schlesinger story artist=Melvin Millar voice artist=Mel Blanc (uncredited)
|musician=Carl W. Stalling
|animator=Gil Turner  Herman R. Cohen (uncredited)  Richard Bickenbach (uncredited)
|background=Art Loomer (uncredited)
|studio=Leon Schlesinger Studios
|distributor=Warner Bros. Pictures release date=12 August 1939 color process=Technicolor
|runtime=7:47 Theatrical Release 8:10 with ending
|country=USA English
}} 1939 Merrie the Bugs Knock Knock, released the following year). This is also the final cartoon to have a banner on the Warner Bros logo.

==Plot==
A man (named as John Sourpuss.) reading a newspaper comes across an article stating that meat prices have soared. Angry, he declares that hell hunt his own meat to get back at the government for the price inflation. He takes his dog with him, revealing he is going hunting for rabbits.

In the woods, a rabbit leads the dog into a hollow log and pushes the log down a hill, where it smashes into a tree.  Meanwhile, John sees several rabbits hopping over a hill. He fires his gun several times and runs to where the rabbits were. When he gets there, he finds two spinning wheels with pictures of rabbits on them, giving the perception of moving rabbits.

He then sees the rabbit sleeping. The hunter starts pouring salt on the rabbit, who quickly gets up and holds a stick of celery under the stream of salt. The rabbit then runs into a cave, and the hunter runs after him. Before he reaches the cave, a pair of elevator doors closes, which the hunter runs into.

The bunny then dresses as a female dog, successfully seducing the hunters dog. When the dog finally realizes hes with the rabbit rather than another dog, he resumes his chase. The rabbit then pretends hes a policeman, citing the dog for numerous crimes (speeding, running on the wrong side of the street, intoxicated "driving", etc.).

After confusing the dog and running away, the rabbit begins singing a song about how crazy he is. When he finishes his song, he turns to find the hunter with his gun aimed at him. The rabbit, trying to gain sympathy, begs for his life, explaining how poor and sick he is. John begins crying, feeling sorry for the rabbit. Despite this, the rabbit shocks John with a joy buzzer. The hunter then shouts that he can whip the rabbit and his whole family. Suddenly, a large group of rabbits surround John, looking for a fight (see "Lost Ending" below for the true ending).

==Lost ending==
 
There had been speculation about the real ending of this cartoon, as the version shown on television ends abruptly after the rabbits appear following the hunter threatening to beat up the wacky rabbit and his entire family. On April 27, 2009, animation historian  , which was released a year earlier prior to Hare-um Scare-ums release.

==Availability==
Hare-um Scare-um is available with the lost ending restored on  , disc 2.

==References==
 

==External links==
*  

 
{{succession box
|before=Prest-O Change-O title = Bugs Bunny Cartoons
|years=1939
|after=Elmers Candid Camera}}
 

 
 
 
 
 
 
 