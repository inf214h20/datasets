Once Upon a Wheel
{{Infobox television film
| bgcolour     = 
| name         =  Once Upon a Wheel
| image        = Newman1971.jpg  
| image_size   = 240px
| caption      = Once Upon a Wheel
| format       = Documentary
| runtime      = 60 min
| creator      =  David Winters David Winters, Burt Rosen
| writer       = 
| starring     = Paul Newman (host), Mario Andretti, Hugh Downs, Dean Martin, Cesar Romero
| editing      = 
| music        = 
| budget       = 
| country      =  
| language     = English ABC
| 1971
| first_aired  = 
| num_episodes = 
| preceded_by  = 
| followed_by  = 
}}
 ABC television David Winters.

A racing enthusiast, Newman narrated this hour-long documentary on the history of auto racing.    Joining Newman was Mario Andretti, Kirk Douglas, Hugh Downs, Dean Martin, Cesar Romero, Dick Smothers and many others.

  on set ]]
TV Guide featured an article on the program as well as Newman on the cover in the April 17, 1971 issue.  The film was released to home video by Monterey Media.   

== Sponsors ==
Coca-Cola bottlers sponsored the show, and Mr. Newman appeared in magazine ads (wearing a Coke racing jacket). Viewers could order special collectibles related to the show: 8-track cartridges or cassette tapes of music from the show, a Coca-Cola jacket similar to Newman’s, and racing jacket style patches. {{cite web
|url= http://www.coca-colaconversations.com/my_weblog/2008/09/paul-newman.html
|title= Coca Cola Conversations |author= The Coca-Cola Company| work=We’ll miss Mr. Newman’s love of racing, and of course his great acting and his philanthropy.
}} 

== International ==
The show was released theatrically in Europe, with additional footage.  The Italian title was "I Giganti del  Brivido", and "Vertigo Sobre Ruedas" in Spanish.  The director, David Winters has stated it was Paul Newmans impression of the finished product that encouraged the extended version. 
{{cite web
|url= http://www.davidwinters.net
|title= David Winters Website
}} 

== Reviews ==

"Spectacular...Paul Newman and celebs performed for director David Winters with joie de la cause, abetted by extraordinary editing.... A poetic study of man and machines...Incredible effect...Most exciting."
-Hollywood Reporter

{{quote|"Dazzlingly brilliant...Exciting…Moving"|
Don Freeman, San Diego Union}}

"Most Exciting show ever ... A Special TV Special"
-TV Guide

{{quote|"The Woodstock of racing...it is very touching...it is entertaining....it has a balletic sense of it......Incredible...."|
Donna Pickrell, Houston Post}}
(see     )

== Further reading ==
* Ormstein, Bill, "Winters-Rosen Productions|Winters-Rosen Triple Budgets", Hollywood Reporter,17, Nov, 1970: p1

== References ==
 

==External links==
 
*{{cite book
|title= Once Upon a Wheel
|isbn= 1-56994-647-7}}
*
*Home video listing on commercial website  
*  
*  

 

 
 
 
 
 
 
 
 
 
 