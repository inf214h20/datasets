Double Door (film)
{{Infobox film
| name           = Double Door
| image          = 
| alt            = 
| caption        =
| director       = Charles Vidor
| producer       = E. Lloyd Sheldon Jack Cunningham Gladys Lehman
| based on       =   Mary Morris Anne Revere Kent Taylor
| music          = 
| cinematography = Harry Fischbeck 
| editing        = James Smith 
| studio         = Paramount Pictures
| distributor    = Paramount Pictures
| released       =  
| runtime        = 75 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =
}}
 Jack Cunningham Mary Morris, Anne Revere and Kent Taylor. Morris and Revere reprised their Broadway roles.    Though Morris had a long stage career, this is her only film performance.  The film was released on May 4, 1934, by Paramount Pictures.    

==Plot==
A domineering woman tries to destroy her brothers new marriage.

== Cast ==
*Evelyn Venable as Anne Darrow
*Mary Morris as Victoria Van Brett
*Anne Revere as Caroline Van Brett
*Kent Taylor as Rip Van Brett
*Guy Standing as Mortimer Neff
*Colin Tapley as Dr. John Lucas
*Virginia Howell as Avery
*Halliwell Hobbes as Mr. Chase
*Frank Dawson as Telson
*Helen Shipman as Louise
*Leonard Carey as William

== Reception ==
The New York Times review was favorable: "The film version of Miss McFaddens play is a careful and intelligent copy of the original."  Morris was praised for her "highly effective performance", while Morris and Revere were described as "effective foils for the cruel old witch.  

== References ==
 

== External links ==
*  

 

 
 
 
 
 
 
 
 
 

 