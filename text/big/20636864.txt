S.O.S. (film)
{{Infobox film
| name           = S.O.S. 
| image          =
| caption        =
| director       = Leslie S. Hiscott 
| producer       = Julius Hagen Walter Ellis (play) 
| starring       = Robert Loraine  Bramwell Fletcher   Ursula Jeans   Lewis Dayton
| music          = 
| cinematography = 
| editing        = 
| studio         = Strand Films Allied Artists
| released       = December 1928
| runtime        = 7,250 feet 
| country        = United Kingdom 
| awards         =
| language       = Silent   English intertitles
| budget         = 
| preceded_by    =
| followed_by    =
}} 1928 Cinema British silent silent adventure film directed by Leslie S. Hiscott and starring Robert Loraine, Bramwell Fletcher and Ursula Jeans. The film takes its title from the morse code distress signal SOS|S.O.S.. It was made at Lime Grove Studios.

The film marked director Hiscotts first move from comedy films, which he had begun his career making, into the straight dramatic films that would become best known for.

==Cast==
* Robert Loraine as Owen Herriott 
* Bramwell Fletcher as Herriott 
* Ursula Jeans as Lady Weir 
* Lewis Dayton as Sir Julian Weir 
* Andrée Sacré as Judy Weir 
* Campbell Gullan as Karensky 
* Anita Sharp-Bolster as Mme. Karensky 
* Viola Lyel as Effie

==References==
 

==Bibliography==
* Low, Rachel. The History of British Film: Volume IV, 1918–1929. Routledge, 1997.

==External links==
* 

 
 
 
 
 
 
 
 
 


 
 