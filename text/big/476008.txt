The Statement (film)
{{Infobox film
 | name = The Statement
 | caption = 
 | director = Norman Jewison
 | producer = Norman Jewison Robert Lantos
 | based on =  
 | screenplay = Ronald Harwood
 | starring = Michael Caine Tilda Swinton Jeremy Northam
 | music = 
 | cinematography = Kevin Jewison
 | editing = Andrew S. Eisen Stephen E. Rivkin
 | distributor = Sony Pictures Classics
 | released = 2003
 | runtime = 120 minutes
 | country = Canada France United Kingdom
 | language = 
 | budget = 
 | gross =  $765,637 
 | image = The Statement VideoCover.jpeg
}} 2003 drama directed by a 1996 Brian Moore, written by Ronald Harwood.
 Vichy French police official, who was indicted after World War II for war crimes. In 1944, Touvier ordered the execution of seven Jews in retaliation for the French Resistance|Resistances assassination of Vichy France minister Philippe Henriot. For decades after the war he escaped trial thanks to an intricate web of protection, which allegedly included senior members of the Roman Catholic priesthood. He was arrested in 1989 inside a Traditionalist Catholic Priory in Nice and was convicted in 1994. He died in prison in 1996.
 In the The Thomas Crown Affair and Moonstruck.

==Plot== Nazi collaborator, executed during World War II. Some 40 years later, he is pursued by "David Manenbaum" (Matt Craven), a hitman who is under orders to kill Brossard and leave a printed Statement on his body proclaiming the assassination was vengeance for the Jews executed in 1944. Brossard kills "Manenbaum," hiding the dead body after finding the printed "Statement" and discovering that his pursuer was travelling on a Canadian passport. Brossard for years has taken refuge in sanctuaries in southern France within the Traditionalist Catholic community, appealing to long-time allies who have operated in great secrecy to shield him and provide him with funds. But now they bring increased scrutiny to themselves for continuing to do so.

The murder of "Manenbaum" attracts the interest of local police and eventually the persistent Investigating Judge Annemarie Livi (Tilda Swinton). She becomes absorbed by the case, not discouraged by the lack of assistance she encounters from official sectors. Livi forms an alliance with the similarly dedicated Colonel Roux (Jeremy Northam), a senior French Gendarmerie investigator, and the pair initially suspect that "Manenbaum" was part of a Jewish assassination plot. They discover that Brossard has been the subject of several previous investigations, dating back more than 40 years, which have all failed. Livi and Roux discover hidden resources, tightening the noose around Brossard, who finds his allies increasingly reluctant to help him. Doubts arise over the theory of a Jewish hit squad, but it is clear that someone wants Brossard dead.

Brossard in desperation pays a surprise visit to his estranged wife Nicole (Charlotte Rampling), a maid who is living in lower-middle-class circumstances in Marseille and is very apprehensive about seeing him again. Brossards allies, including certain priests and a wartime colleague who has risen into a position of great power within the French government, are feeling the heat from the relentless questioning of Livi and Roux. Now desperate and unsure whom to trust, Brossard seeks new identity papers and money so he can escape France forever. But he is now living on borrowed time.

==Main cast==
* Michael Caine as Pierre Brossard
* Tilda Swinton as Anne-Marie Levi
* Jeremy Northam as Colonel Roux
* Alan Bates as Armand Bertier
* Charlotte Rampling as Nicole
* Ciarán Hinds as Pochon
* Matt Craven as David Manenbaum

==Historical basis==
 novel of Brian Moore. In the novel and film, the fictional Brossard is based on Paul Touvier, a member of the Milice, a paramilitary police force of the Vichy French regime during World War II who ordered the execution of seven Jews in 1944. After the war, he was convicted him of treason and sentenced him to death in absentia, but with the aid of right-wing Roman Catholic clergymen, who provided him refuge in safe houses and monasteries, Touvier avoided capture. He received a controversial pardon from the President of France, Georges Pompidou, in 1971, but remained on the run. Unlike Brossard, Touvier finally was arrested in 1989 on a new charge of crimes against humanity; tried, convicted, and sentenced to life in prison, he died in jail in 1996.   

== References ==
 

==External links==
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 