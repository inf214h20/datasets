Shalimar (1978 film)
{{Infobox film
| name           = Shalimar 
| image          = Shalimar 1978 film.jpg
| image_size     = 
| caption        = 
| director       = Krishna Shah 
| producer       = Suresh Shah 
| writer         = Krishna Shah 
| narrator       =  John Saxon Sylvia Miles Shammi Kapoor  Prem Nath Aruna Irani 
| music          = R. D. Burman
| cinematography = Harvey Genkins   
| editing        = Amit Bose
| distributor    = 
| released       = 1978 
| runtime        = 
| country        = India English 
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
 American actors John Saxon and Sylvia Miles appear in supporting roles in their first and only Bollywood film. Jayamalini does a dance number in the film. This was the last time that Mohammed Rafis voice was picturized on Shammi Kapoor.  Its English version known as Raiders of the Sacred Stone. Rex Harrisons voice was dubbed by Kader Khan.
==Plot==

On the run from the police, S.S. Kumar, a thief, comes across a private invitation to the island of Sir John Locksley addressed to Raja Bahadur Singh. When the Raja is shot, Kumar takes him to a nearby hospital, dons a Sikhs turban, poses as the Rajas son and goes to the private island of Sir John. Also attending are K.P.W. Iyengar aka Romeo, Dr. Dubari, Colonel Columbus, and Countess Sylvia Rasmussen. A stunned Kumar finds out that all of these invitees are master criminals and thieves. Kumars guise does not fool anyone, including his former sweetheart, Sheila Enders (Zeenat Aman), nevertheless Sir John permits him to stay on, as he feels that Kumars career, though an amateur, is consistent with those already present. The reason why John has invited them is to find a successor to take his place as he is dying of cancer. He feels that one of his invitees can be trusted to take his place and for this he has arranged for them to steal a ruby (Shalimar) worth 135 crores of rupees. This gem is placed in a secure room within his palace, which is alarmed, and guarded by armed men 24 hours a day. The ruby itself is located within a display case of bulletproof glass and surrounded by a minefield. He challenges one of them to steal the shalimar - but if anyone fails then they are killed by the security system. Pitted against such veterans, it looks like Kumar has got himself into a bind that he may not come out of alive.

==Box Office==

The film was released in two versions; Hindi in India and English in USA. English version was unsuccessful, the Hindi version too was a failure when it was released. However the Hindi version later gained cult status in the DVD-Video circuit, and is now seen as an entertainer ahead of its times.

==Cast==

*Rex Harrison ...  Sir John Locksley 
*Dharmendra ...  S.S. Kumar 
*Zeenat Aman ...  Sheila Enders John Saxon ...  Colonel Columbus 
*Sylvia Miles ...  Countess Rasmussen 
*O. P. Ralhan ...  K.P.W. Iyengar (Romeo) 
*Shammi Kapoor ...  Dr. Dubari
*Prem Nath ...  Raja Bahadur Singh 
*Shreeram Lagoo ...  Tolaram 
*Aruna Irani ...  Dance Teacher 
*Clyde Chai-Fa ...  Dogro 
*M.B. Shetty ...  Tribal Chief

==Soundtrack==
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Naag Devta"
| Mohammed Rafi
|-
| 2
| "Aaina Wohi Rehta Hai"
| Lata Mangeshkar
|-
| 3
| "Hum Bewafaa Hargiz Na They (I)"
| Kishore Kumar
|-
| 4
| "Mera Pyaar Shalimar"
| Asha Bhosle
|-
| 5
| "Hum Bewafaa Hargiz Na They (II)"
| Kishore Kumar 
|-
| 6
| "One Two Cha Cha"
| Usha Uthup
|}
The soundtrack was featured in the book, 1001 Albums You Must Hear Before You Die.

==Nominations==

* Filmfare for Best Music - R.D. Burman
*Filmfare for Best Male Playback Singer - Kishore Kumar for the song "Hum Bewafa Harghiz Na the"
*Filmfare for Best Female Playback Singer - Usha Uthup for the song "One Two Cha Cha" 

==References==
 

==External links==
*  

 
 
 
 
 