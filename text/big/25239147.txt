Sathi Leelavathi (1936 film)
 

{{Infobox film
| name = Sathi Leelavathi
| image = Sathi leelavathi 1936.jpg
| caption  = 
| director = Ellis R. Dungan
| writer =  Subramaniam Srinivasan|S. S. Vasan
| starring = M. K. Radha M. S. Gnanambal M. G. Ramachandran N. S. Krishnan T. S. Balaiya
| producer = Marudachalam Chettiar, Manorama Films
| music = Sundara Vadhyaar
| distributor =
| released = 28 March 1936
| runtime =
| language = Tamil
| budget =
}}
 Tamil film directed by Ellis R. Dungan. This was the first film for M. G. Ramachandran and for Dungan as director. This was one of the first Tamil films to become the  subject of court case involving copyright violations.               

==Plot==
Krishnamurthy (M. K. Radha), a rich man living with his wife Leelavathi (M. S. Gnanambal) and a daughter in Madras, is lured into drinking, gambling and other
vices by his friend Ramanathan (T. S. Balaiah) at a mock tea party arranged for this purpose; Ramanathans collaborator is Rangaiah Naidu (M. G. Ramachandran), an Inspector of Police who misuses the power entrusted to him. Krishnamurthy is lured by the wiles of Mohanangi (Santhakumari), a woman with loose morals. Infatuated by her, he promises to pay her  50,000.

Parasuram (P. Nammalvar), a good friend of Krishnamurthy, tries to reform him but his efforts yield no results. A moneylender who had lent a huge sum
to Krishnamurthy to meet his lavish lifestyle issues a warrant for the recovery of his money and Krishnamurthy sinks into a deeper mess. In his drunken state, he ﬁnds fault with his wife and even accuses her of having an illicit relationship with Parasuram. When Parasuram visits Krishnamurthy to warn him about
the warrant, Leelavathi advises him to leave as Krishnamurthy is not at home. Absent mindedly, Parasuram leaves his umbrella behind. Krishnamurthy comes
home drunk, notices the umbrella, suspects that his wife is having an affair with Parasuram, beats her and rushes out with a revolver to shoot Parasuram.
Meanwhile, Ramanathan sends his servant in the guise of Parasuram to steal the jewels of Ekambareshwarar temple.

Krishnamurthy comes pursuing Parasuram; a shot is heard and a man lies dead. This sudden and unexpected calamity brings the drunken Krishnamurthy to
his senses. Krishnamurthy thinks he has murdered his friend Parasuram, decides to escape and leaves his wife and child in the custody of his faithful servant
Govindan. He goes to Ceylon where he leads a wretched life as a nameless labourer in a tea estate. Ramanathan now takes this opportunity to try to molest Leelavathi who spurns his illicit advances. Penniless, she goes with her servant and daughter and leads a poor but honourable life spinning the charkha (spinning wheel). Fate favours Krishnamurthy. During his labour work, he ﬁnds a treasure trove and gives it to his master, his master is pleased with his honesty and adopts him as his own son.

==Cast==
  (MGR) as Inspector Rangayya Naidu]]
* M. K. Radha
* M. S. Gnanambal
* M. G. Ramachandran
* N. S. Krishnan
* T. S. Balaiya
* Ellis R. Dungan - Director
* M. Kandasamy Mudaliar - Dialogue
* S. S. Vasan - Story
* Sundara Vadhyar - Lyrics 

==Production== Dungan and recommended Dungan be given the chance instead. Dungan was hired as director and the film was made. Some scenes were shot in location at Ceylon. The completed film was 18,000 feet in length.                

==Release==

===Controversy===
The film was released on 28 March 1936. Its release was delayed as it became the subject of a court case. Another film - Pathi Bhakthi - had been released in the same year based on the play of same name and starring K. P. Kesavan. The makers of Pathi Bhakthi sued Marudachalam Chettiar and Kandasamy Mudaliar for plagiarizing their story. The case was resolved when S. S. Vasan, admitted in court that both Pathi Bhakthi (written by Krishnasamy Paavalar) and Sathi Leelavathi had been plagiarized from Ellen Wood (author)|Mrs. Henry Woods story Danesbury House. The film was a modest success.   The art magazine Aadal paadal in its January 1937 issue appreciated the film for its social setting and praised it for its high quality acting.   

===Critical reception===
Several new techniques introduced by Dungan were not understood by the audience and went unappreciated. Writing in the Silver Screen magazine on 1 August 1936, Pe. Ko. Sundararajan (journalist and writer of Manikodi movement) complained:

  }}

==References==
 

==External links==
*  

 
 
 
 
 
 