Kathapurushan
{{Infobox film
| name           = Kathapurushan (The Man of the Story)
| image          = Kathapurushan.jpg
| caption        = A screenshot from the movie
| director       = Adoor Gopalakrishnan
| writer         = Adoor Gopalakrishnan
| starring       = Vishwanathan Mini Nair Aranmula Ponnamma Narendra Prasad Urmila Unni  Babu Namboothiri
| producer       = Adoor Gopalakrishnan
| distributor    =
| music          = Vijaya Bhaskar
| cinematography = Mankada Ravi Varma
| editing        = M. Mani
| released       = 1995
| runtime        = 107 mins
| language       = Malayalam
}}
 Golden Lotus National Film Awards in 1996. Kathapurushan is a journey exploring the recent history of the state of Kerala in India.

==Plot==

Kunjunnis (Vishwanathan) parents separate soon after his birth, and hes left to be cared by his mother, void of paternal care and affection. He is brought up by his mother with the help of his grandmother, an estimate manager and his friend Meenakshi (Mini Nair), the daughter of a maid servant working in Kunjunnis house. Inspired by his Uncle (Narendra Prasad), who was initially a Gandhian, and eventually  a Marxist, Kunjunni finds himself drawn to left-wing ideologies during his studies at college, believing that communism is the answer to heal all social hardships and inequalities. Eventually he joins an extremist Marxist group, and his affairs land him in trouble. After an attack at a police station, Kunjunni is arrested and taken to court, but later acquitted of all charges.
Kunjunni finds himself maturing with experience, yet he feels lonely and disillusioned. Kunjunni tries to turn his life around, and sets off on a quest to find his childhood friend Meenakshi and he does so and marries her. Later he sells all his properties to a new rich man (Mukesh (actor)|Mukesh),whose father was once a servant in Kunjunis house.Then he moves to an ordinary house and  tries to live a normal family life with his wife and son.One day, one of his old day college juniors (Jagadish), a journalist, arrives his home for an interview.But Kunjunni denies it. Later Kujunni, with the help of his journalist friend, publishes his first story "Karaksharangal".But due to its Marxist supporting content and revealing nature, government banned it. When Kunjunni reads this news in the newspaper with his family, he began to laugh. Then he and his family laugh together at this dirty world, which reveals that the revolution had not yet happened completely.

==Awards==
The film has won the following awards since its release:
 National Film Awards (India)
* Won - Golden Lotus Award - National Film Award for Best Feature Film - Adoor Gopalakrishnan
* Won - Silver Lotus Award - National Film Award for Best Supporting Actress - Aranmula Ponnamma

1997 Bombay International Film Festival (India)
* Won - FIPRESCI Prize - Adoor Gopalakrishnan

==External links==
* 

 
 

 
 
 
 
 
 
 


 