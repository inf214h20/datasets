Live and Become
 
{{Infobox Film 
| name           = Live and Become
| image          = LiveAndBecomePoster.jpg
| image_size     = 
| caption        = Theatrical poster
| director       = Radu Mihaileanu
| producer       = Denis Carot Marie Masmonteil Radu Mihaileanu  Marek Rozenbaum Itai Tamir
| writer         = Alain-Michel Blanc Radu Mihaileanu
| narrator       = 
| starring       =Moshe Agazai Moshe Abebe Sirak M. Sabahat Yael Abecassis Roschdy Zem Roni Hadar Rami Danon Raymonde Abecassis Mimi Abonesh Kebede Meskie Shibru-Sivan Hadar
| music          = Armand Amar
| cinematography = 
| editing        = Ludo Troch
| distributor    = Menemsha Films
| released       = 30 March 2005
| runtime        = 140 minutes
| country        = France
| language       = Amharic / Hebrew / French
| budget         = 
| gross          = 
}} Ethiopian Jew to escape famine and emigrates to Israel. It was directed by Romanian-born Radu Mihăileanu. It won awards at the Cannes, Berlin and Vancouver film festivals among others.

== Plot == Ethiopian Jewish woman whose child has died. This woman, who will become his adoptive mother, is about to be airlifted from a Sudanese refugee camp to Israel during Operation Moses in 1984. His birth mother, who hopes for a better life for him, tells him “Go, live, and become,” as he leaves her to get on the bus. The film tells of his growing up in Israel and how he deals with the secrets he carries: not being Jewish and having left his birth mother.

== Cast ==
*Moshe Agazai as Child Shlomo 
*Moshe Abebe as Teenage Shlomo
*Sirak M. Sabahat as Adult Shlomo
*Yael Abecassis as Yael Harrari
*Roschdy Zem as Yoram Harrari
*Roni Hadar as Sarah
*Rami Danon as Papy 
*Raymonde Abecassis as Hana 
*Mimi Abonesh Kebede as Suzy 
*Meskie Shibru-Sivan Hadar as Shlomos mother
*Yitzhak Edgar as Qes Amhra

==Awards==
===International===
* Best Screenplay, Cesar ("French Oscar") 2005
* Audience Award, Berlin Film Festival 2005
* European Cinema Award, Berlin Film Festival 2005
* Ecumenical Award, Berlin Film Festival 2005
* Audience Award, Vancouver International Film Festival 2005
* Golden Swan (Best Film), Copenhagen International Film Festival 2005
* Best Screenplay, Copenhagen International Film Festival 2005
* Jury Prize (Best Film), Valenciennes Film Festival 2005
* Audience Award (Best Film), Valenciennes Film Festival 2005

===In the United States===
Audience Awards unless otherwise noted
* Boston Jewish Film Festival
* Washington Jewish Film Festival
* Miami Jewish Film Festival
* Atlanta Jewish Film Festival
* Detroit Jewish Film Festival (Best Film)
* San Diego Jewish Film Festival
* Orange County Jewish Film Festival
* Palm Desert Jewish Film Festival
* Palm Beach Jewish Film Festival
* Tampa Jewish Film Festival
* Seattle Jewish Film Festival
* Cleveland International Film Festival
* Nashville Film Festival
* Los Angeles Jewish Film Festival
* Detroit Jewish Film Festival (Audience Award)
* Washington DC International Film Festival (Runner-Up)
* Aspen Filmfest

==External links==
* 
* 
*  
* 

 
 
 
 

 
 

 
 
 
 
 
 
 

 
 
 