The Loves of Carmen (1948 film)
{{Infobox film
| name           = The Loves of Carmen
| image          = 
| image size     =
| caption        = 
| director       = Charles Vidor
| producer       = Charles Vidor Rita Hayworth
| screenplay     = Helen Deutsch
| based on       =  
| starring       = Rita Hayworth Glenn Ford
| music          = Mario Castelnuovo-Tedesco
| cinematography = William E. Snyder
| editing        = Charles Nelson The Beckworth Corporation
| distributor    = Columbia Pictures
| released       =  
| runtime        = 99 minutes
| country        = United States English
| budget         = over $2 million Letter from Hollywood
By Frank Daugherty Special to The Christian Science Monitor. The Christian Science Monitor (1908-Current file)   09 Jan 1948: 4.  
}}

The Loves of Carmen is a 1948 American Technicolor romantic drama film directed by Charles Vidor. The film stars Rita Hayworth as the gypsy Carmen and Glenn Ford as her doomed lover Don José. 
 of the same name, which was directed by Raoul Walsh and stars Dolores del Rio and Victor McLaglen.

==Plot==
 
Following the plot of the classic opera, "Carmen," this story follows the wild gypsys adventures as a siren and bandit. Carmen (Rita Hayworth) lures an innocent soldier (Glenn Ford) to his ruin, getting him expelled from the army. He then turns to banditry, killing Carmens husband (Victor Jory) and others. The drama culminates in an ending with the innocent soldier repenting of his sins and dying.

==Cast==
* Rita Hayworth as Carmen (singing voice was dubbed by Anita Ellis)
* Glenn Ford as Don José
* Ron Randell as Andrés
* Victor Jory as  García
* Luther Adler as Dancaire
* Arnold Moss as Colonel
* Joseph Buloff as Remendado
* Margaret Wycherly as Old Crone
* Bernard Nedell as Pablo
* John Baragrey as Lucas

=== 1927 Film ===
* Dolores del Rio as Carmen
* Victor McLaglen as Don Jose
* Don Alvarado as Andres

=== 1917 Film ===
* Pola Negri as Carmen
* Harry Liedtke as Don José

==Production== Eduardo Cansino, to help choreograph the traditional Spanish dances. Also, her uncle José Cansino can be seen as her dance partner in one scene, and her brother Vernon Cansino has a bit part as a soldier.

The musical score of the film was composed by Mario Castelnuovo-Tedesco.

==See also==
* List of American films of 1948
==References==
 
==External links==
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 