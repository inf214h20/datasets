Pride and Glory (film)
{{Infobox film
| name           = Pride and Glory
| image          = Prideandglory 1.jpg
| caption        = Theatrical release poster Gavin OConnor
| producer       = Greg OConnor
| story  = Gavin OConnor Greg OConnor Robert Hopes
| screenplay         = Gavin OConnor Joe Carnahan 
| starring       = Edward Norton Colin Farrell Jon Voight Noah Emmerich
| music          = Mark Isham
| cinematography = Declan Quinn John Gilroy
| studio  = Solaris Entertainment OConnor Brothers Avery Pix
| distributor    = New Line Cinema (US) Entertainment Film Distributors (UK)
| released       =  
| runtime        = 130 minutes
| country        = United States Germany
| language       = English
| budget         = $30 million
| gross          = $43,440,721 
}} Gavin OConnor. It stars Edward Norton, Colin Farrell, Jon Voight, and Noah Emmerich. The film was released on October 24, 2008, in the United States.

Assistant Chief Francis Tierney (Jon Voight) is the head of a multigenerational police family, which includes his sons Francis, Jr. (Noah Emmerich), Ray (Edward Norton), and his son-in-law Jimmy Egan (Colin Farrell) all being police officers. When four of Francis Jr.s officers are killed during a shootout turned bad, everything looks straight initially. However, Ray, who is assigned to the investigation, soon discovers something more sinister. 

==Plot==
 
Jimmy Egan (Colin Farrell) leads the New York City Police Department to victory in police-league football. While everybody is celebrating, Francis "Franny" Tierney Jr. (Noah Emmerich), Jimmys brother-in-law and superior officer, answers his phone to find out that several men from their unit have been killed during a failed drug bust of a local gang leader, Angel Tezo.

Franny and his brother, Ray Tierney (Edward Norton), rush to the scene and find that four officers in Frannys division were the ones killed in the raid. Francis Tierney Sr. (Jon Voight), a retired NYPD police officer of high rank, pushes Ray to lead the task-force being formed to investigate the incident. Ray — still haunted by a prior, unrelated incident where he succumbed to internal department pressures in protecting a bad cop, ultimately costing him his marriage — is reluctant at first, but is unable to deny his fathers request and agrees. He soon learns of an eyewitness, a young Hispanic boy, who saw a bleeding Tezo escape the scene by hijacking a cab. A possible suspects cell phone is also recovered from the scene.

Jimmy, Kenny Dugan (Shea Whigham), Reuben Santiago (John Ortiz) and Eddie Carbone (Frank Grillo) meet at the abandoned getaway cab containing the dead cab driver. These four officers are part of a corrupt gang of NYPD police led by Jimmy. Under his direction, they burn the cab and dead driver, and set about finding Tezo before Ray and the task force do. Later, during a family Christmas dinner, Jimmy and Franny have a private discussion in which Franny reveals knowledge of some of Jimmys illegal dealings and asks for reassurance that he will not face the consequences himself.

A gang-related shootout takes place in an apartment leaving only one living witness, who informs Ray that Tezo was tipped off about the raid by a friend in the NYPD named "Sandy". Ray investigates Tezos phone records and finds a received call from the same phone found outside the crime scene, which was also used to call the police after the shooting, indicating that the witnesss story is correct. Later, Ray tells Franny about this and asks whether there is someone by that name in his division. Franny says no.

Later, Franny confronts officer Reuben "Sandy" Santiago with the disturbing discovery. He admits to what he did and expresses surprise when Franny claims he never knew what was going on with the officers in his unit. Santiago tells Franny the incident took place because they wanted to kill Tezo so they could work with another dealer, Casado, but that Santiago warned Tezo due to a childhood friendship, believing Tezo would simply flee and not thinking the police would be in danger. Franny tells Santiago to hand over his badge.

Jimmy is confronted at his home by Casado, who had paid Jimmy to kill Tezo so that they could start working with him. Casado threatens to harm his family if the problem is not resolved soon. Jimmy and the other corrupt cops rush to find Tezo, threatening his cousin who finally reveals Tezos location when Jimmy threatens to burn his baby son with an iron. 

Ray also discovers Tezos location and, while waiting for backup, enters the building when he hears gunshots. He discovers Jimmy and his gang as they are torturing Tezo to death. Jimmy kills Tezo with Rays gun and tells Ray he can be the hero of the manhunt for the cop killer. Ray attacks Jimmy, but realizes he cannot do much as it was his gun that killed Tezo. Ray calls Franny to tell him what happened. Franny, now realizing the extent of Jimmys corruption, is initially hesitant to risk his position by admitting involvement, but his terminally-ill fiancé insists that Franny do the right thing in order to be a role model for her children.

Sandy calls a reporter to do an expose on the corrupt cops, then kills himself. The police blame Sandy and the four dead cops from the raid. Francis Jr. confronts Jimmy, who is surprised that Francis Jr. is mad, insisting that he knew what was happening. Francis Jr. admits he allowed Jimmy and the cops some leeway to make extra money, but is furious at how far they took the corruption. He tells Jimmy he wont allow him to frame Ray for murdering Tezo. Jimmy offers Francis "his cut", but Francis refuses and leaves.

Ray tells Internal Affairs he didnt kill Tezo, but wont say who did. Francis Sr. sees Jimmys interview tape, in which Jimmy tells the investigators he saw Ray shoot Tezo. Francis Sr. tells Ray to go along with it to protect the family, but Ray refuses. Francis Jr. says that he will come clean, against his fathers wishes.

Two members of Jimmys corrupt crew try to rob a liquor store, which goes badly. A bystander and one of the two cops are killed, and the other holds the store owner hostage as both police and local gangsters look on getting increasingly rowdy. Francis Jr. goes to the liquor store to talk his officer out of holding the owner hostage. He manages to get the officer out of the store alive as police hold back the people on the streets, who are on the verge of rioting thanks in large part to Tezos cousin announcing what Jimmy did to him and his family. 

Ray confronts Jimmy at the bar. They fight, and Ray emerges victorious. When Ray is walking a handcuffed Jimmy to his cruiser, the crowd surrounds them looking for vigilante justice. Jimmy asks Ray to uncuff him and give him back his gun, but Ray refuses. Jimmy offers himself to Tezos cousin to be beaten to death, while the mob holds Ray back. Ray staggers away from the mob and his dead brother-in-law, to the defused hostage scene at the liquor store. Francis Jr. and Ray get in a cruiser and drive off.

The film ends with Ray, Francis Jr. and Francis Sr. about to give testimony.

==Main cast (in credits order)==
 
* Colin Farrell as Jimmy Egan
* Edward Norton as Ray Tierney
* Jon Voight as Francis Tierney, Sr.
* Noah Emmerich as Francis Tierney, Jr.
* Jennifer Ehle as Abby Tierney
* John Ortiz as Ruben Santiago
* Frank Grillo as Eddie Carbone
* Shea Whigham as Kenny Dugan
* Lake Bell as Megan Egan
* Carmen Ejogo as Tasha
* Manny Perez as Coco Dominguez
* Wayne Duvall as Bill Avery Ramon Rodriguez as Angel Tezo
* Rick Gonzalez as Eladio Casado
* Maximiliano Hernández as Carlos Bragon
* Leslie Denniston as Maureen Tierney
* Hannah Riggins as Caitlin Tierney
* Carmen LoPorto as Francis Tierney
* Lucy Grace Ellis as Bailey Tierney
* Ryan Simpkins as Shannon Egan
* Ty Simpkins as Matthew Egan
 

==Production==

 , using corruption in the police force, as a metaphor for wider institutional corruption.  The script was optioned in June 2000 by Fine Line Features, a subdivision of New Line Cinema, and Joe Carnahan was hired to rewrite the script. Production on the film was expected to begin later in 2000, with Gavin OConnor directing and Greg OConnor producing. 

In 2001, the project was subject to a  , would be grounds for hanging." 

In September 2005, the rights were once more with New Line Cinema. Production president Toby Emmerich had been a fan of the script for several years, and the studio entered negotiations with Norton, Farrell and Emmerich to star.  Production was set to begin in New York City in January 2006,  though principal photography did not begin until the following month. 

Cinematographer Declan Quinn said that the biggest challenge was "  to find a fresh way to do a police drama where it feels real and not like something weve seen a hundred times before." 

==Release== No Country American Gangster. The Incredible Hulk, and In Bruges respectively.  The studio has not commented further on the delay, which angered OConnor. He blamed internal New Line politics for the delay, specifically chairman Bob Shaye, saying, "I dont think   believes in it, and hes decided hell only release   films. He never had the decency to call me." OConnor has said he will withhold delivery of his next script for New Line, Warrior, until he discovered the films fate, and also looked at the possibility of taking the film to another studio. In February 2008, OConnor held a screening at the headquarters of talent agency Creative Artists Agency|CAA, in order to publicize that the film may need a new distributor.   

OConnor said of the situation, "Weve delivered something special and unique, a film thats not for everybody but has something to say. Were all heartbroken." Norton blamed a wider industry "paralysis" for the problems, rather than New Line Cinema: "Were a victim of the moment, and I just hope they will either find a way to give the film its due or graciously let us do it with someone else." Farrell said he believed in the film and called the situation "bizarre". 

==Reception==

===Critical reaction===
The film was eventually released on October 24, 2008 in United States. Rotten Tomatoes reported that 35% percent of critics gave the film positive reviews, based on 140 reviews, with the critical consensus being that the film is "Formulaic in its plotting and clichéd in its dialogue, Pride and Glory did little to distinguish itself from other police procedurals."   The film was also accused of racism for its portrayal of Dominican Americans.   

===Box office=== Max Payne, and Beverly Hills Chihuahua,  respectively, from 2,585 theaters with a $2,423 average.

==Soundtrack==
 
The films original score was composed by Mark Isham.

==References==
 

==External links==
* 
* 
* 
* 
* 

 

 
 
 
 
 
 
 
 
 