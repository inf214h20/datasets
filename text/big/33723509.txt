Khoobsurat (1999 film)
 
{{Infobox film
| name           = Khoobsurat
| image          = Khoobsurat_(1999_film).jpg
| image_size     =
| caption        =
| director       = Sanjay Chhel
| producer       = Rahul Sughand	
| writer         = Sanjay Chhel	
| narrator       =
| starring       = Sanjay Dutt   Urmila Matondkar 
| music          = Jatin Lalit
| cinematography = Madhu Ambat
| editing        = Aseem Sinha	 
| distributor    = 
| released       = November 26, 1999
| runtime        = 
| country        = India Hindi
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}

Khoobsurat (  Bollywood film directed by Sanjay Chhel and produced by Rahul Sughand. It stars Sanjay Dutt and Urmila Matondkar in pivotal roles. 

==Plot==

The story revolves round a con man Sanju (Sanjay Dutt) who has a kind heart. He gets into a fight with Jogia Seth (Paresh Rawal), a smuggler whom he owes Rs. 50 lakhs. Sanju has to make the money in a month if he wants save himself and Gudia, an orphan who becomes a hostage. At this moment Natwar (Johny Lever) comes to Sanjus rescue. Natwar advises Sanju to go into the Chaudhary household as Sanju Shastri and squander the money from them. As Sanju has no other choice, he decides to enter the Chaudhary Villa as their NRI relative. Sanju starts befriending the members of the Chaudhary family.  The Chaudhary Parivar has three brothers. The elder one is Dilip (Om Puri), who is an honest man and takes care of the family business. Mahesh (Ashok Saraf) is the second one who is a gambler. The third, Satish (Jatin Kanakia) has the habit of forgetting things and is absent minded.

Shivani (Urmila Matondkar) is the daughter of Dilip. Shivani is beautiful but without self-esteem or confidence. This makes the whole family anxious about Shivanis marital prospects. Sanju comes on the scene and wins everybodys hearts by solving their problems. Naturally, he falls in love with Shivani. The rest of the film is about how Sanju transforms Shivani from a simple girl to a beautiful woman. And, in the process, unites the rest of the family.

==Cast==
* Sanjay Dutt...Sanju (Sanjay Shastri)
* Urmila Matondkar...Shivani
* Om Puri...Dilip Chaudhary (Shivanis father)
* Farida Jalal...Sudha Chaudhary (Dadiji)
* Anjan Srivastav...Dinanath Chaudhary (Dadaji) 
* Ashok Saraf...Mahesh Chaudhary 
* Himani Shivpuri...Savita (Maheshs wife)
* Jatin Kanakia...Satish Chaudhary 
* Johnny Lever...Natwar (Sanjus friend)

==Soundtrack==
{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Aana Zara Paas To Aa" 
| Kumar Sanu, Anuradha Paudwal
|-
| 2
| "Aye Shivani"
| Sanjay Dutt, Shradha Pandit
|- 
| 3
| "Azmale Ye Formule" Abhijeet Bhattacharya|Abhijeet, Shradha Pandit
|-
| 4
| "Bahut Khoobsurat Ho"
| Abhijeet, Neerja Pandit
|-
| 5
| "Barse Kyon Barsaat" Narayan Parshiuram, Sanjivani
|-
| 6
| "Ghash Khake Ho Gaya"
| Sukhwinder Singh
|-
| 7
| "Ghoonghat Mein Chaand"
| Kumar Sanu, Kavita Krishnamurthy
|-
| 8
| "Main Adhuri Si"
| Anuradha Paudwal
|-
| 9
| "Mantra of Khoobsurat"
| Narayan Parshiuram 
|-
| 10
| "Mera Ek Sapna Hai"
| Kumar Sanu, Kavita Krishnamurthy 
|}

==References==
 

==External links==
* 

 
 
 
 