Mourning Becomes Electra (film)
{{Infobox film
| name      = Mourning Becomes Electra
| image     = Mourning Becomes Electra (film).jpg
| director  = Dudley Nichols
| writer    = Dudley Nichols Eugene ONeill  (play)
| producer  = Dudley Nichols
| studio    = RKO Radio Pictures
| distributor = RKO Radio Pictures
| starring  = Rosalind Russell Michael Redgrave Raymond Massey Katina Paxinou Leo Genn Kirk Douglas Nancy Coleman Henry Hull Sara Allgood Thurston Hall Walter Baldwin Elisabeth Risdon Erskine Sanford Jimmy Conlin Lee Baker Tito Vuolo Emma Dunn Nora Cecil Marie Blake Clem Bevans Jean Clarenden
| music     = Richard Hageman George Barnes Chandler House
| runtime   = 173 minutes
| country   = United States
| language  = English
| released  =  
| budget = $2,342,000 Richard Jewel, RKO Film Grosses: 1931-1951, Historical Journal of Film Radio and Television, Vol 14 No 1, 1994 p46 
| gross = $435,000 
}} play of the same title. The film stars Rosalind Russell, Michael Redgrave, Raymond Massey, Katina Paxinou, Leo Genn and Kirk Douglas. 
 Best Actor Best Actress in a Leading Role (Rosalind Russell). Originally released by RKO Radio Pictures at nearly three hours, it was eventually edited down to 105 minutes (losing more than an hour) after it performed poorly at the box office and won no Oscars. It has since been restored to its full length and shown on Turner Classic Movies.
 The Farmers Daughter.

The movie recorded a loss of $2,310,000, making it one of RKOs biggest financial disasters. 

==Plot==
A wealthy New England family, the Mannons, await the return of patriarch Ezra, general for the Union in the Civil War, and son Orin, a timid young man before becoming an Army officer.

Lavinia, who adores her father, is shocked to see mother Christine kissing another man. Worse yet, the man is sea captain Adam Brant, someone whom Lavinia has long fancied herself, even though Peter Niles has been courting her.

Learning from family servant Seth that, complicating matters further, Adam is actually a relative, a son of Lavinias uncle, causes her to confront her mother. To her astonishment, Lavinia finds out that Christine is completely aware of the family relationship, but has hated Ezra since the day she married him.

Adam has hidden motives as well. He hates the Mannons for the way they treated his mother, who is now deceased. He is seeking revenge by toying with Christines affections. But when she comes to him with a plot to kill Ezra, poisoning him, Adam is reluctant to go that far with his scheme.

Ezra returns home. His harrowing experiences during the war have persuaded him to try for a closer relationship with his wife. Christine is thrown by this, but elects to proceed with her plan to kill him. Ezra does indeed die, but Lavinia comes into possession of the pills that her mother used to poison him.

One tragedy follows another. Orin decides to shoot Adam for what hes done. Christine commits suicide. Lavinia decides to marry Peter after all, but his sister Hazel discloses that her family has all but disowned Peter for having anything to do with the Mannons.

Orin is riddled with guilt and kills himself as well. All is lost for Lavinia, who has Seth nail shut the doors and windows to their home, locking herself away from the world forever.

==Cast==
* Rosalind Russell as Lavinia
* Raymond Massey as Ezra
* Michael Redgrave as Orin
* Leo Genn as Adam
* Katina Paxinou as Christine
* Henry Hull as Seth
* Kirk Douglas as Peter

==References==
 

==External links==
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 