The Getaway (1972 film)
{{Infobox film
| name           = The Getaway
| image          = Getaway211.JPG
| caption        = Original U.S. theatrical poster
| director       = Sam Peckinpah
| producer       = Mitchell Brower David Foster Walter Hill
| based on       =   Steve McQueen Ali MacGraw
| music          = Quincy Jones
| cinematography = Lucien Ballard
| editing        = Robert L. Wolfe
| studio         = First Artists Solar Productions Foster-Brower Productions
| distributor    = National General Pictures (1972,original) Warner Bros (2005, DVD, and 2007, Blu-ray DVD)
| released       =  
| runtime        = 122 minutes
| language       = English 
| budget         = $3,352,254
| gross          = $36,734,619  
}}
The Getaway is a 1972 American action-crime film directed by Sam Peckinpah and starring Steve McQueen and Ali MacGraw.
 Jim Thompson novel of Walter Hill. Ben Johnson, Al Lettieri, Sally Struthers, Jack Dodson and Slim Pickens.

A box office hit earning over $36 million in the United States alone, the film was one of the most financially successful productions of Peckinpahs and McQueens careers.
 remade in 1994 starring Alec Baldwin and Kim Basinger.

==Plot==
Carter "Doc" McCoy (McQueen), in prison in Texas, is denied parole. When his wife Carol (MacGraw) visits him, he tells her to do whatever necessary to make a deal with Jack Benyon (Johnson), a corrupt businessman in San Antonio.

Benyon uses his influence and gets Doc paroled on the condition that he takes part in a bank robbery with two of his minions, Rudy (Lettieri) and Frank (Bo Hopkins). During the robbery, Frank kills a guard. Rudy attempts a doublecross, shooting Frank and drawing a gun on Doc, who beats him to the draw and shoots Rudy several times.

Doc meets with Benyon, who attempts a doublecross before Carol shoots and kills him. Doc realizes that Carol had sex with Benyon to secure his release from prison. Doc angrily gathers up the money and, after a bitter quarrel, the couple flees for the border at El Paso.

A bloodied Rudy, having secretly worn a bulletproof vest, is alive. He forces rural veterinarian Harold (Jack Dodson) and his young wife Fran (Sally Struthers) to treat his injuries, then kidnaps them to pursue Doc and Carol.
 con man Richard Bright) swaps locker keys with Carol and steals their bag of money. Doc follows him onto a train and forcefully takes it back. The injured con man and witnesses are taken to the police station, where they identify Docs mug shot.

Carol buys a car while Doc steals a shotgun, leading to several shoot-outs and police chases. The couple escapes by hiding in a large trash bin, only to end up in the back of a garbage truck and dumped at the local landfill. Filthy and frustrated, they argue about whether to stay together or split up. They decide to see things through.

Rudys attraction to the veterinarians wife results in the two having sex in front of her husband. Humiliated, the vet hangs himself in a motel bathroom. Rudy and Fran move on, barely acknowledging the suicide. They arrive at an El Paso hotel used by criminals as a safe house, aware that the McCoys will be heading to the same place.

Doc and Carol check into a room on the same floor. They ask for food to be delivered; but the manager, Laughlin (Dub Taylor), says he is working alone and cant leave the desk. Doc soon realizes that Laughlin sent away his family because something is about to happen. He urges Carol to dress quickly so they can escape.

An armed Rudy comes to their door while Fran poses as a delivery girl who needs their signature for the food. Peering from an adjacent doorway, Doc is surprised to see Rudy alive. He sneaks up behind Rudy and knocks him out and does the same to the screaming Fran.

Cully and his thugs arrive as the McCoys try to leave. A violent gunfight ensues in the halls, stairwell, and elevator; and all of Cullys men are killed but one, whom Doc allows to walk away. Rudy comes to his senses and follows, but Doc shoots and kills him on a fire escape.

With the police en route, the couple hijack a pickup truck and force the driver, a cooperative old cowboy (Slim Pickens), to take them to Mexico. After crossing the United States-Mexico border|border, Doc and Carol pay the cowboy $30,000 for his truck. Overjoyed, the cowboy heads back to El Paso on foot, while the couple continues on into Mexico.

==Cast==
*Steve McQueen as Carter Doc McCoy
*Ali MacGraw as Carol Ainsley McCoy Ben Johnson as Jack Beynon
*Sally Struthers as Fran Clinton
*Al Lettieri as Rudy Butler
*Roy Jenson as Cully Richard Bright as The Thief (a con man)
*Jack Dodson as Harold Clinton
*Slim Pickens as a Cowboy
*Bo Hopkins as Frank Jackson
*Dub Taylor as Laughlin

==Production== Le Mans. Foster acquired the rights to Jim Thompsons crime novel The Getaway. Foster sent a copy of the book to McQueen, urging him to do it. The actor was looking for a good/bad guy role and saw these qualities in the novels protagonist. 

Foster looked for a director and Peter Bogdanovich came to his attention. Terrill 1993, p. 221.  McQueen screened Bogdanovichs soon-to-be released The Last Picture Show and loved it. They met with the director and a deal was made. However, Warner Brothers approached Bogdanovich with an offer to direct Whats Up, Doc? (1972 film)|Whats Up, Doc? starring Barbra Streisand, with the stipulation that he had to start right away. The director wanted to do both, and the studio refused. When McQueen found out, he was upset and told Bogdanovich that he was going to get someone else to direct The Getaway. Terrill 1993, p. 222. 

McQueen had recently worked with Peckinpah on Junior Bonner and enjoyed the experience. He recommended that Foster approach him. Peckinpah, like McQueen, was in need of a box office hit and immediately accepted. The filmmaker read the novel when it was originally published and talked to Thompson about filming it when he was starting out as a director. 
 Robert Evans, allowing Peckinpah to do his personal project if he first directed The Getaway. The director was soon dismissed from Emperor and told that Paramount was not making The Getaway. 

A conflict arose with Paramount concerning the films budget. Terrill 1993, p. 226.  Foster had 30 days to set up a new deal with another studio or Paramount would own the rights. He was inundated with offers and went with First Artists Group because McQueen would receive no upfront salary, just 10% of the gross for the first dollar taken in on the film. This would be a very profitable if the film was a box-office hit. 

===Screenplay=== El Rey, Walter Hill.  Hill had been recommended by Polly Platt, wife of Peter Bogdanovich, who was then still attached to direct; Platt had been impressed by Hills work on Hickey & Boggs. Hill says Bogdanovich wanted to turn the material into a more Hitchcock type thriller, but he had only gotten 25 pages in when McQueen fired the director. Hill finished the script in six weeks, then Sam Peckinpah came on board. {{cite news
 | last = McGilligan
 | first = Patrick
 | coauthors =
 | title = Walter Hill: Last Man Standing
 | work =
 | pages =
 | language =
 | publisher = Film International
 | date = June 2004
 | url = http://www.filmint.nu/?q=node/23
 | accessdate = November 28, 2007 }} 

Peckinpah read Hills draft and the screenwriter remembered that he made few changes: "We made it non-period and added a little more action." Terrill 1993, p. 224.  Hill:
 I didn’t think you could do Thompsons novel. I thought you had to make it more of a genre film. THOMPSONS novel is strange and paranoid, has this fabulous ending in an imaginary city in Mexico, criminals who bought their freedom by living in this kingdom. Its a strange book. Its written in the fifties, takes place in fifties, but it is really a thirties story. I did not believe that if you faithfully adapted the novel the movie would get made, or that McQueen would get the part. There was a brutal nature to Doc McCoy that was in the book that I thought you werent going to be able to go that far and get the movie made. I found myself in this strange position, trying to make it less violent.   accessed 12 July 2014  

===Casting=== Love Story.  She was married to Robert Evans, who wanted the former model to avoid being typecast in preppy roles.

Evans arranged a meeting with her, Foster, McQueen, and Peckinpah. Terrill 1993, p. 225.  According to Foster, she was scared of McQueen and Peckinpah because they had reputations as "wild, two-fisted, beer guzzlers."  McQueen and MacGraw experienced a strong instant attraction.  She said, "He was recently separated and free, and I was scared of my overwhelming attraction to him." 
 Richard Bright. Terrill 1993, p. 234.  Bright had worked with McQueen 14 years before but he did not have the threatening physique that McQueen pictured for Butler because the two men were the same height. Peckinpah got along famously with Bright and cast him as the train station con man instead.  Al Lettieri was brought to Peckinpahs attention by producer Albert Ruddy who was working with the actor on The Godfather. Like Peckinpah, Lettieri was a heavy drinker, which caused problems while filming due to his unpredictable behavior. 

===Principal photography=== San Marcos, San Antonio, Fabens and El Paso. Terrill 1993, p. 227.  Peckinpah shot the opening prison scenes at the Huntsville penitentiary, with McQueen surrounded by actual convicts. 

McQueen and McGraw began an affair during production. Terrill 1993, p. 228.  She would eventually leave her husband Evans and become McQueens second wife. Foster was worried that their relationship would have a negative impact by causing a potential scandal. Terrill 1993, p. 230. 

MacGraw got her start as a model and her inexperience as an actress was evident on the set, as she struggled with the role. Terrill 1993, p. 240.  According to Foster, the actress and Peckinpah got along well but she was not happy with her performance: "I looked at what I had done in it, I hated my own performance. I liked the picture, but I despised my own work." Terrill 1993, p. 241. 

Peckinpahs intake of alcohol increased dramatically while making The Getaway, and he was fond of saying, "I cant direct when Im sober." Weddle 1994, pp. 444-450.  He and McQueen got into the occasional heated arguments during filming. The director recalled one such incident: "Steve and I had been discussing some point on which we disagreed, so he picked up this bottle of champagne and threw it at me. I saw it coming and ducked. And Steve just laughed." Terrill 1993, p. 237. 

McQueen had a knack with props, especially the weapons he used in the film. Hill remembered, "You can see Steves military training in his films. He was so brisk and confident in the way he handled the guns." Terrill 1993, p. 238.  It was McQueens idea to have his character shoot two squad cars in the scene where Doc holds two police officers at gunpoint. 

Under his contract with First Artists, McQueen had final cut on The Getaway and when Peckinpah found out, he was upset. Richard Bright said that McQueen chose takes that "made him look good" and Peckinpah felt that the actor played it safe: "He chose all these Playboy (magazine)|Playboy shots of himself. Hes playing it safe with these pretty-boy shots." 

==Soundtrack==
Peckinpahs long-time composer and collaborator Jerry Fielding was hired to do the musical score for The Getaway. He had previously worked with the director on Noon Wine (1967), The Wild Bunch (1969), Straw Dogs (1971) and Junior Bonner (1972). After the films second preview screening, McQueen was unhappy with the music and used his clout to hire Quincy Jones to rescore the film.  Jones music had a jazzier edge and featured harmonica solos by Toots Thielemans, with Don Elliott credited for "musical voices." Jones was nominated for a Golden Globe award for his original score.

Peckinpah was unhappy with this action and took out a full-page ad in Daily Variety on November 17, 1972 including a letter he had written to Fielding thanking him for his work. Fielding would work with Peckinpah on two more films, Bring Me the Head of Alfredo Garcia (1974) and The Killer Elite (1975). Simmons 1982, pp. 165-167. 

==Release== eighth highest grossing picture of the year, making $36,734,619. It also earned $26,987,155 in worldwide rentals.

Its North American rentals for 1973 were $17,500,000. 

Walter Hill later recalled:
 I thought of the films I wrote, I thought it was far and away the best one, and most interesting. I thought Sam did a few things while shooting that was terrific. When they jump on the bus after they buy the gun, I just had them take the bus out of town. Sam had the bus circle around and come back through. That heightened the tension... I thought the stuff with the veterinarian got too broad and too sadistic for the rest of the film. But again I thought it was good film. It was not reviewed very well, but a huge hit. Biggest hit Sam ever had.... He would always say we did this one for the money which is one of those kind of half truths... He was well paid and the movie made a lot of money and the fact it was about the only film where his points meant anything, he took a fair amount of money out too. After all the disappointment and heartbreak of all these films he had never gotten any reward or been well paid, meant a lot to him.  

==DVD==
The Getaway was released to DVD by Warner Home Video on May 31st, 2005 as a Region 1 widescreen DVD, with a follow-up Blu-ray DVD release on February 27th, 2007.

== Notes ==
 

==References==
*Simmons, Garner (1982) Peckinpah, A Portrait in Montage. University of Texas Press. ISBN 0-292-76493-6
*Terrill, Marshall (1993) Steve McQueen: Portrait of an American Rebel. Plexus. ISBN 978-1-55611-380-2 
*Weddle, David (1994) If They Move...Kill Em!. New York: Grove Press. ISBN 0-8021-3776-8

== External links ==
 
* 
* 
* 
* 

 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 