Lady in the Fog
 
 
{{Infobox film
| name           = Lady in the Fog
| director       = Sam Newfield
| producer       = Anthony Hinds
| image	=	Lady in the Fog FilmPoster.jpeg
| writer         = Lester Powell   Orville H. Hampton
| starring       = Cesar Romero   Lois Maxwell
| music          = Ivor Slaney
| cinematography = Walter J. Harvey 
| editing        = James Needs
| studio         = Hammer Film Productions
| distributor    = Lippert Pictures (USA) Exclusive Films (UK)
| released       = 13 October 1952
| runtime        = 73 minutes
| country        = United Kingdom
| language       = English
}} British mystery film directed by Sam Newfield and starring Cesar Romero and Lois Maxwell.   An English woman asks for help from a visiting American detective to London to help her find out who has killed her brother. It was made by Hammer Films at the Riverside Studios in Hammersmith.

==Cast==
* Cesar Romero as Philip Phil ODell  
* Lois Maxwell as Margaret Peggy Maybrick  
* Bernadette OFarrell as Heather McMara  
* Geoffrey Keen as Christopher Hampden  
* Campbell Singer as Inspector Rigby  
* Alastair Hunter as Detective Sergeant Reilly  
* Mary Mackenzie as Marilyn Durant  
* Lloyd Lamble as Martin Sorrowby  
* Frank Birch as Boswell, the airport manager  
* Wensley Pithey as Sid, the bartender  
* Reed De Rouen as Connors, the thug  
* Peter Swanwick as Smithers  
* Bill Fraser as Sales Manager  
* Lionel Harris as Allan Mellon  
* Betty Cooper as Dr. Campbell, asylum superintendent

==References==
 

== External links ==
*  

 
 

 
 
 
 