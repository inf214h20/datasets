Wild Child (film)
 
 

{{Infobox film
| name           = Wild Child!
| rating         = 1000 out of 10
| image          = Wild child poster.jpg
| caption        = Promotional film poster Nick Moore
| producer       = Tim Bevan Eric Fellner Diana Phillips
| writer         = Lucy Dahl
| starring       = Emma Roberts Kimberley Nixon Juno Temple Linzey Cocker Sophie Wu Alex Pettyfer Natasha Richardson Aidan Quinn Michael Price
| cinematography = Chris Seager
| editing        = Simon Cozens
| studio         = StudioCanal Relativity Media Working Title Films Universal Pictures
| released       =  
| runtime        = 98 minutes
| country        = United States United Kingdom France
| language       = English
| budget         = $20 million
| gross          = $21,972,336
}} teen Romance romantic comedy-drama film starring Emma Roberts, Georgia King, Alex Pettyfer and Natasha Richardson. Wild Child is Richardsons last on-screen film appearance.

==Plot== affluent Californian teenager, ruins all her fathers girlfriends belongings. When her father angrily arrives, he tells her that he is sending her to boarding school in England. Arriving at the school, called Abbey Mount, Poppy meets the headmistress Mrs. Kingsley (Natasha Richardson), head girl Harriet Bentley (Georgia King), and her big sister Kate (Kimberley Nixon). She is miserable in her new life as she does not fit in and has no friends.  
 concussed girl outside for air where he asks her out on a date. 

The next day, Poppy discovers her talent for lacrosse and whips the schools team into shape, getting them into the finals. On the date, Poppy finds herself falling for Freddie. They kiss before Poppy returns to school. In her room, she finds her roommates reading an email supposedly sent by Poppy, stating that she thought all of her new friends were losers. She then finds that Freddie has been given a similar email and is thereafter ignored by her friends. With no one else to turn to, Poppy sneaks down to the cooks room and calls Ruby who accidentally reveals how much she dislikes Poppy and that she is seeing Poppys boyfriend. Feeling even more alone, Poppy starts playing with her lighter and accidentally setting a curtain on fire. Hearing footsteps, she quickly puts out the fire and runs off. A few minutes later, she looks out her window to see a fire and wakes the school. When they find that Drippy is missing, Poppy runs into the burning school to rescue her. After the fire is put out, Freddie finds her lighter and gives it back to her, refusing to listen to what happened. Just as she realizes that she no longer wants to leave, Poppy goes to the headmistress and confesses.

While Poppy waits for the Honour Court who will decide if she should be expelled, Freddie finds her crying and becomes convinced that the fire was an accident. During the hearing, Poppy tells her story while her roommates find out that Poppy did not send the emails. Going to the court, they stand up for Poppy and Harriet accidentally confesses to restarting the fire after Poppy had put it out. The movie is left off at the lacrosse finals. Poppys father comes to the game and is shocked by Poppys dramatic change in appearance and how much she looks like her mother who was also captain of the lacrosse team at Abbey Mount. Abbey Mount wins the lacrosse finals and Harriet is expelled. Some months later, Poppy and her friends (including Freddie) are in the process of relaxing in Malibu, California|Malibu. Poppy ignores Rubys phone calls, now fully aware that she was not the friend she thought she was. The film ends as they prepare to jump off the cliff that helped her towards finding herself and making proper friends.

==Cast==
* Emma Roberts as Poppy Moore, a spoiled teenager living in Malibu.
* Kimberley Nixon as Kate, Poppys roommate.
* Juno Temple as Drippy, Poppys roommate.
* Linzey Cocker as Josie, Poppys roommate.
* Sophie Wu as Kiki, Poppys roommate.
* Natasha Richardson as Mrs. Kingsley, the headmistress of Abbey Mount.
* Alex Pettyfer as Freddie Kingsley, Mrs. Kingsleys son and Poppys love interest.
* Georgia King as Harriet Bentley, the head girl of Abbey Mount.
* Ruby Thomas as Jane, Harriets friend/lackey.
* Aidan Quinn as Gerry Moore, Poppys father.
* Lexi Ainsworth as Molly Moore, Poppys sister.
* Shelby Young as Ruby, Poppys friend.
* Johnny Pacar as Roddy
* Eleanor Turner-Moss as Charlotte, Harriets friend/lackey.
* Shirley Henderson as Matron.
* Kelsey Sanders as Skye
* Nick Frost as Mr. Christopher, the hairdresser.
* Daisy Donovan as Miss Rees-Withers, the sports mistress. Jason Watkins as Mr. Nellist, the French teacher.
==Production==
The boarding school in the film was filmed at Cobham Hall in Kent. 
They also filmed in Haworth in west Yorkshire 
==Reception== Universal had planned a North American release in the summer of 2009, but canceled it and chose to release the movie directly to DVD.

Wild Child has a 42% "rotten" rating at Rotten Tomatoes, based on 21 reviews with the consensus that "More mild than wild. This tween comedy mess falls flat on its face due to poor characters, poor direction and poor jokes".  The Sun Online gave the film 2/5 saying "WILD? More like mild, unless you think short skirts and “horse face” put-downs are outrageous." Though Urban Cinefile gave Wild Child a much more favourable review stating "The film has an energy and honesty about it: its lively, funny and smart and the characters are appealing."

==DVD release== directly to DVD on November 17, 2009.

==Soundtrack==
The "Movie Soundtrack Party Album" is the soundtrack for the film of the same name. The soundtrack was available in the United Kingdom and Australia on August 18, 2008. In the United States, the soundtrack has not been released and it was expected for early 2009.
;Track listing Shut Up and Drive" - Rihanna
# "Let Me Think About It" - Ida Corr feat. DJ Fedde le Grand
# "About You Now" - Sugababes
# "Say It Right" - Nelly Furtado Annie
# If This Is Love" - The Saturdays
# "Heartbreaker (will.i.am song)|Heartbreaker" - will.i.am feat. Cheryl Cole
# "Sweet About Me" - Gabriella Cilmi
# "Cant Speak French" - Girls Aloud
# "Murder on the Dancefloor" - Sophie Ellis-Bextor
# "Ice Cream" - New Young Pony Club
# "Kiss with a Fist" - Florence and the Machine
# "Foundations (song)|Foundations" - Kate Nash Jack McManus
# "Come Around" - Timbaland ft. M.I.A. (artist)|M.I.A. Eve
# Real Wild Child" - Sarah Harding
# "Wild Child" - The Cat Eat Cat Dog Game

;Other songs
The following songs appeared in the movie and in trailers, although they were not included on the soundtrack for the film.
# Imran Hanif - "Set Em Up" Goose - "Black Gloves"
# Robbie Williams - "Angels (Robbie Williams song)|Angels"
# Children of Bodom - "Roadkill Morning"
# Britney Spears - "Toxic (song)|Toxic"  (Instrumental only, used on trailer) 
# Lindsay Lohan - "Playground" Real Wild Child"
# will.i.am - "I Got It from My Mama"
# Belinda Carlisle - "Heaven Is a Place on Earth"
# Adele - "Chasing Pavements"
# Ida Corr vs. Fedde Le Grand-"Let Me Think About It"

==References==
 

==External links==
*  
*  
*  
*  
*    at sadibey.com

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 