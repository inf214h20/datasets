Balu ABCDEFG
{{Infobox film
| name = Balu ABCDEFG
| image = balu2.jpg
| writer = Kona Venkat
| starring = Pawan Kalyan Shriya Saran Neha Oberoi Gulshan Grover
| director = A. Karunakaran
| producer = Aswani Dutt
| editing = Kotagiri Venkateswara Rao
| distributor = Vyjayanthi Movies & KAD Movies
| released = 6 January 2005
| runtime = 169 minutes
| language = Telugu
| music = Mani Sharma
| budget = 18crores
}} Telugu action crime film released on 6 January 2005 and was directed by A. Karunakaran. This film stars Pawan Kalyan, Shriya, and Neha Oberoi. The film was produced by C. Ashwini Dutt under his production company Vyjayanthi Movies. The movie was a good grosser at the box office; it ran for 100 days successfully in 8 theaters in Andhra Pradesh. The story of this film is loosely based on Jet Lis 2004 film Unleashed (film)|Unleashed. Later this film was remade in Dhallywood named Dhakar King starred Shakib Khan and Apu Bishwas. The film was later dubbed in Hindi as Aaj Ka Gundaraaj.

==Plot==
The story starts with a man threatens peoples of Rajapulla Market. Then Balu (Pawan Kalyan) comes.in that time we saw some comedies.Balu lives with his mother (Jayasudha), sister-in-law, his nephew and his friend (Sunil (actor)|Sunil). Then Balu comes across a girl named Priya (Shriya Saran). Balus mother thinks about her past and loses her conceius. In the hospital, Priya falls in love with Balu. After some songs, we see Balu is fighting with the man whom we saw at first and took the documents of Rajapulla Market. Then the man goes to his boss Khan (Gulshan Grover). It is revealed that Balu is not son of Rajeshwari Devi. His real name is Ghani. In flashback, he saves khan. so, Khan takes Ghani with him. After 20 years, Ghani is now Ghani Bhai, a rowdy. A man who is now working under Khan. In other side, a girl is raped by some people. Indu (Neha Oberoi) is very coward. But after meeting with Ghani she becomes brave and fights with the rapers of that girl. Indu and Ghani fall in love with each other. And Indu reveals about her family, her father (Suman) was an honest man. Some People killed her father and brother. This time it is revealed that Rajeshwari Devi is mother of Indu. Indu names Ghani as Balu. But Khan kills Indu. In Indus death time Ghani promises her that he will take care of Indus family. After that we come in present and saw Ghani in Hospital. After Ghani becomes well, Khans henchemen founds him and a fight starts in Khan and Ghani. At last Ghani kills khan and reunites with Priya.

==Cast==
  was selected as lead heroine marking her first collaboration with Pawan Kalyan.]]
* Pawan Kalyan ... Ghani/Balu
* Shriya Saran ...Priya
* Neha Oberoi ... Indu
* Gulshan Grover ... Khan
* Brahmanandam ... Taj Banjara Hotel Manager
*Jayasudha ... Rajeshwari Devi
* Sunil ... Balus friend
*MS Narayana...Landlord
*Duvvasi Mohan... Ghanis gang member
*Suman...Indus father

==Music==
{{Infobox album  
| Name = Balu ABCDEFG
| Type = Album
| Artist = Mani Sharma
| Cover =
| Released =  25 December 2004 (India)
| Recorded = Feature film soundtrack
| Length =
| Label =  Aditya Music
| Producer = Mani Sharma
| Reviews =
| Last album = Gudumba Shankar (2004)
| This album = Balu ABCDEFG (2005)
| Next album = Radha Gopalam (2005)
}}
The film has six songs composed by Mani Sharma. Chandrabose
* "Lokale Gelavaga" - Murali, K. S. Chithra; Lyricist - Jonnavittula
* "Athi Methani" - Ranjith (singer)|Ranjith, Mahalakshmi Iyer; Lyricist-Chandrabose
* "Neelo Jarige" - Hariharan (singer)|Hariharan, Shreya Ghoshal; Lyricist - Sirivennela
* "Hut Hutja" - Kunal Ganjawala; Lyricist - Nitin Raikwar
* "Kannu Kottina" - Udit Narayan, Sujatha Mohan|Sujatha; Lyricist - Sirivennela

==Crew==
* Director: A. Karunakaran
* Screenplay: A. Karunakaran
* Story: A. Karunakaran
* Dialogue: Kona Venkat
* Producer: Ashwini Dutt
* Music: Mani Sharma Ramesh
* Kalyan
* Art direction: Anand Sai
* Editing: Kotagiri Venkateswara Rao
* Costumes: Renu Desai
* Lyrics: Sirivennela Sitarama Sastry|Sirivennela, Chandrabose (lyricist)|Chandrabose, Jonnavithula

==Box-office performance==
* The film received positive reviews from the critics but the film was only a moderate grosser despite having a strong star cast, memorable music and a powerful performance by Pawan Kalyan.The movie was released in 72 countries worldwide by KAD Movies, this made it the first Telugu movie to be released in an African country.

==DVD==
The DVD version of the movie was released by KAD Entertainment, a 
special interview by Pawan Kalyan and other cast and crew was a 
special attraction

==External links==
*  

 

 
 
 
 
 


 