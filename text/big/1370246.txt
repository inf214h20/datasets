I Haven't Got a Hat
{{Infobox Hollywood cartoon
|cartoon_name=I Havent Got a Hat
|series=Merrie Melodies/Porky Pig
|image=
|caption= Isadore Freleng
|story_artist= Jack King
|voice_actor=Joe Dougherty   Billy Bletcher   Bernice Hansen   Elvia Allman
|musician=Bernard Brown
|producer=Leon Schlesinger Leon Schlesinger Productions
|distributor=Warner Bros. Pictures   The Vitaphone Corporation
|release_date=  (USA) Technicolor (two-color)
|runtime=7 minutes
|movie_language=English
}}
 Isadore Freleng Leon Schlesinger Productions as part of Warner Bros. Merrie Melodies series. Released by Warner Bros. on March 9, 1935, the short is notable for featuring the first appearance of several Warner Bros. cartoon characters, most notably future cartoon star Porky Pig. I Havent Got a Hat was one of the earliest Technicolor Merrie Melodies, and (because of Walt Disneys exclusive deal with Technicolor at the time) was produced using Technicolors RG color space|two-strip process (red and green) instead of its more expensive three-strip process.

==Background==
This Merrie Melodies cartoon, explicitly modeled after Hal Roachs Our Gang live-action shorts, introduces several new characters as grade school students in the hope that some would catch on. At the time, the only star for the more character-driven Looney Tunes series was Buddy (Looney Tunes)|Buddy, a meager replacement for the feistier Bosko who left Schlesingers studio with his creators Hugh Harman and Rudolf Ising.

The short introduces the following characters:
*Beans the Cat, a mischievous young cat voiced by Billy Bletcher
*Little Kitty, a nervous girl cat, voiced by Bernice Hansen in falsetto.
*Porky Pig, a stuttering pig voiced by Joe Dougherty
*Oliver Owl, a haughty owlet who taunts Beans. (His look is similar to the "Owl Jolson" character from I Love to Singa)
*Ham and Ex, (Hansen and Bletcher) twin singing puppies

Though the gags in this short are still fairly indicative of early 1930s cartoons, this short is significant for launching the career of Porky Pig. In addition, the short bears the faintest hint of the developing comic style that was to come in later Warner Bros. cartoons (Porkys and Kittys recitals).

==Plot summary== musical and recital for the benefit of teachers and parents.

The school talent show first features Porky Pig reciting Paul Reveres Ride but with his excessive stutter (causing him to recite his part with incredible strain and sweat on some moments). A small gag involves Porky pointing to offstage students to provide sound effects for his next poem The Charge of the Light Brigade (the underside of a turtles shell for a drum, and falling light bulbs for gunfire). However, he points to the wrong student, but the intended student takes his cue, and Porky points to the correct one. The class children whistle and cat-call which makes several stray dogs burst into the schoolhouse and chase poor Porky out.

Next up, Little Kitty attempts to recite "Mary Had a Little Lamb". She is so nervous that she forgets a couple lines (even confusing snow for corn flakes) and then proceeds with the rhyme but gradually speeds up her voice to a high pitch. Throughout her performance she is fidgeting and crossing her legs in a way to suggest she urgently needs the toilet. She reaches the end of the rhyme as she makes a hasty exit, to a building that may be the school outhouse.

Third, Ham and Ex sing the song "I Havent Got a Hat", written by Buddy Bernier and Bob Emmerich. During this performance, Oliver Owl refuses to share some candy with Beans.

When Oliver goes up for his piano recital, Beans decides it is time for payback and sneaks a stray cat and dog into the piano. Their commotion creates a virtuoso performance of Franz von Suppés Poet and Peasant overture to riotous applause. When the animals jump out of the piano (with the cat chasing the dog rather than vice versa) the ruse is revealed to the audiences disapproval and Oliver, humbled and vengeful, covers Beans in green ink from his pen, causing Beans to fall off his ladder and launch a pail of red paint onto Oliver. Caught in the same predicament, they shake hands as the cartoon ends. This end scene emphasizes the fact that this was a two-strip Technicolor cartoon, with only red and green hues. At the time (as stated before), the three-strip process (with blue hues added) was exclusive to Disney for use in cartoons. This contract ran out in the fall of 1935, and WB released their first three-strip Technicolor cartoon, Flowers for Madame, in November of that year.

==References==
 
 

;Further reading
*Beck, Jerry and Friedwald, Will (1989): Looney Tunes and Merrie Melodies: A Complete Illustrated Guide to the Warner Bros. Cartoons. Henry Holt and Company.
*Beck, Jerry.  (2005) Audio commentary on "I Havent Got a Hat" for the Warner Brothers DVD set Looney Tunes Golden Collection, Volume 3.

==External links==
* 
*  at the Big Cartoon Database
* 

 
 
 
 
 
 
 
 