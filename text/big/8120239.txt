Heyy Babyy
 
 
{{Infobox Film
| name           = Heyy Babyy

| image          = Heyybabyyposter.jpg
| caption        = Movie poster for Heyy Babyy Sajid Khan
| producer       = Sajid Nadiadwala
| writer         = Sajid Khan Milap Zaveri
| narrator       = 
| starring       =Akshay Kumar  Juanna Sanghvi Vidya Balan Fardeen Khan Ritesh Deshmukh Boman Irani Anupam Kher
| music          = Shankar-Ehsaan-Loy
| cinematography = Himman Dhamija
| editing        = Rameshwar s. bhagat Nadiadwala Grandson Entertainment
| distributor    = Eros International
| released       = 24 August 2007
| runtime        = 146 mins
| country        = India
| language       = Hindi
| budget         =  
| gross          =  
}}
 Sajid Khan. It released on 24 August 2007 to a good response.

==Plot==
Arush Mehra (Akshay Kumar) lives a fairly wealthy lifestyle in Sydney, Australia with roommates Tanmay Joglekar (Ritesh Deshmukh) and Ali Haider (Fardeen Khan). He works for a popular dance club, while Tanmay entertains children as Eddy Teddy and Ali takes care of their apartment. All three are womanizers and usually end up sleeping with different women. One day, they find a baby girl (Juanna Sanghvi) outside their door with a note instructing them to take care of her, since one of them is her father. The three men go to all the women they dated and slept with, but none claims the baby is theirs.

The men try to take care of the baby, but she becomes a huge pain, so they drop her off at a church. A big rainstorm occurs, and the baby develops an illness after being caught in the rain. The three men stay with the baby in the hospital, realizing how much they love her. She recovers, and the guys become changed men. They love her, pamper her, and grow an attachment towards her. they name her Angel. One day, a woman named Isha (Vidya Balan) comes to take Angel back, claiming the baby to be her daughter. The guys are shocked as Arush tells them about his past.

One year ago, Arush travelled to Delhi to attend his cousin Arjuns wedding. There, he met Isha Sahni, who also resides in Australia. He won her over and they spent the night together. Shortly after, Isha caught him in a compromising situation with Devika Sharma (Sindhura Gadde), Ishas friend, so the couple split up. Arush returned to Sydney and forgot about the incident.

The guys find it very difficult to live without Angel. Arush ends up challenging Isha to marry someone faithful within seven days who will accept Angel: If shes unsuccessful, she will have to give Angel back to him. They sign a contract to finalise the deal. The men worry that they might lose the bet since Isha is beautiful and wealthy. Arush plans with Tanmay, Ali and Bharat who is Ishas father in order to keep Angel with him. 

His first attempt involves Ali posing as a botany professor named Parimal Tripathi who speaks very pure Hindi. Bharat is impressed by Parimal by this. They manage to get through a few days of the week until one day Isha asks him about marriage for the next day. After Isha says this, Ali talks to Arush and Tanmay who tell him to go to Disneyland where Ali gets Bharat attacked. He is stopped from further efforts by Tanmay in the Eddy Teddy costume. Bharat decides that Tanmay is the right man for Isha. Tanmay, Arush and Ali make plans to stop Isha from marrying someone else. As the contract is about to terminate, Isha somehow manages to find out the truth that she has been cheated by her father, Tanmay, Ali and most of all Arush. 

Isha goes with Angel to her private jet to go somewhere very far because she has lost the deal. As she is about to leave she is stopped by some cops because Ali and Tanmay called them. As they are arguing, Arush turns up showing Isha the contract and tearing it up indicating that Isha now has every right over Angel. But before the three men leave, Arush says that a child needs a mother the most but it also needs a father. Just as they are leaving, Angel says her first word "Dada" but Isha takes her away in the jet.

The three men are depressed that they may never see Angel again until they are surprised to see her on their doorstep. Isha finally admits her love for Arush and the film ends with their marriage taking place

==Cast==
* Akshay Kumar as Arush Mehra
* Juanna Sanghvi as Angel 
* Vidya Balan as Isha Sahni
* Fardeen Khan as Ali Haider
* Ritesh Deshmukh as Tanmay aka Eddy Teddy
* Boman Irani as Bharat Sahni
* Aarti Chhabria as Alis ex-girlfriend
* Anupam Kher as Malhotra
* Shahrukh Khan as Raj Malhotra (special appearance in Mast Kalander song)
* Payal Rohatgi (special appearance)
* Sindhura Gadde as Devika Sharma
* Chirag Vohra as Arjun
* Muskaan Mihani as Arjuns bride
* Anupam Sharma as the doctor
* Jennifer Mayani the Supermarket Girl
* Vrajesh Hirjee as Ajay Shah, bridegroom of Devika Sharma
* Bikramjeet Kanwarpal as Ishas lawyer

Special appearance in title song (in order of appearance)
* Malaika Arora
* Celina Jaitley
* Minissha Lamba
* Amrita Rao
* Tara Sharma
* Neha Dhupia
* Diya Mirza
* Ameesha Patel
* Sophie Choudry
* Masumeh Makhija
* Koena Mitra
* Riya Sen
* Amrita Arora
* Shamita Shetty

==Production==
Some shooting took place in Australia    and in Filmistan Studio in Mumbai.   

An Australian features in a promotional video which was not to be in the film. The girl band from Sydney are called the Australian Divas.   

==Soundtrack==
{{Infobox album |  
 Name = Heyy Babyy
| Type = Album
| Artist = Shankar-Ehsaan-Loy
| Cover = Heyybabyysoundtrack.jpg
| Released =  15 July 2007 (India)
| Recorded =  Feature film soundtrack
| Length = 
| Label =  T-Series
| Producer = Shankar-Ehsaan-Loy
| Reviews = 
| Last album = Jhoom Barabar Jhoom (2007)
| This album = Heyy Babyy (2007)
| Next album =   (2007)
}}
The music is composed by Shankar-Ehsaan-Loy and lyrics are penned by Sameer (lyricist)|Sameer.

{{tracklist
| extra_column    = Artist(s)
| title1          = Dholna   
| extra1          = Sonu Nigam, Shreya Ghosal
| length1         = 4:01   
| title2          = Dholna – Love Is in the Air Remix
| extra2          = Sonu Nigam, Shreya Ghosal
| length2         = 4:22
| title3          = Heyy Babyy
| extra3          = Loy Mendonsa, Neeraj Shridhar, Pervez Quadri, Raman Mahadevan
| length3         = 4:39
| title4          = Heyy Babyy – The Big O Remix
| extra4          = Loy Mendonsa, Neeraj Shridhar, Pervez Quadri, Raman Mahadevan
| length4         = 5:28
| title5          = Heyy Babyy featuring Girl Band
| extra5          = Akbar Sami
| length5         = 4:03
| title6          = Jaane Bhi De
| extra6          = Shankar Mahadevan, Loy Mendonsa
| length6         = 3:56
| title7          = Jaane Bhi De – Hiphop Hiccup Remix
| extra7          = Shankar Mahadevan, Loy Mendonsa
| length7         = 4:16
| title8          = Meri Duniya Tuhi Re
| extra8          = Sonu Nigam, Shaan (singer)|Shaan, Shankar Mahadevan
| length8         = 5:53
| title9          = Mast Kalandar
| extra9          = Shankar Mahadevan, Master Saleem, Rehan Khan
| length9         = 5:53
}}

===Reception===
The album received favourable reviews from major critics. Joginder Tuteja of   described the album as "a peppy soundtrack".    The Planet Bollywood review, meanwhile, called it "a fun album" and gave it 7.5 stars.   

The album made its debut at No. 6 in the charts,  later climbed up to the top 5 and remained consistent in the middle of the charts. 

==Reception==

===Reviews===
Heyy Babyy opened to positive reviews, though many criticised the slow pace in the second half. Taran Adarsh gave the film 4 out of 5 stars saying it "is an entertainer that has something for everyone". He praised the performances of the lead stars.    Critic Aparajita Ghosh gave 3 out 5 stars, saying "Heyy Babyy packs in enough masala and emotion to keep you entertained throughout its running time."  Noyon Jyoti Parasara of AOL India stated "Sajid Khans debut is one movie you can go and watch and leave your brains behind. Go laugh aloud!" 

===Box office===
Heyy Babyy opened to packed houses in over 650 cinemas in India. The opening was at 90%+ and continued to do well in the days following.    It grossed   in India, $1.4 million in the USA and £763,000 in the UK and was declared a "super-hit". 

It was a hit abroad, opening to a favorable response and taking the second biggest Hindi film opening in the UK after   (2007).    Heyy Babyy debuted at number 10 and made £289,761 at a screen average of £4,765. 

==Home video==
The film released on DVD but was the first Hindi movie to be released on Blu-ray Disc too. 

==See also==
* Trois hommes et un couffin or Three Men and a Cradle, 1985 French original
* Three Men and a Baby, 1987 American remake

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 