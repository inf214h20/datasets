Kiki (1931 film)
{{Infobox film
| name           = Kiki
| image          = File:Kiki poster 1931.jpg
| image size     = 
| caption        = Theatrical release poster Sam Taylor
| producer       = Joseph M. Schenck
| writer         = Play: David Belasco  Screenplay: Sam Taylor Reginald Denny Alfred Newman
| cinematography = Karl Struss 
| choreography   = Busby Berkeley studio = Art Cinema
| distributor    = United Artists
| released       = March 14, 1931
| runtime        = 84 minutes
| country        = United States
| language       = English gross = $400,000 {{cite book
 | last = Balio
 | first = Tino
 | authorlink =
 | coauthors =
 | title = United Artists: The Company Built by the Stars
 | date = 2009
 | publisher = University of Wisconsin Press
 | isbn = 978-0-299-23004-3
 }} p93 
| preceded by    = 
| followed by    = 
}}
  Reginald Denny, Sam Taylor. 1926 version starring Norma Talmadge.  

==Plot== Reginald Denny). He, however, is really busy and is annoyed by her presence. To get her out of his office, he promises her job back. Before she leaves, she drops her purse and clippings of Victor shaped in hearts fall out. It becomes clear Kiki is secretly in love with him.

When the next show becomes a disaster because of Kiki, she is again fired. She goes complaining at Victor Randalls office for the second time. He is now charmed by her and invites Kiki to his apartment. There, she notices a photo of his ex-wife Paulette Vaile (Margaret Livingston). He kisses her, but she is insulted and slaps him. She hides in another room and makes clear she feels used and thinks Victor is still not over Paulette. 

She eventually falls asleep in the room and finds a letter from Paulette the next morning. Although its for Victor, she reads it. It says she is sorry about last night and wants to make up with Victor. Kiki becomes jealous and ruins the letter. Meanwhile, the servants are irritated by Kiki and try to get her out of Victors apartment. Victor confronts her when the servants inform him Kiki has stolen a few of Paulettes letters. He eventtually finds the letters and reads them.

Victor and Kiki have a conversation and flirt for the first time. Kiki becomes angry when Victor receives a phone call from Paulette and answers it. Paulette later visits Victors apartment. Kiki is outraged and tells Paulette she is in love with Victor and intends to marry him. Victor catches Kiki intimidating and scaring Paulette and orders her to get out.

Victor and Paulette fall in love with each other again, but they find out Kiki hasnt left the apartment. Kiki pretends to be unconscious. Victor puts her in bed to rest and Kiki kisses him. He tells Paulette he cant leave Kiki alone. Paulette feels betrayed and leaves him. Victor and Kiki finally fall in love and kiss.

==Cast==
*Mary Pickford as Kiki Reginald Denny as Victor Randall
*Joseph Cawthorn as Alfred Rapp
*Margaret Livingston as Paulette Vaile
*Phil Tead as Eddie
*Fred Walton as Bunson
*Edwin Maxwell as Dr Smiley

==Release==
The film was released in 1931. New York Times film critic Mordaunt Hall credited the film for its comedy and characterizations of the stars in the movie; however longtime Pickford fans were not used to the loose adult role that the star traded for her earlier ingenuousness and it eventually flopped at the box office.   

A copy of the film still exists at the UCLA Film and Television Archive. However, it has not been released on home video or DVD, the only Mary Pickford talkie not to be released.

It was the first Mary Pickford film since the formation of United Artists to lose money. 

== References ==
 

==External links ==
 
*  

 
 
 
 
 
 
 
 
 
 