Vennila Veedu
{{Infobox film
| name            = Vennila Veedu
| image           = Vennila Veedu Poster.jpg
| caption         = Film poster
| director        = Vetri Mahalingam
| producer        = P. V. Arun
| writer          = Vetri Mahalingam   (Story)     Iyndhukovilan   (Dialogues)  
| screenplay      = Vetri Mahalingam Senthil  Vijayalakshmi
| music           = Dhanraj Manickam
| cinematography  = D. Kannan
| editing         = V. J. Sabu Joseph
| distributor     = Adharsh Studio
| country         = India,Malaysia Tamil
| released        =  
| runtime         =
}}
 Indian Tamil Tamil family Senthil and Vijayalakshmi in the lead.   Cinematography is by D. Kannan, editing by V. J. Sabu Joseph while the soundtrack is scored by Dhanraj Manickam. The film released  on 10 October 2014 to mixed reviews. 

== Plot ==
The film tells the story of a village couple, Karthik (Senthil Kumar|Senthil) and Thenmozhi (Vijayalakshmi (Tamil actress)|Vijayalakshmi), who migrate to the city after their marriage. Karthik works as a manager in a firm, and they live happily and help everyone around according to their capacity and have a beautiful daughter, Vennila. Meanwhile, Ilavarasi (Srinda Ashab), a new neighbor in their apartment arrives, and she lives right next door. Ilavarasi is the daughter of a rich money lender. He is a greedy loan shark who goes to any extent to get his loaned money back from the people who borrowed it. He loves his daughter and will get anything she points at and will do anything for her. Ilavarasi, as a result of this upbringing, is an arrogant, spoilt brat of a woman and she is married to a spineless man who does whatever she bids him to do.  Though Thenmozhi does not have a good opinion about Ilavarasi in the beginning, they become friends after an incident where Ilavarasi thrashes a stalker who was harassing Thenmozhi repeatedly over phone. Soon, the women become good friends and enjoy each others company over the day and spend a lot of time together.

One day, when Karthik and Thenmozhi are preparing for a wedding in Karthiks employers family, Thenu says that she has only a simple jewelry and will feel out of place and belittled at such a grand wedding. Since Karthik is not in a comfortable financial state to buy her some jewels, she decides to ask Ilavarasi if she could borrow her necklace and return it after the ceremony, and Ilavarasi also happily obliges. The trouble begins when Thenmozhi loses the expensive necklace to a chain snatcher thief on their way back home. 

When Ilavarasis father learns of this, he becomes wild and scolds Thenmozhi, Karthik and even his own daughter and asks Karthik to lodge a formal complaint to the police. The policemens attitude towards  this issue is lethargic and they treat them with negligence. Meanwhile, the inspector tells to Ilavarasis father that while Thenmozhis own chain is still intact, his daughters jewelry alone is gone, so even Thenmozhi and Karthik could be possible suspects and this kind of cheating is the latest trend among robbers these days. Ilavarasis father hears this and poisons Ilavarasis mind also. She also misjudges certain incidents with Thenu, and distances herself slowly, believing her dad. One fine day, Ilavarasi throws a surprise party for Karthik and Thenus wedding anniversary and gifts an expensive showpiece. But day after day, Ilavarasis father continues to verbally abuse them and looks at them as thieves. Then, the baby Vennila gets kidnapped suddenly, and after some commotions, Karthik learns that Ilavarasi and her father have orchestrated the whole kidnap drama, to check if they have the money from selling the stolen necklace. Karthik is enraged and argues with Ilavarasis father who asks him to first find the stolen necklace or at least give the money for the jewelrys worth and then talk as much as he pleases. Karthik, unable to see his family suffer humiliation, agrees to give back the money somehow.

He goes to his village to sell a property leaving Thenu and Vennila at home. But here too, the land brokers make use of his helpless emergency situation and buy the land for exactly the amount Karthik owes Ilavarasis father, much lesser than the lands actual worth. Karthik comes back to the city with the money but is in for a rude shock. He finds that Thenu has committed suicide by hanging herself. He reads her suicide note, where she tells that there was a secret camera in the gift Ilavarasi gave for their anniversary and their intimate moments were filmed and leaked to the internet. Her stalker had seen this and verbally abused her in public. When she confronted Ilavarasi and her father about this, he told that they fixed the camera only to monitor their activities to find the truth about the theft and they are not responsible if the video is leaked to the internet somehow. When she argued with him, he slapped and humiliated her further. Unable to bear all the humiliation, she has taken this extreme step. 

Karthik is blinded with fury after reading this and when Ilavarasis father comes to get his money, he gives it to him and asks him to return his dead wife. This leads to an argument and he is beaten up by goons, but he bashes all of them and finally strangles Ilavarasis father to death with his own gold chains. No one except Ilavarasi stops Karthik, not even Ilavarasis husband. At the end, we see that Karthik cries out loud, looking at the state he left his baby daughter Vennila in.

== Cast == Senthil as Karthik Vijayalakshmi as Thenmozhi Pandi
* Srinda Ashab
* VJ Settai Senthil
* VJ Dindugal Saravanan

; Special appearance in promo song
* Sivakarthikeyan
* Premji Amaren
* Venkat Prabhu
* Udhayanidhi Stalin
* Kreshna
* Sreeja Chandran of Saravannan Meenatchi fame

== Soundtrack ==
{{Infobox album
| Name      = Vennila Veedu
| Longtype  = to Vennila Veedu
| Type      = Soundtrack
| Artist    = Dhanraj Manickam
| Producer  = Dhanraj Manickam
| Genre     = Film soundtrack Tamil
| Label     = Sony Music India
}}
 Airtel song Cheran and received by Thamizhachi Thangapandian and director R. K. Selvamani. 

; Tracklist
{{track listing
| extra_column    = Singer(s)
| lyrics_credits  = yes

| title1      = Silu Silu Mazhaiyum Karthik
| lyrics1     = Kabilan

| title2      = Naayana Oasai Ketten
| extra2      = Shakthisree Gopalan
| lyrics2     = Kabilan

| title3      = Aala Athattuthu Vayasu
| extra3      = Velmurugan, Padmalatha
| lyrics3     = Kabilan

| title4      = Johny Johny — Friendsbook Song
| extra4      = Gaana Bala, Dhanraj Manickam
| lyrics4     = Vetri Mahalingam

| title5      = Johny Johny — Gold Craze Song
| extra5      = Sathya Prakash
| lyrics5     = Vetri Mahalingam

| title6      = Vennila Veetukulla
| extra6      = Veena Arun
| lyrics6     = Vetri Mahalingam

| title7      = Theme
| extra7      = Humming by Shakthisree Gopalan
| lyrics7     = Instrumental
}}

== References ==
 

==External links==
*  

 
 
 
 
 