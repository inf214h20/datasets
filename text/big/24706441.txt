Twilight: Los Angeles (film)
 
{{Infobox Film

 | name = Twilight: Los Angeles
 | caption = 
 | director = Marc Levin
 | producer = Cherie Fortis Daphne Pinkerson Anna Deavere Smith Ezra Swerdlow Steven Tabakin
 | writer = Anna Deavere Smith
 | starring = Anna Deavere Smith
 | music = Camara Kambon
 | cinematography = Maryse Alberti Joan Churchill
 | editing = Bob Eisenhardt
 | distributor = Offline Releasing
 | released = 2000
 | runtime = 76 minutes
 | country = United States
 | language = English
 | image = Twilight- Los Angeles VideoCover.jpeg
}}
Twilight: Los Angeles is an American film starring Anna Deavere Smith and directed by Marc Levin.

==Plot==
In this film adaptation of the Broadway play,   Anna Deavere Smith performs her one woman show portraying various real life people involved in the aftermath of the 1992 Rodney King trial verdict riots in Los Angeles.

== Sources ==
*http://www.nytimes.com/2000/09/27/arts/27TWIL.html
*http://www.offoffoff.com/film/2000/twilightla.php
*http://www.imdb.com/title/tt0237865/
*http://www.variety.com/review/VE1117778697.html?categoryid=31&cs=1&p=0

 
 
 
 
 
 
 


 