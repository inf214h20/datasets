Meinu Ek Ladki Chaahiye
{{Infobox film
| name               = Meinu Ek Ladki Chaahiye
| image              =Theatrical_poster_of_film_Meinu_EK_Ladki_Chaahiye.jpg

| alt                = 
| caption            = 
| director           = Abhishek Bindal 
| producer           = Manoj  Bindal  Santosh Bindal
| writer             = Shekhar Kochgaven Abhishek Bindal
| editing            =  Swaminath Pandey
| starring           =  Raghubir Yadav  Puru Chibber   Reecha Sinha  Rashee  Zakir Hussain
| music              = Ravi Pawar    Shadab Bharti
| cinematography     = K.V. Ramanna
| studio             = BVM Flms Pvt. Ltd.
| distributor= Armstrong Media
| released           =  
| country            = India
| language           = Hindi
| gross              = 
}}

Meinu Ek Ladki Chaahiye  (  movie starring Raghuvir Yadav,  Puru Chibber, Reecha Sinha and Zakir Hussain (actor). The Movie is deals with the sensitive subject of rape cases. The backdrop of the movie is a false rape case and the hero’s fight for justice. 

==Plot==
Meinu Ek Ladki Chaahiye is a comical satirical Hindi movie starring Raghubir Yadav, Puru Chibber, Reecha Sinha, Zakir Hussain, Yatin Karyekar & Rashee Bindal. Govind (Raghubir Yadav) & his assistant Shishupal (Puru Chibber) get their first legal case after a lot of difficulties even though Govind’s father is a renowned lawyer. Govind, a jovial person does everything that he can to save the accused. It’s during the investigation that Govind and Shishupul come across some startling incidents. The case is of a serious nature but these two solve it in a very comical and hilarious way. However this case turns Govind’s life upside down. Harassment by the police, getting locked up inside a jail, wife and daughter’s hatred resulting in strained relations with them...Govind endures a lot. Even after all this, Govind is determined to find out the truth. This movie touches upon several issues in the country however in an entertaining manner. Since its Govind’s first case, will he and his assistant manage to save the accused? And will the culprits be brought to the book? Meinu Ek Ladki Chaahiye will answer all these questions.
. 

==Cast== Zakir Hussain.

* Irfan Razaa Khan as Firoz
* Raghubir Yadav  as Govind Ji Hero
* Puru Chibber as Shishupal Romantic Hero
* Reecha Sinha as Deepa Hiroine Zakir Hussain  as Lawyer Kharbanda
* Yatin Karyekar  as Shanti Bhushan
* Jyoti Gauva as Harshita (Govind Ji’s wife)
* Rashee as Mishrita (Govind Ji’s Daughter)
* Khushboo Purohit in Special Song
* Kashmira Kulkarni as Kamya
* Ahtesham Azad Khan as Kulbhushan
* Deepak Sinha as Inspector of Police  
* Sanjay Verma as Police Constable  

==Soundtrack==

{{Infobox album
| Name = Meinu Ek Ladki Chaahiye
| Type = Soundtrack
| Artist = Ravi Pawar, Shadab Bhartiy
| Cover = 
| Released =  
| Recorded = 2013 Feature film soundtrack
| Length = 23:06
| Label = Zee Music
| Producer = Ravi Pawar, Shadab Bhartiy
}}

The album is complied with songs of all kinds for every music lover. From a melodious sound track to a typical Punjabi song, Meinu Ek Ladki Chaahiye is a complete album. The biggest highlight of the album is an item number sung by Mamta Sharma of Munni Badnaam and Fevicol Se fame. Mamta has crooned on ‘Gori Chitti’ which has been specially composed by Shahdab Bhartiya while Sanjay Dhoopa Mishra is the lyricist.
 
Super-hit Mika Singh has sung Teri Toh Jhand and for the first time, audience will get to witness Raghubir Yadav’s dancing abilities.  Mesmerising Roop Kumar Rathod has voiced Nanhe Paon which beautifully emotes the unique relation between a parent and his daughter. There’s also a romantic track featuring Puru Chibber and Reecha Sinha ‘Main Sifar’. Javed Ali is the voice behind this love song. Ravi Pawar is the composer of all the songs and Sanjay Dhoopa Mishra is the lyricist.

{{Track listing
| extra_column = Singer(s)
| lyrics_credits = yes
| total_length = 23:06
| title1 = Gori Chitti
| extra1 = Mamta Sharma
| lyrics1 = Sanjay Dhoopa Mishra
| length1 = 5:21
| title2 = Raam naam Ladoo
| extra2 = Nikhi  Dsouza, Nitin Arora
| lyrics2 = Sanjay Dhoopa Mishra
| length2 = 4:35
| title3 = Teri To Jhand
| extra3 = Mika Singh
| lyrics3 = Sanjay Dhoopa Mishra
| length3 = 3:15
| title4 = Main Sifar Me
| extra4 = Javed Ali
| lyrics4 = Sanjay Dhoopa Mishra
| length4 = 4:53
| title5 =  Nanhe Paon
| extra5 = Roop Kumar Rathod
| lyrics5 = Sanjay Dhoopa Mishra
| length5 = 6:36
}}

==Reception==

Meinu Ek Ladki Chaahiye received mixed reviews and has been appreciated for the content of the film. RJ Jeeturaj gave 2.5 stars  while Bhavikk Sanghavi 0f planetbollywood.com gave 3 stars and said, “Raw. Real. Riveting. One of the most daring film of the year”. Another website JustBollywood.com gave 3.5 stars  and said, “A well timed movie and a beautiful satire which is worth watching considering the ace performance of Raghubir Yadav.” Also Celebexplore.com given 2.5 Stars. 
 
Filmytown gave 3 stars and said, “The movie touches upon several issues in the country however in an entertaining manner.” 

== References ==
 

==External links==
*  

 
 
 