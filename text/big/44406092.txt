Dozakh In Search of Heaven

{{Infobox film
| name = Dozakh In Search Of Heaven
| image = Dozakhthemovie1.jpg
| caption = 
| director = Zaigham Imam 
| production = AB Infosoft/ JALSA Pictures
| producer = Zaigham Imam
Pawan Tiwari
| writer = Zaigham Imam
| starring = Lalit Mohan Tiwari         
     Pawan Tiwari
| Cinematography = Mailesan Rangaswamy 
| music = Aman Pant
| released =  
| country = India
| language = Urdu
}}

Dozakh In Search of Heaven (English translation: The Hell In search of Heaven) is a Urdu film,written and directed by Zaigham Imam,  premiered at the Kolkata International Film Festival on 12 November 2013.  It has been jointly produced by Zaigham Imam(AB Infosoft) & actor Pawan Tiwari (JALSA Pictures). The film is an adaptation of a book by the same name. 

== Cast==
* Lalit Mohan Tiwari as Maulvi Sahab
* Nazim Khan as Panditji 
* Garrick Chaudhary as Janu  
* Pawan Tiwari as Auto driver
* Ruby Saini as Amma

==Festivals==
Dozakh has been screened at various film festivals across the world. These include Kolkata International Film Festival; Habitat Film Festival 2014,   Bangalore International Film Festival 2013; 12 Third Eye Asian Film Festival, Mumbai; Indian International Film Festival of Queensland 2014;  3rd Ladakh International Film Festival; Hidden Gems Film Festival, Canada. 

==Controversies==
The film was had a minor run in the censor board when it was denied a certificate at first two screenings. Mid-day reported on the same. 

==Critical reception==
DNA newspaper in a review of the film stated, "Dozakh reinforces the influence that cinema can have on the thought process of a society."  Deccan Herald wrote, "potent theme of religious tension will leave audiences spell bound". 

==References==
 
 http://www.tellychakkar.com/movie/movie-news/actor-turned-producer-pawan-tiwari-gears-the-world-wide-release-of-his-maiden-movie
 http://www.tellychakkar.com/tv/tv-news/actor-pawan-tiwari-and-writer-zaigham-imams-film-gets-appreciated-film-festivals-307

 http://www.liveindiahindi.com/film-dozakh-special-interview-of-pawan-tiwari

==External links==
*  
*  
* {http://www.tellychakkar.com/tv/tv-news/actor-pawan-tiwari-and-writer-zaigham-imams-film-gets-appreciated-film-festivals-307}


 
 
 
 
 


 