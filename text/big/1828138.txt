The People Under the Stairs
 
{{Infobox film
| name           = The People Under the Stairs
| image          = The People Under the Stairs Poster.jpg
| image_size     = 215px
| alt            =
| caption        = Theatrical release poster
| director       = Wes Craven
| producer       = Shep Gordon Wes Craven Marianne Maddalena Stuart M. Besser Dixie Capp Peter Foster
| writer         = Wes Craven Brandon Adams Everett McGill Wendy Robie A. J. Langer Ving Rhames Sean Whalen
| music          = Don Peake & Graeme Revell
| cinematography = Sandi Sissel
| editing        = James Coblentz
| studio         = Alive Films Universal Pictures
| released       =  
| runtime        = 102 minutes   
| country        = United States
| language       = English
| budget         = $6 million
| gross          = $31,347,154 
}} horror film Brandon Adams, Everett McGill, Wendy Robie, A. J. Langer, Ving Rhames and Sean Whalen.

==Plot==
Poindexter "Fool" Williams is a resident of a Los Angeles ghetto. He and his family are being evicted from their apartment by their landlords, the Robesons.

Leroy, his associate Spenser, and Fool sneak into the Robesons household by using Spenser to pose as a municipal worker. The Robesons leave the home shortly but Spenser doesnt return. Fool and Leroy,  break into the house to find Spencer, and find his dead body and a large group of strange pale children in a locked pen in a dungeon-like basement.

The Robesons return and Fool flees while Leroy is shot to death by Daddy. Fool runs into another section of the house, where he meets Alice. She tells him that the people under the stairs were children who broke the "see/hear/speak no evil" rules of the Robesons. The children have degenerated into cannibalism to survive and Alice has avoided this fate by obeying the rules without question. A boy named Roach whose tongue was removed also evades the Robesons by hiding in the walls.

Fool is discovered by Daddy and is thrown to the cannibalistic children to die. However, Roach helps Fool escape but succumbs to his wounds. As he dies, he gives Fool a small bag of gold coins and a written plea to save Alice. Fool reunites with Alice and the two escape into the passageways between the walls. Daddy releases Prince into the walls to kill them. Fool tricks Daddy into stabbing Prince and he and Alice reach the attic where they find an open window above a pond. Unfortunately, Alice is too afraid to jump and Fool is forced to go without her. He promises to return for Alice.

Mommy attempts to kill Alice but the cannibal children charge on her. Daddy finds Fool at the vault, where Fool sets off the explosives, which demolishes the house and causes the money to blow up through the crematorium chimney and into the crowd of people outside. Daddy is killed in the explosion and Alice and Fool reunite in the basement. Meanwhile, the people outside claim the money distributed by the blast, and the freed children venture into the night.

== Cast (selected) == Brandon Adams as Poindexter "Fool" Williams
* Everett McGill as Man / "Daddy" / Eldon Robeson
* Wendy Robie as Woman / "Mommy" / Mrs. Robeson
* A. J. Langer as Alice Robeson
* Ving Rhames as Leroy
* Bill Cobbs as Grandpa Booker
* Kelly Jo Minter as Ruby
* Sean Whalen as Roach
* Jeremy Roberts as Spenser

==Reception==

===Box office===
The film opened at the #1 spot at the box office, taking in over $5.5 million that weekend, and stayed in the top 10 for a month until early December.  The film went on to gross over $24,204,154 domestically (U.S.) and $7,143,000 internationally, bringing its worldwide total to $31,347,154.   

===Critical===
The People Under the Stairs has received a mixed reception from critics. It currently holds a 58% rotten rating on movie review aggregator website Rotten Tomatoes based on eighteen reviews.  Austin Chronicle wrote, "this is the work of the Wes Craven we came to admire". 

==Possible remake== remake the The Last 2009 remake of The Last House on the Left, Craven has not since mentioned a remake of either The People Under the Stairs or Shocker. 

==Release== The Serpent and the Rainbow on February 20, 2010 in the Aero Theatre in Santa Monica, California. 

==Attractions==
Universal Studios Florida incorporated some of the films plot as well as the house into a haunted house several times for their annual Halloween Horror Nights event. It is also featured on the drive-in movie screen in the Twister...Ride It Out attraction.

==See also==
* List of films featuring home invasions

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 