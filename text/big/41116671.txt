Sweet Dreams (2012 film)
{{Infobox film
| name           = Sweet Dreams
| image          = 
| caption        = 
| director       = Lisa Fruchtman Rob Fruchtman
| producer       = Lisa Fruchtman Rob Fruchtman Russell Long Tiffany Schauer
| writer         = 
| starring       = Kiki Katese Jennie Dundas Alexis Miesen
| music          = 
| cinematography = Rob Fruchtman Lex Fletcher
| editing        = Lisa Fruchtman Rob Fruchtman
| studio         = Liro Films
| distributor    = International Film Circuit
| released       = 19 July 2012 (Silverdocs Festival) 1 November 2013 (NYC theatrical release)
| runtime        = 86 minutes
| country        = United States
| language       = English Kinyarwandan
| budget         = 
| gross          = 
}}
Sweet Dreams is a 2012 documentary film about the Rwandan womens drumming troupe Ingoma Nshya, which was founded in 2005 by playwright Odile "Kiki" Katese with women from both sides of the 1994 Rwandan Genocide. The drumming troupes success then led to the opening of an ice cream store in 2010, which also brings together people from both sides of the genocide. The documentary was co-directed by siblings Lisa Fruchtman and Rob Fruchtman; Lisa Fruchtman had learned of the troupe and the plans for the shop from Katese in 2009.   

The film has been shown at many film festivals (Mill Valley Film Festival, Ashland Independent Film Festival, IDFA Amsterdam International Documentary Film Festival, the Savannah Film and Video Festival, Silverdocs, and DOC NYC). In late 2013, the film was released to theaters in New York City, Los Angeles, and San Francisco. 

Miriam Bale wrote in her New York Times review, "When viewers are facing the aftermath of genocide in Rwanda, in which hundreds of thousands of Tutsis were slaughtered in 1994, it’s easy to think that ice cream is a comparatively petty concern. But, thankfully, the sibling directors Lisa and Rob Fruchtman have made a nuanced and deftly edited film about a complex issue."    Scott Foudras wrote in Variety, "Although the 1994 genocide and its aftermath have been explored extensively in both narrative and nonfiction films over the past decade (especially documentarian Anne Aghion’s remarkable suite of films culminating in 2009’s My Neighbor My Killer), Sweet Dreams nevertheless forges its own path, dwelling less on the violent crimes of the past than on the small but meaningful ways in which a once-divided people are working to rebuild the social and psychological health of their country."   

 
==See also==
*Hotel Rwanda - 2004 film directed by Terry George based on the heroic efforts of hotelkeeper Paul Rusesabagina to save hundreds of refugees during the 1994 genocide.
*My Neighbor, My Killer - 2009 documentary by Anne Aghion describing the reconciliation process in Rwanda.

==References==
 

==Further reading==
*  Transcript of an interview with Rob Fruchtman that describes the process by which the filmmakers earned the trust of the women in Ingoma Nshya.

==External links==
*  Films official website
* 

 
 
 
 
 
 
 