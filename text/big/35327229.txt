National Red Cross Pageant
{{infobox film
| name           = National Red Cross Pageant
| image          = Guy Favières & Ina Claire.jpg
| imagesize      =
| caption        = Guy Favières and Ina Claire in the film
| director       = Christy Cabanne
| producer       = National Red Cross Pageant Committee
| writer         =
| starring       = 
| music          =
| cinematography =
| editing        = Mildred Richter
| distributor    = 
| released       =  
| runtime        = 5 reels
| country        = United States
| language       = Silent film(English intertitles)
}}
National Red Cross Pageant (1917) is an all-star revue silent film, now considered a lost film,    directed by Christy Cabanne.

==Production background==
On October 5, 1917, a live open-air pageant was held on a private estate, Rosemary Farm, near Huntington, New York. The event was mainly the brainchild of Ben Ali Haggin, famous as a stage designer. His wife appears in one of the episodes. The earnings from the live pageant itself went to the Red Cross.

Presumably the filming of the pageant was made with a patriotic fervor in the wake of the United States entry into World War I in April 1917. The proceeds from the film going to the war effort, such as the Red Cross, War Bonds, etc.

A litany of famous Broadway and motion picture stars of the period participated in the production. Lionel Barrymore|Lionel, Ethel Barrymore|Ethel, and John Barrymore all appeared in the production but not together in the same scenes as they did in Rasputin and the Empress (1932). 

==Synopsis==
Vogue (magazine)|Vogue magazine (November 15, 1917) wrote about the production:

"In the open-air theatre at “Rosemary Farm,” the Long Island estate of Mr. Roland R. Conklin was given, early in October, a most gorgeous pageant, which proved to be, at the same time, one of the most successful of war benefits. This pageant, which consisted of episodes from the history of each of the Allied nations, and the presentation of the case of each Ally before the bar of Truth, Justice, and Liberty, was organized by actors and actresses of the American stage as their contribution to the American Red Cross. It had been long in preparation, and many noted men and women had given generously of their time and effort, --an effort which found its reward, for this single performance brought a net profit of fifty thousand dollars, and the motion picture films which will carry the pageant all over the country will afford an additional income to the Red Cross for some time to come.
 Carnegie Institute and President of the Pageantry Association of America, and the rehearsals were under the personal direction of Mr. Stevens, Daniel Frohman, and B. Iden Payne Award|B. Iden Payne, while decorators and artists collaborated in the settings and costuming. The result was a pageant of rare beauty and dramatic worth, as well as of historic accuracy and patriotic inspiration.

Of the two parts which composed this pageant, the first was given over to historic episodes in the lives of the Allied nations and presented a glowing and sumptuous picture. The prologue, spoken by Edith Wynne Matthison, dedicated an altar to Peace and was followed by rhythmic dancing by Florence Fleming Noyes and her pupils. A scene from early Flemish days followed, and four famous cities, Bruges, Ghent, Ypres, and Louvain paid their allegiance to Flanders, personated by Ethel Barrymore in the gorgeous costume familiar in Flemish paintings.

The Italian scene which followed was succeeded by the scene of the birth of English liberty, as represented by King John signing the Magna Charta, and Medieval Russia was personified by John Barrymore as a tyrant borne upon the shoulders of his serfs. Most dramatic of the events of this first part, however, was the French episode, in which Ina Claire appeared as Jeanne D’Arc riding her white charger and the whole audience sprang to its feet in silent tribute to France.

In the second half of the pageant, called “The Drawing of the Sword,” each nation among the Allies appeared to present its case before the court of Truth, Justice, and Liberty. Serbia entered first and told her story of the opening of the war, to which Truth spoke assent. Belgium followed, and to her aid came England and France, while Russia came to the support of her ally, Serbia. Next, England called upon her overseas colonies, and Japan also, brought her pledge to maintain the cause of liberty on the Pacific. Armenia came to tell her wrongs; and Italy, shaking off the bonds of the Triple Alliance, cast her lot with the defenders of liberty. The grand climax was reached with the entry of America in the person of Marjorie Rambeau."

==Cast==
*Edith Wynne Matthison - Prologue
*Douglas Wood - Herald, Flemish episode
*Ethel Barrymore - Flanders, Belgium; Flemish and Final episodes
*Kitty Gordon - Bruges; Flemish episode
*Margaret Moreland - Ghent; Flemish episode
*Adelaide Prince - Ypres; Flemish episode
*Olive Tell - Louvain; Flemish episode
*Irene Fenwick - Herald; Italian episode
*Montgomery Irving - The Alps; Italian episode
*Annette Kellerman - The Mediterranean; Italian episode
*Josephine Drake - The Adriatic; Italian episode
*Ethel McDonough - Leader of the Lakes; Italian episode
*Norman Trevor - Herald; English episode
*George Backus - King John; English episode
*Marjorie Wood - Queen; English episode
*Maclyn Arbuckle - Baron Fitz-Walter; English episode (billed as Macklyn Arbuckle)
*Lumsden Hare - The Archbishop of Canterbury; English episode Eugene OBrien, Jeanne Eagels, Ben Ali Haggin. ]]
*Frank Keenan - The Secretary; English episode
*Frederick Truesdell - The Papal Legate; English episode
*Mrs. Ben Ali Haggin - Dance, The Pavane; French episode
*Clifton Webb - Dancer, The Pavane; French episode
*Ben Ali Haggin - Dunois, Defender of France; French episode
*Ina Claire - Jeanne DArc; French episode
*Guy Favières - Charles VII, the Dauphin; French episode
*John Barrymore - The Tyrant; Russian episode
*George F. Smithfield - The Fugitive; Russian episode Alice Fischer - Herald; Final episode
*Howard Kyle - Justice; Final episode
*Blanche Yurka - Truth; Final episode
*Gladys Hanson - Liberty; Final episode
*Tyrone Power, Sr. - Servia; Final episode (billed as Tyrone Power)
*E. H. Sothern - England; Final episode
*Rita Jolivet - France; Final episode Richard Bennett - Final episode
*Michio Itō - Japan; Final episode
*Marjorie Rambeau - America; Final episode
*Lionel Barrymore
*Mrs. H. P. Davison
*Hazel Dawn
*William T. Rock
*Helen Ware
*Frances White

==See also==
*List of lost films

==References==
 

==External links==
* 
* 
* 
* 

 
 
 
 
 
 
 