Bangles (film)
 
 
{{Infobox film
| name             = Bangles
| image            = Bangles film poster.jpg
| caption          = Theatrical release poster
| director         = Dr. Suvid Wilson 
| producer         = Dipti Biju Unni
| Production House = Citadel Cinemas
| writer           = Dr. Suvid Wilson     (story)   Shyam Menon    (screenplay & dialogue) 
| starring         = Ajmal Ameer Poonam Kaur Archana Kavi Tilakan
| music            = Dr. Suvid Wilson
                   Rajeev Alunkal(lyrics)
| cinematography   = Dileep Raman
| editing          = Don Max
| studio           = Citadel Cinema
| distributor      = Citadel Cinema
| released         =  
| country          = India
| runtime          = 1 hr 58 min
| language         = Malayalam
| budget         = 
| gross          = 
}}
Bangles ( ) is a 2013 Indian Malayalam-language thriller film written and directed by debutant Dr. Suvid Wilson who also composed the songs, starring Ajmal Ameer, Poonam Kaur, Archana Kavi, and Aparna Bajpai.
The film is produced by Dipti B Unni under the banner of Citadel Cinemas. The cinematography is handled by Dileep Raman and is edited by Don Max. Popular author Shaiju Mathew was also a part of the production house Citadel Cinemas.

The film is about a murderer, who leaves a few pieces of bangles at the crime spot.    It was released on 18 October.   

==Cast==
*Ajmal Ameer as Vivek, a cinematographer
*Poonam Kaur as Avanthika
*Archana Kavi as Angel
*Aparna Bajpai as Dancer
*Thilakan as Prof. Vincent Chenna Durai
*Thalaivasal Vijay as D.Y.S.P Solomon
*Joy Mathew as Priest

==References==
 

==External links==
*  

 
 
 
 


 