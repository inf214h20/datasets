O Pão e o Vinho
  Portuguese Documentary documentary feature Ricardo Costa, his second docufiction after Changing Tides (Mau Tempo, Marés e Mudança) – 1996/7. The third is Mists (Brumas) - 2003.

==History==
O Pão e o Vinho is contemporary to the  , Pitões a village of Barroso, Far away is the city and Further ahead on this road.

They are all  , since fictional elements have been added as part of the film narrative, in order to strength drama and highlight the nature of the subject (see visual anthropology). Bread and Wine is both docufiction and ethnofiction.

A common denominator for these films is that no conventional narrative is used. The story is told with poetic ellipses linking alternate actions and situations. Meaning arises from the flow of pictures, from the harmonic association of shots, slowly drawing a human portrait: a survivor.

All the Portuguese “ethnographic” films of these period involved passion (the portrait should be touching) and that means they are art films. António Campos, António Reis and some other left .important living “documents” of patrimonial interest that will be seen with emotion any time.

==Synopsis==
"During an Easter celebration, on a Good Friday, black night, hooded figures dressed with black mantles walk in deep silence, only broken by the noise of a rattle and by the canticles of a young Veronique rising in her hands a sheet with the bloody face of Christ stamped on it. She sings a song of grief and sorrow in an understandable language, telling an old story. The chorus of the hooded men answers to her complaints, echoed by a music band.

"At each turning point of the procession, now under a burning sun, other figures appear, bent on the earth, other voices, other sweaty faces. At each blow of the sickle, at each progress of the harvest, at each hit of the hoe, at each impromptu of a native poet, Anastásio Pires, Gil Quintas, or the bohemian Joaquim, the dishes’ seller, the portrait is drawn, the true story is told. The rattling and the Veronica’s song sounds again. And the procession proceeds until the moment in which, with a strong blow, the coffin cover falls over the body of Christ.

"The story which is told is of pain and wandering. The motive is the same as when people talk in front of a glass of wine, of a bit of bread with a slice of cheese. We can see that something happened, by that time, which was not deserved. Now we can see that, extinguished all hopes in a project that was not accomplished, the Alentejo, a singular and attaching land, still had a memory, was still alive. The actors of this film tell what happened. In each of these stories, the main character is Man." 

==Cast==
* Joaquim da Louça (native poet)
* Anastásio Pires (native poet)
* Gil Quintas (native poet)
*   (singer)
*   (singer) Redondo (Group of Singers from Redondo)

==Credits==
*  Production year – 1981
*  Format – 16 mm colour
*  Length – 90’  (approx.)
*  Production: Diafime with R.T.P. (Rádio e Televisão de Portugal) Ricardo Costa Four Seasons by Antonio Vivaldi and from pieces of Portuguese music of the 15th century (Segréis de Lisboa)
*  Laboratory – Tobis Portuguesa Redondo and Alandroal – Alentejo
* Premiere: RTP, 1981
* 1st theatrical show : Vila Viçosa, summer 2004

==Bibliographic references==
*O Cais do Olhar by José de Matos-Cruz, Portuguese Cinematheque, 1999
* , article by José de Matos-Cruz

==References==
 

== See also ==
* Docufiction
* List of docufiction films

==External links==
*   – producer’s web page (English, French and Portuguese)
* 
*  – article by José de Matos-Cruz

 
 
 
 
 
 
 
 
 
 