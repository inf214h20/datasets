The Saint and Her Fool
{{Infobox film
| name           = The Saint and Her Fool
| image          =
| image_size     = 
| caption        = 
| director       = William Dieterle
| producer       = William Dieterle  
| writer         = Agnes Günther (novel)   Curt J. Braun   Charlotte Hagenbruch
| narrator       = 
| starring       = William Dieterle     Lien Deyers   Gina Manès   Félix P. Soler
| music          =   
| editing        = 
| cinematography = Frederik Fuglsang
| studio         = Deutsche Film Union
| distributor    = Deutsche First National Pictures
| released       = 4 October 1928
| runtime        = 
| country        = Germany
| language       = Silent   German intertitles
| budget         =  
| gross          =
| preceded_by    = 
| followed_by    = 
}} German silent silent drama film directed by William Dieterle and starring Dieterle, Lien Deyers and Gina Manès. It was based on a novel by Agnes Gunther and premiered at the Capital am Zoo in Berlin.  Art direction was by Andrej Andrejew.

==Cast==
* William Dieterle as Harro, Graf von Torstein 
* Lien Deyers as Rosemarie von Brauneck 
* Gina Manès as Fürstin von Brauneck 
* Félix P. Soler as Fürst von Brauneck 
* Camilla von Hollay as Fräulein Braun 
* Mathilde Sussin as Frau von Hardenstein 
* Heinrich Gotho as Märt 
* Sophie Pagay as Tante Uli 
* Auguste Prasch-Grevenberg as Tante Marga 
* Hanni Reinwald as Lisa, Rosemaries Zofe 
* Loni Nest as Rosemarie als Kind

==References==
 

==Bibliography==
* Kreimeier, Klaus. The Ufa Story: A History of Germanys Greatest Film Company, 1918-1945. University of California Press, 1999.

==External links==
* 

 

 
 
 
 
 
 
 
 
 


 
 