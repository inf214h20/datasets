Hind Mahila
{{Infobox film
| name           = Hind Mahila
| image          = 
| image_size     = 
| caption        = 
| director       = Premankur Atorthy
| producer       = Kohlapur Cinetone 
| writer         = 
| narrator       = 
| starring       = Rattan Bai Master Vithal Shahu Modak Hafisji 
| music          = H. C. Bali
| cinematography = 
| editing        = 
| distributor    =
| studio         = Kohlapur Cinetone 
| released       = 1936
| runtime        = 
| country        = India
| language       = Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}} 1936 Hindi social film directed by Premankur Atorthy.    It was produced by Kolhapur Cinetone.     Premankur Atorthy Moved from New Theatres and worked for Kohlapur Cinetone making Bhikharan and Hind Mahila for them before going to Imperial Film Company.       The music direction was by H. C. Bali. The cast included Rattan Bai, Shahu Modak, Master Vithal, and Hafisji.   

==Cast== 
*Rattan Bai
*Shahu Modak
*Master Vithal
*Hafisji
*Raja Pandit
*Sudhabala

==Songs==
Songlist   
*Basant Aayi Hai Muskarati
*Aaj Bana Madak Sansar
*Acharya Jo Sab Deshon Ka Raha
*Bana De Beena Hai Bhagwan
*Ise Hi kehte Hain Prem
*Bhar Bhar Jaam Pila De Saaki
*Kahin Karta Hoon Maat Kahin Khaata Hoon Laat
*Saqi Mai e Gulgoo Ki Jab Jalwagiri Dekhi
*Zahid Ko Jaam Door Se Dikha Le Ke Pi Gaya
*Magan Aaj Hui Tan Man Se
*Samay Akarath Mat Kho Pyaare 


==References==
 

==External links==
* 

 

 
 
 
 

 