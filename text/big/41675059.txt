Ciao Bella (film)
{{Infobox film
| name          = Ciao Bella
| image         = Ciao-Bella-by-Mani-Masserat-Agah.jpg
| image_size    =
| caption       = Theatrical release poster
| director      = Mani Masserat-Agah
| producer      = Christian Holm and Mani Maserrat-Agah  (co-producers)  Christer Nilson, Lena Rehnberg, Olle Wirenhed small>(executive producers) 
| writer        = Jens Jonsson Oliver Wahlgren-Ingrosso
| music          = Martin Willert
| cinematography = Andréas Lennartsson
| editing        = Kristin Grundström
| studio         = 
| distributor    = Götafilm
| released       =  
| runtime        = 86 minutes
| country        = Sweden
| language       = Swedish / English / Persian / Italian
| budget         = 
| gross          = 
}}
Ciao Bella is a 86-minute 2007 Swedish feature film by director Mani Masserat-Agah and is the Iranian-Swedish directors debut long feature film.

==Synopsis==
Gothenburg is host for an international soccer tournament called the Gothia Cup where teams from Sweden and various other countries are competing. A 16-year-old Swedish teenager / soccer player, from an immigrant family of Iranian origin, Mustafa Moradi from Lerum (played by Poyan Karimi), is yearning for love, but he is just too "nerdy" and lacks self-confidence to develop meaningful relationships and is downright intimidated by the girls who just want to have him as a "friend" but not as a lover.

Mustafa has entered with his team in the tournament where he meets a visiting team from Italy who happen to need an "extra". So he wears a #10 jersey as "Massimo" and begins practicing with them. Son he discovers a sudden surge in his popularity by just changing his name to something Italian. Encouraged by his initial success, he tries to conceal his Iranian identity and develop an Italian personna convinced this is his chance with the girls attending the Cup games.
 Oliver Wahlgren-Ingrosso) who gladly volunteers, but cautioning him not to fall in love with girls he goes out with. Mustafa and Enrico are clearly attracted to each other and a male bond develops between the two as Enricos fascination for Mustafa grows.

There is a gradual transformation of Mustafa into "Massimo", his new assumed name. And sooner than he knows, he starts dating Linnea (played by Chanelle Lindell), a young but more mature Swedish woman, who longs for a genuine guy with sincerity and courage who fall for "Massimo". But she has her own baggage as she is pregnant from a previous one-stop affair in a bar and is planning on an abortion with the help of a nurse friend (played by Maria Hedborg). Disenchanted with her father (played by Jimmy Lindström) she also plans on quitting Sweden by escaping with "Massimo" to Italy at the end of the tournament as the relationship between the two blossoms into a very serious love affair.

Frustrated with his own dilemmas, and seeing the Italian players are intentionally ignoring him and do not pass the ball, Mustafa leaves the Italian soccer team in disgust. His best buddy and Italian teammate Enrico, his "partner in crime" becomes frenetic as he begs Mustafa to stay and in a moment of weakness, professes to him his love with a kiss, but Mustafa rejects him and Enrico leaves in despair.

Meanwhile "Massimo"s position with Linnea becomes more and more precarious and has any close calls as his acquaintances unknowingly call his bluff. But Massimo makes a pact with Linnea upon the insistence of the latter never to lie to each others, he eventually confesses and tells her the truth about his continuous lies. Eventually they reconcile and decide to continue their life together.

==Reception==
The film, shot on a low budget and with many of the main characters played by young aspiring novice actors of no previous film experience, was well received by critics as Mani Maserrat-Agahs debut film finding it a witty romantic comedy. The film got a review of 4/5 by Moviezine,  VeckoRevyn and Frida (magazine)|Frida and 3/5 by Svenska Dagbladet,  Aftonbladet and Expressen. 

The film premiered on August 3, 2007 with more than 30,000 attending in the first two weeks. The film contained some graphic sex scenes by the teenagers with demands to censor the film for certain ages.

==Cast==
*Poyan Karimi as Mustafa / Massimo
*Chanelle Lindell as Linnea Oliver Wahlgren-Ingrosso as Enrico
*Mina Azarian as Mustafa / Massimos mother
*Jimmy Lindström as Lineas father
*Arash Bolouri as Babak
*Maria Hedborg as Nurse
*Lotta Karlge as Johanna
*Adam Lundgren as Mattias
*Oscar Rydelius	as a criminal thug

==Awards and nominations==
In 2009, director Mani Maserrat Agah was nominated to "New Voices/New Visions Grand Jury Prize" for Ciao Bella during the Palm Springs International Film Festival.

==References==
 

==External links==
* 

 
 
 
 