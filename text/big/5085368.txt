Once in the Life
 
{{Infobox film
| name           = Once in the Life
| image_size     = 
| image	=	Once in the Life FilmPoster.jpeg
| alt            =
| caption        = 
| director       = Laurence Fishburne
| producer       = 
| writer         = Laurence Fishburne (play Riff-Raff)
| narrator       = 
| starring       = Laurence Fishburne Titus Welliver Eamonn Walker
| music          = 
| cinematography = 
| editing        = 
| studio         =
| distributor    = 
| released       = 2000 
| runtime        = 
| country        = United States
| language       = English
| budget         = 
| gross          =
}}

Once in the Life is a 2000 film written by, directed by, and starring Laurence Fishburne. Fishburne adapted the script from his own play, Riff-Raff.

==Plot==
Once in the life of drug dealing and organized crime, can anyone get out? During a brief jail stay, two half-brothers, who have rarely seen each other while growing up, connect. One of them, called 20/20 Mike because he can sense people nearby, concocts a scheme in which the two of them will steal drugs from young couriers. The heist goes awry when Billy, the junkie brother, shoots the victims of the theft. The brothers hole up in an abandoned building, and 20/20 Mike seeks help from an old cellmate, Tony, whom he thinks is out of the life. It turns out that they have stolen Tonys dope, and Tonys boss wants the two thieves dead. Is there any way out?

==Cast==
*Laurence Fishburne - 20/20 Mike
*Titus Welliver - Torch
*Eamonn Walker - Tony
*Gregory Hines - Ruffhouse
*Michael Paul Chan - Buddha
*Annabella Sciorra - Maxine
*Paul Calderón - Manny Rivera
*Andres Titus - Hector (as Andres Dres Titus)
*Madison Riley - Precious

==Production==
Much of the film was shot in the Hotel Rivieria in Newark, New Jersey. 

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 
 
 


 