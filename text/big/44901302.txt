Sadik 2
{{Infobox film
| name           = Sadik 2
| image          = File:Sadik 2 poster.jpg
| alt            = 
| caption        = 
| film name      =  
| director       = Robin Entreinger
| producers = Robin Entreinger Vincent Michaud
| writers = Robin Entreinger Jean-Nicolas Laurent
| screenplay     = 
| story          = 
| based on       =  
| starring       = Alexandra Bialy Valentin Bonhomme Mathieu Coniglio Marjolaine Pottlitzer
| narrator       =  
| music          = 
| cinematography = Virginie Trottet
| editing        = Robin Entreinger
| production companies = 2017 Films
| distributor    = 2017 Films
| released       =  
| runtime        = 76 minutes
| country        = France
| language       = French
| budget         = 
| gross          =  
}}
Sadik 2 is a 2013 French horror film that was directed by Robin Entreinger. The film had its world premiere on 23 August 2013 at the London FrightFest Film Festival and centers upon a group of friends that finds themselves hunted by a sadistic killer. 

The films title implies that it is the second film in a series but is actually the first film, as Entreinger thought that it would be interesting to film a "sequel" before creating the first film.    Sadik 2 references the fictional first film by having the characters reference it by commenting that Sadik became popular due to rumors that it was a snuff film, as all of the actors were genuinely murdered on camera.   

==Synopsis==
A group of friends has decided to head out to a secluded rental house in order to hang out and hold a New Years Eve party. Each party goer has their own plans to make the night memorable, but theyre ill prepared for when one of the group goes missing and someone begins picking them off one by one.

==Cast==
*Alexandra Bialy as Isa
*Valentin Bonhomme as Kevin
*Mathieu Coniglio as Fred
*Guillaume Gamand as Franck
*Guillaume Levil as Marco
*Guillaume Moiton as Antoine
*Marjolaine Pottlitzer as Gwendo  
*Léon Vitale as Al

==Production== sequences a day.  This was made somewhat more difficult as part of the filming had to be shot in a basement that was not part of the house used for another location, necessitating that the crew move between the two locations. 

==Reception==
What Culture and Grolsch Film Works both panned the film,  and What Culture wrote "Though wearing its meta tone on its sleeve, Sadik 2 takes far too long to engage with its twisty concept, and is painfully subject to a subterranean budget."  Screen Daily was mixed in their review and they commented that "It is well-acted and written and the first half almost works as character drama, though surprisingly little is made of the victims’ shared history in care.  However, all that goes out the window when the slaughter starts.  The snuff scenes are horrid but filmed with some restraint, evoking the added-on coda of the 1978 exploitation effort Snuff (film)|Snuff in the depiction of the killer filmmakers as part of a regular industry, just trying to get the job done, bitching about the chores they have to do because they’re understaffed and falling behind schedule." 

==References==
 

==External links==
*  
*  

 
 
 