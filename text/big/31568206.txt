Poor Pretty Eddie
{{Infobox film
| name           = Poor Pretty Eddie
| image          = Poor_Pretty_Eddie_poster.jpg
| caption        = Original film poster
| director       = Richard Robinson
| producer       = 
| studio         = Artaxerxes Productions, Michael Thevis Enterprises 
| released       = 1975
| runtime        = 86 minutes
| country        = United States
| language       = English
}}
 exploitation film making, Southern gothic, and pornographic film. It has subsequently become popular in cult and B movie circles. 

==Plot==

Liz Wetherly is a popular black singer in need of a break from her hectic schedule.  When her car breaks down, she ends up stuck in a remote southern town that‘s been left for dead “ever since they put in the interstate.” 

She is forced to spend the night at “Bertha’s Oasis”, a rundown lodge that serves as the bizarre fiefdom of an overweight ex-burlesque star who lords over her much younger boyfriend, Eddie, and a cast of equally-strange townsfolk.  Eddie fancies himself a singer on par with Elvis, and expects Wetherly to make him famous.  But things turn ugly for Wetherly, who endures rape and abuse at the hands of her captors, before culminating in her bloody revenge on the “rednecks” that terrorized her.

==Cast==

*Leslie Uggams as Liz Wetherly
*Shelley Winters as Bertha
*Michael Christian as Eddie Collins
*Slim Pickens as Sheriff Orville
*Dub Taylor as Justice of the Peace Floyd
*Ted Cassidy as Keno

==Background==
 launder money he had made through dealings with various Mafia figures, which had attracted the attention of the FBI.   Shortly after the films production, Thevis was jailed on an assortment of charges and, following a prison escape in 1978, was placed on the FBI Ten Most Wanted Fugitives, 1970s|FBI’s most-wanted list.
 Little House on The Prairie and Charlies Angels. 

The film was shot on location in and around Athens, Georgia in 1973.  The film’s biggest star, Shelley Winters, was flown in on a private plane that nearly crashed upon landing.  

Contemporary reviews for the film were almost unanimously negative, owing to the films dark racial undercurrents and repeated depictions of rape.  One Georgia-based film writer concluded:  “Upon leaving the theater, I quite honestly felt nauseous.” 

==Alternate titles and versions==

Over the course of its 10-year run at   film, The Victim as a vigilante thriller in the vein of   scene between Eddie and Bertha missing from "Poor Pretty Eddie," set to Dusty Springfields You Dont Have to Say You Love Me.   

==Home Video==
HD Cinema Classics and Film Chest released the movie on Blu-ray and DVD on April 26, 2011. "Poor Pretty Eddie is another public domain feature restored for Blu-ray by HD Cinema Classics/Film Chest. Anyone who doubts any significant restoration beyond overly aggressive DNR was done on this title need only look at the restoration demo included on the disc. The 35mm print utilized for the master is literally littered with virtually nonstop vertical green scratches, most of which have been removed. Color doesnt seem to have been retimed, at least not significantly, and so things have a slightly ruddy cast some of the time. But lets face it. Though filmed in Technicolor (and mid-70s Technicolor was not the Technicolor of yore), this was never a big budget film, and that low budget indie ethos shines (if thats the right word) through virtually every frame of Poor Pretty Eddie. This was never a glossy, pretty film to begin with and it still isnt. The DNR applied means theres an overly smooth texture to this release, but it also means that we have a largely blemish free image. Color is certainly above average, if not mind blowingly robust, given the low budget confines of the original film. Contrast is on the low side, as it obviously has been from day one, and therefore detail tends to get lost in some of the darker interior scenes. The image is also very soft most of the time, but, again, thats how this film looked from day one. But overall, this is the second color film HD Cinema Classics/Film Chest has released in the last week or so (The Terror being the other), and the results, while not perfect, are not as hideously troublesome as those who want grain, and lots of it, seem to think." 

==References==

{{reflist|refs=

 {{cite news
|url= http://www.filmthreat.com/reviews/6599/
|title=Poor Pretty Eddie
|last=McDonald
|first=Mariko
|date=October 29, 2004
|work=Film Threat}} 

 {{cite news
|url= http://www.imdb.com/title/tt0070556
|title=Poor Pretty Eddie
|last= 
|first=
|date=
|work=Poor Pretty Eddie cast & crew
|publisher=Internet Movie Database}} 

 {{cite news
|url= http://www.amazon.com/Poor-Pretty-Eddie-Blu-Ray-Combo/dp/B004I3Z6GI 
|title=Poor Pretty Eddie
|last=Poggiali
|first=Chris
|date=
|work=DVD Liner Notes
|publisher=Cultra Studios}} 

 {{cite news
|url= http://atlantatimemachine.com/misc/eddie15.htm
|title=Poor Pretty Eddie Should Offend Local Residents
|last=Myers
|first=Steve
|date=May 27, 1975
|work=The Red and Black}} 

 {{cite news
|url=http://www.imdb.com/name/nm0761346/ 
|title=B.W. Sandefur
|last= 
|first=
|date=
|work=B.W. Sandefur IMDB profile
|publisher=Internet Movie Database}} 

}}

==External links==
*  
*   at Turner Classic Movies
* 
* 

 
 
 
 
 