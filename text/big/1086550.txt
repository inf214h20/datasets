Deep Blue Sea (1999 film)
{{Infobox film
| name           = Deep Blue Sea
| image          = Deep Blue Sea (1999 film) poster.jpg
| caption        = Theatrical release poster
| director       = Renny Harlin
|producer=Akiva Goldsman Robert Kosberg Tony Ludwig Alan Riche Rebecca Spikings   
| writer         = Duncan Kennedy Donna Powers Wayne Powers
| starring       =Saffron Burrows Thomas Jane LL Cool J Jacqueline McKenzie Michael Rapaport Stellan Skarsgård Aida Turturro Samuel L. Jackson
| music          = Trevor Rabin
| cinematography = Stephen Windon
| editing        = Derek Brechin Dallas Puett Frank J. Urioste
| studio         = Village Roadshow Pictures
| distributor    = Warner Bros. Roadshow Entertainment   
| released       = July 28, 1999
| runtime        = 105 minutes
| country        = United States Australia
| location       =
| language       = English
| budget         = $60 million http://www.boxofficemojo.com/movies/?id=deepbluesea.htm 
| gross          = $164.6 million 
| preceded_by    =
| followed_by    =
}} science fiction horror film, starring Saffron Burrows, Thomas Jane, LL Cool J, Michael Rapaport, Stellan Skarsgård and Samuel L. Jackson. The film was directed by Renny Harlin and was released in the United States on July 28, 1999.

==Plot== Mako sharks code of genetically engineered the sharks to increase their brain size, but this has the side effect of making the sharks smarter and more dangerous.

After one of the sharks escapes and attacks a boat full of teenagers, Aquaticas financial backers send corporate executive Russell Franklin (Samuel L Jackson) to investigate the facility. To prove that the research is working, the team removes fluid from the brain tissue from the largest shark. While examining it, Dr. Jim Whitlock (Stellan Skarsgard), one of the researchers, is attacked by the shark and his arm is bitten off. Brenda Kerns (Aida Turturro), the towers operator, calls a helicopter to evacuate Jim but a strong hurricane causes the helicopter to lose control and crash into the tower, killing Brenda and the pilots. One of the sharks uses Jims body as a battering ram to smash an underwater window, flooding the research facility and freeing the other sharks. Susan then confesses to the others that she genetically altered the sharks.

Susan, Russell, Carter Blake (Thomas Jane), Janice Higgins (Jacqueline McKenzie) and Tom Scoggins (Michael Rapaport) make their way to the top of the center. While delivering a dramatic speech emphasizing the need for group unity, Russell is dragged into the water by the largest shark and killed. While climbing up the industrial elevator, a ladder falls and gets wedged between the walls of the shaft, leaving them dangling over the water. Janice loses her grip and falls, and despite Carters attempts to save her, she dies. Meanwhile, the cook, Sherman "Preacher" Dudley (LL Cool J) is attacked by the first shark, but manages to blow it up by throwing a lighter into the kitchens oven that had been turned on. He then runs into Carter, Tom and Susan.

Traumatized by Janice and Russells deaths, Tom goes with Carter to the flooded lab because the controls to open a door to the surface are in the lab. The largest shark attacks them and kills Tom. Susan heads into a room to collect her research material. The second shark follows and almost eats her but she manages to electrocute it with a power cable, killing it instantly, though at the cost of destroying her research in the process. Carter, Susan and Preacher go to the top of the research center through a decompression chamber and swim to the surface. Preacher is caught by the third shark and is dragged through the water, but swims to safety after stabbing the shark in the eye with his crucifix, forcing it to release him.

Carter then realizes that the third shark is trying to escape to the open sea, and that the sharks had made them flood the facility so that they could escape through the weaker mesh fences at the surface. Susan, in an effort to distract the third and final shark, cuts herself and dives into the water. When she attempts to climb out, the ladder breaks, and she is killed by the shark. Carter at this point dives in to try to save her but is too late. Managing to dodge the sharks attack and grab hold of its fin, he is pulled through the water by it. Preacher, despite his injuries, grabs hold of the harpoon and shoots the shark through its dorsal fin, but the spear also goes through Carters thigh.  As the shark breaks through the fence, Carter is still attached to the shark by the harpoon and tells Preacher to connect the trailing wire to a car battery, sending an electric current through the wire and to the explosive charge in the harpoon, blowing up the final shark in a spectacular fashion. It is revealed that Carter managed to free himself in time and he swims to the wreckage of the facility and joins Preacher in time to see the workers boat en-route on the horizon.

==Cast==
*Thomas Jane as Carter Blake
*Saffron Burrows as Dr. Susan McAlester
*Samuel L. Jackson as Russell Franklin
*Jacqueline McKenzie as Janice Higgins
*Michael Rapaport as Tom Scoggins
*Stellan Skarsgård as Jim Whitlock
*LL Cool J as Sherman "Preacher" Dudley
*Aida Turturro as Brenda Kerns

==Production==
According to an interview in The Los Angeles Times, Deep Blue Sea was originally inspired by Australian screenwriter Duncan Kennedys witnessing firsthand "the horrific effects of a shark attack when a victim washed up on a beach near his home." This brought on a recurring nightmare of "being in a passageway with sharks that could read his mind." The interview mentions that Kennedy "purged those dreams by sitting down and writing a screenplay that eventually evolved into (the) Warner Bros. thriller, "Deep Blue Sea."" Kennedy acknowledged that "whenever anyone mentions a shark movie, they naturally think of Steven Spielberg. The problem with approaching a shark movie is how do you do it without repeating Jaws?"   

Renny Harlin describes the production on the films commentary.  The film was shot entirely in Mexico. The sets used for the interiors of the facility were built so that they could be submerged in a water-tank to create the illusion of the facility sinking practically. However, for windows, separate water-tanks with lights shining through them were used.
 computer generated 1975 Steven Spielberg film.

Samuel Jackson was initially offered the role eventually played by LL Cool J. Jacksons management didnt like the idea of him playing a chef, so Harlin created the role of Russell Franklin for him.

Speaking with The Los Angeles Times, screenwriter Kennedy noted that in Jaws, the shark was 25 feet long, so Harlin had to do Spielberg one better. "He increased   to 26 feet," Kennedy said. 

==Reception==
The film received mixed reviews from critics. Rotten Tomatoes gave it a 56% rating with 51 positive reviews out of 91 reviews.
Empire magazine|Empire magazine gave the film three out of five stars, saying "It was never going to crash any parties come Oscar night, or usurp previous nature-fights-back epics (Jurassic et al), but Deep Blue Sea remains defiant. Its about giant sharks eating people. And thats exactly what you get."  Roger Ebert went further, saying of the film "In a genre where a lot of movies are retreads of the predictable, Deep Blue Sea keeps you guessing." 

Writing in People Magazine, horror master Stephen King described his recovery from a near fatal accident:  "My first trip after being smacked by a van and almost killed was to the movies (Deep Blue Sea, as a matter of fact; I went in my wheelchair and loved every minute of it.)"  

The film opened on July 28, 1999 and grossed $19,107,643 ($25,164,533 including Thursday screenings/previews) in its opening weekend and went on to earn $73,648,142 domestically and $164,648,142 worldwide.   Adjusted for inflation, the films worldwide total would equal $258,168,286 in 2011.   The film is listed as #12 on Box Office Mojos list of highest grossing "Creature Features" (1982 -) behind such films as the "Jurassic Park" franchise, though outgrossing such films as "Predator" and "Alien: Resurrection."  The film was released on DVD December 7, 1999 and was ranked #1 release for the week ending December 12, 1999 and remained in the DVD rental top 10 for eight weeks. 

==Soundtrack==
 
A soundtrack was released on June 27, 1999 by Warner Bros. Records featuring rap and R&B music. The soundtrack made it to #55 on the Top R&B/Hip-Hop Albums.  Composer Trevor Rabin scored the original music for the film.  The released soundtrack contains 10 tracks. 

==In popular culture==
*Samuel L. Jacksons surprising death scene in the film appears on several lists of best movie deaths of all time—including Den of Geeks "10 surprise deaths in blockbuster movies",   the list "Greatest Movie Deaths of All Time"   and The Vines "Top ten surprise movie deaths".  Phone Book Friction"  when they tested the many elements of the sharks death at the end of the film, with most being proven true.
*The Sealab 2021 episode "Tinfins"  centers around the crew of Sealab making a movie which is an obvious spoof of Deep Blue Sea.
*Several film reviews, including Rolling Stone, have noted distinct plot similarities between Deep Blue Sea and Rise of the Planet of the Apes (2011).  Both stories center on researchers using genetic therapies on animals brains in an attempt to cure Alzheimers disease; therapies that inadvertently make the animals intelligent, enabling them to escape and cause murderous mayhem. Reviewer Peter Travers noted that the newer film has mixed "twists lifted from 1972s Conquest of the Planet of the Apes and 1999s Deep Blue Sea." 

==Potential sequel==
In 2008, it was stated that Warner Premiere was planning a sequel to Deep Blue Sea, according to a studio source. It was also announced that the film would be released direct-to-Video, hitting shelves in 2009. Renny Harlin confirmed no involvement.
Since then, no news has been given on the sequel, implying that the film is probably not in the works. 

==See also==
 
*List of killer shark films

==References==
 

==External links==
* 
* 
* 
* 
* 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 