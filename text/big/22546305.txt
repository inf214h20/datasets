Molly and Me
 
{{Infobox film
| name           = Molly and Me
| caption        =
| image	         = Molly and Me FilmPoster.jpeg
| director       = Lewis Seiler
| producer       = Robert Bassler
| based on       =  
| writer         = Roger Burford Leonard Praskins
| narrator       =
| starring       = Monty Woolley Gracie Fields Reginald Gardiner Roddy McDowall
| music          = Cyril J. Mockridge
| cinematography = Charles G. Clarke
| editing        = John W. McCafferty
| distributor    = Twentieth Century-Fox Film Corporation
| released       =  
| runtime        = 76 minutes
| country        = United States
| language       = English
| budget         =
}}

Molly and Me is a 1945 American comedy film directed by Lewis Seiler and released  by 20th Century Fox. The screenplay was based on the novel written by Frances Marion and adapted by Roger Burford. It starred Monty Woolley, Gracie Fields, Reginald Gardiner and Roddy McDowall.

==Plot==
In 1937 London, struggling vaudeville actress Molly Barry (Gracie Fields) grows tired of the hopeless search for acting roles and instead applies for a job as housekeeper for upper class gentleman John Graham (Monty Woolley). She informs her friends and fellow actors, Lily (Queenie Leonard) and Julia (Edith Barrett), at the boardinghouse where she lives about her plans, but since she doesnt have any housekeeping references, she convinces former exotic dancer, Kitty Goode (Natalie Schafer), who has married into the peerage and become "Lady Burroughs", to act as a fake reference. 

The Graham butler, Peabody (Reginald Gardiner), arrives at the boardinghouse to interview her. Molly acts convincingly as an experienced housekeeper but when Kitty plays her part, he recognizes her as an actress, because he himself is former actor Harry Phillips, who left the profession because of drinking problems he has conquered. He then remembers Molly from her theatre work. Discovering this, Peabody doesnt want another former actor in the household.

Desperate to get the job, Molly tricks Peabody into a pub and where he falls off the wagon. She brings the half-unconscious man back to the Graham house, occupies the housekeepers room, and in the morning informs Mr. Graham that Peabody has hired her. When Peabody sees what she has done, he has no other alternative than to continue using her as housekeeper.

The "difficult" Graham takes a liking to Mollys cheerful personality, whereas the rest of the staff does not, feeling threatened. Graham entertains an old friend, Jamie McDougall (Gordon Richards), who suggests to him the possibility of Graham standing again for Parliament of the United Kingdom|Parliament. Graham is reluctant to do so and shows an old newspaper clipping to McDougall to remind him that Graham ended his previous political career to avoid public disgrace after his wife ran off with a "sportsman." McDougall burns the clipping in the fireplace and tells Graham it all happened 15 years ago and will not be remembered. After he leaves, Molly brings Graham a late snack and hot drink to cure him of his insomnia.

Graham is convinced to travel to Suffolk to meet with the head of his party about the seat in Parliament. Peabody acts as his chauffeur. While they are gone, Molly discovers that the domestic staff all steal from the household, selling flowers they pretend to donate as charity and charging undelivered food to Grahams account, then splitting the difference between themselves and the vendor. When she confronts them, they threaten to quit as blackmail but she sacks them instead. Molly puts the house in order by herself. From a fragment of the clipping she finds in the fireplace, Molly learns the truth about Grahams ex-wife, who went abroad because of the scandal.

That night Grahams teenage son Jimmy (Roddy McDowall) unexpectedly returns home from prep-school. Jimmy suffers from a fever and Molly takes care of him. Jimmy confides in Molly about difficulties with his father. While he was young, Jimmy was told that his mother died and is convinced that Graham doesnt like him because he is a constant reminder that Mrs. Graham "died."

The next day Peabody sends Molly a telegram telling her to prepare a formal dinner for eight because a man of influence, Sir Arthur Burroughs (Lewis L. Russell), publisher of a big London newspaper, will be guest. Unable to find professional help on short notice, Molly hires her theatre friends from the boardinghouse to become the new staff.

Peabody recognizes them but has no choice but to accept their services. Despite numerous mistakes made in their amateur efforts, the dinner is a success with Sir Arthur. The new staff celebrates downstairs in the kitchen, particularly pleased that the common English fare Molly improvised for dinner instead of food "of subtlety and distinction" impressed Sir Arthur much more. Jimmy joins their celebration. Graham comes down to congratulate them but overhears Jimmy join in by imitating Grahams gruff pomposity and sour outlook. He sends Jimmy to bed and tries to sack the others, including Peabody, when he learns that they are actors, but Molly gives notice for the group and uses the opportunity to scold Graham for behaving badly toward his son, not just that evening but as a father.

Everything calms down by the next morning when Jimmy apologizes for his behavior. Graham has also thought over matters, approves of the change in the households atmosphere, and agrees to re-hire the staff by whatever means it takes. The former Mrs. Graham makes an unexpected and untimely appearance, however, to extort money from her former husband. Molly intercepts her and plays along, promising she will bring the money to Mrs. Graham at the hotel where she is staying, just to get her out of the house before either Graham or Jimmy know shes there.

Molly is delighted that father and son have reconciled, but when she tries to tell Graham that "something has happened," he assures her that he has full confidence in her ability to fix any problem. To keep Graham from ever learning of the extortion attempt and Jimmy from discovering the truth about his mother, Molly uses her friends in an act to convince Mrs. Graham that she has committed a murder. Thus fearful of being imprisoned, Mrs. Graham flees the country again, while Jimmy and Graham are out for the evening. When they return, Molly prepares Graham a late snack in the kitchen.

==Cast==
* Monty Woolley as John Graham
* Gracie Fields as Molly Barry
* Reginald Gardiner as Harry Phillips/Peabody, The Butler
* Roddy McDowall as Jimmy Graham
* Natalie Schafer as Kitty Goode-Burroughs
* Edith Barrett as Julia
* Clifford Brooke as Pops
* Aminta Dyne as Musette
* Queenie Leonard as Lily
* Doris Lloyd as Mrs Graham
* Patrick OMoore as Ronnie
* Lewis L. Russell as Sir Arthur Burroughs David Clyde as Angus, the Gardener Matthew Boulton as Sergeant

==References==
 

== External links ==
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 