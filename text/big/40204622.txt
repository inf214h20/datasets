Starred Up
 
 
{{Infobox film
| name           = Starred Up
| image          = StarredUp2013.png
| image_size     = 250
| caption        = Theatrical release poster David Mackenzie
| producer       = Gillian Berrie   Brian Coffey 
| writer         = Jonathan Asser Jack OConnell Ben Mendelsohn Rupert Friend  
| music          = Tony Doogan   David Mackenzie
| cinematography = Michael McDonough
| editing        = Jake Roberts   Nick Emerson Film4   Creative Scotland   Quickfire Films   Northern Ireland Screen   LipSync Productions   Sigma Films
| distributor    = Fox Searchlight
| released       =  
| runtime        = 106 minutes  
 
| country        = United Kingdom
| language       = English
| budget         = 
| gross          =  
}}
 prison crime David Mackenzie Jack OConnell, Young Offender Institution to an adult prison.   

==Cast== Jack OConnell as Eric Love
* Ben Mendelsohn as Neville Love
* Rupert Friend as Oliver Baumer
* Sam Spruell as Deputy Governor Hayes
* Anthony Welsh as Hassan
* David Ajala as Tyrone
* Peter Ferdinando as Dennis Spencer
* Raphael Sowole as Jago 
* Gilly Gilchrist as Principal Officer Scott
* Duncan Airlie James as Officer White
* Gershwyn Eustache Jnr as Des
* Ashley Chin as Ryan
* David Avery as Ashley
* Tommy McDonnell as Officer Self 
* Frederick Schmidt as Officer Gentry
* Sian Breckin as Governor Cardew

==Production==
On 8 October 2012 it was confirmed that Mackenzie was attached to direct the film in Northern Ireland, with OConnell attached to star as the male lead.   On a budget of £2 million, shooting was based in former prisons HM Prison Crumlin Road in Belfast and HM Prison Maze in Lisburn, which was a 24-day shoot including 18 days of stunts.   Principal photography started on 11 February at Crumlin Road with the post-production scheduled to begin during May. 

Financing for Starred Up was provided by Film4 alongside Creative Scotland, Quickfire Films, Northern Ireland Screen and Lip Sync Productions.   

==Release==
  premiere in October 2013]] premiered at the Telluride Film Festival,  before screening at the 2013 Toronto International Film Festival on 9 September 2013,  where it opened the official sidebar section, Special Presentations, sparking both acclaim and praise, the London Film Festival on 10 October,  the Les Arcs International Film Festival on 15 December,  and the International Film Festival Rotterdam on 24 January 2014.  And is soon to be released at the Tribeca Film Festival. 
 Tribeca Film picked up the North American distribution rights to Starred Up, with a planned 2014 theatrical release. 

The film was released in the United Kingdom and Ireland on 21 March 2014. 

==Reception==

===Critical response=== Jack OConnell, David Mackenzies average score score of 7.9/10, with the sites consensus stating, "Smart, hard-hitting, and queasily realistic, Starred Up is an instant classic of U.K. prison cinema."   Metacritic, another review aggregator, assigned the film a weighted average score of 81 (out of 100) based on 26 reviews from mainstream critics, considered to be "universal acclaim".  
 This is London praised the film, commenting that, "Starred Up is the finest British-made prison drama for a long time, courting comparison even with the likes of A Man Escaped."   Guy Lodge of HitFix was very praising of OConnells performance, "OConnells scuzzy charisma and chippy swagger has enlivened a handful of B-level Britpics in the past, though his presence has never been so fearsomely concentrated as it is here." 
 Time Out stated, "For the most part this is furiously compelling stuff, convincingly mounted and superbly acted." 
 The Playlist gave the film an "A-" rating, stating, "Starred Up, like its characters, never loses face, never compromises its bloodily-earned hard-man cred, yet its real agenda is one of compassion." and praising the acting, commentating, "Mendelsohn is amazing" and "The supporting cast all do excellent work too, but this is Eric’s story, and so it’s O’Connell’s film. His performance is a revelation." Allan Hunter of Screen International commended the father-son relationship of the piece, "A complex father/son relationship is viewed through a raw depiction of prison life in the riveting Starred Up." 

Jason Gorber of Twitch Film|Twitch gave the film an "B" rating, stating "Starred Up is a gritty, intense, and shockingly unique take on the prison drama genre." Heralds the acting OConnell and Mendelsohn, "OConnells performance is one of the finest of the year, and Mendelsohn once again demonstrates his unique brand of cold hearted intensity.", as well as Assers screenplay, "Asser captures life in the system with enormous clarity." And notes the depth in its execution, "Shakespearean in its levels of violence and manipulation".  Anton Bitel of Eye for Film stated, "McKenzies   high ambition in the pecking order of the prison flick - a subgenre known to be overcrowded, hierarchically organised and unforgivingly hostile to any weaker new entries." Adding, "Clichés are avoided by the complicated characterisation of both Neville and Oliver."  
 The List spoke positively and commented, "Starred Up gives you a good sharp shake and, in doing so, truly opens your eyes." Describing Assers script as "authentically abrasive and peppered with welcome snatches of humour" and both Mackenzie and cinematographer Michael McDonough as being able to "capture the volatility of the environment without surrendering sensitivity to character."  Chris Bumbray  of JoBlo.com was most praising of the piece, especially the acting performances. In regard to Mendelsohn he stated, "  manages to hold his own opposite OConnells almost Brando-like performance." And of OConnell, "OConnell is brilliant, managing to give Eric a kind of Bronson-like intensity, although they keep him sympathetic in that its clear that his rage is a by-product of a vicious upbringing." 
 
Eric Kohn of IndieWire gave the piece an "A-" rating, commenting of director Mackenzie, "Pushing beyond the brutal exterior of his material, Mackenzie reveals the tender story of estrangement beneath, but never forces the sentimentality." And additional, "British director David Mackenzies gradually affecting "Starred Up" has all those ingredients but uses them for more precise means that merely revealing the harsh nature of life behind bars. Mackenzie applies a sharp kitchen sink realism to this haunting setting and directs it toward an ultimately moving family drama that just happens to involve vicious convicts." 

Mark Kermode of The Observer gave a positive review of the piece, commenting, "Mackenzie keeps us grounded in the maze of prison life, coaxing powerful performances from his cast, each apparently encouraged and emboldened to find their own space." Speaking highly of Friends performance as "terrifically edgy" and describing OConnells as "  electrified and electrifying performance" furthering such a statement by remarking "theres a hint of the young Malcolm McDowell about him." He concludes by praising cinematographer Michael McDonoughs ability to "  the claustrophobia of the physical environment without reducing the characters within the frame."  

===Accolades===

==Awards and nominations==
{| class="wikitable sortable" style="font-size: 95%;"
|-
! Award
! Date of ceremony
! Category
! Recipients and nominees
! Result
|-
| rowspan="8"| British Independent Film Awards  December 8, 2013
| Best British Independent Film
| 
|  
|-
| Best Director David Mackenzie
|  
|-
| Best Actor Jack OConnell
|  
|-
| Best Supporting Actor
| Ben Mendelsohn
|  
|-
| Best Supporting Actor
| Rupert Friend
|  
|-
| Best Screenplay
| Jonathan Asser
|  
|-
| Best Achievement in Production
| 
|  
|-
| Best Technical Achievement
| Shaheen Baig
|  
|-
| rowspan="1"| Dublin International Film Festival  February 22, 2014
| Best Actor
| Jack OConnell
|  
|-
| rowspan="1"| Irish Film & Television Awards  April 5, 2014
| Best Editing
| Jake Roberts, Nick Emerson
|  
|-
| rowspan="2"| London Film Festival 
| rowspan="2"| 8 January 2014
| Best Film 
| 
|  
|-
| Best British Newcomer 
| Jonathan Asser
|  
|-
|}

==References==
 

==External links==
 
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 