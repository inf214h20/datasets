Kangaroo Jack: G'Day U.S.A.!
{{Infobox film name = Kangaroo Jack: GDay U.S.A.!
|image= Kangaroo Jack G Day U.S.A. poster.png caption = director = Emory Myrick Jeffrey Gatrall producer = Emory Myrick writer = Adam Scheinman Andrew Scheinman starring = Jeff Bennett Keith Diamond Phil LaMarr Kath Soucie Ahmed Best Steven Miller | music = Randall Crissman cinematography = editing = Susan Edmunson studio = Warner Bros. Family Entertainment Warner Bros. Animation Castle Rock Entertainment distributor = Warner Home Video  released =   runtime = 77 minutes
|country= United States language = English budget =
}}

Kangaroo Jack: GDay U.S.A.! is an animated sequel to 2003s Kangaroo Jack that is directed by Emory Myrick and Jeffrey Gatrall (time director). It was produced by Warner Bros. Animation and sister company Castle Rock Entertainment and was distributed by Warner Bros. and released direct-to-video in 2004.

==Synopsis==
The movie takes place some time after the first film. The sequels protagonists - Charlie Carbone, his wife Jessie and his best friend Louis Booker - travel to Australia once again and it is up to them to save the animals of the outback from danger by poachers along with the help of their old friend Jackie Legs, the kangaroo character from the original film.

==Main characters==

===Kangaroo "Jackie Legs" Jack===
He is an anthropomorphic rapping kangaroo who had a previous encounter with Charlie Carbone and Louis Booker from the original film (though the sequel is usually canon). An animal with a love for candy, he is a friend to Charlie, Jessie, and Louis, and he is currently living in the Australian outback along with his family.
 Jeff Bennett (Adam Garcia in the movie, uncredited).

===Charlie Carbone===
Charlie Carbone is the stepson of Salvatore Maggio (who appears briefly in a picture on the news in the movie). He is married to Jessie, another character from the original film. He is long-time friends with Louis Booker, whom he and Charlie encounter Jackie Legs. He often takes things seriously and is currently selling a new shampoo alongside Louis.

Charlie is voiced by Josh Keaton.

===Louis Booker===
An African American, Louis is slightly chubby, slightly slow-witted and is the closest one to Jackie Legs. He is Charlie Carbones best friend who enjoys adventuring yet he often seems to attract bad luck. Louis has a farting problem.

Louis is voiced by Ahmed Best.

===Jessie Carbone===
Charlie Carbones wife. Rather than being a wildlife preserve worker, she is an adventurer traveling alongside her husband (though fans thought that both Charlie and Jessie act as siblings rather) and Louis Booker aside from being a close one to Jackie Legs.

Jessie is voiced by Kath Soucie.

===Outback Ollie===
He acts as an innocent park owner with many Australian animals but is revealed to be an evil smuggler who disguises his New York accent with an Australian accent. At the end of the movie, he shows his dark side and ends up spending the rest of his life in prison.
 Jim Ward.

===Rico and Mikey===
Outback Ollies henchmen. Rico and Mikey are the men who stole the sacred stones from Chief Ankamuti and his tribe. They dressed up as cops in one scene to take the stones from Charlie, Louis and Jessie because they thought they had them but Louis knocked them out. The trio then discovered the thieves were the pair because Rico was the one with a smiling forehead(it was really red marker) and Mikey was the one who carried a snake on his left arm(it was really a tattoo). They are arrested at the end. But end up serving 16 years in prison.

Rico and Mikey are voiced by Phil LaMarr.

===Ronald Booker=== exterminator and limo driver. He lives in Nevada and is crazy about gems.
 Keith Diamond.

===Chief Ankamuti===
The leader and medicine man of the Australian Aborigines|Aborigines. He and his people worship the legendary rainbow serpent with four sacred stones which he described as "one as red as fire, one as blue as the sky, one as green as the ocean and one as black as night". The stones go by these names of the gods when Ankamuti used his magic, the red one was "Kakuru", representing the fire element, the blue one was "Kalseru", representing the sky element, the green one was "Langal", representing the water element, and the black one was "Galeru", representing the earth element. Louis often mispronounces his name. He gave Charlies group two gifts for returning their stones - pango pangoes for their shampoo and the gift of being "one with nature" so they can talk to Jackie and the other animals. Charlie considers the chief a lunatic until he gives them the gift of being "one with nature".

Chief Ankamuti is voiced by Obba Babatundé.

===Agent Jackson===
The FBI agent and policeman who thought Charlie and his friends were stealing Jackie from Outback Ollie and lost them when Jackie caused a large traffic jam. Like Charlie, he often takes things seriously. He arrests Charlie, Louis and Jessie but later realizes Outback Ollie and his two henchmen were behind the crime. When Charlie, Louis, and Jessie tell him that Outback Ollie switched the collar on Jackie before they went to shore at Lake Mead and that Outback Ollie put the jewels in Louis jacket.

Agent Jackson is voiced by Dorian Harewood.

==Minor characters==

===Mrs. Sperling===
A crazy elderly woman who is one of Ronalds customers. Her house is loaded with booby traps to capture aliens from outer space. She has a bulldog named Neil as an assistant. She thinks Jackie is a "six-foot tall red possum that stands on its hind legs". Like Jessie, she is voiced by Kath Soucie.

===Jackies Family===
Jackies mate and son. According to Louis, they are "Mrs. Legs" and "Jackie Jr." They dont talk in the film.

===Killer Putulski=== Keith Diamond.

===The Panther===
A show animal, he tried to eat Charlie and Louis but he licked the gravy on them. He is voiced by Frank Welker.

===The Emu===
An emu that was captured by the poachers. When Charlie, Louis and Jessie were hiding in his crate, the emu kept pecking Charlie in the head, much to Charlies annoyance. He escapes the airport (because the trio left his crate open) and is seen on TV, seemed to be missing. After a long absence from the film, he finally reappears at Outback Ollies Animal Park where he pecks Charlies head again. At the end of the film, he apologizes for pecking Charlies head and explains that he loves hollow sounds "rather like an empty coconut". The emu is portrayed by Steven Miller.

===Larry=== Jeff Bennett.

===The Frilled Lizard===
A frilled lizard who first appears in Louis dream in the prologue. He then appears in the epilogue where he asks Larry the echidna if his frill makes him look scary, or if he is just kidding himself. He is voiced by Jess Harnell.

== Song ==
The film featured a rendition of LL Cool Js single "Mama Said Knock You Out" during the dream boxing scene. The film also featured a few lines from "I Only Have Eyes for You" when Jackie is in the lounge. Jeff Bennett performed both songs.

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 