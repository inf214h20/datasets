Sitting on a Branch, Enjoying Myself
{{Infobox film
| name = Sitting on a Branch, Enjoying Myself
| image = Sitting pretty on a branch poster.jpg
| caption =
| director = Juraj Jakubisko
| writer =
| starring =
| music = Jiří Bulis
| cinematography = Ladislav Kraus
| editing =
| producer =
| distributor =
| released =  
| runtime =
| awards =
| country =
| language = French
| budget =
}}
Sitting on a Branch, Enjoying Myself ( ) is a 1989 Czechoslovak-German comedy-drama film directed by Juraj Jakubisko.  It was entered into the main competition at the  46th Venice International Film Festival. 

== Synopsis ==
A tragi-comedy about the end of WWII and the roll call of the communist regime. With this background you have a part-dream sequence story of the two main characters coming back from the war - comedian Pepe and front line soldier Prengel that become best friends after discovering gold together. By coincidence these two meet Ester who is coming back from a concentration camp and now love enters the story with this melancholic Jewish girl. Three homeless people are trying to fulfill their idea of happiness. They chose the wrong time period. The monstrosity of the 1950’s is represented by a fanatic young communist, Želmíra, who turns their world upside-down.

== Cast ==
*Bolek Polívka - Pepe
*Ondřej Pavelka - Prengel
*Deana Horváthová - Želmíra
*Štefan Kvietik - kapitán Kornet
*Markéta Hrubešová - Ester
*Miroslav Macháček - Krištofík

==Film Awards==
*   Cran Gavier´99 1999   • Prize for the best film
*   Moscow International Film Festival 1990   • Grand Prize
*   Festival of Czechoslovak film 1990   • Special Jury Prize
*   IFF Stasbourg 1990   •Le Prix du Jury and the Alsace Media de Strasbourg Prize
*   Venice Film Festival 1989   • Certificate of Merit RAI II

==References==
 

==External links==
* 

 
 
 
 
 
 


 