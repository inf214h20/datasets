The Baby and the Battleship
{{Infobox film
| name           = The Baby and the Battleship
| image          = 
| caption        = 
| director       = Jay Lewis 
| producer       = 
| writer         = 
| starring       = John Mills 
| cinematography = 
| music          = 
| studio = 
| distributor   = British Lion Films Distributors Corporation of America (US)
| released       = 1956
| runtime        = 
| country        = United Kingdom English
| gross = £258,845 (UK) 
}} British comedy HMS Birmingham and in Malta.

==Plot==
When a group of Royal Navy sailors go ashore on shore leave to Naples, they are forced to care for a baby, separated from its mother.   During a brawl, Puncher Roberts is knocked unconscious and finds the square empty, except for the baby.  Unable to find his friend Knocker, or the childs mother, he smuggles the baby aboard their ship in the midst of a series of joint operations with Allied navies off the coast of Italy.

==Cast==
* John Mills - Puncher Roberts
* Richard Attenborough - Knocker White
* André Morell - Marshal
* Bryan Forbes - Professor Evans
* Michael Hordern - Captain Hugh
* Ernest Clark - Commander Geoffrey Digby
* Harry Locke - Chief Petty Officer Blades Michael Howard - Joe
* Lionel Jeffries - George
* Clifford Mollison - Sails
* Thorley Walters - Lieutenant Setley
* Duncan Lamont - Master-at-Arms
* Lisa Gastoni - Maria
* Cyril Raymond - PMO
* Harold Siddons - Whiskers
* D. A. Clarke-Smith - The Admiral
* Kenneth Griffith - Sub-Lieutenant
* John Le Mesurier -  The Marshals Aide
* Carlo Giustini - Carlo Vespucci
* Ferdy Mayne - Interpreter
* Vincent Barbi - Second Brother Gordon Jackson - Harry
* Vittorio Vittori - Third Brother
* Martyn Garrett - The Baby Barry Foster - Sailor at Dance Robert Ayres - American Captain

==Reception==
The film was one of the ten most popular movies at the British box office in 1956. BRITISH. FILMS MADE MOST MONEY: BOX-OFFICE SURVEY
The Manchester Guardian (1901-1959)   28 Dec 1956: 3 

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 


 
 