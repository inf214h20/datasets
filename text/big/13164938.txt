Weekend, Italian Style
{{Infobox film
| name           = Lombrellone
| image          = Lombrellone.jpg
| image_size     = 
| caption        = 
| director       = Dino Risi
| producer       = Mario Cecchi Gori
| writer         = Ennio De Concini Dino Risi
| narrator       = 
| starring       = Sandra Milo Enrico Maria Salerno Jean Sorel Leopoldo Trieste
| music          = Lelio Luttazzi
| cinematography = Armando Nannuzzi
| editing        = Franco Fraticelli Emilio Rodríguez
| distributor    = 
| released       = 1966 (Italy) 6 February 1967 (Spain) January 1968 (U.S.)
| runtime        = 103 Min
| country        = Italy Italian
| budget         = 
}}
 1966 Italy|Italian comedy film|comedy-drama film directed by Dino Risi. It was co-produced with Spain and France. The soundtrack is full of Italian Pop songs from the 1960s.

==Plot==
Ferragosto in Rome. Everybody is on holidays on the beach, except Enrico Marletti (Enrico Maria Salerno), who spends the week working. On the weekends, he drives to Rimini to meet his wife Giuliana (Sandra Milo), but she is living her dolce vita (sweet life) and her husband is out her chic friends.

==Cast==
*Sandra Milo: Giuliana Marletti
*Enrico Maria Salerno: Enrico Marletti
*Jean Sorel: Sergio
*Daniela Bianchi: Isabella Dominici
*Lelio Luttazzi: Conte Antonio Bellanca
*Raffaele Pisu: Pasqualino
*Leopoldo Trieste: professor. Ferri
*Trini Alonso: Clelia Valdameri
*Alicia Brandet: Vicina di stanza
*Véronique Vendell: Giuliana
*Pepe Calvo: Mr. Tagliaferri
*Antonella Della Porta: Miss De Rossi
*Solvi Stubing: Miss Marini

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 
 
 


 
 