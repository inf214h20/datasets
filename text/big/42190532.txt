Hannele Chiguridaga
{{Infobox film
| name           = Hannele Chiguridaga
| image          = 
| caption        = 
| director       = M. R. Vittal
| producer       = Srikanth Nahata   Srikanth Patel Triveni
| screenplay     = M. R. Vittal
| based on       =   Rajkumar Kalpana Kalpana R. Nagendra Rao
| music          = M. Ranga Rao
| cinematography = Srikanth   Kumar
| editing        = S. P. N. Krishna   T. P. Velayudham
| studio         = Srikanth & Srikanth Enterprises
| released       =  
| runtime        = 131 minutes
| country        = India Kannada
}}
 Kannada film Kalpana in lead roles. The film is based on the novel of the same name by Anasuya Shankar|Triveni.  The story deals with the empowerment of women in education and also issue around widow marriage. 
 Karnataka State Film Award for First Best Film. 

==Cast== Rajkumar
* Kalpana
* Arun Kumar
* R. Nagendra Rao
* Ranga
* Radha
* Jayasree
* Papamma
* Dinesh

==Soundtrack==
{{Infobox album
| Name        = Hannele Chiguridaga
| Type        = Soundtrack
| Artist      = M. Ranga Rao
| Cover       = 
| Border      = yes
| Caption     = Soundtrack cover
| Released    = 1968
| Recorded    =  Feature film soundtrack
| Length      = 22:01
| Label       = Saregama
| Producer    = 
| Last album  = 
| This album  = 
| Next album  = 
}}

The music for the soundtracks was composed by M. Ranga Rao, and lyrics written by R. N. Jayagopal.  The album consists of six soundtracks. 

{{track listing
| headline = Track listing
| extra_column = Singer(s)
| total_length = 22:01
| lyrics_credits = yes
| collapsed = no
| title1 =  Hoovu Cheluvella 
| extra1 = P. Susheela
| lyrics1 = R. N. Jayagopal
| length1 = 3:37
| title2 = Baara Olidu Baara
| extra2 = P. Susheela
| lyrics2 = R. N. Jayagopal
| length2 = 3:34
| title3 = Ide Hudugi Ide Bedagi
| extra3 = P. B. Sreenivas, P. Susheela
| lyrics3 = R. N. Jayagopal
| length3 = 3:31
| title4 = Haalalli Mindavalo
| extra4 = M. Balamuralikrishna
| lyrics4 = R. N. Jayagopal
| length4= 3:57
| title5 = Malle Malle
| extra5 = P. Susheela, L. R. Eswari
| lyrics5 = R. N. Jayagopal
| length5 = 3:41
| title6 = Malle Malle
| extra6 = L. R. Eswari
| lyrics6 = R. N. Jayagopal
| length6 = 3:41
}}

==Awards==
;1968–69 Karnataka State Film Awards Karnataka State Film Award for First Best Film Best Actor - R. Nagendra Rao Best Actress Kalpana
* Best Music Director - M. Ranga Rao Triveni

==References==
 

==External links==
*  

 
 
 
 
 
 
 


 