Union Station (film)
{{Infobox film
| name           = Union Station
| image          = UnionStationPoster.jpg
| alt            = 
| caption        = French treatrical release poster
| director       = Rudolph Maté
| producer       = Jules Schermer
| screenplay     = Sydney Boehm
| story          = Thomas Walsh
| starring       = William Holden Nancy Olson Barry Fitzgerald
| music          = Heinz Roemheld
| cinematography = Daniel L. Fapp
| editing        = Ellsworth Hoagland
| studio         = Paramount Pictures
| distributor    = 
| released       =  
| runtime        = 80 minutes
| country        = United states
| language       = English
| budget         = 
| gross          = 
|}} crime drama, directed by Rudolph Maté.  The drama features William Holden, Barry Fitzgerald, and Nancy Olson, among others. 

==Plot== Los Angeles Union Station ), a railroad policeman, William Calhoun, is approached at work by an apprehensive passenger named Joyce Willecombe (Nancy Olson) who believes that two travelers aboard her train may have been up to no good.

Joyce is the secretary to a rich man named Henry Murchison (Herbert Hayes), whose blind daughter, Lorna, has been kidnapped and held for ransom. The railway station where Calhoun works has been chosen as the location to pay off the ransom. Calhoun and fellow cop Inspector Donnelly race against time to find the kidnappers and bring them to justice.

==Cast==
* William Holden as Detective Lt. William Calhoun
* Nancy Olson as Joyce Willecombe
* Barry Fitzgerald as Inspector Donnelly
* Lyle Bettger as Joe Beacom
* Jan Sterling as Marge Wrighter
* Allene Roberts as Lorna Murchison
* Herbert Heyes as Henry L. Murchison
* Fred Graff as Vince Marley
* James Seay as Detective Eddie Shattuck
* Parley Baer as Detective Gottschalk (as Parley E. Baer)
* Ralph Sanford as Detective Fay
* Richard Karlan as Detective George Stein
* Bigelow Sayre as Detective Ross
* Charles Dayton as Howard Kettner
* Jean Ruth as Pretty Girl

==Background== Sydney Boehms New York Los Angeles Union Station was the actual filming location), and changing the kidnap victim from a little boy to  a blind, teen-aged girl, the script was quite faithful to its source material.
 Sunset Boulevard the same year.

===Filming locations=== Union Station, Downtown, Los Angeles, California.  Also, it looks like it was filmed on Chicagos South Side El from 1892 to Indiana station, where the train is uncoupled to go on the Stockyards Branch, which ran until 1957.  Normally, the branch ran as a shuttle. It terminated at Exchange station, which was the terminal after 1956. 

==Reception==

===Critical response===
The staff at Variety (magazine)|Variety magazine gave actor William Holden a good review, writing, "William Holden, while youthful in appearance to head up the railway policing department of a metropolitan terminal, is in good form." 

Channel 4s film review notes, "Despite the barely believable plot, the film has a real edge. Made in 1950, it obviously cant push to the extremes of Dirty Harry but it shares the same mean spirit. Maté capitalizes on the storys setting by using innocent passengers and the stations dramatic spaces to heighten the feverish atmosphere." 

Critic Jerry Renshaw lauded the film and wrote, "On the surface, Union Station is a fairly routine action film for 1950, with its high level of suspense, strong-arm police procedural tactics, and caper-film trappings. However, a definite noir outlook is belied (sic) by the fact that the police play as rough as the bad guys, blurring the lines of good and evil. Audiences are used to seeing Barry Fitzgerald as a kindly Irish priest in most roles; during the scene on the empty platform, though, Fitzgeralds Inspector Donnelly tells the cops in his most charming Father OFlaherty voice, Make it look accidental. Thats one of the more chilling moments of noir, more suited to James Ellroy than Fifties Hollywood. Director Maté also helmed the classic D.O.A. (1950 film)|D.O.A. in 1950." 

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 