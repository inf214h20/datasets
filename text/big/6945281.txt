The Prime Minister (film)
 
 
{{Infobox film
| name           = The Prime Minister
| image          =
| image_size     =
| caption        =
| director       = Thorold Dickinson
| producer       = Max Milder Michael Hogan Brock Williams
| narrator       = Stephen Murray
| music          = Jack Beaver
| cinematography = Basil Emmott
| editing        = Leslie Norman
| studio         = Teddington Studios
| distributor    = Warner Brothers
| released       = 3 May 1941
| runtime        = 94 minutes
| country        = United Kingdom English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
}}
 1941 directed Stephen Murray. Gielgud would later reprise his role as Disraeli in the 1975 ITV television drama Edward the Seventh.

==Plot==
In 1837, London novelist Benjamin Disraeli (John Gielgud) crashes his bicycle and is given a ride to a garden party by Mary Ann Wyndham-Lewis (Diana Wynyard). She read his novels and says he should be in Parliament. Disraeli asks Mary Ann to help him, so she goes to the Conservative party leaders and gets their support for Disraeli.

==Cast==
* John Gielgud as Benjamin Disraeli Mary Disraeli Queen Victoria
* Pamela Standish as Princess Victoria Stephen Murray as William Ewart Gladstone Lord Derby Lord Melbourne Nicholas Hannen as Sir Robert Peel
* Will Fyffe as The Agitator Anthony Ireland as Count DOrsay
* Irene Brown as Lady Londonderry

==See also==
* The Thorold Dickinson Archive is held at the University of the Arts London Archives and Special Collections Centre  .

==Notes==
The Prime Minister opened in the United States in February 1942, nearly a year after its British premiere. The American version was some 15 minute shorter than the British version, and among the scenes cut was one featuring a young actress just beginning her career, Glynis Johns. 

==References==
 

==External links==
 
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 


 