The White Flower
{{Infobox film
| name           = The White Flower
| image          = The White Flower (1923) - Compson.jpg
| image_size     = 200px
| caption        = Film still with Compson
| director       = Julia Crawford Ivers
| producer       = Adolph Zukor Jesse Lasky
| writer         = Julia Crawford Ivers
| cinematography = James Van Trees
| studio         = Famous Players-Lasky
| distributor    = Paramount Pictures
| released       =  
| runtime        = 6 reel#Motion picture terminology|reels; (5,731 feet)
| country        = United States
| language       = Silent English intertitles
}}
 silent romantic drama film written and directed by Julia Crawford Ivers and starring Betty Compson and Edmund Lowe. Ivers son, James Van Trees, was the films cinematographer.  Set in Hawaii, the film was shot on location in Honolulu.  The White Flower is now considered Lost film|lost.  

==Cast==
*Betty Compson - Konia Markham
*Edmund Lowe - Bob Rutherford
*Edward Martindel - John Markham
*Arline Pretty - Ethel Granville
*Sylvia Ashton - Mrs. Gregory Bolton
*Arthur Hoyt - Gregory Bolton
*Leon Barry - David Panuahi
*Lily Philips - Bernice Martin
*Reginald Carter - Edward Graeme
*Maui Kaito - Kahuna

==References==
 

==External links==
*  
*  
*  
* , a website devoted to island themed films

 
 
 
 
 
 
 
 
 
 
 
 


 
 