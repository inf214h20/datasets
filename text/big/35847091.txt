Cuban Story
{{Infobox film
| name           = Cuban Story
| image          = 
| image_size     = 
| caption        = 
| director       = Victor Pahlen
| writer         = Victor Pahlen
| narrator       = 
| starring       = Errol Flynn
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       = 1959
| runtime        = 60 mins
| country        = United States
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
| amg_id         = 
}}
Cuban Story: The Truth about Fidel Castro Revolution is a 1959 film documentary narrated by Errol Flynn, and the last known performance work of his career.

It was one of two films Flynn made about the Cuban Revolution during the early period when Castro was publicly denying his communist allegiance, the other being the drama-documentary Cuban Rebel Girls (1959). 

==References==
 

==External links==
*  at IMDB

 
 
 

 