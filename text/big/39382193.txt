Blood Pressure (film)
Blood Sean Garrity. The film stars Michelle Giroux as a pharmacist who is unhappy with her life and marriage, who starts to receive letters from an anonymous admirer, asking her to undertake increasingly risky activities. 

==Production==
The idea for the film originated in a writers group in the Neighbourhood Bookstore and Cafe in Wolseley, Winnipeg, and is based on a story by the films cowriter Bill Fugler. Garrity then added narrative elements to bring the idea to the big screen. The director had planned to shoot the film in Winnipeg when a sudden career opportunity for his wife led to them relocating to Toronto. Garrity found the expense of shooting in a big city to be financially and physically draining, compared to his experiences in his native Winnipeg. In an effort to find a big enough house for the films middle-class main characters, who would not be able to afford such a large residence in Toronto, Garrity ended up filming in suburban Richmond Hill, Ontario.      

Giroux, better known as a stage actress and friend of cast member Jonas Chernick, was originally brought in simply to read with the male actors during pre-production. But she so impressed Garrity that she was chosen for lead role over other actresses with more film experience.   

==Release==
Blood Pressure premiered in Winnipeg on February 22, 2013 with sold out screenings at the citys Cinematheque, followed by a run at the Empire Theatres 8 in Grant Park, Winnipeg starting Mar. 8. The film opened in Toronto at The Royal Theatre on March 15, and was moved to the Carlton Theatre on March 22, where it was held over. The film reportedly averaged $3000 per screen in Toronto and Winnipeg, according to distributor Flying Horsie Films.   

==References==
 

==External links==
* 
* 

 
 
 
 
 
 


 
 