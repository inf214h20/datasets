Close to Home (film)
{{Infobox Film
| name       = Close to Home
| image      = Close to Home poster.jpg
| image_size =
| caption    = theatrical release poster
| director   = Dalia Hager Vidi Bilu
| starring   = Smadar Sayar Naama Schendar Irit Suki Katia Zimbris
| released   = 2005
| runtime    = 90 minutes
| country    = Israel
| language   = Hebrew
| music      = 
}}

Close to Home (Karov la bayit) is a 2005 Israeli movie directed by Dalia Hager and Vidi Bilu, and starring Smadar Sayar and Naama Schedar. It is the first film about the experience of female soldiers in the Israel Defense Forces.

Smadar (Sayar) and Mirit (Schendar), both 18&nbsp;years old, are assigned to patrol the streets of Jerusalem together as part of their military service. Worlds apart in their personality, their initial frosty relationship becomes a friendship as they deal with their own emotional issues, the crushes and break-ups in their love lives, as well as the political realities of the city in which they live.   

==See also==
* Womens cinema

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 


 
 