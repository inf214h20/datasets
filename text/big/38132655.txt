Housemaster (film)
 
{{Infobox film
| name = Housemaster
| image =
| image_size =
| caption =
| director = Herbert Brenon
| producer = Walter C. Mycroft
| writer = Ian Hay (play)   Dudley Leslie   Elizabeth Meehan
| narrator = Diana Churchill   Phillips Holmes   Joyce Barbour
| music = Harry Acres
| cinematography = Otto Kanturek
| editing = Flora Newton
| studio = Associated British Picture Corporation
| distributor = Associated British Film Distributors
| released = 31 January 1938
| runtime = 100 minutes
| country = United Kingdom
| language = English
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}} Diana Churchill ABPC at public school, play of the same name by Ian Hay.

==Cast==
* Otto Kruger as Charles Donkin Diana Churchill as Rosemary Faringdon
* Phillips Holmes as Philip de Pourville
* Joyce Barbour as Barbara Fane
* Rene Ray as Chris Faringdon
* Kynaston Reeves as The Rev. Edmund Ovington
* Walter Hudd as Frank Hastings
* Michael Shepley as Victor Beamish John Wood as Flossie Nightingale
* Cecil Parker as Sir Berkely Nightingale
* Henry Hepworth as Bimbo Faringdon
* Rosamund Barnes as Button Faringdon
* Laurence Kitchin as Crump
* Jimmy Hanley as Travers

==References==
 

==Bibliography==
* Low, Rachael. History of the British Film: Filmmaking in 1930s Britain. George Allen & Unwin, 1985 .
* Warren, Patricia. Elstree: The British Hollywood. Columbus Books, 1998.

==External links==
* 

 

 
 
 
 
 
 
 
 
 


 