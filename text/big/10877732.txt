Last Orders (film)
{{Infobox film name = Last Orders image = Last Orders (film) poster.jpg image_size = caption = Original poster director = Fred Schepisi writer = Fred Schepisi based on =   starring = Michael Caine Tom Courtenay David Hemmings Bob Hoskins Helen Mirren Ray Winstone music = Paul Grabowsky cinematography = Brian Tufano editing = Kate Williams producer = Elisabeth Robinson Fred Schepisi
| studio = Future Films distributor = Sony Pictures Classics country = UK/Germany budget = released =   runtime = 109 minutes language = English
}}

Last Orders is a 2001 British/German drama film written and directed by Fred Schepisi. The screenplay is based on the 1996 Booker Prize-winning novel Last Orders by Graham Swift.

==Plot== last call and the final wishes of a dying man, in this instance Jack Dodds (Michael Caine), an east London butcher who greatly influenced four men over the course of his flawed but decent lifetime. The quartet gathers to scatter Jacks ashes in Margate, where he had hoped to retire to a small seaside cottage with his wife Amy (Helen Mirren), a dream that never was fulfilled.
 horse race gambler Ray boxer Lenny undertaker Vic buffer of luxury cars, whose relationship with his father never quite recovered when, as a young boy, he learned his real family perished in a wartime bombing and Jack and Amy took in the orphaned infant and raised him as their own.
 hop farm pubs en mentally retarded institutionalized since shortly after her birth fifty years earlier. Over the years Jack barely acknowledged her existence, but Amy faithfully has visited her weekly, even though June has no idea of who she is or why shes there.
 flashbacks that stretch across six decades, the stories of the events that brought these people to this point in their lives slowly unfold, ultimately revealing the importance of friendship and love.

==Production==
According to the films official website, producer Elisabeth Robinson and screenwriter/director Fred Schepisi were preparing a feature film about Don Quixote in 1997 when she brought Graham Swifts novel to his attention. The two acquired the film rights to the book, and Schepisi begin to work on his adaptation, completing the first draft of the script by February 1998. Schepisi met potential cast members and forged commitments with Michael Caine, Tom Courtenay, Bob Hoskins, and Ray Winstone.

Nik Powell, head of the independent production company Scala, signed on as an executive producer and during the summer of 2000 brought in German-based Rainer Mockert and MBP to help with the financing. Principal photography began in October of that year and lasted nine weeks. Locations included Eastbourne, Peckham and Bermondsey in London, Canterbury, Chatham, Medway|Chatham, Old Wives Lees, Margate, and Rochester, Kent|Rochester.  Interiors were shot at the Pinewood Studios in Buckinghamshire.

The film premiered at the Toronto Film Festival in September 2001 and was shown at the San Sebastián Film Festival, the Warsaw Film Festival, the Reykjavik Film Festival, and the London Film Festival before opening in the US on 7 December 2001. The film went into theatrical release in the UK on 11 January 2002.

The film grossed $2,329,631 in the US and $4,544,261 in foreign markets for a total worldwide box office of $6,873,892. 

==Principal cast==
*Michael Caine ..... Jack Dodds
*Tom Courtenay ..... Vic Tucker
*David Hemmings .....  Lenny
*Bob Hoskins ..... Ray Johnson
*Helen Mirren ..... Amy Dodds
*Ray Winstone ..... Vince Dodds
*JJ Feild ..... Young Jack
*Cameron Fitch ..... Young Vic
*Nolan Hemmings ..... Young Lenny
*Anatol Yusef ..... Young Ray
*Kelly Reilly ..... Young Amy
*Stephen McCole ..... Young Vince
*Laura Morelli ..... June Dodds
*George Innes ..... Bernie (Landlord of the Coach and Horses)

==Critical reception== David Hare Six Degrees of Separation). Last Orders, though quite different in theme and structure, shares with these films a quiet, amused wonder at the complexities of human character, and a reluctance to shoehorn them into narrative conventions or deduce obvious morals."  
 score . . . The action moves constantly between present and past, which isnt a bad narrative scheme, but when its done so frequently and deliberately, we feel as if were looking over Schepisis shoulder as he diagrams the whole story for us."  

Peter Travers of Rolling Stone called it "a funny and touching film" and "a bawdy delight" and commented, "The acting is of the highest order, but the magnificent Mirren . . . is the films glory and its grieving heart."  

Philip French of The Guardian called the film "a moving study of the pleasures and obligations of friendship, and of facing up to a death and going on" and added, "Schepisi always handles actors sympathetically and here he has a perfect cast, most of whom can draw on their own and their parents experiences. Without a touch of patronisation, they sink into their characters and never attempt to steal scenes from each other."   Peter Bradshaw, Frenchs colleague at the same newspaper, said, "I sometimes felt more than a little coerced by the emotion being deployed" but added, " lassy is indubitably what this film is - as well as intelligent, high-minded, and touching."  

Neil Smith of the BBC said "The plot may be on the mawkish side, but that doesnt stop Fred Schepisis adaptation . . . being a gentle, affecting mix of road movie and soap opera. It helps that the Australian director has assembled a crack cast . . . Brian Tufanos handsome widescreen photography and Paul Grabowskys excellent music turn this fairly parochial melodrama into something really rather special."  

Time Out New York described it as "Sober, even elegiac in tone, and elegantly shot" and added, " At the films heart is an attempt to suggest the extraordinary nature of ordinary people, and if it fails to achieve profundity, it still makes for one of the most rewarding and authentic depictions of/tributes to the Cockney way of life in recent years."  

==Awards and nominations== National Board London Film Critics Circle Award for Best British Supporting Actress. Fred Schepisi was nominated for the Satellite Award for Best Adapted Screenplay and the Golden Seashell at the San Sebastián International Film Festival.

==References==
 

==External links==
* 
* 
* 

 

 
 
 
 
 
 
 
 