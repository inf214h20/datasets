The Love Special
{{Infobox film
| name           = The Love Special
| image          = The Love Special (1921) - 1.jpg
| alt            = 
| caption        = Newspaper ad
| director       = Frank Urson
| producer       =
| screenplay     = Eugene B. Lewis Frank H. Spearman
| starring       = Wallace Reid Agnes Ayres Theodore Roberts Lloyd Whitlock Sylvia Ashton William Gaden
| music          = 
| cinematography = Charles Edgar Schoenbaum
| editing        = 
| studio         = Famous Players-Lasky Corporation
| distributor    = Paramount Pictures
| released       =  
| runtime        = 50 minutes
| country        = United States
| language       = Silent (English intertitles)
| budget         = 
| gross          =
}}
 silent drama film directed by Frank Urson and written by Eugene B. Lewis and Frank H. Spearman. The film stars Wallace Reid, Agnes Ayres, Theodore Roberts, Lloyd Whitlock, Sylvia Ashton, and William Gaden. The film was released on March 20, 1921, by Paramount Pictures.  
 
==Plot==
 

== Cast ==
*Wallace Reid as Jim Glover
*Agnes Ayres as Laura Gage
*Theodore Roberts as President Gage
*Lloyd Whitlock as Allen Harrison
*Sylvia Ashton as Mrs. Whitney
*William Gaden as William Bucks
*Clarence Burton as Morris Blood
*Snitz Edwards as Zeka Logan
*Ernest Butterworth as Gloomy
*Zelma Maja as Stenographer

== References ==
 

== External links ==
*  
*   at silenthollywood.com

 
 
 
 
 
 
 

 