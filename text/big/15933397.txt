Guilty Hearts
{{Infobox Film
| name           = Guilty Hearts
| image          = 
| caption        = 
| director       = George Augusto / George Gargurevich Krystoff Pizykucki Paul Black Phil Dornfield Ravi Kumar Savina Dellicour Benjamin Ross
| producer       =  Dominic Norris Josie Law Peter Soldinger Stephen Sacks.
| writer         = George Augusto
| narrator       = 
| starring       = Eva Mendes Julie Delpy Stellan Skarsgård Charlie Sheen Anna Faris Kathy Bates Imelda Staunton
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       = June 28, 2011, or September 25, 2012 (sources differ)
| runtime        = 93 min. 
| country        = United States English
| budget         = 
| gross          = 
}} omnibus drama film consisting of six short stories. It is directed by George Gargurevich, Krystoff Pizykucki, Paul Black, Phil Dornfield, Ravi Kumar, and Savina Dellicour, and written by George Augusto.  One source gives Gargurevich as Augusto and also includes director Benjamin Ross. 

Charlie Sheen and Anna Faris star in the episode "Spelling Bee"; Eva Mendes in "Outskirts"; Julie Delpy in "Notting Hill Anxiety Festival"; Stellan Skarsgård in "Torte Bluma"; Kathy Bates in "The Ingrate"; and  Imelda Staunton in "Ready".

It was produced by Dominic Norris, Josie Law, Peter Soldinger, and Stephen Sacks. 

==Cast==
Source unless otherwise noted:   
*Kathy Bates - The Judge
*Julie Delpy - Charlotte
*Andrea Di Stefano - The Ingrate
*Anna Faris - Jane Conelly
* Frank Gallegos - Francisco
*Eva Mendes - Gabriella
* Harriet Rogers - Young Charlotte
* Charles Rubendall - SS Officer
*Charlie Sheen - Himself
* Stellan Skarsgård - Stangl
* Imelda Staunton - Naomi
* Gerard Butler 

==Home Release==
It was released on DVD in the U.S. by Phase 4 Films on June 28, 2011.    or September 25, 2012.   

==Reception==
The film was not released in theaters nor screened for critics. 

==References==
 

==External links==
*  

 
 
 

 