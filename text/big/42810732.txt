Kleinhoff Hotel
{{Infobox film
 | name = Kleinhoff Hotel
 | image = Kleinhoff Hotel.jpg
 | caption =
 | director = Carlo Lizzani
 | writer = Valentino Orsini  Faliero Rosati
 | starring =  
 | music =  Giorgio Gaslini
 | cinematography =  Gábor Pogány
 | editing =  Franco Fraticelli 
 | producer =  
 | language = Italian  
 }} 1977 Erotic erotic drama film directed by Carlo Lizzani.        

== Plot ==
Pascale is a rich and beautiful French lady married to an architect, both often traveling around the world for work and then far away from each other.
Just one of these circumstances, Pascale loses his plane to London and is forced to stay in Berlin. She chooses to stay at Kleinhoff Hotel, where she had lived as a student years earlier.
The room next to her is occupied by Alex, a young fugitive terrorist hunted by German police and instructed by his group to eliminate Pedro, a suspected traitor.

The curiosity and attraction to Alex becomes so overwhelming that Pascale renounces to her departure for London in order to stalk the young terrorist. 

== Cast ==

*Corinne Cléry: Pascale Rostand 
*Bruce Robinson: Karl aka Alex 
*Katja Rupé: Petra
*Werner Pochath: David 
*Peter Kern: Erich Müller
*Michele Placido: Pedro 

==References==
 

==External links==
* 

 

 
 
 
  
 
 

 
 