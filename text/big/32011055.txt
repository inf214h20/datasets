Papapapá
 

Papapapá is a 1995 documentary by Alex Rivera   about immigration. The film stars Augusto Rivera, the director’s father, and Alex Rivera himself. It calls attention to ideas of transculturation, national belonging, cultural imperialism, and cultural globalization, as Rivera chronicles his father’s journey from his native Perú to his life in the United States and parallels it with the journey of the potato.

==Overview==

The film’s title “Papapapa” is a portmanteau of the Spanish terms "potato" and "father," which connects the relationship between the Peruvian roots of the potato and Augusto Rivera. Rivera sets out to connect the worldwide distribution of potatoes to his fathers journey in migrating to the United States. The potato, a vegetable first cultivated by the Incas in Peru, has undergone transformation since the Spanish introduced it to the rest of the world in the 16th century.  The director mirrors the migration of the potato to that of his father; he asserts that in the process of migration, his father has also been culturally transformed like the potato.

Using home footage and satirical undertones, Rivera acknowledges the challenges his father faces in trying to acquire a better life for himself and his family in the United States. He juxtaposes the story of the potato with that of his father, but also often creates images that intertwine the images of both. In one instance, he uses a handcrafted figure with a picture of his fathers face riding a potato from Perú to the United States to capture the essence of both stories he is telling. Rivera crosses the final product of the potato&nbsp;– the potato chip and the French fry, for example&nbsp;– with that of his father, "sitting on the American sofa, eating potato chips and watching Spanish-Language television."

Within the film, he also adds a pseudo television broadcast named “Inca Television” to discuss the colonial history of the potato. This film highlights the widespread acceptance of the white potato in European and American culture. When the potato traveled from the Americas around the 1622, the brown colored potato was excluded. This idealism is still demonstrated within the potato chip industry. Tri-Sum potato chips say that the consumerism of this snack food is strongly competitive. Therefore, they try to offer the best potato chips possible, done by excluding the brown-colored chips.

In modern times, there have been various representations of exclusion to the colored idealism. As Augusto Rivera mentions in his interviews, when he arrived to the United States, there were signs for “Colored” and “White” people, neither of which he identified as. Although he identified as neither, he thought it would be simple to go for the “White” label, as it is the most accepted cultural norm in the United States. Discrimination for colored people are illustrated at a global scale and Augusto Rivera said it was bad in Perú but worsen when he arrived to the United States.

==Analysis==

The film captures the essence of Latino immigrants trying to live "American" lifestyles in an attempt to fulfill a desire for national belonging. Univision,  a television network featured in Spanish, integrates daily news from Latin America. Latin people can maintain an emotional linkage by watching the daily progress of their country of origin while living an American life in the United States.

At a certain extent, Alex Rivera embeds transculturation in his film to illustrate the dichotomies of Perú and the United States. Univision, ”Inca Television,” and the potato carry various forms of representation that merge Peruvian identity and American assimilation. Augusto Rivera mentions staying away from the Spanish people to fulfill the “American Dream.”

Conscious rising about the life of immigrants is perpetuated within Papapapá. Other ideas presented are the roles of NAFTA   and the Border Patrol. Latino are presumed to play roles of housemaids and other while the United States continue prosper in their economy and still seek to leave people south of the border.

==Awards==
*Chicago Intercom Competition-Silver Hugo 1995
*New York Expo of Short Film and Video-Silver Award 1995

==References==
 

 
 
 