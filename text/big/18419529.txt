The Inheritance (2007 film)
{{Infobox Film
| name           = The Inheritance
| image          = 
| image_size     = 
| caption        = 
| director       = Charles-Henri Belleville
| producer       = Tim Barrow David Boaretto 
| writer         = Tim Barrow 
| narrator       = 
| starring       = Tim Barrow  Fraser Sivewright  Imogen Toner  Tom Hardy
| music          = Fiona Rutherford  Freemoore  Rachel Newton
| cinematography = Chris Beck 
| editing        = Man Chong Li 
| distributor    = 
| released       = 
| runtime        = 62 minutes 
| country        =  English
| budget         = £5000
| gross          = 
| preceded_by    = 
| followed_by    = 
}}

The Inheritance is a Scottish road movie following 2 brothers journeying north to find their late father’s inheritance. Made by Lyre Productions, the film was shot for just £5000 and filmed over the course of 11 days in February 2007. It is the debut feature film from director Charles-Henri Belleville and stars Fraser Sivewright, Tim Barrow, Tom Hardy and Imogen Toner.

==Plot==
Two brothers meet at their fathers (Tom Hardy) funeral. David (Tim Barrow) has come up from London, Fraser (Fraser Sivewright) lives in the village he was brought up in. In their fathers old workshop they discover a note instructing them to find his inheritance located somewhere on the Isle of Skye. They set off in his old VW van heading north, finding conflict, comedy and bitter memories as they are forced to confront their past and each other. Picking up a hitchhiker – the mysterious, enigmatic Tara (Imogen Toner) – finally brings them to breaking point as they reach Skye. She leaves them, taking off in the van, and David heads off on foot through the island searching for the destination – a remote place named Cille Chriosd. On a remote beach, as the sun sets, David and Fraser face their final confrontation.

==Production==
The Inheritance was directed by Charles-Henri Belleville and written & produced by Tim Barrow. It was filmed in 11&nbsp;days on miniDV by a cast and crew of 11, entirely on location in Scotland, only using natural light. The script allowed improvisation and the story was adapted to suit circumstances. Says Tim:

 "Originally we were going to improvise everything – work with actors I knew, get a minibus, shoot at specific locations, drive north, film everything, live it, breathe it, edit together and present accordingly. But we needed a story to match the scenery and the music. So I wrote it." 

DOP Chris Beck shot the road trip between Edinburgh and Skye and although the shoot was over in 11 intense days the editing took time to perfect. Those involved were working full-time and had to edit through the night and all weekends. By September the film was finished and had been invited by Raindance Film Festival to premiere at their festival in October 2007.

Success came at the   and throughout Scotland.

==Reception==
The Inheritance received glowing reviews and screened at 11 film festivals including its premiere at Raindance Film Festival where it was nominated Best UK Feature. The film went on to win the Raindance Award at the 2007 British Independent Film Awards.  Further nominations include Best Director & Best Producer at 2008 BAFTA Scotland New Talent Awards,  and Best Debut UK Feature at Londons East End Film Festival.

The film screened in cinemas and film societies across the UK, in Ireland and Canada.
 Time Out named director Charles-Henri Belleville "a talent to watch".

In January 2009, the film was released on DVD, partnered by a 60 documentary showing exactly how its possible to make an award-winning, micro budget, independent feature film.

 
Image:The Inheritance.jpg|   Scottish sea loch

Image:WholecrewatElgol.jpg|   Final days wrap

 
 

==References==
 

==External links==
* 
* 

 
 
 