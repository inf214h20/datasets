Meghasandesam
{{Infobox film
| name           = Meghasandesam
| image          = Megha Sandesham.jpg
| image_size     =
| caption        =
| director       = Dasari Narayana Rao
| producer       = Dasari Padma
| writer         = Dasari Narayana Rao (Dialogues)
| story          = Dasari Narayana Rao
| screenplay     = Dasari Narayana Rao
| narrator       =
| starring       = Akkineni Nageswara Rao Jayasudha Jayaprada Kongara Jaggaiah
| music          = Ramesh Naidu
| cinematography = P. N. Selvaraj
| editing        =
| studio         =
| distributor    = Taraka Prabhu Films
| released       =  
| runtime        = 151 minutes
| country        = India
| language       = Telugu
| budget         =
}}
 Telugu drama film, directed by Dasari Narayana Rao. National Film Awards. 

==Plot==
Meghasandesam is the story of a common man who has an innate urge for poetry. He leads a simple life, marrying a common villager, with children and respected in society. His life is normal until he finds a lady who is a Devadasi (a village dancer), and she inspires his creativity. As a truly inspired poet, he writes excellent poetry. But the other villagers mistake him as being attracted to the dancer.

Consequently after some family drama, his wife leaves him, to let her husband fulfill his poetic thirst. Against social norms, and against customs, and probably as a triumph of love and art, he settles down with the dancer.

==Cast==
* Akkineni Nageswara Rao  as  Ravindra Babu
* Jayasudha  as  Ravindra Babus wife
* Jayaprada  as  Padma
* Kongara Jaggaiah  as  Ravindra Babus brother-in-law
* Mangalampalli Balamuralikrishna  as  Himself
* Subhashini

==Crew== Director - Dasari Narayana Rao
* Story, Dialogues and Screenplay - Dasari Narayana Rao
* Producer - Dasari Padma
* Production company -  Taraka Prabhu Films
* Cinematography - P. N. Selvaraj

==Soundtrack==
The movie was a musical hit, and featured Ashtapadis|Ashtapadi hymns from Jayadeva.
{{Tracklist
| headline        = Songs
| extra_column    = Playback
| all_music       = Ramesh Naidu
| lyrics_credits  = yes

| title1  = Aakaasa Desaana Aashada Masaana
| lyrics1 = Veturi Sundararama Murthy
| extra1  = K. J. Yesudas

| title2  = Aakulo Aakunai Puvvulo Puvvunai
| lyrics2 = Devulapalli Krishnasastri
| extra2  = P. Susheela

| title3  = Mundu Telisena Prabhu
| lyrics3 = Devulapalli Krishnasastri
| extra3  = P. Susheela

| title4  = Navarasa Suma Maalika
| note4   = Poem
| lyrics4 = Veturi Sundararama Murthy
| extra4  = K. J. Yesudas

| title5  = Ninnati Daaka Silanaina
| lyrics5 = Veturi Sundararama Murthy
| extra5  = P. Susheela

| title6  = Paadana Vani Kalyaniga
| lyrics6 = Veturi Sundararama Murthy
| extra6  = Mangalampalli Balamuralikrishna

| title7  = Priye Charusheele
| lyrics7 = Jayadeva
| extra7  = K. J. Yesudas, P. Susheela

| title8  = Radhika Krishna Radhika
| lyrics8 = Jayadeva
| extra8  = K. J. Yesudas, P. Susheela

| title9  = Seetavela Raaneeyaku Sisiraniki Choteeyaku
| lyrics9 = Devulapalli Krishnasastri
| extra9  = K. J. Yesudas, P. Susheela

| title10  = Sigalo Avi Virulo
| lyrics10 = Devulapalli Krishnasastri
| extra10  =
}}

==Awards==
 
|- 1983
| Dasari Narayana Rao
| National Film Award for Best Feature Film in Telugu
|  
|-
| Ramesh Naidu
| National Film Award for Best Music Direction
|  
|-
| P. Susheela
| National Film Award for Best Female Playback Singer
|  
|-
| K. J. Yesudas
| National Film Award for Best Male Playback Singer
|  
|-
| Dasari Narayana Rao
| Nandi Award for Best Feature Film - Swarna Nandi
|  
|-
| Dasari Narayana Rao
| Filmfare Award for Best Film – Telugu
|  
|}

==References==
 

==External links==
*  
 
 
 
 
 
 
 

 