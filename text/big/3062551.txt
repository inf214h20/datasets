Wizard of Oz (1925 film)
{{Infobox film
| name           = Wizard of Oz
| caption        =
| image	=	Wizard of Oz FilmPoster.jpeg
| director       = Larry Semon
| producer       = Larry Semon
| based on       =  
| writer         = Larry Semon Frank Joslyn Baum|L. Frank Baum, Jr. Charles Murray
| music          = Leonard Smith
| editing        = Sam S. Zimbalist
| distributor    = Chadwick Pictures Corporation
| released       =  
| runtime        = 93 minutes
| country        = United States
| language       = Silent film English intertitles
}}

Wizard of Oz is a 1925 American silent film directed by Larry Semon, who also appears in a lead role—that of a farmhand disguised as a Scarecrow. The only completed 1920s adaptation of L. Frank Baums novel The Wonderful Wizard of Oz, this film features a young Oliver Hardy as the Tin Woodman. "L. Frank Baum, Jr." is top-billed with the writing of the script. This is Frank Joslyn Baum, Baums eldest son, and although his actual contribution to the screenplay is doubted by Baum scholar Michael Patrick Hearn, he was certainly involved in the business angle of the production.

==Plot==
A toymaker (Semon) reads L. Frank Baums book to his granddaughter. 

The   (Frederick Ko Vert) appear out of a seemingly empty basket. Kruel sends Wikked on a mission.

Meanwhile, in Kansas,  , who frequently appeared in Semons films) and Hardys and Semons unnamed characters. The latter two are both in love with Dorothy, who favors Hardys character. Aunt Em reveals to Dorothy that she was placed on their doorstep as a baby, along with an envelope and instructions that it be opened only when she turned 18.

On her 18th birthday, however, Wikked and his minions come to the farm and demand the envelope. When Uncle Henry refuses to hand it over, Wikked suborns Hardys character by promising him wealth and Dorothy. Wikked then has Dorothy tied to a rope and raised high up a tower; his men start a fire underneath the rope. Hardys character finds the note, but Semons character takes it and saves Dorothy, only to have Wikked and his men capture them all at gunpoint.

Then a tornado suddenly strikes. Dorothy, the two rivals for her affections and Uncle Henry take shelter in a shed. It (and Snowball) are carried aloft and land in Oz. Dorothy finally reads the contents of the envelope; it declares that she, Princess Dorothea, is the rightful ruler of Oz. Thwarted, Kruel blames the farmhands for kidnapping her and orders the Wizard to transform them into something else, such as monkeys, which he is of course unable to do. Chased by Kruels soldiers, Semons character disguises himself as a scarecrow, while Hardys improvises a costume from the pile of tin in which he is hiding. They are still eventually taken captive. During their trial, the Tin Man accuses his fellow farmhands of kidnapping Dorothy. Kynd has the Scarecrow and Snowball put in the dungeon.
 lion costume, which he uses to scare away the guards. Though the Scarecrow manages to reach Dorothy to warn her against Kruel, he is chased back down into the dungeon by the Tin Man, and ends up getting trapped inside a lion cage (with real lions) for a while. He and Snowball finally escape. 

When Kynd finds Kruel trying to force Dorothy to marry him, they engage in a sword fight. When Kruels henchmen intervene and help disarm Kynd, the Scarecrow saves Dorothy and Kynd. Defeated, Kruel claims that he took Dorothy to Kansas in order to protect her from court factions out to harm her, but she orders that he be taken away.

The Scarecrow is heartbroken to discover that Dorothy has fallen for Prince Kynd. He then flees up a tower from the Tin Man, who tries to blast him with a cannon. Snowball flies a biplane overhead, and the Scarecrow manages to grab a rope ladder dangling underneath it. However, the ladder breaks, and he falls. The scene shifts abruptly back to the little girl, who had fallen asleep. She wakes up and leaves. The grandfather reads from the book that Dorothy marries Prince Kynd and they live happily ever after.

==Cast== Dorothy
*Larry Scarecrow
*Oliver Tin Man Spencer Bell as Snowball / the Cowardly Lion. In the credits he is listed as G. Howe Black. Charles Murray Wizard
*Bryant Prince Kynd Prime Minister Kruel
*Mary Carr as Aunt Em Frank Alexander Uncle Henry
*Virginia Pearson as Lady Vishuss,
*Otto Lederer as Ambassador Wikked
*Frederick Ko Vert as the Phantom of the Basket 

The names of William Hauber and William Dinus appear in the cast list at the beginning of the film, but the characters they play are not given. It is also thought that Chester Conklin and Wanda Hawley made minor appearances in the film, but their names do not appear in the credits. 

==Production== 1939 version.

The major departure from the book and film is that the Scarecrow, the Tin Man, and the Cowardly Lion are not actually characters, but are in fact disguises donned by three farm hands who find themselves swept into Oz by a tornado. Dorothy is here played by Dorothy Dwan — Semons wife - as a young woman. In a drastic departure from the original book, the Tin Man is a villain.
 the 1902 Oz books.

==Reception==
According to Ben Mankiewicz of Turner Classic Movies, the film was poorly received by critics and audiences. Chadwick Pictures went bankrupt during its run, and many theaters did not even receive a print to show. Semon did not fully recover from the financial debacle, and died only a few years later in 1928.

==Home media==
The film is in the public domain,  and many home media releases of the film, including Betamax, VHS, Laserdisc, Capacitance Electronic Disc|CED, DVD, HD DVD and Blu-ray Disc, are available.

The film is also included in all of the home media releases of the The Wizard of Oz along with earlier silent films based on the Oz stories, beginning with the 2005 3-disc Collectors Edition of the film.

==Music==
The films premiere in 1925 featured original music orchestrated by Louis La Rondelle, conducted by Harry F. Silverman, featuring Julius K. Johnson at the piano.

Many home video releases of the film completely lacked a score, as with many early releases of public domain silent films.

The version with an organ score performed by Rosa Rio was made in 1986, and was included in the Video Yesteryear edition.

In 1996, a new version was made. This version was included in all of the home media releases of the film, beginning with the "L. Frank Baum Silent Film Collection of Oz", released by American Home Entertainment on November 26, 1996, and features a score performed by Mark Glassman and Steffen Presley, and a narration performed by Jacqueline Lovell.

In 2005, another version was made. This version features original music composed and arranged by Robert Israel and performed by the Robert Israel Orchestra (Europe), and is included in all of the home media releases of the 1939 film, beginning with the 2005 3-Disc Collectors Edition DVD of the film. 

==See also== Adaptations of The Wizard of Oz

==References==
 

==External links==
* 
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 