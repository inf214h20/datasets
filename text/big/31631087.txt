Amour de poche
{{Infobox film
| name           = Amour de poche
| image          = 
| caption        = 
| director       = Pierre Kast
| producer       = Cila Films
| writer         = France Roche Waldemar Kamempfert
| starring       = Jean Marais Geneviève Page
| music          = Georges Delerue
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 88 minutes
| country        = France
| language       = French
| budget         = 
}}
Amour de poche ( ) is a French comedy Fantasy film from 1957, directed by Pierre Kast, written by France Roche, starring Jean Marais. The scenario was based on a novel "Diminishing Draft" of Waldemar Kamempfert.  The film was known under the title "Girl in His Pocket" (USA), "Alchimie der Liebe" (West Germany), "Nude in His Pocket". 

== Cast ==
* Jean Marais : Jérôme Nordman
* Geneviève Page : Édith Guérin
* Jean-Claude Brialy : Jean-Loup
* Agnès Laurent : Monette Landry
* Alexandre Astruc : 1er employé des objets trouvés
* Christian-Jaque : 2e employé
* Léo Joannon : 3e employé
* Jean-Pierre Melville : commissar
* Hubert Deschamps : inspector
* Boris Vian : manager of baths
* Alex Joffé : seller of articles of camping
* France Roche : Anne-Lise
* Michel André : secretary in the police office
* Yves Barsacq : student
* Alfred Pasquali : Bataillon
* Jacques Hilling : professor
* Amédée (acteur)|Amédée : Maubru
* Régine Lovi : Brigitte
* Joëlle Janin : Solange
* Victor Vicas : the secretary of town hall

== References ==
 

== External links ==
*  
*   at the Films de France

 
 
 
 
 
 
 
 
 
 


 
 