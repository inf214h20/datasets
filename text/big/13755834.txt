The Bigamist (1953 film)
{{Infobox film
| name           = The Bigamist
| image          = The Bigamist 1953.jpg
| caption        = 
| director       = Ida Lupino
| producer       =
| writer         = Collier Young Larry Marcus Lou Schor
| starring       = Joan Fontaine  Ida Lupino  Edmond OBrien Edmund Gwenn
| music          =
| cinematography = George E. Diskant
| editing        = Stanford Tischler
| distributor    = The Filmmakers
| released       =  
| runtime        = 80 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
| preceded_by    =
}}
The Bigamist is a 1953 film noir directed by and starring Ida Lupino. The films other leads are Edmund OBrien, Joan Fontaine and Edmund Gwenn. It was written by Collier Young from a story by Larry Marcus and Lou Schor. Young was married to Fontaine at the time and had previously been married to Lupino. The Bigamist has been cited as the first film in which a female star of the film directed herself.    

==Plot==
Harry (Edmond OBrien) and Eve Graham (Joan Fontaine) want to adopt a child, as Eve is infertile. Adoption agent Mr Jordan (Edmund Gwenn) warns the couple that he would need to investigate them thoroughly. Harry looks curiously at Jordan, something that worries Jordan.

Harry and Eve live in San Francisco and are co-owners of a business, with Harry traveling to Los Angeles frequently for work. Jordan arrives at Harrys L.A. office looking for information about Harry. The receptionist calls around to all the hotels but none of them have a Harry Graham registered. One or two of the managers remembers Harry but he hadnt been checked in to their hotels in months. Jordan is very puzzled and even more adamant in investigating Harry. He finds a letter opener on Harrys desk with the name Harrison Graham.  Jordan visits the address listed for that name in the phone book and there finds Harry, with a wife and baby. When Jordan is about to call the police, Harry tells him how he got into the situation.

Upon learning of Eves infertility, Harry had suggested that she join him in his business as means of coping with her disappointment. Though shed done well at work, she soon began focus solely on the business, leaving Harry feeling lonely for an emotional spousal connection.  His feelings of loneliness were most acute during the long stretches he spent away from her as he traveled.  On a particular day, while staying in a hotel in L.A., Harry met an interesting woman named Phyllis (Ida Lupino). They talked and spent time together, but parted with Harry not expecting to see her again.

Talking on the phone with Eve that night, Harry tried to tell her everything about Phyllis, and about his loneliness, but Eve was only interested in talking about business. Back home, he tried again, planning a vacation for the two of them, but she dismissed the idea, noting that she was pleased with the state of their marriage. On his next trip to L.A., Harry began seeing Phyllis again, platonically at first, but romantic feelings developed.  Not wanting to fall in love, Phyllis had not allowed Harry to share with her anything about his background, and thus remained ignorant of his marriage.  On Harrys last night in town, his birthday, they spent the night together.

Upon returning home, Harry was resolved to rededicate himself to his marriage, starting with planning to hire someone else to handle the L.A. business so that Harry would no longer have to be away from Eve.  He was overjoyed to find that this time, Eve was fully receptive.  She acknowledged and apologized for having been so emotionally distant.  She embraced the idea of their adopting a child, after having rejected it out of hand years before.  The single piece of bad news was that her father had taken ill and she needed to go spend time with her family in Florida.  

Harry stayed close to home and began the adoption process. Three months later, with Eve still away, Harry had no choice but to return to L.A. to tend to the business interests there.  Once there, he found that Phyllis was pregnant.  She told Harry that she didnt wish to trap him and that he was free to leave.  However, Harry had no interest in turning his back on the responsibility he felt to her and to their child.  He planned to call Eve, confess his infidelity, and ask for a divorce, but then came the news of her fathers death. Hearing how distraught she was, he couldnt go through with his plan. But he couldnt bring himself to leave Phyllis either, and instead proposed to her.  With Eve pinning all of her hopes for happiness on becoming a mother, Harry had hoped to maintain his secret double life long enough for the adoption to be finalized and then divorce Eve, who would then at least still have her child. 

Upon hearing the story, Jordan leaves without calling the police. Harry writes a farewell letter to the sleeping Phyllis and leaves the house. Eve returns to their home in San Francisco as Harry is about to meet the police who are waiting for him there.   Harry ends up in court, where the two women finally meet. The judge notes that once Harry has served his sentence, hell be legally obligated to support both women.  And with regard to Harrys personal life, "it wont be a question of which woman hell go back to, but rather which woman will take him back."  The film ends with Harry awaiting his sentencing hearing.

==Cast==
*Edmond OBrien as Harry Graham
*Joan Fontaine as Eve Graham
*Ida Lupino as Phyllis Martin
*Edmund Gwenn as Mr Jordan
*Kenneth Tobey as Tom Morgan
*Jane Darwell as Mrs. Connelley
*Peggy Maley as Phone Operator
*Lillian Fontaine as Miss Higgins
*Matt Dennis as Himself John Maxwell as Judge

==Reception== Carl Dreyer and Nicholas Ray".     The Encyclopedia of Film Noir considers The Bigamist to be "unusually ambiguous" for the period.    Ray Hagen and Laura Wagner remark that The Bigamist is "not a sensationalized rendering of a potentially sordid subject, but a very human story of a man (Edmond OBrien) tangled between two women".   

== Production note ==
The film contains in-jokes referring to co-star Edmund Gwenns most famous film role, playing Kris Kringle in Miracle on 34th Street.  At one point, the Harry Graham character derisively refers to Gwenns Mr. Jordan character as "Santa Claus".  In another scene, Harry is on a tour bus in Beverly Hills, driving past the homes of movie stars.  One of the homes visited on the tour is that of Edmund Gwenn, with the tour operator noting that Gwenn is known to the world as Santa Claus from Miracle on 34th Street.  Harry then comments about how much he enjoyed the film.

==References==
 

==External links==
* 
* 
* 
*   complete film on YouTube

 
 
 
 
 
 
 
 
 
 
 
 