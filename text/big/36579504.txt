The Unguarded Moment (film)
{{Infobox film
| image          = Unguarded 1sheet.jpg
| caption        = Theatrical release poster Gordon Kay
| director       = Harry Keller
| screenplay     = Herb Meadow Larry Marcus
| story          = Rosalind Russell John Saxon
| music          = Herman Stein
| cinematography = William H. Daniels
| editing        = Edward Curtiss
| distributor    =Universal Pictures
| released       = November 1956
| runtime        = 95 minutes
| country        = United States
| language       = English
}} John Saxon.   

==Plot==
Lois Conway (Williams) works as a music teacher at a local high school in a small town, where recently a woman was found murdered. When she starts receiving notes from an anonymous admirer, she suspects her favorite student Sandy (Wilder) is responsible, and tells him they could never be lovers. The notes grow more violent and when, in her latest letter, she is invited to meet at the schools lockers at night, Lois decides to visit, hoping to stop the young man. There, she is attacked by an initial shadowy figure, whom she later identifies as Leonard Bennett (Saxon), the high schools star football player. 

She successfully gets away, though drops her purse, and is aided by Lieutenant Harry Graham (Nader). Graham advises her to press charges, but Lois wants to drop the matter in hopes of it blowing over. 

Back at home, she notices her purse on her table, and aware that the thief is in her home, orders him to leave. As he bashes through the door to get away, Lois is now certain that Leonard is her attacker. 

Leonard is able to get home without his dominant and overbearing father (Andrews) noticing he is gone. Mr. Bennett lectures his son on the dangers of women, stimulated by the occurrence of him being left by his wife and Leonards mother when he was very ill.

The following day, Lois reports the incident to the principal Pendleton (Tremayne), but when Leonard denies the whole matter, Pendleton protects the schools most valuable athletic asset by suggesting to Lois that she should provide evidence. 

Soon the story spreads around school, and with gossip surrounding Lois allegedly pursuing Leonard, both her personal and professional life becomes a mess. One day, she pulls him out of class and tries to reason with him, but he refuses to listen to her. Meanwhile, she grows closer to Graham, who does not understand why she is sympathetic to Leonard. 

Nonetheless, she decides to visit the Bennetts, but the father does not want her to interfere with his son and accuses her of seducing Leonard. He is startled, though, upon finding out the police are now involved in the matter. Mr. Bennett is unaware that Leonard again sneaked out of his room to visit a waitress whom he has dated in the past.

Sometime later, Graham accompanies Lois to a football game, where Graham is inspired to retrieve Leonards fingerprints from his locker. It turns out the fingerprints match those found at Lois place. 

At a school dance, she tries to warn Leonard about the police discovery, assuring him he will get into big trouble if he does not come clean. Leonard, for the first time, speaks truthfully to her, but they are interrupted by Mr. Bennett, who convinces Leonard Lois is manipulating him. Leonard asks her to meet him in the cloak room to discuss the matter, but Lois is unaware Mr. Bennett and Pendleton are hiding in the same room. Her presence convinces them she must be having an affair with the teenager. Lois, being tricked by Leonard, falls into Grahams arms, and finally allows him to arrest the kid.

At the police station, the now suspended Lois is brought in by Graham to get an honest confession from Leonard. During the interrogation, the couple is informed that another man has admitted to having committed the murder. Graham wants to continue prosecuting Leonard for breaking into Lois apartment, but she wants to drop the case and orders him to bring the boy home. 

Back home, Lois is about to undress, when suddenly Mr. Bennett jumps out of her closet and starts assaulting her. At the same moment, Leonard, impressed by having been forgiven by his teacher, confesses to Graham that his father is responsible for the murders. Graham decides to share the news with Lois and arrives at her home just in time to save her from being murdered by Mr. Bennett. Bennett suffers a heart attack after attempting to flee the scene, and dies in front of Leonard.

==Cast==
* Esther Williams as Lois Conway
* George Nader as Lieutenant Harry Graham John Saxon as Leonard Bennett
* Edward Andrews as Mr. Bennett
* Les Tremayne as Principal Pendleton
* Jack Albertson as Prof
* Dani Crayne as Josie Warren
* John Wilder as Sandy
* Edward Platt as Attorney Briggs
* Eleanor Audley as Mr. Pendletons Secretary Robert Williams as Detective

==Production==
===Development===
The origin of The Unguarded Moment is as surprising as Esther Williams casting in it. According to biographer Bernard F. Dick in Forever Mame: The Life of Rosalind Russell, the story idea came from writer Larry Marcus and Rosalind Russell, as a possible vehicle for herself. (Marcus had written scripts for Russells own production company. KRAMER AND KATZ PLAN MOVIE FIRM: Producer and Theatre Owner Negotiating on Establishment of New Coast Company Of Local Origin
By THOMAS F. BRADY Special to THE NEW YORK TIMES.. New York Times (1923-Current file)   24 Oct 1950: 32.  )

The first draft of the screenplay by Marcus and Russell (under the pseudonym C.A. McKnight, her mothers maiden name) had a working title of Teach Me to Love and was completed by 1951.   

In a 1951 draft of the story, Harry Graham was a fellow teacher instead of a policeman, and Leonard Bennett was revealed to be responsible for the murders, before being killed. Forever Mama: The Life of Rosalind Russell by Bernard F. Dick. p. 141.  

In 1952 it was announced the story, called The Hidden Heart had been purchased by Benagoss Productions, who had made The Green Glove. Herbert Meadow had worked on the script and Rudolph Mate was to direct. Benagoss to Produce Hidden Heart; Carroll Back for Condors Nest
Schallert, Edwin. Los Angeles Times (1923-Current File)   17 Jan 1952: B9.   The story had been originally set in France but was relocated to California. Possible female stars discussed in the press included Olivia de Havilland, Loretta Young and Teresa Wright. Drama: Benagoss Will Produce Feature Here; Noted Ballets in Kelly Film
Schallert, Edwin. Los Angeles Times (1923-Current File)   23 June 1952: B7.   The movie was not made.

Russell had no time to work on the screenplay as she became busy with back to back Broadway productions including Wonderful Town, The Girl Rush and Picnic. As a result, she didnt return to the project until 1955 when Marcus and scenarist Herb Meadow had made further revisions to the script under working titles of The Lie and The Hidden Heart.  

In October 1955 it was announced Universal International had purchased the script and Gordon Kay was going to produce. Drama: Saint Husband, Jeffrey Hayden, Pacted; Hidden Heart Unique Purchase
Schallert, Edwin. Los Angeles Times (1923-Current File)   14 Oct 1955: B9.   (It was later estimated Russell and Marcus made $50,000 for their work on the film. BY WAY OF: Oedipus Rex to Face camera--Other Items
By A.H. WEILER. New York Times (1923-Current file)   29 Jan 1956: 101.  )

In November Esther Williams signed on to play the lead and the film was called The Gentle Webb. ESTHER WILLIAMS TAKES A DRY ROLE: Swim Star to Act a Straight Part in Screen Play About a High School Teacher
Special to The New York Times.. New York Times (1923-Current file)   23 Nov 1955: 17.  

===Casting=== John Saxon had yet to make a name for himself when the film went into production. He received the co-starring role after several screen tests, and the studio attempted to make him fill the void actor James Dean left when he died.  

For Esther Williams, the film was her first since her contract ended with Metro-Goldwyn-Mayer, and it proved to be her first non-swimming dramatic role since The Hoodlum Saint (1946).  

In her autobiography, she noted:
 I thought it was a curious choice for Universal to offer me the lead in a dry psychological thriller, and I wasnt sure the public would accept me without my glittering crowns and sparkly swimsuits. Nonetheless, Universal offered me $200,000, which was more than I ever made for a single film at MGM in or out of the water...Later, after we had started shooting, Rosalind Russell came up to me at a party and said, I hear youre doing my script. I looked at her blankly until she explained that she had written it under the pseudonym C. A. McKnight. I wrote the part for me, but I got too old."  

==Reception==
The film flopped at the box office.  Saxon received warm reviews for his performance of Leonard Bennett.  

Some reviewers lauded Williams performance, others lamented her change of milieu.  Russell later wrote that Williams was "very good in" the film. 

The Chicago Tribune said that "the film is a pleasant surprise... the well knit story has suspense and the dialogue is excellent." Miss Williams Star of Sharp Mystery Film: "THE UNGUARDED MOMENT"
TINEE, MAE. Chicago Daily Tribune (1923-1963)   28 Sep 1956: a10.  

The Los Angeles Times said "it carries enough suspense to please any but rabid devotees". Swim Star Stays Dry in Drama
Scott, John L. Los Angeles Times (1923-Current File)   04 Oct 1956: B7.  

In present days, the film is regarded as a B film, despite the fact it was heavily promoted in the 1950s.   

According to Turner Classic Movies, Williams "unintentionally accents   limitations as a dramatic actress though she still looks gorgeous.   While Esther Williams is the top-billed star of The Unguarded Moment, it is Andrews unexpectedly creepy performance that hijacks the film and imbues it with an underlying mood of malice and menace." 

==References==
 

==External links==
*  
*  
*  
*   review at Film Noir of the Week by Wheeler Winston Dixon
*  

 
 
 
 
 
 
 
 
 
 
 