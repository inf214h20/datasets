List of horror films of 1964
 
 
A list of horror films released in 1964 in film|1964.

{| class="wikitable" 1964
|-
! scope="col" | Title
! scope="col" | Director
! scope="col" | Cast
! scope="col" | Country
! scope="col" | Notes
|-
!   | 2000 Maniacs
| Herschell Gordon Lewis || Connie Mason, Shelby Livingston, Michael Korb ||   ||  
|-
!   | The Black Torment John Turner, Ann Lynn ||   ||  
|- Blood and Black Lace Cameron Mitchell, Thomas Reiner ||     ||   
|-
!   | Castle of Blood
| Antonio Margheriti || Georges Riviere, Barbara Steele ||   ||  
|-
!   | Castle of the Living Dead Lorenzo Sabatini Philippe Leroy ||    ||  
|-
!   | Der Fluch der Gruenen Augen
| Akos von Ratony || Adrian Hoven, Carl Mohner, Erika Remberg ||    ||  
|-
!   | The Creeping Terror
| Art J. Nelson || Arthur Nelson, Shannon ONeil, William Thourlby ||   ||  
|-
!   | The Curse of the Living Corpse
| Del Tenney || Helen Warren, Roy Scheider, Margot Hartman ||   ||  
|-
!   | The Curse of the Mummys Tomb Ronald Howard, Fred Clark ||   ||  
|- Devil Doll
| Lindsay Shonteff || Bryant Halliday, William Sylvester, Yvonne Romain ||    ||  
|-
!   | Dr. Orloffs Monster
| Jesús Franco || Hugo Blanco, Agnès Spaak, Perla Cristal ||    ||  
|-
!   | Dungeon of Harrow
| Pat Boyette || Eunice Grey, Helen Hogan, Michele Buquor ||   ||  
|-
!   | The Evil of Frankenstein
| Freddie Francis || Peter Cushing, Peter Woodthorpe, Duncan Lamont, Sandor Eles ||   ||  
|-
!   | Face of the Screaming Werewolf Lon Chaney, Donald Barron, Yolanda Varela ||   ||  
|- The Flesh Eaters Jack Curtis || Byron Sanders, Ray Tudor ||   ||  
|-
!   | The Gorgon
| Terence Fisher || Peter Cushing, Christopher Lee, Richard Pasco ||   ||  
|-
!   | The Horror of Party Beach John Scott ||   ||  
|-
!   | I Eat Your Skin
| Del Tenney || Heather Hewitt, William Joyce ||   ||  
|-
!   | The Incredibly Strange Creatures Who Stopped Living and Became Mixed-Up Zombies
| Ray Dennis Steckler || Cash Flagg, Brett OHara, Carolyn Brandt, Atlas King ||   ||  
|-
!   | Kwaidan (film)|Kwaidan
| Masaki Kobayashi || Rentarō Mikuni, Michiyo Aratama, Misako Watanabe ||   || Costume horror 
|- The Last Man on Earth
| Ubaldo Ragona, Sidney Salkow || Vincent Price, Giacomo Rossi-Stuart, Tony Cerevi ||    ||  
|- The Masque of the Red Death
| Roger Corman || Vincent Price, Hazel Court, Jane Asher ||    ||  
|-
!   | Das Ungeheuer von London City
| Edwin Zbonek || Marianne Koch, Hansjörg Felmy, Dietmar Schönherr ||   ||  
|-
!   | Monstrosity (film)|Monstrosity
| Joseph Mascelli || Frank Gerstle, Erika Peters ||   ||  
|- The Night Walker Robert Taylor, Hayden Rorke ||   ||  
|-
!   | Cien Gritos de Terror
| Ramón Obon || Joaquín Cordero, Ariadne Welter, Ofelia Montesco ||   ||  
|-
!   | Onibaba (film)|Onibaba
| Kaneto Shindo || Nobuko Otowa, Jitsuko Yoshimura, Kei Sato ||   ||  
|-
!   | Pyro Barry Sullivan, Martha Hyer ||    ||  
|-
!   | Strait-Jacket Leif Erickson ||   ||  
|-
!   | La Cripta e lIncubo
| Camillo Mastrocinque || Adriana Ambessi, Christopher Lee, Pier Ana Quaglia ||    ||  
|-
!   | The Tomb of Ligeia John Westbrook ||    ||  
|- War of the Zombies
| Giuseppe Vari || John Drew Barrymore, Susy Andersen, Ettore Manni ||   ||  
|-
!   | Witchcraft (1964 film)|Witchcraft
| Don Sharp || Lon Chaney, Jr., Jack Hedley, Jill Dixon ||   ||  
|-
!   | Las Luchadoras contra la Momia
| René Cardona || Lorena Velázquez, Elisabeth Campbell, Armando Silvestre ||   ||  
|}

==References==
 

==Citations==
 
*  <!--
-->
*  <!--
-->
*  <!--
-->
*  <!--
-->
*  <!--
-->
 

 
 
 

 
 
 
 