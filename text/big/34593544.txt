Rubber Heels
{{infobox film
| name           = Rubber Heels
| image          =
| imagesize      =
| caption        =
| director       = Victor Heerman
| producer       = Adolph Zukor Jesse Lasky
| writer         = Thomas J. Crizer Ray Harris J. Clarkson Miller Sam Mintz William Le Baron
| starring       = Ed Wynn Thelma Todd
| music          =
| cinematography = J. Roy Hunt
| editing        = 
| distributor    = Paramount Pictures
| released       = June 11, 1927
| runtime        = 7 reels(6,303 feet)
| country        = United States
| language       = Silent film(English intertitles)
}}
Rubber Heels is a 1927 silent film comedy produced by Famous Players-Lasky and distributed through Paramount Pictures. It stars stage comedian Ed Wynn in his first motion picture.  

==Cast==
*Ed Wynn - Homer Thrush
*Chester Conklin - Tennyson Hawks
*Thelma Todd - Princess Anne Robert Andrews - Tom Raymond
*John Harrington - Grogan
*Bradley Barker - Gentleman Joe
*Armand Cortez - The Ray
*Ruth Donnelly - Fanny Pratt
*Mario Majeroni - Prince Zibatchefsky
*Truly Shattuck - Mrs. P. Belmont-Fox

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 
 


 