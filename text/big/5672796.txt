Lone Wolf and Cub: Baby Cart in the Land of Demons
{{Infobox film
| name = Lone Wolf and Cub:  Baby Cart in Land of Demons
| image = Lonewolf5.jpg
| caption = Cover of the AnimEigo DVD.
| director = Kenji Misumi
| producer = Tomisaburo Wakayama Masanori Sanada
| writer = Kazuo Koike Goseki Kojima
| starring = Tomisaburo Wakayama
| music = Hideaki Sakurai
| cinematography = Fujio Morita
| editing =
| distributor = Toho
| released =  
| runtime = 89 min.
| rating =
| country = Japan
| awards =
| language = Japanese
| budget =
}}
Lone Wolf and Cub: Baby Cart in Land of Demons ( s based on the long-running Lone Wolf and Cub manga series about Ogami Ittō, a wandering assassin for hire who is accompanied by his young son, Daigoro.

==Synopsis== Ogami Daigoro is again separated from his father and proves his courage and sense of honor as he refuses to admit the guilt of a woman pickpocket he became mixed up with.

==Cast== Ogami Ittō Daigoro
* Akira Yamauchi as Ayabe Ukon, Messenger 1
* Hideji Otaki as Mogami Shusuke, Messenger 2
* Taketoshi Naitô as Mawatara Hachiro, Messenger 3
* Fujio Suga as Kikuchi Yamon, Messenger 4
* Rokko Toura as Sazare Kanbei, Messenger 5
* Shingo Yamashiroas Lord Kuroda Naritaka Tomomi Sato as "Quick Change" Oyô
* Michiyo Ookusu as Shiranui
* Koji Fujiyama as Tomekichi the Moll
* Sumida Kazuyo as Hamachiyo
* Bin Amatsu as Inspector Senzo
* Taizen Shishido as Izumi Kazuna
* Eiji Okada as Wakita
* Minoru Ohki as Yagyu Retsudo

==Production== Tatsuo Endo.
*  .

==External links==
*  
*  
 
 
 

 
 
 
 
 
 