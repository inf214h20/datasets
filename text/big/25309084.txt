National Lampoon's Golf Punks
{{Infobox Film
| name = National Lampoons Golf Punks
| image = GolfPunks.jpg
| image size =
| caption = DVD cover
| director = Harvey Frost
| producer =
| writer = Jill Mazursky
| narrator = Tom Arnold
| music =
| cinematography =
| editing =
| distributor = Fox Family Channel Avalanche Home Entertainment Lionsgate (DVD)
| released = September 6, 1998
| runtime =
| country = United States
| language = English
| budget =
| gross =
| preceded by =
| followed by =
}} Tom Arnold as an out-of-work golf pro, who gets pulled into teaching the game to a group of young golfers at a public course. 

==Cast== Tom Arnold - Al Oliver James Kirk - Peter
* Rene Tardif - Allister McGrath
* Katelyn Wallace - Christina
* Rhys Huber - Billy
* Neil Denis - Thork
* Tarik Batal - Raghad
* Katrina Pratt - Mai
* Greg Thirloway - Bo
* Cory Fry - Cameron
* Alf Humphreys - Jack (as Alfred E. Humphreys)
* Elizabeth Carol Savenkoff - Nancy
* Jano Frandsen - Trip Davis
* French Tickner - Burt
* Lee Taylor - Harry
* Jerry Wasserman - Joe
* Dave Squatch Ward - Tiny
* Lachlan Murdoch - Bernie
* William MacDonald - Phelps
* Marcus Hondro - Dave McMillan
* Jonathan Palis - Jake
* Tara Lea - Ms. Williamson
* Brendan Beiser - News Reporter

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 


 
 