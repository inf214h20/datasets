Punk's Not Dead (2007 film)
{{Infobox film
| name           = Punks Not Dead
| image          = Punks not dead post.jpg
| image_size     =
| caption        = Theatrical release poster
| director       = Susan Dynner
| producer       = Susan Dynner Todd Traina
| writer         =
| narrator       =
| starring       =
| music          =
| cinematography = Susan Dynner Markus G. Kaeppeli
| editing        = Patrick Nelson Barnes
| distributor    = Vision Films
| released       =  
| runtime        = 93 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}

Punks Not Dead is a 2007 documentary film directed by Susan Dynner, an American hardcore punk fan. 
    The film claims to infiltrate American clubs, malls, recording studios, etc. where it sets out to claim hardcore punk and pop punk music is "thriving" from an American perspective. Its content features performances largely from 1980s hardcore bands and MTV skate punk and pop punk/rock acts. It also includes various interviews and behind-the-scenes footage with the bands, labels and fans.

== Film festivals ==
The film appeared in film festivals around the world for about a year prior to its theatrical release. Punks Not Dead premiered at the Cannes Film Festival on June 23, 2006. 
   The film first public showing was at the Silverdocs AFI/Discovery Channel Documentary Festival in June 2006 and went on to screen at numerous others. 
   The following is a list of film festivals in 2006 where the film was shown. 

*Cannes Film Festival
*Silverdocs AFI/Discovery Channel Documentary Festival (in Maryland)
*Melbourne International Film Festival
*San Francisco International Film Festival
*Buenos Aires International Festival of Independent Cinema
*Independent Film Festival of Boston
*Cleveland International Film Festival
*AFI Dallas International Film Festival
*Wisconsin Film Festival
*In-Edit Film Festival (in Barcelona)
*Copenhagen International Documentary Festival
*Gijón International Film Festival

== Appearances ==
The following people appear in the documentary. 
  
*Craig Aaronson (A&R for Reprise Records)
*Colin Abrahall (vocalist for Charged GBH)
*Jan Nils Ackermann (guitarist for The Vandals)
*Lorraine Ali (music editor of Newsweek)
*Quinn Allman (guitarist for The Used)
*Nick "Animal" Kulmer (vocalist for Anti-Nowhere League)
*Billie Joe Armstrong (guitarist and vocalist for Green Day) Operation Ivy and Rancid (band)|Rancid; founder of Hellcat Records; producer) Brian Baker (guitarist for Bad Religion)
*Dave Baksh (former guitarist for Sum 41)
*Brian Barnes (guitarist for the U.K. Subs)
*Jay Bentley (bass guitarist for Bad Religion)
*Jello Biafra (former vocalist for Dead Kennedys)
*Rodney Bingenheimer (disc jockey on KROQ-FM)
*Colin "Jock" Blyth (guitarist for Charged GBH)
*Riley Breckenridge (drummer for Thrice)
*Lisa Browlee (of the Warped Tour)
*Steve Bruce (drummer for Cock Sparrer)
*Phil Bryant (bass guitarist for the Subhumans (UK band)|Subhumans)
*Didit (Guitarist for BRASSO)
*Wattie Buchan (vocalist for The Exploited)
*Willie Buchan (drummer for The Exploited)
*Jake Burns (vocalist for Stiff Little Fingers)
*Peter Bywaters (vocalist for Peter and the Test Tube Babies)
*Alan Campbell (guitarist for the U.K. Subs)
*Brendan Canty (drummer for Fugazi (band)|Fugazi)
*Becky Carriage (of Drunk Tank)
*Carrie (of Drunk Tank)
*Nick Cash (of 999 (band)|999)
*Keith Clark (drummer for the Circle Jerks)
*Pat Collier (bass guitarist for The Vibrators)
*Claire Costa (of Drunk Tank)
*Mitch Cramer (tour manager for Green Day)
*Crazy Danny Culpables Michael Davenport (bass guitarist for The Ataris)
*Dominic Davi (bass guitarist and song writer for Tsunami Bomb)
*Pete Davies (drummer for the U.K. Subs) Michael Davis (bass guitarist for MC5)
*Jane Davison
*Kid Dee Davison (drummer for The Adicts)
*Pete Dee Davison (guitarist for The Adicts)
*Guy Days (guitarist and vocalist for 999)
*Kevin De Franco (guitarist and vocalist for The God Awfuls)
*Ashley Dekoster (of Drunk Tank)
*Steve Diggle (guitarist for Buzzcocks) John Doe (bass guitarist for X (U.S. band)|X)
*Fletcher Dragge (guitarist for Pennywise (band)|Pennywise) Indie 103.1) Chris "Magoo" Exall (guitarist for the Anti-Nowhere League)
*Eyeball (of Drunk Trunk) The Shocker) Peter Finestone (drummer for Bad Religion)
*Warren Fitzgerald (guitarist for The Vandals; cofounder of Kung Fu Records)
*Micky Fitz (vocalist for vocalist The Business)
*Bruce Foxton (bass guitarist for The Jam and Stiff Little Fingers) Rancid and Lars Frederiksen and the Bastards) Channel 3)
*Nicky Garratt (guitarist for the U.K. Subs)
*Alvin Gibbs (bass guitarist for the U.K. Subs)
*Cooper Gilespie (vocalist for Bang Sugar Bang) Black Flag)
*Ursula Glaviano (of Drunk Tank)
*Greg Graffin (vocalist for Bad Religion)
*Steve Grantley (drummer for Stiff Little Fingers and The Alarm)
*Dan Graziani (violinist, pianists, and mandolinist for The Adicts) Iron Cross)
*Jack Grisham (vocalist for T.S.O.L.)
*Jim Guerinot (manager for The Offspring and Social Distortion)
*Brett Gurewitz (guitarist for Bad Religion and founder of Epitaph Records) Charlie Harper (vocalist for the U.K. Subs)
*Adam Hecht (of Enough Fanzine)
*Greg Hetson (guitarist for Bad Religion and the Circle Jerks)
*Dexter Holland (guitarist and vocalist for The Offspring; co-founder of Nitro Records)
*Jeph Howard (bass guitarist for The Used) Generation X)
*Frank Iero (former guitarist for My Chemical Romance)
*Steve Jocz (drummer for Sum 41)
*Inge Johansson (bass guitarist for The (International) Noise Conspiracy)
*Barry Jones (co-founder of The Roxy nightclub)
*Joe Keithley A.K.A Joey Shithead (guitarist and vocalist for D.O.A. (band)|D.O.A.)
*Keith "Monkey" Warren (vocalist for The Adicts)
*Jake Kolatis (guitarist for The Casualties) Wayne Kramer (guitarist for MC5)
*Pablo LaBritain (drummer for 999)
*Joyce Lacovara Levi (of Pogo Atak) Terri Laird
*Joe Lally (bass guitarist for Fugazi (band)|Fugazi)
*Cindy Levitt (vice-president of licensing for Hot Topic)
*Jim Lindberg (vocalist for Pennywise (band)|Pennywise)
*Bruce Loose (bass guitarist and vocalist for Flipper (band)|Flipper) Dick Lucas (vocalist for the Subhumans (UK band)|Subhumans)
*Kyle Lumsden (bass guitarist for The God Awfuls) Warped and Taste of Chaos tours; co-founder of Warcon Enterprises)
*Dennis Lyxzén  
*Ian MacKaye (vocalist for Minor Threat, Embrace (U.S. band)|Embrace, and Fugazi (band)|Fugazi)
*Benji Madden (guitarist for Good Charlotte) Channel 3)
*Glen Matlock (bass guitarist for the Sex Pistols) Jason "Cone" McCaslin (bass guitarist for Sum 41)
*Mike McColgan (vocalist for Dropkick Murphys, now for Street Dogs)
*Gary McCormack (bass guitarist for The Exploited)
*Bert McCracken (vocalist for The Used)
*John McGivern
*Legs McNeil (co-founder and writer for Punk (magazine)|Punk; senior editor for Spin (magazine)|Spin; author)
*Eric Melvin (guitarist for NOFX)
*Fat Mike (vocalist and bass guitarist for NOFX; bass guitarist for Me First and the Gimme Gimmes and founder of Fat Wreck Chords)
*Chris Morris (senior writer for Billboard (magazine)|Billboard; music editor for The Hollywood Reporter) Black Flag and Circle Jerks)
*Brendan Mullen (operator of The Masque nightclub) Jeff Nelson (drummer for Minor Threat)
*Mike Ness (guitarist and vocalist for Social Distortion)
*Steve E. Nix (guitarist and vocalist for The Briefs) Derek OBrien (drummer for Social Distortion and D.I. (band)|D.I.
*Kelly Osbourne (television personality; singer; actress)
*Alan Parker (film director; producer; writer)
*Kirsten Patches (vocalist for Naked Aggression)
*Paulene (bass guitarist for The Diffs)
*D.H. Peligro (drummer for Dead Kennedys)
*Jimmy Pursey (vocalist for Sham 69)
*Dave Quackenbush (vocalist for The Vandals)
*Johnny Ramone (guitarist for the Ramones
*Marky Ramone (drummer for the Ramones)
*Tommy Ramone (drummer for the Ramones)
*Monica Richards (vocalist for Madhouse (band)|Madhouse, Strange Boutique, and Faith and the Muse)
*Mike Roche (bass guitarist for T.S.O.L.)
*Kris Roe (guitarist for The Ataris) Black Flag and State of Alert)
*Lance Romance (bass guitarist for The Briefs)
*Kate Ross
*Jenny Russell (of Wasted and HITS)
*Scott Russo (vocalist for Unwritten Law)
*Justin Sane (guitarist and vocalist for Anti-Flag)
*Maria Scarlett (culture and incentive co-ordinator for Hot Topic)
*Zander Schloss (bass guitarist for the Circle Jerks; actor)
*Rob Schwartz (executive creative director for TBWA\Chiat\Day) The Damned)
*Stormy Shepherd (founder of Leave Home Booking)
*Matt Skiba (guitarist and vocalist for Alkaline Trio and Heavens (band)|Heavens)
*Paul Slack (bass guitarist for the U.K. Subs)
*Richie Slick (guitarist and vocalist for The Diffs)
*Jennie Smith (co-founder of the Wasted Festival)
*TV Smith (vocalist for The Adverts)
*Kurt Soto (entertainment marketing manager for Vans (band)|Vans)
*Matthew Southwell (of Bang Sugar Bang)
*Franz Stahl (guitarist for Scream (band)|Scream)
*Peter Stahl (vocalist for Scream (band)|Scream)
*Branden Steineckert (drummer for Rancid (band)|Rancid) Youth Brigade) Youth Brigade; co-founder of BYO Records) Youth Brigade; co-founder of BYO Records) Stryker (disc jockey on KROQ-FM)
*Torch (of Drunk Tank)
*Bruce Treasure (guitarist for the Subhumans (UK band)|Subhumans)
*Trotsky (drummer for the Subhumans (UK band)|Subhumans)
*Steve Van Doren (vice-president of marketing and son of co-founder of Vans) The Damned)
*Keith "Monkey" Warren (vocalist for The Adicts)
*Kevin "Noodles" Wasserman (guitarist for The Offspring)
*Jon Watson (bass guitarist for 999)
*Gerard Way (vocalist for My Chemical Romance) The Business)
*Deryck Whibley (guitarist and vocalist for Sum 41) Emily Grace "Agent M" Whitehurst (vocalist for Tsunami Bomb)
*Wade Youman (drummer for Unwritten Law)
*Annie Zaleski (music journalist)
*Tim Taylor (Bassist for U.S. Chaos and The Undead)
*Brian Daley (Guitarist and vocalist for U.S. Chaos)

== References ==
 

== External links ==
* 
* 
*  
*  

=== Reviews ===
* 
* 
* 
* 

 
 
 
 
 
 
 