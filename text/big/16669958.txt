This Is Not a Test (1962 film)
{{multiple issues|
 
 
}}

{{Infobox Film

 | name = This Is Not a Test
 | caption = 
 | director = Fredric Gadette   
 | producer = Murray De Atley Fredric Gadette James Grandin (executive) Arthur Schmoyer (executive)
 | writer = Fredric Gadette Peter Abenheim Betty Lasky Mike Green  Alan Austin  Carol Kent  Norman Winston Ron Starr  Don Spruance 
 | music = Greig McRitchie
 | cinematography = Brick Marquard
 | editing = Hal Dennis Allied Artists
 | released = 1962
 | runtime = 73 minutes
 | country = United States
 | language = English
 | budget = 
 | preceded_by = 
 | followed_by = 
 | image = This Is Not a Test VideoCover.jpeg
}}
 nuclear war.

==Plot==

Starring a group of mostly unknown actors, This Is Not a Test begins with lone deputy sheriff Dan Colter (Seamon Glass)    receiving orders to block a road leading into an unidentified city (dialogue indicates the location is somewhere in central California, however).  Soon, he has detained several vehicles with a variety of occupants ranging from an elderly man and his granddaughter, to a man who has recently become rich and his alcoholic wife, to a trucker and a hitchhiker. The motorists and the police officer hear attack warnings over the police radio and begin to prepare for the inevitable bombing. The film focuses on the reactions to the impending attack by the motorists, and the officers efforts to keep order.  Complicating matters is the revelation that the hitchhiker Clint Delany (Ron Starr) is a psychotic who is wanted for murder.  As the countdown to the missile attack continues, the men and women try desperately to convert a supply truck into an impromptu bomb shelter.  As time goes by, the deputys behavior becomes irrational (Gary Westfahl mentioned that the film shows the ineptness that would come from ordinary people in the face of impending nuclear attack)  and the film ends with the deputy trying to enter the closed-up truck where the others have sheltered just as the nuclear strike happens. 

==DVD==

This Is Not a Test is presently in the public domain in the United States, and has been released in numerous DVD formats, on its own or in collections of similar films.

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 


 