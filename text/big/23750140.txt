The Passenger – Welcome to Germany
 
{{Infobox Film
| name           = The Passenger - Welcome to Germany
| image          = 
| image size     = 
| caption        = 
| director       = Thomas Brasch
| producer       = George Reinhart Joachim von Vietinghoff
| writer         = Thomas Brasch Jurek Becker
| narrator       = 
| starring       = Tony Curtis
| music          = 
| cinematography = Axel Block
| editing        = Tanja Schmidbauer
| distributor    = 
| released       = 5 May 1988
| runtime        = 102 minutes
| country        = Germany
| language       = German
| budget         = 
| preceded by    = 
| followed by    = 
}}

The Passenger - Welcome to Germany ( ) is a 1988 German drama film directed by Thomas Brasch. It was entered into the 1988 Cannes Film Festival.   

==Cast==
* Tony Curtis - Mr. Cornfield
* Katharina Thalbach - Sofie
* Matthias Habich - Körner
* Karin Baal - Frau Tenzer
* Charles Régnier - Silbermann
* Alexandra Stewart - Mrs.
* George Tabori - Rabbiner Michael Morris - Donelly
* Ursula Andermatt - Rosa
* Guntbert Warns - Danner
* Fritz Marquardt - Herr Tenzer
* Birol Ünel - Baruch
* Gedeon Burkhard - Janko
* Nina Lorck-Schierning - Bauernmädchen

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 