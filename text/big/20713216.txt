June Moon
 
{{Infobox play
| name       = June Moon
| image      = 
| image_size = 
| caption    = 
| writer     = George S. Kaufman Ring Lardner 
| chorus     = 
| characters = 
| mute       = 
| setting    = New York City, 1929
| premiere   =  
| place      = Broadhurst Theatre New York City
| orig_lang  = English
| series     = 
| subject    = A young man from upstate New York seeks fame as a Tin Pan Alley lyricist
| genre      = Comedy
| web        = 
}}

June Moon is a play by  .

At its center is Fred Stevens, a young aspiring lyricist who journeys from   sister-in-law Eileen. The two men sell a song to a music publisher and it develops into a hit. Ultimately, revelations about Eileens true character help return Fred to his senses and Edna, who he realizes he truly loves.

==Productions== Broadway production Norman Foster Lee Patrick as Eileen.

There was a production of June Moon prior to the Broadway production with the same cast, that opened at the Shubert-Belasco Theatre in Washington, D.C. on September 23, 1929.
  Ambassador Theatre on May 15, 1933 and ran for 49 performances. The cast included Thomas Gillen as Fred, Emily Lowry as Edna, Fred Irving Lewis as Paul, and Lee Patrick reprising the role of Eileen.

On March 24, 1940, Orson Welles and the Campbell Playhouse presented an hour-long adaptation of "June Moon". Cast: Orson Welles (Candy Butcher on train), Jack Benny (Fred Ste
vens), Benny Rubin (Maxie Schwartz), Gus Schilling (Paul Sears), Bea Benaderet (Lucille Sears), Lee Patrick (Eileen), Virginia Gordon (Edna Baker). 

Mark Nelson directed an off-Broadway revival performed by the Drama Dept. that had a short run at the Ohio Theater in Soho in January 1997. Writing in the New York Times, Ben Brantley observed, "There is indeed a discernible streak of anxiety in the comedy. It may have been written as fluff, but its flavored with a subliminal sadness that belies its own happy ending."   The production won the Lucille Lortel Award for Outstanding Revival and was nominated for the Drama Desk Award for Outstanding Revival of a Play Actor Albert Macklin, who portrayed sardonic accompanyist Maxie received a Best Actor Obie Award. On January 15, 1998, it reopened at the Variety Arts Theatre and ran for 101 performances. The cast included Geoffrey Nauffts as Fred, Robert Joy as Paul, and Cynthia Nixon as Eileen. Albert Macklin reprised his Obie-winning role as Maxie.

A new production is being performed at UC San Diego from February 24th, 2012 through March 3rd, 2012, directed by Jonathan SIlverstein.

==Film adaptations==
Vincent Lawrence, Joseph L. Mankiewicz, and Keene Thompson adapted the play for a 1931 feature film directed by A. Edward Sutherland. The cast included Jack Oakie as Fred, Frances Dee as Edna, Ernest Wood as Paul, and June MacCloy as Eileen.

Paramount Pictures produced a 1937 version called Blonde Trouble.
 Studio One.  The live television production was directed by Walter Hart, adapted by Gerald Goode, and featured Jack Lemmon as Fred Stevens, Eva Marie Saint as Edna, Edward Andrews as Paul, and Jean Carson as Eileen. 
 Kevin McCarthy, Lee Meredith, Estelle Parsons, Austin Pendleton, and Stephen Sondheim in supporting roles. A DVD of this production was released by Kultur Video on April 16, 2002.

==References==
 
*  
==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 