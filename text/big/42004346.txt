Swelter (film)
{{Infobox film
| name           = Swelter
| image          = Swelter_movies_poster.jpg
| caption        = Official film poster
| director       = Keith Parmer
| producer       = {{plainlist|
* Eleonore Dailly
* Chris Ranta
}}
| writer         = Keith Parmer
| starring       = {{plainlist|
* Jean-Claude Van Damme
* Josh Henderson
* Brad Carter
* Alfred Molina
* Lennie James
}}
| music          = Tree Adams
| cinematography = Michael Mayers
| editing        = 
| studio         = Grand Peaks Entertainment
| distributor    = afm
| released       =  
| runtime        = 96 minutes
| country        = United States
| language       = English
| budget         = $8 million  
| gross          = 
}}
Swelter is a 2014 American action film written and directed by Keith Parmer.  It stars Lennie James, Grant Bowler, Jean-Claude Van Damme, Josh Henderson, Daniele Favilli, and Alfred Molina.  James plays a sheriff in a small town who has a dark past that he can not remember, only to have to confront it when his ex-partners show up looking for stolen money they believe he hid.

== Plot ==
Five masked robbers steal $10 million from a Las Vegas casino.  All but one, who is shot in the head and assumed dead, are captured, but the money goes missing.  Ten years later, Cole, the leader, is broken out of jail by the rest of his gang: Stillman, Boyd, and Coles half-brother Kane.  From a boastful mechanic, they learn that their former partner apparently survived and escaped with the money due to the intervention of a local physician.  They track the physician to Baker, a small, isolated town with many secrets.  They find Doc, the physician, at a local bar and, from him and other patrons, learn that the sheriff mysteriously appeared ten years ago.  Curious, they start a bar fight in order to draw out the sheriff, who they realize is Pike, the partner who escaped.  Pike now calls himself Bishop and claims to suffer from amnesia and migraines from the bullet fragments lodged in his head.  Cole saves Bishops life during the bar fight, and Bishop runs a belligerent biker gang out of town.

Later, Boyd and Kane suggest that they raze the town in search of the loot.  Stillman objects, and Kane suggests that Stillman has gone soft.  Cole is able to smooth things over and decides to instead probe Bishop to see how much he remembers.  The two men discuss the town, and Cole drops a few hints about Bishops past.  Cole asks Bishop to call a coin toss, but Bishop declines and says that it is meaningless, as a man will do what he wants regardless of the result.  Cole, who was holding a gun on Bishop under the table, holsters his weapon and does not interfere when Bishop leaves.  Meanwhile, Bishop experiences trouble with his step-daughter, London, whose mother, Carmen, has a past with Cole.  Unknown to Bishop, Cole and Carmen were once lovers, and she moved to Baker to escape her previous life.  Cole attempts to rekindle their romance, but she refuses.

Cole fatally injures Doc while researching Bishop, and Bishop learns more about his past from Docs notes.  As he dies, Doc explains that he was the one who treated Bishop.  Spurred on by the information in Docs notes, Bishop begins to remember bits of his past, though he still does not know where the money is.  Boyd and Kane become restless and start trouble in the town.  After a fight with her boyfriend, London makes out with Kane.  When she refuses to have sex, Kane rapes her.  Boyd engages in a draw with the towns deputy, an award-winning sharpshooter, and wins, only to be shot down by Bishop, who is faster.  Stillman, outraged that Kane would rape a teenage girl, confronts and is killed by Kane.  Bishop and Cole meet at the towns church, and Cole reveals that he has recruited the biker gang to replace his fallen men.  Cole reveals Bishops criminal background to the populace and gives them until sunrise to find the missing money.

At the local diner, Cole takes London hostage in order to ensure Bishops cooperation.  Disarmed and without the support of the townspeople, Bishop is close to giving up when Carmen reveals that she knew about Bishops past the whole time and still accepted him.  She recovers a hidden pistol and gives it to Bishop, who then goes to the diner to confront Cole.  Kane uses London as human shield, to the disgust of all the others.  The biker gang leaves in protest, and Cole shoots Kane dead himself, to the surprise of Bishop.  As Cole leaves the diner, Bishop stops him and says that they still must settle their issues.  The two have a duel, and Bishop kills Cole.  Concerned that his criminal background has now become commonly known, Bishop prepares to go on the run.  However, the townspeople rally behind him and offer to cover up the recent events.  Bishop stays on the towns sheriff, and an aerial shot reveals the spot where the money is hidden.

== Cast ==
* Lennie James as Bishop
* Grant Bowler as Cole
* Jean-Claude Van Damme as Stillman
* Josh Henderson as Boyd
* Daniele Favilli as Kane, Coles half-brother
* Alfred Molina as Doc, the towns physician
* Catalina Sandino as Carmen
* Freya Tingley as London, Carmens daughter
 Guy Wilson, Dawn Lewis, and Tracey Walter appear as townspeople.

== Production ==
Exchange Peaks Film Capital financed the production.   Filming began in April 2013. 

== Release ==
Well Go USA purchased the North American rights at the American Film Market  and released Swelter on DVD in the United States on August 12, 2014. 

== Reception ==
David Johnson of DVD Verdict called it "a forgettable piece of gangster fare". 
 Guardians of the Galaxy. 

== References ==
 

== External links ==
*  

 
 
 
 
 