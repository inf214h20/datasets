The Cutman
{{Infobox Film
| name           = The Cutman
| image          = Poster of the movie The Cutman.jpg
| caption        = 
| director       = Yon Motskin
| producer       = 
| writer         = Yon Motskin
| starring       = 
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       = 
| runtime        =  USA
| English
| budget         = 
| preceded_by    = 
| followed_by    = 
}}

The Cutman  was written and directed by Yon Motskin. It tells the tale of a boxing cutman at the end of his career, losing his edge, and struggling to repair his relationship with his estranged son.

2003 Sundance Film Festival Official Selection. After its debut at Sundance, the film also screened at the LA International Short Film Festival, and several others including Santa Barbara, Philadelphia, South by Southwest, and Tribeca. The film took first place at the First Run Festival in New York 2003, where Motskin also won the Wasserman Award for outstanding directing.

== External links ==
*  

 
 
 
 


 