Sin dejar rastros
{{Infobox film| name = Sin dejar rastros
| image        =
| caption      =
| director     = Quirino Cristiani
| producer     =
| writer       = Quirino Cristiani
| starring     =
| distributor  =
| released     = 1918
| country      = Argentina
| runtime      = Spanish (Castellano) intertitles
| budget       =
| music        =
}}
Sin dejar rastros (Spanish: "Without a Trace") was an 1918 Argentine animated feature film. It was written and directed by Quirino Cristiani. The film used cutout animation. 

==Plot== Entente for the act. Reports from survivors helped everyone to realize what had truly happened.

==Reception==
The film was not as successful as Cristianis previous film, El Apóstol from 1917, since Sin dejar rastros was confiscated by the Ministry of Foreign Affairs by order of President Hipólito Yrigoyen. It is unknown if any copies of the film exist, and is considered a lost film.

==See also==
*List of animated feature-length films

==External links==
* 
* 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 