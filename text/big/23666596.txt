Il mostro di Venezia
{{Infobox film
| name           =The Embalmer
| image          = Imdvpos.jpg 
| image_size     =
| caption        = Original film poster
| director       =Dino Tavella
| producer       =
| writer         = Dino Tavella (screenplay), Antonio Walter (story)
| narrator       =
| starring       =
| music          = Marcello Gigante 	
| cinematography = Mario Parapetti 
| editor       = Gian Battista Mussetto
| distributor    =
| released       = 1965
| runtime        = 83 minutes
| country        = Italy Italian
| budget         =
}}
 1965 Italy|Italian horror film directed by Dino Tavella that was filmed in Trieste and Venice.  In the US the film was released as a double feature with The She Beast. 

==Plot== travelogue scenes of Venice, a killer who dresses as the Phantom of the Opera and an Italian Elvis Presley impersonator.

==Cast==
*Maureen Brown
*Luigi Martocci (credited in the American print as "Gin Mart")
*Luciano Gasper
*Anita Todesco
*Francesco Bagarin
*Alba Brotto
*William Caruso
*Viki Castillo
*Roberto Contero
*Gaetano DellEra
*Alcide Gazzotto
*Antonio Grossi
*Jack Judd
*Carlo Russo
*Paola Vaccari
*Maria Rosa Vizzina
*Pietro Walter

==Biography== 
* 

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 




 
 