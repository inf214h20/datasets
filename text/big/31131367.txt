Somewhere in Politics
Somewhere in Politics is a 1949 British comedy film directed by John E. Blakeley and starring Frank Randle, Tessie OShea and Josef Locke.  It was the fifth film in the Somewhere series of films featuring Randle followed by Its a Grand Life (1953).

According to the British Film Institute (BFI), only a print of an "18-minute short from the film, entitled Full House", is known to exist. 

==Cast==
* Frank Randle - Joe Smart
* Tessie OShea - Daisy Smart
* Josef Locke - Cllr. Willoughby
* Sally Barnes - Marjorie Willoughby
* Syd Harrison - Tony Parker
* Max Harrison - Arthur Parker
* Bunty Meadows - Martha Parker
* Jimmy Clitheroe - Sonny
* Sonny Burke - Reggie Smart
* Anthony Oakley - Howard
* Bernard Youens - Bank Manager
* Effi McIntosh - Mrs. Jones
* Kay Compston - Lady Hazelmere
* Fred Simister - Detective Sergeant
* George Little - Mayor

==References==
 

==Bibliography==
* Richards, Jeffrey. Films and British national identity: from Dickens to Dads army. Manchester University Press, 1997.

==External links==
* 
* , with extensive notes

 
 
 
 
 
 


 
 