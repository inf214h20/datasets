Il Postino: The Postman
 
 
{{Infobox film
| name           = Il Postino: The Postman
| image          = Il Postino poster.jpg
| caption        = Theatrical release poster
| starring       = Philippe Noiret Massimo Troisi Maria Grazia Cucinotta
| director       = Michael Radford
| screenplay     = Anna Pavignano Michael Radford Furio Scarpelli Giacomo Scarpelli Massimo Troisi
| based on       =  
| producer       = Mario Cecchi Gori Vittorio Cecchi Gori Gaetano Daniele
| distributor    = Miramax Films
| music          = Luis Enríquez Bacalov
| cinematography = Franco Di Giacomo
| editing        = Roberto Perpignani
| released       =  
| country        = Italy France
| runtime        = 108 minutes
| language       = Italian Spanish
| budget         = $3 million 
| gross          = $21,848,932 
}} film of the same name, the film has gone by the title Il Postino: The Postman.

The film tells a fictional story in which the real life Chilean poet Pablo Neruda forms a relationship with a simple postman who learns to love poetry. It stars Philippe Noiret, Massimo Troisi, and Maria Grazia Cucinotta. The screenplay was adapted by Anna Pavignano, Michael Radford, Furio Scarpelli, Giacomo Scarpelli, and Massimo Troisi from the novel Ardiente paciencia by Antonio Skármeta. In 1983, Skármeta himself wrote and directed the film "Ardiente paciencia" (English translation: "Burning Patience"), which he later adapted to the novel of the same name in 1985.

Writer/star Massimo Troisi postponed heart surgery so that he could complete the film. The day after filming was completed, he suffered a fatal heart attack. 

== Plot ==
Set in the year 1950, Pablo Neruda, the famous Chilean poet, is exiled to a small island in Italy for political reasons. His wife accompanies him. On the island, local Mario Ruoppolo is dissatisfied with being a fisherman like his father. Mario looks for other work and is hired as a temporary postman with Neruda as his only customer. He uses his bicycle to hand deliver Nerudas mail (the island has no cars). Though poorly educated, the postman eventually befriends Neruda and becomes further influenced by Nerudas political views and poetry.

Meanwhile, Mario falls in love with a beautiful young lady, Beatrice Russo, who works in her aunts village cafe. He is shy with her, but he enlists Nerudas help. Mario constantly asks Neruda if particular metaphors he uses are suitable for his poems. Mario is able to better communicate with Beatrice and express his love through poetry. Despite the aunts strong disapproval of Mario, because of his sensual poetry (which turns out to be largely stolen from Neruda), Beatrice responds favourably.

The two are married. The priest refuses to allow Mario to have Neruda as his best man, due to politics; however, this is soon resolved. This was because Di Cosimo was the politician in office in the area with the Christian Democrats. At the wedding, Neruda receives the welcome news that there is no longer a Chilean warrant for his arrest, so he returns to Chile.

Mario writes a letter but never gets any reply. Several months later he receives a letter from Neruda. However to his dismay it is actually from his secretary, asking Mario to send Nerudas old belongings back to Chile. While there Mario comes upon an old phonograph and listens to the song he first heard when he met Neruda. Moved by this he makes recordings of all the beautiful sounds on the island onto a cassette including the heartbeat of his soon-to-be-born child.

Several years later, Neruda finds Beatrice and her son, Pablito (named in honour of Neruda) in the same old inn. From her, he discovers that Mario had been killed before their son was born. Mario had been scheduled to recite a poem he had composed at a large communist gathering in Naples; the demonstration was violently broken up by the police. She gives Neruda recordings of village sounds that Mario had made for him.

== Cast ==
* Philippe Noiret – Pablo Neruda
* Massimo Troisi – Mario Ruoppolo
* Maria Grazia Cucinotta – Beatrice Russo
* Renato Scarpa – Telegrapher
* Linda Moretti – Donna Rosa
* Mariano Rigillo – Di Cosimo
* Anna Bonaiuto – Matilde
* Simona Caparrini – Elsa Morante

== Setting == Aeolian Island chain off the north coast of Sicily.

== Soundtrack ==
 

In 1994 to promote the film,  , includes Nerudas poems recited by many celebrities. There are a total of 31 tracks.
 CAM Original Soundtracks released a 17 track version of the score (CAM 509536-2) which was mastered in Dolby Surround.
 Academy Award for Best Original Dramatic Score and the BAFTA Award for Best Film Music.

For the 2010 opera based on the film see Daniel Catan.

== Reception ==
The film was very well received. Rotten Tomatoes reports that 93% of the critics liked the film, based on 26 reviews.  It received a score of 81 on Metacritic, indicating "Universal Acclaim", based on 13 critic reviews. 

== Awards ==

=== Academy Awards ===
At the 68th Academy Awards (1995), Il Postino: The Postman received five nominations and one Academy Award.
 Luis Enríquez Academy Award for Best Music (Original Dramatic Score). Best Picture; Best Director Best Actor Best Writing (Screenplay Based on Material Previously Produced or Published).
 posthumous nominations.

=== BAFTA Awards === BAFTA Award for Best Film Not in the English Language. Luis Enríquez Bacalov, won the BAFTA Award for Best Film Music.

==See also==
*Biographical film

== References ==
 

== External links ==
 
*  
*  
*  

 
 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 