Quatermass and the Pit (film)
 
 
 
{{Infobox film
| name           = Quatermass and the Pit
| image          = Quatermass and the Pit (1967 film) poster.jpg
| border         = yes
| caption        = Film poster by Tom Chantrell
| director       = Roy Ward Baker
| producer       = Anthony Nelson Keys
| writer         = Nigel Kneale
| starring       = James Donald Andrew Keir Barbara Shelley Julian Glover
| music          = Tristram Cary Arthur Grant
| editing        = Spencer Reeve
| distributor    = Associated British Pathé|Ass. British Pathé (UK) 20th Century Fox (USA)
| released       =  
| runtime        = 97 minutes
| country        = United Kingdom
| language       = English
| budget         = £275,000 
| gross          =
}}

Quatermass and the Pit (US title: Five Million Years to Earth) is a 1967 British science fiction horror film. Made by Hammer Film Productions it is a sequel to the earlier Hammer films The Quatermass Xperiment and Quatermass 2. Like its predecessors it is based on a BBC Television serial – Quatermass and the Pit – written by Nigel Kneale. It was directed by Roy Ward Baker and stars Andrew Keir in the title role as Professor Bernard Quatermass, replacing Brian Donlevy who played the role in the two earlier films. James Donald, Barbara Shelley and Julian Glover appear in co-starring roles.
 Martian spacecraft, crane – is swung into the centre of the force and the energy is ground (electricity)|discharged.
 A Night MGM studios Associated British Studios, also in Elstree.
 ITV network; in a re-edited form it received a limited cinema release under the title The Quatermass Conclusion.

==Plot== Palaeontologist Dr apemen over unexploded bomb, they call in an army bomb disposal team.

Meanwhile, Professor Bernard Quatermass (Andrew Keir) is dismayed to learn that his plans for the colonisation of the Moon are to be taken over by the military. He gives a cold reception to Colonel Breen (Julian Glover) who has been assigned to join Quatermass British Experimental Rocket Group. When the bomb disposal team call for Breens assistance, Quatermass accompanies him to the site. Breen concludes it is a V-weapons|V-weapon, but Quatermass disagrees. When another skeleton is found in an inner chamber, Quatermass and Roney realise that the object must also be five million years old. Quatermass suspects it is of alien origin, but Roney is certain the apemen are terrestrial.

Quatermass becomes intrigued by the name of the area, recalling that "hob" is an old name for the Devil. Working with Roneys assistant, Barbara Judd (Barbara Shelley), Quatermass finds historical accounts of hauntings and other spectral appearances going back over many centuries. They deduce that these events coincided with any disturbances of the ground around Hobbs End.

An attempt to open a sealed chamber using a  . Quatermass and Roney note the similarity between the appearance of the creatures and the Devil.
 telekinetic force emanating from the missile and flees to the sanctuary of a church. Sladden tells Quatermass he saw a vision of hordes of the creatures from the missile. Quatermass believes this is a race memory. Seeking proof, he returns to Hobbs End, bringing a machine Roney has been working on which taps into the primeval psyche (psychology)|psyche. While trying to replicate the circumstances under which Sladden was affected, he notices that Judd has fallen under its influence. Using Roneys machine, he is able to record her thoughts.

Quatermass presents his theory to a government minister (  exercise designed to sow fear of an alien invasion among the populace. The minister rejects Quatermass theory in favour of Breens and decides to unveil the missile to the press.
 discharged into crane and swings it into the spectre. The crane bursts into flames as it discharges the energy, killing Roney. With this, the image disappears.
As Quatermass and Judd sit, dazed, in the rubble of Hobbs End, Fire Engine and Police bells can be heard ringing in the distance.

==Production==

===Origins===
  Seven Arts, ABPC and Twentieth Century Fox, and Quatermass and the Pit finally entered production. 

===Writing=== John Trevelyan British Board of Film Censors in December 1966. Kinsey, p. 20  Trevelyan replied that the film would require an X rating|X-Certificate and raised concerns regarding the sound of the vibrations from the alien ship, the scenes of the Martian massacre, the scenes of destruction and panic as the Martian influence takes hold and the image of the Devil. 

===Casting===
  Lust for The Great King Rat (1965).  Although not playing the title role, Donald was accorded billing (filmmaking)|top-billing status. Hearn, p. 11. 

*   who had played Quatermass in the television version of Quatermass and the Pit. Hearn & Barnes, p. 117.  However, Morell was not interested in revisiting a role he had already played.  The producers eventually settled on Scottish actor Andrew Keir who had appeared in supporting roles in a number of Hammer productions including   (1966).  Keir found the shoot an unhappy experience: he later recalled, “The director – Roy Ward Baker – didnt want me for the role. He wanted Kenneth More... and it was a very unhappy shoot.   Normally I enjoy going to work every day. But for seven and a half weeks it was sheer hell.” Mayer, p. 40.  Roy Ward Baker denied he had wanted Kenneth More, who he felt would be "too nice" for the role, Kneale & Baker, DVD Commentary  saying, “I had no idea he   was unhappy while we were shooting. His performance was absolutely right in every detail and I was presenting him as the star of the picture. Perhaps I should have interfered more.” Baker, p. 125.  Keir went on to appear for Hammer in The Viking Queen (1967) and Blood from the Mummys Tomb (1971).  He reprised the role of Quatermass for BBC Radio 3 in The Quatermass Memoirs (1996), making him the only actor other than Donlevy to play the role more than once. 

*   (1958),   (1966) for them.  Quatermass and the Pit was her last film for the company and she subsequently worked in television and the theatre.  Roy Ward Baker was particularly taken with his leading lady, telling Bizarre Magazine in 1974 he was “mad about her in the sense of love. We used to waltz about the set together, a great love affair.” 

*   (1961–69). Baker said of Glovers performance, “He turned in a tremendous character, forceful, autocratic but never over the top.”  Glover recalled of the role, “I think I was too young for it.   I think I played it all right. It was very straightforward. Bit of a stereotype.   The obligatory asshole!” Kinsey, p. 22. 
 Grant Taylor, Robert Morris. Kinsey, p. 19.  Duncan Lamont, playing Sladden, had appeared in the original BBC production of The Quatermass Experiment in the key role of the hapless astronaut Victor Carroon. Hearn, p. 13.  Quatermass and the Pit also features an early film role for Sheila Steafel who makes a brief appearance as a journalist near the start of the movie. 

===Filming=== Casino Royale The One A Night Two Left The Saint (1962–69) and The Avengers.  Producer Anthony Nelson Keys chose Baker as director because he felt his experience on such films as A Night to Remember gave him the technical expertise to handle the films significant special effects requirements.  Baker, for his part, felt that his background on fact-based dramas such as A Night to Remember and The One That Got Away enabled him to give Quatermass and the Pit the air of realism it needed to be convincing to audiences.  He was impressed by Nigel Kneales screenplay, feeling the script was "taut, exciting and an intriguing story with excellent narrative drive. It needed no work at all. All one had to do was cast it and shoot it." Baker, p. 124.  He was also impressed with Hammer Films’ lean set-up: having been used to working for major studios with thousands of full-time employees, he was surprised to find that Hammers core operation consisted of just five people and enjoyed how this made the decision making process fast and simple.  Quatermass and the Pit was the first film the director was credited as “Roy Ward Baker”, having previously been credited as “Roy Baker”. The change was made to avoid confusion with another Roy Baker who was a sound editor.  Baker later regretted making the change as many people assumed he was a new director. 
 Associated British MGM Borehamwood Bernard Robinson The Witches dressing of his set for the Hobbs End station. Kinsey, p. 24.  Another Hammer regular was special effects supervisor Les Bowie. Roy Ward Baker recalled he had a row with Bowie, who believed the film was entirely a special effects picture, when he tried to run the first pre-production conference.  Bowies contribution to the film included the Martian massacre scene, which was achieved with a mixture of puppets and live locusts, and model sequences of Londons destruction, including the climatic scene of the crane swinging into the Martian apparition. Kinsey, p. 27. 

===Music=== Second World War.  He became a professional composer in 1954, working in film, theatre, radio and television,  with credits including The Ladykillers (1955).  He said of his assignment, “I was not mad about doing the film because Hammer wanted masses of electronic material and a great deal of orchestral music. But I had three kids, all of which were at fee-paying schools, so I needed every penny I could get!”. Martell, p. 15.  Cary also recalled that, “The main use of electronics in Quatermass, I think, was the violent shaking, vibrating sound that the "thing in the tunnel" gave off   It was not a terribly challenging sound to do, though I never played it very loud because I didnt want to destroy my speakers – I did have hopes of destroying a few cinema loudspeaker systems, though it never happened”.  Carey went on to write the score for another Hammer film, Blood from the Mummys Tomb, in 1971.  Several orchestral and electronic cues from the film were released by GDI Records on a compilation titled The Quatermass Film Music Collection. 

==Reception== double bill Evening News who described the film as "entertaining hokum" with an "imaginative ending".  A slightly more critical view was espoused by Penelope Mortimer in The Observer who said, “This nonsense makes quite a good film, well put together, competently photographed, on the whole sturdily performed. What it totally lacks is imagination.” 

==Legacy== ITV television The Anniversary The Vault of Horror (1973) for Hammers rival, Amicus Productions. 
 John Baxter Bill Warren in Keep Watching the Skies! said, “The ambition of the storyline is contained in a well-constructed mystery that unfolds carefully and clearly”.  Nigel Kneale had mixed feelings about the end result: he said, “I was very happy with Andrew Keir, who they eventually chose, and very happy with the film. There are, however, a few things that bother me... The special effects in Hammer films were always diabolical.” 

==DVD release== region 1 Anchor Bay commentary from Nigel Kneale and Roy Ward Baker as well as trailers and an instalment of a documentary called The Worlds of Hammer devoted to Hammers forays into science fiction. 

==References==

===Notes===
 

===Bibliography===
 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
 

==Further reading==
*  

==External links==
*  
*  
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 