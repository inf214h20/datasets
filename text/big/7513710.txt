Fanatic (film)
 
 
{{Infobox film
| name           = Fanatic
| image          = Fanatic 1965 poster.jpg
| image_size     =
| caption        = British original poster
| director       = Silvio Narizzano
| producer       = Anthony Hinds
| writer         = Richard Matheson
| based on       =  
| narrator       =
| starring       = Tallulah Bankhead Stefanie Powers Donald Sutherland
| music          = Wilfred Josephs
| cinematography = Arthur Ibbetson
| editing        = John Dunsford
| studio         = Hammer Film Productions
| distributor    = Columbia Pictures
| released       = 1965-03-21
| runtime        = 97 minutes
| country        = United Kingdom English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
}}
 British Thriller thriller directed Hammer Films. It stars Tallulah Bankhead, Stefanie Powers, Peter Vaughan, Yootha Joyce, Maurice Kaufmann and Donald Sutherland.
 location in Letchmore Heath, Hertfordshire, during the summer of 1964. It was Bankheads final feature film.

==Plot details== American woman, married to fanatically religion|religious, and it soon becomes apparent she blames Patricia for her sons death. Patricia reveals to her that she was not actually going to marry Stephen, who, it turns out, committed suicide. With the help of her two married servants Harry (Vaughn) and Anna (Joyce), Mrs Trefoile holds Patricia captive in an attempt to cleanse her soul. After Mrs. Trefoile kills Harry, she then tries to kill Patricia. Patricia is rescued by Alan. Anna finds Harrys body, and finally Mrs. Trefoile falls dead with a knife in her back - presumably put there by Anna.

==Cast==
*Tallulah Bankhead as Mrs. Trefoile
*Stefanie Powers as Patricia Carroll
*Peter Vaughan as Harry
*Maurice Kaufmann as Alan Glentower
*Yootha Joyce as Anna
*Donald Sutherland as Joseph
*Gwendolyn Watts as Gloria
*Robert Dorning as Ormsby
*Philip Gilbert as Oscar
*Winifred Dennis as Shopkeeper Diana King as Shopper
*Henry McGee as Priest

==See also==
* Looped, a 2010 Broadway play that uses the production of this film as its setting
* Psycho-biddy|Psycho-biddy genre

==External links==
* 
*  
* 

 
 

 
 
 
 
 
 
 
 


 
 