Miquette (1950 film)
{{Infobox film
| name =  Miquette 
| image =
| image_size =
| caption =
| director = Henri-Georges Clouzot
| producer = Raymond Borderie   Robert Dorfmann  
 | writer =   Robert de Flers  (play)   Gaston Arman de Cavaillet  (play)   Jean Ferry    Henri-Georges Clouzot
 | narrator =
| starring = Louis Jouvet   Bourvil   Saturnin Fabre   Danièle Delorme
| music = Albert Lasry    
| cinematography = Louis Née   Armand Thirard 
| editing =  Monique Kirsanoff     CICC
 | distributor = Les Films Corona
| released = 14 April 1950
| runtime = 95 minutes
| country = France French 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}}
Miquette (French:Miquette et sa mère) is a 1950 French comedy film directed by Henri-Georges Clouzot and starring Louis Jouvet, Bourvil and Saturnin Fabre. The film was an adaptation of the play Miquette et sa mere by Robert de Flers and Gaston Arman de Cavaillet, which had previously been adapted into 1934 (Miquette (1934 film)|Miquette and 1940 (Miquette (1940 film)|Miquette) films.

Clouzot was reluctant to make the film, but was contractually obliged to. It was not a commercial or critical success. 

== Partial cast ==
* Louis Jouvet as Monchablon 
* Bourvil as Urbain de la Tour-Mirande 
* Saturnin Fabre as Le marquis 
* Danièle Delorme as Miquette 
* Mireille Perrey as Madame veuve Hermine Grandier 
* Pauline Carton as Perrine 
* Jeanne Fusier-Gir as Mademoiselle Poche 
* Madeleine Suffel as Noémie 
* Maurice Schutz as Panouillard 
* Pierre Olaf as Le jeune premier 
* Paul Barge as Labbé

== References ==
 

== Bibliography ==
* Lloyd, Christopher. Henri-Georges Clouzot. Manchester University Press, 2007. 

== External links ==
*  

 

 

 
 
 
 
 
 
 
 
 