Among the Dust of Thieves
{{Infobox film
| name           = Among the Dust of Thieves
| image          = Dustofthieves.jpg
| caption        = Tim Maloney as Albert J. Fountain
| director       = Sean Pilcher
| producer       = Eric Seo Josh Mason
| writer         = Sean Pilcher
| starring       = Tim Maloney Chris Norden Bryan Head Algernon D’Ammassa
| music          = Daniel James Chan
| cinematography = Matt Wilson
| editing        = Sean Pilcher Matt Wilson
| studio         = Wilcher Productions Colonel Fountain Productions
| released       =  
| runtime        = 43 minutes
| country        = United States
| language       = English
}} Pinkerton Agency. Oliver M. Lee for cattle rustling. The narrative switches from Fountain’s investigation and arrest of Lee culminating in Fountain’s disappearance, to Fraser’s investigation, which concludes after a shootout at Lee’s ranch.

==Plot==
Mariana, a granddaughter of Albert Jennings Fountain, discovers her great grandson playing with her grandfather’s clothing and personal possessions. As she begins to tell him of Fountain’s history, the film flashes back to a New Mexico ranch in 1896, where Oliver Lee murders two ranchers and steals their cattle. Fountain, serving as Doña Ana County, New Mexico|Doña Ana County District Attorney, arrests a member of Lee’s gang in the case, and eventually apprehends Lee despite death threats. After Lee’s arraignment, Fountain is seen leaving town, never to be seen again. Following the disappearance, Pinkerton detective John C. Fraser arrives from Colorado to investigate and discovers Fountain’s wagon, disabled, where there is evidence of a struggle and blood. When suspicion leads to Oliver Lee, Fraser leads a small party to Lee’s ranch where they are ambushed by Lee and his gang. Fraser wounds Lee but ultimately retreats. The film ends with the case unresolved.

==Cast==
* Tim Maloney as Albert Jennings Fountain Oliver M. Lee
* Algernon D’Ammassa as John C. Fraser
* Bryan Head as Deputy Williams
* Tyler Robinson as Deputy Sheriff Kent Kearney
* Bruce Holbrook as Les Dow
* Kim Kiegel as Mariana
* Joe T. Meier as Jack Maxwell
* Miguel Martinez as Bill McNew
*Eric Young as Jim Gililland

==Release==
This independent film premiered on January 10, 2013, in Mesilla, New Mexico.     Its initial theatrical run was at the Allen Theaters Cineport 10 in Las Cruces, New Mexico     and it was released on DVD  by Colonel Fountain Productions, LLC, simultaneously with the film’s premiere.

After the initial release, the Albuquerque Journal reported that filmmakers Pilcher and Wilson "plan to show the movie at film festivals...to attract financing so they can expand it to a feature-length version, more than double the current length."   

==References==
 

==External links==
*  
* Official   released in 2012.
* Albert Fountain  .

 
 
 
 