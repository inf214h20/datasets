Grand Larceny
 

{{Infobox film
| name           = Grand Larceny
| image          = Grand Larceny 1987.jpg
| image_size     =
| caption        =
| director       = Jeannot Szwarc
| producer       = Patrick Deschamps Patrick Dromgoole Robert Halmi Jr. Robert Halmi Sr. Jacques Méthé
| writer         = Peter Stone
| narrator       =
| starring       = Marilu Henner Ian McShane Omar Sharif Louis Jourdan
| music          = Irwin Fisch
| cinematography = Tony Impey
| editing        = Lyndon Matthews
| studio         = France 3 Cinéma Harlech Television
| distributor    = Taurus Video
| released       = 1987
| runtime        =
| country        = UK USA France
| language       = English
| budget         =
| gross          =
| website        =
| amg_id         =
}}
Grand Larceny is a 1987 thriller film directed by Jeannot Szwarc and starring Marilu Henner, Ian McShane, Omar Sharif and Louis Jourdan. 

==Plot summary==

Freddy Grand, a woman estranged from her father for 18 years, travels to his estate on the French Riviera after learning of his death. There she carries out his final wishes and begins to learn something about his past. She discovers he was a thief who served time in prison, but was approached by an insurance company on his release to employ his talent in helping them to recover some stolen valuables. A reformed man of many years before his death, he wishes Freddy to continue his work.

==Cast==
* Marilu Henner - Freddy Grand
* Ian McShane - Flanagan
* Omar Sharif - Rashid Saud
* Louis Jourdan - Charles Grand
* Phillip Tan - Shomin

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 


 