Girl Mistress
{{Infobox film
| name = Girl Mistress
| image = Girl Mistress.jpg
| image_size =
| caption = Theatrical poster for Girl Mistress (1980)
| director = Banmei Takahashi 
| producer = 
| writer = Kei Ōnuki
| narrator = 
| starring = Cecile Gōda Satoshi Miyata Shirō Shimomoto
| music = Rōman Kikaku
| cinematography = Yūichi Nagata
| editing = 
| studio = Kokuei Takahashi Productions
| distributor = Shintōhō
| released = October 1980
| runtime = 60 minutes
| country = Japan
| language = Japanese
| budget = 
| gross = 
| preceded_by = 
| followed_by = 
}}
 1980 Japanese pink film directed by Banmei Takahashi.

==Synopsis==
An older yakuza man falls in love with Seru, a high school girl. When he is put in prison, Seru begins working as a prostitute to earn money for the gangsters parole. During her yakuza lovers incarceration, Seru gains a new, young boyfriend. The yakuza discovers the new boyfriend after he is released from prison. Realizing that she will have a better life with her new boyfriend who is not associated with the yakuza, he sacrifices his love for Seru and gives her up.   

==Cast==
* Cecile Gōda (豪田路世留) as Seru  
* Satoshi Miyata (宮田諭) as Serus younger boyfriend
* Shirō Shimomoto (下元史郎) as yakuza in love with Seru
* Naomi Oka (丘なおみ)
* Maria Satsuki (五月マリア)
* Ren Ōsugi (大杉漣)

==Background and critical appraisal==
Along with Mamoru Watanabe and Genji Nakamura, Banmei Takahashi was known as one of the "Three Pillars of Pink" before he made Girl Mistress. He was known for his stylistically unique approach to the genre which brought college students back to pink film theaters at this time, when Nikkatsus Roman Porno series was beginning to lose its popularity among this audience. 

In their Japanese Cinema Encyclopedia: The Sex Films, Thomas and Yuko Mihara Weisser give Girl Mistress three-and-a-half out of four stars. Without stating what award the film won, they note that it is an "award-winning motion picture". Director Takahashi had already made a name for himself in the pink film through his films at Kōji Wakamatsus production company, such as Raping the Sisters (1977) and Japanese Inquisition (1978), and films made at his own company, such as Attacking Girls and Scandal: Pleasure Trap (both 1979). However, according to the Weissers, Girl Mistress is the film which cemented his name in the history of pink cinema. 

==Availability==
Banmei Takahashi filmed Girl Mistress for his own Takahashi Productions and Kokuei and it was released theatrically in Japan by Shintōhō in October 1980.  Uplink released it on DVD as part of their Nippon Erotics series on June 28, 2002.  

==Bibliography==

===English===
*  
*  
*  

===Japanese===
*  
*  
*  

==Notes==
 

 

 
 
 
 
 


 
 