In Which Annie Gives It Those Ones
{{Infobox film
| name           = In Which Annie Gives It Those Ones
| image          = In Which Annie Gives Those Ones.jpg
| image size     =
| caption        =
| director       = Pradip Krishen
| producer       = Bobby Bedi (Kaleidoscope Entertainment))
| writer         = Arundhati Roy  Arjun Raina
| narrator       =
| starring       = Arundhati Roy, Roshan Seth, Shahrukh Khan
| music          =
| cinematography =Rajesh Joshi
| editing        =A. Thyagaraju 
| distributor    =
| released       =  
| runtime        = 112 Mins
| country        = India
| language       = English
| budget         =
}}
 National Awards in 1989.   iffi.nic.in. Directorate of Film Festival. Retrieved 17 November 2012  This film acquired a cult status in the years after it was made.   

Set in the 1970s, In Which Annie Gives it Those Ones is a funny film of architecture students in their final year of college.

The film was part autobiographical with Roy recounting her own experiences of studying in the School of Planning and Architecture, Delhi, a leading architecture institute in India.

== Plot ==

Anand Grover, better known as Annie, is victimized for making fun of his principal, Y.D. Billimoria (popularly known as Yamdoot or Hells messenger), years ago. At the National Institute of Architecture, New Delhi, Annie is repeating his fifth year for the fourth time. He spends his hours in the hostel which is the best part of his life, by giving it those ones &mdash; indulging in daydreams of social uplift. His latest idea is to plant fruit trees on either side of railway tracks, where rural India defecates daily. The fecal matter will provide the necessary compost for the trees, while the trains, with sprinklers attached, will automatically water the plants.

Annie keeps two hens in his room and earns a modest sum by selling the eggs, until one day his friend, Mankind, and his Ugandan roommate, Kasozi, make a roasted meal out of them. Soon, however, hirsute Arjun and his girlfriend Radha &mdash; a non-conformist student who steals cigarettes from Yamdoot and talks back to the teachers &mdash; present Annie with a rabbit.

Many adventures later, the day to submit the thesis draws near. Annie, urged by his friends, apologises to Yamdoot. A panel of judges call the students one by one for their final interviews and the tension mounts. Radha goes dressed in a saree but wears a mans hat to detract from her sober attire. To make sure that Annie gets a sympathetic hearing from the hostile panel, Radha and Arjun work out a plan. Just when Annie is called in, Yamdoot receives a phone call from his dominating deep-voiced mother, in actuality Mankind. The trick works and the weary panel gives Annie a good grade.

At the party after the graduation ceremony, Annie arrives with heavy books under his arm, his hair shaved off and a butterfly painted on his head. He informs his friends that he has decided to study law and then sue Yamdoot. But subsequently, Annie became an Associate Professor of Design at the National Institute of Architecture, a year after Yamdoots retirement.

== Credits ==
* Direction: Pradip Krishen
* Story & Screenplay: Arundhati Roy
* Audiography: Indrajit Neogi
* art Direction:Ravi Kaimal
== Cast ==
* Arjun Raina: Annie
* Arundhati Roy: Radha
* Rituraj: Arjun
* Roshan Seth: Y.D. Billimoria/Yamdoot Isaac Thomas: Mankind
* Divya Seth: Lakes
* Idries Malik: Papey
* Moses Uboh: Kasozi
* Jagan Shah: Medoo
* Himani Shivpuri: Bijli
* Shahrukh Khan:Senior
*Niraj Shah:Canteen boy
* Dhianee: Canteen boy

==Awards== 1988 National National Film Award
**        
**   

==References==
 

==External links==
* 

 
 
 
 
 
 
 