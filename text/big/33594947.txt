The Campaign (film)
 
{{Infobox film
| name           = The Campaign
| image          = Campaign film poster.jpg
| alt            = 
| caption        = Teaser poster
| director       = Jay Roach
| producer       = {{plain list|
*Jay Roach
*Adam McKay
*Will Ferrell
*Zach Galifianakis
}}
| screenplay     = {{plain list|
*Chris Henchy
*Shawn Harwell
}}
| story          = {{plain list|
*Adam McKay
*Chris Henchy
*Shawn Harwell
}}
| starring       = {{plain list|
*Will Ferrell
*Zach Galifianakis
*Jason Sudeikis
*Katherine LaNasa
*Dylan McDermott
*John Lithgow
*Dan Aykroyd Brian Cox
}} Theodore Shapiro 
| cinematography = Jim Denault
| editing        = Craig Alpert
| studio         = {{plain list|
*Gary Sanchez Productions
*Everyman Pictures
}} Warner Bros. Pictures
| released       =  
| runtime        = 85 minutes   95 minutes    
| country        = United States
| language       = English
| budget         = $95 million 
| gross          = $104.9 million 
}} political comedy North Carolinians vying for a seat in United States Congress|Congress. The screenplay was written by Shawn Harwell and Chris Henchy, and directed by Jay Roach.

==Plot== Democratic Congressman Cam Brady (Will Ferrell) of North Carolinas fictional 14th District is running for his fifth term unopposed. However his campaign is damaged by the revelation of his affair with one of his supporters, when Cam accidentally leaves a sexually explicit voice message on a local familys answering machine.
 Republican ticket, snake handlers, but he gets bitten by a snake. A video of the bite is leaked into the Internet and goes viral, increasing Cams popularity.

When Cams son plans to slander his competition for class president, Cam realizes he has set a bad example and visits Marty to make peace. A drunken Cam tells Marty that he originally became a politician to help people, citing that as class president he had a dangerous, rusty slide removed from the playground. After Cam leaves, Wattley convinces Marty to call the police and report Cam for driving while drunk. Cam is arrested and his campaign is again damaged. Marty later airs a TV ad of Cams son addressing Marty as "dad". Cam gets revenge on Marty by seducing his neglected wife Mitzi and recording the act. The released sex tape humiliates the Huggins family and causes Cams campaign manager, Mitch, to abandon him. Marty retaliates by shooting Cam in the leg on a hunting trip, increasing his own popularity.

As the election nears, Marty meets with the Motch brothers and learns of their plans to sell Hammond to their Chinese business partner and turn the town into a factory complex. Marty realizes he has been used and rejects the Motch brothers support. The Motch brothers offer Cam their support instead to preserve their plans. Marty meanwhile reconciles with his family.

On election day, Cams victory appears to be certain until Marty comes forward and exposes the Motch brothers intent and promises to preserve Hammond if elected. Cam still wins and remains congressman due to rigged voting machines owned by the Motch brothers. While Cam gloats, Marty shows his large scars to Cam and reveals that he looked up to Cam in school for getting rid of the dangerous slide. Realizing he has strayed from his true objectives as a politician, Cam withdraws from the election and Marty wins by default. Cam earns back Mitchs respect, and Marty later appoints him his chief of staff.

Six months later, Marty and Cam expose the Motch brothers scandals and the brothers are called to appear before Congress. The Motch brothers point out that everything they did is legal under Citizens United v. Federal Election Commission, but they are arrested for their association with Wattley, who is actually an international fugitive.

==Cast==
 
* Will Ferrell as Camden "Cam" Brady
* Zach Galifianakis as Martin Sylvester "Marty" Huggins
* Jason Sudeikis as Mitch Wilson
* Katherine LaNasa as Rose Brady
* Dylan McDermott as Tim Wattley
* John Lithgow as Glenn Motch
* Dan Aykroyd as Wade Motch Brian Cox as Raymond Huggins
* Sarah Baker as Mitzi Huggins
* Grant Goodman as Clay Huggins
* Kya Haywood as Dylan Huggins
* Karen Maruyama as Mrs. Yao
* Taryn Terrell as Janette
* Josh Lawson as Tripp Huggins
* P.J. Byrne as Rick
* Tzi Ma as Mr. Zheng
* Jack McBrayer as Mr. Mendenhall
* Kate Lang Johnson as Shana St. Croix
* Scott A Martin as Wes Talager
* Steve Tom as Representative Ben Langley
* Seth Morris as Confession Husband
* Nick Smith as Courtroom cameraman
* John Goodman (uncredited) as Scott Talley
 

;Cameos as themselves
 
* Wolf Blitzer
* Piers Morgan
* Bill Maher
* Chris Matthews
* Dennis Miller
* Lawrence ODonnell
* Joe Scarborough
* The Miz
* Mika Brzezinski
* Willie Geist
* Ed Schultz
* Bachman & Turner
* Dustin Milligan (uncredited)
* Uggie
* Rob Mariano
 

==Production== West Bank. 

===Music===
Green Day played the song 99 Revolutions, from their ¡Uno!, ¡Dos!, ¡Tré! trilogy, in the end credits.

==Themes==
The film lampoons modern    s New Labour, New Danger campaign. 

==Release==
The film was released on August 10, 2012.  The DVD release was on October 30, 2012.  It was on The Movie Network in 2013

==Reception==
===Box office===
The Campaign grossed $86.9 million in North America and $18 million in other territories for a total gross of $104.9 million, against a budget of $95 million. 
 The Bourne Legacy ($38.1 million). 

===Critical reception===
The Campaign received generally mixed reviews from critics. On Rotten Tomatoes, the film holds a rating of 64%, based on 195 reviews, with an average rating of 5.9/10. The sites critical consensus reads, "Its crude brand of political satire isnt quite as smart or sharp as one might hope in an election year, but The Campaign manages to generate a sufficient number of laughs thanks to its well-matched leads."  On Metacritic, the film holds a score of 50 out of 100, based on 35 critics, indicating "mixed or average reviews". 

Richard Roeper awarded the film an A- and described it as "one of the best comedies of the year" where "the material is offensively funny, but the laughs are very consistent". 

==References==
 

==External links==
 
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 