Vietnam Colony
 
 
{{infobox film
| name = Vietnam Colony
| image = Vietnam Colony.jpg
| image size =
| alt =
| caption =
| director = Siddique-Lal
| producer = Swargachitra Appachan   Joy
| writer = Siddique-Lal Innocent Kanaka Kanaka
| music = S. Balakrishnan (composer)|S. Balakrishnan Venu
| editing = T. R. Shekhar
| studio = Swargachitra
| distributor = Swargachitra
| released = 1992
| runtime =
| country = India
| language = Malayalam
| budget =
| gross =
}}
 written and directed by Kanaka and lead roles, Prabhu was Vietnam Colony.

==Plot==
 
G. Krishnamoorthy (Mohanlal), hailing from an orthodox Tamil Brahmin community, gets a job with Calcutta Constructions as a supervisor. Calcutta Company has been working to restructure their land by vacating an illegal colony lying adjacent to its premise. Popularly known as Vietnam Colony, it is inhabited mainly by day laborers. The company has been in efforts to demolish the colony for long time, but failed to do so. The colony is now under the rule a few hardcore criminals to whom the residents have to pay specific amount every week. Now, Krishnamoorthy is appointed by company to evacuate the colony, by dealing with these criminals. He is assisted by K.K Joseph (Innocent (actor)|Innocent). Both arrive at the colony under the disguise of professional writers planning to write a story on the life of the colony residents. Upon arrival, both enter the house of Pattalam Madhavi Amma (K. P. A. C. Lalitha) in search of a house, but she mistakes them to have come to see her daughter Unnimol(Kanaka (actress)|Kanaka) and calls Unni to bring tea and snacks. But after knowing about the goof happened, she lets them stay on the top floor of her house. Upon the advice of Erumely (Kuthiravattam Pappu), the broker, Madhavi Amma believes that with time, her daughter might fall in love with Krishnamoorthy and might get married to this educated Brahmin man. In the coming days, Krishnamoorthy befriends various members of the colony and tries to read out their idea about vacating the colony. But he realizes that it is not an easy task to  evacuate the people and thinks about different plans to be operated. From Madhavi Amma, Krishnamoorthy comes to know that the entire colony was owned by Moosa Sait (Nedumudi Venu), a millionaire, who even gave up his mom for money. Suhra Bai (Philomina), his mother is now living a pathetic life inside the colony. Krishnamoorthy meets up with Paravoor Rauthar (Rajkumar), Irumbu John (Bheeman Raghu) and Kannappa Srank, the criminal leaders who are now controlling the colony. One night, Rauthar kicks Surabai in a fit of rage and she is killed. To get her final rituals done, Krishnamoorthy sets out in search of Moosa Sait, but is shocked to find Advocate Thomas (Devan (actor)|Devan), the  legal adviser of his company in Saits Bungalow. More shocked he was, when saw Moosa Sait, now living on streets like a beggar. Moosa Sait tells Krishnamoorthy that he was duped by Thomas, who by crook owned up his whole property including his house. Krishnamoorthy brings in Sait to the colony and make him do the last rites of his mother. In the meantime Unni, falls in lov with Krishnamoorthy. He slowly realizes the fact that the company is illegally trying to own up the land, while the justice is on the side of colony residents. He decides to support the people in their fight for justice. But Company join hands with the criminal leaders to finish off Krishnamoorthy. He is attacked  by the criminal gang, but Krishnamoorthy succeeds in finishing them off and bringing justice to the people.

==Cast==
 
* Mohanlal as G. Krishnamurthy Innocent as K. K. Joseph Kanaka as Unnimol Devan as Adv. Thomas
* Jagannatha Varma as Company MD
* K. P. A. C. Lalitha as Pattalam Madhavi Amma
* Nedumudi Venu as Moosa Settu
* Kuthiravattam Pappu as Erumeli Vijayaraghavan as Vattappalli
* Bheeman Raghu as Irumpu John
* Philomina as Suhra Bai
* Kaviyoor Ponnamma as Parvathiyammal
* T. P. Madhavan as Krishnamurthys uncle Kunchan as Pattabhi Raman
* Sankaradi as Insane Man
*Raj Kumar as Ravuthar

==Awards==
Kerala State Film Award for Best Art Director - Mani Suchitra

==Production==
Vietnam Colony was produced and distributed by Appachan for the banner of Swargachitra.

== Sound Track ==
The films soundtrack contains 6 songs, all composed by S. Balakrishnan (composer)|S. Balakrishnan and Lyrics by Bichu Thirumala.

{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Lallalam Chollunna"
| Dr. K. J. Yesudas
|-
| 2
| "Paathiravayi Neram"
| Minmini
|-
| 3
| "Pavanarachezhuthunnu"
| Dr. K. J. Yesudas
|-
| 4
| "Pavanarachezhuthunnu"
| Sujatha Mohan
|-
| 5
| "Thala Melam"
| M. G. Sreekumar, Minmini 
|-
| 6
| "Sooryodayam"
| Dr. K. J. Yesudas
|}

==Trivia==
* The film ran for 200 days in Kerala and was the third biggest hit of the year.   Prabhu in the lead role.

==References==
  

==External links==
*  

 

 
 
 
 
 
 