Cartouches Gauloises
  French film, filmed in Algeria and directed by Mehdi Charef. It was an official selection of the 2007 Cannes Film Festival.

==Plot==
 FLN moudjahid, his mother and his French and Arab friends, as they experience the massive social changes of the end of French rule. The ensemble cast consists mostly of amateur actors.

==Cast==
*Mohamed Faouzi Ali Cherif - Ali
*Zahia Said - Aïcha
*Thomas Millet - Nico
*Tolga Cayir - Gino
*Julien Amate - David
*Nassim Meziane	- Paul
*Aurore Labrugère - Julie
*Nadia Samir - Habiba
*Bonnafet Tarbouriech - Barnabé (Station Chief)
*Mohamed Dine Elhannani	- Djelloul
*Betty Krestinsky - Rachel
*Assia Brahmi - Zina
*Marc Robert - Lt. Laurent

==Credits==
*Directed & written by Mehdi Charef	 	
*Produced by Salem Brahimi, Yacine Laloui & Michèle Ray-Gavras
*Original music by Armand Amar	 	
*Cinematography by Jérôme Alméras
*Editing by Yorgos Lamprinos

==References==
*http://www.imdb.com/title/tt1024188/

 
 
 
 


 