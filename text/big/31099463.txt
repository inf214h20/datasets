Theofilos (film)
{{Infobox film
| name           = Theofilos
| image          = 
| image size     = 
| caption        = 
| director       = Lakis Papastathis
| producer       = Lakis Papastathis
| writer         = Lakis Papastathis
| starring       = Dimitris Katalifos
| music          = 
| cinematography = Thodoros Margas
| editing        = Vangelis Gousias
| distributor    = 
| released       =  
| runtime        = 115 minutes
| country        = Greece
| language       = Greek
| budget         = 
}}

Theofilos ( ) is a 1987 Greek drama film directed by Lakis Papastathis on the life of the Greek painter Theofilos Hatzimichail. It was entered into the 38th Berlin International Film Festival.   

==Cast==
* Dimitris Katalifos as Theofilos Hatzimichail
* Manthos Athinaios
* Thodoros Exarhos
* Stamatis Fasoulis
* Irini Hatzikonstadi
* Dimitris Kaberidis
* Dimitris Katsimanis
* Anastasia Kritsi
* Constantine Lyras (as Dinos Lyras)
* Fraizi Mahaira
* Ivonni Maltezou
* Themis Manesis
* Stratos Pahis
* Stelios Pavlou
* Aris Petropoulos

==References==
 

==External links==
* 

 
 
 
 
 
 