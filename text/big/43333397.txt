Ilaignar Ani
{{Infobox film
| name           = Ilaignar Ani
| image          =
| image_size     =
| caption        = 
| director       = K. R. Selvaraj
| producer       = Radha Ravi
| writer         = K. Dinakar  (dialogues) 
| screenplay     = K. S. Selvaraj
| story          = Radha Ravi
| starring       =   Deva
| cinematography = Rajarajan
| editing        = L. Kesavan
| distributor    =
| studio         = Akhshaya Creators
| released       =  
| runtime        = 150 minutes
| country        = India
| language       = Tamil
}}
 1994 Tamil Tamil drama Deva and was released on 7 October 1994.   

==Plot==

The film begins with the caught of the terrorist Kumar (Vasu Vikram) by ACP Rajarajan (Radha Ravi). Rajarajan investigates on Kumars possible links, so he meets his room-mates Raja (R. Harish), Balu (Ranjeev), Murali (Harikumar (actor)|Harikumar), Devaraj (Kannan), Shankar (Prem Raj), Peter (Vijayraj) and Siva (Sivasanth). Soon, the youngsters clash with the terrorist Kumar who used them wrongly and the local liquor-shop owner (Raviraj).

==Cast==

*Radha Ravi as Rajarajan
*R. Harish as Raja
*Ranjeev as Balu Harikumar as Murali
*Supergood Kannan as Devaraj
*Prem Raj as Shankar
*Vijayraj as Peter
*Sivasanth as Siva Chandrasekhar as Tamizhmani
*S. S. Chandran as Kaliappan
*Vadivelu Sonia as Janani
*Padmashri as Devi
*Vasu Vikram as Kumar
*Raviraj
*Ganthimathi as Kothamalli
*Disco Shanti as Rukku
*K. S. Jayalakshmi as Pudina
*Anuja
*Srilekha
*Shanthi Ganesh
*T.K.S. Chandran
*Peeli Sivam
*R. Veeramani
*Navarasam G.K.S.
*Joker Thulasi
*Marthandan Karthik in a guest appearance

==Soundtrack==

{{Infobox Album |  
| Name        = Ilaignar Ani
| Type        = soundtrack Deva
| Cover       = 
| Released    = 1994
| Recorded    = 1994 Feature film soundtrack
| Length      = 24:25
| Label       =  Deva
| Reviews     = 
}}

The film score and the soundtrack were composed by film composer Deva (music director)|Deva. The soundtrack, released in 1994, features 5 tracks with lyrics written by Muthulingam and Piraisoodan.  

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Track !! Song !! Singer(s) !! Duration
|- 1 || Ammaavaasai || Malgudi Subha || 5:01
|- 2 || Chinna Chinna || S. P. Balasubrahmanyam, K. S. Chithra || 4:59
|- 3 || Kanni Poove Vaa || S. P. Balasubrahmanyam, K. S. Chithra || 4:24
|- 4 || Rukku Rukku || Mano (singer)|Mano, Krishnaraj || 4:58
|- 5 || Deva || 5:03
|}

==Reception==
Malini Mannath of The New Indian Express gave the film a mixed review and said : "A tighter hold on his screenplay, avoiding of repetition of situations and stronger characterisations would have brought out the message better". 

==References==
 

 
 
 
 