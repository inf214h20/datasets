Flying Pat
{{Infobox film
| name           = Flying Pat
| image          = Flying Pat (1920) - 2.jpg
| caption        = Still with Gish and Rennie
| director       = F. Richard Jones
| producer       = New Art Film Co.
| writer         = Virginia Philley Withey (story) Harry Carr (scenario) F. Richard Jones James Rennie
| music          =
| cinematography = Fred Chaston
| editing        =
| distributor    = Paramount Pictures
| released       = November 11, 1920
| runtime        = 50 minutes; 5 reels
| country        = United States
| language       = Silent (English intertitles)
}} James Rennie that was directed by F. Richard Jones. It was produced by Famous Players-Lasky and distributed by Paramount Pictures. 

The film is preserved in the Cinematheque Francais. 

==Cast==
*Dorothy Gish - Patricia Van Nuys James Rennie - Robert Van Nuys
*Morgan Wallace - William Endicott
*Harold Vizard - Butler
*Porter Strong - Reporter
*Tom Blake - Policeman
*Kate Bruce - Old Lady
*Mrs. Waters - Cook
*Miss Waters - Housemaid

==Note== James Rennie, Dorothy Gish and an unknown player. The unknown player is actor Morgan Wallace.

==References==
 

==External links==
 
* 
* 
* 

 
 
 
 
 
 
 
 


 