Three Fables of Love
 
{{Infobox film
| name           = Three Fables of Love
| image          = Las cuatro verdades.jpg
| caption        = Film poster
| director       = Alessandro Blasetti Hervé Bromberger René Clair Luis García Berlanga
| producer       = Gilbert de Goldschmidt
| writer         = Hervé Bromberger Frédéric Grendel
| starring       = Anna Karina
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 110 minutes
| country        = France Italy Spain
| language       = French Spanish
| budget         = 
}}

Three Fables of Love ( ,  ,  ) is a 1962 French-Italian-Spanish comedy film starring Anna Karina.    It was shown as part of a retrospective on Italian comedy at the 67th Venice International Film Festival.   

==Cast==
* Manuel Alexandre
* Ángel Álvarez
* Charles Aznavour - Charles (segment "Les deux pigeons")
* Alessandro Blasetti
* Xan das Bolas
* Alain Bouvette - Un collègue de Charles
* Rossano Brazzi - Leo
* Raymond Bussières - Le voleur (segment "Les deux pigeons")
* Leslie Caron - Annie (segment "Les deux pigeons")
* Ana Casares
* Hubert Deschamps - Lavocat
* Lola Gaos
* Anna Karina - Colombe
* Sylva Koscina - Mia
* Hardy Krüger
* Albert Michel - Un collègue de Charles
* Mario Passante - Le restaurateur
* Jean Poiret - Renard (segment "Le corbeau et le renard")
* Michel Serrault - Corbeau
* Gianrico Tedeschi - Valerio
* Monica Vitti - Madeleine

==References==
 

==External links==
* 

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 