The Libel Tourist
{{Infobox film
| name        = The Libel Tourist
| image       = Libel_Tourist_poster.jpg
| caption     = 
| director    = Jared Lapidus
| writer      = Jared Lapidus Sheldon Lapidus Thor Halvorssen Rob Pfaltzgraff 
| starring    = 
| narrator    = Dave Benson
| music       = Allan Fox
| studio      = Moving Picture Institute
| distributor = 
| released    =  
| runtime     = 8 minutes
| country     = United States
| language    = English Arabic
| budget      = 
| gross       = 
}}
 libel suits against writers and publishers.  The seeking out of such favorable environments, most notably the courts of England and Wales, has been dubbed libel tourism.

==Purpose== MPs have speech and press around the world, and intend to use the film as a means by which the dangers of such practices can be brought to public attention.   

==Film content==

===Sheikh Khalid bin Mahfouzs operations=== Saudi Sheikh Khalid bin Mahfouz against writer and terrorism and political corruption expert Dr. Rachel Ehrenfeld.  Bin Mahfouz claimed that the allegations that Dr. Ehrenfeld made against him in her book Funding Evil were libelous, as he had never "knowingly funded terrorism".  While Dr. Ehrenfeld never makes any assertions about the degree of bin Mahfouzs cognizance of the alleged funding, she does say that bin Mahfouz founded and/or operates a number of bogus front organizations that operate under the guise of charities, but whose actual objectives are frequently the funding and support of terrorist organizations like al-Qaeda, Hamas, and Hezbollah. Examples of such organizations are the Muwafaq ("Blessed Relief") Foundation, the International Islamic Relief Organization (HRO), and the National Commercial Bank (NCB).  These claims have been bolstered by some degree by the United States Department of the Treasury|U.S. Treasury Department, who in October 2001, froze the assets of Yasin al-Qadi, another Saudi who had been hired to run the Muwafaq Foundation "charity", who they listed as a supporter of terrorism.   

===Causes of libel tourisms prevalence===
The film asserts that "because English courts arent bound by a written constitution that protects freedom of expression" (in contrast to the courts of the United States) England has become a haven for those seeking to suppress information via libel lawsuits.  This is given as the reason why bin Mahfouz filed his lawsuit in the U.K., rather than in the U.S., where the book was initially published.  Ehrenfeld ignored the English courts in an effort to avoid a costly legal battle.
 Lord Justice destroy all copies of her book remaining in England.

===Outcome of litigations=== free press.

==Critical reception==
Reviewers have lauded the concise poignance of the film, calling it a "chilling 8-minutes"     that adeptly explain the importance of the protection of the freedom of media, and have praised the filmmakers for covering an important topic that is conspicuously overlooked by other media outlets.   

==External links==
*  

==References==
 

 