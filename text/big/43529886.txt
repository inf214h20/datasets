Letters from My Windmill (film)
{{Infobox film
| name           = Letters from My Windmill
| image          = Les lettres de mon moulin.jpg
| border         = yes
| caption        = Theatrical release poster
| director       = Marcel Pagnol
| producer       = Marcel Pagnol
| screenplay     = Marcel Pagnol
| based on       = Letters from My Windmill by Alphonse Daudet
| starring       = 
| cinematography = Willy Faktorovitch
| music          = Henri Tomasi
| editing        = Monique Lacombe Jeanne Rongier Jacqueline Baltez
| studio         = 
| distributor    = 
| released       =  
| runtime        = 160 minutes
| country        = France
| language       = French
| budget         = 
}}
Letters from My Windmill ( ) is a 1954 French comedy-drama film directed by Marcel Pagnol, starring Rellys, Robert Vattier, Fernand Sardou and Édouard Delmont. Set in the countryside of Provence, the film is based on three tales from Alphonse Daudets 1869 short story collection Letters from My Windmill: "The Three Low Masses", "The Elixir of Father Gaucher" and "The Secret of Master Cornille". It premiered on 5 November 1954 and had 2,399,645 admissions in France. 

In 1968 Pagnol made a television film based on another story from the same collection, Le curé de Cucugnan. Roger Crouzet was hired again and reprised his role as Daudet from Letters from My Windmill. 

==Cast==
;Prologue
* Henri Crémieux as Honarat Grapazzi
* Roger Crouzet as Alphonse Daudet
* Cambis as Seguin
;"The Three Low Masses"
* Henri Vilbert as Dom Balaguere
* Marcel Daxely as Garrigou
* Yvonne Gamy as The Old Woman
* Keller as The Marquis
* René Sarvil as The Chef
;"The Elixir of Father Gaucher"
* Rellys as Father Gaucher
* Robert Vattier as The Abbot
* Christian Lude as Father Sylvestre
* Fernand Sardou as M. Charnigue, apothecary
;"The Secret of Master Cornille"
* Édouard Delmont as Master Cornille
* Roger Crouzet as Alphonse Daudet
* Pierrette Bruno as Vivette

==Critical reception==
 , this collection of three little tales is full of the meat of human characters and the wine of the land from which they spring. It is spiced with the sharp and earthy humor that M. Pagnol has distilled from his long association with the Frenchmen who draw their life from her bare and rolling hills. And because of the way he shoots his pictures, in the buildings, streets and country of Provence, it has an air of authenticity about it that makes one feel an immediate participant in his scenes." 

Brett Bowles, an Associate Professor of French at Indiana University, called it "incongruously burlesque." Brett Bowles, Marcel Pagnol, Manchester: Manchester University Press, p. 4 

==References==
 

==External links==
*   at marcel-pagnol.com

 

 
 
 
 
 
 
 
 
 
 