Bhabhi (1991 film)
 
{{Infobox film
| name           = Bhabhi
| image          = Bhabhi1991film.jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Kishore Vyas	 	
| producer       = Jawaharlal B. Bafana
| writer         = Nawab Arzoo	
| screenplay     = 
| story          = 
| based on       =  
| narrator       =  Govinda Bhanupriya Juhi Chawla
| music          = Anu Malik
| cinematography = U.S. Srivastava
| editing        = 
| studio         = 
| distributor    = 
| released       =   
| runtime        = 
| country        = India
| language       = Hindi
| budget         = 
| gross          = 
}}
 Indian Bollywood film directed by Kishore Vyas and produced by Jawaharlal B. Bafana. It stars Govinda (actor)|Govinda, Bhanupriya and Juhi Chawla in pivotal roles.  

==Cast==
* Govinda (actor)|Govinda...Amar / Nakadram
* Bhanupriya...Sita / Kamini
* Juhi Chawla...Asha
* Gulshan Grover...Rakesh
* Ajit Vachani...Ghanshyamdas
* Shashi Puri...Inspector Sudhir
* Ram Mohan...Ramdas
* Dinesh Hingoo...Parsi with five sons
* Anand Balraj...Prakash
* Shobha Pradhan...Shanti
* Sahila Chaddha...Sonia
* Sangeeta Naik...Shobha

==Soundtrack==
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Tumhi Meri Mata Tumhi Pita Ho"
| Alka Yagnik
|-
| 2
| "Chandi Ki Cycle Sone Ki Seat"
| Nitin Mukesh, Anuradha Paudwal
|-
| 3
| "Aa Jana Jara Haath Batana"
| Mohammed Aziz, Chandrani Mukherjee
|-
| 4
| "Achchhaa Ji, Chehare Pe Pasine Ki Bunden"
| Udit Narayan, Alka Yagnik
|-
| 5
| "Milne Main Aayi Saasu Ji"
| Abhijeet Bhattacharya|Abhijeet, Alka Yagnik
|-
| 6
| "O Taak Dhinaa Dhin, Naach Nachaae Rupaiyaa"
| Kumar Sanu
|-
| 7
| "Tumhi Meri Mata Tumhi Pita Ho (II)"
| Alka Yagnik
|}

==References==
 

==External links==
* 

 
 
 


 