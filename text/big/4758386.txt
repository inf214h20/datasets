My Blueberry Nights
{{Infobox film
| name           = My Blueberry Nights
| image          = My Blueberry Nights poster.jpg
| caption        = Original poster Wong Kar Wai
| producer       = Wong Kar Wai Stéphane Kooshmanian Jean-Louis Piel Jacky Pang Yee Wah Wang Wei
| writer         = Wong Kar Wai Lawrence Block
| starring       = Norah Jones Jude Law David Strathairn Rachel Weisz Natalie Portman
| music          = Ry Cooder
| cinematography = Darius Khondji
| editing        = William Chang   
| studio         = StudioCanal Block 2 Pictures Jet Tone Films
| distributor    = The Weinstein Company
| released       =  
| runtime        = 95 minutes
| country        = France China Hong Kong
| language       = English
| budget         = $1,000,000
| gross          = $21,968,877   Retrieved 2011.07.22 
}}
 road film Wong Kar feature in short Chinese language|Chinese-language film written and directed by Wong. This film was the debut of jazz singer Grammy-winner Norah Jones as an actress, and also starred Jude Law, David Strathairn, Rachel Weisz, Natalie Portman, and Benjamin Kanes.

The cinematographer of this film was Darius Khondji. Christopher Doyle was Wongs cinematographer for his last seven features before My Blueberry Nights, starting from 1990s Days of Being Wild.

==Plot==
Jeremy is an émigré from Manchester who owns a small New York City cafe. The cafe becomes a haven for Elizabeth, a young woman, as Jeremy tells her that he saw her boyfriend cheating on her. Devastated, she stays in his cafe the entire night, eating a blueberry pie he made, and also telling stories about each other. Jeremy, having an eidetic memory, explains her the bowl of keys he is keeping, knowing the story of every person who left the key in his cafe and keeping it in the bowl in case someone comes back for them. Elizabeth leaves her key of her apartment and leaves Jeremys cafe.

Elizabeth, now calling herself Lizzie, eventually drifts to Memphis, Tennessee, where she takes two jobs, waitress by day and barmaid by night, in order to earn enough money to finance the purchase of a car. She regularly sends postcards to Jeremy, taking a liking to him, without revealing where she lives or works and, although he tries to locate her by calling all the restaurants in the area, he fails to find her. Later on, he decides to send out postcards to any restaurants she may be to try to find her.
 AA meetings but failing. One night, he drunkenly threatens Sue Lynne with his gun if she leaves. Later on, Arnie drives drunk and dies after crashing into a pole.  Lizzie comforts Sue Lynne, at the crash site, who gives her the money towards Arnies tab at the bar Lizzie works at in the evening, before leaving town, revealing that she made a mistake and that she misses him, also revealing that the place where he died is the place where they met, revealing that he may have committed suicide.
 Las Vegas so she can borrow money from her father, whom she has not seen in a long time.

While en route she receives a call from a Vegas hospital, where her father has been admitted and is dying. Leslie believes the call is simply a ruse to lure her home, but upon arrival in Vegas she discovers her father died the previous night. Leslie announces she wants to keep the car, which she had stolen from her father, who had sent her the title and registration despite their estrangement. She confesses she really won the card game and gives Beth her promised share of the winnings, which she uses to finally purchase the car she always wanted.

Elizabeth returns to Manhattan and, discovering her ex-boyfriend has vacated his apartment and moved on with his life, returns to the cafe, where Jeremy has had a stool at the counter reserved for her ever since she left. As she eats a slice of blueberry pie, Elizabeth realizes her feelings for him are reciprocated. She passes out on the counter after spending the night, and Jeremy kisses her while she is asleep, and she returns the kiss as the film ends.

==Production==
In Making My Blueberry Nights, a bonus on the DVD release of the film, screenwriter/director Wong Kar Wai reveals his first choice for Elizabeth was singer Norah Jones despite her lack of prior acting experience. He originally intended to shoot the film in sequence, but when he discovered Rachel Weisz, whom he wanted to cast as Sue Lynne, was pregnant, he agreed to film the Memphis scenes last to allow her time to give birth and recuperate before beginning work.
 South Main Arts District in Memphis, and Caliente, Nevada|Caliente, Ely, Nevada|Ely, and Las Vegas in Nevada.

The film premiered at the Cannes Film Festival in May 2007 and was shown at the Hamburg Film Festival, the Valladolid International Film Festival, and the Munich Asia Filmfest before going into limited theatrical release in Canada on November 16. It opened throughout Europe and Asia before opening on six screens in the US on April 4, 2008, as a limited release on USA. It earned $74,146 on its opening weekend. It eventually grossed $867,275 in the US and $21,101,602 in foreign markets for a total worldwide box office of $21,968,877. 

==Cast==
;Main
* Norah Jones as Elizabeth (Lizzie/Beth)
* Jude Law as Jeremy
* David Strathairn as Arnie Copeland
* Rachel Weisz as Sue Lynne Copeland
* Natalie Portman as Leslie
;Support Chan Marshall ... Katya
*John Malloy ... Diner Manager
*Demetrius Butler ... Male Customer
*Frankie Faison ...	Travis
*Adriane Lenox ... Sandy
*Benjamin Kanes ... Randy
*Michael Hartnett ... Sunglasses
*Michael May ... Aloha
*Chad Davis ... Boyfriend
*Katya Blumenberg ... Girlfriend

==Reception==

===Box office===
In its opening weekend, the film grossed $74,146 in 6 theaters in the United States, ranking #43 at the box office. By the end of its run, My Blueberry Nights grossed $867,275 domestically and $21,101,602 internationally, totaling $21,968,877 worldwide. {{cite web
| url=http://boxofficemojo.com/movies/?id=myblueberrynights.htm
| title=My Blueberry Nights (2007)
| publisher=Box Office Mojo
| accessdate=2011-07-27
}} 

===Critical reception===
Mick LaSalle of the San Francisco Chronicle observed, "The movies overall story is modest, and if it were any longer the film might start to drag. But at 90 minutes, its short enough to be carried along on the drama of its individual scenes and the strength of its performances . . . The nice thing about Wong is that, like a good gambler, he knows when to bet the farm and when to hold back. Most of the time, he plays it straight, and other times he will speed up the action into a kind of blur, to indicate time passing; or hell fade out and back into the same shot, as though to indicate renewed focus. Everything he does re-creates a state of mind. Its such a relief to realize hes doing everything for a reason and not to show off." 

Meghan Keane of the New York Sun said the film "keenly displays Wong Kar Wais aptitude for relationship drama and showcasing the female form, but the Chinese directors American debut often makes the earnest miscalculation of a dubbed foreign film . . .  n translating his fascination with the distances between two people into American vernacular, Mr. Wong betrays an unfamiliarity with his subject matter that often undermines his story . . . Sadly,   interpretation of American lives and landscapes has an alien quality to it. He fetishizes the American countryside, drowns his characters sorrows in whiskey, and makes plot-oriented decisions based on aesthetics rather than continuity or logic. The image of beautiful women in oversize sunglasses leaning against convertibles is not an accurate depiction of Americana – but it doesnt make for a bad visual." 

In Hong Kong, critical reception was generally mixed. Perry Lam of Muse (Hong Kong magazine)|Muse Magazine compared the film to Wongs earlier work, Chungking Express, and found My Blueberry Nights "a much lesser, more ordinary affair." 

==Awards and nominations==
Wong Kar Wai was nominated for the Palme DOr at the Cannes Film Festival and for Best Foreign Film at the Cinema Writers Circle Awards in Spain.
;Cannes Film Festival
 
|-
| 2007|| Kar Wai Wong || Palme dOr ||  
|}
;Cinema Writers Circle Awards, Spain
 
|-
| 2009|| My Blueberry Nights || Best Foreign Film (Mejor Película Extranjera) ||  
|}

==Soundtrack==
{{Infobox album  
| Name        = My Blueberry Nights
| Type        = Soundtrack
| Artist      = Various Artists 
| Cover       =
| Released    = April 1, 2008
| Recorded    = 
| Genre       = Film soundtrack
| Length      = 
| Label       = Blue Note Records
| Producer    = 
| Reviews     = 
| Last album  = 
| This album  = 
| Next album  = 
}}

The soundtrack, released on the Blue Note Records label, features tracks by the star of the film Norah Jones, Cat Power, Ry Cooder, Academy Award|Oscar-winning composer Gustavo Santaolalla, Otis Redding, Cassandra Wilson, and Amos Lee.

#The Story – Norah Jones – 4:10
#Living Proof – Cat Power – 3:10
#Ely Nevada – Ry Cooder – 2:31
#Try a Little Tenderness – Otis Redding – 3:19
#Looking Back – Ruth Brown – 4:16
#Long Ride – Ry Cooder – 3:13
#Eyes on the Prize – Mavis Staples – 4:06
#Yumejis Theme – Chikara Tsuzuki – 2:22
#Skipping Stone – Amos Lee – 2:21
#Bus Ride – Ry Cooder – 2:58
#Harvest Moon (Neil Young) – Cassandra Wilson – 4:44
#Devils Highway –   – 5:34
#Pajaros – Gustavo Santaolalla – 2:22
#The Greatest – Cat Power – 3:24

==References==
 

==External links==
* 
* 
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 