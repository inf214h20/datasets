The Second Mother (2015 film)
{{Infobox film
| name           = The Second Mother
| image          =  The Second Mother (2015 film) POSTER.jpg
| caption        = Theatrical release poster
| director       = Anna Muylaert
| writer         = Anna Muylaert
| producer       = {{Plainlist|
*Fabiano Gullane
*Caio Gullane
*Debora Ivanov
*Anna Muylaert
}}
| starring       = {{Plainlist|
*Regina Casé
*Michel Joelsas
*Camila Márdila
}}
| music          = 

| cinematography = Bárbara Alvarez
| editing        = Karen Harley
| studio         = {{Plainlist|
*Africa Filmes
*Gullane Filmes
}}
| distributor    = 
| released       =  
| runtime        = 114 minutes
| country        = Brazil
| language       = Portuguese
| gross          = 
| budget         = R$4 million 
}}

The Second Mother, originally titled Que Horas Ela Volta? (lit. What Time Does She Return?), is a 2015 Brazilian film directed by Anna Muylaert. 

==Premise==
Val, a woman from Pernambuco, goes to São Paulo, leaving behind her daughter, Jéssica, with Jéssicas grandfather. In São Paulo, Val finds a job as housemaid of a high-class house where she takes care of the familys child, Fabinho. 

Thirteen years later, Val is economically stable but feeling guilty for having left behind Jéssica. Suddenly, Jéssica calls saying he would like to do an admission exam in São Paulo. With the support of her bosses, Jéssica comes to live with them; however, living together is not easy.      

==Cast==
*Regina Casé as Val
*Michel Joelsas as Fabinho
*Camila Márdila as Jéssica
*Karine Teles as Barbara
*Lourenço Mutarelli as Carlos

==Production==
Muylaert originally envisioned it before the release of her first feature film, Durval Discos (2002),  but she felt she was not capable of directing it at the time.  Originally titled A Porta da Cozinha ("The Kitchens Door"), it was based on Muylaerts own experience with a nanny, who took care of Muylaerts son after leaving her own daughter.    The screenplay was rewritten four times since its original conception,  since Muylaert felt it was "immature",  by her and Casé.  Casé had an important role as she knew closely the reality of several Northeastern women who went to São Paulo to find a job, and Muylaert wanted to not create a "caricature" of a real figure.  On the contrary, she wrote the story because in her opinion taking care of other peoples children is "sacred work that is very underrated."  The film took nine months to make. 

The main filming location of The Second Mother, shot in February 2014, was a mansion on Morumbi, São Paulo|Morumbi, a high-class neighborhood of São Paulo.   

==Release==
Its worldwide premiere took place at the 2015 Sundance Film Festival,  in which Casé and Camila Márdila shared the World Cinema Dramatic Special Jury Award for Acting.  Its European premiere took place in the Panorama section of the 65th Berlin International Film Festival,    where it won the Panorama Audience Award.    On December 15, 2014 The Match Factory acquired international rights to the film. 

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 