Bala Bala Sese

{{Infobox film
| name           = Bala Bala Sese
| image          = Bala Bala Sese Official Poster.jpg
| caption        = Official poster
| director       = Lukyamuzi Bashir
| producer       = Lukyamuzi Bashir Usama Mukwaya
| screenplay     = Usama Mukwaya
| starring       =  
| music = Nessim Mukuza
| cinematography = Alex Ireeta
| studio         = Badi World
| released       = 2015
| country        = Uganda
| language       = Luganda
}}

Bala Bala Sese is an upcoming Ugandan film directed by Lukyamuzi Bashir based upon a screenplay by Usama Mukwaya   starring Michael Kasaija, Natasha Sinayobye, Raymond Rushabiro, Ismael Ssesanga, Fiona Birungi, Ashraf Ssemwogerere and Ddungu Jabal. Its the directors, writers and producers debut feature film. 

== Plot ==
A boyfriends battle for love through perseverance. In the outskirts of Sese Island, John (Michael Kasaija) is madly in love with Maggie and both are willing to take their love forward. Facing abuses and harassment by malicious Maggie’s father Kasirivu (Raymond Rushabiro), John, helped by his young brother Alex (Ssesanga Ismael) is determined to take in all but to retain the love of his life especially when he finds out that he has a contender village tycoon (Jabal Dungu) who is also lining up for Maggie (Natasha Sinayobye).

== Cast ==
* Michael Kasaija as John
* Natasha Sinayobye as Maggie
* Raymond Rushabiro as Kasiriivu
* Fiona Birungi as Elena
* Ismael Ssesanga as Alex
* Ddungu Jabal as Zeus
* Ashraf Ssemwogerere as Ireene
* Allen Musumba as Nanziri

==Production==
===Filming===
Principal photography on Bala Bala Sese began late 2012.  The movie was filmed on the Ssese Islands in Uganda, from which it takes its name. 

===Music and soundtrack===
The official theme song of the Bala Bala Sese is Wuuyo, recorded by A Pass and produced by Nessim of Badi Musik and became the singer most successful single todate. The official video of the song premiered on March 20, 2015 at Club Guvnor and features clips from the movie.    

== References ==
 

== External links ==
* 
* 
 
 
 
 
 
 