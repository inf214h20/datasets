Marty (film)
 
{{Infobox film
| name           = Marty
| image          = Marty film poster.jpg
| alt            = Film poster
| caption        = Theatrical release poster
| director       = Delbert Mann
| producer       =  
| writer     = Paddy Chayefsky
| starring       =  
| music          = Roy Webb
| cinematography = Joseph LaShelle
| editing        = 
| studio         = 
| distributor    = United Artists
| released       =  
| runtime        = 94 minutes
| country        = United States
| language       = English
| budget         = $350,000 HOLLYWOOD DOSSIER: MARTY HITS JACKPOT – TEAM – ON THE SET By OSCAR GODBOUTHOLLYWOOD.. New York Times (1923–Current file)   11 Sep 1955: X7.  Tino Balio, United Artists: The Company The Changed the Film Industry, Uni of Wisconsin Press, 1987 p 82 
| gross          = $2,000,000 (US/ Canada rentals)  $1,500,000 (overseas rentals) 
}}
 1953 teleplay The Lost Weekend (1945) are the only two films to win both organizations grand prizes.

==Plot==
Marty Piletti (Ernest Borgnine) is an Italian-American butcher who lives in The Bronx, New York City, with his mother (Esther Minciotti). Unmarried at 34, the good-natured but socially awkward man faces constant badgering from family and friends to get married, pointing out that all his brothers and sisters are already married with children. Not averse to marriage but disheartened by his lack of prospects, Marty has reluctantly resigned himself to bachelorhood. In spite of his failed love life, Marty maintains an optimistic outlook on life, characterized by his frequent outbursts such as "Perfect!" or "Fantastic!".

After being harassed by his mother into going to the Stardust Ballroom one Saturday night, Marty connects with Clara (Betsy Blair), a plain schoolteacher who is quietly weeping on the roof after being callously abandoned at the ballroom by her blind date. Spending the evening together dancing, walking the busy streets, and talking in a diner, Clara and Marty discover many shared affinities. He eagerly spills out his life story and ambitions, and they encourage each other. He brings Clara to his house, and they awkwardly express their mutual attraction, shortly before his mother returns. Marty, delighted with his new-found love, takes her home by bus, promising to call her at two-thirty the next afternoon, after Mass (liturgy)|Mass. In an exuberant scene, he punches the bus stop sign and weaves between the cars, looking for a cab, a rare luxury matching his mood.

Meanwhile, his cranky, busybody widowed aunt moves in to live with Marty and his mother. She warns his mother that living alone, when children marry, is a widows fate. Fearing that Martys romance could spell her abandonment, his mother belittles Clara. Martys friends, with an undercurrent of envy, deride Clara for her plainness and try to convince Marty to forget her and to remain with them, unmarried, in their fading youth. Harangued into submission by the pull of his friends, Marty doesnt call Clara. 

That night, back in the same lonely rut (among his regular cast of male friends), Marty realizes that he is giving up a chance of love with a woman whom he not only likes, but who makes him happy. Over the objections of his friends, he dashes to a phone booth to call Clara, who is disconsolately watching television with her parents. When his friend asks what hes doing, Marty bursts out saying:
 
Marty closes the phone booths door when Clara answers the phone. In the last line of the film, he tentatively says "Hello... Hello, Clara?".

== Cast==
* Ernest Borgnine as Marty Piletti
* Betsy Blair as Clara
* Esther Minciotti as Ms. Piletti, Martys mother
* Augusta Ciolli as Aunt Catherine
* Joe Mantell as Angie
* Karen Steele as Virginia
* Jerry Paris as Tommy
* Frank Sutton as Ralph (uncredited)

==Production==
For the film, Esther Minciotti, Augusta Ciolli and Joe Mantell reprised their roles from the live television production. The screenplay changed the name of the Waverly Ballroom to the Stardust Ballroom. The film expanded the role of Clara, and subplots about Martys career and his mother and her sister were added. Chayefsky, Paddy. "Two Choices of Material". Television Plays, Simon & Schuster, 1955. 
 Grand Concourse, Gun Hill Third Avenue, White Plains Jerome Avenue lines. On-set filming took place at Samuel Goldwyn Studios on November 1, 1954. Bronx native Jerry Orbach made his film debut in an uncredited role as a ballroom guest. Chayefsky had an uncredited cameo as Leo.
 Marxist and communist sympathies. It was only through the lobbying of Kelly, who used his major star status and connections at MGM to pressure United Artists, that Blair got the role. Reportedly, Kelly threatened to pull out of the film Its Always Fair Weather if Blair did not get the role of Clara.

==Reception==
With an April 11, 1955, premiere (followed by a wide release July 15), the film received overwhelmingly positive reviews from critics. Ronald Holloway of Variety (magazine)|Variety wrote, "If Marty is an example of the type of material that can be gleaned, then studio story editors better spend more time at home looking at television."  Time (magazine)|Time described the film as "wonderful".  Louella Parsons enjoyed the film, although she felt that it would not likely be nominated for Oscars.  At a budget of $343,000, the film generated revenues of $3,000,000 in the US alone, making it a box office success. 

==Accolades==

===Academy Awards===

{| class="wikitable"
|-
! Award    !! Result !! Winner
|- Best Motion Picture ||   || United Artists (Harold Hecht, producer)
|- Best Director ||   || Delbert Mann
|- Best Actor ||   || Ernest Borgnine
|- Best Writing, Adapted Screenplay ||   || Paddy Chayefsky
|- Mister Roberts
|- East of Eden
|- The Rose Tattoo
|- The Rose Tattoo
|-
|}

===Cannes Film Festival===
Winner Palme dOr   

==Legacy==
In 1994, Marty was deemed "culturally, historically, or aesthetically significant" by the Library of Congress and selected for preservation in the National Film Registry.
 Only the Lonely. The television film Queen of the Stardust Ballroom (1975) has been reviewed as a "gender reversal" of Marty.   
 Twenty One, Quiz Show (1994).
 
The animated series Rockos Modern Life featured a parody of the character Marty, and modeled partially on his physical likeness, in the Season 1 episode "Rockos Happy Sack".

Another animated series, Hey Arnold! featured a butcher named Marty Green who was also based on Borgnines character. 

Comedian and Curb Your Enthusiasm star Jeff Garlin is a fan of Marty, and his 2006 film, I Want Someone to Eat Cheese With, contains numerous references, including a subplot involving a studio remake starring singer Aaron Carter as the title character and actress Gina Gershon as Martys mother.

==References==
 

==External links==
 
*  
*  

 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 