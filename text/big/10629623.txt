I'm with Lucy
{{Infobox film
| name           = Im with Lucy
| image          = Im with Lucy - dvd cover.jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = DVD cover
| director       = Jon Sherman
| producer       = Fabien Liron Tim Perrell Amy Kaufman ( )
| writer         = Eric Pomerance John Hannah Anthony LaPaglia Henry Thomas
| music          = Stephen Endelman Tom Richmond
| editing        = Elizabeth Kling
| studio         = Gaumont Film Company Fabulous Films Process Productions
| distributor    = Gaumont Buena Vista International (France) Columbia Pictures/Sony Pictures (United States)
| released       =  
| runtime        = 
| country        = France United States
| language       = English
| budget         = $15 million
| gross          = 
}}
 John Hannah.

== Plot ==
Lucy is a journalist who is dumped by her "perfect" boyfriend and then goes on a series of dates with five different men—Doug, an entomologist; Gabriel, a playwright; Bobby, a baseball player; Barry, a computer store owner; and Luke, a doctor. She acts differently around each of the men—she is drunk on her date with Doug; she uncharacteristically jumps into bed with Gabriel; she is, at first, irritated with but then moved by ex-baseball star Bobby; her date with Barry gets off to a rough start, but then while on their date, they run into her parents and end up having dinner with them; and her date with Luke is sidetracked when they see a colleague of Lukes who is with his daughter, Eve, who appears to have her own eye on Luke.

She doesnt connect with Doug, but she does get him to come out of his shell by the end of the date.  She takes Bobby to several places that put him completely out of his element and then he takes her to a baseball card show where she discovers a different side of him.  Her date with Gabriel essentially becomes a one night stand when she realizes he isnt what she wants in life.  She becomes serious with Luke, but an incident at a restaurant in which he is rude to one of the waiters makes her realize he isnt the one she wants, either.  Barry surprises her several times throughout the movie with touching and thoughtful gestures, which of course win her over in the end.

==Cast==
* Monica Potter as Lucy
* Henry Thomas as Barry
* David Boreanaz as Luke
* Anthony LaPaglia as Bobby John Hannah as Doug
* Gael García Bernal as Gabriel
* Harold Ramis as Jack
* Julie Christie as Dori
* Robert Klein as Dr. Salkind
* Julie Gonzalo as Eve
* Julianne Nicholson as Jo
* Flora Martinez as Melissa
* Craig Bierko as Peter

==Production==
Budgeted with $15 million, Im with Lucy started filming on April 16, 2001. The film was shot in New York City and in Florida. 

==Reception==
Lisa Nesselson of Variety (magazine)|Variety wrote: "Performances are sharper than the material in Im With Lucy, a moderately diverting comedy. Although Monica Potter is versatile and amusing in the title role, pic feels like a string of incidents rather than a full-bodied narrative." 

==References==
 

== External links ==
*  
*  
*  

 
 
 
 
 
 
 
 
 


 