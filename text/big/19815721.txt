The Olsen Gang's Big Score
 
{{Infobox film
| name           = The Olsen Gangs Big Score
| image	         = The Olsen Gangs Big Score FilmPoster.jpeg
| caption        = Poster by Aage Lundvald
| director       = Erik Balling
| producer       = Bo Christensen Hanne Krebs Jan Lehmann
| writer         = Henning Bahs Erik Balling
| starring       = Ove Sprogøe
| music          = 
| cinematography = Jørgen Skov
| editing        = Ole Steen Nielsen
| distributor    = Nordisk Film
| released       =  
| runtime        = 99 minutes
| country        = Denmark
| language       = Danish
| budget         = 
}}

The Olsen Gangs Big Score ( ) is a 1972 Danish comedy film directed by Erik Balling and starring Ove Sprogøe. The film was the fourth in the Olsen Gang-series. 

==Cast==
* Ove Sprogøe - Egon Olsen
* Morten Grunwald - Benny Frandsen
* Poul Bundgaard - Kjeld Jensen
* Kirsten Walther - Yvonne Jensen Arthur Jensen - Kongen
* Poul Reichhardt - Knægten
* Jesper Langberg - Mortensen
* Bjørn Watt-Boolsen - Politichefen
* Helle Virkner - Højesteretssagførerens hustru
* Asbjørn Andersen - Højesteretssagfører
* Jes Holtsø - Børge Jensen
* Annika Persson - Sonja
* Gotha Andersen - Bademesteren
* Gert Bastian - Bankchauffør
* Jørgen Beck - Overtjener på hotel
* Edward Fleming - Lufthavnsbetjent
* Poul Glargaard - Pilot
* Poul Gregersen - Bassist
* Børge Møller Grimstrup - Værkfører
* Tage Grønning - 2. violin
* Gunnar Hansen - Kommentator ved fodboldkamp (voice)
* Ove Verner Hansen - Købmanden
* Knud Hilding - Betjent
* Kay Killian - Pianist
* Anton Kontra - 1. violin
* Bertel Lauring - Tjener på hotel Ernst Meyer - Bankfunktionær
* Jens Okking - Køkkenmester
* Søren Rode - Fremmedarbejder
* Claus Ryskjær - Ekspedient i tøjforretning
* Bjørn Spiro - Bankchauffør
* Karl Stegger - Tjener
* Søren Strømberg - TV-speaker
* Solveig Sundborg - Nabokonen
* Bent Thalmay - Bud fra pelsfirma
* Poul Thomsen - Pilot
* Bent Vejlby - Lufthavnsbetjent
* Holger Vistisen - Receptionist på hotel
* Bent Warburg - TV-regissør
* Henrik Wiehe - Mand i fly

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 
 

 
 