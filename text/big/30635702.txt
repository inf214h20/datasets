Akhiyon Se Goli Maare
{{Infobox film
| name           = Akhiyon Se Goli Maare
| image          =Akhiyon_Se_Goli_Maare.jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| film name      = 
| director       = Harmesh Malhotra
| producer       = Harmesh Malhotra
| writer         = Anwar Khan (Dialogues)
| screenplay     = Praful Parekh, Rajeev Kaul
| story          = Praful Parekh, Rajeev Kaul
| based on       =  
| narrator       = 
| starring       = Govinda (actor)|Govinda, Raveena Tandon, Kader Khan, Shakti Kapoor, Johnny Lever, Asrani, Dinesh Hingoo, Razzak Khan Sameer (Lyrics)
| cinematography = Shyam Shilposkar
| editing        = Govind Dalwadi
| studio         = 
| distributor    = 
| released       =  
| runtime        = 
| country        = 
| language       = 
| budget         = 
| gross          = 
}}

Akhiyon Se Goli Maare ( ; Translated: Shoots with eyes) is a 2002 Bollywood comedy film directed by Harmesh Malhotra, and starring Govinda (actor)|Govinda, Raveena Tandon, Kader Khan, Shakti Kapoor, Asrani and Johnny Lever. The film was released on August 02nd, 2002.

==Plot==
Akelinder or Bhangari Dada (Kader Khan) has a shop at Chor bazaar which he runs illegally, he lives with his wife and only daughter Kiran (Raveena Tandon). Bhangari wants his daughter to be married to a gangster, so he begins his search. He finds Shakti dada (Shakti Kapoor) and wants him to be married to Kiran but Kiran already loves Raj (Govinda), a wealthy man who has no links to crime. But in order to satisfy her dad, she persuades Raj to pose as a Gangster for time being. For this they search a Trainer called Subramaniam (Johnny Lever), who teaches Raj to act as a Gangster. Finally Raj becomes successful posing as a gangster but Bhangaris dad (Kader Khan) somehow appears and doesnt want Kiran to marry to a gangster. Will Raj be able to clean his image?

==Cast== Govinda  as  Raj Oberoi / Bobdey Dada
*Raveena Tandon  as  Kiran Bhangare
*Kader Khan  as  Aklinder "Topichand" Bhangare / Rana Bheeshmbar Pratap (Bhangares father)
*Asrani  as  Topichands brother-in-law
*Johnny Lever  as  Master Subramaniam
*Avtar Gill  as  Thomson
*Dinesh Hingoo  as  Shaadilal
*Razzak Khan  as  Faiyaz Takkar Pehelwan
*Satyen Kappu  as  Magistrate Kapoor
*Shakti Kapoor  as  Shakti Dada
*Tiku Talsania  as  Mr. Oberoi
*Viju Khote  as  Chorge
*Sharat Saxena  as  Babu Chaphri  Beena  as  Mrs. Oberoi
*Ghanashyam Rohera  as  Constable Dhanche
*Rana Jung Bahadur  as  Jerry
*Anjana Mumtaz  as  Sulekha Bhangare
*Manmauji
*Shyam Avasthi
*Shyam Solanki
*Veeru Krishnan

==Box Office==
The movie was a box office bomb.

==Soundtrack==
{{Track listing
| heading        = Songs
| extra_column   = Singer(s)
| lyrics_credits = yes
| music_credits  = yes

| title1  = Akkh Jo Tujh Se Lad Gayi Hai
| lyrics1 = Dev Kohli
| music1  = Anand-Milind
| extra1  = Sonu Nigam, Jaspinder Narula

| title2  = Dehradun Ka Chuna Lagaya
| lyrics2 = Salim Bijnauri
| music2  = Daboo Malik
| extra2  = Vinod Rathod, Sunidhi Chauhan

| title3  = Gore Tan Se Sarakta Jaye Sameer
| music3  = Anand-Milind
| extra3  = Sonu Nigam, Alka Yagnik, Sanjeevini

| title4  = Ladka Mud Mud Ke Maare 	
| lyrics4 = Sameer
| music4  = Anand-Milind
| extra4  = Vinod Rathod, Alka Yagnik

| title5  = Maine Tujhe Dekha
| lyrics5 = Sameer
| music5  = Anand-Milind
| extra5  = Sonu Nigam, Alka Yagnik

| title6  = O Chhori Gori Gori
| lyrics6 = Sameer
| music6  = Anand-Milind
| extra6  = Sonu Nigam, Jaspinder Narula

| title7  = Rabba O Rabba
| lyrics7 = Dev Kohli
| music7  = Anand-Milind
| extra7  = Udit Narayan, Alka Yagnik

| title8  = Thumka Lagake Naachlo
| lyrics8 = Nitin Raikwar
| music8  = Dilip Sen-Sameer Sen
| extra8  = Sonu Nigam, Vinod Rathod, Sanjeevini
}}

==Trivia==
The name of the film is taken from the song of the same name from the 1998 movie Dulhe Raja, also starring Govinda (actor)|Govinda, Raveena Tandon, Kader Khan, Asrani, and Johnny Lever.

==External links==
*  

 
 
 


 

 