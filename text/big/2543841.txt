The Floorwalker
{{Infobox film
| name           = The Floorwalker
| image          = The Floorwalker (poster).jpg
| caption        = Theatrical poster to The Floorwalker
| director       = Charles Chaplin Edward Brewer (technical director)
| producer       = Henry P. Caulfield
| writer         = Charles Chaplin (scenario) Vincent Bryan (scenario) Maverick Terrell (scenario) Eric Campbell Edna Purviance
| music          =
| cinematography = William C. Foster Roland Totheroh
| editing        = Charles Chaplin
| distributor    = Mutual Film Corporation
| released       =  
| runtime        = 2 Reels (24 minutes in restored, speed-corrected version)
| country        = United States English intertitles
| budget         =
}}
 Charlie Chaplins Tramp persona, Eric Campbell, and the stores floorwalker, played by Lloyd Bacon, to embezzle money from the establishment.  
 
The film is noted for the first "running staircase" used in films which is used for a series of slapstick that climaxes with a frantic chase down an upward escalator and finding they are remaining in the same position on the steps no matter how fast they move.  Edna Purviance plays a minor role as a secretary to store manager, Eric Campbell.
 

==Mirror sequence==
Roughly seven minutes from the start of the film, Chaplin and the stores floorwalker, Lloyd Bacon, stumble into opposite doors of an office and are intrigued by their likeness to each other. They mirror each others movements to deft comic effect in a way that is believed to have inspired the "mirror scene" in Max Linders 1921 movie Seven Years Bad Luck. In that comedy film, Maxs servants accidentally break a mirror and try to hide their mistake by having one of them dress just like their employer. Then, when Max looks into the non-existent glass, the disguised servant mimics his every action.

Max Linders movie in turn inspired many similar scenes, most famously in the Marx Brothers film Duck Soup (1933 film)|Duck Soup. Later renditions can be found in the Bugs Bunny cartoon Hare Tonic, the Mickey Mouse cartoon Lonesome Ghosts, and in the TV series Family Guy and The X-Files. A scene in The Pink Panther, with David Niven and Robert Wagner wearing identical gorilla costumes, mimics the mirror scene. Harpo Marx did a reprise of this scene, dressed in his usual costume, with Lucille Ball also donning the fright wig and trench coat, in an episode of I Love Lucy. Additionally, an early episode of The Patty Duke Show contains a mirror scene in which the characters Patty and Cathy Lane (both played by Patty Duke) act out a version similar to the one found in the film Duck Soup.

==Sound version== Amedee Van Beuren of Van Beuren Studios, purchased Chaplins Mutual comedies for $10,000 each, added music by Gene Rodemich and Winston Sharples and sound effects, and re-released them through RKO Radio Pictures. 

==Cast==
* Charles Chaplin - Tramp Eric Campbell - Store manager
* Edna Purviance - Managers secretary
* Lloyd Bacon - Assistant manager
* Albert Austin - Shop assistant
* Charlotte Mineau - Beautiful store detective

== References ==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 

 