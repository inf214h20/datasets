Pidakkozhi Koovunna Noottandu
{{Infobox film
| name           = Pidakkozhi Koovunna Noottandu
| image          = Pidakkozhi Koovunna Noottandu.jpg
| caption        = 
| alt            =
| director       = Viji Thampi Urvashi
| story         = Urvashi
| screenplay = Sasidharan Arattuvazhi Kalpana Vinaya Janardhanan Saikumar Saikumar
| music          = S. P. Venkatesh
| cinematography = Dinesh Baboo
| editing        = A. Sreekar Prasad
| studio         =
| distributor    = Prathiksha Pictures
| released       = 1994
| runtime        = 144 minutes
| country        = India
| language       = Malayalam
| budget         =
| gross          =
}}
 Malayalam comedy thriller film directed by Viji Thampi and produced by Urvashi (actress)|Urvashi, starring Urvashi, Manoj K. Jayan, Jagathy Sreekumar, K. P. A. C. Lalitha, Kalpana (Malayalam actress)|Kalpana, Vinaya Prasad, Janardhanan (actor)|Janardhanan, and Saikumar (Malayalam actor)|Saikumar.

==Plot==

The story revolves around a paying guest home run by Chinthamani Ammal(K. P. A. C. Lalitha) who is a determined man-hater due to a failed love affair. Only man-haters are allowed as paying guests. Ponnamma (Kalpana (Malayalam actress)|Kalpana) hates men because of her parents being partial to her twin brother Ponnappan(Kalpana). Another guest, Nancy, is a lawyer and is engaged on a post divorce custody battle for the child with Tony Varghese. Rudra and Bhagyarekha pretend to be man-haters to please Chinthamani Ammal.

The story is about how each of the women finds true life partner.

==Cast==
{| class="wikitable" border="1"
! Actor || Role
|- Urvashi
| Bhagyarekha - Sujatha
|-
| Manoj K. Jayan
| Sachidanandan
|-
| Jagathy Sreekumar
| Ikru, Sathyasheelan
|-
| K. P. A. C. Lalitha
| Chinthamani Ammal
|- Kalpana
| Ponnamma, Ponnappan
|-
| Vinaya Prasad
| Nancy Joseph
|- Janardhanan
| Dr. Vishnu Narayanan Potty
|- Saikumar
| Binoy Vishwam
|-
| Rudra
| Vasundhara
|-
| Ratheesh
| Tony Varghese
|-
| Sukumari
| Amalu
|-
| Idavela Babu 
| 
|- Jagannathan
| 
|- Dileep
|-
| Mavelikkara Ponnamma
|
|-
|}

==External links==
*  

 
 
 


 