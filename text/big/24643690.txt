The Loudspeaker
 
{{Infobox film
| name           = The Loudspeaker
| image          = Julie Bishop and Ray Walker, in The Loudspeaker (1934).jpg
| image_size     = thumb
| caption        = Julie Bishop and Ray Walker (1934)
| director       = Joseph Santley
| producer       = William T. Lackey (producer)
| writer         = Albert DeMond (screenplay) Ralph Spence (dialogue and story)
| narrator       =
| starring       = See below
| music          =
| cinematography = Gilbert Warrenton
| editing        = Jack Ogilvie
| distributor    = Monogram Pictures
| released       = 1 June 1934
| runtime        = 67 minutes
| country        = USA
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}

The Loudspeaker is a 1934 American  comedy film directed by Joseph Santley.

The film is also known as The Radio Star in the United Kingdom.

== Plot summary ==
Wisecracking Joe Miller (Ray Walker) makes it to the big time in radio.
 Julie Bishop) her big break, she becomes an even bigger star. He feels like the jokes on him, when she starts going out with his friend George (Lorin Raker); and, he turns to booze, for comfort.

Whether Joe can crawl back up, out of the bottle; or, if Janet, or the sponsors and audience will take him back, remains to be seen.

== Cast == Ray Walker as Joe Miller Julie Bishop as Janet Melrose
*Charley Grapewin as Pop Calloway
*Noel Francis as Dolly
*Lorin Raker as George Green
*Spencer Charters as Burroughs
*Larry Wheat as Thomas
*Mary Carr as Grandma
*Ruth Romaine as Amy Witherspoon
*Billy Irvin as Caleb Hawkins
*Eddie Kane
*Wilbur Mack as Walker
*Sherwood Bailey as Ignatz
*The Brownies Trio as Vocal Ensemble

== Soundtrack ==
* Jacqueline Wells (dubbed) - "Who But You" (Written by Harry Akst and Lew Brown)
* Jacqueline Wells (dubbed) - "Doo Ah Doo Ah Know What Im Doing" (Written by Harry Akst and Lew Brown)

== External links ==
* 
* 

 

 
 
 
 
 
 
 


 