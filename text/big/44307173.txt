The Blood Demon
{{Infobox film name = The Blood Demon image = The Blood Demon poster.jpg caption = Poster of the original title director = Harald Reinl  producer = Constantin Film  writer = Manfred R. Köhler based on =   music = Peter Thomas cinematography = {{plainlist|
* Ernst W. Kalinke
* Dieter Liphardt
}} editing =  starring = {{plainlist|
* Christopher Lee
* Karin Dor
* Lex Barker Carl Lange
}} studio =  distributor = {{plainlist|
* Constantin Film 
* Hemisphere Pictures 
}} released =   runtime = 90 minutes  country = Germany language = English German
}}
The Blood Demon or Die Schlangengrube und das Pendel, also known as The Torture Chamber of Dr. Sadism, The Snake Pit and the Pendulum,    and The Castle of the Walking Dead, is a 1967 German horror film directed by Harald Reinl and starring Christopher Lee, Karin Dor, and Lex Barker.       The plot is based on the short story The Pit and the Pendulum by Edgar Allan Poe and concerns the saga of Count Regula, played by Christopher Lee, who, after being drawn and quartered for murdering twelve maidens, returns to life seeking revenge.   
The film was advertised in Rhode Island newspapers as Crimson Demon due to a practice at the time of deleting the word "Blood" from film titles. 

==Plot==

The plot chronology is set in the 18th century and the probable story location is Germany.  Baroness Lilian von Brabant and her lawyer Roger Mont Elise receive an invitation to the Blood Castle,    in Sander Valley, where a large inheritance is awaiting the baroness. Both decide to go; the Baroness because of the inheritance and Roger seeing a chance to get more information regarding his birth.  Upon arriving at the valley, they meet the monk Fabian with a proclivity for profanities who offers to assist them in finding their way to the castle, the place where, forty years ago, Count Regula had murdered twelve maidens, in an attempt to use their blood to achieve immortality.     However he was one maiden short of his goal,     and he was drawn, quartered and beheaded for his crime.     As he was dying, the Count threatened revenge against those responsible for his death.   

On their way to the castle, passing through a dead forest full of corpses, with human limbs and torsos hanging from the dead tree branches,  they get attacked by mysterious hooded creatures riding horses who try to abduct the women. Despite Rogers successful efforts to protect the Baroness and her maid Babette from the creatures,    the two women mysteriously vanish. Fabian, who is revealed to be a robber rather than a monk, is so terrified by the unexplained disappearance of the women that he offers to help Roger find them. The two men finally locate the women locked in an iron chamber at Blood Castle, although they get caught before they can rescue them.  
 .   ]]
After their capture, the Counts evil, green-blooded,  servant Anatol informs the two men that he is planning to bring the  Count back to life after forty years.  Anatol, using his own green blood,    finally achieves his goal of reviving the Count.  Following his resurrection, the Count appears to the prisoners wearing an iron mask, informing the men that he needs the blood of maiden number thirteen to achieve his goal of immortality. The maiden is revealed to be the Baroness. 

After the pronouncements by the Count, the prisoners make an escape attempt but they are unsuccessful. As punishment, the baroness is locked into a snake and spider pit where she loses her sanity. Roger, imprisoned in a pit with a pendulum,  manages to overcome the odds and survive. He also recovers the diamond-encrusted cross of the Baroness, which he uses to destroy the Count and Anatol, finally succeeding in freeing the prisoners. The Baroness recovers and falls into Rogers embrace while Fabian leaves the crumbling castle with Babette.  

==Cast==
Selected cast.   
* Christopher Lee as Count Regula
* Karin Dor as Baroness Lilian von Brabant
* Lex Barker as Roger Mont Elise and Roger von Marienberg Carl Lange as Anatol
* Dieter Eppler as the Coachman

==Distribution==

The film was distributed as a single bill until Kane W. Lynn, president of low-budget distribution company Hemisphere Pictures, combined it in a double bill with the film Mad Doctor from Blood Island.  

==Reception==
  stones found in the Teutoburg Forest.   ]]
TLA Video & DVD Guide describes the film as "an effective bit of   film".    

Halloween calls it a "delight for hardcore adult fans",    and the Katholisches Institut für Medieninformationen includes the description of the film as a "German attempt at a horror film by Edgar Allan Poe, more laughable than creepy".    
 Wizard of Oz-like journey into horror". The review also calls the film a "skillfull blend of horror and adventure" and a picture which offers "creepy delights" such a "forest of hanging corpses", "a castle full of torture traps" and a "sinister one-legged messenger on a cobbled village street".  

According to TV Guide, the plot may be weak but the film has "fascinating visuals" including an "eerie forest of the dead".  Monsters & Vampires mentions that the "The movie had some good chilled moments, particularly a ghostly ride through a literally dead forest, with branches filled with severed limbs and torsos."  Film critic Leonard Maltin described the film as "atmospheric".   

==Newspaper practice==
In the state of Rhode Island in the United States, as well as some other U.S. states, a practice was adopted by newspapers of the era under which the word "Blood" was deleted from the title of film advertisements and another was substituted in its place. Film titles such as Blood Demon became Crimson Demon, Mad Doctor from Blood Island became  Mad Doctor from Crimson Island, and Blood of Draculas Castle became Red of Draculas Castle, the only exception being Roger Corman  film Bloody Mama which retained its original title. The newspapers, when faced with enquiries regarding this unusual advertising practice, did not provide any answers. Theater managers were indifferent to the policy because it did not seem to have an impact at the box office. 

==See also==
*Edgar Allan Poe in television and film

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 