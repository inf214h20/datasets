Endless Love (1981 film)
 
{{Infobox film
| name = Endless Love
| image = Endless love.jpg
| caption = Theatrical release poster
| director = Franco Zeffirelli
| producer = Keith Barish Dyson Lovell
| screenplay = Judith Rascoe Endless Love Scott Spencer
| starring = {{Plain list | 
* Brooke Shields Martin Hewitt
* Shirley Knight Don Murray
* Richard Kiley
* Beatrice Straight
* James Spader
}}
| music = Lionel Richie Jonathan Tunick David Watkin
| editing = Michael J. Sheridan
| studio = PolyGram Filmed Entertainment Universal Pictures
| released =  
| runtime = 116 minutes
| country = United States
| language = English
| budget =  
| gross = $32,492,674 
}} romantic drama Martin Hewitt, Scott Spencer. score was composed by Jonathan Tunick.
 Endless Love", became a #1 hit on the Billboard Hot 100|Billboard Hot 100, and was the biggest-selling single in Ross career. Billboard magazine chose it as "The Best Duet of All Time" in 2011, 30 years after its debut. It spent 9 weeks at #1 and received Academy Award and Golden Globe nominations for "Best Original Song", along with 5 Grammy nominations.

==Plot== Martin Hewitt) bohemian lifestyle, allowing them to develop an all-consuming and passionate relationship; including allowing the two to make love in Jades bedroom. In contrast to the openness of her family, Davids home life is dull; his parents are wealthy political activists who are not actively involved in his life.
 Don Murray), however, watches the couple with increasing unease. Jades nightly trysts begin negatively impacting her grades and her ability to sleep. She attempts to steal one of her fathers prescription sleeping pills but is caught in the act. This is the last straw for Hugh and he insists David stop seeing Jade until the end of the school term in 30 days. Although David initially causes a scene, Ann gently coaxes him into agreeing, telling him not to let Hugh "do something hell regret".

Back at school, Davids friend Billy (Tom Cruise) tells him that when he was eight years old he tried burning a pile of newspapers and after he became scared, put the fire out, only to find his parents think he was a hero for saving the house from burning. Inspired by this story, David starts a fire on the Butterfields front porch and walks away briefly but by the time he returns, the flame has spread too far. He rushes to warn the family but he is too late, the entire house is lost.

Following the trial, David is convicted of second-degree arson, sentenced to five years probation, sent to a mental hospital for evaluation, and is forbidden from ever going near Jade or her family again.  Nevertheless, he continues to write daily, but his letters are not sent due to the court order to not contact Jade. His parents pull strings to have him released early, much to Hughs chagrin. David receives his many letters upon his exit, and upon realizing why Jade never wrote back, decides to pursue her even though he knows full well that it will violate his parole.

In the meantime, following the loss of their home, the Butterfield family has moved from Chicago to Manhattan, and Ann and Hugh divorce. In Manhattan, Ann tries to seduce David, but he refuses which leaves her rather nonplussed. When Ann isnt looking, David thumbs through her address book to see where Jade is. On his way over, Hugh sees David on the street and while chasing him, is hit by a car and killed.  Hughs new wife Ingrid Orchester (Penelope Milford), catches up to the scene just in time to witness David flee.

Later, Jade goes to Davids apartment to say goodbye but he pulls her back as she tries to leave, throwing her on the bed and forcefully holding her down until she admits she loves him, which she eventually does. Keith comes home to find the pair together again and angrily informs Jade that David is at fault for their fathers death. Jade refuses to believe it at first but when David confirms it she becomes horrified and hides behind Keith, whom David then shoves out of the way in a desperate bid to grab her. Keith fights him off until the police arrive and arrest David.

Sentenced to prison, David seems doomed never to see Jade again. Jade tells her mother at her fathers lakeside funeral that no one will ever love her the way David does, and Ann speaks her understanding and approval. The final scene shows David watching Jade walk towards him through his barred cell window.

==Cast==
* Brooke Shields as Jade Butterfield Martin Hewitt as David Axelrod
* Shirley Knight as Ann Butterfield Don Murray as Hugh Butterfield
* Richard Kiley as Arthur Axelrod
* Beatrice Straight as Rose Axelrod
* James Spader as Keith Butterfield
* Ian Ziering as Sammy Butterfield
* Penelope Milford as Ingrid Orchester
* Tom Cruise as Billy
* Jami Gertz as Patty
* Jeff Marcus as Leonard
* Walt Gorney as Passerby

==Production==
Endless Love was the feature film debut for a number of actors, including Tom Cruise, Jami Gertz, and Jeff Marcus, and features very early appearances by James Spader, as the elder brother of Brooke Shields, and of a pre-Beverly Hills, 90210, Ian Ziering.
 Don Murray) gets hit by a car in New York.  The stuntman does a high end-over-end flip in mid-air.
 MPAA initially X rating. R rating. 

==Differences between the film and the novel==
 
The novel and film differ in several respects. The novel begins with David burning down the Butterfields house in 1967. The death of Hugh takes place near the end of the film, but only in the middle of the novel. After that, in the novel Jade and David reunite and live together in Vermont for several months before he is re-arrested. Then David is held in psychiatric hospitals for several years, during which time Jade marries and moves to Europe and David has sexual relationships with several other women. At the end of the novel, it is 1977 and David is released and living with an unnamed woman while Jade remains married and in Europe.

==Soundtrack==
 
 
The soundtrack peaked at #9 on the Billboard Top 200 and was certified platinum. It also featured a second duet between Ross and Richie, "Dreaming of You," that received considerable airplay but was never released as a single.

==Reception==
  
The film was panned by numerous critics for poorly handling the source material as well as glorifying what audiences saw as an unhealthy, damaging relationship. Movie historian Leonard Maltin called it "a textbook example of how to do everything wrong in a literary adaptation ... Scott Spencers deservedly-praised novel is thoroughly trashed." 

In 2014, Scott Spencer, the author of the novel on which the film was based, wrote, "I was frankly surprised that something so tepid and conventional could have been fashioned from my slightly unhinged novel about the glorious destructive violence of erotic obsession".  In 2014, Spencer described the film as a "botched" job and wrote that Franco Zeffirelli "egregiously and ridiculously misunderstood" the novel. 

 
Endless Love was a financial box office success.  It sold 11.2 million tickets in the U.S. alone (the 22nd highest grossing film of 1981).

===Awards and nominations===
  
: Winner: 1981 ASCAP Award, Lionel Richie, Endless Love
: Nominee: 1982 Academy Award for Best Song, Lionel Richie, Endless Love
: Winner: 1982 American Movie Award, Marquee Award, Lionel Richie, Endless Love
: Nominee: 1982 Golden Globe Award, Best Original Song - Motion Picture, Endless Love
* Young Artist Awards
: Nominee: Best Young Motion Picture Actor - Martin Hewitt
: Nominee: Best Young Motion Picture Actress - Brooke Shields
* 1981 Golden Raspberry Awards
: Nominee: Worst Actress - Brooke Shields
: Nominee: Worst Director - Franco Zeffirelli
: Nominee: Worst New Star - Martin Hewitt
: Nominee: Worst Picture - Dyson Lovell
: Nominee: Worst Screenplay - Judith Rascoe
: Nominee: Worst Supporting Actress - Shirley Knight
American Film Institute
* AFIs 100 Years...100 Passions - Nominated 
* AFIs 100 Years...100 Songs:
** Endless Love (song)|"Endless Love" (song)" – Nominated 

==Home media==
The film was released in the United States on DVD (Region 1) for the first time on May 27, 2014.  The film is also available from video streaming services such as Amazon Instant Video. 

==Remake==
 
In August 2012, Universal Pictures announced plans for the remake of the 1981 film. Country Strong  Shana Feste is slated to direct the remake.  Alex Pettyfer and Gabriella Wilde have the roles played by Hewitt and Shields in the original. 

==See also== The Delinquents The Promise

==References==
 

==External links==
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 