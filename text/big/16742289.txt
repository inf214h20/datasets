On the Silver Globe (film)
{{Infobox film
  | name = On the Silver Globe
  | image          = Na srebrnym globie 1988 polish poster.jpg
  | image_size     = 
  | caption        = 
  | director       = Andrzej Żuławski	
  | producer       = 
  | writer         = Andrzej Żuławski Jerzy Żuławski
  | starring       = Andrzej Seweryn Jerzy Trela Iwona Bielska Grażyna Dyląg
  | music          = Andrzej Korzyński
  | cinematography = Andrzej Jaroszewicz
  | editing        = 
  | distributor    =  Cannes Film Festival); February 1989 (Poland)
  | runtime        = 157 minutes
  | country        = Poland
  | language       = Polish 
  | budget         = }}
 Polish film premiered in 1988, directed by Andrzej Żuławski and adapted from a novel by Jerzy Żuławski.

==Plot==
 
A group of astronauts leaves Earth to find freedom, and their spaceship crashes on an unnamed Earth-like planet. The astronauts, equipped with video-recorders, reach a seashore, where they build a village. After many years, only one member of the crew, Jerzy, is still alive, watching the growth of a new society, whose religion is based on mythical tales of an expedition from the Earth. The first off-Earth generation calls him the "Old Man", treating him as a demi-God. The Old Man leaves them and before his death sends his video diary back to Earth in a rocket. A space researcher named Marek (Andrzej Seweryn) receives the video diary and travels to the planet. When he arrives, he is welcomed by the cast of priests as the Messiah, who can release them from the captivity of the Szerns, indigenous occupants of the planet. Shortly afterwards, Marek organizes an army and enters the city of the Szerns. Meanwhile, the priests start to believe that Marek was an outcast from the Earth, rather than a messiah who came to fulfill the religious prophecy.

==Cast==
* Andrzej Seweryn as Marek
* Jerzy Trela
* Grazyna Dylag
* Waldemar Kownacki
* Iwona Bielska
* Jerzy Gralek
* Elzbieta Karkoszka
* Krystyna Janda as The Actress
* Maciej Góraj
* Henryk Talar
* Leszek Dlugosz
* Andrzej Frycz
* Henryk Bista
* Wieslaw Komasa
* Jerzy Golinski

==Production==
The novel On the Silver Globe on which the film is based was written around 1900 as part of The Lunar Trilogy by Jerzy Żuławski, who was the granduncle of Andrzej Żuławski. Andrzej Żuławski had left his native Poland for France in 1972 to avoid censorship by the Polish government. Thanks to his critical success with the 1975 film Limportant cest daimer, the Polish authorities in charge of cultural affairs reevaluated their assessment of Żuławski. He was then invited to return to Poland and to realize a project of his own choice. Żuławski, who had always wanted to make a film of his grand uncles novel, saw the offer as a unique opportunity to achieve this aim.
 Baltic seashore Lower Silesia, the Wieliczka Salt Mine, the Tatra Mountains, the Caucasus mountains in Georgia (country)|Georgia, the Crimea in the Ukraine and the Gobi Desert in Mongolia. {{cite web
  | last = Żuławski
  | first = Andrzej 
  | authorlink = 
  | coauthors = 
  | title = Na Srebrnym Globie (1987)
  | work = 
  | publisher = 
  | date = 
  | url = http://www.andrzej-zulawski.com/NaSrebrnymGlobie.html
  | doi = 
  | accessdate = 2008-04-02}}
  In fall 1977 the project came to a sudden halt with the appointment of Janusz Wilhelmi as the vice-minister of cultural affairs. He perceived the battle between the Selenites and the Szerns in the film as a thinly-veiled allegory of the Polish peoples struggle with totalitarianism. Consequently Wilhelmi shut down the film project, which was eighty percent complete, and ordered all materials destroyed.

Żuławski went back to France, saying that he was in despair over the loss and waste of so much artistic effort.  The reels of the unfinished film were ultimately not destroyed, but preserved, along with costumes and props, by the film studio and by members of the cast and crew. Although Wilhelmi died a few months later in a plane crash the film was only released after the end of communist rule. In May 1988 a version of the film, consisting of the preserved footage plus a commentary to fill in the narrative gaps, premiered at the 1988 Cannes Film Festival.   

==References==
 

==External links==
* 
*  on the  
*   

 

 
 
 
 
 
 
 
 