Woman to Woman (1929 film)
{{infobox film
| name           = Woman to Woman (1929 film)
| image          = 
| caption        = 
| director       = Victor Saville
| producer       =  Woman to Michael Morton
| writer         = 
| starring       = Betty Compson
| music          = 
| cinematography = 
| editing        =
| studio         = 
| distributor    = Woolf & Freedman Film Service (UK) Tiffany Pictures (US)
| released       = 1 November 1929
| runtime        = 9 reels (2460 meters)
| country        = United Kingdom Silent (English intertitles)
}} British drama Woman to Michael Morton previously been made into a film in 1923.

==Plot==
During the First World War, a British officer on leave from the trenches in Paris falls in love with and has a liaison with a French woman.
 shellshock and forgets her. She turns up, years later, with his illegitimate child to find that he is now married to a British woman. 

==Cast==
* Betty Compson – Deloryce / Lola
* George Barraud – David Compton
* Juliette Compton – Vesta Compton Margaret Chambers – Florence
* Reginald Sharland – Hal
* Georgie Billings – Davey Compton
* Winter Hall – Doctor Gavron

==References==
 

==Bibliography==
* Cook, Pam (ed.). Gainsborough Pictures. Cassell, 1997.

==External links==
* 
* 
* 
* 

 

 
 
 
 
 
 
 


 