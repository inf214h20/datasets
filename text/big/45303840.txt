Bhoolokadalli Yamaraja
{{Infobox film 
| name           =  	Bhoolokadalli Yamaraja
| image          =  
| caption        = 
| director       = Siddalingaiah
| producer       = N. Veeraswamy S. P. Varadaraj Siddalingaiah J. Chandulal Jain
| story          = Basumani
| writer         = N. S. Rao (dialogues)
| screenplay     = M. D. Sundar
| starring       = Lokesh M. P. Shankar Jai Jagadish Mohan
| music          = C. Ashwath
| cinematography = V. K. Kannan
| editing        = P. Bhakthavathsalam
| studio         = Jain Combines
| distributor    = Jain Combines
| released       =  
| runtime        = 128 min
| country        = India Kannada
}}
 1979 Cinema Indian Kannada Kannada film, directed by Siddalingaiah and produced by N. Veeraswamy, S. P. Varadaraj, Siddalingaiah and J. Chandulal Jain. The film stars Lokesh, M. P. Shankar, Jai Jagadish and Mohan in lead roles. The film had musical score by C. Ashwath.  

==Cast==
 
*Lokesh
*M. P. Shankar
*Jai Jagadish
*Mohan
*T. Thimmayya
*Uday Jadugar
*Anjali
*Vijayalalitha
*Kaminidharan
*Manjulamma
*Vani
*Chandrika
*G. V. Lakshmiprabha
*Shanthala
*Sharapanjara Iyengar
*Camedian Guggu
*Prasanna
*Surendranath
*Chandulal Jain
*A. L. Srinivasamurthy
*Kannada Raju
*Pranaya Murthy
*T. N. Balakrishna in Guest Appearance
*C. H. Loknath in Guest Appearance
*N. S. Rao in Guest Appearance
 

==Soundtrack==
The music was composed by C. Ashwath. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Endu Kaanada Belaka Kande || S. P. Balasubrahmanyam, Vani Jayaram || Dodda Range Gowda || 04.20
|-
| 2 || Ninna Myaage || S. P. Balasubrahmanyam, S. Janaki|Janaki, Vani Jayaram || Dodda Range Gowda || 03.45
|- Janaki || Bhangi Ranga || 04.17
|- Janaki || Dodda Range Gowda || 05.38
|}

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 