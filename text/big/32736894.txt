In Desert and Wilderness (2001 film)
{{Infobox film
| name           = In Desert and Wilderness
| image          = W pustyni i w puszczy 2001.jpg| image_size     = 
| caption        = Movie poster
| director       = Gavin Hood 
| producer       = Waldemar Dziki, Włodzimierz Otulak
| writer         = Gavin Hood, Henryk Sienkiewicz (novel)
| narrator       =
| starring       = Karolina Sawka Adam Fidusiewicz Mzwandile Ngubeni Lungile Shongwe
| music          = Krzesimir Dębski
| cinematography = Jacek Januszyk
| editing        =
| distributor    =
| released       = 2001
| runtime        = 111 minutes
| country        = Poland
| language       = Polish 
| budget         = 4.2 mln. $
}}
In Desert and Wilderness ( ) is a 2001 Polish film directed by Gavin Hood.  Adapted from the 1911 novel In Desert and Wilderness by  Henryk Sienkiewicz,  it tells the story of two kids, Staś Tarkowski and Nel Rawlison, kidnapped by the rebels during Muhammad Ahmad|Mahdis rebellion in Sudan.

Filming took three months. It was filmed in South Africa and Tunisia. The original director fell ill at the very beginning of filming and his role was taken by Hood. A miniseries were made at the same time this time as well (In Desert and Wilderness (2001 TV series)).  The film was said to be more modern, notably in its depiction of intercultural relationships. Although its not that close to the book, it still enjoyed a considerable success.

== Cast ==

*   .... Nel Rawlison
* Adam Fidusiewicz .... Staś Tarkowski Artur Żmijewski .... Władysław Tarkowski (Staśs father)
* Andrzej Strzelecki .... George Rawlinson (Nels father)
* Mzwandile Ngubeni .... Kali
* Lungile Shongwe .... Mea
* Konrad Imiela .... Chamis
* Lotfi Dziri .... Gebhr
* Ahmed Hafiane .... Idrys
* Krzysztof Kowalewski .... Kalioupoli
* Krzysztof Kolberger .... Linde
* Agnieszka Pilaszewska .... Madame Olivier Mahdi
and others.

== References ==
 

== External links ==
* 

 

 
 
 
 
 
 

 