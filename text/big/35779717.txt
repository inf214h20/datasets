Geometria (film)
{{Infobox film
| name           = Geometria
| director       = Guillermo del Toro
| producer       =  
| screenplay     = Guillermo del Toro
| based on       =  
| starring       =  
| music          = Christopher Drake
| cinematography = Juan Carlos Muñez
| editing        =  
| studio         = 
| distributor    = 
| released       =  
| runtime        =    |   }}
| country        = Mexico
| language       = Spanish
| budget         = $1,000
| gross          = 
}} short fantasy fantasy horror horror comedy comedy film written and directed by Guillermo del Toro. It is based loosely on Fredric Browns short story, Naturally, which was originally published in Beyond Fantasy Fiction and later reprinted in the short story collection Honeymoon in Hell.  Geometria was shot in Guadalajara, Jalisco in Mexico. It is the tenth short film del Toro directed, though all but 1985s Doña Lupe remain unreleased.

Del Toro was not satisfied with the original cut of the film, and said that he was not able to finish it the way he wanted to at the time.  A directors cut of the film, slightly shorter than the 1987 cut, was included on The Criterion Collections 2010 release of del Toros 1993 feature film debut, Cronos (film)|Cronos.   

==Plot==
A Mexican widow (Guadalupe del Toro) receives a letter from the high school attended by her son (Fernando Garcia Marin). It informs her that the boy is about to fail his geometry exams for the third time. The woman berates her son, then turns on the television, refusing to speak to him. The boy leaves the room, vowing that he will never fail geometry again.

The boy resorts to using black magic in order to pass the exam. In a dark room, he reads from a tome of sorcery, which states, "As a protection for the invocation of a major demon, place yourself inside a pentagon drawn with your own blood. This pentagon will be your only protection." The boy proceeds to follow these instructions.
 The Exorcist, she hears her son screaming from the next room. When she enters, she finds him standing in the middle of the bloody seal. He shouts a warning, telling her that the pentagon is his only protection. A glowing portal opens in the wall, and a demon (Rodrigo Mora) steps through. The boy asks the demon to grant two wishes. The first is that he will not fail geometry again. The second is the return of his father, Francisco, who died in an accident three months ago. The demon complies with the latter, causing Francisco to materialise immediately. However, he is now a mindless Zombie (fictional)|zombie. Francisco kills and eats his wife, while their son looks on in horror, unable to step out of the pentagon for fear of losing its magical protection.

The demon commands the boy to surrender, but he refuses to give in, saying that he cannot be harmed while he stands within the pentagon. The demon reasons that he has already granted one of the boys two wishes: his family is together again. He also points out that what the boy has drawn is not a pentagon, but a hexagon, which offers no magical protection whatsoever. The demon muses that the boys other wish has also been granted: he will never fail geometry again. Francisco approaches his son from behind and takes hold of his head, tilting it backwards. The boy laments the unfairness of his situation. The demon agrees, and reaches out a hand to take hold of his throat. The screen fades to black as ripping sounds are heard.

 

==External links==
*  
*  
*  

==References==
 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 