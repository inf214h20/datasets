Bob's Baby
 
{{Infobox film
| name           = Bobs Baby
| image          =
| caption        =
| director       =
| producer       =
| writer         =
| narrator       =
| starring       = Jean Acker
| cinematography =
| editing        =
| distributor    =
| released       =  
| runtime        =
| country        = United States Silent
| budget         =
| website        =
}}
 1913 Comedy film

==Plot==
Bob, a typically devoted husband, is told by his wife that the stork has paid a visit to their household; the first time, it turns out to be a puppy; the second time, expecting another canine, he is surprised to find the more traditional offspring.

==Cast==
{| class="wikitable"
|- 
! Actor/Actress || Role
|- Glen White || Robert Waring  
|-
| Violet Horner || Mrs. Robert Waring    
|-
| Irene Wallace || The Maid    
|-
| William Sorelle || Bobs Uncle (as William Sorrell)
|-
| Louise Mackin || Bobs Aunt (as Mrs. Mackin) 
|-
| Jean Acker || Bobs Cousin     
|}

 
 
 
 
 
 
 


 