The Stepfather (1987 film)
{{Infobox film
| name           = The Stepfather
| image          = Stepfather 1987.jpg
| caption        = Film poster
| director       = Joseph Ruben
| producer       = Jay Benson
| screenplay     = Donald E. Westlake
| story          = Carolyn Lefcourt Brian Garfield Donald E. Westlake
| starring       = Terry OQuinn Jill Schoelen Shelley Hack
| music          = Patrick Moraz
| cinematography = John W. Lindley George Bowers
| distributor    = New Century Vista Film Company ITC Entertainment
| released       =  
| runtime        = 89 minutes
| language       = English
| country        = United States
| budget         =
| gross          = $2,488,740 
}} thriller slasher slasher starring directed by written by Donald E. Westlake, from a story by Westlake, Carolyn Lefcourt and Brian Garfield.
 1989 and The Stepfather, which was released on October 16, 2009.

== Plot ==
The film opens with Henry Morrison washing off blood, in a bathroom, before changing his appearance and putting a few of his belongings into a suitcase. After packing his things, Henry leaves through the front door of his house, nonchalantly passing the butchered remains of his family and others. Boarding a ferry, Henry throws the suitcase containing the objects from his former life into the ocean. One year later, Henry — now operating as a real estate agent named Jerry Blake — has married the widow Susan Maine. Jerrys relationship with Susans 16-year-old daughter, Stephanie, is strained. Her psychiatrist, Doctor Bondurant, advises her to give Jerry a chance.

Meanwhile, amateur detective Jim Ogilvie, the brother of Jerrys murdered wife, runs an article about his sisters slayer in the newspaper. While hosting a neighborhood barbecue, Jerry discovers the article and is disturbed by it. Jerry goes into the basement of the house and begins maniacally rambling to himself (possibly recalling memories of his unhappy childhood), unaware that Stephanie is also in the basement. Discovering his stepdaughter, Jerry brushes off his outbursts by saying he was simply letting off steam. Stephanie finds the newspaper mentioning Jerrys earlier killings and comes to believe her stepfather is the murderer mentioned in the article. She writes a letter to the newspaper requesting a photo of Henry Morrison, but Jerry finds the photo in the mail and replaces it with another, allaying her suspicions.

Curious about Stephanies stepfather, Dr. Bondurant makes an appointment with Jerry under an assumed name, saying he wants to buy a house. During their meeting, Jerry realizes that Bondurant is not who he says he is, beats Bondurant to death, and fakes a car accident. The next day, Jerry sympathetically informs Stephanie of Bondurants death and succeeds in bonding with her. Jerrys newfound relationship with his stepdaughter is quickly cut short when he catches Stephanie kissing her boyfriend. Jerry accuses Paul of attempting to rape Stephanie, which causes an argument with Stephanie and Susan, and drives the boyfriend away. Thinking that all hope of having a happy life with Susan and Stephanie has been ruined, Jerry quits his job and creates a new identity for himself in another town. He begins to court another widow, while planning to get rid of Susan and Stephanie.

Having discovered where Jerry is now living, Jim Ogilvie begins going door to door, in search of his former brother-in-law. After Jim stops by, Susan phones the real estate agency to tell Jerry that someone was looking for him, only to be informed that Jerry quit several days ago. Susan confronts Jerry, but, while explaining himself to Susan, Jerry confuses his identities, and Susan realizes that Stephanie was right about Jerry. Jerry bashes Susan with the phone and knocks her down the basement stairs. Content that Susan is dead, Jerry then sets out to kill Stephanie. Jerry first kills Jim, who shows up again at the house. After terrorizing Stephanie, he corners her in the attic, only to fall through the weak floor. Jerry recovers and renews his attack, but Susan shoots him, using Jims revolver. Ultimately, he is stabbed by Stephanie, after which he weakly utters, "I love you."

The film ends with Stephanie cutting down the birdhouse she and Jerry had put up during the brief time they had bonded with one another.

== Cast ==
*Terry OQuinn as Jerry Blake/Henry Morrison/Bill Hodgkins/The Stepfather
*Jill Schoelen as Stephanie Maine
*Shelley Hack as Susan Maine
*Charles Lanyer as Dr. A. Bondurant
*Stephen Shellen as Jim Ogilvie

== Release ==
During its opening weekend, The Stepfather grossed around $260,587; it was released in 148 movie theatres and earned a total domestic gross of $2,488,740.   

== Reception ==
The Stepfather has an 86% approval rating from Rotten Tomatoes, with an average rating of 6.7/10, out of 29 reviews. Film critic Roger Ebert, from the Chicago Sun Times, gave the movie 2.5 stars out of 4, and commented:   }}

On "Combustible Celluloid", the movie ranked 3 out of 4 stars, with reviewer Jeffrey M. Anderson commenting:  
 Saturn and Ruben was Cognac Festival.  The film was also nominated for the International Fantasy Film Award for Best Film at the 1990 Fantasporto  and included in Bravo (US TV channel)|Bravos 100 Scariest Movie Moments on spot #70. 

It is also now considered a cult film. 

==Home media==
The film was released on DVD for the first time in North America by Shout! Factory on October 13, 2009.    Shout! Factory released the first-ever Blu-ray version of the film on June 15, 2010.

==Related films==

===Sequels===
The film was followed by a sequel, Stepfather II, in 1989. Another sequel, Stepfather III, was released in 1992.

===Remake=== The Stepfather was released in 2009, to negative reviews.

==References==
 

==External links==
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 