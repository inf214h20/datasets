List of horror films of 1973
 
 
A list of horror films released in 1973 in film|1973.

{| class="wikitable sortable" 1973
|-
! scope="col" | Title
! scope="col" | Director
! scope="col" class="unsortable"| Cast
! scope="col" | Country
! scope="col" class="unsortable"| Notes
|-
!   | Alabamas Ghost
| Fredric Hobbs || Christopher Brooks ||   ||  
|-
!   | And Now the Screaming Starts! Patrick Magee ||   ||  
|- At the Meeting with Joyous Death
| Juan Buñuel || Jean-Marc Bory, Françoise Fabian, Yasmine Dahm ||    ||  
|- The Baby
| Ted Post || Marianna Hill, Tod Andrews, Anjanette Comer ||   ||  
|-
!   | La campana del infierno
| Claudio Guerin Hill || Christine Betzner ||   ||  
|-
!   | Blackenstein John Hart ||   ||  
|-
!   | Blood for Dracula
| Paul Morrissey || Joe Dallesandro, Udo Kier, Vittorio De Sica ||   ||  
|- Blue Eyes of the Broken Doll
| Carlos Aured || Maria Perschy, Paul Naschy, Eva Leon ||   ||  
|- The Boy Who Cried Werewolf
| Nathan Juran || Paul R. Baxley, Jr., Dave Cass ||   ||  
|-
!   | Cannibal Girls
| Ivan Reitman || Bonnie Neilson, Eugene Levy, Andrea Martin ||   ||  
|-
!   | Children Shouldnt Play With Dead Things
| Bob Clark || Alecs Baird, Paul Cronin ||   ||  
|- The Crazies
| George A. Romero || Edith Bell, Lane Carroll, Will Disney ||   ||  
|-
!   | Crypt of the Living Dead
| Ray Danton, Julio Salvador || Andrew Prine, Patty Shepard, Mark Damon ||   ||  
|- Dark Places
| Don Sharp || Christopher Lee, Joan Collins, Robert Hardy ||   ||  
|-
!   | Dr. Death: Seeker of Souls John Considine, Sivi Aberg, Leon Askin ||   ||  
|- The Exorcist
| William Friedkin || Ellen Burstyn, Linda Blair, Max von Sydow ||   ||  
|-
!   | Female Vampire
| Jesus Franco || Lina Romay, Monica Swinn, Luis Barboo ||    ||  
|-
!   | Flesh for Frankenstein
| Paul Morrissey || Joe Dallesandro, Udo Kier, Monique Van Vooren ||    ||  
|- The Forgotten
| S.F. Brownrigg || Bill McGhee, Jessie Lee Fulton, Robert Dracup ||   ||  
|-
!   | From Beyond the Grave Kevin Connor David Warner, Nyree Dawn Porter, Donald Pleasence ||   ||  
|-
!   | Ganja & Hess Bill Gunn || Tara Fields, Enrico Fales ||   ||  
|-
!   | Godmonster of Indian Flats
| Fredric Hobbs || Peggy Browne, Robert Hirschfeld, Karen Ingenthron ||   ||  
|- The Graveyard
| Dan Chaffey || Lana Turner, Olga Georges-Picot, Shelagh Fraser ||   ||  
|-
!   | The Hanging Woman
| José Luis Merino || Paul Naschy, Stelvio Riso ||    ||  
|-
!   | The Hidan of Maukbeiangjow Lee Jones || David Roster, Paul Lenzi, Harlo Cayse ||   ||  
|-
!   | Horror Hospital
| Antony Balch || Robin Askwith, Ellen Pollock, Martin Grace ||   ||  
|-
!   | The Iron Rose
| Jean Rollin || Françoise Pascal, Hugues Quester, Mireille Dargent ||   ||  
|-
!   | The Legend of Hell House John Hough || Pamela Franklin, Roddy McDowall, Clive Revill ||   ||  
|-
!   | Lemora
| Richard Blackburn || Cheryl Smith ||   ||  
|-
!   | Leptirica
| Djordje Kadijević || Tanasije Uzunović, Mirjana Nikolić, Vasja Stanković ||   ||  
|-
!   | Malatestas Carnival of Blood
| || Lenny Baker, Jerome Dempsey ||   ||  
|-
!   | Mark of the Devil Part II
| Adrian Hoven || Erika Blanc, Anton Diffring ||   ||  
|-
!   | Messiah of Evil
| Gloria Katz, Willard Huyck || Dyanne Asimow, Marianna Hill, Joy Bang ||   ||  
|-
!   | A Name for Evil
| Bernard Girard || Robert Culp, Samantha Eggar ||   ||  
|- Night of the Sorcerers
| Amando De Ossorio || Simon Andreu, Kali Hansa ||   ||  
|-
!   | The Pyx
| Harvey Hart || Karen Black, Christopher Plummer ||   || {{hidden|fw1=normal|multiline=y
|1=Alternative titles
|2=Also known as The Hooker Cult Murders.}} 
|-
!   | Return of the Blind Dead
| Amando De Ossorio || Frank Blake, Jose Canalejas ||    || {{hidden|fw1=normal|multiline=y
|1=Alternative titles
|2=Also known as Return of the Evil Dead}} 
|- The Return of Walpurgis
| Carlos Aured || Paul Naschy, Eduardo Calvo, Patty Shepard ||    ||  {{hidden|fw1=normal|multiline=y
|1=Alternative titles
|2=Also known as Curse of the Devil}}
|-
!   | Satans School for Girls (1973 film)|Satans School for Girls
| David Lowell Rich || Pamela Franklin, Gwynne Gilford, Jamie Smith Jackson ||   ||  
|-
!   | The Satanic Rites of Dracula Alan Gibson || Christopher Lee, Peter Cushing, Joanna Lumley ||   ||  
|-
!   | Schlock (film)|Schlock
| John Landis || Joseph Piantadosi, Eliza Roberts, Saul Kahan ||   ||  
|-
!   | Scream Blacula Scream William Marshall, Pam Grier ||   ||  
|-
!   | Seven Deaths in the Cats Eye
| Anthony M. Dawson || Jane Birkin, Françoise Christophe, Venantino Venantini ||    ||  
|-
!   | Sisters (1973 film)|Sisters
| Brian De Palma || Margot Kidder, Jennifer Salt, Charles Durning ||   ||  
|-
!   | Sssssss
| Bernard Kowalski || Ray Ballard, Rick Beckner ||   ||  
|-
!   | Theatre of Blood
| Douglas Hickox || Vincent Price, Diana Rigg ||   ||  
|-
!   | Torso (1973 film)|Torso
| Sergio Martino || Suzy Kendall, Tina Aumont ||   || {{hidden|fw1=normal|multiline=y
|1=Alternative title
|2=Also known as The Bodies Bear Traces of Carnal Violence}} 
|-
!   | Vampires Night Orgy Jack Taylor, Dianik Zurakowska ||    ||  
|- Vault of Horror
| Roy Ward Baker || Terry-Thomas, Denholm Elliott, Tom Baker ||   ||  
|-
!   | A Virgin Among the Living Dead Paul Müller, Brit Nichols ||       ||  
|-
!   | Voodoo Black Exorcist
| Manuel Cano || Eva Leon, Aldo Sambrell || ||  
|-
!   | The Werewolf of Washington Nancy Andrews, Dean Stockwell, Despo Diamantidou ||   ||  
|- The Wicker Man Robin Hardy || Edward Woodward, Britt Ekland, Diane Cilento ||   ||  
|}

==References==
 

==Citations==
 
*  <!--
-->
* {{cite book |last=Young |first=R. G. |year=2000 |title=The Encyclopedia of Fantastic Film: Ali Baba to Zombies |url=http://books.google.com/books?id=QoJ4jTghUPYC |accessdate=June 29, 2011 |publisher=Hal Leonard Corporation |isbn=1-55783-269-2 |ref=harv
}}<!--
-->
 

 
 
 

 
 
 
 