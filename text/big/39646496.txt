Wibbel the Tailor (1920 film)
{{Infobox film
| name           = Wibbel the Tailor
| image          =
| caption        =
| director       = Manfred Noa
| producer       = Franz Vogel
| writer         = Hans Müller-Schlösser (play) Georg Jacoby Léo Lasko
| starring       = Hermann Picha Margarete Kupfer Meinhart Maur Wilhelm Diegelmann
| editing        =
| cinematography = Paul Adler
| studio         = Eiko Film
| distributor    = Eiko Film
| released       =  
| runtime        =
| country        = Germany
| language       = Silent German intertitles
| budget         =
| gross          =
}} German silent silent comedy Wibbel the Tailor by Hans Müller-Schlösser. It was made by Eiko Film in Berlin.  The films art direction is by Karl Machus.

==Cast==
* Hermann Picha as Schneider WIbbel 
* Margarete Kupfer as Wibbels Frau 
* Meinhart Maur as Gefängnisschliesser 
* Gustav Trautschold as Gehilfen
* Wilhelm Diegelmann   
* Christian Elfeld   
* Loo Hardy   
* Emil Stammer  
 
==References==
 

==Bibliography==
* Usai, Paolo Cherchi. Before Caligari: German cinema, 1895-1920. University of Wisconsin Press, 1991.

==External links==
* 

 
 
 
 
 
 
 
 
 
 


 
 