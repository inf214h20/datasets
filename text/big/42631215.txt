The Cavalier from Kruja
{{Infobox film
| name =  The Cavalier from Kruja
| image =
| image_size =
| caption =
| director = Carlo Campogalliani
| producer =  Liborio Capitani   Carlo Malatesta   Gian Gaspare Napolitano   Alberto Spaini    Aldo Vergano
| narrator =
| starring = Doris Duranti   Antonio Centa   Leda Gloria   Guido Celano  Guido Celano  
| music = Alberto Ghislanzoni       Giorgio Orsini   Aldo Tonti 
| editing = Mario Bonotti      
| studio = Produzione Capitani Film  ENIC
| released = 5 September 1940
| runtime = 78 minutes
| country = Italy Italian 
| budget =
| gross =
| preceded_by =
| followed_by =
| website = propaganda work Fascist regime of Benito Mussolini.

==Cast==
*    Doris Duranti as Eliana Haidar  
* Antonio Centa as Stafano Andriani  
* Leda Gloria as Alidjé 
* Guido Celano as Hasslan Haidar  
* Nico Pepe as Mario Marini 
* Giuseppe Rinaldi as Essad Haidar  
* Vasco Creti as Ilias Haidar  
* Arnaldo Arnaldi as Mirko 
* Oscar Andriani as Il capo della polizia  
* Dino Di Luca as Osman  
* Carlo Duse as Argiropulos  
* Katiuscia Odinzova as Miss Parker  
* Emilio Petacci as Loperaio italiano Ferrero 
* Walter Lazzaro as Magnani  
* Luigi Carini    
* Noëlle Norman

== References ==
 

== Bibliography ==
* Liehm, Mira. Passion and Defiance: Film in Italy from 1942 to the Present. University of California Press, 1984. 

== External links ==
*  

 

 
 
 
 
 
 
 
 
 
 
 