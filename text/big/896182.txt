Magic (1978 film)
 
{{Infobox film
| name = Magic
| image = Magicposter.jpg
| caption = Theatrical release poster
| image_size = 250px
| director = Richard Attenborough
| producer = Joseph E. Levine Richard P. Levine
| screenplay = William Goldman
| based on =  
| starring = Anthony Hopkins Ann-Margret Burgess Meredith Ed Lauter David Ogden Stiers
| music = Jerry Goldsmith
| cinematography = Victor J. Kemper John Bloom
| distributor = 20th Century Fox
| released =  
| runtime = 107 min.
| country = United States
| language = English
| budget = $7,000,000
| gross = $23,800,000 
}} psychological horror novel upon which it was based.

== Plot == magician and dummy named Fats and is a huge success.

His powerful agent Ben Greene is on the verge of signing Corky for his own television show, but Corky bails out for the Catskills, where he grew up, claiming to be "afraid of success." In truth, he does not want to take the TV networks required medical examination because doctors might find out that he suffers from severe issues, and that even off-stage he cannot control Fats (a manifestation of Corkys id).

In the Catskills, he reunites with his high-school Puppy love|crush, Peggy Ann Snow, who is stuck in a passionless marriage with Corkys friend from high school, Duke. A magic trick with a deck of cards charms Peg into thinking they are soulmates. She and Corky make sex|love, which sparks the jealousy not only of Peggys tough-guy husband but also the dummy Fats.

Greene arrives unexpectedly, having tracked Corky down. After a tense confrontation where Greene discovers the truth about Corkys mental state, the agent demands that Corky get help.
 drown Greene.

The next morning, Fats becomes even more possessive and jealous when Corky says that he plans to leave Fats behind so that he and Peggy can honeymoon by themselves.

Duke returns from his trip earlier than expected. He suspects his wife cheated on him and wants to have a talk with Corky on the lake. Rather than confront him, Duke awkwardly confides to Corky that he loves Peggy and is worried about losing her.

Duke suddenly spots a dead body on the edge of the lake. They row toward it. Duke, believing it could still be alive, sends Corky to get help. Duke finds that the man is indeed dead. Curious, he decides to search Corkys cabin.

Fats kills him with "help" from Corky. (The dummy stabs Duke while Corky is covered by a curtain behind him.)
 seduce women, and that Peg is only the latest of his conquests. Repulsed, she rejects Corky and locks herself in her bedroom.

Fats says that, from this point on, he will make the decisions in Corkys life. He immediately asserts this new authority by ordering Corky to kill Peg.

Corky, turning on the charm and using Fats voice, apologizes to Peggy from in front of her locked door. A short while later, Corky returns with a bloodstained knife, Fats seems pleased — until it is revealed that the blood on the knife is Corkys, having committed suicide so that he wont kill anyone else. As a result Fats also feels faint. They wonder which of them will die first.

Moments later, Peggy returns to their cabin, happily calling out that she has changed her mind and has decided to run away with Corky after all.  As she speaks, her voice changes into a caricature that sounds like a female Fats.

== Cast ==
* Anthony Hopkins as Corky (and the voice of Fats)
* Ann-Margret as Peggy Ann Snow
* Burgess Meredith as Ben Greene
* Ed Lauter as Duke
* E. J. André as Merlin
* Jerry Houser as Taxi Driver
* David Ogden Stiers as Todson
* Lillian Randolph as Sadie

== Production notes ==
Joseph E. Levine bought the film rights to Goldmans novel for $1 million. This included Goldmans fee to write the screenplay. Levine Buys Film Rights To William Goldman Novel
New York Times (1923-Current file)   03 Mar 1976: 27.  

The first draft was written for director Norman Jewison.  MOVIE CALL SHEET: Knievel to Star as Himself
Kilday, Gregg. Los Angeles Times (1923-Current File)   12 June 1976: c7.  Jewison wanted Jack Nicholson to star but Nicholson turned it down, claiming he did not want to wear a hairpiece. Magic: Fats and Friends (2006) Dir: David Gregory, video short  Richard Attenborough, who had just made A Bridge Too Far with Goldman and Levine, then agreed to direct.

Gene Wilder was the original choice for Corky, and director Richard Attenborough and screenwriter William Goldman wanted him, but producer Joseph E. Levine refused on grounds he wanted no comedians in the movie to distract from the serious nature of the story. 

Laurence Olivier was originally offered the role of the agent but was unable to do it so Burgess Meredith was cast instead. 

Goldman later wrote about the film that "Burgess Meredith was perfect and Tony Hopkins... was so wonderful here. But running stride for stride with him was Miss Olsson. I think Ann-Margret is the least appreciated emotional actress anywhere." 

Ann-Margret and Anthony Hopkins were each paid around $300,000 for their performances. European filmgoers are holding up Bridge
Beck, Marilyn. Chicago Tribune (1963-Current file)   20 Oct 1977: a8.  

Filming took place in Ukiah, California. Tempo People
Gold, Aaron. Chicago Tribune (1963-Current file)   15 Dec 1977: a2.  

== Reception ==
The film received positive reviews from critics. It received a "certified fresh" 83% on Rotten Tomatoes. Vincent Canby for the New York Times wrote that "Magic is neither eerie nor effective. It is, however, very heavy of hand." 
 Siskel & Ebert, gave the film a very positive review, and ranked it at #9 on his list of the 10 best films of 1978.

The Science Fiction, Horror and Fantasy Film Review 1990 writeup of the film remarks that Hopkins appears stiff in the lead role, but praised the supporting cast: "Ann-Margaret...invests her role with a considerable sparkle. Particularly good is the great and underrated Burgess Meredith whose sharp and alert Hollywood agent is a real plum of a performance. Jerry Goldsmith also adds a fine nervy carnivalesque score."   

Goldman received a 1979 Edgar Award, from the Mystery Writers of America, for Best Motion Picture Screenplay. Hopkins received each a Golden Globe and BAFTA nomination for his role as the tragically disturbed Corky.
 trailer for this film was pulled from TV due to calls from parents who claimed that it gave their children nightmares.  The trailer in question is less than 30 seconds in length.  It features Fats reciting the tagline, after which his eyeballs roll into the back of his head. This is followed by a cast reading, then Fats opens his eyes and gazes to his left.

== Legal issues with the film == American Movie AMC Film Holdings, LLC, while TV rights are handled for syndication by Trifecta Entertainment & Media (under Paramount Pictures).  The uncut version is currently available on widescreen DVD and Blu-ray.

== Soundtrack == American composer Jerry Goldsmith.   The complete soundtrack was released on CD through Varèse Sarabande in April, 2003 and features twenty-two tracks score at a running time of forty-two minutes. 

== Cultural references ==
In 2010, the BBC Radio 4 satirical comedy series The Now Show claimed that Michael Gove looked like a scary ventriloquist puppet. As a result, whenever Gove is referenced, Hugh Dennis does an impression of Fats ordering "Govey" to do things.

== See also == one person living two personas through a ventriloquists dummy has been portrayed several times before in film and television, most notably: 
*The Great Gabbo a 1929 film 
*Dead of Night a 1945 British film Knock on Wood a 1954 film
*"The Dummy" a 1962 episode of The Twilight Zone
*"Caesar and Me" a 1964 episode of The Twilight Zone Devil Doll a 1964 film Saga partly influenced by Magic.

== References ==
 

== External links ==
*  

 
 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 