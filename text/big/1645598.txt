The Big Sleep (1946 film)
 
{{Infobox film
| name           = The Big Sleep
| image          = Bigsleep2.JPG
| image_size     =
| alt            =
| caption        = Theatrical release lobby card
| director       = Howard Hawks
| producer       = Howard Hawks
| screenplay     = {{Plain list | 
* William Faulkner
* Leigh Brackett
* Jules Furthman
}}
| based on       =  
| starring       = {{Plain list |
* Humphrey Bogart 
* Lauren Bacall
}}
| music          = Max Steiner
| cinematography = Sidney Hickox
| editing        = Christian Nyby
| distributor    = Warner Bros.
| released       =  
| runtime        = {{Plain list |
* 114 minutes   (released cut) 
* 116 minutes   (re-released original cut) 
}}
| country        = United States
| language       = English
| budget         =
| gross          = $3 million Thomas Schatz, Boom and Bust: American Cinema in the 1940s Uni of California Press, 1999 p 221 
}} novel of the same name. The movie stars Humphrey Bogart as detective Philip Marlowe and Lauren Bacall as  Vivian Rutledge in a story about the "process of a criminal investigation, not its results".   William Faulkner, Leigh Brackett, and Jules Furthman co-wrote the screenplay.

In 1997, the U.S. Library of Congress deemed the film "culturally, historically, or aesthetically significant", and added it to the National Film Registry.

==Plot==
Private detective Philip Marlowe (Bogart) is summoned to the mansion of his new client General Sternwood (Waldron). The wealthy retired general wants to resolve gambling debts his daughter, Carmen Sternwood (Vickers), owes to bookseller Arthur Gwynn Geiger. As Marlowe is leaving, General Sternwoods older daughter, Mrs. Vivian Rutledge (Bacall), stops him.  She suspects her fathers true motive for calling in a detective is to find his young friend Sean Regan, who had mysteriously disappeared a month earlier.

Marlowe goes to Geigers rare book shop. Agnes Louzier (Darrin), Geigers assistant, minds the shop, which is the front for an illegal operation. Marlowe follows Geiger to his house, where he hears a gunshot and a woman scream. Breaking in, he finds Geigers body and a drugged Carmen, as well as a hidden camera with an empty cartridge. Marlowe picks up Carmen and takes her home. He goes back to the house, but discovers the body is no longer there. Marlowe later learns that the Sternwood driver (Owen Taylor) has been found dead, with his car driven off a pier.

Vivian comes to Marlowes office the next morning with scandalous pictures of Carmen she received with a blackmail demand for the negatives. Marlowe returns to Geigers bookstore, where they are packing up the store. Marlowe follows a car to the apartment of Joe Brody (Heydt), a gambler who previously blackmailed General Sternwood. He returns to Geigers house and finds Carmen there. She initially claims ignorance about the murder, then insists Brody killed Geiger. They are interrupted by the owner of the home, small-time gangster Eddie Mars (Ridgely).

Marlowe follows Vivian to the apartment of Joe Brody, where he finds Brody armed, and Agnes and Vivian initially hiding. They are interrupted by Carmen, who wants her photos. Marlowe keeps the pictures and sends Vivian and Carmen home. Brody admits he was blackmailing both General Sternwood and Vivian, then he is suddenly shot and killed; the assailant flees. Marlowe chases and apprehends Carol Lundgren, Geigers former driver, who has killed Brody in revenge for Geigers death.

Marlowe visits Mars casino, where he asks about Regan, who is supposed to have run off with Mars wife. Mars is evasive and tells Marlowe that Vivian is running up gambling debts. Vivian wins a big wager and then wants Marlowe to take her home. A stooge of Mars attempts to rob Vivian, but Marlowe intervenes and knocks him out.

While driving home, Marlowe unsuccessfully presses Vivian on her connection with Mars, saying he knew the money she won and subsequent robbery was a setup by Mars and her. Vivian admits to nothing. Marlowe returns home to find a flirtatious Carmen waiting for him. She admits she didnt like Regan and mentions that Mars calls Vivian frequently. She attempts to seduce Marlowe, who throws her out of his apartment. In the morning, Vivian calls to say that Regan has been found in Mexico, and that she is going to see him.

Harry Jones (Cook), an associate of Brodys who apparently is now Agnes lover, conveys an offer from Agnes to reveal the location of Mars wife for $200. When Marlowe goes to meet Jones, Canino, a killer hired by Mars, is there attempting to find Agnes himself. Jones is poisoned by Canino after disclosing Agnes location (which turns out to be false).

Marlowe goes to meet Agnes after she telephones the office where Jones was killed. She reveals that shes seen Mona Mars near a town called Realito by an auto repair shop. When he gets there, Marlowe is attacked by Canino. He wakes to find himself tied up, with Mona watching over him. Vivian then comes in. Mona angrily leaves after Marlowe tells her that Mars is a gangster and a killer. Vivian fears for Marlowes life and frees him, allowing him to get to his car and his gun. She distracts Canino, who is shot by Marlowe. During the drive back to Geigers bungalow, Vivian unconvincingly tries to claim she killed Sean Regan.

When they arrive, Marlowe calls Mars and lies that he is still in Realito at a pay phone. They arrange to meet at Geigers house, giving Marlowe ten minutes to prepare. Mars arrives with four men, who set up ambush points outside. Mars enters the home and is surprised by Marlowe, who holds him at gunpoint. Marlowe reveals he has discerned the truth: Mars has been blackmailing Vivian, claiming that her sister Carmen had killed Regan. As soon as Mars threatens Marlowe with his men outside, Marlowe retaliates by firing shots that just miss Mars, causing him to run outside, where he is mistakenly shot by his own men.

Marlowe then calls the police, telling them that Mars is the one who killed Regan. In the process, he tells them that Vivian helped him with Eddie Mars, exempting her from criminal prosecution, and that her sister Carmen needs psychiatric care.

==Cast==
 
* Humphrey Bogart as Philip Marlowe
* Lauren Bacall as Vivian Sternwood Rutledge
* John Ridgely as Eddie Mars
* Martha Vickers as Carmen Sternwood
* Pat Clark as Mona Mars  
* Peggy Knudsen as Mona Mars  
* Regis Toomey as Chief Inspector Bernie Ohls
* Charles Waldron as General Sternwood
* Charles D. Brown as Norris Bob Steele as Lash Canino
* Elisha Cook, Jr. as Harry Jones
* Louis Jean Heydt as Joe Brody
* Dorothy Malone as Acme Bookstore proprietress
* Sonia Darrin as Agnes Lowzier, the salesgirl at A.J. Geiger bookstore
* Ben Welden as Pete, Mars flunky
* Tom Fadden as Sidney, Mars flunky
* Trevor Bardette as Art Huck
* Theodore von Eltz as Arthur Gwynn Geiger
* James Flavin as Captain Cronjager  
* Thomas E. Jackson as District Attorney Wilde  
* Dan Wallace as Carol Lundgren
* Joseph Crehan as Medical Examiner
* Joy Barlowe as the cab driver (uncredited)
 

==Production and release==
 
The Big Sleep is known for its convoluted plot. During filming, allegedly neither the director nor the screenwriters knew whether chauffeur Owen Taylor was murdered or had killed himself. They sent a cable to Chandler, who told a friend in a later letter: "They sent me a wire ... asking me, and dammit I didnt know either". 

After its completion, Warner Bros. did not release The Big Sleep until they had turned out a backlog of war-related films. Because the war was ending, the studio feared the public might lose interest in the films, while The Big Sleeps subject was not time-sensitive. Attentive observers will note indications of the films wartime production, such as period dialogue, pictures of President Franklin D. Roosevelt, and a woman taxi driver, who says to Bogart, "Im your girl."  Wartime rationing influences the film: dead bodies are called "red points", which referred to wartime meat rationing, and Marlowes car has a "B" gasoline rationing sticker in the lower passenger-side window, indicating he was essential to the war effort and therefore allowed eight gallons of gasoline per week.
  To Have Julius Epstein).  The re-shot ending featured Peggy Knudsen as Mona Mars because Pat Clark, the originally cast actress, was unavailable. Furthermore, the parts of James Flavin and Thomas E. Jackson were completely eliminated. Because of the two versions created by the re-shooting, there is a substantial difference in content of some twenty minutes between them, although the difference in running time is two minutes. The re-shot, revised The Big Sleep was released on 23 August 1946.

The cinematic release of The Big Sleep is regarded as more successful than the 1945 pre-release version (see below), although some complain it is confusing and difficult to follow. This may be due in part to the omission of a long conversation between Marlowe and the Los Angeles District Attorney where facts of the case, thus far, are laid out. Yet movie-star aficionados prefer the film noir version because they consider the Bogart-Bacall appearances more important than a well-told story. For an example of this point of view, see Roger Eberts The Great Movies essay on the film.   

Novelist Raymond Chandler said Martha Vickers (Carmen) overshadowed Lauren Bacall (Vivian) in their scenes together, which led the producers to delete much of Vickers performance to enhance Bacalls. 

==Effects of the Hays Code==

===Killers identity===
There is some confusion as to the identity of Sean Regans killer. In the novel Carmen is definitely the culprit, but that would have made Vivian, Marlowes love interest, an accessory to murder, which would have run afoul of the Hollywood Production Code.    Hence, the film alters the original story, to imply that Mars killed Regan himself because Regan was romancing Marss wife. He then convinced Vivian that her sister committed the crime during one of her mental blackouts so that he could blackmail the Sternwood family.

===Sexuality===
Another primary focus of the Hays Office censorship policies was to heavily restrict sexual themes.  In the novel, Geiger is selling pornography, then illegal and associated with organized crime, and is also a homosexual having a relationship with Lundgren. Likewise, Carmen is described as being nude in Geigers house, and later nude and in Marlowes bed. To ensure the film would be approved by the Hays Office, changes had to be made. Carmen had to be fully dressed, and the pornographic elements could only be alluded to with cryptic references to photographs of Carmen wearing a "Chinese dress" and sitting in a "Chinese chair".  The sexual orientation of Geiger and Lundgren goes unmentioned in the film because references to homosexuality were prohibited. The scene of Carmen in Marlowes bed was replaced with a scene in which she appears, fully dressed, sitting in Marlowes apartment, when he promptly kicks her out. The scene, shot in 1944, was entirely omitted in the 1945 cut but restored for the 1946 version. 

==Reception==

===Critical response===

====1946 version====
At the time of its 1946 release, Bosley Crowther said the film leaves the viewer "confused and dissatisfied", points out that Bacall is a "dangerous looking female" ..."who still hasnt learned to act" and notes: 
 The Big Sleep is one of those pictures in which so many cryptic things occur amid so much involved and devious plotting that the mind becomes utterly confused. And, to make it more aggravating, the brilliant detective in the case is continuously making shrewd deductions which he stubbornly keeps to himself. What with two interlocking mysteries and a great many characters involved, the complex of blackmail and murder soon becomes a web of utter bafflement. Unfortunately, the cunning script-writers have done little to clear it at the end. 

Time (magazine)|Time called the film "wakeful fare for folks who dont care what is going on, or why, so long as the talk is hard and the action harder" but insists that "the plots crazily mystifying, nightmare blur is an asset, and only one of many"; it calls Bogart "by far the strongest" of its assets and says Hawks, "even on the chaste screen...manages to get down a good deal of the glamorous tawdriness of big-city low life, discreetly laced with hints of dope addiction, voyeurism and fornication." 

====1997 release of the 1945 original cut====
In the late 1990s, a pre-release version—director Hawkss original cut—was found in the UCLA Film and Television Archive. That version had been released to the military to play to troops in the South Pacific. Benefactors, led by American magazine publisher Hugh Hefner, raised the money to pay for its restoration, and the original version of The Big Sleep was released in art-house cinemas in 1997 for a short exhibition run, along with a comparative documentary about the cinematic and content differences between Hawkss film noir and the Warner Brothers "movie star" version. 

Film critic Roger Ebert, who included the film in his list of "Great Movies",    praises the films writing: 

 Working from Chandlers original words and adding spins of their own, the writers (William Faulkner, Jules Furthman and Leigh Brackett) wrote one of the most quotable of screenplays: its unusual to find yourself laughing in a movie not because something is funny but because its so wickedly clever. 

Note that the above quote is not specific to the 1945 version but, rather, is in reference to both films. In fact, Ebert preferred the 1946 version. He stated:
 The new scenes   add a charge to the film that was missing in the 1945 version; this is a case where "studio interference" was exactly the right thing. The only reason to see the earlier version is to go behind the scenes, to learn how the tone and impact of a movie can be altered with just a few scenes... As for the 1946 version that we have been watching all of these years, it is one of the great films noir, a black-and-white symphony that exactly reproduces Chandlers ability, on the page, to find a tone of voice that keeps its distance, and yet is wry and humorous and cares.   

In a 1997 review, Eric Brace of The Washington Post wrote that the 1945 original had a "slightly slower pace than the one released a year later, and a touch less zingy interplay between Bogart and Bacall, but it’s still an unqualified masterpiece."   

===Accolades===
  AFI named 32nd greatest Empire magazine Masterpiece collection in the October 2007 issue. 

==Home media==
  To Have and Have Not (1944), "The fullscreen transfer of The Big Sleep is generally good but, again, not crystalline, though the grain that afflicts the earlier picture is blissfully absent. Shadow detail is strong—important given that The Big Sleep is oneiric—and while the brightness seems uneven, its not enough to be terribly distracting. The DD 1.0 audio is just fine." 

==1978 remake== released in the second movie in three years featuring Mitchum as Marlowe. Many  have noted that while it was more faithful to the novel, due to lack of restrictions on what could be portrayed on screen, it was far less successful than the original 1946 version with Bogart and Bacall.

==In popular culture==
Smallville features the movie in a scene in season 6, episode 20 ("Noir"). In the episode Jimmy Olsen, a passionate fan of the movie (and old movies in general), plays his favorite scene to Chloe, and he even knows the whole scene by heart, much to Chloes amusement.

==References==
 

==External links==
 
*  
*  
*  
*  
*  
*  

 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 