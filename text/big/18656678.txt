With or Without You (film)
{{Infobox film
| name           = With or Without You
| image          = With_or_Without_You_DVD.jpg
| director       = G. Stubbs
| producer       = Anita M. Cal Bobby Thompson G. Stubbs Guy A. Young Lisa Diane Washington
| writer         = Bobby Thompson
| narrator       = Mushond Lee Victor Williams
| music          = Ivan Graham
| cinematography = Keith L. Smith
| editing        = G. Stubbs
| distributor    = Screen Media Films
| released       =  
| runtime        = 88 minutes
| country        = United States
| language       = English
}}
With or Without You is a 2003 comedy-drama film directed by G. Stubbs. The film stars Cynda Williams, Mushond Lee, and Wendy Raquel Robinson.

==Plot==
Roberts (Mushond Lee|Lee) life has been going just the way he wanted it to, from both a professional and personal standpoint. That is until his girlfriend Cheri (Cynda Williams|Williams) tells him shes expecting. While she feels they should get married, Robert still isnt quite sure if hes ready to take such a major step in his life. He tries to get advice from his friends, who all (besides not being married or in any sort of exclusive relationships of their own) basically tell him to stand firm in his decision if he doesnt feel hes ready. Cheri also has a group of friends however, and they are just as much in support of her opinion that the two should be looking to settle down as Roberts friends are of his. When they all get together to celebrate the baby shower, tensions inevitably flare, and a few unexpected secrets finally come to light, leading to a frothy and mirthful climax.

==Cast==
*Cynda Williams &mdash; Cheri Fontenot
*Mushond Lee &mdash; Robert Hightower
*Dannon Green &mdash; Cousin Jacque
*Guy Torry &mdash; Greg
*J.B. Smoove &mdash; Darnell
*Maia Campbell &mdash; Teresa
*Maria de Los Angeles &mdash; Maria
*Maurice Smith &mdash; Lee
*Nicki Micheaux &mdash; Rochelle
*James "Talent" Harris &mdash; Eddie Victor Williams &mdash; Kenneth
*Wendy Raquel Robinson &mdash; Serena

==External links==
*  
*  

 
 
 
 
 
 
 


 