Alien vs. Predator (film)
 
 
{{Infobox film
| name           = Alien vs. Predator
| image          = Avpmovie.jpg
| caption        = Theatrical release poster
| director       = Paul W. S. Anderson
| producer       =  
| screenplay     = Paul W. S. Anderson
| story          =  
| based on       =    
| starring       =  
| music          = Harald Kloser
| cinematography = David Johnson
| editing        = Alexander Berner
| studio         = Davis Entertainment; Brandywine Productions
| distributor    = 20th Century Fox
| released       =  
| runtime        = 101 minutes 109 minutes  
| country        =   
| language       = English
| budget         = $60 million   
| gross          = $172.5 million 
}} crossover bringing 1989 comic book. Anderson, Dan OBannon, and Ronald Shusett wrote the story, and Anderson and Shane Salerno adapted the story into a screenplay. Their writing was influenced by Aztec mythology, the comic book series, and the writings of Erich von Däniken.
 Predators who Aliens as a rite of passage. The humans are caught in the middle of a battle between the two species and attempt to prevent the Aliens from reaching the surface.

The film was released on 13 August 2004 in North America and received mixed to negative reviews from film critics. The film grossed over $172 million at the worldwide box office. The film is followed by a sequel   (2007).

==Plot==
In 2004, a satellite detects a mysterious heat bloom beneath Bouvet Island|Bouvetøya, an island about one thousand miles off the coast of Antarctica. Wealthy industrialist Charles Bishop Weyland (Lance Henriksen) discovers through thermal imaging that there is a pyramid buried 2000ft beneath the ice. Weyland wants to claim it for his multinational communications company, Weyland Industries and assembles a team of experts to go investigate. The team includes archaeologists, linguistic experts, drillers, mercenaries, and a guide named Alexa Woods (Sanaa Lathan).
 Predator ship whaling station rib cages.
 Alien queen cryogenic stasis and begins to produce eggs. When the eggs hatch, several facehuggers attach themselves to humans trapped in the sacrificial chamber. Chestbursters emerge from the humans and quickly grow into adult Aliens. Conflicts erupt between the Predators, Aliens, and humans, resulting in several deaths. The two Predators are killed by a lone Alien, and Weyland is killed by the remaining Predator, Scar, while allowing Alexa and Sebastian enough time to flee. The two witness Scar killing a facehugger and an Alien before unmasking and marking himself. After Alexa and Sebastian leave, another facehugger attaches itself to the unmasked Predator.

Through translation of the pyramids hieroglyphs, Alexa and Sebastian learn that the Predators have been visiting Earth for thousands of years. It was they who taught early human civilizations how to build pyramids, and were worshiped as gods. Every 100 years they would visit Earth to take part in a rite of passage in which several humans would sacrifice themselves as hosts for the Aliens, creating the "ultimate prey" for the Predators to hunt. If overwhelmed, the Predators would activate a self-destruct device to eliminate the Aliens and themselves. The two deduce that this is why the current Predators are at the pyramid, and that the heat bloom was to attract humans for the sole purpose of making new Aliens to hunt.

Alexa and Sebastian decide that the Predators must be allowed to succeed in their hunt so that the Aliens do not reach the surface. Unfortunately, Sebastian is captured by the Aliens, leaving only Alexa and Scar to fight against the Aliens. The two form an alliance and use Scars self-destruct device to destroy the pyramid and the remaining Aliens. Alexa and Scar reach the surface, where they battle the escaped Alien queen. They defeat the queen by attaching its chain to a water tower and pushing it over a cliff, dragging the queen to the ocean floor and drowning it. Scar, however, dies from his wounds.

A Predator ship  .

==Cast==
 
 
*Sanaa Lathan as Alexa Woods, an experienced guide who spent several seasons exploring the Arctic and Antarctic environments.
*Lance Henriksen as Charles Bishop Weyland, the billionaire head of Weyland Industries.
*Raoul Bova as Sebastian De Rosa, an Italian archaeologist who is able to translate the pyramids hieroglyphs.
*Ewen Bremner as Graeme Miller, a chemical engineer of the exploration team.
*Colin Salmon as Maxwell "Max" Stafford, assistant to Mr. Weyland. Tommy Flanagan as Mark Verheiden, a mercenary member of the exploration team.
*Joseph Rye as Joe Connors, a member of the exploration team.
*Agathe de La Boulaye as Adele Rousseau, a mercenary member of the exploration team.
*Carsten Norgaard as Rustin Quinn, a mercenary member of the exploration team.
*Liz May Brice as Selene, the teams supervisor.
*Glenn Conroy as Technician
*Karima Adebibe as Sacrificial Maiden
*Sam Troughton as Thomas "Tom" Parkes, a member of the exploration team. Ian Whyte Scar - The Predator, one of three Predators who come to Earth to create and hunt Aliens within the pyramid as a rite of passage. Whyte played the lead Predator, called "Scar" in the films credits due to the predator marking himself with the aliens acidic blood. Whyte also played the two Predators, Chopper & Celtic. Grid - The Alien. The Alien played by Woodruff is listed in the films credits as "Grid", after a grid-like wound received from the battle with Celtic during the film.
*Petr Jákl as Stone
*Pavel Bezdek as Bass
*Kieran Bew as Klaus
*Carsten Voigt as Mikkel
*Jan Filipensky as Boris 
*Adrian Bouchet as Sven
*Eoin McCarthy as Karl
*Andy Lucas as Juan Ramirez
 

==Production==

===Alien 5 and sequel=== Frankenstein Meets Werewolf. It was Universal just taking their assets and starting to play them off against each other...Milking it."    After viewing Alien vs. Predator, however, Cameron remarked that "it was actually pretty good. I think of the five Alien films, Id rate it third. I actually liked it. I actually liked it a lot."  Conversely, Ridley Scott had no interest in the Alien vs. Predator films. When asked in May 2012 if he had watched them, Scott laughed, "No. I couldnt do that. I couldnt quite take that step." 

===Development=== John Davis, who hoped to give the film an original approach by setting it on Earth. 

As there were six producers between the film franchises, Davis had difficulty securing the rights as the producers were worried about a film featuring the two creatures.  , with Shane Salerno co-writing. Salerno spent six months writing the shooting script, finished its development, and stayed on for revisions throughout the films production. 

===Story and setting===
  and Aztec mythology, Anderson had the Predators come to Earth in spaceships and teach humans how to build pyramids. As a result they were treated as gods.|alt=]]
Early reports claimed the story was about humans who tried to lure Predators with Alien eggs, although the idea was scrapped.  Influenced by the work of Erich von Däniken, Anderson researched von Dänikens theories on how he believed early civilisations were able to construct massive pyramids with the help of aliens, an idea drawn from Aztec mythology.    Anderson wove these ideas into Alien vs. Predator, describing a scenario in which Predators taught ancient humans to build pyramids and used Earth for rite of passage rituals every 100 years in which they would hunt Aliens. To explain how these ancient civilisations "disappeared without a trace", Anderson came up with the idea that the Predators, if overwhelmed by the Aliens, would use their self-destruct weapons to kill everything in the area.  H. P. Lovecrafts 1931 novella At the Mountains of Madness served as an inspiration for the film, and several elements of the Aliens vs. Predator comic series were included.   Andersons initial script called for five Predators to appear in the film, although the number was later reduced to three. 
 Norwegian Antarctic Bouvet commenting, "Its definitely the most hostile environment on Earth and probably the closest to an Alien surface you can get."    Anderson thought that setting the film in an urban environment like New York City would break continuity with the Alien series as the protagonist, Ellen Ripley, had no knowledge the creatures existed. "You cant have an Alien running around the city now, because it wouldve been written up and everyone will know about it. So theres nothing in this movie that contradicts anything that already exists." 

===Casting===
 
The first actor to be cast for Alien vs. Predator was Lance Henriksen, who played the character Bishop in Aliens (film)|Aliens and Alien 3. Although the Alien movies are set 150 years in the future, Anderson wanted to keep continuity with the series by including a familiar actor. Henriksen plays billionaire Charles Bishop Weyland, a character that ties in with the Weyland-Yutani Corporation. According to Anderson, Weyland becomes known for the discovery of the pyramid, and as a result the Weyland-Yutani Corporation models the Bishop android in the Alien films after him; "when the Bishop android is created in 150 years time, its created with the face of the creator. Its kind of like Microsoft building an android in 100 years time that has the face of Bill Gates." 

Anderson opted for a European cast including Italian actor Raoul Bova, Ewen Bremner from Scotland, and English actor Colin Salmon. Producer Davis said, "Theres a truly international flavor to the cast, and gives the film a lot of character."    Several hundred actresses attended the auditions to be cast as the films heroine Alexa Woods. Sanaa Lathan was selected, and one week later she flew to Prague to begin filming. The filmmakers knew there would be comparisons to Alien heroine Ellen Ripley and did not want a clone of the character, but wanted to make her similar while adding something different. 
 recall election on condition that the filming should take place at his residence.  Schwarzenegger, however, won the election with 48.58% of the votes and was unavailable to participate in Alien vs. Predator. Actress Sigourney Weaver, who starred as Ellen Ripley in the Alien series, said she was happy not to be in the film, as a possible crossover was "the reason I wanted my character to die in the first place", and thought the concept "sounded awful".  

===Filming and set designs===
Production began in late 2003 at Barrandov Studios in Prague, Czech Republic, where most of the filming took place. Production designer Richard Bridgland was in charge of sets, props and vehicles, based on early concept art Anderson had created to give a broad direction of how things would look. 25 to 30 life-sized sets were constructed at Barrandov Studios, many of which were interiors of the pyramid. The pyramids carvings, sculptures, and hieroglyphs were influenced by Ancient Egypt|Egyptian, Cambodian, and Aztec civilisations, while the regular shifting of the pyramids rooms was meant to evoke a sense of claustrophobia similar to the original Alien film.  According to Anderson, if he was to build the sets in Los Angeles they would have cost $20 million. However, in Prague they cost $2 million, an important factor when the films budget was less than $50 million. 
 miniatures several meters in height were created to give the film the effect of realism, rather than relying on computer generated imagery (CGI). For the whaling station miniatures and life-sized sets, over 700 bags of artificial snow were used (roughly 15–20 tons).  A 4.5-meter miniature of an icebreaker with working lights and a mechanical moving radar was created, costing almost $37,000 and taking 10 weeks to create. Visual effects producer Arthur Windus, claimed miniatures were beneficial in the filming process: "With computer graphics, you need to spend a lot of time making it real. With a miniature, you shoot it and its there."    A scale 25-meter miniature of the whaling station was created in several months. It was designed so the model could be collapsed and then reconstructed, which proved beneficial for a six-second shot which required a re-shoot. 

===Effects and creatures===
  John Bruno were in charge of the project, which contained 400 effects shots.  ADI founders Alec Gillis, Tom Woodruff Jr., and members of their company, began designing costumes, miniatures and effects in June 2003. For five months the creatures were redesigned, the Predators wrist blades being extended roughly four times longer than those in the Predator films, and a larger mechanical plasma caster was created for the Scar Predator. 
 hydraulic Alien puppet was created so ADI would be able to make movements faster and give the Alien a "slimline and skeletal" appearance, rather than using an actor in a suit. The puppet required six people to run it; one for the head and body, two for the arms, and a sixth to make sure the signals were reaching the computer. Movements were recorded in the computer so that puppeteers would be able to repeat moves that Anderson liked. The puppet was used in six shots, including the fight scene with the Predator which took one month to film. 

The crew tried to keep CGI use to a minimum, as Anderson said people in suits and puppets are scarier than CGI monsters as they are "there in the frame".  Roughly 70% of scenes were created using suits, puppets, and miniatures. The Alien queen was filmed using three variations: a 4.8-meter practical version, a 1.2-meter puppet, and a computer-generated version. The practical version required 12 puppeteers to operate,  and CGI tails were added to the Aliens and the queen as they were difficult to animate using puppetry.   The queen aliens inner-mouth was automated though, and was powered by a system of hydraulics. Anderson praised Alien director Ridley Scotts and Predator director John McTiernans abilities at building suspense by not showing the creatures until late in the film, something Anderson wanted to accomplish with Alien vs. Predator. "Yes, we make you wait 45 minutes, but once it goes off, from there until the end of the movie, its fucking relentless". 

===Music===
 
Austrian composer Harald Kloser was hired to create the Film score|films score. After completing the score for The Day After Tomorrow, Kloser was chosen by Anderson as he is a fan of the franchises.    It was recorded in London, and was primarily orchestral as Anderson commented, "this is a terrifying movie and it needs a terrifying, classic movie score to go with it; at the same time its got huge action so it needs that kind of proper orchestral support." 
 character development in the film, "too generic to completely engage or leave a permanent impression." 

==Reception==

===Box office=== Predator  when adjusted for inflation. 

===Critical reception===
 

Critics were not allowed to view the film in advance. Reviews from critics were mainly negative, but reviews from fans were more mixed.  Based on 142 reviews, the film scored a 21% approval rating on Rotten Tomatoes with the consensus stating "Gore without scares and cardboard cut-out characters make this clash of the monsters a dull sit",    and 29 out of 100 based on 21 reviews on Metacritic.    Chief criticisms of the film included its dialogue, "cardboard characters", a PG-13 rating, the "fast-paced editing" during fight sequences, and lighting. However, special effects and set designs received praise.  

Rick Kisonak of Film Threat praised the film stating, "For a big dumb production about a movie monster smackdown, Alien vs. Predator is a surprisingly good time".    Ian Grey of the Orlando Weekly felt, "Anderson clearly relished making this wonderful, utterly silly film; his heart shows in every drip of slime."  Staci Layne Wilson of Horror.com called it "a pretty movie to look at with its grandiose sets and top notch creature FX, but its a lot like Andersons previous works in that its all facade and no foundation." 
 Battlefield Earth."  Gary Dowell of Dallas Morning News called the film, "a transparent attempt to jumpstart two run-down franchises".  Ed Halter of The Village Voice described the films lighting for fight sequences as, "black-on-black-in-blackness",  while Ty Burr of The Boston Globe felt the lighting "left the audience in the dark". 

===Accolades===
The film received a Golden Raspberry Award nomination 2005 in the category of "Worst Remake or Sequel". 

==Home media releases== audio commentaries. The first featured Paul W. S. Anderson, Lance Henriksen, and Sanaa Lathan, while the second included special effects supervisor John Bruno and ADI founders Alec Gillis and Tom Woodruff. A 25-minute "Making of" featurette and a Dark Horse AVP comic cover gallery were included in the special features along with three deleted scenes from the film. On release, Alien vs. Predator debuted at number 1 on the Top DVD Sales and Top Video Rental charts in North America.  
 behind the scenes footage of the conception, pre-production, production, post-production, and licensing of the film. An "Unrated Edition" was released on 22 November 2005, containing the same special features as the Extreme Edition as well as an extra eight minutes of footage in the film. John J. Puccio of DVD Town remarked that the extra footage contained "a few more shots of blood, gore, guts, and slime to spice things up...and tiny bits of connecting matter to help us follow the story line better, but none of it amounts to much."  The film was released on Blu-ray Disc in North America on 23 January 2007.

==Sequel== Greg and Colin Strause, the story continues from the conclusion of Alien vs. Predator.  The film was panned, and despite having a worldwide theatrical gross of nearly $130 million,    the film grossed less than its predecessor.

==See also==
 
* List of action films of the 2000s
* List of horror films of 2004
* List of science-fiction films of the 2000s

==References==
 

==External links==
 
*  
*  
*  
*  
*  

 
 
 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 