The White Heather
{{Infobox film
| name           = The White Heather
| image          =Maurice Tourneur The White Heather.jpg
| caption        =Advertisement, 1919
| director       = Maurice Tourneur
| producer       = Maurice Tourneur Henry Hamilton Charles E. Whittaker Ben Alexander   Ralph Graves   Mabel Ballin
| music          =
| cinematography = René Guissart (director)|René Guissart   Harold S. Sintzenich 
| editing        = 
| studio         = Maurice Tourneur Productions
| distributor    = Famous Players-Lasky Corporation
| released       = June 29, 1919
| runtime        = 70 minutes 
| country        = United States
| awards         =
| language       = Silent (English intertitles)
| budget         = 
}} silent drama Ben Alexander play of Henry Hamilton. John Gilbert appeared in a supporting part. The survival status of the film is classified as being unknown,  which suggests that it is a lost film.

==Plot== housekeeper Marion Hume (Ballin) on his yacht before it was sunk. Documentary evidence of the marriage now lies many fathoms underwater, and one witness is dead while another, a sailor, has vanished on some voyage. During a hunt Angus accidentally shoots his son from the marriage, leading Marion to announce it to save her injured son. Angus denies the marriage, so Marion goes to her father James Hume (Aitken), while two admirers of Marion hunt for the missing witness in the London underworld. Her father fights for his daughters honor in court, but the case is lost for lack of evidence, and he is ruined on the exchange, dying when he is unable to meet his liabilities. When the missing witness is found, Angus bribes him to disappear. There remain only the papers in a chest on the sunken yacht, and diving operations are ongoing. The two admirers and Lord Angus hasten to the scene. One of the admirers dives on the yacht as does Angus armed with a knife. During an underwater struggle Angus accidentally cuts his own air hose and is killed. The admirer returns to the surface with the proof of the marriage and claims Marion for himself, while the second admirer dies while also confessing his love.

==Cast==
* Holmes Herbert as Lord Angus Cameron Ben Alexander as Donald Cameron
* Ralph Graves as Alec McClintock
* Mabel Ballin as Marion Hume John Gilbert as Dick Beach
* Spottiswoode Aitken as James Hume

==References==
 

==Bibliography==
* Waldman, Harry. Maurice Tourneur: The Life and Films. McFarland, 2008.

==External links==
* 

 
 
 
 
 
 
 
 
 
 
 
 