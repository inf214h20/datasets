That Cold Day in the Park
{{Infobox film
| name           = That Cold Day in the Park
| image          = Poster of the movie That Cold Day in the Park.jpg
| image_size     =
| caption        =
| director       = Robert Altman
| producer       = Peter Miles (novel) Gillian Freeman
| narrator       =
| starring       = Sandy Dennis
| music          = Johnny Mandel
| cinematography = László Kovács (cinematographer)|László Kovács
| editing        = Danford B. Greene
| distributor    = Commonwealth United
| released       = 8 June 1969 (USA) 
| runtime        = 113 min
| country        = U.S.A. / Canada English
| budget         =
| preceded_by    =
| followed_by    =
}} 1969 suspense Peter Miles Michael Burns, Michael Murphy.  The picture was screened at the 1969 Cannes Film Festival outside of the main competition.   

==Cast==
* Sandy Dennis - Frances Austen Michael Burns - The boy
* Susanne Benton - Nina
* David Garfield - Nick (billed as John Garfield, Jr.)
* Luana Anders - Sylvie Michael Murphy - The Rounder
* Edward Greenhalgh - The doctor
* Alicia Ammon - Mrs. Pitt 
* Lloyd Berry - Mr. Parnell 
* Rae Brown - Mrs. Parnell 
* Linda Sorenson - The prostitute
* Frank Wade - Mr. Ebury 

==References==
 

==External links==
*  
*  
*   (essay by Michael Turner)
*   (note by Shannon Kelley)
*  

 

 
 
 
 
 
 
 
 
 
 
 


 