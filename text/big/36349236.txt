Loosies
 
{{Infobox film
| name = Loosies
| image = Loosies theatrical poster.jpg
| alt =
| caption = Loosies theatrical poster
| director = Michael Corrente
| producer = Peter Facinelli   Chad A. Verdi Gino Pereira   Anthony Gudas
| writer = Peter Facinelli William Forsythe Christy Carlson Romano Glenn Ciano Vincent Gallo Chad A. Verdi
| music = Chad Fischer
| cinematography =
| editing = Daniel Boneville
| casting = Adrienne Stern
| studio = IFC Films  Verdi Productions  Facinelli Films
| distributor = IFC Films
| released =  
| runtime = 89 minutes  
| country = United States
| language = English 
| budget =
| gross =
}} William Forsythe, Christy Carlson Romano, Glenn Ciano, Vincent Gallo and Chad A. Verdi.   

==Plot== New York pickpocket, enjoys the free lifestyle he has until one day he is confronted by a former one night stand, Lucy Atwood (Alexander), who says she is pregnant with his child. She gives him a chance to leave his daily life and take responsibility of his child. 

==Cast==
 
 
* Peter Facinelli as Bobby Corelli
* Jaimie Alexander as Lucy Atwood
* Michael Madsen as Lt. Nick Sullivan
* Vincent Gallo as Jax William Forsythe as Capt. Tom Edwards
* Christy Carlson Romano as Carmen
* Marianne Leone as Rita Corelli
* Joe Pantoliano as Carl
* Glenn Ciano as Gomer
* Eric Phillips as Donny
* Tom DeNucci as Detective Jeffrey
* Tom Paolino as Detective Verdi
* Ara Boghigian as Officer
* Johnny Cicco as Stoner Adam
* Benny Salerno as Man on Subway
* Tyler and Travis Atwood as Mickey and Mikey 
* David Goggin as NYPD Officer
 

==Production==
The film was shot on location in Providence, Rhode Island|Providence, Rhode Island, and New York.  

==Release==
The film was limited released on November 2, 2011, and wide released on January 11, 2012 and finally released on the DVD on March 13, 2012.    The film was rated Motion Picture Association of America film rating system|PG-13 by Motion Picture Association of America for some sexual content, violence and language used in the film. 

==International Distribution==
The International distribution rights of Loosies a.k.a. Pick Pocket are being licensed by Cinema Management Group  

==References==
 

==External links==
*   (IFC Films)
*   (Verdi Films)
*  

 

 
 
 
 
 
 
 
 
 
 