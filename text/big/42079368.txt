Meu Passado Me Condena
{{Infobox film
| name           = Meu Passado Me Condena
| image          = Meu Passado Me Condena Film Poster.jpg
| caption        = Theatrical release poster
| director       = Julia Rezende
| producer       = 
| writer         = Tati Bernardi  Leonardo Muniz
| starring       = Fábio Porchat  Miá Mello  Marcelo Valle  Inez Viana  Juliana Didone  Alejandro Claveaux
| music          = 
| cinematography = 
| editing        = 
| distributor    = Downtown Filmes  Paris Filmes
| studio         = Atitude Produções  Globo Filmes  H Films  Morena Films  Multishow  Paris Filmes   Riofilme
| released       =  
| runtime        = 102 minutes
| country        =  
| language       = Portuguese
| budget         = BRL|R$3,6 million 
| gross          = R$34,802,424 
}}

Meu Passado Me Condena is a 2013 Brazilian comedy film directed by Julia Rezende, and starring Fábio Porchat and Mia Mello. The film is based on the television series of the same name, and was written by Tati Bernardi. It was released theatrically on October 25, 2013.   

==Plot==
Fábio (Fábio Porchat) and Miá (Miá Mello) are two newlyweds who decided to get engaged after only a month of dating. The couple decides to spend their honeymoon on a cruise from Rio de Janeiro to Europe. However, the two soon discover that an ex-boyfriend of Miá and an old Platonic passion of Fábio are also on board. 

==Production==

Meu Passado Me Condena  is the first fiction feature film from director Julia Rezende and has Mariza Lion as the producer. The film is based on the television comedy of the same name, transmitted by the Multishow channel, which was created and directed by Rezende and the scriptwriter Tati Bernardi.   

The film began shooting in early March 2013, on the cruise ship Costa Favolosa, being the first time that a Brazilian team shot a film on a real cruise. The cast departed from Rio de Janeiro and went through Ilhéus, Maceió, Salvador, Bahia|Salvador, Recife, Fortaleza and Italy. 

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 
 
 

 