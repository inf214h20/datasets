The Art of Flight
{{Infobox film
| name           = The Art of Flight
| image          = ArtOfFlight2011Poster.jpg
| alt            = 
| caption        = Film poster
| director       = Curt Morgan
| producer       = 
| writer         = 
| screenplay     = 
| story          = 
| narrator       = 
| starring       = 
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 
| country        = United States, Austria
| language       = English
| budget         = Approx. $2,000,000
| gross          = 
}}

The Art of Flight is a Red Bull sponsored documentary film about snowboarding and a successor to Thats It, Thats All. It was released on September 8, 2011 in the United States.

==Cast==
* Travis Rice
* Mark Landvik
* John Jackson
* Nicolas Müller
* Scotty Lago
* Bjorn Leines
* David Carrier Porcheron "DCP" Jeremy Jones
* Pat Moore
* Eero Niemela
* Kyle Clancy
* Eric Willett
* Bode Merrill
* Jack Mitrani
* Luke Mitrani
* Mark McMorris
* Jake Blauvelt

==Locations==
* Nelson, British Columbia, Canada
* Revelstoke, British Columbia
* Patagonia, Chile
* Alaska
* Jackson, Wyoming
* Aspen, Colorado

==Shooting and production==
The Art of Flight was shot over the course of two years from 2009 to 2011.

It was filmed using the RED camera system, the GoPro Hero, Vision Researchs Phantom camera and Panasonic camera systems.

==Soundtrack==
The Art of Flight soundtrack features songs by: The Naked and Famous, M83 (band)|M83, Deadmau5, Sigur Rós, and others. 

==References==
 

==External links==
*  official film site
* 

 

 
 
 
 
 
 

 