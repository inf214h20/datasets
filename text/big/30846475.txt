The Pit and the Pendulum (2009 film)
{{Infobox film
| name           = Edgar Allan Poes The Pit and the Pendulum
| image          = The-pit-and-the-pendulum-by-david-decateau.jpg
| caption        = DVD release cover 
| director       = David DeCoteau
| producer       = Paul Colichman, Stephen P. Jarchow, John Schouweiler
| written by      = Edgar Allan Poe (story), Simon Savory (screenplay)
| starring       = Lorielle New, Stephen Hansen and Bart Viotila
| music          = Jerry Lambert
| cinematography = Howard Wexler
| editing        = Danny Draven(as Jack Harkness)
| studio         = Rapid Heart Pictures
| distributor    = E1 Entertainment, Regent Releasing, Regent Worldwide Sales
| released       =  
| runtime        = 86 minutes
| country        = United States
| language       = English}}

Edgar Allan Poes The Pit and the Pendulum (2009) is the retelling of Edgar Allan Poes classic The Pit and the Pendulum with a new twist. The film is directed by David DeCoteau and stars Lorielle New, Stephen Hansen and Bart Viotila.

==Synopsis==
Seven students answer an advertisement to participate in an experiment to explore how the sensation of pain can be eliminated. Arriving at a secluded institute, they are welcomed by mysterious and eccentric scientist JB Divay (Lorielle New). But when students begin to disappear one by one, they begin to question JBs true intentions. In a final showdown, Jason (Stephen Hansen) confronts her and discovers his boyfriend Kyle (Bart Voitila) strapped to a table beneath her final experiment, inches away from the razors edge.

==Cast==
:Lorielle New as JB Divay
:Stephen Hansen as Jason
:Bart Voitila as Kyle
:Danielle Demski as Alicia
:Amy Paffrath as Gemma
:Tom Sandoval as Vinnie
:Michael King as Trevor
:Jason-Shane Scott as Julian
:Andrew Bowen as Alan Divay
:Jason Stuart as Dimitri Divay

==References==
 

== External links==
* 

 
 

 
 
 
 
 
 
 


 