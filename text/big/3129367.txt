Oriundi (film)
{{Infobox film
| name           = Oriundi
| image          = Oriundi film.jpg
| caption        = Brazilian theatrical poster
| director       = Ricardo Bravo
| producer       = Rubens Aparecido Gennaro Virgínia W. Moraes
| writer         = Marcos Bernstein
| starring       = Anthony Quinn
| music          = Arrigo Barnabé
| cinematography = Toca Seabra Jacques Cheuiche Gui Gonçalves
| editing        = Isabelle Rathery Ana Teixeira
| studio         = LAZ Audiovisual
| distributor    = Warner Bros.
| released       =  
| runtime        = 97 minutes
| country        = Brazil
| language       = Portuguese Italian
| budget         = $3–5,5 million  
| gross          = R$223,608 
}}

Oriundi is a 2000 Brazilian drama film directed by Ricardo Bravo. It stars Anthony Quinn as an Italian of an oriundo family in Brazil. Oriundo is an Italian word used to describe Italian diaspora|foreign-born Italian nationals, especially South Americans of Italian heritage. The film was enterely shot in Curitiba, Paraná (state)|Paraná.  

==Cast==
*Anthony Quinn as Giuseppe Padovani
**Lorenzo Quinn as young Giuseppe Padovani
*Letícia Spiller as Caterina Padovani / Sofia DAngelo
*Paulo Betti as Renato Padovani
*Gabriela Duarte as Patty
*Paulo Autran as Dr. Enzo
*Araci Esteves as Paola
*Marly Bueno as Matilde
*Tiago Real as Stephano
*Raquel Rizzo as Chris

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 

 
 