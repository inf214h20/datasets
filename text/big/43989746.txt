The Defender (1988 film)
{{Infobox film
| name           = The Defender
| image          =THE DEFENDER (1998).jpg
| image size     =150px
| caption        =Theatrical poster
| director       = Stephen Low
| producer       = Charles Konowal Ches Yetman (Executive producer)
| writer         = Stephen Low Cedric Smith
| starring       =
| music          =Eldon Rathburn
| cinematography =Charles Konowal
| editing        =Alfonso Peccia
| studio         = National Film Board of Canada 
| distributor    = National Film Board of Canada
| released       =  
| runtime        = 54 minutes, 54 seconds
| country        = Canada
| language       = English
| budget         =
| gross          =
}}

The Defender is a 54-minute Canadian documentary film, made in 1988 by the National Film Board of Canada (NFB) and directed by Stephen Low. The film depicts the building of a strike fighter aircraft by Bob Diemert, an eccentric Canadian aviation engineer. His dream of building the next Canadian fighter aircraft to challenge the might of the Soviet Union was dependent on selling a restored warbird. 

==Plot==
  COIN aircraft Junkers Ju Ilyushin Il-2 Second World War.
 Mitsubishi A6M2 Battle of Confederate Air Force in Texas has to await the painstaking restoration of the Japanese fighter aircraft. As it is readied for a test flight, Diemert runs afoul of Canadian aviation authorities, who refuse to allow him to fly the aircraft. Trucking the restored aircraft to Midland, Texas is the solution and after successful test flights, the Zero is passed over to its new American owners. 

Completing the Defender becomes the sole preoccupation of Diemert and his friend. Trying to ensure that the aerodynamics are properly established leads to a curious use of a bathroom scale mounted on the back of a pickup truck, an example of the unorthodox engineering that is employed in the project. Another example of Diemerts out-of-the-box thinking comes when his children ask him to build a swimming pool. His wife comes back from work to find a swimming pool in the living room, complete with wall-to-wall carpeting.

When the Defender finally emerges from its hangar, Diemert prepares for the all-important maiden flight, but things do not go as planned.

==Production==
Director Stephen Low committed himself to the film project from 1982, following the story of the Diemert projects as they continually changed. The result was a six-year-long period of acquiring footage, along with producer/cameraman Charles Kenowal, at a variety of locations including Diemerts home base of Carman, Winnipegs Transport Canada offices and Texas. During the production, Low had the full cooperation of Diemert, his friends and his family. Ohayon, Albert.   National Film Board, July 27, 2009. Retrieved: September 30, 2014. 

==Reception==
The Defender was received "charitably" by audiences and critics alike. After a number of premieres and repeated broadcast showings beginning on March 22, 1990, the film became a cult hit for aviation enthusiasts. Montreal-based writer and photographer and NFB writer Carolyne Weldon characterized The Defender as "this doc is part of our deliciously nutty Outside the Box film channel, whose enticing tagline reads Experimental films, humorous films and films that make you go "Wha…?"" 

The Defender has won awards at numerous festivals. At the 1989 Yorkton Film Festival, the film won "Best Production", "Superchannel Award" and the Golden Sheaf "Best Director" Award for Stephen Low. The film was also nominated for two Genie Awards in 1990 (Best Direction, Documentary and Best Writing, Documentary categories). 

==References==
===Notes===
 
===Citations===
 
===Bibliography===
 
* Gunston, Bill. Aircraft of World War 2. London: Octopus Books Limited, 1980. ISBN 0-7064-1287-7.
* Mackenzie, S. P. The Battle of Britain on Screen: The Few in British Film and Television Drama. Edinburgh: Edinburgh University Press, 2007. ISBN 978-0-7486-2390-7.
 

==External links==
*  
*  
*  .

 
 
 
 
 
 
 
 
 
 
 
 