Our Feature Presentation
{{Infobox Film
| name           = Our Feature Presentation
| image          = 
| image_size     = 
| caption        = 
| director       = Gardner Loulan
| producer       = Gavin Heslet G. Lewis Heslet JoAnn Loulan
| writer         = Joseph Brady Gardner Loulan
| narrator       = 
| starring       = Chad Eschman Diane Tasca Ron Crawford Liam Brady Jeffrey Weissman Craig Lewis Dustin Diamond Erin Cahill
| music          = Haley Mancini
| cinematography = Dan Schmeltzer
| editing        = Joseph Dillingham
| distributor    = Aunt Colony 2008
| runtime        = 
| country        = United States English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}

Our Feature Presentation is a  , Retrieved 11-18-2008    , cast/crew/synopsis information, Retrieved 11-18-2008 

==Production==
Our Feature Presentation is a low-budget independent film. by Sean Howell, 
 , The Almamac, August 9, 2006, "Cover story: Their feature presentation- Portola Valleys Gardner Loulan comes home to make a movie and gets plenty of help from local residents", Retrieved 11-18-2008   Cast and crew include many professionals and volunteers.   The director is Gardner Loulan who is a   began May 15; principal filming began June 15 and wrapped on July 15.  Post-production was done in New York City and the film had its theatrical premiere in Atherton, California on December 21, 2008. {{cite web
|url= http://www.insidebayarea.com/movies/ci_11255948
|title= Coming Attractions: Hey, kids, lets make a movie
|author= Barry Caine 
|date= December 19, 2008
|publisher= Oakland Tribune
}} 

==Plot== Hollywood star and salt fortune heiress Jasmine Danell (Christina Rosenberg) wants to star in Codys film in order to make her current lover and famed French-Canadian director LeStat LeChaton (Craig Lewis), jealous.  The residents of town react to the news by confronting Cody, and one by one lobbying to be a part of the film. His ego and ambition overpower Cody, and he allows the people of the town to distract and corrupt his initial vision.  Jasmine stars in his film, and Codys brother auctions the directorial role to LeChaton much to Codys dismay.  Finally, and after significant personal compromise, with his production usurped and his unprofessional cast and crew, Cody attempts to sabotage his set only to realize it has taken on a life of its own.

==Partial cast==
* Chad Eschman as Cody Weever 
* Ron Crawford as Alexander Weever
* Jeffrey Weissman ...  Hugo Wilmington
* Dustin Diamond as Mr. Renolds
* Erin Cahill as Mrs. Renolds
* Elissa Stebbins as Finny Wong
* Diane Tasca as Helen Weever 
* Craig Lewis as Lestat LeChaton
* Liam Brady as Kenneth Weever 
* Christina Rosenberg as Jasmine Danell
* Maggie VandenBerghe as Sam Biscotti 
* Haley Mancini as Michelle Biscotti 
* Karen Hager as Judy Templeton
* Brandon Alexander as Bobby Roberts

==References==
 

==External links==
* 
* 
* 
* 
* 
* 
* 
* 

 