Spirit of the Forest (film)
{{Infobox film
| name=Spirit Of The Forest
| image=
| caption=
| director=David Rubin
| producer= Manolo Gómez
| writer=Beatriz Iso
| starring=Sean Astin Giovanni Ribisi Ron Perlman Anjelica Huston
| music=Arturo B. Kress
| cinematography =
| editing=
| studio=Dygra Films
| distributor=
| released= 
| runtime=90 minutes
| country=Spain
| language=English
| budget=
| gross=
}}
Spirit of the Forest, ( ) is a 2008 Spanish computer-animated family film by Dygra Films and sequel to The Living Forest. The film was released in Spain on September 12, 2008. 

==Plot==
Mrs. DAbondo wants the forest of the living trees to be cut down to make way for a highway, but Furi, Cebolo, Tigre and other animals manage to defeat her to save it.

==Voice cast== English speaking version includes:
*Sean Astin as Furi
*Giovanni Ribisi as Cebolo
*Ron Perlman as Oak
*Anjelica Huston as Mrs. DAbondo

===Additional Voices===
Nevertheless, most of the voice production for this film was recorded in London at St Annes Court, now part of Ascent Media. English voices include talented actors such as
*Tom Clark-Hill as Tigre, Mr. DAbondo and Triston
*Jo Wyatt Pearl
*Eric Meyers
*Laurence Bouvard as Linda
*Martin T. Sherman as Hu-Hu and Piorno

Recording sessions were cast and directed by Xevi Fernandez, who specialises in Spanish-English-Spanish film dubbing. The cast of actors (above) were recorded in Hollywood.

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 

 
 