Little Rose
{{Infobox film
| name           = Little Rose
| image          = LittleRose2010Poster.jpg
| alt            =  
| caption        = Polish film poster
| director       = Jan Kidawa-Błoński
| producer       = 
| writer         = 
| screenplay     = 
| story          = 
| starring       = Andrzej Seweryn Magdalena Boczarska Robert Więckiewicz
| music          = Michał Lorenc
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 118 minutes
| country        = Poland
| language       = Polish
| budget         = 
| gross          = 
}}

Little Rose ( ) is a 2010 Polish drama film directed by Jan Kidawa-Błoński.

== Plot ==
  Zionist counter-revolutionary hiding his Jewish name Wajner behind the Polish name Warczewski.
 Polish government stops a theatre play due to anti-Soviet propaganda, Warczewski calls a meeting of the PEN Club to protest. Kamila is the secretary of the meeting, and she forwards the minutes to the State Security with her termination of cooperation. She wants to marry the professor.

Roman is desperate in his jealousy and informs Warczewski about Kamilas collaboration with the State Security. After the suppression of the riots Władysław Gomułka frames Warczewski as an example of Jewish anti-socialist elements in Polish society. The Antisemitism#Twentieth century|anti-Semitic mood forces 15,000 Jewish citizens to leave Poland. At the same time, however, Rożek, whose name was originally Jewish Rosen, is unmasked and released from State Security. He also has to leave the country. In a final act of jealousy, he kills the professor.

== Cast ==
* Andrzej Seweryn – Adam Warczewski 
* Magdalena Boczarska – Kamila Sakowicz Rózyczka 
* Robert Więckiewicz – Roman Rożek
* Jan Frycz – Wasiak 

== External links ==
* 
* 

 
 
 
 
 