There Be Dragons
 
{{Infobox film
| name           = There Be Dragons
| image          = There be dragons poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Roland Joffé
| rating         = PG-13
| producer       = Roland Joffé Ignacio G. Sancha Ignacio Núñez Guy J. Louthan
| writer         = Roland Joffé
| starring       = Charlie Cox Wes Bentley Dougray Scott Unax Ugalde Olga Kurylenko Golshifteh Farahani Geraldine Chaplin Rodrigo Santoro
| music = Stephen Warbeck (original) Robert Folk (American re-cut)
| cinematography =  Gabriel Beristain
| editing        =  Richard Nord
| studio         =
| distributor    =
| released       =  
| runtime        = 122 minutes Spain
| country        = United States Spain Argentina
| language       = English
| budget         = $36,000,000 
| gross          = $4,372,642 
}} historical epic film written and directed by Roland Joffé. It is a drama set during the Spanish Civil War of the 1930s and features themes such as betrayal, love and hatred, forgiveness, friendship, and finding meaning in everyday life. The film was released on 6 May 2011. It includes the story of soldiers, a journalist, his father, and a real life priest, Josemaría Escrivá, the founder of Opus Dei who was canonized as a Roman Catholic saint. 

The film stars Charlie Cox, Wes Bentley, Rodrigo Santoro, Derek Jacobi, Geraldine Chaplin, Jordi Mollà, Golshifteh Farahani, Dougray Scott, Olga Kurylenko, Unax Ugalde and Lily Cole.

==Story and themes==
Director Roland Joffé said There Be Dragons is "a story about people trying to find meaning about their lives."    The epic film tells the story of a Spanish journalist, Robert, who is mending relations with his dying father, Manolo, who took part in the Spanish Civil War. The journalist discovers through his investigations that his father was a close childhood friend of Josemaría Escrivá, a candidate for sainthood, with whom he had a complicated relationship.   Manolo became a soldier during the Spanish Civil War and became obsessed with a beautiful Hungarian revolutionary, Ildiko. She rejects him and gives herself to a brave militia leader Oriol. Manolo becomes jealous and takes a path of betrayal. 

The film includes the early life of Josemaría Escrivá, the controversial founder of Opus Dei. Escrivá, who died in 1975, was canonized by John Paul II in 2002. Joffé, who initially shied away from the project, was "ultimately intrigued by the chance to dramatize the life of a modern-day saint, particularly considering Escrivás liberating view that a path to God could be found in an ordinary life." 

There Be Dragons is a drama which explores themes such as betrayal, forgiveness, friendship, and finding the meaning of life in everyday life. According to Joffé, they are "making a film about love, human love and divine love, about hate, about betrayal, about war, about mistakes, about everything it is to be a human being."      The theme of forgiveness, says Charlie Cox, who plays Josemaría Escrivá|St. Josemaria, is "always going to be a key when youre talking about Christianity at all, especially if you’re talking about a man who is canonized." Josemaria, Cox adds, "understood that the reason one must forgive is because that hatred and that anger and that resentment lives in you."   
 wobbly agnostic" The Mission which deals with a Jesuit mission in South America, said that he is "very interested in the idea of embarking on a piece of work that took religion seriously on its own terms and didnt play a game where one approached religion denying its validity."   

"Reconciliation matters" is the main take away message that Joffe expects from the viewers. Life, he said, is an opportunity to love: "Its a choice, and in making that decision you become free. You do not become free when you hate. The weird thing is when you really love, you feel it like a breath of freedom, you think ‘Oh my God, I’ve chosen this, and it’s beautiful’.”    He emphasized that Christianity is about love and the teaching of St. Josemaria "encourages a spiritual relationship with God in very simple things, in cooking a meal, being with one’s family, or even having a fight."  Joffé states that this is "a film about what it means to be a saint in this day and age."   
 here there be dragons" from the Latin hic sunt dracones, an ancient way of denoting in maps a place where there is danger, or an unknown place, a place to be explored.

==Cast==
* Charlie Cox as Josemaría Escrivá
* Wes Bentley as Manolo
* Dougray Scott as  Robert
* Golshifteh Farahani as Leila
* Olga Kurylenko as Ildiko
* Rodrigo Santoro as Oriol,
* Derek Jacobi as Honorio
* Lily Cole as Aline
* Unax Ugalde as Pedro

  plays a young Hungarian woman fighting with the International Brigades.]]

==Production==
The film is produced by Roland Joffé, who is the director. Guy J. Louthan and Opus Dei members Ignacio G. Sancha and Ignacio Núñez are also producers.
 Antena 3, the first private station in Spain, is also funding the film. The production services have been provided by Morena Films of Spain and Historias Cinematográficas of Argentina.
 religious epic, reported that a different script written by Barbara Nicolosi was first offered to Hugh Hudson and Alejandro González Iñárritu, who both turned it down. Joffé also initially turned down the offer to work as the films director. "But he said he reconsidered after he saw a video of Escrivá answering a question from a Jewish girl who wanted to convert to Catholicism. Escrivá told her that she should not convert, because it would be disrespectful to her parents. I thought this was so open-minded, Mr. Joffé said."  At that point, Joffé signed on to direct, with the condition of writing a new screenplay from scratch and becoming a producer. "In writing his own script, Mr. Joffé came up with a convoluted plot in which a young journalist discovers that his estranged father has a long-buried connection to Escrivá," reported the Times.  Joffé traveled to Spain, Italy and South America to do additional research in order to write his script.  After an official credits arbitration conducted by the Writers Guild of America (WGA), the Guild has decided that the writing credits of the film will be "Written by Roland Joffé", which is the highest writing credit possible. With this decision, the WGA has confirmed that Joffés screenplay is completely original and that the script written by Barbara Nicolosi bears no relationship whatsoever with the film There Be Dragons.

In the press conference held in Buenos Aires on 24 August 2009, Sancha stated that "our role is to create a space of free creativity for Roland, who has absolute free hand as a filmmaker. The value of the project lies in the fact that someone completely independent from the subject or the Catholic Church, and who is an agnostic, is portraying Josemaría according to his own view."

Director Roland Joffé wanted to share Josemarias story on film because he admired the way the saints faith influenced his day-to-day life. "Josemaria’s idea was that you find sanctity; you find your religious experience not only in liturgical things or in the church, but in the very act of living in your daily life," Joffé reported to CBN.com. 

There Be Dragons features Argentine production director Eugenio Zanetti, who won the Oscar in 1996 for Restoration (1995 film)|Restoration. The costume designer is Yvonne Blake, who won an Oscar for Nicholas and Alexandra and designed the costumes for Superman (1978 film)|Superman. Two-time Oscar winner Michèle Burke is in charge of the special make-up effects. Stephen Warbeck, who won an Oscar for Shakespeare in Love, composed the original score.
 The Mission. Luis Gordon, a former spokesman of the prelature of Opus Dei, stated that "The film team asked us for help in gathering information and we gave them access to the documentation."   

To portray Madrid in the 1930s, a part of the film was filmed in Luján, Buenos Aires|Luján, Argentina.   

The film was heavily re-edited by Ken Blackwell and re-released in the United States in January 2012 with the sub-title "Secretos de Pasión". The new cut of the film was re-scored by Robert Folk, and a soundtrack was released on Varese Sarabande Records. 

==Reactions==
The film was not well received either at the box office or by critics. Based on 19 published review,  ). 

According to Joaquin Navarro-Valls, one of the investors, the film "has started a movement of many people who feel moved to forgive. The producers are daily receiving messages of thanks (some are on the Internet) from people who see the film and decide to return home after years of separation, from spouses who are reconciled, from parents and children who have come to accept one another again, from others who return to God after a long time of being distanced from him." 

==See also==
* List of Spanish Civil War films

==References==
 

==External links==
*  
*  
*  
*  
*  
*  
*  
*  , Spanish review by Enrique Sánchez Costa.

===Commentary during production===
*  
*  
*  
*  
*  
*  
*  
*  
*    

===Reviews===
*  

 

 
 
 
 
 
 
 
 
 
 
 
 