Bartleby (1976 film)
Bartleby French drama film directed by Maurice Ronet and starring Michael Lonsdale, Maxence Mailfort and Maurice Biraud.  It is an adaptation of the short story Bartleby by Herman Melville.

==Plot==
Bartleby is a loner who gets hired as a clerk when a lawyer is deperate to find a reliable assistant. Unfortunately Bartleby isnt happy with his work. He falls into passivity and depression. When the law office moves, Bartleby prefers to stay in the abandoned rooms. People get fascinated his strange behaviour.

==Cast==
* Michael Lonsdale as the bailiff
* Maxence Mailfort as Bartleby
* Maurice Biraud as Dindon
* Dominique Zardi as Cisaille
* Jacques Fontanelle as Gingembre
* Hubert Deschamps as the manager
* Albert Michel as the prison cook
* Philippe Brigaud as the vice director of the prison
* Michel Fortin as the taxi driver


==Release==
Bartleby was released in France in 1978.  

==References==
 

==External links==
* 

 

 
 
 
 
 
 


 