Tawaif (film)
{{Infobox Film 
  | name = Tawaif
  | image = 
  | caption = 
  | director = Baldev Raj Chopra|B. R. Chopra
  | producer = R.C. Kumar
  | writer = Aleem Masrur Dr. Rahi Masoom Reza  
  | starring = Rishi Kapoor Rati Agnihotri Poonam Dhillon Deepak Parashar Ravi
  | cinematography = 
  | editing = 
  | distributor =  1985
  | runtime = 160 min. Hindi
  | budget = 
  | preceded_by = 
  | followed_by = 
  }}
Tawaif is a Bollywood movie released in Bollywood films of 1985|1985. It was produced by R.C. Kumar and directed by Baldev Raj Chopra|B. R. Chopra.

==Plot==
The film mainly is about a tawaif (courtesan or some one who willingly or otherwise adopts the profession of entertaining others for a living), who accidentally enters the life of Dawood (Rishi Kapoor) who already has fallen in love with an upcoming writer (Poonam Dhillon). The situation becomes such that he has to claim that she (the Tawaaif) happens to be his wife. Soon the Tawaaif makes room in everybodys heart by her softness and intelligence, love and affection. It becomes difficult for Dawood as he cannot forget his first love (Poonam Dhillon), nor the Tawaaif (Rati Agnihotri) and also that he cannot tell the identity of Rati to anybody else. However, gradually all problems are solved, misunderstandings removed and Dawood willingly accepts Rati while Suleman Seth (Deepak Parashar) who also loves Poonam, accepts her. The songs of the film were nicely written by Hassan Kamal and composed by Chopras favourite Ravi. The song Tere Pyar Ki Tamanna and Bahut De Kar Di are very meaningful and ear-pleasing.

==Cast==
* Rishi Kapoor...Dawood Mohammed Ali Khan Yusuf Zahi 
* Rati Agnihotri...Sultana 
* Poonam Dhillon...Kaynat Mirza
* Ashok Kumar...Mr. Nigam
* Deepak Parashar...Sulaiman
* Asrani...Constable Pandya 
* Kader Khan...Rahim Sheikh
* Iftekhar...Lala Fakirchand 
* Sushma Seth...Amina Bai

==Soundtrack==
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Joban Anmol"
| Asha Bhosle
|-
| 2
| "Aai Khudai"
| Mahendra Kapoor
|-
| 3
| "Bahut Der Kardi"
| Asha Bhosle
|-
| 4
| "Mera Shauhar"
| Mahendra Kapoor, Asha Bhosle
|-
| 5
| "Aaj Ki Sham"
| Asha Bhosle
|-
| 6
| "Tere Pyar Ki Tamanna"
| Mahendra Kapoor
|}

==Awards==
It won two Filmfare awards in 1986
* Best Dialogue Writer for Dr. Rahi Masoom Reza 
* Best Story Writer for Aleem Masroor

== External links ==
*  

 
 
 
 
 
 


 