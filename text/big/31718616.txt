Always on Duty
{{Infobox film
| name           =Zu jeder Stunde
| image          = 
| image size     =
| caption        =
| director       =Heinz Thiel
| producer       =Siegfried Nürnberger
| writer         = Lothar Dutombé
| narrator       =
| starring       =Rolf Stövesand
| music          = Helmut Nier
| cinematography = Erwin Anders
| editing        = Wally Gurschke
| studio    = DEFA 
| distributor    = PROGRESS-Film Verleih
| released       = 29 January 1960
| runtime        = 85 minutes
| country        = East Germany German
| budget         =
| gross          =
| preceded by    =
| followed by    =
}} East German black-and-white film, directed by Heinz Thiel. It was released in List of East German films|1960. 

==Plot== Border Troops soldier Martin arrives in a village on the Inner German border. He falls in love with a local girl, Renate. Their relationship is opposed to by her father, who promised her to the son of farmer Grabow. When Grabow plans to leave to the West with the aid of the corrupt officer Zimmer, Martin discovers their plans and informs his superiors, although Zimmer was his friend.

==Cast==
*Rolf Stövesand as Martin Kraft
*Erika Radtke as Renate Wedel
*Hans-Peter Minetti as Hermann Höhne
*Roman Silberstein as Heinz Tröger
*Manfred Borgesas Schlegel
*Erich Franz as Otto Grabow
*Otmar Richter as Felix Grabow
*Rolf Ripperger as Fred Wedel
*Hans Finohr as Arthur Wedel
*Fritz Diez as Father Kraft, the priest
*Werner Lierck as Köhler
*Josef Stauder as Schröder
*Horst Kube as Erich Willembrot
*Harry Hindemith as Marian Klein

==Production==
The DEFA Commission reviewed 58 scripts that were proposed for filming in the years 1959/60. Four of those were dubbed as "aesthetic films", and were all centered on portraying Christians as backward and reactionary. Out of the four, State Secretary of Cinema Erich Wendt authorized one script, that became the basis to Always on Duty. Although the picture was produced, the improvement in church and state relations in East Germany during 1960 prompted several changes in the plot, and the pictures antagonists were not presented as devout Catholics. 

==Reception==
Miera and Antonin Liehm cited Zu Jeder Stunde as one of DEFAs "contemporary socialist films." Miera Liehm, Antonin J. Liehm . The Most Important Art: Soviet and Eastern European Film After 1945. ISBN 0-520-04128-3. Page 266.  The Der Monat journals critic wrote that while viewing the film, "the public could be impressed by the alertness of the Border Troops."  The German Film Lexicon regarded it as "unassuming, propagandistic, not persuading and artistically weak, as well as full of stereotypes."   

==References==
 

==External links==
*  on the IMDb.
*  on PROGRESS website.
*  on ostfilm.de.

 
 
 
 