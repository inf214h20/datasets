Det ender med bryllup
 
{{Infobox film
| name           = Det ender med bryllup
| image          = 
| caption        = 
| director       = Lau Lauritzen, Jr.
| producer       = Henning Karmark
| writer         = Peter Lind
| starring       = Poul Reumert
| music          = 
| cinematography = Rudolf Frederiksen
| editing        = Sven Ejlersen
| distributor    = ASA Film
| released       =  
| runtime        = 80 minutes
| country        = Denmark 
| language       = Danish
| budget         = 
}}

Det ender med bryllup is a 1943 Danish comedy film directed by Lau Lauritzen, Jr. and starring Poul Reumert.

==Cast==
* Poul Reumert - Fabrikant Steen Andersen
* Berthe Qvistgaard - Rideskolelæreinde Grethe Mikkelsen
* Ib Schønberg - Propagandachef Ib Holm
* Poul Reichhardt - Reklamefuldmægtig Poul Hammer
* Povl Wøldike - Sekretær Jørgensen
* Charles Wilken - Andersen
* Susanne Friis - Fru Andersen
* Knud Heglund - Hovmester Thomsen
* Sigurd Langberg - Berider Mikkelsen Henry Nielsen - Staldknægten Herluf
* Anita Prülaider - Irene
* Preben Kaas - Piccolo Poul Petersen

==External links==
* 

 

 
 
 
 
 
 
 
 
 


 
 