Mariona Rebull (film)
{{Infobox film
| name =   Mariona Rebull 
| image =
| image_size =
| caption =
| director = José Luis Sáenz de Heredia
| producer = 
| writer = Ignasi Agustí (novel)   José Luis Sáenz de Heredia 
| narrator =
| starring = José María Seoane   Blanca de Silos   Sara Montiel   Alberto Romea
| music = Manuel Parada 
| cinematography = Alfred Gilks 
| editing = Julio Peña
| studio = Estudios Ballesteros 
| distributor = Cibeles Film 
| released = 14 April 1947  
| runtime = 110 minutes
| country = Spain Spanish 
| budget =
| gross =
| preceded_by =
| followed_by =
}} historical drama novel of high society of late nineteenth century Barcelona. Mariona Rebull is unhappily married, and begins an affair. She is eventually killed on 7 November 1893 when an anarchist Santiago Salvador throws a bomb into the stalls of the Gran Teatre del Liceu, Barcelonas opera house. This was based on a real-life incident. 

==Cast==
* José María Seoane as Joaquín Rius 
* Blanca de Silos as Mariona Rebull  
* Sara Montiel as Lula  
* Alberto Romea as Señor Llobet 
* Tomás Blanco (actor)|Tomás Blanco as Ernesto Villar   Carlos Muñoz as Arturo 
* José María Lado 
* Adolfo Marsillach as Darío  
* Fernando Sancho as Señor Roig 
* Rafael Bardem as Señor Llopis  
* Mario Berriatúa as Desiderio  
* Manrique Gil as Señor Pàmies  
* Ramón Martori
* Cándida Suarez
* Rosita Yarza
* Carolina Giménez as Muchacha en El Liceo

== References ==
 
 
==Bibliography==
* Helio San Miguel, Lorenzo J. Torres Hortelano. World Film Locations: Barcelona. Intellect Books, 2013.

== External links ==
* 

 
 
 
 
 
 
 
 
 
 
 


 