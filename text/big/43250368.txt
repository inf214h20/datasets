Axel: The Biggest Little Hero
 
{{Infobox film
| name           = Axel: The Biggest Little Hero
| image          = 
| alt            = 
| caption        = 
| director       = Leo Lee
| producer       = Leilei Hu Leo Lee
| writer         = Leo Lee
| starring       = Ed Asner Tim Curry Matthew Lillard George Takei
| music          = 
| cinematography = 
| editing        = Leo Lee
| studio         = 
| distributor    = 
| released       =   }}
| runtime        = 80 minutes
| country        = China
| language       = 
| budget         = 
| gross          = 
}}
Axel: The Biggest Little Hero is a 2014 direct-to-video animated film.

==Plot==
Boca and his best friend Jono go out in search of a magical plant from the Bonta Forest that will feed their starving tribe, but they are threatened by the Lizard King who wants to take over the forest. The two become friends with a brave princess and a giant ostrich robot who assist them on their journey for the plant. 

==Cast==
*Yuri Lowenthal as Boca 
*Sarah Natochenny as Young Boca
*Colleen OShaughnessey as Jono   
*Jessica Paquet as Neepop
*Kate Higgins as Gaga 
*Veronica Taylor as Young Gaga
*Ed Asner as Bonta
*Tim Curry as Papa Qi
*Matthew Lillard as Lizard King
*George Takei as Elder

==Release==
The film was released on DVD in the United States on June 3, 2014. 

==References==
 

 


 