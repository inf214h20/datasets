Dathuputhran
{{Infobox film 
| name           = Dathuputhran
| image          =
| caption        =
| director       = Kunchacko
| producer       = M Kunchacko
| writer         = Kanam EJ
| screenplay     = Sathyan Sheela Jayabharathi  Rajasree
| music          = G. Devarajan
| cinematography =
| editing        =
| studio         = Excel Productions
| distributor    = Excel Productions
| released       =  
| country        = India Malayalam
}}
 1970 Cinema Indian Malayalam Malayalam film,  directed and produced by Kunchacko. The film stars Prem Nazir, Sathyan (actor)|Sathyan, Sheela and Jayabharathi in lead roles. The film had musical score by G. Devarajan.   

==Cast==
 
*Prem Nazir as Ponnachan Sathyan as Kunjachan
*Sheela as Gracy
*Jayabharathi as Annakutty
*Adoor Pankajam as Achamma
*Alummoodan as Kothan Mathai
*K. P. Ummer as Jose
*Kottayam Chellappan as Police Officer
*S. P. Pillai as Kunjavarachan
*Ushakumari as Omana
*Pankajavalli as Rahel
*Kaduvakulam Antony as Paulose
*Vijayasree as Vanaja
*PJ Antony as Maanichan
*Adoor Bhasi as Joses father
 

==Soundtrack==
The music was composed by G. Devarajan and lyrics was written by Vayalar Ramavarma.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Aazhi Alayaazhi || K. J. Yesudas || Vayalar Ramavarma || 
|-
| 2 || Swargathekkal || K. J. Yesudas || Vayalar Ramavarma || 
|-
| 3 || Theeraatha Dukhathin || P Susheela || Vayalar Ramavarma || 
|-
| 4 || Thurannitta Jaalakangal || P Susheela || Vayalar Ramavarma || 
|-
| 5 || Wine Glass || LR Eeswari || Vayalar Ramavarma || 
|}

==References==
 

==External links==
*  

 
 
 


 