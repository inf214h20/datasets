Tales from the Vienna Woods (1979 film)
{{Infobox film
| name           = Tales from the Vienna Woods
| image          = 
| caption        = 
| director       = Maximilian Schell
| producer       = 
| writer         = Christopher Hampton Ödön von Horvath Maximilian Schell
| starring       = Birgit Doll
| music          = 
| cinematography = Klaus König
| editing        = 
| distributor    = 
| released       =  
| runtime        = 90 minutes
| country        = Austria
| language       = German
| budget         = 
}}
 Best Foreign Language Film at the 52nd Academy Awards, but was not accepted as a nominee. 

==Cast==
* Birgit Doll as Marianne
* Hanno Pöschl as Alfred
* Helmut Qualtinger as Zauberkönig
* Jane Tilden as Valerie
* Adrienne Gessner as Alfreds Grandmother
* Götz Kauffmann as Oskar
* André Heller as Hierlinger
* Norbert Schiller as Rittmeister
* Eric Pohlmann as Mister Robert Meyer as Erich
* Martha Wallner as Alfreds Mother

==See also==
* List of submissions to the 52nd Academy Awards for Best Foreign Language Film
* List of Austrian submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 