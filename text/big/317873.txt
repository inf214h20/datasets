Xanadu (film)
{{Infobox film
| name = Xanadu
| image = Xanadufilm.jpg
| alt = KIRA
| caption = Theatrical release poster
| director = Robert Greenwald Lawrence Gordon Joel Silver
| writer = Richard Christian Danus Marc Reid Rubel
| starring = Olivia Newton-John Michael Beck Gene Kelly
| music = Barry De Vorzon
| cinematography = Victor J. Kemper
| editing = Dennis Virkler Universal Pictures
| released =  
| runtime = 96 minutes  
| country = United States
| language = English
| budget = $20 million
| gross = $22,762,571
}} romantic Musical musical fantasy Down to Earth.
 music by Newton-John, Electric Light Orchestra, Cliff Richard, and The Tubes. The film also features animation by Don Bluth.

Not a financial success, Xanadu earned mixed to negative critical reviews and was an inspiration for the creation of the Golden Raspberry Awards to memorialize the worst films of the year.

==Plot==
Sonny Malone is a talented artist who dreams of fame beyond his job, which is the non-creative task of painting larger versions of  , Simpson, he resumes painting record covers.

At work, Sonny is told to paint an album cover for a group called The Nine Sisters. The cover features a beautiful woman in front of an art deco auditorium (the Pan-Pacific Auditorium). This same woman collided with him earlier that day, kissed him, then roller-skated away, and Malone becomes obsessed with finding her. He finds her at the same auditorium, now abandoned. She identifies herself as Kira, but she will not tell him anything else about herself. Unbeknownst to Sonny, Kira is one of nine mysterious and beautiful women who sprang to life from a local mural in town near the beach. 
 Olympian Muse ("Kiras" real name is Terpsichore, the Muse of Dance). The other eight women from the beginning of the film are her sisters and fellow goddesses, the Muses, and the mural is actually a portal of sorts and their point of entry to Earth.
 Greek gods Zeus and Mnemosyne, recall her to the timeless realm of the Olympian gods. Sonny follows her through the mural and professes his love for her. A short debate between Sonny and Zeus occurs, with Mnemosyne interceding on behalf of Kira and Sonny. Kira herself then enters the discussion, saying the emotions she has toward Sonny are new to her; if only they could have one more night together, Sonnys dream of success for the nightclub Xanadu could come true. Zeus ultimately sends Sonny back to Earth. After Kira expresses her feelings for Sonny, Zeus and Mnemosyne decide to let Kira go to him for a "moment, or maybe forever." They cannot keep these straight because mortal time confuses them, and the audience is left to wonder her fate.

In the finale, Kira and the Muses perform for a packed house at Xanadus grand opening, and after Kiras final song, the Muses all return to the realm of the gods in spectacular fashion. With their departure, Sonny is understandably depressed. But that quickly changes when Danny has one of the waitresses bring Sonny a drink, because the waitress who brings him the drink looks exactly like Kira. Sonny approaches this seeming double and says he would just like to talk to her. The film ends with the two of them talking, in silhouette, as the credits begin to roll.

==Cast==
  
* Olivia Newton-John as Kira (Terpsichore)
* Michael Beck as Sonny Malone
* Gene Kelly as Danny McGuire
* Matt Lattanzi as young Danny
* James Sloyan as Simpson
* Dimitra Arliss as Helen
* Katie Hanley as Sandra
* Fred McCarren as Richie
* Ren Woods as Jo
* Melvin Jones as Big Al
 
* Ira Newborn as 40s Band Leader
* Jo Ann Harris as 40s Singer
* Wilfrid Hyde-White as Heavenly Voice #1
* Coral Browne as Heavenly Voice #2
* Darcel Wynne as Background Dancer
* Deborah Jennsen as Background Dancer
* Alexander Cole as Background Dancer
* Adolfo Quinones as Xanadu Dancer
* Matt Lattanzi as Xanadu/Background Dancer
 
The Muses
* Sandahl Bergman Lynn Latham
* Melinda Phelps
* Cherise Bate
* Juliette Marshall
* Marilyn Tokuda
* Yvette Von Voorhees
* Teri Beckerman
 
Members of the Tubes John "Fee" Waybill
* Rick Anderson
* Michael Cotten
* Prairie Prince
* Bill Spooner
* Roger Steen
* Vince Welnick
* Re Styles
 

Cast notes:
* Joe Mantegna was in the films cast, but the scenes he appeared in were deleted.

==Musical numbers==
 
The album grouped Olivia Newton-John (ONJ) and Electric Light Orchestra|ELOs songs into the opposite sides of the album, and some tunes were excluded from the album. The following is the actual order in the film:
* Instrumental medley of "Whenever Youre Away From Me" and "Xanadu", over first part of opening credits
* "Whenever Youre Away From Me" excerpt Danny playing on the clarinet, at the beach 
* Instrumental underscoring of "Xanadu" with Sonny drawing and painting
* Extended intro to "Im Alive" (only a portion of which is in the soundtrack album)
* "Im Alive (Electric Light Orchestra song)|Im Alive" (ELO) on the films music track, as Muse wall paintings come to life
* "Whenever Youre Away From Me" excerpt Danny again playing the clarinet, at the beach.
* "Magic (Olivia Newton-John song)|Magic" (ONJ) on the films music track, while Kira is roller skating in the dark auditorium while Sonny watches and talks to her You Made Me Love You" (ONJ) (non-soundtrack LP track released as B-side of the "Suddenly" single) on Glenn Miller record played by Danny in the ballroom of his home
* "Whenever Youre Away From Me" (Gene Kelly and ONJ) Danny and Kira singing and dancing in the ballroom. This song was heavily influenced by Frank Sinatra.  According to the DVD special, this was the last sequence filmed. Suddenly (ONJ duet with Cliff Richard) – on the track as Kira and Sonny roller-skate through the recording studio.
* "Dancin" (ONJ duet with The Tubes) – Danny and Sonny, in the auditorium, imagine differing visions of their ideal club. Sonnys hard-rocking glam band and Dannys Big Band female trio lip-synching to ONJs self-harmony musically and physically merge into a unified whole, leading to agreement on "Xanadu" as the name of the club
* "Dont Walk Away (Electric Light Orchestra song)|Dont Walk Away" (ELO) on the films music track during an animated sequence featuring Sonny and Kira as several animals, such as fish and birds All Over the World" (ELO) on the films music track in the "franchised glitz dealer" store, with Danny running through various dance steps, and some rollerskating, as he tries on different outfits
* "The Fall" (ELO) on the films music track, as Sonny roller-skates toward (and through) the Muse wall painting
* "Suspended in Time" (ONJ) – Kira sings
* "Drum Dreams" (ELO) (non-soundtrack LP track released as B-side of the "Im Alive" and "All Over The World" singles) beginning of Xanadu opening night roller disco sequence, with Danny leading the group on skates
* "Xanadu (Olivia Newton-John and Electric Light Orchestra song)|Xanadu" (ONJ and ELO) Kira sings
* "Fool Country" (ONJ) (non-soundtrack LP track released as B-side of the "Magic" single) Kira in various costumes, singing
* "Xanadu" reprise, Kira singing; dancing with the other 8 Muses; they disappear, then Kira disappears
* "Magic" (ONJ) reprise, on the films music track, fades out as Kira reappears
* Instrumental riff from "Xanadu", Kira and Sonny become silhouetted; "The End"
* "Xanadu" (ONJ and ELO) short version, over closing credits.

==Themes== Down to Earth was loosely used as the basis for Xanadu. In the film, Rita Hayworth played Terpsichore, opposite male lead Larry Parks, who played a producer of stage plays.

==Production==
  transformed into "Xanadu" via special effects.]]
 
The film was originally conceived as a relatively low-budget roller disco picture. As a number of prominent, A-list performers joined the production, it evolved into a much larger project, while retaining rollerskating as a recurring theme, especially in the final scenes of the clubs opening night. 
 Marvel Super Special #17  retained the more strongly emphasized connection between Sonny and the painting.
 Cover Girl, also appears in Xanadu and was Kellys final film role, except for compilation films of the Thats Entertainment series. Kenny Ortega and Jerry Trent served as choreographers. 

The Pan-Pacific Auditorium in Los Angeles was used for exterior shots of the nightclub. Xanadus nightclub interior was built on Stage 4 of the Hollywood Center Studios (1040 N. Las Palmas Avenue, Hollywood) beginning in 1979.  Sonny refers to the Auditorium as "a dump", which was a fair characterization of the Pan-Pacific by then. Danny jokes that "they used to have wrestling here", which was a true statement about the Auditorium. The building would be consumed by fire a decade later.

==Reception==
Xanadu has generally earned mixed to negative reviews. For example, Roger Ebert gave the film two stars, describing the film as "a mushy and limp musical fantasy" with a confused story, redeemed only by Newton-Johns "high spirits" and several strong scenes from Kelly. Moreover, Ebert criticized the choreography, saying "the dance numbers in this movie do not seem to have been conceived for film."    He noted that mass dance scenes were not photographed well by cinematographer Victor J. Kemper, who shot at eye level and failed to pick up the larger patterns of dancers, with dancers in the background muddying the movement of the foreground.  With a combination of contemporaneous and modern reviews, Xanadu today holds a "Rotten" rating of 39% from the review aggregator website Rotten Tomatoes.  

The film barely broke even at the box office in its initial release.  A double feature of Xanadu and another musical released at about the same time, Cant Stop the Music, inspired John J. B. Wilson to create the Golden Raspberry Awards (or "Razzies"), an annual event "dishonoring" what is considered the worst in cinema for a given year.    Xanadu won the first Razzie for Worst Director and was nominated for six other awards.
 cult audience.  
 soundtrack album (UK #2, US #4), however, was a major hit. It was certified Double Platinum in the US and Gold in the UK, and also spent one week atop the Cashbox and Record World Pop Albums charts. The soundtrack contained five Top 20 singles:
* "Magic" Olivia Newton-John (No. 1 (4 weeks) Pop, No. 1 (5 weeks) AC, certified gold)
* "Xanadu" Olivia Newton-John/Electric Light Orchestra (No. 8 Pop, No. 2 AC, No. 1 (2 weeks (UK)))
* "All Over the World" Electric Light Orchestra (No. 13 Pop, No. 45 AC)
* "Im Alive" Electric Light Orchestra (No. 16 Pop, No. 48 AC, certified gold)
* "Suddenly" Olivia Newton-John/Cliff Richard (No. 20 Pop, No. 4 AC)

==Home media== theatrical trailer and a photo gallery. A bonus music CD with the soundtrack album was included. The CD was the films standard soundtrack album, i.e. with no extras such as omitted tracks.

==Stage musical==
  Tony Roberts as Danny. In the musical, Kira is the Muse Clio, not Terpsichore. Jackie Hoffman and Mary Testa co-starred (in a plot twist new to the Broadway version) as "evil" Muse sisters. The show, which humorously satirized the plot of the film, was a surprise hit, and was nominated for several Tony awards. The original cast recording was released December 2007. The Broadway production closed on September 28, 2008 after 49 previews and 512 performances.   Playbill, January 20, 2007.  Retrieved on January 29, 2007.   A successful national tour followed.

==Awards and nominations==
 
* Ivor Novello Award Best Motion Picture Film soundtrack  Jeff Lynne
* Grammy Awards Best Female Pop Vocal Performance Magic Olivia Newton-John
* Young Artist Awards
:Nominated: Best Major Motion Picture Family Entertainment
  1st Golden Raspberry Award Worst Director (Robert Greenwald) Worst Picture Worst Screenplay Worst Actor (Michael Beck) Worst Actress (Olivia Newton-John) Worst Original Song ("Suspended in Time")
:Nominated: Worst "Musical" of Our First 25 Years
 

Note: Xanadu was the inspiration for the Golden Rasperry Awards

==See also==
* Muses in popular culture

==References==
 

==External links==
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 