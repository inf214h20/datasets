Tennessee Champ
{{Infobox film
| name           = Tennessee Champ
| image_size     = 200px
| image	         = Keenan Wynn in Tennessee Champ trailer.jpg
| caption        = Keenan Wynn from the trailer
| director       = Fred M. Wilcox
| producer       = Sol Baer Fielding
| writer         = Art Cohn, Eustace Cockrell (story)
| based on = The Lord in his corner
| starring       = Shelley Winters Keenan Wynn Charles Bronson
| music          = Conrad Salinger
| cinematography = George Folsey Ben Lewis
| distributor    = Metro-Goldwyn-Mayer
| released       = 1954
| runtime        = 73 minutes
| country        = United States English
| budget         = $548,000  . 
| gross          = $769,000 
| preceded_by    =
| followed_by    =
}} Dewey Martin, and Charles Bronson (credited as Charles Buchinsky), and directed by Fred M. Wilcox.

Mounted as a title to fill out double and triple bills (a B-movie), Tennessee Champ was one of several films Metro-Goldwyn-Mayer shot in its pet process of Ansco Color, a ruddy-looking process employed on the same years Brigadoon.
 A Place in the Sun (1951).

== Plot ==
Shelley Winters plays Sarah Wurble, whose husband, Willy (Keenan Wynn), is the larceny-inclined manager of an illiterate, and very religious boxer from Tennessee named Danny Norson (Dewey Martin). Gifted with a powerful punch and a nickname that gives the film its title, Danny mistakenly believes he killed a man defending himself in a street brawl, and goes on the lam as a prizefighter. His Christian convictions turn out to be both a source of inspiration and, ultimately, conflict when Willy urges him to throw a fight (while mistakenly fearing Willy will turn him in on the murder charge if he doesnt). Credulity flies out of the window when Danny discovers the man he is to take on in the fixed fight is actually the man he thought he killed, Sixty Jubel, The Biloxi Blockbuster (Charles Bronson). Dannys example of unwavering faith causes Willy to rethink his sinful ways.

==Reception==
According to MGM records the movie earned $555,000 in the US and Canada and $214,000 elsewhere, making a loss to the studio of $189,000. 

==References==
 

==External links==
* 
*  at TCMDB

 
 
 
 
 
 


 