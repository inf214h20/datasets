Doraemon: New Nobita's Great Demon—Peko and the Exploration Party of Five
{{Infobox film
| name           = Doraemon: New Nobitas Great Demon—Peko and the Exploration Party of Five
| image          = File:Doraemon Shin Nobita no Daimakyo ~Peko to 5-nin no Tankentai Poster.jpg
| caption        = Japanese theatrical poster
| director       = Shinnosuke Yakuwa
| producer       =
| writer         = Higashi Shimizu
| starring       = Wasabi Mizuta   Megumi Ohara   Yumi Kakazu   Tomokazu Seki   Subaru Kimura   Yu Kobayashi   Kenji Tada   Shun Oguri   Miku Natsume   Yoshi Yamada   Kotono Mitsuishi   Yasunori Matsumoto
| music          = Kan Sawada
| cinematography =
| studio         = Shin-Ei Animation
| editing        =
| distributor    = Toho
| released       =  
| runtime        = 109 minutes 
| country        = Japan
| language       = Japanese
| gross          = US$33,989,696 / 3,697,059,233.92 yen (Japan)
}}
  is a 2014 Japanese  .   There is an alternate translation of the title widely used on the internet: "Doraemon: New Nobitas Great Demon—Peko and the Exploration Party of Five".This movie was 5th highest grossing animated film of 2014 anime in japan. 

==Plot==
The movie starts with Jian, Shizuka, Nobita and Sonio discussing going for an adventure in their Summer Vacation. Sonio and Jian get tired of thinking about this, so they pass this responsibility to Nobita. Nobita asks Doraemon to do so and they launch a rocket on their roof to take photos of each and every small part of the Earth. Meanwhile Nobitas Mom, Tamako comes and asks Nobita to go and bring some vegetables and butter. Nobita accepts this job unlike his refusal in the animated episodes. On his way he passes through the Playground where he sees a dog alone. He feels sorry for the dog and continues to go to the market. In the market after he buys all the things his mother asked him to bring, he spares a few yen from his monthly pocket money and buys the cheapest sausages available at the butchers shop. On his way back he enters the playground and tells the dog which he saw earlier to leave him   alone when he gives him a few sausages. The dog barks friendly to Nobita and he gives a few sausages to the dog, but the dog follows him, and as soon as Nobita notices this he runs through a shortcut to his home. At home Nobita enters his room and asks Doraemon how many photos the remote sensing satellite would produce. Doraemon says about 900,000,000. After listening this Nobita asks Doraemon to find a better way to find a good place for their adventure rather than checking all the photos. Nobita wants to drink a juice, so he opens the door to get to the kitchen when he sees the dog which gives him a huge scare. His Mom runs upstairs ignoring the dog even she sees the dog   and says Nobita that her purse is missing. Nobita, Doraemon, Nobitas Mom and the dog go outside to find her purse. On the way the dog smells Nobitas Moms slipper and goes a place which is far away from Nobitas home and there Tamako finds her purse. Tamako allows the dog to stay in their home and names it Peko.    


==Reception==
As of May 4, 2014, the film has grossed US$33,989,696 / 3,697,059,233.92 yen in Japan. 

==Television broadcast==
The movie aired in Hindi on Disney Channel India on 13 December 2014 as Doraemon The Movie Nobita The Explorer Bow! Bow! 

==References==
 

==External links==
*    
*  

 
 

 
 
 
 
 
 
 
 


 