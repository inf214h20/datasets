Ręce do góry
{{Infobox film
| name           = Ręce do góry
| image          = 
| caption        = 
| director       = Jerzy Skolimowski
| producer       = 
| writer         = Jerzy Skolimowski Andrzej Kostenko
| starring       = Jerzy Skolimowski
| music          = 
| cinematography = Andrzej Kostenko Witold Sobocinski
| editing        = Grazyna Jasinska-Wisniarowska
| distributor    = 
| released       =   (originally filmed in 1967: withheld by censorship: re-edited with introduction 1981)
| runtime        = 76 minutes
| country        = Poland
| language       = Polish
| budget         = 
}}
 autobiographical films in which Skolimowski himself plays his alter ego, Andrzej Leszczyc. 

The film was originally made in 1967 in monochrome.  In a twenty minute section (filmed in colour) added by Skolimowski in 1981 he explains how the original was withheld by Polish censors of the time, and that this was a principal cause of his leaving his country; however following liberalisation in Poland, he was invited to resuscitate it. The introduction includes, apart from some fictional apocalyptic passages, shots of Beirut ruined by the civil wars of the 1970s, where Skolimowski is working as an actor on Volker Schloendorffs German film Die Fälschung (Circle of Deceit), and also shots of London featuring demonstrations in favour of Solidarnosc, Speakers Corner, and an exhibition of Skolimowskis own paintings. These sections include cameo roles by Bruno Ganz, David Essex, Mike Sarne and others. Some of the music in this introduction is from the 1970 choral work Kosmogonia by the Polish composer Krzysztof Penderecki.
 speed (although it is later revealed the pills are a placebo), and carousing in the cattle truck of a freight train, the group offers various satirical sidelights on Polish society of the 1960s. The characters also reflect that the truck may have been one of those in which the former generation were transported during World War II to the Nazi death camps.

The final credits show the actors as they are in 1981, with the exception of Bogumil Kobiela,who died in 1969.

The film was screened out of competition at the 1981 Cannes Film Festival.   

==Cast==
* Jerzy Skolimowski as Andrzej Leszczyc (Zastava)
* Joanna Szczerbic as Alfa
* Tadeusz Lomnicki as Opel Rekord
* Adam Hanuszkiewicz as Romeo
* Bogumil Kobiela as Wartburg

==External links==
*  

==References==
 

 

 
 
 
 
 
 
 
 