Glass Lips
{{Infobox Film
| name           = Glass Lips
| image          = 
| image_size     = 
| caption        = 
| director       = Lech Majewski
| producer       = Lech Majewski, Angelus Silesius
| writer         = Lech Majewski
| narrator       = 
| starring       = Patryk Czajka, Grzegorz Przybył, Joanna Litwin, Ryszarda Celinska, Dorota Lis
| music          = Lech Majewski, Jozef Skrzek
| cinematography = Lech Majewski
| editing        = Eliot Ems, Norbert Rudzik
| distributor    = Opus Film
| released       = 
| runtime        = 100 minutes
| country        = Poland USA
| language       = 
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}

Glass Lips is a feature film with almost no words, directed by Lech Majewski.

The film began life as an installation of interrelated short films, entitled Blood of a Poet (alluding to   in Łódź from 2006-05-19 to 2006-06-18 (Polish-language name “Krew poety”).,     The installation also was exhibited at the Wrocław Opera House from 2006-07-20 to 2006-07-30 during the Era New Horizons Film Festival; and, in the same year, in Gallery 2Πr, Poznań, and National Museum, Szczecin. It was described there as:

:a video art screened on eight walls, the remaining works will be shown in loops on 24 screens. Entering this labyrinth, every viewer walks from one stage to another and builds his or her private story out of ready elements. 

Blood of a Poet was further exhibited as a video installation from 2007-02-08 to 2007-02-18 in Berlin, during Berlinale. There it was described as “a circuit of 33 short films and a series of interrelated photographic works”. 

On June 6, 2007,  , and on 19 screens inside the Teatro Junghans on Giudecca.

The short films of the video installation Blood of a Poet were assembled into a single feature film entitled Glass Lips, premiering in that form in  February 2007 in Vancouver.,   

In Poland the film in its feature-length format is known as Szklane usta and premieres on 2007-03-23.,   
 University of Colorado International Film Series in March/April 2007, the National Gallery of Art in Washington, D.C.,   and Locarno Film Festival in August 2007; The Art Institute of Chicago showed it in September 2007; the UCLA Film Archive in October, and in November it was presented by SIFF Seattle; Portland Art Museum; Cleveland’s Wexner Arts Center; and Berkeley Art Museum / Pacific Film Archive.

On November 7, 2007, Glass Lips opened in   creates an aesthetic of dysfunction that’s as beautiful as it is disturbing. After a while the film’s expressiveness becomes so hypnotic that it’s difficult not to make your own connections.”   film critic, V. A. Musetto, gave the film three and a half stars (out of four) and wrote: “The hypnotic, painterly images of Glass Lips combine with haunting music in one of the most unusual, beautiful films of the year.”
 mental asylum; he recalls events from his life beginning with his birth. Many of the sequences of the film are surrealistic. None of the actors speak any words, but the electronic voice of an answering machine gives the date at one point, and there is a voice-over song that gets repeated.

The film includes some shocking images, such as a woman who gives birth to a large bloody rock, and a man eating dog food.

In addition to directing, Majewski is credited as the writer, producer, and cinematographer. 

==Notes==
 

==External links==
*  
*  
*  
* 
* 
* 

 
 
 
 