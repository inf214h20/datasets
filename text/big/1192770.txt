How to Kill Your Neighbor's Dog
{{Infobox film
| name           = How To Kill Your Neighbors Dog
| image          = Howtokilldogdvd.jpg
| image_size     = DVD cover
| caption        = Release Poster
| director       = Michael Kalesniko
| producer       = Michael Nozik, Nancy M. Ruff and Brad Weston
| writer         = Michael Kalesniko Robin Wright Penn and Jared Harris
| music          = David Robbins
| cinematography = Hubert Taczanowski	
| editing        = Pamela Martin
| distributor    = Lonsdale Productions
| released       =  
| runtime        = 107 minutes
| country        = United States
| language       = English
| budget         = $7,300
| gross          = $48,564
}}

How to Kill Your Neighbors Dog is a 2000 American black comedy film written and directed by Michael Kalesniko and produced by Michael Nozik, Nancy M. Ruff and Brad Weston.

==Plot== Robin Wright Penn) is determined to have a baby; he finds himself bonding with a new neighbors lonely young daughter (Suzi Hofrichter) who has mild cerebral palsy; and during one of his middle-of-the-night strolls, he encounters his oddball doppelgänger (Jared Harris) who claims to be Peter McGowan and develops a friendship of sorts with him.

==Cast==
* Kenneth Branagh as Peter McGowen Robin Wright Penn as Melanie McGowen
* Jared Harris as "Pseudo"/"False" Peter McGowen
* Suzi Hofrichter as Amy Walsh
* Lynn Redgrave as Edna
* Peter Riegert as Larry
* David Krumholtz as Brian Sellars
* Johnathon Schaech as Adam
* Kaitlin Hopkins as Victoria
* Suzy Joachim as Allana
* Brett Rickaby as Janitor
* Lucinda Jenney as Trina Walsh
* Derek Kellock as Amys Father
* Stacy Hogue as Babysitter
* Peri Gilpin as Debra Salhany

==Production==
Petula Clarks recordings of "I Couldnt Live Without Your Love" and "A Groovy Kind of Love" were heard during the opening and closing credits respectively, and "Downtown 99", a disco remix of her 1964 classic "Downtown (Petula Clark song)|Downtown", was heard during a party scene. Additional songs originally recorded by Petula Clark were sung by the character of Brian Sellars throughout the film.

==Critical response==
In his review in The New York Times, Stephen Holden described the film as "a Hollywood rarity, a movie about an icy grown-up heart-warmed by a child that doesnt wield emotional pliers to try to squeeze out tears . . . It is a tribute to Mr. Branaghs considerable comic skills that he succeeds in making a potentially insufferable character likable by infusing him with the same sly charm that Michael Caine musters to seduce us into cozying up to his sleazier alter egos . . . Mr. Kalesnikos satirically barbed screenplay, whose spirit harks back to the comic heyday of Blake Edwards, stirs up an insistent verbal energy that rarely flags."
 Shakespeare screen performance, grounds even the softest moments in the angry revolt of his wit." Justine Elias of The Village Voice stated it was "slight but unendurable . . . its fractured time frame gets confusing." 

The film was the prestigious closing night film at the 2000 Toronto International Film Festival and won multiple festival awards. It was released as Mad Dogs and Englishmen in Australia.

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 