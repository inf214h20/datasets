Aatamin puvussa ja vähän Eevankin (1931 film)
{{Infobox film
| name           = Aatamin puvussa ja vähän Eevankin
| image          =
| image size     =
| caption        =
| director       = Jaakko Korhonen
| producer       =
| writer         = Jaakko Korhonen Yrjö Soini
| narrator       =
| starring       = Joel Rinne Elsa Segerberg Yrjö Tuominen
| music          = Martti Similä
| cinematography = Eino Kari
| editing        =
| distributor    = Suomi-Filmi
| released       = 2 February 1931
| runtime        = 104 min.
| country        = Finland
| language       = Finnish
| budget         =
| gross          =
| preceded by    =
| followed by    =
}} 1931 Finland|Finnish comedy film directed and written by Jaakko Korhonen based on the novel by Yrjö Soini. The films runtime is 104 minutes. It is the first Finnish film to have sound.

==Cast==
*Joel Rinne as Aarne Himanen
*Elsa Segerberg as Alli
*Yrjö Tuominen as Paavo Kehkonen
*Kaarlo Saarnio as Viirimäki
*Uuno Montonen as Kalle Vikström
*Heikki Välisalmi as Police chief
*Anja Suonio-Similä as Liisa
*Olga Leino as  Old woman
*Ellen Sylvin as Lehtinens daughter
*Kaija Suonio as  Mrs. Lehtinen
*Runar Idefeldt as Constable Lehtinen

==See also==
*Aatamin puvussa ja vähän Eevankin (1940 film)
*Aatamin puvussa ja vähän Eevankin (1971 film)

==External links==
*  

 
 
 
 
 

 
 