Claire (film)
{{Infobox Film
| name           = Claire
| image          = Clairefilm.jpg
| caption        = Region 1 DVD Cover
| imdb_rating    = 
| director       = Milford Thomas
| producer       = Milford Thomas
| writer         = Milford Thomas
| starring       = Toniet Gallego Mish P. DeLight James Ferguson Pat Bell Katherine Moore Anna May Hirsch Allen Jeffrey Rein    
| music          = 
| cinematography = Jonathan Mellinger
| editing        = Anne Richardson
| distributor    = 
| released       = November, 2001
| runtime        = 57 minutes
| country        = United States
| language       = No dialogue
| budget         = 
| box office     = 
| preceded_by    = 
| followed_by    = 
}}
Claire is a 2001 film by Milford Thomas.
 South who find the moon princess in an ear of corn. Milford Thomas took the prize for Best First Feature (Special Mention) with this little number at the San Francisco International Lesbian & Gay Film Festival in 2002. Claire is an homage to early cinema. Accompanied by its silent-era camera, all set-design and special effects were achieved the old-fashioned way. Canvas backgrounds are present, alongside semi-hidden wires, multiple exposures and was filmed with a hand-cranked 35mm camera.

==Plot==
When the curtains open, viewers find themselves caught within a nightmare that strongly contrasts the dreaminess of sequences to come: a young girls birthday party is cruelly interrupted by Joshs (Mish P. DeLight) loss of custody over her.  He wakes in a fright and squeezes the hand of his husband (James Ferguson) for comfort. Early on, a theme in Claire is identified here: the transcendence of love beyond social norms.  The audience is briefly walked through the pairs daily routine.  One exception emerges after the couple has lain down the following night: the appearance of Claire; their unexpected miracles, just a tiny thing inside an iridescent ear of corn.  As the story progresses and Claire grows into the shape of a young woman, she enchants onlookers with her beauty and readings of fantastic poetry in various languages.  Richard (Allen Jeffrey Rein) is particularly entranced; and gives her his copy of Shelleys work as a token.  It is in him that Claire discovers her miraculous healing powers proceeding his dive from a cliff in an attempt to win her affection.  While exemplifying the nontraditional family---two men lovingly raising a child of the moon---the film celebrates the diversity of family and also addresses the grief of losing a loved one.

==Cast==
*Toniet Gallego as Claire, on whom this story revolves
*Mish P. DeLight as Josh, one of the men that find Claire in a cornstalk whom adopts her as his own
*James Ferguson as Walt, husband to Josh; Claires second father
*Allen Jeffrey Rein as Richard, Claires (almost) romantic interest
*Pat Bell as the antagonist of the story
*Anna May Hirsch as Miss Earwood
*Katherine Moore as Kat

==Soundtrack==
The film is accompanied solely by Orchestra De Lune conducted by Anne Richardson.  It was performed live when the film was originally shown at the San Francisco International Lesbian & Gay Film Festival.

==External links==
* 
*  
*  

 
 
 
 
 
 
 
 
 