Shree Krishna Gaarudi
{{multiple issues|
 
 
}}

{{Infobox film
| name           = Shree Krishna Gaarudi
| image          =  
| image size     = 
| caption        = 
| director       = Hunsur Krishnamurthy
| producer       = K. M. Naganna
| story          = Bellave Narahari Shastri
| screenplay     = Hunsur Krishnamurthy
| narrator       =  Rajkumar Revati Narasimha Raju
| music          = Pendyala Nageshwara Rao
| cinematography = G. Dorai
| editing        = Manikyam
| studio         = Nandi Pictures
| distributor    = 
| released       =  
| runtime        =  
| country        = India
| language       = Kannada
}}

Shree Krishna Gaarudi ( ) is 1958 Indian Kannada language film in the mythological genre written and directed by Hunsur Krishnamurthy. It narrates Bheema and Arjunas tale of pride crushed by Krishna, after the Kurukshetra War.

==Plot==
The Kurukshetra War is over and Dharmaraya ascends the throne of Hastinavati. Dharmaraya distributes powers and Bheema and Arjuna are unhappy with the powers and responsibilities vested to them. They fume in private, that they were the reason for Pandavas victory in Kurukshetra War, but have to be subservient to nakula and sahadeva.

Sri Krishna senses this and disguises as Gaarudi, a street player, skilled in warfare. He reaches Hastinavati, challenging men to fight him. He gathers fame, as unbeatable warrior.

When news reaches Bheema and Arjuna, in a fit of rage, they challenge Gaarudi. Arjuna has to set a bow and fails. They get beaten, are pushed below earth and suffer indignity. Bheema has to fight a giant snake that has bound him and made immovable. Arjuna has to fight off a family and infamy. Both suffer, because of their pride and forgetting Lord Sri Krishna.

After what seems a long time, they pray to Krishna and are able to come over their agony.

When they reach their palace at Hastinavati, Lors Krishna informs them, it was he, as Gaarudi, who taught them the lesson.

Nakula and Sahadeva pray to Krishna, before starting the task and easily finish it off.

Shri Krishnas mighty power and bliss fills Bheema and Arjuna. They happily perform the duties, bestowed on them.

Songs by Pendyala Nageshwara Rao were popular. 

==References==
 

 
 
 
 


 