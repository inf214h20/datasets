Porky's Party
{{Infobox Hollywood cartoon
|series=Looney Tunes (Porky Pig)
|image=
|caption=
|director=Robert Clampett
|story_artist= Charles Jones Norman McCabe
|voice_actor=Mel Blanc
|musician=Carl Stalling
|producer=Leon Schlesinger
|studio=Leon Schlesinger Studios
|distributor=Warner Bros.
|release_date=1938
|color_process=Black-and-white
|runtime=8 minutes
|preceded_by=Porky the Fireman
|followed_by=Porkys Spring Planting
|movie_language=English
}}
 animated short Bob Clampett, which starred Porky Pig and his dog "Black Fury", as well as two characters named Penguin and Goosey, and an unnamed silkworm. In this short, Porky prepares for his birthday party, but antics ensue. 

==Plot==
The cartoon begins with Porky Pig lighting candles on a birthday cake while singing and stuttering, Happy Birthday to himself.

Porky receives a package from his uncle Pinkus. Inside is a tiny silkworm. The silkworm knits garments whenever the word sew is spoken. Porky commands the worm to sew and it sews a sock as Porky and Black Fury look on in awe. Porky gives the command again.  The worm sews a brassiere, which Porky disposes of bashfully.

Porky and his dog Black Fury enter the bathroom, where Porky applies hair-growth formula on his own head, producing no results.  Then, needing to get ready, Porky hurriedly leaves the bathroom. Black Fury takes the formula and seeing that it contains 99% alcohol, begins to drink it.  He becomes shaggy after ingesting the formula. He becomes severely intoxicated, loudly shouting "Happy Birthday!"

Meanwhile, Porky hears a knock at the door.  It is his friend Penguin, who rushes in and begins wolfing down ice cream. Goosey saunters in and holds out his hand for Porky to shake.  The hand is a prop, adorned with a sign reading "Happy Birthday, Fat Boy!". Porky chuckles at Goosey, stating "Hes so silly."

Porky repeatedly stutters the word so, and the silkworm in Porkys pocket begins sewing garment after garment.  As garments come from underneath Porkys jacket, he notices they are knickers|womens underwear and brassieres; he hides them guiltily. He tosses the silkworm away.

The silkworm lands on Penguins ice cream, and continues sewing garments, which end up in the ice cream. Disgusted, Penguin pulls a sock out of his mouth. As he has difficulty swallowing, a top hat forms in his mouth. The hat pops up, and Penguins head assumes the shape of it. Failing to quell the hat, Penguin shouts for Gooseys help, who rams Penguins head into the wall, hits him with a mallet, and slams a washtub over his head, to no avail.
 mad dog, and the party guests run around the house with Black Fury, not realizing that the "rabid dog" is actually Porkys pet.

After several more antics, the shaving cream is removed, and Porky sees that it is just Black Fury. Penguin, angrily rolls up his "sleeve" and stares Black Fury down, uttering "so..." in anticipation of a fight. This sets off the silkworm, who wraps Penguin up into a state of Mummy|mummification.

==Censorship==
*On the Nickelodeon redrawn version, the following parts were edited  :
**The scene in which Goosey helps Penguin flatten the top hat that keeps growing from his head was shortened (though this part was cut short for time reasons, as opposed to inappropriate content).
**The scene in which Porky lights a match in the closet and sees Black Fury beside him.

==Availability==
*This cartoon is available (uncut, uncensored, and in its original black and white format) on the third volume of the Looney Tunes Golden Collection, with a special optional commentary track by Ren and Stimpy creator John Kricfalusi and animator Eddie Fitzgerald, as well as a storyboard featuring drawings that originally had Gabby Goat and Petunia Pig as party attendees, but, for reasons unknown, they were replaced by Goosey and Penguin.

==Production Notes==
*Chuck Jones was an animator in Clampetts unit at this time, and his work can be easily identified, particularly in the scene where Black Fury gets drunk on hair restoration tonic. Jones became a director and was awarded his own unit shortly after this cartoon was produced.

==References==
 

==External links==
* 

 
{{Succession box|
before=Porky the Fireman|
title=Cartoons Featuring Porky Pig|
years=1938|
after=Porkys Spring Planting
}}
 

 
 
 
 
 
 