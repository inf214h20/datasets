Morcha (1980 film)
{{Infobox film
| name           = Morcha
| image          =
| image size     =
| caption        = Raveekant Nagaich
| producer       = Rakesh Gopikrishna Global Entertainers
| writer         =
| dialogue       =
| screenplay     =
| story          = Ramesh Pant
| based on       =
| narrator       =
| starring       = Ravi Behl, Aruna Irani, Suresh Oberoi, Jagdeep, Shakti Kapoor, Jayshree T., Mac Mohan
| opentheme      =
| music          = Bappi Lahiri Ramesh Pant (lyrics), Faruk Kaisar (lyrics)
| cinematography =
| editing        =
| studio         =
| distributor    =
| released       =   
| runtime        =
| country        = India
| language       = Hindi
| budget         =
| gross          =
}}
 Raveekant Nagaich. This action drama casts Ravi Behl, Aruna Irani, Chandrashekhar,  Jagdeep,  Jayshree T., Mac Mohan, Shakti Kapoor, Suresh Oberoi.

==Plot==
 

==Cast==
*Ravi Behl
*Aruna Irani
*Ammi Mahendra
*Anita
*Chandrashekhar
*Ganesh
*Jagdeep
*Jayshree T.
*Mac Mohan
*Mahindram
*Mala Jaggi
*Pandey
*Prem Bedi
*Prem Kumar
*Rammi
*Shahajehan
*Shakti Kapoor
*Suresh Oberoi
*Thomas Lee
*Vijaya Bhanu

==Crew== Raveekanth Nagaich
*Story &ndash; Ramesh Pant
*Production &ndash; Rakesh
*Production Company &ndash; Gopikrishna Global Entertainers
*Editing &ndash; Shyam Mukherjee Raveekanth Nagaich
*Costume Design &ndash; Mani Rabadi
*Action Direction &ndash; B. Nagaraajan
*Choreography &ndash; Oscar Vijay, Hiralal, Raju Naidu
*Music Direction &ndash; Bappi Lahiri
*Lyrics &ndash; Ramesh Pant, Faruk Kaisar
*Playback &ndash; Annette Pinto, Bappi Lahiri, Chandrani Mukherjee, Preeti Uttam Singh, Usha Mangeshkar

==Soundtrack==
{{Track listing
| collapsed       =
| headline        =
| extra_column    = Singer(s)
| total_length    =

| all_writing     =
| all_lyrics      =
| all_music       = Bappi Lahiri

| writing_credits =
| lyrics_credits  =
| music_credits   =

| title1          = Aap Ki Baras Bada Juliam Hua
| note1           =
| writer1         =
| lyrics1         =
| music1          =
| extra1          = Usha Mangeshkar, Chorus
| length1         =

| title2          = Dil Dena Hai to Aaj Hi Dede
| note2           =
| writer2         =
| lyrics2         =
| music2          =
| extra2          = Usha Mangeshkar, Chorus
| length2         =

| title3          = Koi Ban Jaaye Apna Yaar
| note3           =
| writer3         =
| lyrics3         =
| music3          =
| extra3          = Chandrani Mukherjee, Preeti Uttam Kumar
| length3         =

| title4          = Lets Dance For the Great Guy Bruce Lee
| note4           =
| writer4         =
| lyrics4         =
| music4          =
| extra4          = Bappi Lahiri, Annette Pinto, Chorus
| length4         =

| title5          = Pehle To Bhajia Paao Tha Mushkil
| note5           =
| writer5         =
| lyrics5         =
| music5          =
| extra5          = Jagdeep, Bappi Lahiri, Usha Mangeshkar
| length5         =
}}

==References==
 

==External links==
* 

 
 
 


 