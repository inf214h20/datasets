Low and Behold
{{Infobox film
| name           = Low and Behold
| image          = 
| caption        = 
| director       = Zack Godshall
| producer       = Ravi Anne Sarah Hendler Barlow Jacobs Colby Johnson Douglas Matejka Jared Moshe
| writer         = Zack Godshall Barlow Jacobs
| starring       = Barlow Jacobs Eddie Rouse Robert Longstreet
| music          = 
| cinematography = Daryn De Luco
| editing        = Travis Sittard
| distributor    = 
| released       =  
| runtime        = 90 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} neorealistic and documentary techniques interwoven with actors in fictional narrative story into a post-Hurricane Katrina ravaged New Orleans. The film won Best Narrative Feature at the International Rome Film Festival  and the Best Feature award at the New Orleans Film Festival. 

The story follows Turner Stull (played by Barlow Jacobs), a young insurance claims adjuster who arrives in New Orleans after Hurricane Katrina. Turner finds it difficult to deal first hand with the pain and personal loss in the claims he must file. Eventually he meets a free-spirited local man named Nixon (played by Eddie Rouse) who is looking for his lost dog. Nixon and Turner agree to help each other and form an unlikely friendship along the way. 

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 


 