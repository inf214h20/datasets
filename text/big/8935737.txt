El Amor y el Espanto
{{Infobox film
| name           = El Amor y el Espanto
| image          = ElAmor2001.jpg
| caption         = Theatrical release poster
| director       = Juan Carlos Desanzo
| producer       = Isidro Miguel
| writer         = José Pablo Feinmann
| starring       = Miguel Ángel Solá Blanca Oteyza Víctor Laplace
| music          = Martín Bianchedi
| cinematography = Carlos Torlaschi
| editing        = Sergio Zottola
| distributor    = Argentina Video Home
| released       = May 17, 2001
| runtime        = 110 minutes
| country        = Argentina
| language       = Spanish
| budget         = $650,000 (estimate)
| preceded by    = 
| followed by    = 
}} 2001 cinema Argentine drama film directed by Juan Carlos Desanzo and written by José Pablo Feinmann. Starring Miguel Ángel Solá and Victor Laplace.

==Release==
The film premiered in Argentina on 17 May 2001. It was produced with an estimated budget of $650,000.

==Plot summary== 
A fictitious incident in the life of the Argentine short-story writer and poet Jorge Luis Borges.

==Cast==
*Miguel Ángel Solá ....  Jorge Luis Borges 
*Blanca Oteyza ....  Beatriz Viterbo 
*Víctor Laplace ....  Carlos Daneri 
*Norman Briski ....  Erik Lönnrot 
*Roberto Carnaghi ....  Pierre Menard 
*Alicia Berdaxagar ....  Madre de Borges 
*Christina Banegas ....  Dueño Pensión 
*Rolly Serrano ....  Alejandro Villari 
*Jean Pierre Reguerraz ....  Otto Dietrich 
*Víctor Bruno ....  Cantinero 
*José María López ....  Funes 
*Jorge Ochoa ....  Bibliotecario

==External links==
*  at the Internet Movie Database

 
 
 
 
 
 
 