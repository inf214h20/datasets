Murderers Club of Brooklyn
Murderers Club of Brooklyn ( ) is a 1967 German thriller film directed by Werner Jacobs and starring George Nader, Heinz Weiss and Karel Stepanek.  It was part of the Jerry Cotton series of films. 

==Plot==
A gang of kidnappers is specialised in abducting children of top managers. Jerry Cottons mission is to save the children and to arrest the gangsters. His first attempts backfire. One of the abducted kids gets killed and then Jerrys colleague Phil is taken hostage. Before he succeeds Jerry has to discover the criminals have a secret partner on the inside.

==Cast==
* George Nader ...  Jerry Cotton 
* Heinz Weiss ...  Phil Decker 
* Helmut Förnbacher ...  Bryan Dyers 
* Karel Stepanek ...  Dyers 
* Helga Anders ...  Edna Cormick 
* Helmut Kircher ...  Burnie Johnson 
* Heinz Reincke ...  Sam 
* Helmuth Rudolph ...  Mr. Johnson 
* Dagmar Lassander ...  Jean Dyers 
* Wolfgang Weiser ...  Harry Long

==Bibliography==
* 

==References==
 

==External links==
* 

 

 
 
 
 
 
 


 
 