Los Gauchos judíos
 

{{Infobox film
| name           = Los Gauchos judíos
| image          = Los gauchos judíos.jpg
| image_size     =
| caption        = Pepe Soriano and China Zorrilla in "Los Gauchos judíos"
| director       = Juan José Jusid
| producer       =
| writer         =
| narrator       =
| starring       = Pepe Soriano China Zorrilla
| music          =
| cinematography =
| editor       =
| distributor    =
| released       = 1975
| runtime        =
| country        = Argentina Spanish
| budget         =
}}
 1975 Argentina|Argentine film.

==Summary==
 		
The Jews arrive in Argentina by train, coming from Russia because of pogroms. Israel Kaufman is their administrator. His brother in law, Bartz, is also a Jew, but is bad and wants the Jews’ land for the Christians that already live there. He wants the Jews to return to Russia and is corrupt. The Jews don’t speak Spanish and need to learn how to plant, dig wells, settle the land, and become gauchos (cowboys). Calamaco and his son are two Christians who help the Jews become farmers.
 			
They need a mayor, according to Bartz—who appoints Isaac Stein, a man he can manipulate and control for his own purposes. Bartz wants more land and money; he doesn’t want the Jews to divide the land he wants and that should be his.
 		
The Jews go through a transformation from greenhorns to farmers who can plow and work the land. They lack laborers who can help them, and are opposed by the bad Christians. When they do hire Christian workers, they invite them to eat with them and treat them as equals. Bartz and (bad Christian) Chino Ledesma don’t ike what is happening with the workers.
 		
The widow Rutman goes to the doctor because she wants to marry him, acting as if something is wrong with her.
 		
The families of Raquel and Pasqual have a meal together, drinking herbal tea (yerba mate). Pasqual’s family is rich, and Pasqual is in love with Raquel. But Raquel is smarter than the idiot Pasqual and is not interested in him,
 		
Jaime and Maria are a married couple. They go to the doctor because Maria has a heart disease. The doctor invites Maria into the office. It turns out that Maria is pregnant! (Her nickname is “jorobada”, hunchback, because they one thing she doesn’t have is a hump.) She has problems giving birth.	
 	
There is a deadly fire in the community. It is not an accident—it was set by Chino Ledesma and Bartz! Gabriel’s mother believes it to be a pogrom, and thinks that they’re back in Russia, which Gabriel rushes to correct her on. The fire destroys everything but the table of Raquel’s father, which cleverly was placed on the left side of the house.
 		
The mayor Stein and Bartz talk—Bartz wants the Jews to return to Russia, wishing that they would all leave.
 		
Maria is having problems with her pregnancy, but now there is no doctor. Soon a new doctor arrives by train, and three women come out to watch. They are shocked to see what he looks like.
 		
Gabriel’s mother is having trouble sleeping because she hears a noise from the roof. Gabriel goes to the synagogue to get help, but then learns from the doctor that his mom has died.	
 		
Meanwhile, Pasqual wants to impress Raquel, so he puts on fancy clothing and enlists the help of a young boy (Jacobo), the film’s narrator. But the boy plays a trick on him—when Pasqual whistles to him to signal if Raquel is there, the boy tells him to take off the clothing, and then calls for Raquel while Pasqual has stripped down to his underpants. Pasqual, very embarrassed, falls into a river.
 		
The women go to the doctor’s office and meet the new doctor, who proves himself to be an interesting character—he has them watch the clouds! But he does resolve to help Maria (after they joke about her beauty) when Rutman talks to him.
 		
La Grillita is a girl who helps a Christian, who is helping a horse give birth to a foal. Later they joke around, but the Christian—Rogellio, wants her; she says it’s impossible because she’s Jewish and he is not.
 		
The Jews’ animals start dying. The Christians had poisoned their water. The Jews find the jar of venom, and figure out what’s going on.
 	
Calamaco and Juan see Gabriel in a bar, depressed. Gabriel no longer wants his land because of his mother’s death.	
 		
Maria’s baby is born and gets its circumcision. Calamaco doesn’t know what a circumscion is and is pretty weirded out to see one. He overhears Stein discussing with Rabi Israel that anyone who doesn’t plow and work their land will lose it. But Gabriel has not been working his land! So Calamaco and Juan go and work Gabriel’s land for him; when he sees them, he is inspired to work with them and is rejuvenated.
 		
Pasqual’s parents come to Raquel’s house in order to work out a deal for Pasqual and Raquel to get married. They discuss the dowry and how much it will be. Raquel will have to marry Pasqual—she doesn’t want to but has no choice. Their parents want to put their land together to make it bigger.
 	
Chino Ledesma is shot, presumably because he was the one who left the jar of venom out by accident. With no choice, he goes to the doctor for help. While treating him, the   doctor takes out the empty venom jar and makes sure Ledesma can see it. Ledesma, after riding with the doctor, admits to what they did.	
 		
Raquel and Gabriel meet in a sunflower field. As a shocked Jacobo (the boy) watches, they proclaim their love for each other and kiss.
 		
The Jews learn about the Fiesta of Civica, an Argentinian holiday. They celebrate with dancing and a horse race.
 		
Meanwhile, la grillita is upset and crying—she’s gone off and married the Christian, and now thinks that if a pogrom occurs, it will be her fault. Her parents act as if she died.	
 		
Two female mares are racing in the horse race. Many people are betting on the outcome, except Calamaco who has no money. A Jew gives money to bet on Juan and runs out. Juan races against Castro, another Christian. Castro grabs Juan’s horse’s reins—an unfair move—but then once Juan wins the race, accuses Juan of doing the action. Juan and Castro then are equipped with weapons for a duel. They fight, but Juan recoils and soon runs to Calamaco for safety. Calamaco, disgusted with how his son is disgracing their family’s honor with his cowardice, stabs and kills Juan. Gabriel is extremely upset by this event and goes after Castro. They fight, and then Castro admits that he actually took Juan’s reins. Juan died for nothing.
 		
One Saturday, the Jews of the town come together and march on Isaac Stein’s house. They decide to depose him as mayor, stealing his notebook with important information and wrecking his house. They install Rabi Kelner, one of the original mayoral choices, as the new mayor.
 	
The doctor overhears how Bartz wishes that the Jews would return to Russia. He comes up with a plan with Chino Ledesma. One night, the doctor goes to Bartz’s house and lies that Ledesma tried to kill him, showing him proof in the torn buttons on his shirt. It’s a plan to trap Bartz: Bartz goes to Ledesma and talks to him about going against the Jews, not realizing that the doctor and Kaufman are listening. (Bartz wanted to assist Ledesma in going against the Jews and acquiring their land.) When the doctor and Kaufman reveal themselves, Bartz breaks down and in the end leaves the community in disgrace.
 		
The wedding of Raquel and Pasqual takes place. Raaquel is obviously not happy, but everyone else is having a ball. The woman dance and break the ceremonial glass. There is music and a feast. The newer doctor meets the old doctor and makes a partnership with him. When it comes time for Raquel and Pasqual to dance, Pasqual does not want to dance, disappointing Raquel. Gabriel, attending the wedding, comes over and takes Raquel’s hand in dance.	
 		
Raquel’s father has everyone come in a picture, but some disagreement breaks out when people don’t want to be in the photo. Rutman goes to Raquel, realizing that she is truly in love with Gabriel. She mysteriously leaves with Raquel.
 	
Some time later, all of a sudden—Raquel and Gabriel appear on a horse, leaving! Raquel’s dad calls Gabriel a thief, and Pasqual’s mom says Raquel is an adulterer. They lash out at the widow Rutman, who thinks it’s romantic. The Rabbi tries to calm everyone down, but peace is only restored once a Christian fires his gun at the sky.

The Rabbi says that they can do something about the situation, because Raquel and Pasqual were married for less than 24 hours—they can annul the wedding! Pasqual agrees to annul it, and now Gabriel and Pasqual are married.

==Cast==
 

==External links==
*  

 
 
 
 
 


 