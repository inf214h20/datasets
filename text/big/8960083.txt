Megaville
{{Infobox Film
| name = Megaville
| image = Megaville poster.jpg
| image_size =
| caption = VHS cover
| director = Peter Lehner
| producer = Robert Michael Steloff Peter Lehner Cynthia Hill Andres Pfäffli Christina Schmidlin
| writer = Gordon Chavis Peter Lehner
| narrator = J.C. Quinn
| starring = Billy Zane Kristen Cloke Daniel J. Travanti J.C. Quinn Grace Zabriskie
| music = Stacy Widelitz
| cinematography = Zoltán David
| editing = Pietro Scalia
| distributor = Live Entertainment
| released = 1990
| runtime = 95 minutes
| country = United States
| language = English
| budget = Unknown
| preceded_by =
| followed_by =
}}
 1990 United American science fiction movie|film, starring Billy Zane in his first lead role. Megaville is a neo-noir psychological thriller which utilizes elements of science fiction such as cyberpunk and existentialism.

==Cast==
* Billy Zane as Agent Raymond Palinov/Jensen
* J.C. Quinn as Mr. Newman
* Kristen Cloke as Christine
* Grace Zabriskie as Palinovs mother
* Raymond OConnor as Mr. Taylor
* Daniel J. Travanti as Director Duprell
* Stefan Gierasch as Dr. Vogel
* John Lantz as Secretary of State Mr. Heller
* Bryan Clark as Megaville President Hughes
* Leslie Morris as Presidential Aide, Mr. Vargas
* Vincent Guastaferro as the hotel clerk
* Jim ODoherty as Megaville TV Reporter

==Plot==

National boundaries have been broken, two giant super-states remain—a bleak, decaying Hemisphere, and the sprawling media state Megaville. Travel between the states is restricted. The "CKS" governs daily life in the Hemisphere. All forms of media are illegal here.

Raymond Palinov, an unassuming captain of the media police finds himself drawn to a spaghetti western and cannot pull away from it during a media raid. Palinov is ostracized by his superior for the incident. After nearly losing his job, Palinov begins to exhibit strange character traits. During a rally in which outlawed media recordings are shown to the media police as examples of contraband, Palinov laughs out loud at a comedy clip. Palinov then appears to have a complete mental breakdown and loses consciousness. Whereas most agents would have been terminated following such an incident, Palinov is spared. Palinov is found to be exhibiting unusual brain activity and is sent for an examination. Palinov explains to Dr. Vogel that he has been having bizarre flashbacks and seeing memories in dreams which are not his own. Dr. Vogel tells Palinov he believes the strange behavior is due to years of exposure to media "filth" and initiates a procedure which will remove the unexplained brain activity and restore Palinovs personality in its entirety. The procedure doesnt work and Palinovs mental episodes become steadily worse.
Palinov is contacted by Mr. Duprell, the director of the CKS, with an infiltration assignment. The Hemisphere war against media is about to intensify with news of the introduction of "Dream-A-Life" (DAL) a hallucination-inducing consumer product. People in Megaville could started new lives in their own fantasy world of choice. The government of Megaville has deemed DAL legal and its use has become prevalent with the help of a mysterious figure in the Megaville underworld known as Mr. Newman. Duprell explains that Palinov bears a striking physical resemblance to Newmans only known contact in the Hemisphere, Mr. Jensen. Palinovs mission is to assume Jensens life and infiltrate the criminal underworld of both the Hemisphere and Megaville, discover who Newman is working with, and who Newmans contacts are in the Hemisphere. In order to assist his infiltration, Palinov is implanted with some of Jensens memories.
As he slips into the role, he soon meets Jensens almost lover, Christine, a former demolitions expert for the armed forces who went AWOL after being ordered to kill civilians. Christine demands that Jensen take her to Megaville. Christine and Palinov travel to Megaville and meet Newman, who refuses to reveal any information about his operation. Meanwhile, the president of Megaville, outspoken against DAL, is assassinated. Palinov suspects Newman is involved, but the only witness is the presidents personal aide, who is now permanently trapped in a DAL induced hallucination. When Palinov attempts to remove the headset, the aide is shot in broad daylight by Newman. Duprell contacts Palinov through his brain implant, informs him to close the deal with Newman or his mother will die; Duprell does not want to stop DAL, he wants in on the deal. Palinov refuses and Duprell attempts to kill him via the implant. Palinovs mother saves him by destroying Duprells transmitting device, but is then murdered. However, before she dies she reveals the truth to Palinov; she is not his mother at all, he is actually Jensen the man he thinks he is impersonating, Palinovs mind is the fake. Palinov escapes with Christine, pursued by Megavilles underworld; all except Palinov are killed in a shoot-out.
Duprell has been watching Palinovs mind, which Palinov finally realizes. He uses a DAL device to fake Duprells activities on a Megaville national broadcast, which Duprell believes to be real. Palinov says all the evidence of the conspiracy is in a briefcase at Christines apartment. Duprell opens it and detonates the bomb inside. Newman reveals to Palinov that he is Jensens biological father and claims he wishes they had spent more time together before shooting Palinov. Newman then notices his leg is handcuffed to Palinov and he is now trapped in the desert.

==Production==
Much of the filming for "the Hemisphere" scenes, especially the CKS headquarters, took place over a two-day period, on the weekend of the 17–18 February 1990. It took place at the Oakland City Hall which was closed at the time due to damage from the 1989 Loma Prieta earthquake.

==Festival==
Megaville was nominated for the title of "best film" at the international Mystfest special interest festival in Cattolica, Italy, in 1991 (although it lost to a Spanish language film, Amantes (film)|Amantes).

==External links==
*  

 
 
 
 
 
 
 
 