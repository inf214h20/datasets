The Last Witness (1980 film)
{{Infobox film
| name           = The Last Witness
| image          = File:The_Last_Witness_(1980)_poster.jpg
| caption        = 
| film name      = {{Film name
 | hangul         =  
 | hanja          =  의  
 | rr             = Choehuui jeungin
 | mr             = Ch‘oehu-ŭi chŭngin}}
| director       = Lee Doo-yong
| producer       = Kim Hwa-sik
| writer         = Yoon Sam-yook
| based on       =  
| starring       = Hah Myung-joong Jeong Yun-hui Choi Bool-am
| music          = Kim Hee-kap
| cinematography = Jeong Il-seong
| editing        = Lee Kyung-ja
| studio         = SeKyung Enterprises Inc.
| distributor    = 
| released       =  
| runtime        = 158 minutes
| country        = South Korea
| language       = Korean
| budget         = 
| gross          = 
}} 1980 South Korean mystery film directed by Lee Doo-yong, based on the 1974 novel of the same title by Kim Seong-jong. When the film was originally released, a 40-minute portion was cut due to censorship laws of that time.  A remake with the same title was released in 2001.

== Cast ==
* Hah Myung-joong 
* Jeong Yun-hui 
* Choi Bool-am 
* Hyun Kill-soo 
* Han Hye-sook 
* Lee Dae-keun 
* Han So-ryong 
* Shin Woo-chul 
* Sin Dong-uk 
* Han Tae-il 
 
== References ==
 

== External links ==
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 


 