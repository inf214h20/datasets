The Way Back
{{Infobox film
| name           = The Way Back
| image          = The Way Back Poster.jpg
| caption        = Theatrical release poster
| director       = Peter Weir
| producer       = Peter Weir Joni Levin Duncan Henderson Nigel Sinclair Scott Rudin
| based on       =  
| screenplay     = Peter Weir Keith Clarke
| starring       = Jim Sturgess Ed Harris Saoirse Ronan Colin Farrell 
| music          = Burkhard Dallwitz
| cinematography = Russell Boyd Lee Smith
| studio         = National Geographic Films Spitfire Pictures Imagenation Abu Dhabi Film Fund Luxembourg
| distributor    = Newmarket Films Exclusive Film Distribution Meteor Pictures
| released       =  
| runtime        = 127 minutes
| country        =  English Russian Russian
| budget         = $30 million   
| gross          = $20,348,249  
}} POW Sławomir Rawicz, who escaped from a Soviet Gulag and walked 4,000 miles to freedom in World War II, and stars Jim Sturgess, Colin Farrell, Ed Harris, and Saoirse Ronan, with Alexandru Potocean, Sebastian Urzendowsky, Gustaf Skarsgård, Dragoş Bucur, and Mark Strong. 
 Academy Award for Best Makeup.

==Plot==
During World War II, after the Soviet invasion of Poland, young Polish military officer Janusz Wieszczek (Jim Sturgess) is held as a POW and interrogated by the NKVD. The Soviets, unable to get him to say he is a spy, take into custody his wife from whom they extort a statement condemning him. He is sentenced to 20 years in a Gulag labour camp deep in Siberia.
 American engineer; Khabarov (Mark Strong), an actor; Valka (Colin Farrell), a hardened Russian criminal; Tomasz (Alexandru Potocean), a Polish artist; Voss (Gustaf Skarsgård), a Latvian priest; Kazik (Sebastian Urzendowsky), a Pole suffering from night blindness; and Zoran (Dragoş Bucur), a Yugoslavian accountant. Khabarov secretly tells Janusz that he is planning to escape south to Mongolia, passing Lake Baikal. Mr. Smith cautions Janusz that it is Khabarovs way to discuss escape plans with newcomers, to maintain his morale, but nothing will come of it. At times Janusz seems to hallucinate the front door of a country home and adjoining window ledge, which holds plants and a rock he attempts to reach for. Janusz follows through with the escape with Mr. Smith, Valka, Voss, Tomasz, Zoran, and Kazik during a severe snowstorm that covers their tracks.

Kazik freezes to death the second night of the trek, after losing his way back to the campsite while looking for wood, and the group buries him. After many days of travelling across the snows of Siberia, the group reaches Lake Baikal. There they meet Irena (Saoirse Ronan), a young Polish girl, who tells them Russian soldiers murdered her parents sent her to a collective farm near Warsaw, where they treated her cruelly, so she escaped. Mr. Smith realises the inaccuracies in her story, as Warsaw is ruled by the Germans; nevertheless, despite his misgivings that shell slow them down and tax their meagre food supply, he agrees with the group to let her in. Mr. Smith eventually cautions her about the fib and says he will not tolerate any more, in response to which she admits that her parents were communists but the communist rulers killed them anyway and sent her to an orphanage.

When the group reaches an unpatrolled border between the Soviet Union and Mongolia, Valka, who idolizes Stalin and doesnt know what hed do elsewhere, decides to stay. The rest continue to Ulan Bator, but soon they see images of Stalin and a red star. Janusz realises that Mongolia is under communist control and tells the group they should take refuge in India. As they continue south across the Gobi Desert, lack of water, sandstorms, sunburn, blisters, and sunstroke weaken the group. Irena collapses several times and soon dies. A few days later, Tomasz collapses and dies. Mr. Smith is on the verge of death, but after being motivated by Janusz, Zoran, and Voss, he decides to rejoin the group, and the severely dehydrated four find a much-needed water source.
 Buddhist monastery, where they regain their strength. Mr. Smith decides to go to Lhasa with the help of one of the monks contacts, who will smuggle him out through China. Once there, he anticipates he will be able to connect with the US military; his return to America ensured. The remaining three continue to trek through the Himalayas and soon reach India.

At the end of the film, Janusz keeps walking around the world until 1989, when Poland ousts the communists. The final scene of the film shows Janusz, 50 years after being taken captive, again envisioning the door and reaching for the rock; this time he takes a key hidden beneath the rock to open the door and reconcile with his wife.

==Cast==
* Jim Sturgess as Janusz Wieszczek, a young Polish inmate taken Prisoner of War during the Soviet invasion of Poland
* Colin Farrell as Valka, a tough Russian inmate and gambler 
* Ed Harris as Mr. Smith, an American inmate and former engineer
* Saoirse Ronan as Irena Zielińska, an orphaned teenage Polish girl on the run from Soviet Russia, who meets up with the fugitives near Lake Baikal
* Mark Strong as Khabarov, an actor
* Dragoş Bucur as Zoran, a Yugoslavian inmate who used to be an accountant
* Gustaf Skarsgård as Voss, a Latvian inmate and former priest
* Alexandru Potocean as Tomasz, an artist and former pastry chef
* Sebastian Urzendowsky as Kazik

==Production==
===Background===
The film is loosely based on The Long Walk (1955),  , and Linda Willis in her November 2010 book   and   described newly discovered evidence which may confirm the presence of Polish gulag escapees in western India in March 1942.   .  Though the director Peter Weir continues to claim that the so-called long walk happened, he himself now describes The Way Back as "essentially a fictional film".    Retrieved 2011-02-19.    Retrieved 2011-02-19. 

Regardless of whether or not this particular "long walk" really took place, during World War II other Poles undertook difficult journeys attempting to leave the Soviet Union. Accounts of their escapes can be found in the archives of the Polish Institute and Sikorski Museum in London, and in the Hoover Institute at Stanford University, in California.  . Retrieved 2011-02-05. Specific references or citations are needed to support this assertion.  Also, several relatively verifiable and believable escapee autobiographies have been published in English, e.g., Michael Krupas Shallow Graves in Siberia. 

===Filming===
Principal photography took place in Bulgaria, Morocco, and India. 

==Reception== The Telegraph called the film "A journey that feels awful and heroic and unfathomable – and one you’ll want to watch again." 

==Music==
The soundtrack to The Way Back was released on January 18, 2010. 

{{Track listing
| collapsed       = no
| extra_column    = Artist
| total_length    = 42:27

| title1          = Interrogation
| length1         = 3:36
| extra1          = Burkhard Dallwitz

| title2          = New Arrivals
| length2         = 1:16
| extra2          = Burkhard Dallwitz

| title3          = Plans for Escape
| length3         = 1:58
| extra3          = Burkhard Dallwitz

| title4          = A Brave Man
| length4         = 1:00
| extra4          = Burkhard Dallwitz

| title5          = Escape
| length5         = 2:48
| extra5          = Burkhard Dallwitz

| title6          = Lake Baikal
| length6         = 3:35
| extra6          = Burkhard Dallwitz

| title7          = Freedom?
| length7         = 3:02
| extra7          = Burkhard Dallwitz

| title8          = Mirages Dont Have Birds
| length8         = 2:34
| extra8          = Burkhard Dallwitz

| title9          = The Abandoned Temple
| length9         = 1:17
| extra9          = Burkhard Dallwitz

| title10         = Water!
| length10        = 3:36
| extra10         = Burkhard Dallwitz

| title11         = Tibet
| length11        = 5:26
| extra11         = Burkhard Dallwitz

| title12         = India
| length12        = 1:58
| extra12         = Burkhard Dallwitz

| title13         = Keep on Walking
| length13        = 2:42
| extra13         = Burkhard Dallwitz

| title14         = Closing Credits
| length14        = 7:39
| extra14         = Burkhard Dallwitz

}}

==See also==
*Berlin Wall (depicted in the film)
*Iron Curtain (noted in the film)
*Lech Walesa (depicted in the film)
*Survival film, featuring a list of related films
*The Great Wall of China (depicted in the film)

==References==
 

==Further reading==
*  
*  This is an excellent collection of articles relating to the movie The Way Back, the book upon which it was based The Long Walk, Linda Willis Looking For Mr. Smith, and related materials.
*   An interesting contemporary discussion of Rawiczs book by a noted explorer.

==External links==
*   walk path at  
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 