Home Before Midnight
 
{{Infobox Film
| name           = Home Before Midnight
| image          = "Home_Before_Midnight"_DVD_cover.jpg
| caption        = UK DVD cover Pete Walker
| producer       = Pete Walker Murray Smith Michael Armstrong Pete Walker
| narrator       =  James Aubrey Mark Burns Juliet Harmer Richard Todd Ray Russell Jigsaw
| cinematography =  Peter Jessop	
| editing        = Alan Brett
| distributor    = Anchor Bay Entertainment
| released       =  18th October, 1979 
| runtime        = 111 min.
| country        = UK English
| budget         = 
| preceded_by    = 
| followed_by    =  Pete Walker, Murray Smith, James Aubrey, Alison Elliot and Richard Todd.  It follows a songwriter who is put on trial for having sex with an underage girl.

==Cast== James Aubrey - Mike Beresford
* Alison Elliot - Ginny Wilshire Mark Burns - Harry Wilshire
* Juliet Harmer - Susan Wilshire
* Richard Todd - Geoffrey Steele
* Debbie Linden - Carol
* Andy Forray - Vince Owen
* Chris Jagger - Nick
* Sharon Maughan - Helen Owen Charles Collingwood - Burlingham
* Faith Kent - Miss Heatherton
* John Hewer - Donelly
* Jeff Rawle - Johnnie McGee
* Patrick Barr - Judge
* Edward de Souza - Archer
* Ian Sharrock - Malcolm
* Emma Jacobs - Lindy
* Leonard Kavanagh - Mr Beresford
* Joan Pendleton - Mrs Beresford Ivor Roberts - Inspector Gray
* Claire McClellan - Tracey Wilshire Nicholas Young - Ray

==References==
 

==Bibliography==
* Sweet, Matthew. Shepperton Babylon: The Lost Worlds of British Cinema. Faber and Faber, 2005.

==External links==
* 

 

 
 
 
 
 
 


 