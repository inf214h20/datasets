Gangs of Wasseypur – Part 1
 
 
 
{{Infobox film
| name = Gangs of Wasseypur – Part 1
| image= Gangs_of_Wasseypur_poster.jpg
| caption = Theatrical release poster
| director = Anurag Kashyap
| producer = Atul Shukla Anurag Kashyap Sunil Bohra Viacom 18 Motion Pictures
| screenplay =
| writer = Zeishan Quadri Akhilesh Sachin Ladia Anurag Kashyap Akhilesh Jaiswal Huma Qureshi Tigmanshu Dhulia Manoj Bajpai Vineet Kumar Singh Piyush Mishra Pankaj Tripathi Richa Chadda Reemma Sen
| music = Sneha Khanwalkar (soundtrack) G. V. Prakash Kumar (score)
| cinematography = Rajeev Ravi
| editing = Shweta Venkat
| studio = Anurag Kashyap Films   Jar Pictures
| distributor = Viacom 18 Motion Pictures
| released =  
| runtime = 160 minutes   
| country = India
| language = Hindi
| budget =     
| gross =  (9 weeks domestic)        
| IMDB Top 250 = TOP#245  
}} Indian crime coal mafia Huma Qureshi, Richa Chadda, Tigmanshu Dhulia in the major roles and its story spans from the early 1940s to mid-1990s.

Both parts were originally shot as a single film measuring a total of 319 minutes and screened at the 2012 Cannes Directors Fortnight     but since no Indian theatre would volunteer to screen a five plus hour movie, it was divided into two parts (160 mins and 159 mins respectively) for the Indian market.

The film received an A certification from the Indian Censor Board. {{cite news| url=http://articles.timesofindia.indiatimes.com/2012-06-13/news-interviews/32214107_1_cbfc-anurag-kashyap-fresh-trouble | title= Now, Wasseypur in censor trouble
| date=13 June 2012 | work=The Times Of India}}  The films soundtrack is heavily influenced by traditional Indian folk songs.

Part 1 was released on 22 June 2012 in more than 1000 theatre screens across India. It was released on 25 July in France and on 28 June in the Middle East but was banned in Kuwait and Qatar.   Gangs of Wasseypur was screened at the Sundance Film Festival in January 2013.    Gangs of Wasseypur has won four nominations, including best film and best director, at the 55th Asia-Pacific Film Festival. 

The combined film won the Best Audiography, Re-recording (filmmaking)|Re-recordists of the Final Mixed Track Special Mention Best Film Best Actress (Critics), at the 58th Filmfare Awards 

Although not a huge hit by any financial standard, the meagre combined budget of ₹18.5cr  allowed the 2 films to be commercially successful, with net domestic earnings of ₹50.81cr (of the 2 parts combined). It is considered by many as a modern cult film.

==Plot==

A gang of heavily armed men scour and finally narrow down on a house in Wasseypur. They surround the house and unleash a wave of bullets and grenades on it with the intention of killing the family inside it. After heavy firing on the house, they retreat away from the crime scene in a vehicle, convinced they have killed everyone within. The leader of the gang then calls one JP Singh (Satya Anand) on his cell phone and reports that the family have been successfully executed but he is double crossed by JP Singh as a fire fight erupts between them and a police check post blocking their escape route. The scene cuts abruptly for a prologue by the narrator, Nasir.

===Introduction of Wasseypur and Dhanbad===
Nasirs narration describes the history and nature of Wasseypur. During the British Raj, Wasseypur and Dhanbad were located in the Bengal region. After India gained its independence in 1947, they were carved out of Bengal and redistricted into the state of Bihar in 1956. In 2000, Wasseypur and Dhanbad were redistricted for a second time into the newly formed state of Jharkhand where they remain. The village has been historically ruled by the Qureshi Muslims, a sub-caste of animal butchers who are feared by the non-Qureshi Muslims living there and Dhanbad by extension.

During British colonial rule, the British had seized the farm lands of Dhanbad for coal which began the business of coal mining in Dhanbad. The region was a hotbed of the local faceless dacoit Sultana Qureshi who robbed British trains in the night and thus holds some patriotic value for the locals.

===1940s===
"Shahid Khan" (Jaideep Ahlawat), a Pathan, takes advantage of the mysteriousness of the faceless dacoit Sultana, a Qureshi, by impersonating his identity to rob British ferry trains. The Qureshi clans eventually find out and order the banishment of Shahid Khan and his family from Wasseypur. They settle down in Dhanbad where Shahid begins work as a labourer in a coal mine. He is unable to be at his wifes side who dies during childbirth. The enraged Shahid kills the coal-mines muscleman who had denied him leave on that day. In 1947, Independent India begins to assert its authority over itself. The British coal mines are sold to Indian industrialists and Ramadhir Singh (Tigmanshu Dhulia) receives a few coal-mines in the Dhanbad region. He hires Shahid as the new muscle-man of the coal mine who terrorises the local population to seize their lands and extract compliance.

On a rainy day, Ramadhir Singh overhears Shahids ambitions of taking over the coal mines from him. Singh tricks Shahid into traveling to Varanasi for business but instead has him murdered by an assassin named Yadav. Nasir, Shahids assistant, finds Ramadhirs umbrella with his initials near the door, concluding that Ramadhir eavesdropped on their conversation. He flees from the place with Shahids son Sardar in the nick of time as Ehsaan Qureshi (Vipin Sharma), another associate of Ramadhir Singh, shows up too late for killing them. An unsuccessful Ehsaan lies to Singh that Shahids family has been murdered and buried. Under the care of Nasir, Sardar grows up along with his cousin Asghar (Jameel Khan), and learns the truth about his fathers death, upon which he shaves his head vowing not to grow his hair until he has avenged his fathers death.

===1950–60===
Ramadhir Singh establishes himself in coal mining by misusing his position and power as a trade union leader by turning it into a mafia organisation threatening laborers into giving a substantial portion of their income to Ramadhirs henchmen.

===Early and Mid 1970s===
The coal mines are nationalised. A mature Sardar Khan (Manoj Bajpai) and his kin start hijacking Ramadhirs coal trucks mid-transit. Ramadhir Singh suspects SP Sinha, a Coal India official, to be behind the hijackings and has him murdered. Post Sinhas murder, Ramadhirs reputation for ruthlessness grows, and he is feared in Dhanbad.

Sardar marries Nagma Khatoon (Richa Chadda). The pregnant Khatoon confronts Sardar Khan and a prostitute inside a brothel and chases him away. Later, Nagma gives birth to Danish but gets pregnant immediately afterwards. Unable to have sex with a pregnant Nagma, Sardar confesses his sexual frustrations with his kin. At dinner, Nagma gives her consent to Sardar to sleep with other women but with the condition that he wont bring them home or dishonour the family name.

Sardar, Asghar and Nasir (Piyush Mishra) start working for JP Singh. They misuse their employment by secretly selling the company petrol in the black market. Later, they rob a petrol pump and a train bogey belonging to the Singh family. They usurp Singhs land, which forces the two families to confront each other for talks. The meeting ends in a scuffle, but Ramadhir Singh realizes that Sardar Khan is in fact the son of Shahid khan who he had murdered in the late 1940s. Sardar and Asghar are jailed for assaulting JP Singh.

===Early 1980s=== Bengali Hindu girl named Durga (Reema Sen). Asghar informs Nagma that Sardar has taken a second wife, leaving Nagma helpless at the situation. Meanwhile, Wasseypur has merged with Dhanbad where the Qureshi goons terrorise the non-Qureshi Muslims. The locals approach Sardar Khan for help. During Muharram, both Shias and Sunnis are out mourning, including the Qureshi clan. Sardar uses the opportunity to launch a major bomb attack on many Qureshi shops and houses. When word spreads about Sardars raids, his reputation grows and he commands more fear than the Qureshi clan.

Eventually, Sardar returns home to Nagma and she gets pregnant again. Sardar tries to initiate sex with a pregnant Nagma but she refuses, which prompts an angry Sardar to leave. He goes to stay with his second wife, Durga, and she gives birth to his son, Definite. Ramadhir Singh, noticing that Sardar has abandoned his first family, tries to reach out to Nagma through Danish by giving him money. An enraged Nagma beats Danish for taking the money while she breaks down in front of Nasir. A thirsty Faizal wakes up in the middle of the night to find Nagma and Nasir about to have sex. Angry, he storms out of the house and becomes a stoner, permanently seen with his chillum. Nasir reveals that the desires were never consummated, but Faizal and Nasir never see eye to eye again.

===Mid and Late 1980s===
Sensing Sardars increasing clout, Ramaadhir calls his old associate Ehsaan Qureshi who brokers a meeting between Sultan Qureshi and Ramaadhir Singh where the two decide to become allies against their common enemy, Sardar Khan. Sultan asks Ramaadhir for modern automatic weapons which the latter promises to give.

===1990s===
Sardar becomes the most feared man in Wasseypur and shifts his business to stealing iron ore. Danish Khan joins in the family business. A failed attack from Sultan Qureshi leaves Danish with a minor injury and causes reconciliation between  Sardar and Nagma. Sardar finds Ramaadhir and warns him of terrible consequences if anything ever happens to his family.

A mature Faizal (Nawazuddin Siddiqui) is seriously affected by Bollywood movies as he starts behaving, talking and dressing like Bollywood characters. Faizal is caught by the police for buying guns and jailed. Upon release, he kills the gun-seller (Yadav who unbeknownst to Faizal was the nameless assassin who had killed Shahid Khan (Faizals grandfather)) who had implicated him to police earlier. Meanwhile, Sardar seizes a lake belonging to a local temple and charges commission on fish sellers who make a catch in that lake. An uneasy peace is maintained between the Qureshi and Khan families when Danish Khan marries Shama Parveen, the sister of Sultan Qureshi.

Faizal reveals to a friend that his father Sardar would be travelling without security the next day. Late night, while Faizal is still asleep, his friend - who turns out to be a spy - calls up the Qureshis and passes this bit of information to them. Sardar leaves home alone and reaches the Durga household to give them their expense allowance. Durga turns out to be yet another Qureshi spy. The Qureshi men follow Sardars car, and when the latter stops at a petrol pump to refuel they start shooting while Sardar ducks in the car for cover. The Qureshi men put several close rounds through the car window ensuring a precise & unmistakable hit, after which they escape. A shocked Sardar opens the car door and stands up to reveal multiple bullet wounds, one directly on his head. He steps out with his gun drawn trying to locate the shooters but he eventually collapses to his death on a ferry cycle.

==Sequel==
 

==Cast==
* Manoj Bajpai as Sardar Khan, forms the core of the chronicle. He escapes death as a child while his father Shahid Khan is killed by Yadav on the instructions of Ramadhir Singh. He shaves his head vowing not to grow his hair until he exacts revenge by making Singh endure many silent deaths through humiliation and the fear of being stalked. Sardar is an immoral womaniser, a megalomaniac and a hyper-sexual who ends up marrying two wives (Nagma Khatoon and Durga) besides making regular rounds at the brothels. Outside the house, Sardar is vicious, brutal and merciless towards his enemies but inside the house, he cowers from beatings and runs when hes caught in the brothel by his first wife Nagma. Sardar is crafty and comical, almost a goof-ball.
 
* Nawazuddin Siddiqui as Faizal Khan ,is Sardars second son from his first wife Nagma. He is forced to drop out from school when Sardar leaves his mother Nagma to live with his second wife Durga. Due to such a stressful and traumatic event in his developing childhood years, he becomes a chronic marijuana smoker. Although, Faizal never quits this habit, he eventually joins his fathers criminal profession. Huma Qureshi as Mohsina, is the love interest of Faizal Khan and plays hard to get. However, she is very supportive of Faizal Khan.
* Satya Anand as J.P Singh, MLA son of Ramadhir Singh. His father expects him to control things and become future leader and in process keep humiliating him. However he has achieving kingdom.
* Piyush Mishra as Nasir, is the narrator of the film and a cousin of Shahid Khan, who saves an adolescent Sardar from Ramadhirs henchmen and takes care of him along with Asghar till adulthood. He is unmarried and feels helpless on seeing Sardar cheat on his wife Nagma. Later in life, Sardar and Nasir become duos in crime.
* Jameel Khan as Asghar, is a cousin of Sardar Khan who assists him during the takeover of Wasseypur from Sultan Qureshi.
* Jaideep Ahlawat as Shahid Khan, is a pathan and the father of Sardar Khan. He robs government trains under the guise of Qureshi dacoit sultana. When the Qureshi clan find out, he and his family are ordered into exile from Wasseypur. He becomes a henchmen for Ramadhir Singh but is killed when the latter eve-drops on the former and learns about his ambitions for power which sets off the revenge trail that lasts between the Khans and the Qureshis for around three generations.
* Richa Chadda as Nagma Khatoon,is Sardar Khans first wife and mother of four sons. She is a motor-mouth and firebrand who doesnt shy away from the ugliest street slang, and isnt beyond confronting her husband in the brothel and chasing him with a stick. In time, she learns to accept her husbands habit of infidelity but asks him to continue his behaviour albeit with the family honour in mind.
* Reema Sen as Durga ,is a Hindu Bengali girl who serves as a maid. Sardar khan charms her and makes her his second wife. She is initially coy but shows her real self when Sardar abandons her and her son. In the films climax, she plays a significant part in Sardar Khans death.
* Tigmanshu Dhulia as Ramadhir Singh, is a politician who orders the hit on Shahid Khan. He is calculating, cold and unemotional. He controls Wasseypur area by allying himself with the Qureshi clan but is slowly losing power to Sardar Khan and his sons.
* Pankaj Tripathi as Sultan Qureshi, is the nephew of Dacoit Sultana and is a butcher by profession and association. He is a henchmen and an ally of Ramadhir Singh. He is from the Qureshi clan who exiled Shahid Khan and his family out of Wasseypur. Sultans reign in Wasseypur is overthrown by the arrival of Sardar Khan. Now Sultans only agenda is to kill Sardar.
* Vipin Sharma as Ehsaan Qureshi, is a long running associate of Ramadhir Singh. Somewhere in the late 40s and the early 50s, he is sent by Ramadhir to kill Shahid Khans family but arrives too late as the family had already escaped. Later, Ehsaan returns to Ramadhir and lies about the familys execution and burial. A Scene which is one of the most talked about from the film.
* Vineet Kumar Singh , as Danish Khan, is Sardars son from his first wife, Nagma. He takes a liking for Sultan Qureshis Sister Shama Parveen and later marries her against Sultans wishes.
* Anurita Jha as Shama Parveen, is the Daughter of Ehsaan Qureshi and the sister of Sultan Qureshi. She marries Sardars son Danish Khan against the wishes of her brother Sultan.
* Shankar as Shankar
* Hazarat Ali as Definite Khan (young), is Sardars son from his second wife, Durga.
* Aditya Kumar plays the role of Perpendicular (based on Chottna Khan).
* Tilak Raj Mishra as Sanjeev
* Syed Khan as Iqbal Khan
* Naman Tiwari as Ajay Singh (Young)
* Aniket Raj as Vijay Singh (Young)
* Jaikumar Solanki as Jatin
* Sanjay Varma as Inspector Udayveer Singh
* Sandeep Arora as ACP Jadhav
* Pramod Pathak as Sultana Daku and also as Sultan Qureshis Uncle
* Harish Khanna as Yadav, is a black market weapons seller and an associate of Ramadhir Singh. Yadav kills Sardar Khans father Shahid Khan on the instructions of Singh. The older Yadav is killed by Faizal Khan later for conning him.

==Production==

===Development===
Anurag Kashyap said he had wanted to make a film on Bihar with the name Bihar for some time but for various reasons it didnt take off. In 2008 he met Zeishan Quadri, writer of GANGS who told him about Wasseypurs story. He found it unreal to believe that mafia activity and gang war existed at such high level. Zeishan narrated enough stories but what really attracted him was not gang war but the entire story of emergence of mafia. According to him to tell the story through a few families is what interested him but that also meant a longer reel. "We all know mafia exists but what they do, how they operate, why they do we dont know and that is something which forms the basis of the film". 

===Casting===
According to Bajpai, the role of Sardar Khan is the most negative role he has done till date. His motivation for doing this role came from the fact that there was "something new" with the character of Sardar Khan. 

Piyush Mishra and Tigmanshu Dhulia were given the discretion to decide who, among them, would perform the roles of Nasir and Ramadhir. Mishra chose the role of Nasir and Dhulia portrayed Ramadhir Singh. 

Chadda revealed in an interview that this role helped her bag 11 film roles. 

This is Huma Qureshis first film, and she characterised this as her "dream debut". Qureshi landed this role after director Anurag Kashyap spotted her in a Samsung commercial he was directing. 

===Filming===
During filming in Varanasi in December 2010, films chief assistant director Sohil Shah was killed on shoot while performing one of the stunt shot scene which was an accident.  The Movie has been dedicated to Sohil Shah as is seen in the opening titles.
The film finished production in late March 2011, with Anurag Kashyap moving on to direct his next film immediately due to that accident.  Major portions of the film were shot at villages near Bihar.   Shooting of film also took place in Chunar.  Anurag Kashyap, who co-produced the film with Sunil Bohra, has said that it is his most expensive film and he reportedly had to spend   15&nbsp;crore on paying the actors.  Both parts of Gangs of Wasseypur together cost just ₹18.4cr to make. Anurag Kashyap, the director of film tweeted – "450&nbsp;million as reported in the media is false." 

==Themes and Portrayals==

===Style===
The filming style adopted by Anurag Kashyap in Gangs of Wasseypur bears a striking similarity to the styles of Sergio Leone and Sam Peckinpah. The scenes are short in length, several in number and often a series of montages take the story forward. Anurag Kashyap never has to resort to extraneous elements like stylised entries, editing patterns or camera motions to add to the effect because the story has an intrinsic impact of its own. However the film doesnt fall short of any technical finesse. Theres unabashed blood, gore and abuse wherever the scene demands.
Lines like "Tum sahi ho, woh marad hai," ("You are right, he is male") said in resigned agreement to a wronged wife stand out for their cruel truths of rural life.    Kashyaps use of occasional bursts of music and comedy to punctuate the slowly augmenting tension at different junctures is highly reminiscent of Spaghetti Westerns. Kashyaps use of dark humour to judiciously propagate violence bears an uncanny similarity to Quentin Tarantino’s style of movie-making.  Absorbing styles as diverse as those of old-school Italo-American mafia classics a la Coppola, Scorsese and Leone, as well as David Michods taut crime thriller "Animal Kingdom," Kashyap never lets his influences override the distinct Indian color. The pacing is machine-gun relentless, sweeping incoherence and repetitiveness under the carpet as it barrels forward with hypnotic speed. 

===Theme===
 
  BP Sinha, one of the most respected labour leader but also considered as Godfather of Dhanbad Mafia. The character of S.P. Sinha is vaguely based on him Surajdeo Singh, MLA and the first Coal Mafia of Dhanbad. The Character of Ramadhir Singh in Gangs of Wasseypur is based on him. He actually got his mentor, BP Sinha murdered to become the undisputed Mafia King.
 
The movie chronicles the journey of the saga associated with coal mines.It portrays the gang lords of Wasseypur like Shafi Khan, Fahim Khan and Shabir Alam.  The film has also been inspired from the story of Jharkhand politician BP Sinha, Suryadev Singh, Binod Singh, Sakeldeo Singh, and Ramadhir Singh, who was convicted of murder.   
    Rajeev Masand of CNN-IBN calls the movie, a gang warfare and notes that " On the surface, Gangs of Wasseypur is a revenge saga, a tableau of vengeance between generations of gangsters. Scratch that surface and you’ll discover more than just a grim portrait ". "   
While some of the critics noted that the film, is a powerful political film, which underlines the party politics system (at that time) allowing the growth of illegal coal trading and mafias in the region (Bihar) and their use as a political tool, thus making the allotment of coal blocks, one of the most powerful expressions of controlling power in the region.  Despite its grim theme, the film also has an inherent sense of humour that comes quite naturally to it from its series of events. The scene where Reema Sen is charmed by Manoj Bajpai over her daily chores or the one where Nawazuddin goes on a formal date with Huma Qureshi are outrageously hilarious.   
The household politics is one of the many subplots rendering layers to the story. You realise Sardars family is emerging into a Corleone set-up of sorts. His sons&nbsp;- the brooding Danish and the doped-out Faizal (Nawazuddin Siddiqui) from Nagma, and the enigmatic Definite Khan (Zeishan Quadri) from Durga&nbsp;- will become key players in this revenge story.    Violent as his screenplay is, Kashyap reveals wit while narrating his tale. Ample black comedy is used to imagine the gang war milieu. The humour lets us relate to the intrinsic irreverent nature of men who live by the gun. 
Character development can best justify the length of Part 1.   

==Soundtrack==
 
{{Infobox album
| Name = Gangs of Wasseypur
| image= Gangs_of_Wasseypur_poster.jpg
| Type = Soundtrack
| Artist = Sneha Khanwalkar
| Cover =
| Released =  
| Recorded = Feature film soundtrack
| Length = 56:12
| Label = T-Series
| Producer =
| Last album =
| This album =
| Next album =
}}
Music of Gangs of Wasseypur was launched on 23 May. The film which is in two parts, has a whopping 25 songs.   Music for the album is composed by Sneha Khanwalkar and Piyush Mishra. Lyrics for the album are written by Varun Grover and Piyush Mishra.
{{track listing
| headline = Tracklist: Part-I
| extra_column = Singer(s)
| total_length = 56:12
| title1 = Jiya Tu
| lyrics1 =
| extra1 = Manoj Tiwari
| length1 = 5:19
| title2 = Ik Bagal
| lyrics2 =
| extra2 = Piyush Mishra
| length2 = 5:28
| title3 = Bhoos
| lyrics3 =
| extra3 = Manish Tipu, Bhupesh Singh
| length3 = 5:09
| title4 = Keh ke lunga
| extra4 = Amit Trivedi
| length4 = 4:47
| title5 = O Womaniya Live
| extra5 = Khusboo Raaj, Rekha Jha
| length5 = 4:49
| title6 = Hunter
| extra6 = Vedesh Sokoo, Rajneesh, Munna, Shyamoo
| length6 = 4:17
| title7 = Humni ke Chhodi ke
| extra7 = Deepak Kumar
| length7 = 4:17
| title8 = Loonga Loonga
| extra8 = Ranjeet Kumar Baal Party, Akshay Verma
| length8 = 2:52
| title9 = Manmauji
| extra9 = Usri Banerjee
| length9 = 2:53
| title10 = Womaniya
| extra10 = Khusboo Raaj, Rekha Jha
| length10 = 5:22
| title11 = Aey Jawano
| extra11 = Ranjeet Baal Party (Gaya)
| length11 = 1:54
| title12 = Soona kar ke gharwa
| extra12 = Sujeet (Gaya)
| length12 = 2:01
| title13 = Tain Tain To To
| extra13 = Sneha Khanwalkar
| length13 = 3:59
| title14 = Bhaiyaa
| extra14 = The Mushahar of Sundarpur
| length14 = 3:06
}}

===Reception===
Raja Sen of Rediff gave a five star rating to the soundtrack calling it a "A strikingly flavourful and headily authentic collection of quirky music".  Purva Desai of Times of India said "The music is brilliant and this album deserves all the praises."  Shivi Reflections in her favourable review wrote that "Gangs of Wasseypur is a soundtrack which should be acknowledged for its experimentation and uniqueness." 
 Best Music Director award at the 58th Filmfare Awards. 

==Marketing==
 .]]
The marketing of Gangs of Wasseypur was noted for its uniqueness. Gamucha, a thin traditional East Indian towel was taken to Cannes, the Gangs of Wasseypur team danced on the streets wearing red gamchhas, after the Cannes Film Festival and has been making public appearances in them ever since. While most music launches in India happen with a big party in a 5-star banquet hall in a Delhi or Mumbai, and formal announcements before the press, the music of this film, was launched in Patna.

In another effective way of building the world of Wasseypur for the audience, a fictitious newspaper was made available online which was named Wasseypur Patrika.

In keeping with the language and setup of the film, wall paintings instead of posters, reading Goli Nahi Marenge, Keh Ke Lenge&nbsp;– Gangs of Wasseypur were painted on walls across 20 cities.

Gangs of Wasseypur mementos&nbsp;— The Gangs of Wasseypur team has made a memento consisting of bullets of different era. While all sorts of weapons have been used in the film, this is the best thing one could give as memento.
 

==Reception==
===Critical reception===
;India IMDB it has a rating of 8.5 out of 10 and is the only Indian film from its decade to make its place in the Top 250 global films of all-time list. 

In the review aggregator website ReviewGang, the movie received 6.5 stars out of 10, based on 16 reviews.  Bikas Bhagat of Zee News gave the movie 4 stars out of 5, concluding that "So if you want to experience an all new wave of cinema in Bollywood, Gangs of Wasseypur is your movie. Its has some real quirky moments which I’ll leave for you to explore in the film. Watch it for its sheer cinematic pleasure!" 

Subhash K. Jha of IANS gave the movie 4 out of 5 stars, saying that "Brutal, brilliant, dark, sinister, terrifying in its violence and yet savagely funny in the way human life is disregarded Gangs of Wasseypur is one helluva romp into the raw and rugged heartland. Not to be missed."  Taran Adarsh of Bollywood Hungama gave the movie 3.5 stars out of 5, saying that "On the whole, Gangs of Wasseypur symbolizes the fearless new Indian cinema that shatters the clichés and conventional formulas, something which Anurag Kashyap has come to be acknowledged for. It has all the trappings of an entertainer, but with a difference. The film prides itself with substance that connects with enthusiasts of new-age cinema. But, I wish to restate, one needs to have a really strong belly to soak up to a film like Gangs of Wasseypur. Also, this striking movie-watching experience comes with a colossal length and duration. The reactions, therefore, would be in extremes. Gangs of Wasseypur is for that segment of spectators who seek pleasure in watching forceful, hard-hitting and gritty movies." 

Rajeev Masand of CNN-IBN gave the movie 3.5 stars out of 5, concluding that "Bolstered by its riveting performances and its thrilling plot dynamics, this is a gripping film that seizes your full attention. I’m going with three-and-a-half out of five for Anurag Kashyaps Gangs of Wasseypur. Despite its occasionally indulgent narrative, this bullet-ridden saga is worthy of a repeat viewing, if only to catch all its nuances. Don’t miss it."  Mansha Rastogi of Now Running gave the movie 3.5 stars out of 5, commenting that "Gangs of Wasseypur works like an explosive leaving you wanting for more. Gangs of Wasseypur – Part 2 will definitely be a film eagerly awaited! Devour part one in the meantime!" 

Madhureeta Mukherjee of Times of India gave the movie 3.5 stars out of 5, saying that "Director Anurag Kashyap, in his trademark style of story- telling – realistic, with strong characters, over-the-top sequences, and unadulterated local flavour (crude maa-behen gaalis galore), gruesome bloody violence and raw humour – interestingly spins this twisted tale. This first of a two-part film, is ambitious indeed; showing promise of brilliance in parts, but not bullet-proof to flaws. With a runtime this long, meandering side tracks and random sub-plots, countless characters, documentary-style narrative backed with black and white montages from actual history, it loses blood in the second half because of the Directors over-(self)indulgence. So, hold on to your guns, gamchas and womaniyas." 

Saibal Chatterjee of NDTV gave the movie 3.5 stars out of 5, concluding that "It may not be for the faint-hearted and the prissy. Gangs of Wasseypur is a heavyweight knockout punch. You’re down for the count!"  Blessy Chettiar of DNA gave the movie 3.5 stars out of 5, commenting that "Even though theres so much going for Part 1, theres something always amiss, something that leaves you underwhelmed after all those expectations. May be its a hope of a dashing Part 2. Lets wait and watch." 

Kunal Guha of Yahoo! gave the movie 3 stars out of 5, concluding that "Considering the amount of blood spilled in this film, it could’ve just been called ‘Gangs of Sauce-e-pur’. Hot and sweet and different. ‘Bata deejiyega sabko!’"  Roshni Devi of Koimoi gave the movie 3 stars out of 5, saying that "Gangs of Wasseypur is a very good movie that gets bogged down by the endless characters and length of the movie. If you love those hinterland mafia movies, this is definitely for you." 

On the contrary, Raja Sen of Rediff gave the movie 2.5 stars out of 5, concluding that "It is the excess that suffocates all the magic, originality dying out for lack of room to breathe. Kashyap gets flavour, setting and character right, but the lack of economy cripples the film. There is a lot of gunfire, but like the fine actors populating its sets, Wasseypur fires too many blanks." 

Mayank Shekhar of   says, "Most movies have a definite beginning (starting point), middle (turning point) and end (high point), or what playwrights call the three act structure in a script. There doesn’t seem to be one here, at least on the face of it. The genre it comes closest to then is an epic, spelt with a capital E, along the lines of say Francis Ford Coppolas Godfather trilogy, or this films immediate inspiration Martin Scorseses Gangs of New York (2002). And, of course, it is like all mythologies are supposed to be. You enjoy them for the parts rather than caring merely for the heros final goal. If it wasn’t a film, this would’ve been a stylised graphic novel. But you would’ve missed a memorable background score and striking sound design. "

;International
The film met positive international reviews. Deborah Young of The Hollywood Reporter called the film "an extraordinary ride through Bollywoods spectacular, over-the-top filmmaking". Referring to the violence and pace of the film she says "Gangs of Wasseypur puts Tarantino in a corner with its cool command of cinematically-inspired and referenced violence, ironic characters and breathless pace". 
 Variety notes Kashyap never lets his diverse influences of old-school Italo-American mafia classics a la Coppola, Scorsese and Leone, as well as David Michods taut crime thriller "Animal Kingdom, override the distinct Indian color. Calling the film "the love child of Bollywood and Hollywood," she felt the film was "by turns pulverizing and poetic in its depiction of violence."  Lee Marshall of Screen International writes "the script alternates engagingly between scenes of sometimes stomach-churning violence and moments of domestic comedy, made more tasty by hard-boiled lines of dialogue like “in Wasseypur even the pigeons fly with one wing, because they need the other to cover their arse”. He describes song lyrics "as if mouthed by a Greek chorus of street punks" commenting sarcastically on whats happening onscreen. 

===Box office===
Gangs of Wasseypur collected ₹12.25cr in first four days. Gangs of Wasseypur collected ₹10cr net approx over its first weekend. The collections were good all over.  Both instalments of the film were made at a production cost of ₹18.5cr and with ₹17.5cr as the total first week collection of the first part, film has successfully recovered the total production cost minus promotion cost. Gangs of Wasseypur held up week two but with low collections. The second week was around   7&nbsp;crore nett. Gangs of Wasseypur – Part 1 has earned ₹ 27.52cr in India, as of 27 July 2012 and the film was finally declared as a hit. 

The success party for the film was held at Escobar in Bandra, Mumbai on Thursday, 5 July, late evening. 

==Differences from actual events==
The film mainly draws its story from the real life gang wars that took place in the region of Dhanbad, Jharkhand. There are several differences in the film which contradict actual documented events most notability the character of Faizal Khan (based on Fahim Khan) who dies in the climax. Fahim Khan is currently in jail in Hazaribagh and has been sentenced to life imprisonment. http://www.thehindu.com/arts/cinema/pangs-of-wasseypur/article3967902.ece Pangs of Wasseypur  In the film, Sardar Khan marries the Bengali girl but in real life, the woman was kept as a mistress. Most of the gang wars were between the gangs of Wasseypur, not with the Singhs, who had been instrumental in instigating these wars, but never participated in them.  There was no character akin to Shahid Khan.

Another scene in the movie, where a Muslim girl is kidnapped by Singh’s men, has been portrayed conversely. In real life, the victim was a local Hindu girl and the kidnappers were a few goons from Wasseypur. The members of the Singh family ultimately had to threaten the entire Wasseypur community to return the girl in 24 hours. The girl was eventually returned as the Singhs were regarded in the village with might and fear. 

The character of Ramadhir Singh is based on Surajdeo Singh. In the films climax, Singh is brutally killed by Faizal but in real life, Singh died of natural causes in June 1991. 

Fazloos character is based on Sabir Alam. In the film, Fazloo is killed and dismembered by Faizal Khan. In real life, Sabir Alam and Fahim Khan were childhood friends turned enemies. Sabir was awarded life sentence in 2007 for the murder of Fahim Khan’s mother and aunt, is out on bail in Wasseypur. 

The mafias downfall in Dhanbad didnt come from gang wars but rather it came from the differences between Kunti Singh, the widow of Surajdeo Singh, and his three brothers – Baccha Singh, Rajan Singh and Ram Dhani Singh – which gave others an opportunity to make space for themselves. 

==References==
 
*  

==External links==
 
 
* 
* 
* 
* 
* 
* 
* 
* 

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 