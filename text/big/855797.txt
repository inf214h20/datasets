A Walk to Remember
 
{{Infobox film
| name           = A Walk to Remember
| image          = A Walk to Remember Poster.jpg
| caption        = Theatrical release poster
| director       = Adam Shankman
| producer       = Denise Di Novi Hunt Lowry
| writer         = Nicholas Sparks   Karen Janszen
| starring       = Shane West Mandy Moore 
| music          = Mervyn Warren
| cinematography = Julio Macat
| editing        = Emma E. Hickox Di Novi Pictures Pandora Cinema
| distributor    = Warner Bros.
| released       =  
| runtime        = 102 minutes
| country        = United States
| language       = English
| budget         = $11 million
| gross          = $47,494,916
}}
 teen Romance romantic drama novel of the same name by Nicholas Sparks. The film stars Shane West and Mandy Moore, was directed by Adam Shankman, and produced by Denise Di Novi and Hunt Lowery for Warner Bros.
==Plot==
 
The popular, rebellious teenager Landon Carter (Shane West) is threatened with expulsion from school after he and his friends leave evidence of underage drinking on the school grounds and  cause a student to suffer serious injuries from a prank they pulled on him. The head of the school gives Landon the choice of being expelled from school or atoning for his actions by tutoring fellow students and participating in the class play.
 
During these functions, he notices Jamie Sullivan (Mandy Moore), a girl he has known since kindergarten and who has attended many of the same classes as him, and is also the local ministers daughter. Since hes one of the in-crowd, he has seldom paid any attention to Jamie who wears modest dresses all the time and owns only one sweater. She makes no attempt to wear make-up or otherwise improve her looks or attract attention to herself.

Landon has trouble learning his lines for the school play. He asks Jamie, who is also in the play, for help, but on one condition: Jamie warns Landon not to fall in love with her. Landon and Jamie begin practicing together at her house after school. They get to know each other and a spark of affection arises between them.

On the opening night of the play, Jamie astounds Landon and the entire audience with her beauty and her voice. Onstage at the peak of the ending to the play, Jamie sings a song. When Jamie finishes, Landon kisses Jamie (not part of the play).

Jamie avoids Landon after the play, and it is not until a cruel prank is played on Jamie by Landons friends that she warms up to him again. Landon asks Jamie on a date soon after, but Jamie says her father doesnt allow her to date. Landon asks her father if he can date his daughter. Reluctant at first, he gives in.

On their first date, Landon helps Jamie to fulfill her list of things she  wants to achieve in life, such as being in two places at once, and getting a tattoo; she also hopes to read all of the hundred great American books recommended by her English teacher. After that, they go to the docks. Jamie tells Landon about how she experiences belief and how its like the wind. It is then that he asks her to kiss him.

On another date, Landon asks Jamie what her plans for the future are, she confesses she isnt making any because she has leukemia and hasnt been responding to treatment. Landon asks for his fathers help in curing her, but the doctors all say there is no cure.

One by one, his friends become aware of the tragedy looming for Jamie and Landon. They give their support to him. Jamies condition grows worse and she gets sent to the hospital.

Recovering, Jamie gives Landon a book that once belonged to her mother. She states that maybe God sent Landon to her to help her through the rough times and that Landon is her angel.

Unbeknownst to Landon,  Jamie is given private home care by Landons estranged father relieving her fathers financial burden. Landon visits his dad, tearfully thanking him for his help. They embrace and are reunited.

Landon is building a telescope for Jamie to be able to see a comet in the springtime. Jamies father helps him get it finished in time. The telescope is brought to her on the balcony. She gets a beautiful view of the comet through the new telescope. It is then that Landon proposes marriage.

Jamie tearfully accepts, and they get married in the church in which her deceased mother grew up and got married. Jamie and Landon spend their last summer together, filled with lots of love like no other. Jamie dies when summer ends.

Four years later, Landon finished college and has been accepted into medical school. Landon visits Reverend Sullivan to return to him Jamie’s precious book that belonged to her mother. Landon apologizes to the Reverend for not letting Jamie witness a miracle (an ambition she expressed in the class yearbook). The Reverend disagrees saying that she did witness a miracle and that her miracle was Landon.

Landon visits the docks contemplating the belief that although Jamie is dead, that she is with him. It is then that he understands love is like the wind; you cant see it, but you can feel it.

==Cast==
* Shane West as Landon Rollins Carter
* Mandy Moore as Jamie Elizabeth Sullivan
* Peter Coyote as Reverend Hegbert Sullivan
* Daryl Hannah as Cynthia Carter
* Lauren German as Belinda
* Clayne Crawford as Dean Al Thompson as Eric
* Paz de la Huerta as Tracy
* David Lee Smith as Dr. Carter
* Jonathan Parks Jordan as Walker
* Matt Lutz as Clay Gephardt

==Background and production==
The inspiration for A Walk to Remember was Nicholas Sparks sister, Danielle Sparks Lewis, who died of cancer in 2000. In a speech he gave after her death in Berlin, the author admits that "In many ways, Jamie Sullivan was my younger sister". The plot was inspired by her life; Danielle met a man who wanted to marry her, "even when he knew she was sick, even when he knew that she might not make it".  Both the book and film are dedicated to Danielle Sparks Lewis.

It was filmed in Wilmington, North Carolina, at the same time that Divine Secrets of the Ya-Ya Sisterhood (2002) and the TV show Dawsons Creek were being filmed there. Many of the sets were from Dawsons Creek (1998) - particularly the school, hospital and Landons home.    The total shooting time was only 39 days, despite Mandy Moore being able to only work 10 hours a day because she was a minor.  Daryl Hannah, who wore a brown wig as her character, had received a collagen injection in her lips, which went awry and caused noticeable swelling. By the end of filming, however, the symptoms were less obvious. 

===Casting===
Director Adam Shankman wanted the lead characters to be portrayed by young actors: "I wanted young actors with whom teenagers could connect", he said.    Shankman arranged a meeting with Shane West after he saw him in a magazine. He was looking for someone who could transition from being very dark to very light. He described his choice as "an instinct" he had about West, who would appear in almost every scene and had "to be either incredibly angry and self-hating or madly in love and heroic."  West said: "I dont generally read love stories, but after reading the screenplay, I knew I couldnt wait to read the book so I could truly understand Nicholas Sparks story and how he envisioned the character of Landon. Its a beautiful story and the characters are very believable, which is what attracted me to the project. 

Shankman said of Mandy Moore that she "has the voice and the face of an angel" and added that she is luminous.  Moore explained that she was moved by the book: "I had such a visceral reaction to it that I remember not being able to read because I was almost hyperventilating while I was crying." Commenting on the film, she said: "It was my first movie and I know people say it may be cliche and its a tearjerker or its cheesy, but for me, its the thing Im most proud of." 

==Comparisons to novel==
While there are many similarities to the novel by Nicholas Sparks, many changes were made. On his personal website, Sparks explains the decisions behind the differences. For example, he and the producer decided to update the setting from the 1950s to the 1990s, worrying that a film set in the 50s would fail to draw teens. "To interest them," he writes, "we had to make the story more contemporary." {{cite web| last=Sparks| first=Nicholas| authorlink=Nicholas Sparks (author)| title=Nicholas Sparks on the Movie Adaptation of A Walk to Remember| url=http://www.nicholassparks.com/Novels/AWalkToRemember/Movie.html
| archiveurl=http://web.archive.org/web/20080417085418/http://www.nicholassparks.com/Novels/AWalkToRemember/Movie.html
| archivedate=2008-04-17| accessdate=2007-07-12}} ( )  To make the update believable, Landons pranks and behavior are worse than they are in the novel; as Sparks notes, "the things that teen boys did in the 1950s to be considered a little rough are different than what teen boys in the 1990s do to be considered rough." 

Sparks and the producer also changed the play in which Landon and Jamie appear. In the novel, Hegbert wrote a Christmas play that illustrated how he once struggled as a father. However, due to time constraints, the sub-plot showing how he overcame his struggles could not be included in the film. Sparks was concerned that "people who hadnt read the book would question whether Hegbert was a good father", adding that "because he is a good father and we didnt want that question to linger, we changed the play." 

A significant difference is that at the end of the novel, unlike the film, it is ambiguous whether Jamie died. Sparks says that he had written the book knowing she would die, yet had "grown to love Jamie Sullivan", and so opted for "the solution that best described the exact feeling I had with regard to my sister at that point: namely, that I hoped she would live." {{cite web| last=Sparks| first=Nicholas| authorlink=Nicholas Sparks (author)
| title=FAQ on A Walk to Remember - Did Jamie Die?| url=http://www.nicholassparks.com/Novels/AWalkToRemember/FAQ.html#1
| archiveurl=http://web.archive.org/web/20080416040410/http://www.nicholassparks.com/Novels/AWalkToRemember/FAQ.html#1
| archivedate=2008-04-16| accessdate=2007-07-12}} 

==Release==

===Box office=== Black Hawk Down.

Even though not a critical success, it was a modest box office hit, earning $41,281,092 in the United States alone,  and a sleeper hit in Asia. The total revenue generated worldwide was $47,494,916.

===Critical response===
  average score of 35, based on 26 reviews, which indicates "generally unfavorable".  Entertainment Weekly retitled the film "A Walk to Forget".  In 2010, Time (magazine)|Time named it one of the 10 worst chick flicks ever made. 

A Walk to Remember found a warmer reception with the general public, particularly in the Christian community due to the films moral values; as one reviewer from Christianity Today approvingly noted, "The main character is portrayed as a Christian without being psychopathic or holier-than-thou". {{cite journal| last=Overstreet| first=Jeffrey| title=A Walk to Remember| publisher=Christianity Today| date=January 23, 2002| url= http://www.christianitytoday.com/movies/reviews/walktoremember.html
| archiveurl= http://web.archive.org/web/20080503054047/http://www.christianitytoday.com/movies/reviews/walktoremember.html
| archivedate=2008-05-03| postscript= }}  
Chicago Sun-Times Roger Ebert praised Mandy Moore and Shane West for their "quietly convincing" acting performances. {{cite news| last=Ebert| first=Roger| authorlink=Roger Ebert| title=A Walk to Remember
| date=2002-01-25| url= http://rogerebert.suntimes.com/apps/pbcs.dll/article?AID=/20020125/REVIEWS/201250306/1023
| accessdate=2007-07-12| work=Chicago Sun-Times}}  The   deemed it one of the 30 most romantic movies of all time. 

===Accolades===
{| class="wikitable"
|- Year
!align="left"|Ceremony Category
!align="left"|Nominated
!Result
|- 2002
|align="left"|MTV Movie Awards MTV Movie Best Breakthrough Female Performance Mandy Moore
| 
|- Teen Choice Awards Choice Breakout Performance – Actress Mandy Moore
| 
|- Choice Chemistry Mandy Moore|Moore/Shane West
| 
|- Choice Liplock
|align="elft"|Moore/West
| 
|- MYX Music Awards Song of the Year
|align="left"|"Cry (Mandy Moore song)|Cry" by Mandy Moore
| 
|- 2011
|align="left" rowspan="2"|Yahoo! OMG Awards Philippines Best Foreign Romantic Film of 2000s Adam Shankman
| 
|- Favorite Actress of 2000s Mandy Moore
| 
|}

===Home media===
A Walk to Remember was released on DVD on July 9, 2002. 

==Soundtrack==
{{Infobox album
| Name       = A Walk to Remember: Music From the Motion Picture
| Type       = soundtrack
| Artist     = Various Artists
| Cover      = 
| Released   =   contemporary Christian, post-grunge
| Length     =    (Standard)     (2003 Special Expanded Edition)  Sony Music Soundtrax
| Producer   = Jon Leshay
| Misc       = {{Singles
| Type       = soundtrack
| Name       = A Walk to Remember: Music From the Motion Picture Cry
| single 1 date =  
}}}}
 Sony Music Soundtrax on January 15, 2002.  It features six songs by Mandy Moore and others by acts Switchfoot, Rachael Lampa and many more.

The lead song "Cry (Mandy Moore song)|Cry" was originally released on Moores Mandy Moore (album)|self-titled third studio album. The soundtrack also includes two versions of Switchfoots song "Only Hope" including the version Moore sang in the film.

Moores manager, Jon Leshay, the musical supervisor for A Walk to Remember, "instantly wanted" Switchfoots music to be a vital part of the film after hearing them. He later became Switchfoots manager.  When they were approached to do the film, the band was unfamiliar with Moore or her music (despite her status as a pop star with several hits on the charts). Before their involvement with A Walk to Remember, Switchfoot was only recognized in their native San Diego and in Contemporary Christian music circles, but have since gained mainstream recognition, with a double platinum album, The Beautiful Letdown which included hits such as "Meant to Live" and "Dare You to Move".

The soundtrack was re-released on October 21, 2003  as an special expanded edition and featured three songs that were not originally included on the first release of the soundtrack but were featured in the film. The song "Only Hope" by Moore had dialogue added that featuring Shane West as his character Landon Carter taken from the scene from where the song is featured in the film, as well as Wests narration at the end of the film.

===Track listing===
{{tracklist
| headline          = Standard edition
| writing_credits   = no
| total_length      =  
| extra_column      = Recording artist(s)
| title1            = Dare You to Move
| extra1            = Switchfoot
| length1           = 4:09 Cry
| extra2            = Mandy Moore
| length2           = 3:43
| title3            = Someday Well Know Jonathan Foreman
| note3             = cover of New Radicals
| length3           = 3:52
| title4            = Dancing in the Moonlight|Dancin in the Moonlight
| extra4            = Toploader
| note4             = cover of King Harvest
| length4           = 3:52
| title5            = Learning to Breathe
| extra5            = Switchfoot
| length5           = 4:36
| title6            = Only Hope
| extra6            = Moore
| note6             = cover of Switchfoot
| length6           = 3:53
| title7            = Its Gonna Be Love
| extra7            = Moore
| length7           = 3:51
| title8            = You
| extra8            = Switchfoot
| length8           = 4:14
| title9            = If You Believe
| extra9            = Rachael Lampa
| length9           = 3:49
| title10           = No One Cold
| length10          = 3:17
| title11           = So What Does It All Mean?
| extra11           = Shane West|West, Gould, & Fitzgerald
| length11          = 3:00
| title12           = Mother We Just Cant Get Enough|Mother, We Just Cant Get Enough
| extra12           = New Radicals
| length12          = 5:45
| title13           = Only Hope
| extra13           = Switchfoot
| length13          = 4:14
}}
{{tracklist
| headline          = 2003 Special Expanded Edition
| writing_credits   = no
| extra_column      = Recording artist(s)
| total_length      =  
| collapsed         = yes
| title1            = Dare You to Move
| extra1            = Switchfoot
| length1           = 4:09
| title2            = Cry
| extra2            = Moore
| length2           = 3:43
| title3            = Someday Well Know
| extra3            = Moore and Foreman
| note3             = cover of New Radicals
| length3           = 3:52
| title4            = Dancin in the Moonlight
| extra4            = Toploader
| note4             = cover of King Harvest
| length4           = 3:52
| title5            = Learning to Breathe
| extra5            = Switchfoot
| length5           = 4:36
| title6            = Only Hope
| extra6            = Moore as Jamie Sullivan with dialogue by Shane West as Landon Carter
| note6             = cover of Switchfoot
| length6           = 3:53
| title7            = Its Gonna Be Love
| extra7            = Moore
| length7           = 3:51
| title8            = You
| extra8            = Switchfoot
| length8           = 4:14
| title9            = If You Believe
| extra9            = Rachael Lampa
| length9           = 3:49
| title10           = No One
| extra10           = Cold
| length10          = 3:17
| title11           = So What Does It All Mean?
| extra11           = West, Gould, & Fitzgerald
| length11          = 3:00
| title12           = Mother, We Just Cant Get Enough
| extra12           = New Radicals
| length12          = 5:45 Cannonball
| extra13           = The Breeders
| note13            = 2003 Special Expanded Edition bonus track
| length13          = 3:37
| title14           = Friday on My Mind
| extra14           = Noogie
| note14            = 2003 Special Expanded Edition bonus track
| length14          = 3:14
| title15           = Empty Spaces Fuel
| note15            = 2003 Special Expanded Edition bonus track
| length15          = 3:26
| title16           = Only Hope
| extra16           = Switchfoot
| length16          = 4:16
| title17           = Cry
| note17            = Music Video) (Multi-media track
| extra17           = Moore
| length17          = 3:41
}}

;Complete listing of music in the film 
# "Cannonball (The Breeders song)|Cannonball" — The Breeders
# "So What Does It All Mean?" — Shane West|West, Gould, & Fitzgerald Fuel
# "Lighthouse" — Mandy Moore
# "Friday on My Mind" — Noogie
# "Anything You Want" — Skycopter 9
# "Numb in Both Lips" — Soul Hooligan
# "Tapwater" — Onesidezero
# "If You Believe" — Rachael Lampa
# "No Mercy" — Extra Fancy Cold
# "Enough" — Matthew Hager
# "Mother We Just Cant Get Enough|Mother, We Just Cant Get Enough" — New Radicals
# "Only Hope" — Mandy Moore
# "Get Ur Freak On" — Missy Elliott
# "Flood" — Jars of Clay
# "Dancing in the Moonlight|Dancin in the Moonlight" — Toploader Jonathan Foreman Learning to Breathe" — Switchfoot All Mixed 311
# "Dare You to Move" — Switchfoot
# "You" — Switchfoot
# "Its Gonna Be Love" — Mandy Moore
# "Only Hope" — Switchfoot
# "Cry (Mandy Moore song)|Cry" — Mandy Moore

==In other media==
In the HBO television series Entourage (TV series)|Entourage, the character of Vincent Chase was credited as having a small supporting role in the film. In the fictional Entourage universe, Chase has an on-set relationship with Mandy Moore during the filming of A Walk to Remember.

==References==
 

==External links==
 
*  
*   (Archive)
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 