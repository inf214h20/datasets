Cops vs. Thugs
{{Infobox film
| name           = Cops vs. Thugs
| image          = 
| image_size     =
| caption        = Japanese release poster
| director       = Kinji Fukasaku
| producer       =
| writer         = Kazuo Kasahara Goro Kusakabe (original concept)
| starring       = Bunta Sugawara Hiroki Matsukata
| music          = Toshiaki Tsushima
| cinematography = Shigeru Akatsuka
| editing        = Toei
| released       = April 26, 1975
| runtime        = 100 minutes
| country        = Japan
| language       = Japanese
| gross          =
}} Kino International released the film on DVD in North America in 2006. 

==Plot==
In Kurashima City, there are two yakuza families; the Kawade, who use political connections to further their activities, and the Ohara, who have an alliance with the local police. When Ohara acting boss Hirotani usurps a staged land deal away from Kawade, thanks to the help of his police friend Kuno, a war breaks out. At the same time, Kunos superiors start cracking down on the yakuza and command the cops stop fraternizing with criminals.

==Cast==
 
*Bunta Sugawara as Tokumatsu Kuno
*Hiroki Matsukata as Kenji Hirotani
*Mikio Narita as Katsumi Kawade
*Tatsuo Umemiya as Shoichi Kaida
*Hideo Murota as Tsukahara
*Shingo Yamashiro as Yasuo Kawamoto
*Reiko Ike as Mariko
*Jukei Fujioka as Ikeda
*Asao Sano as Yusaku Yoshiura
*Nobuo Kaneko as Masaichi Tomoyasu
*Harumi Sone as Kyuichi Okimoto
*Takuzo Kawatani as Taku Matsui
*Tatsuo Endo as Takeo Ohara
 
*Kunie Tanaka as Kinpachi Komiya
*Toru Abe as Azuma Kikuchi
*Akira Shioji as Chujiro Shiota
*Shotaro Hayashi as Shimodera
*Masaharu Arikawa as Tokuda
*Sanae Nakahara as Reiko
*Yoko Koizumi as Yuri
*Maki Tachibana as Kasumi
*Keiko Yumi as Miya
*Midori Shirai as Chiyomi
*Masako Matsumoto as Mitsuyo
*Gentaro Mori as Tanpo
 

==Production==
Tetsuya Watari was originally set to play Hirotani, but had to step down due to illness. The role then went to Hiroki Matsukata. 

==References==
 

==External links==
*  

 

 
 
 
 