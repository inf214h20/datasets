Novel (film)
{{Infobox Film
| name           = Novel
| image          = Novel (film).jpg
| caption        =
| director       = East Coast Vijayan
| producer       = East Coast Vijayan
| writer         =
| starring       = Jayaram  Sadha Jagathy Sreekumar
| music          = M. Jayachandran,  Bala Bhaskar
| cinematography =
| editing        =
| distributor    =
| released       =  
| runtime        =
| country        = India
| language       = Malayalam
| budget         =
| gross          =
}}
Novel is a 2008 Malayalam film produced and directed by East Coast Vijayan. This is East Coast Vijayans debut directorial film.

== Plot ==
Sethunath (Jayaram) is a prosperous business man and also a writer. However, when his creation titled Swantham becomes a best seller and bags the commonwealth awards. The writer is the least interested so much so that he is not even aware who translated his work & earned the award for the book.

Aneesa, a journalist, is determined to get a personal interview with her favorite writer Sethu and does not hesitate to get it at the expense of bribing Sethus secretary Subramaniam Swamy and finally succeeds. Luckily for her, Sethu is impressed with her resilience and also the fact that she comes from the same orphanage that he hailed from makes him open his heart. He talks about his failed marriage and Priyanandini (Sadha) whom he encounters during the making of a lottery commercial. Gradually, Priya reaches the pinnacle of stardom with the support of Sethu.

In this process, both of them fall in love with each other and decide to get married. Manju (Shari), Sethus wife from the US does not allow this. What does Manju do forms the rest of the story.

== Cast ==
* Jayaram as Sethunath
* Sadha as Priyanandini
* Sharika Menon as Manju
* Jagathy Sreekumar
* Nedumudi Venu
* Sarika
* Saiju Kurup
* Bindu Panicker
* Indrans Devan
* M. Jayachandran as himself in a telephone call (cameo)

== External links ==
*  

 
 
 


 