The Singing Hill
{{Infobox film
| name           = The Singing Hill
| image          = The_Singing_Hill_Poster.jpg
| border         = yes
| caption        = Theatrical release poster
| director       = Lew Landers
| producer       = Harry Grey
| screenplay     = Olive Cooper
| story          = {{Plainlist|
* Jesse Lasky Jr.
* Richard Murphy
}}
| based on       =  
| starring       = {{Plainlist|
* Gene Autry
* Smiley Burnette
* Virginia Dale
}}
| music          = Raoul Kraushaar (supervisor)
| cinematography = William Nobles
| editing        = Lester Orlebeck
| studio         = Republic Pictures
| distributor    = Republic Pictures
| released       =  
| runtime        = 54 minutes Magers 2007, p. 187. 
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} Western film Blueberry Hill" which would become a standard recorded by such artists as Louis Armstrong (1949), Fats Domino (1956), and Elvis Presley (1957).  |group=N}} The song became one of Autrys best-selling recordings. Magers 2007, p. 188.  In 1987, "Blueberry Hill" received an ASCAP Award for Most Performed Feature Film Standards on TV.      

==Plot==
Singing cowboy Gene Autry (Gene Autry) is the foreman of the Circle R Ranch, which has been in the Adams family for generations. The ranchers in the area have enjoyed free grazing rights on the Circle R for years. Recently, the madcap heiress of the ranch, beautiful Jo Adams (Virginia Dale), negotiated the sale of the ranch in order to pay off some of her debts. She accepted a $25,000 down payment, with an option to purchase in 60 days, from unscrupulous cattle broker John Ramsey (George Meeker) who is conspiring with Adams business manager James Morgan (Harry Stubbs) to buy the ranch and cut off grazing rights to the ranchers. 
 Mary Lee), drive to Jos palatial house in the city. The scatterbrained Jo mistakes them for the band hired to play at her birthday party that evening, and they perform in order to stay.

After the party, Gene explains who they are and urges Jo to return to the ranch and run it the way her grandfather did. The frivolous heiress, however, has no intention of changing her extravagant lifestyle. With few options available to him, Gene abducts Jo and her butler, Dada (Gerald Oliver Smith), and takes them back to the Circle R Ranch, where Pop Sloan (Wade Boteler), a rancher who has known Jo all her life, has organized a welcoming party for her. Despite her appreciation, Jo tells Gene that she is broke and has to sell the ranch to pay her debts.

Soon after, Gene approaches Judge Henry Starbottle (Spencer Charters) and explains the impact the sale of the Circle R would have on the ranchers in the area. They conspire to have Jo declared legally incompetent in order to buy some time. At the hearing, Henry rules that she is to become a ward of the court, and Gene is placed in charge of the ranch until Jo can prove her competency.
 The dam used in The Singing Hill was a miniature built by the Lydecker brothers for Ralph Byrds Born to Be Wild (1938). It was later used in the serials Dick Tracy Returns (1938) and King of the Texas Rangers (1941).
|group=N}} The dam is destroyed and the cattle stampede. In the ensuing chaos, most of the cattle are drowned or dispersed, and Gene is barely able to save Pop from drowning.

Meanwhile, Jo wins her competency hearing, regains control over the ranch, and quickly fires Gene and the other Circle R cowboys. As the men are packing to leave, Jo arrives to say goodbye to the men, but is distressed to see families who will be displaced because of her actions. She drives away, but is stopped on the road by Judge Henry, who tells her that she must say goodbye to Pop before she leaves. Jo is overcome with grief when the judge brings her to the old mans funeral; the old man did not survive his ordeal.

Later, after Jo reveals that she told Ramsey about the cattle drive, Gene realizes that Ramsey was behind the dam explosion. When Jo tells him that she now intends to keep the ranch, Gene devises a plan, sending her to Ramseys office where she tells him that Gene is holding Morgan in an attempt to get information from him. Worried that Morgan will implicate him in the dam explosion, Ramsey hires a group of gunmen to kill Gene. When the hired guns sneak onto the Circle R that night, they are beaten and captured by Gene and the ranch hands. Gene himself apprehends Ramsey after giving the corrupt cattle broker a beating. Afterwards, the ranchers are promised a good deal on their next herds, and Jo keeps Gene as her foreman. 

==Cast==
* Gene Autry as Gene Autry
* Smiley Burnette as Frog Millhouse
* Virginia Dale as Jo Adams Mary Lee as Patsy
* Spencer Charters as Judge Henry Starrbottle
* Gerald Oliver Smith as Dada the Butler
* George Meeker as John R. Ramsey
* Wade Boteler as Pop Sloan
* Harry Stubbs as James Morgan
* Cactus Mack as Cactus Mack
* Jack Kirk as Rancher Flint
* Frankie Marvin as Short Dancing Cowhand
* William H. OBrien as Butler
* Dan White as Rancher (uncredited)
* Champion as Genes Horse (uncredited)    

==Production==
===Filming and budget===
The Singing Hill was filmed March 11–24, 1941. The film had an operating budget of $86,869 (equal to $ }} today), and a negative cost of $87,184. 

===Casting===
Virginia Dale was a former Earl Carroll showgirl. Working for Paramount as a contract player, she was loaned out to Republic Pictures for The Singing Hill when the studios original choice, Patricia Morison refused the part because of the unsuitability of the story and the clothes. 

===Stuntwork===
* Joe Yrigoyen (Gene Autrys stunt double)
* Tommy Coats
* Nellie Walker
* B. Kane
* Yakima Canutt   

===Filming locations===
* Andy Jauregui Ranch, Placerita Canyon Road, Newhall, California, USA
* Walker Ranch, Placerita Canyon Road, Newhall, California, USA Red Rock Canyon State Park, Highway 14, Cantil, California, USA    

===Soundtrack=== Blueberry Hill" (Al Lewis, Larry Stock, Vincent Rose) by Gene Autry
* "The Last Round-Up" (Billy Hill) by Gene Autry
* "Let a Smile Be Your Umbrella on a Rainy Day" (Sammy Fain, Irving Kahal, Francis Wheeler)
* "Tumbledown Shack in Havana" (Jule Styne, Eddie Cherkose, Sol Meyer)
* "Sail the Seven Seas" (Smiley Burnette) by Smiley Burnette
* "Patsys Birthday Routine" (Jule Styne, Sol Meyer)
* "Ridin Down That Old Texas Trail" (Milt Mabie, Dott Massey)
* "Good Old-Fashioned Hoedown" by Gene Autry
* "Therell Never Be Another Pal Like You" (Gene Autry, Johnny Marvin)    

==References==
;Notes
 

;Citations
 

;Bibliography
 
*  
*  
*   

==External links==
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 