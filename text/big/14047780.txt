Battle Beneath the Earth
{{Infobox Film
| name           = Battle Beneath the Earth
| image          = Battle_Beneath_the_Earth_OS.jpg
| director       = Montgomery Tully
| producer       = {{Plain list|*Charles Reynolds
*Charles F. Vetter}}
| writer         = Charles F. Vetter
| starring       = {{Plain list|*Kerwin Mathews
*Viviane Ventura
*Robert Ayres
*Peter Arne
*Al Mulock}} Ken Jones
| cinematography = Kenneth Talbot
| editing        = Sidney Stone
| distributor    = Metro-Goldwyn-Mayer
| released       = October 1967
| runtime        = 91 minutes
| country        = UK
| language       = English
}}
Battle Beneath the Earth (1967) is a British spy film starring Kerwin Mathews. It was released by Metro-Goldwyn-Mayer. The film also features character actor Ed Bishop, who later went on to star in the Gerry Anderson cult TV show UFO (TV series)|UFO.

==Plot==
The plot involves rogue elements of the communist Chinese army who use fantastic burrowing machines in an effort to place atomic bombs under major U.S. cities. The U.S. Navy sends troops underground to combat them. The film has been described as "deliriously paranoid". 

==Cast==
* Kerwin Mathews as Cmdr. Jonathan Shaw
* Viviane Ventura as Tila Yung Robert Ayres as Adm. Felix Hillebrand
* Peter Arne as Arnold Kramer
* Al Mulock as Sgt. Marvin Mulberry Martin Benson as Gen. Chan Lu
* Peter Elliott as Dr. Kengh Lee Earl Cameron as Sgt. Seth Hawkins John Brandon as Maj. Frank Cannon
* Ed Bishop as Lt. Cmdr. Vance Cassidy
* Carl Jaffe as Dr. Galissi

==Release== UA film library. Metro-Goldwyn-Mayer originally handled North American theatrical distribution
 The Ultimate Warrior. 

==Score== musical score. 

==References==
 

== External links ==
*  
*  

 

 
   
 
   
   
 
 
 
 
 