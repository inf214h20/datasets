Skipalong Rosenbloom
{{Infobox film
| name           = Skipalong Rosenbloom
| image          = 
| alt            = 
| caption        = 
| director       = Sam Newfield
| producer       = Wally Kline 
| screenplay     = Eddie Forman Dean Riesner
| story          = Eddie Forman
| starring       = Max Rosenbloom Max Baer Jackie Coogan Fuzzy Knight Hillary Brooke Jacqueline Fontaine
| music          = Irving Gertz Ernest Miller
| editing        = Victor Lewis J.R. Whittredge 
| studio         = Wally Kline Enterprises
| distributor    = United Artists
| released       =  
| runtime        = 71 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 Western comedy film directed by Sam Newfield and written by Eddie Forman and Dean Riesner. The film stars Max Rosenbloom, Max Baer, Jackie Coogan, Fuzzy Knight, Hillary Brooke and Jacqueline Fontaine. The film was released on April 30, 1951, by United Artists.  

==Plot==
 

== Cast ==
*Max Rosenbloom as Skipalong Rosenbloom 
*Max Baer as Butcher Baer
*Jackie Coogan as Buck Lovelace
*Fuzzy Knight as Sneaky Pete	
*Hillary Brooke as Square Deal Sal
*Jacqueline Fontaine as Caroline Witherspoon
*Raymond Hatton as Granpappy Tex Rosenbloom
*Ray Walker as TV Announcer
*Al Shaw as Al 
*Sam Lee as Sam 
*Joseph J. Greene as Judge Bean 
*Dewey Robinson as Honest John
*Whitey Haupt as The Pecos Kid
*Carl Mathews as Fake Indian
*Artie Ortego as Henchman Artie 

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 
 
 