Fuelin' Around
{{Infobox Film |
  | name           = Fuelin Around|
  | image          = Fuelinfaround49stooge.jpg|
  | caption        = |
  | director       = Edward Bernds
  | writer         = Elwood Ullman | Andre Pola Harold Brauer|
  | cinematography = Vincent J. Farrar|
  | editing        = Henry Batista
  | producer       = Hugh McCollum |
  | distributor    = Columbia Pictures |
  | released       = July 7, 1949 |
  | runtime        = 16 40" |
  | country        = United States
  | language       = English
}}

Fuelin Around is the 116th short subject starring American slapstick comedy team the Three Stooges. The trio made a total of 190 shorts for Columbia Pictures between 1934 and 1959.

==Plot==
The Stooges are carpetlayers working at the home of Professor Sneed (Emil Sitka) and his daughter (Christine McIntyre). Sneed is developing a rocket fuel in secret for the government. Anemian spy Captain Rork (Philip Van Zandt) watches the professor through his front window, with hopes of kidnapping him. Of course, the Anemians capture the Stooges instead, mistaking Larry for the professor. Trouble brews when the Stooges are required to whip up some the fuel, and then write down the formula. It does not take long for the Anemians to capture the real Professor Sneed, along with his daughter, and throw them in jail until the formula is disclosed. Thanks to a shy prison guard (Jock Mahoney) who cannot help but flirt with Sneeds daughter, the group make a quick exit.

==Production notes== remade in Hot Stuff using available stock footage. The film title is a pun on the expression "fooling around." 

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 

 