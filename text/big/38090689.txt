Goddess (2013 film)
{{Infobox film
| name           = Goddess
| image          = Goddess film poster.jpg
| caption        = 
| director       = Mark Lamprell
| producer       = {{Plainlist |
* Richard Keddie
* Andrena Finlay
}}
| writer         = {{Plainlist |
* Mark Lamprell
* Joanna Weinberg
}}
| starring       = {{Plainlist |
* Laura Michelle Kelly
* Ronan Keating
* Magda Szubanski
* Dustin Clare
* Hugo Johnstone-Burt
* Natalie Tran
}}
| music          = {{Plainlist |
* Joanna Weinberg
* Bryony Marks
* Jude Morris   
}}
| cinematography = Damian Wyvill ACS Mark Warner
| studio         = {{Plainlist |
* The Film Company Ealing Metro International
* Wildheart Films
}} Roadshow Films
| released       =     
| runtime        = 104 minutes 
| country        = Australia
| language       = English
| gross          = $1,655,620  
}} romantic comedy film, directed by Mark Lamprell. The film stars singer Ronan Keating, Laura Michelle Kelly and Magda Szubanski.

==Plot==
Elspeth Dickens (Laura Michelle Kelly) dreams of finding her "voice" despite being stuck in an isolated farmhouse with her twin boys (Phoenix and Levi Morrison). A webcam she installs in her kitchen becomes her pathway to fame and fortune, making her a cyber-sensation. Through singing her funny sink-songs into the webcam, Elspith becomes a cyber-sensation.

While her husband James (Ronan Keating) is off saving the worlds whales, Elspeth is offered the chance of a lifetime. But when forced to choose between fame and family, the newly anointed internet goddess almost loses it all. 

==Cast==
 
*Ronan Keating as James Dickens
*Laura Michelle Kelly as Elspeth Dickens
*Magda Szubanski as Cassandra Wolfe
*Phoenix Morrison as Fred (twin)
*Levi Morrison as Zac (twin)
*Dustin Clare as Rory
*Hugo Johnstone-Burt as Ralph
*Corinne Grant as Fizz
*Pia Miranda as Sophie
*Natalie Tran as Helen
*Lucy Durack as Cherry

==Production==
Goddess was filmed, in part, in Sydney, Australia and Tasmania.  The film is based on the original stage play Sinksongs, written and performed by Joanna Weinberg with music for the film written by Joanna Weinberg, Bryony Marks and Jude Morris. 

==Film Festivals==
In early June 2014, Goddess opened the Maui International Film Festival to great reviews 
 

==Box office==
In its opening weekend, Goddess grossed $512,445 in Australia. The film was shown on 207 screens, which gave it a per-screen average of $2,476.
 

==External links==
 
* 
* 
* 

==References==
 

 
 
 
 
 
 