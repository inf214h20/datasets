Aap Ke Deewane
{{Infobox film
| name           = Aap Ke Deewane
| image          = Aapkedeewane.jpg
| image_size     =
| caption        = 
| director       = Surendra Mohan
| producer       = Rakesh Roshan
| writer         = 
| narrator       = 
| starring       = Rishi Kapoor Tina Munim Rakesh Roshan
| music          = Rajesh Roshan
| cinematography = 
| editing        = 
| distributor    = Filmkraft Productions (India) Pvt. Ltd
| released       = 31 December 1979
| runtime        = 
| country        = India
| language       = Hindi
| budget         = 
| preceded_by    = 
| followed_by    = 
}}

Aap Ke Deewane is a 1979 Hindi movie produced unofficially by Rakesh Roshan   and directed by Surendra Mohan. The film stars Rakesh Roshan, Rishi Kapoor, Pran (actor)|Pran, Ashok Kumar, Tina Munim, Ranjeet, Deven Verma, Hrithik Roshan, Keshto Mukherjee and Poonam Dhillon. Jeetendra has a guest appearance. The films music is by the producers brother Rajesh Roshan. The lyrics were penned by the legendary Anand Bakshi. The film did good business due to its freshness and haunting music.

The film is Rakesh Roshans first attempt at production, going forward he produced and directed films. Rishi Kapoor and Jeetendra being off screen friends of his, he had a song put in to ensure the 3 get together on screen too as Jeetendra did not have a role in the film and is present only in the song.

The script required a brief visual flashback showing how the two central characters, played by Rishi Kapoor and Rakesh Roshan, had originally become friends as children.  Rakesh Roshans actual son—future Hindi film superstar actor Hrithik Roshan, but then aged only 6&mdash;appears momentarily in Aap Ke Deewane as the child-version of the character otherwise played by his father, sharing his tricycle with the other child on a beach.

Trivia

Mohammed Rafi sings all six songs in the movie with other male playbacking singer like Kishore Kumar, Lata Mangeshkar and Amit Kumar.

==Cast==

*  Rishi Kapoor: Ram
*  Tina Munim: Sameera
*  Rakesh Roshan: Rahim
*  Shoma Anand: Meena
*  Ashok Kumar: Inshallah Khan
*  Jeetendra: Rocky (Guest Appearance)
*  Pran (actor)|Pran: Thakur Vikramjit Singh
*  Ranjeet: Kundan
*  Deven Verma: Butler
*  Keshto Mukherjee: Lawyer
*  Pinchoo Kapoor: Judge
*  Sudhir Dalvi
*  Gulshan Bawra
*  Yusuf Khan: Yusuf
*  Hrithik Roshan: (uncredited) boy, as young Rahim, sharing his tricycle with the young Ram on a beach during the one-shot flashback of how the two characters originally met, in song "Ram Kare Allah Kare"

==Soundtrack==
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 2
| "Hum To Aap Ke Deewane Hain"
| Mohammed Rafi, Kishore Kumar, Amit Kumar
|-
| 1
| "Mere Dil Mein Jo Hota Hai"
| Mohammed Rafi, Kishore Kumar, Lata Mangeshkar
|-
| 4
| "Tera Jalwa Tauba Hai Tera Jalwa"
| Kishore Kumar, Mohammed Rafi
|-
| 6
| "Tumko Khush Dekh Kar"
| Kishore Kumar, Mohammed Rafi
|-
| 3
| "Ram Kare Allah Kare"
| Mohammed Rafi, Amit Kumar, Benny, Lata Mangeshkar
|-
| 5
| "Apni Khushiyan Tujhko De Doon"
| Mohammed Rafi, Amit Kumar
|}

== External links ==
*  

 
 
 


 