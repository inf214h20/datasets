Pay Your Dues
 
{{Infobox film
| name           = Pay Your Dues
| image          = 
| image size     = 
| caption        = 
| director       = Vincent P. Bryan Hal Roach
| producer       = Hal Roach
| writer         = H. M. Walker
| starring       = Harold Lloyd
| music          = 
| cinematography = Walter Lundin
| editing        = 
| distributor    = 
| released       =  
| runtime        = 12 minutes
| country        = United States  Silent English intertitles
| budget         = 
}}
 short comedy film featuring Harold Lloyd.

==Cast==
* Harold Lloyd - The Boy
* Snub Pollard 
* Bebe Daniels  
* Sammy Brooks
* Lige Conley - (as Lige Cromley)
* Frank Daniels
* William Gillespie
* Charles Inslee
* Mark Jones
* Dee Lampton
* Gus Leonard
* Earl Mohan
* Marie Mosquini
* Fred C. Newmeyer - (as Fred C. Newmeyer)
* H.L. OConnor
* Robert Emmett OConnor Charles Stevenson - (as Charles E. Stevenson)
* Chase Thorne
* Noah Young

==See also==
* Harold Lloyd filmography

==External links==
* 

 
 
 
 
 
 
 
 
 


 