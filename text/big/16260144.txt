Violent Saturday
{{Infobox film
| name           = Violent Saturday
| image          = Violent Saturday poster.jpg
| caption        = Theatrical lobby card
| director       = Richard Fleischer
| producer       = Buddy Adler
| writer         = Sydney Boehm William L. Heath (novel) Richard Egan Lee Marvin Stephen McNally Sylvia Sidney
| music          = Hugo Friedhofer
| cinematography = Charles G. Clarke
| editing        = Louis R. Loeffler
| distributor    = 20th Century Fox
| released       =  
| runtime        = 90 min.
| country        = United States
| language       = English
| budget         = $955,000 
| gross          = $1.25 million (US rental)  
}} crime drama Richard Egan and Stephen McNally. The film, set in a mining town, depicts the planning of a bank robbery as the nexus in the personal lives of several townspeople.

Prominent actors Sylvia Sidney and Ernest Borgnine are in supporting roles. Violent Saturday was filmed in color, on location in Bisbee, Arizona.

==Plot== traveling salesman. He arrives in town, soon to be joined by sadistic benzedrine addict Dill (Lee Marvin) and bookish Chapman (J. Carrol Naish).
 Richard Egan) is manager of the local copper mine, troubled by his philandering wife (Margaret Hayes). He considers an affair with nurse Linda Sherman (Virginia Leith), though he truly loves his wife. His associate, Shelley Martin (Victor Mature), has a happy home life, but is embarrassed that his son believes he is a coward because he did not serve in World War II.

Subplots involves a Voyeurism|peeping-tom bank manager, Harry Reeves (Tommy Noonan), and a larcenous librarian, Elsie Braden (Sylvia Sidney). As the bank robbers carry out their plot, the separate character threads are drawn together. Violence erupts during the robbery.  Fairchilds wife is slain and bank manager Reeves is wounded. Martin is held hostage on a farm with an Amish family. With the help of the father (Ernest Borgnine), he defeats the crooks in a savage gunfight. In the aftermath, Martin becomes a hero to his son, and Linda comforts Fairchild as he grieves for his wife.

==Cast==
*Victor Mature as Shelley Martin
*Richard Egan as Boyd Fairchild
*Stephen McNally as Harper
*Virginia Leith as Linda Sherman
*Tommy Noonan as Harry Reeves
*Lee Marvin as Dill
*Margaret Hayes as Mrs. Fairchild
*J. Carrol Naish as Chapman
*Sylvia Sidney as Elsie Braden
*Ernest Borgnine as Stadt
*Dorothy Patrick as Helen Martin
*Billy Chapin as Steve Martin

==Critical reception==
The New York Times did not approve of the violence of the movie. Critic Bosley Crowther called the movie an "unedifying spectacle," while praising the performance of Lee Marvin as a hood "so icily evil he is funny." Borgnines performance was panned by Crowther as "a joke."

More recent reviewers have been favorable. In a 2008 article, the   thought he would be perfect as Diomed in Troilus and Cressida."
 Witness have been observed.

==References==
 
*Solomon, Aubrey. Twentieth Century Fox: A Corporate and Financial History (The Scarecrow Filmmakers Series). Lanham, Maryland: Scarecrow Press, 1989. ISBN 978-0-8108-4244-1.

== External links ==
*  
*  
*  
*   review at Film Noir of the Week
* New York Times review  
* Village Voice article  

 

 
 
 
 
 
 
 
 
 
 