Murder in Pacot
{{Infobox film
| name = Murder in Pacot
| image = Mip poster1.png
| caption = Poster for world premier at 2014 Toronto International Film Festival
| alt = 
| director = Raoul Peck
| producer =  
| writer =  
| based on =  
| starring =  
| music =  Alexeï Aïgui
| sound =  
| cinematography = Éric Guichard
| editing =  Alexandra Strauss
| studio = 
| distributor = Doc&Film International
| released =  
| runtime = 130 minutes
| country = Haiti
| language = French, Haitian creole
| budget =
| gross = 
}} earthquake of 12 January 2010 which opened at New Yorks Film Society of Lincoln Center in February 2014.  The lead producers of the joint Haitan, French and Norwegian production were Peck and Remi Grellety. 

The story, a traditional stranger-comes-to-town plot line examining how the earthquake upended Haitis strongly divided class system, is loosely inspired by the 1968 mystery Teorema (film)|Teorema by Italian director Pier Paolo Pasolini. In that screenplay, which Pasolini adapted from his own novel, a mysterious stranger, played by Terence Stamp, intrudes an Italian family and seduces every member of the household before leaving as suddenly and mysterious as he came.   
  in Pacot in 2002]]
The shooting location of Pecks film, a three level villa, lies in the already completely rebuild neighbourhood Pacot in Port-au-Prince. The first ideas for the film came when Peck drove daily through that wealthy area while shooting the documentary Assistance Mortelle seeing that the rich were equally affected by the earthquake.     
Peck stated that he wanted to approach the serious consequences of the disaster in another way than the usual images of slums and homeless camps. He was struck by seeing the collapsed homes of the prosperous class, the amazing visual of wealth in ruins.
{{quotation | Raoul Peck remarks in an interview:
:Visually it was incredible, this image of wealth, totally crumbled. That story didnt interest many people, because you could do better images in the slums, in the camps, in downtown Port-au-Prince.    (Visuellement, c’était incroyable, cette image de la richesse en ruines. Cette histoire n’a pas intéressé beaucoup de gens parce qu’il était possible de faire de meilleures images dans les bidonvilles, les camps et le centre-ville de Port-au-Prince.)   }}
 ]]
By mid-May 2014 the film was ready for editing and according to director Peck is expected to finish by September to be announced at the Toronto International Film Festival or the Venice Film Festival  and released end of 2014.    The film made its world premier at the 2014 Toronto International Film Festival   where it is a Masters selection   and was shown on 5 September 2014 at the Isabel Bader Theatre.  
 CUBIX cinema Grand Hyatt hotel. 

The Swiss premiere takes place at the International Film Festival and Forum on Human Rights (FIFDH) in Geneva on the 1 March 2015. The film is part of the Fiction and Human Rights Competition.  In April 2015 the film will be shown at the 34th International Istanbul Film Festival. 

The international sales of the film are handled by Doc&Film.  

==Plot==
An upper middle class affluent couple in Port-au-Prince, Haïti, tries to rebuild their lives after the earthquake of 2010. They live in the ruins of their luxury home which was almost completely destroyed, in the district Pacot. The tension is even greater because their young adopted son is also missing.  Shortly after the earthquake they are visited by a team of foreign experts which tell them to repair the house or it will be razed. In order to earn some money for the repair they move to the previous servants quarters and rent out the only still-intact room to Alex a foreign aid worker of unspecified nationality. The tenant, who came with good intentions to Haiti, meets soon a beautiful and naive 17 year old Haitian girl, Andrémise, from a modest background who lives in the neighbourhood. To the couples surprise he moves in together with this sassy and enterprising young woman, who calls herself Jennifer to attract foreign men. They forge a relationship but soon her maliciousness comes to light and someone gets killed. The once privileged and now helpless and defenseless owners are for the first time faced with the rigid contradictions of Haitian society.    

==Cast==
* Joy Olasunmibo Ogunmakin as wife
* Alex Descas as husband
* Thibault Vinçon as Alex
* Lovely Kermonde Fifi as Andrémise / Jennifer

==Production==
===Funding=== costs considered low budget compared to international standards, although Peck paid the production team, people who worked with him already on his previous films, at a "very high level". The relative low funding also allowed Peck "to work freely without having to answer to anyone". 

===Casting===
In early March the casting started and young Haitian women and men between 18–27 years were invited to apply to participate in trials in order to obtain one of the main roles in the film. Lovely Kermonde Fifi, a young debutant actress and poet,  was the successful contender to play beside French actors Alex Descas and Thibault Vinçon and the German-Nigerian Joy Olasunmibo Ogunmakin, a singer-songwriter who performs under the name Ayọ,  also appearing for the first time in a lead role.      

===Screenplay===
According to Raoul Peck the script was written by three hands.    Haitian novelist and poet Lyonel Trouillot    and French screenwriter Pascal Bonitzer offered to collaborate with Peck to work on the script.  

===Music===
The music for the film was composed by Alexeï Aïgui and the soundtrack was released together with other film music by the composer on a CD (MEURTRE À PACOT (2014) 18. Meurtre à Pacot (suite) (06:49)). 

===Filming=== ongoing outbreak of chikungunya. At times between 60 and 100 technicians, actors and extras were on scene mostly from Haiti but also from the Dominican Republic, Cuba and France. 

==External links==
*  
*   (French with English subtitles)

==References==
 

 
 
 
 
 
 