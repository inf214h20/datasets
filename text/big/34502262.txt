Hell and Back Again
{{Infobox film
| name           = Hell and Back Again
| image          = 
| image size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Danfung Dennis
| producer       = Thomas Brunner
| music          = J. Ralph
| cinematography = 
| editing        = Fiona Otway
| studio         = 
| distributor    = New Video (USA)
| released       =  
                    
| runtime        = 88 min
| country        = USA UK Afghanistan
| language       = English Pashtu
| budget         = 
| gross          = $40,634 
}} Afghanistan conflict with a badly broken leg and post-traumatic stress disorder. On January 24, 2012, the film was announced as one of the five nominees for the Academy Award for Best Documentary Feature. 

==Background==
Director Danfung Dennis worked as a  .

Dennis decided seven months later that Harris to be the person around whom the documentary would revolve. At the Marines homecoming, Harris did not get off the bus, at which point Dennis learned Harris had been wounded.  He made contact with Harris after this and invited Dennis to his home. Dennis spent a total of a year with Harris and his wife. 

==Production==

===Visual style===

In six months, approximately 100 hours of footage was shot. Dennis and the editor Fiona Otway worked closely in the formulation of the visual style. They discussed their views about the war, where it became clear that popular images of war were at odds with Dennis experiences.   

To create an "honest portrayal of war", Dennis combines the two storylines of the mission in Afghanistan and the situation of Nathan Harris in North Carolina in his documentary. Here he uses flashbacks to represent the "disorientation" and "emotional numbness" experienced "leaving a world of life and death" and "coming back to a world that seems mundane and superficial". According to Dennis is there "really just one battle", at home and on the field, rather than two different ones.    In another interview he stated that he worked to combine the "ethics of photojournalism", the role of pure observer, with the "narrative of film" to create an "immersive, visceral experience". 

Danfung Dennis processed many personal experiences as he did not discuss his footage with Nathan Harris. Harris got to watch the film after its completion.  

===Film technique=== body armor when he was not filming. In addition, he focused the camera manually. Simply switching off the camera helped protect it against overheating. For his filming with Harris and his wife, he changed his equipment so it would be as compact as possible and non intrusive.   He explained in an interview that his decision to use the Canon EOS 5D Mark II allowed him to combine the "aesthetics of photography" and the "ethics of journalism" with the "narrative documentary" to create an "impressive, comprehensive experience". 

In Afghanistan, Dennis used a zoom lens with a focal length of 24&nbsp;mm to 70&nbsp;mm with a maximum aperture of 2.8. Dennis founded the lens choice with the "diversity necessary to get wide and tight shots". He used two normal lenses in Yadkinville: a lens with a focal length of 35&nbsp;mm with a maximum aperture of 1.4 and a second with a focal length of 50&nbsp;mm and a maximum aperture of 1.2. Due to the wide aperture he could even film in low light situations. 

===Tone===
There is no music in the classical sense in the film. Dennis used only natural sounds as background music, which he picked up in Afghanistan and in part significantly altered (see musique concrète). A scene in the film, in which a village is secured, is underlaid with actual sounds of warfighting which were slowed down to 2% of their original speed. This results in a "persistent drone". Dennis used the same drone in the background of a conversation between Harris and his physician regarding the dangers of painkillers. He tries   "the line between past and present through sound alone". Dennis stated that Harris flashbacks "often begin with a sound". He was trying to "convey what it feels like to actually have a flashback".   

Dennis and the sound designer J. Ralph worked closely for the film, as did Dennis and editor Fiona Otway. Ralph also wrote the song "Hell And Back", heard during the end credits. The performer of the song is Willie Nelson.

==Release== Bloor Hot Docs Cinema.   On October 12, 2011 the film was released in the UK, France followed on December 21, 2011.   In the United Kingdom it grossed US$ 315.  The cinema releases in the United Kingdom and France were several broadcasts on the Spanish TV station Canal+ in April and September 2012. 
The only screening of the documentary in German-speaking countries took place in Austria in 2012 and 2013 at the frame ut-freestyle-Filmfestival and the Filmfestival Kitzbühel.  

==Reception==

===Reviews===
The film was very positively assessed by critics. Rotten Tomatoes lists a total of 27 reviews, all of which were positive.  The Metascore is 81 out of 100, based on 17 reviews. 

The film critic   in a review for the A.V. Club. Willmore called it a work of cinéma vérité with "almost distracting beauty" and gives the movie the grade B.   Two reviews in the newspapers The Observer and The Guardian agree with the positive tenor. Philip French indicates the film was "painful and deeply moving" and Peter Bradshaw thinks the film doesnt hold back. Bradshaw gave it four out of five stars.  

Chris Knight of the   prologue".  Lauren Wissot, critic for Slant Magazine, criticized the editing technique and labeled it as partly "distracting and obvious", but the documentation is a "universal soldiers story".  Both critic gave the film of two and a half stars out of four possible.

===Veterans===
In conjunction with the release of the documentary, the organization Disabled American Veterans launched an awareness campaign about the disease of post-traumatic stress disorder.  In the same way, the organization Still Serving Veterans used the film to increase the attention for soldiers with post-traumatic stress disorder and organized for this purpose a public screening of Hell and Back Again.  Florida State University showed the film at a special "Veterans Day", which was the start of an initiative for a more veteran-friendly university.  At the same time the university inaugurated a "student veteran film festival". Danfung Dennis, the producer Karol Martesko window, as well as Nathan Harris and Ashley Harris took part in the event.  Even the veteran Association of the University of Iowa organized a screening of the documentary. 

REACT to FILM launched its College Action Network with a screening of Hell and Back Again at American University in Washington, D.C. on September 21, 2011.  Director Danfung Dennis spoke to the audience both at the launch event, and in-person and via Skype at subsequent College Action Network screenings across the country. 

===Accolades===
At the Sundance Film Festival 2011 the film won the jury prize and the camera Prize for best foreign documentary. Also at the Moscow International Film Festival 2011, the film won the prize for best documentary. In addition, it received documentary awards at several smaller film festivals and award ceremonies was awarded. This included IDA Award in the category Jacqueline Donnet Emerging Filmmaker Award, Cinema Eye Honors prize for Outstanding Achievement In Cinematography, the Alfred I. duPont–Columbia University Award 2012 and the Harrell Award for Best Documentary at the Camden International Film Festival 2011. 

In addition to the awards won, the film was nominated at some film festivals and award ceremonies. It was nominated for Independent Spirit Award for Best Documentary Feature, and best documentary at Gotham Independent Film Awards and British Independent Film Awards. At the Cinema Eye it was nominated in four categories, and won in the category of outstanding achievement in cinematography. The additional categories were outstanding achievement in direction, best debut feature and outstanding achievement in production. 

On January 24, 2012, the film was nominated for an Academy Award in the category of best documentary, but lost to the American contribution Undefeated (2011 film)|Undefeated. In response to the nomination, the producer Mike Lerner received a congratulation letter from British Prime Minister David Cameron. 
 Grierson Award 2012 in the category Best Documentary on a Contemporary Theme – International on November 6, 2012.  On 11 July 2013, the documentary was nominated due to its appearance in the program series Independent Lens for a News & Documentary Emmy Award. 

{| class="wikitable"
|-
!  Award !! Date of ceremony !!Category !! Recipients and nominees !! Result
|-
| rowspan="2"| Sundance Film Festival   
| rowspan="2"| January 29, 2011
| World Cinema Grand Jury Prize: Documentary 
| Danfung Dennis
|  
|-
| World Cinema Cinematography Award: Documentary 
| Danfung Dennis
|  
|- Academy Awards   
| 26 February 2012 Best Documentary Feature
| Danfung Dennis
|  
|}

==References==
 

==External links==
* 
* 
*  

 
 
{{Succession box
| title= 
| years=2011
| before=The Red Chapel
| after=The Law in These Parts}}
 

 
 
 
 
 
 
 
 
 
 
 