Casper (film)
 
 
{{Infobox film
| name           = Casper
| image          = Casper poster.jpg
| caption        = Theatrical release poster
| director       = Brad Silberling Colin Wilson
| writer         = Sherri Stoner Deanna Oliver
| based on       =  
| starring       = Christina Ricci Bill Pullman Cathy Moriarty Eric Idle
| music          = James Horner
| cinematography = Dean Cundey Michael Kahn The Harvey Entertainment Company
| distributor    = Universal Pictures
| released       =  
| runtime        = 100 minutes
| country        = United States
| language       = English
| budget         = $55 million   
| gross          = $287,928,194 
}}
 family comedy comedy fantasy film starring Christina Ricci, Bill Pullman, Cathy Moriarty, Eric Idle, and Amy Brenneman, based on the Casper the Friendly Ghost comic books and animated cartoons. The ghosts featured in the film were created through computer-generated imagery.

==Plot== Casper and his obnoxious prankster uncles the Ghostly Trio; Fatso, Stretch and Stinky who immediately frighten them off the property. Carrigan tries multiple methods to rid the house of ghosts, but fails time and time again. Meanwhile, Casper sees a report of a paranormal therapist James Harvey and is immediately smitten by his teenaged daughter Kat. He quickly manipulates the television in order to convince Carrigan to have the Harveys come to the manor to investigate the haunting.

James and Kats relationship is strained, after the death of her mother and his wife Amelia, he devotes all of his time into finding her, believing her to have unfinished business and having not moved on to the afterlife. As a result, Kat has had no chance to make friends as they are constantly moving across the country. Upon arriving in Maine, Caspers plan to befriend Kat and her father backfires when his chaotic uncles come home and try to force the Harveys out. James stands resolute and manages to successfully move in. Kat tries to fit in at school, inadvertently gaining popularity for staying at the popular Whipstaff manor and winning the students over for a Halloween party there instead of the popular Ambers boat house where it would have been less authentic. Meanwhile, the trio has tried to make peace with James and even say they will find Amelia for him, while Kat and Casper become friends. She learns Casper does not remember the details of his life, so she takes him to remember it; eventually coming across an old abandoned toy room. Casper begins to remember details of his life, and when he comes across a sled he remembers that he died of an illness (likely pneumonia) when he stayed out late playing with it, but he chose not to move on, in order to take care of his lonely father. He also remembers a machine his father created called the Lazarus, capable of bringing a ghost back to life. Carrigan and Dibs overhear this and contemplate one of them dying and coming back as a ghost in order to get into the vault supposedly containing the treasure they sought. Carrigan tries to kill Dibs, but accidentally falls from a cliff, dying herself instead.

James and the trio go out to several bars, as James becomes increasingly drunk, the trio decides they could always have fun if they make their trio a "quartet" by killing off James. However, they grow to like him too much and decide they cant kill him. Instead, a drunken James falls to his death in an open manhole. Back at the manor, Carrigan as a ghost stops the Lazarus machine before it can resurrect Casper (having earlier learning there was only one dose of the potion to bring him to life) and steals the treasure. She also throws Dibs, who finally grows a spine and stands up to her, out the window, but Casper and Kat trick her into admitting that with her treasure and the potion to bring her back to life she has no unfinished business, and she is involuntarily ejected to the afterlife. The treasure, revealed by Casper, is a Brooklyn Dodgers baseball signed by his favorite player Duke Snider. James returns home, now a ghost and does not recognize Kat until she shows him who she is. Casper then gives up his chance at a new life in order to bring James back to life. The party starting downstairs, Kat goes down to greet the guests, while the trio scare off Amber and her boyfriend Vic who had arrived to sabotage the party. Amelia appears to Casper and for his sacrifice, she gives him until the stroke of 10 to be human. Casper approaches Kat, who does not recognize him until they start dancing while Amelia tells James that she was so well taken care of during life that she has no "unfinished business", and pleads James not to make finding her become his own. At the chime of 10, just as Casper and Kat kiss, the spell breaks and he becomes a ghost again. He attempts to say "Hi" to the guests, but they scream in terror and flee the manor. Not wanting to let a party go to waste, James leads the trio into starting a Halloween party all their own. Kat, James and Casper dance as the Ghostly Trio play music for them.

==Cast==
* Christina Ricci as Kathleen "Kat" Harvey
* Bill Pullman as Dr. James Harvey
* Malachi Pearson as Casper McFadden (voice)
* Cathy Moriarty as Carrigan Crittenden
* Eric Idle as Paul "Dibs" Plutzker
* Ben Stein as Rugg
*  Joe Nipote as Stretch (voice)
* Joe Alaskey as Stinkie (voice)
* Brad Garrett as Fatso (voice)
* Spencer Vrooman as Andreas
* Chauncey Leopardi as Nicky 
* Wesley Thompson as Mr. Curtis 
* Amy Brenneman as Amelia Harvey
* Devon Sawa as Casper McFadden (human form)
* Garette Ratliff Henson as Vic DePhillippi
* Jessica Wesson as Amber Whitmire
* Don Novello as Father Guido Sarducci
* John Kassir as the Crypt Keeper (voice)
* Dan Aykroyd as the mustachioed Raymond Stantz (uncredited)
Cameos as themselves

* Rodney Dangerfield
* Fred Rogers (archive footage)
* Clint Eastwood Terry Murphy
* Mel Gibson (uncredited)
* Steven Spielberg (cameo cut)
* Jess Harnell as Arnold (voice)

==Production==
In the mirror scene, Dr. Harvey was also supposed to transform into Steven Spielberg. According to director Brad Silberling, the cameo was filmed, but was cut for pacing reasons. Spielberg was relieved, feeling that hes not much of an actor himself and was quite nervous in front of the camera.  

==Soundtrack==
The soundtrack was composed by award-winning composer James Horner, who had worked on a number of previous movies for Amblin Entertainment, including An American Tail.
{{Infobox album
| Name       = Casper
| Type       = soundtrack
| Artist     = James Horner
| Cover      = 
| Alt        = 
| Released   = April 29, 1995
| Recorded   = 1994 - 1995
| Genre      = Soundtrack
| Length     = 
| Label      = MCA Records
| Producer   = 
}}

{{Album ratings rev1 = Allmusic rev1score =  
|accessdate=2012-11-24
}}
# "No Sign of Ghosts"
# "Carrigan and Dibbs"
# "Strangers in the House"
# "First Haunting/The Swordfight"
# "March of the Exorcists"
# "Lighthouse—Casper & Kat"
# "Casper Makes Breakfast"
# "Fond Memories"
# "Dying to Be a Ghost"
# "Caspers Lullaby"
# "Descent to Lazarus"
# "One Last Wish" Jordan Hill
# "Casper the Friendly Ghost" – Little Richard
# "The Uncles Swing/End Credits"

==Reception==

===Box office===
Overall the film was a huge success at the box office, opening at #1 over the Memorial Day weekend, grossing $16,840,385 over its first three days from 2,714 theaters, averaging $6,205 per theater. Over four days it grossed $22,091,975, averaging $8,140 per theater. It stayed at #1 in its second weekend, grossing another $13,409,610, and boosting its 10-day cume to $38,921,225. It played solidly all through the summer, ending up with a final gross of $100,328,194 domestically, and achieved even greater success internationally, grossing an additional $187,600,000, for a total worldwide gross of $287,928,194, against a $55 million budget, making it a massive commercial success. 

===Critical===
Casper received generally mixed reviews from film critics. At Rotten Tomatoes based on 36 reviews the film has a "rotten" rating of 44%. Time Out London described it as "an intimate and likeable film".  Roger Ebert gave the film three out of four stars, calling it a "technical achievement, its impressive, and entertaining. And there is even a little winsome philosophy."  The CGI effects, which were cutting edge at the time, and the performances of Bill Pullman and Christina Ricci were praised, especially considering that, in the scenes where the Harveys interact with the ghosts, Pullman and Ricci were actually acting either with nothing or with stand-in maquettes used as animators references.

Cathy Moriartys performance was criticized, with Variety (magazine)|Variety saying she does "a poor womans Cruella de Vil".  Many reviewers also felt that Eric Idle, being a venerable comedian, was underused in the role of Moriartys obsequious henchman.

== Legacy ==

===Sequel===
In the mid-1990s, Simon Wells co-wrote a screenplay for Casper 2, which he was set to direct. Amblin Entertainment cancelled the sequel because they did not believe there would be enough interest from moviegoers. Wells also credited the uncertainty of actress Christina Ricci returning and Foxs ill-received direct-to-video Casper films as contributing to the cancellation of Casper 2. 

===Video games=== games based on or tied-in with Casper released on the major consoles of the time, such as the 3DO Interactive Multiplayer|3DO, Super Nintendo, Sega Saturn, PlayStation (console)|PlayStation, Game Boy Color and original Game Boy.

A LCD Handheld based for same name was released for TIGER Electronics on 1995.

==See also==
* List of ghost films

==References==
 

==External links==
 
*  
*  
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 