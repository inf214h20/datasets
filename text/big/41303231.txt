Apur Panchali
{{Infobox film
| name = Apur Panchali
| image = Apur Panchali.jpg
| caption =
| director = Kaushik Ganguly
| producer = Shree Venkatesh Films
| writer = 
| starring = Parambrata Chatterjee, Parno Mitra
| music = Indradeep Dasgupta
| cinematography = Shirsha Ray
| editing = Bodhaditya Banerjee
| distributor =
| released =  
| runtime = 
| country = India
| language = Bengali
| budget =
}}
Apur Panchali is a 2013 Bengali film directed by Kaushik Ganguly and produced by Shree Venkatesh Films. It is based on the life of Subir Banerjee, the actor who played Apu in Pather Panchali (1955), the first film of Satyajit Rays Apu trilogy.     s child artiste|publisher=NDTV|date=13 December 2012|accessdate=7 December 2013}}  Director Kaushik Ganguly won the award of best director for Apur Panchali in the 44th International Film Festival of India (IFFI) in November 2013.  The director mentioned in an interview that he found similarities between certain parts of the life of Subir Banerjee and the iconic character Apu.  The film uses several minutes of footage from Pather Panchali in its narrative.

==Plot==
The film follows the journey of Subir Banerjee the child actor who portrayed "apu" of Satyajit Rays masterpiece Pather Panchali. The events take place in two time periods. while the present day events are shot in color the past events are in black and white. Comparison is drawn between Subirs life and the Apu trilogy throughout the film.
The film opens with a film school student Arka(Gaurav Chakrabarty) who comes to deliver a letter, an invite to attend a prestigious award ceremony in Germany to an Older Subir banerjee (Ardhendu Bannerjee). Now Retired,lost in the crowd Subir initially doesnt want to receive the letter but he eventually agrees after Arka convinces him. Subir recounts his days working for pather panchali to Arka. They also visit an old broken house where the film was shot. Arka collects a brick from the house as souvenir.  Later as they prepare for their journey to Germany Subir narrates his life story to Arka.

After Subirs brother went away to work in Dhanbad Subir now in his 20s had to give up his football career to take care of his ill father and help his mother in her household chores. On advice of his friend Subir gets married to Ashima(parno Mittra) daughter of a wealthy villager in Burdwan after his father passes away. Two years into their marriage Ashima gets pregnant.

In Present day Subir and Arka open up to each other. Arka talks about his late father whom he deeply admired. Subir reveals that due to Jaundice Ashima gave premature birth and the baby died soon after. Ashima went into depression and also died. Subir is heart-broken and isolates himself.

As Arka and Subir board the plane to Germany Arko thanks Subir for this was his first business class trip to which Subir says he ought to thank "Apu" since its his first time on a plane.

Subir on request of his mother  comes to return all of Ashimas belonging in her native house. Her grandmother asks him to bathe in the local pond and stay for lunch. Subir goes to the pond and as he prepares for bathe a young boy asks him if his name is apu to which subir weepingly nods.

As the credit rolls it is shown that Subir Banerjee was felicitated in International preis fur film-und medianmusik, Germany.

==Cast==
*Parambrata Chatterjee as young Subir Banerjee
*Parno Mittra
*Gaurav Chakrabarty
*Ardhendu Bannerjee as older Subir Banerjee

==Soundtrack==
The Music and Background Score of the film is composed by music composer Indradeep Dasgupta. The background score of the film has been appreciated widely.
On 4 April 2014 - a song from the soundtrack titled Apur Paayer Chhaap sung by Arijit Singh was released.

==Release==
The film was screened at the 44th International Film Festival of India (IFFI) in November 2013 and was received warmly. Kaushik Ganguly received the Best Director Award for the film there.

The film was released on 25 April 2014, and gained wide critical acclaim.

==References==
 

 
 
 
 


 