Robin Hood (1912 film)
{{Infobox film
| name           = Robin Hood
| image          = Robin_Hood-1912_lowres-scene.jpg
| image_size     =
| caption        = Scene from the film.
| director       = Étienne Arnaud Herbert Blaché
| producer       =
| writer         = Eustace Hale Ball
| starring       = Robert Frazer Barbara Tennant
| cinematography =
| editing        =
| distributor    =
| released       =  
| runtime        = 30 minutes
| country        = United States English
| budget         =
}}
Robin Hood is a 1912 film made by Eclair Studios when it and many other early film studios in Americas first motion picture industry were based in Fort Lee, New Jersey at the beginning of the 20th century.    The movies costumes feature enormous versions of the familiar hats of Robin and his merry men, and uses the unusual effect of momentarily superimposing the image of a different animal over each character to emphasize their good or evil qualities.  The film was directed by Étienne Arnaud and Herbert Blaché, and written by Eustace Hale Ball. A restored copy of the 30-minute film exists and was exhibited in 2006 at the Museum of Modern Art in    The restoration was completed by the Fort Lee Film Commisison from nitrate elements and a safety print donated to the commission from film collector Al Detlaff of Wisconsin.  Sirk Productions of NYC worked with the Fort Lee Film Commission to restore  this, the only existing print of the earliest surviving American made film of Robin Hood. The Fort Lee Film Commission in 2015 will have the film scored by the Mont Alto Motion Picture Orchestra, and the film will be placed on a DVD  of Fort Lee silent films by Milestone Video that will be available to the public in late 2015 or early 2016... 

==Cast==
 
* Robert Frazer ... Robin Hood
* Barbara Tennant ... Maid Marian
* Alec B. Francis ... Sheriff of Nottingham
* Julia Stuart ... Sheriffs Housekeeper
* Mathilde Baring ... Maid at Merwyns
* Isabel Lamon ... Fennel
* Muriel Ostriche ... Christabel
* M. E. Hannefy ... Friar Tuck
* Guy Oliver ... Guy Oliver
* George Larkin ... Alan-a-Dale
* Charles Hundt ... Will Scarlet Much
* Richard the Lion-Hearted
* Lamar Johnstone ... Guy of Gisbourne
* John G. Adolfi ... Thomas Merwin

==References==
 

==External links==
 
*  at the Internet Movie Database
 

 
 
 
 
 
 
 
 
 


 