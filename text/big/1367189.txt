Needing You...
 
 
{{Infobox film
| name           = Needing You...
| image          = Needing You....jpg
| image size     =
| caption        =
| director       = Johnnie To Wai Ka-Fai
| producer      = Johnnie To Wai Ka-Fai
| writer         = Wai Ka-Fai Yau Nai-Hoi Original Story: Cindy Tang
| narrator       =
| starring       = Andy Lau Sammi Cheng
| music          = Cacine Wong
| cinematography = Cheng Siu-Keung
| editing        = Law Wing-Cheong One Hundred Years of Film Milkyway Image
| distributor    = Hong Kong: China Star Entertainment Group
| released       =  
| runtime        = 101 min.
| country        = Hong Kong
| language       = Cantonese
| budget         =
| gross          = HK$35,214,661
| preceded by    =
| followed by    =
}} 2000 Cinema Hong Kong romantic comedy film, produced and directed by Johnnie To and Wai Ka-Fai, starring Andy Lau and Sammi Cheng.

Needing You... is the first film produced by One Hundred Years of Film Co. Ltd., a subsidiary of China Star Entertainment Group.

==Plot==
 pathological cleaning under emotional stress.  Her workplace is full of gossip-mongers perpetually looking to shirk work.
 office politicking at the top, he comes to appreciate Kinkis work ethic and good-naturedness as something of a rarity in the company.

After Kinki helped Andy defuse a sticky work situation, Andy offers to help his subordinate in her private love life.  He plots with Kinki to get back at Dan, her philandering boyfriend.  In the process, the two realize they may harbor romantic feelings for each other.  
 Raymond Wong). Although Kinki does not fancy Roger, she realize that Andy is showing fits of unease and jealousy that is pleasing her.

==Reception==
===Box office=== Summer Holiday earned HKD 21 million, which was ranked 2nd best-selling local film after Needing You... in Hong Kong.

==Cast==
* Andy Lau - Andy Cheung
* Sammi Cheng - Kinki Kwok
* Fiona Leung - Fiona Yu Raymond Wong - Roger Young Hui Shiu-Hung - Ronald
* Florence Kwok
* Lam Suet - Martin
* Kenny Kung
* Sylvia Lai
* Gabriel Harrison
* Andy Tse
* May Fu
* Englie Kwok
* Vanessa Chu
* Terence Lam

==External links==
*  

 
 
 

 
 
 
 
 
 
 
 
 