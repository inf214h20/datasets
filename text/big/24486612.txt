Naughty, Naughty (1918 film)
 
__NOTOC__
{{Infobox film
| name           = Naughty, Naughty
| image          =
| caption        =
| director       = Jerome Storm
| producer       = Thomas H. Ince
| writer         = C. Gardner Sullivan
| starring       = Enid Bennett
| cinematography = John Stumar
| editing        =
| distributor    =
| released       =  
| runtime        = 5 reels (approximately 50 minutes)
| country        = United States Silent English English intertitles
| budget         =
}}
 silent comedy starring Enid Bennett and written by C. Gardner Sullivan. The films protagonist is Roberta Miller, an innocent girl who leaves her rural hometown for the big city.  She returns after four months, but her sophistication draws suspicion in her hometown.

==Cast==
* Enid Bennett - Roberta Miller
* Earle Rodney - Matthew Sampson
* Marjorie Bennett - Prudence Sampson
* Gloria Hope - Judith Holmes Andrew Arbuckle - Adam Miller

==External links==
* 

 
 
 
 
 
 


 