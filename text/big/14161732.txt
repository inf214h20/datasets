Padri (film)
 
{{Infobox film
| name           = Padri 
| image          =
| director       = N. Rajesh Fernandis
| producer       = Raymond Quadros
| writer         = Prince Jacob
| starring       = Newton DSouza   Arona
| music          = Wilfy Rebimbus
| cinematography = Yelukote Chandru
| editing        = Bala Nayak
| studio         = 
| distributor    = Prince Jacob Productions
| released       =  
| runtime        = 
| country        = India
| language       = Konkani
| budget         = Indian rupee|Rs. 60 lakhs
| gross          = 
}} 2005 Konkani language film directed by Rajesh Fernandes and produced by Raymond Quadros. It stars Newton DSouza and Arona Fernandes in the lead roles as protagonists and Kiran Kerkar, Thapan Acharya, and Annie Quadros in the supporting roles. Sandeep Malani plays a guest role in the film. The soundtrack and background score were composed by Wilfy Rebimbus. The film is based on a Goan play with the same title Padri, which was directed by Prince Jacob.

==Plot==

Savio, Julies and Regan are three brothers growing up under the tutelage of their sister in law, Anushka. Savio intends to become a Padri (Priest), Julius ekes out a living in a business company, and Regan is an engineer. Anushka grew up in an aristocratic family and rules the house. Her uncle, Roldhawa, on a short visit, schemes and destroys the familys peace. Anushka is an unwitting pawn in her uncles intrigues.

==Production==

The film was begun when Rajesh Fernandes along with Raymond Quadros, during one of their visits to Goa, watched the play Padri by Prince Jacob. Soon, the idea of turning the play into a movie was born. The movie is unique in that all the actors and actresses acted on camera for the first time, but they are all professional stage artists. The whole movie was filmed in 2005 
{{cite web | title = Padri Screened in Bahrain | url = http://www.daijiworld.com/news/news_disp.asp?n_id=17534&n_tit=Bahrain%3A+Padri+Movie+to+be+Screened+on+Jan+6+-+A+Reminder | work = Daiji World | accessdate = 2011-12-09 
 }}  without a single indoor shot in 26 days. In all, "it is a package to be viewed by the entire family with model clippings and modern anecdotes", according to director Fernandes. The censor board has given ‘U’ certificate without even a single cut, he added. He said the language used in the movie is a mixture of Mangalorean Konkani and Goan Konkani.

==Cast==
*Newton DSouza
*Aruna Fernandes
*Kiran Kerkar
*Thapan Acharya
*Annie Quadros
*Prince Jacob
*Humbert
*Frank Coelho
*Diana
*Jacinth
*Justin
*Roseferns
*Sandeep Malani (cameo appearance)

==Crew==
*Direction : N. Rajesh Fernandes
*Producer : Raymond Quadros
*Associate producer : Desmond Quadros
*Story, Screenplay and Dialogues : Prince Jacob
*Music and Lyrics : Wilfy Rebimbus
*Editor : Bala Nayak
*Cinematography : Yelukote Chandru
*Stunts : Tiger Madhu
*Choreography : Prasad

==Soundtrack==

Lyrics and tunes were written by Wilfy Rebimbus.

==References==
 

==External links==
*  
* 
* 

 
 
 
 
 
 