It Happened in Brooklyn
{{Infobox Film
| name           =
| image_size     =
| image	         = It Happened in Brooklyn FilmPoster.jpeg
| caption        =
| director       = Richard Whorf Jack Cummings
| writer         = Isobel Lennart J. P. McGowan
| starring       = Frank Sinatra Peter Lawford Kathryn Grayson Jimmy Durante
| music          = Johnny Green
| cinematography = Robert Planck
| editing        = Blanche Sewell
| distributor    = Metro-Goldwyn-Mayer
| released       = April 7, 1947
| runtime        = 104 minutes
| country        = United States
| language       = English
| budget         =$1,819,000  . 
| gross          = $2,664,000 
}}
 MGM musical musical romantic comedy film directed by Richard Whorf and starring Frank Sinatra, Kathryn Grayson, Peter Lawford, and Jimmy Durante and featuring Gloria Grahame and Marcy McGuire. It Happened in Brooklyn was Sinatras third film for Metro-Goldwyn-Mayer, who had purchased his contract from RKO because Louis B. Mayer was a huge Sinatra fan.  
 Time After Time", and "Its the Same Old Dream".

==Plot==
A post-World War II feel-good movie, It Happened in Brooklyn begins in England at the end of the war. Danny Miller (Sinatra) is with a group of GIs awaiting transportation home to the US. On his last night there, he meets Jamie Shellgrove (Lawford), who is a very shy young man whose father feels should be taken under someones wing. After observing Miller come to his sons aid at the piano, he asks Danny to speak with his son, to give him "some words of encouragement". In order to look good in front of the nurse (Gloria Grahame), he agrees, even going so far as to saying what would really fix Jamie up would be for him to come to   member Sinatra. One highlight of the film is seeing and hearing Sinatra and Grayson singing "Là ci darem la mano" from Wolfgang Amadeus Mozart|Mozarts 1787 opera Don Giovanni.

==Filming==
The original director was supposed to be George Sidney,  but he was replaced by Richard Whorf, who is probably best known for his television directing, particularly The Beverly Hillbillies, Gunsmoke and My Three Sons. Filming was interrupted for approximately ten days when Durante had to go and finish filming on This Time for Keeps. The piano solos in the film were performed by the pianist and composer André Previn.
==Box office==
The film earned $1,877,000 in the US and Canada and $787,000 elsewhere, resulting a loss of $138,000. 

==Critical reception==
It Happened in Brooklyn was generally well received, Variety (magazine)|Variety noting that: "Much of the lure will result from Frank Sinatras presence in the cast. Guys acquired the Bing Crosby knack of nonchalance, throwing away his gag lines with fine aplomb. He kids himself in a couple of hilarious sequences and does a takeoff on Jimmy Durante, with Durante aiding him, thats sockeroo."

==References==
 

==External links==
*  
*  
*  , Variety (magazine)|Variety

 
 
 
 
 
 
 
 
 
 
 
 
 