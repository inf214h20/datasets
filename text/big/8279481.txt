Manhunt of Mystery Island
{{Infobox film
| name          = Manhunt of Mystery Island
| image         = Manhuntmysteryisland.jpg
| caption       =
| director      = Spencer Gordon Bennet Yakima Canutt Wallace Grissell
| producer      = Ronald Davidson
| writer        = Albert DeMond Basil Dickey Jesse Duffy Alan James Grant Nelson Joseph Poland Richard Bailey Jack Ingram Harry Strang cinematography = Bud Thackery
| distributor   = Republic Pictures
| released      = {{Film date|1945|03|17|U.S. serial|ref1= {{cite book
 | last = Mathis
 | first = Jack
 | title = Valley of the Cliffhangers Supplement
 | origyear = 1995
 | publisher = Jack Mathis Advertising
 | isbn = 0-9632878-1-8
 | pages = 3, 10, 82–83
 | chapter =
 }} |1956|01|02|re-release|ref2= |1966| | |U.S. TV|ref3= }}
| runtime       = 15 chapters (219 minutes (serial)  100 minutes (TV) 
| country       = United States English
| budget        = $167,912 (negative cost: $182,388) 
}}
 Republic Movie serial.  It was the thirty-sixth serial produced by Republic (of a total of sixty-six) and the first released in 1945.

It is the penultimate 15-chapter serial to be released by the studio. The year 1945, the end of the Golden Age of Serials,  was the last in which Republic released any 15-chapter serials, the remainder being either 12- or 13-chapters in length.

In 1966 footage from the serial was edited together into the 100-minute film Captain Mephisto and the Transformation Machine.

Three of the serials cliffhanger gags / set pieces are copied in Indiana Jones and the Temple of Doom.

==Plot==
A breakthrough scientific device will revolutionize the worlds energy usage if the kidnapped creator can be found. To rescue her father, Claire Forrest enlists the help of private detective, Lance Reardon. Clues lead them to a remote Pacific isle known only as Mystery Island, where the two confront sinister and astonishing forces. The descendants of a long-dead pirate, Captain Mephisto are holding the scientist for their own gain. Worst of all, one of the heirs possesses a Transformation Machine with the impossible ability of changing him into the molecular duplicate of his ancestor, Mephisto...

==Cast== Richard Bailey as Lance Reardon, a private detective
*Linda Stirling as Claire Forrest, daughter of Professor Forrest
*Roy Barcroft as Higgins/ Captain Mephisto 
*Kenne Duncan as Sidney Brand
*Forrest Taylor as Professor William Forrest, inventor of the Radiatomic Power Transmitter
*Forbes Murray as Professor Harry Hargraves Jack Ingram as Edward Armstrong
*Harry Strang as Frederick "Fred" Braley

==Production==
Manhunt of Mystery Island was budgeted at $167,912 although the final negative cost was $182,388 (a $14,476, or 8.6%, overspend). 

It was filmed between 16 October and 18 November 1944 under the working titles Mystery Island and Manhunt.   The serials production number was 1496. 

==Release==
===Theatrical===
Manhunt of Mystery Islands official release date is 17 March 1945, although this is actually the date the seventh chapter was made available to film exchanges. 

The serial was re-released on 2 January 1956 between the similar re-releases of Dick Tracys G-Men and Adventures of Frank and Jesse James.  The last original Republic serial release was King of the Carnival in 1955. 

===Television===
Manhunt of Mystery Island was one of twenty-six Republic serials re-released as a film on television in 1966.  The title of the film was changed to Captain Mephisto and the Transformation Machine.  This version was cut down to 100-minutes in length. 

==Chapter titles==
# Secret Weapon (24min 38s)
# Satans Web (14min 27s)
# The Murder Machine (14min 26s)
# The Lethal Chamber (14min 27s)
# Mephistos Mantrap (14min 26s)
# Ocean Tomb (14min 27s)
# The Death Trap (14min 27s)
# Bombs Away (14min 26s)
# The Fatal Flood (13min 20s)
# The Sable Shroud (13min 20s) - a clipshow|re-cap chapter
# Satans Shadow (13min 20s)
# Cauldron of Cremation (13min 20s)
# Bridge to Eternity (13min 20s)
# Power Dive to Doom (13min 20s)
# Fatal Transformation (13min 20s)
 Source:   {{cite book
 | last = Cline
 | first = William C.
 | title = In the Nick of Time
 | origyear = 1984
 | publisher = McFarland & Company, Inc.
 | isbn = 0-7864-0471-X
 | pages = 239–240
 | chapter = Filmography
 }} 

==See also==
* List of film serials by year
* List of film serials by studio

==References==
 

==External links==
*  
*  
*   by David P. Hayes
*  

 
{{Succession box Republic Serial Serial
| before=Zorros Black Whip (1944 in film|1944)
| years=Manhunt of Mystery Island (1945 in film|1945)
| after=Federal Operator 99 (1945 in film|1945)}}
 

 

 

 
 
 
 
 
 
 
 
 