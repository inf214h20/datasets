The Moment of Truth (film)
 
{{Infobox film
| name           = The Moment of Truth
| image          = The Moment of Truth (film).jpg
| caption        = Film poster
| director       = Francesco Rosi
| producer       = 
| writer         = Pedro Beltrán Ricardo Muñoz Suay Pere Portabella Francesco Rosi
| starring       = Miguel Mateo Miguelín
| music          = Piero Piccioni
| cinematography = Pasqualino De Santis Gianni Di Venanzo Aiace Parolin
| editing        = Mario Serandrei
| distributor    = 
| released       =  
| runtime        = 110 minutes
| country        = Italy
| language       = Italian
| budget         = 
}}

The Moment of Truth ( ) is a 1965 Italian drama film directed by Francesco Rosi.    It was entered into the 1965 Cannes Film Festival.   

==Plot==
Miguel leaves the countryside because he doesnt want to become a poor farmer like his father. In the big city he tries everything to make it but accomplishes nothing until he becomes a bullfighter.

==Cast==
* Miguel Mateo Miguelín – Miguel Romero Miguelín
* José Gómez Sevillano
* Pedro Basauri Pedrucho – himself
* Linda Christian – Linda, American woman
* Curro Carmona
* Luque Gago
* Salvador Mateo
* Gregorio Sánchez

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 


 
 
 