Le Vilain
{{Infobox film name           = Le Vilain director       = Albert Dupontel writer         = Albert Dupontel Simon Moutairou Diane Clavier  starring       = Albert Dupontel Catherine Frot released       =   runtime        = 86 minutes country        = France language       = French
}} French movie written and directed by and starring Albert Dupontel.

==Plot==
Sydney Thomas, a gangster nicknamed Le Vilain, decides to hide from the police by going to his mothers. She discovers that he hid from her his "rogue" nature since his childhood and promises to God to make him repent, but Sydney would prefer to kill his mother. 

==Cast==
* Albert Dupontel : Sydney Thomas, "le Vilain"
* Catherine Frot : Maniette Thomas, Sydneys mother
* Bouli Lanners : Korazy
* Nicolas Marié : Doc William
* Bernard Farcy : inspector Elliot
* Christine Murillo : Carmen Somoza, the Spanish teacher
* Philippe Duquesne : the redhead painter
* Xavier Robic : Korazys secretary
* Husky Kihal : the other painter
* Jacqueline Herve : Huguette

==References==
 

==External links==
*  

 
 
 
 


 