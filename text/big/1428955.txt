My Beautiful Laundrette
 
 
{{Infobox film
| name           = My Beautiful Laundrette
| image          = My Beautiful Laundrette Poster.jpg
| image_size     = 215px
| alt            =
| caption        = Theatrical release poster
| director       = Stephen Frears
| producer       = Tim Bevan Sarah Radclyffe
| screenplay =
| story =
| based on =
| narrator =
| writer         = Hanif Kureishi
| starring       = Gordon Warnecke Daniel Day-Lewis Saeed Jaffrey Roshan Seth
| music          = "Ludus Tonalis"  , produced Stanley Myers and Hans Zimmer
| cinematography = Oliver Stapleton
| editing        = Mick Audsley
| studio         = Working Title Films Channel Four Films Orion Classics
| released       =  
| runtime        = 97 minutes   
| country        = United Kingdom
| language       = English Urdu
| budget         = £650,000 
| gross          = $2,451,545 
}}
My Beautiful Laundrette is a 1985 British Comedy-drama|comedy-drama film directed by Stephen Frears from a screenplay by Hanif Kureishi. The film was also one of the first films released by Working Title Films.

The story is set in London during the contemporary Thatcher era, as reflected in the complex—and often comical—relationships between members of the Asian and White communities. The story focuses on Omar, a young Pakistani man living in London, and his reunion and eventual romance with his old friend, a street punk named Johnny. The two become the caretakers and business managers of a launderette originally owned by Omars uncle Nasser. The plot addresses several polemical issues of the time, including homosexuality and racism, depicted within the social and economic climate of Thatcherism.

==Plot== London Pakistani community. Omars father asks his uncle to give Omar a job and, after working for a brief time as a car washer in one of his uncles garages, he is assigned the task of managing a run-down laundrette and turning it into a profitable business.

At Nassers, Omar meets a few other members of the Pakistani community: Tania, Nassers daughter and possibly a future bride; and Salim, who trafficks drugs and hires him to deliver them from the airport. While driving Salim and his wife home that night, the three of them get attacked by a group of right-wing extremists shouting racist slogans. Among them but not taking part in the attack is Johnny, an old friend of Omars. Omar tries to reestablish their past friendship, offering Johnny a job and the opportunity to adopt a better life by working to fix up the laundrette with him. Johnny decides to help with the laundrette and they resume a romantic relationship that (it is implied) had been interrupted after school. Running out of money, Omar and Johnny sell one of Salims drug deliveries to make cash for the laundrettes substantial renovation.

Omar confronts Johnny on his fascist past on the opening day of the laundrette. Johnny, feeling guilty, says that there are no words to apologize for the past, though he will use his actions to prove to Omar that he is with him from then on. Nasser visits the store with his mistress, Rachel. They dance together in the laundrette while Johnny and Omar secretly make love in the back room. Omar and Johnny are almost caught by Nasser, but Omar claims they were sleeping. The laundrette is finally opened to the public and Tania confronts Rachel about having an affair with her father, Nasser. Nasser and Rachel leave the laundrette and fight in the road; Rachel storms off because she feels humiliated by Tanias words. Omar, while drunk, proposes to Tania. She agrees on the condition that Omar gathers money. Johnny is told about Omars potential engagement and goes out drinking with his old friends, but Omar soon finds him drunk in his apartment and tells him to get back to work. Omars father stops by late in the night and appeals to Johnny to persuade Omar to go to college because he is unhappy with his son running a laundrette.

With the financial help of Salim, Omar decides to take over two laundrettes owned by a friend of Nasser. Salim drives Johnny and Omar to view one of the properties, and he expresses his dislike of the British non-working punks (Johnnys friends). Salim attempts to run over the group of punks who had previously attacked Omars car and injures one of them. Rachel falls ill with a skin rash apparently caused by a ritual curse from Nassers wife, and she decides it is best for both her and Nasser if they part ways. The group of working class punks decides to wait for Salim around the laundrette. While the punks circle the laundrette, Tania drops by and tells Johnny she is leaving, asking him if he will come with her. He refuses Tania by revealing his dedicated romantic relationship with Omar and she departs the laundrette wordlessly. When Salim enters the laundrette, the punks trash his car. Upon noticing their destruction, he runs out of the laundrette and is then ambushed. He is beaten and bloodied while Johnny watches from the window until he decides to interrupt and defend him, despite their mutual dislike. The punks turn their attention to attacking Johnny instead for supporting the Pakistani community, and as he refuses to fight back, they beat him savagely until Omar returns and intervenes, protecting Johnny as the punks trash the laundrette and flee the scene.

The film cuts to Nasser visiting Omars father, and their discussion about Omars future. Nasser sees Tania at a train platform while she is running away, and he shouts to her but she disappears. Omar proceeds to clean up Johnnys wounds, but Johnny is still agitated from the fight and becomes frustrated with his playful affection and lack of seriousness. He lashes out at Omar, insisting he needs to go and telling him to stop touching him. Johnny walks to the window and stares outside in silence as Omar comes up behind him and kisses the back of his neck. The film ends with them both topless and playfully splashing each other with water from a sink, implying that they are continuing their relationship together.

==Cast==
* Gordon Warnecke as Omar Ali
* Daniel Day-Lewis as Johnny Burfoot
* Saeed Jaffrey as Nasser Ali
* Roshan Seth as Hussein Ali
* Derrick Branche as Salim N. Ali
* Rita Wolf as Tania N. Ali
* Souad Faress as Cherry N. Ali Richard Graham as Genghis
* Shirley Anne Field as Rachel
* Stephen Marcus as Moose

==Production==
My Beautiful Laundrette was Frears fifth feature film. Originally shot for television, it was later released in cinemas and eventually became his first international success.

The film marked the first time Oliver Stapleton was in charge of cinematography in one of Frears projects. He would later become one of the directors most consistent collaborators.

==Reception==
My Beautiful Laundrette received positive reviews, currently holding a 100% "fresh" rating on Rotten Tomatoes. 

===Accolades=== Best Original Screenplay, by Hanif Kureishi. It lost to Woody Allens Hannah and Her Sisters. Kureishi was also nominated for a 1986 BAFTA award. The screenplay received an award from the American National Society of Film Critics.

==Soundtrack== a work Les Patineurs, by French composer Emile Waldteufel, and excerpts from Giacomo Puccini|Puccinis Madama Butterfly.

==See also==
* London in film

==References==
 

==Further reading==
*  

==External links==
*  
*  
*  
*  
* Chicago Sun-Times Review:  
*   – a British Library sound recording

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 