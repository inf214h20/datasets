Kudrat
 
{{Infobox Film
| name           = Kudrat
| image          = Kudrat2.jpg
| image_size     =
| caption        = DVD Cover Chetan Anand
| producer       = B.S. Khanna Chetan Anand
| narrator       = 
| starring       = Raaj Kumar Rajesh Khanna Hema Malini Vinod Khanna Priya Rajvansh Aruna Irani Deven Verma
| music          = Rahul Dev Burman
| cinematography = Jal Mistry
| editing        = Keshav Naidu
| studio         = Shimla
| distributor    = Trishakti Productions
| released       = 3 April 1981
| runtime        = 165&nbsp;mins
| country        = India
| language       = Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
 Chetan Anand. The film stars Rajesh Khanna and Hema Malini in the main lead roles supported by Raaj Kumar, Priya Rajvansh and Vinod Khanna. This is second movie of Rajesh Khanna-Hema Malini pair with reincarnation theme after Mehbooba. Kudrat in the year 1981 grossed Rs. 4.00 crores at the box office.  Rajesh Khanna received the 1982 All-India Critics Association (AICA) Best Actor Award for Kudrat. Even though it was declared a flop at box office, it won the Filmfare Award for Best Story.

==Plot==

Chandramukhi (Hema Malini), accompanied by her parents, visits the hill resort of Shimla for the first time in her life. She senses some familiarity with the place and gets strange feelings for which she does not know the reason. Chandramukhi and her family meet Dr. Naresh Gupta (Vinod Khanna). Naresh is attracted towards Chandramukhi and their families talk about getting them married to each other. Mohan Kapur (Rajesh Khanna), an upcoming lawyer, comes to Shimla to meet his patron and godfather, Janak Singh (Raaj Kumar). Mohan owes his education and career to Janak Singhs generosity. Janak Singh wants his daughter Karuna, also a lawyer, to marry Mohan. Mohan agrees out of gratitude to his patron and gets engaged to Karuna.

Once, Chandramukhi dashes into Mohan and feels some strange connection with him.  Mohan also meets an elderly singer named Saraswati Devi. She is shocked to see him but does not say anything. Whenever Chandramukhi meets Mohan, she acts strangely. She has the nightmares of a man named Madhav, who is Mohans doppelganger, plummeting to death from a cliff. Naresh senses that something is amiss and coerces Mohan to help him find the truth. On an excursion, Chandramukhi remembers everything: She was Paro in previous life and Madhav was her lover. A zamindars son raped her and accidentally murdered her.

Here, Naresh too realizes that he should step out of their life, as their romance soon gets rekindled. Here, with help of Chandramukhi, Mohan soon finds out that Saraswati Devi is Satto, Madhavs sister. Satto tells them that the villain is none other than Janak. Mohan is in a dilemma: he has broken off his engagement with Karuna for Chandramukhi and now he has to drag Janak to court. Realizing that he has no other option, Mohan decides to take matters to the court. Karuna, who doesnt have an idea of the truth, steps in to defend her father against this "conspiracy".

Here, it is revealed that Janak is indeed guilty of death of both Paro and Madhav. He raped Paro and after listening this Madhav commits suicide. Paro had cursed him that just as he took away something precious to her, Mother Nature will take something important from him too. Meanwhile, Mohan seems to be losing the case, until he finds the mansion where Paro was purportedly murdered. He also traces Billi Ram, a senile old mason, who may hold a clue to Paros disappearance. Billi Ram remembers that Janak had called him that day to patch up a wall in the mansion.

Mohan comes to the mansion with police, who tear the wall down. A skeleton tumbles out in front of Karuna. Shocked by this revelation, Karuna goes to her home and sets it on fire. She sits inside, playing a piano and succumbs to the fire. After learning of his daughters death and the new discovery, Janak realizes that Paros curse has done its work. Later, Janak pleads guilty for his crime in the court and is sentenced accordingly. As he is escorted out, he and Chandramukhi see each other for a last time. After Chandramukhi and Mohan get united, Naresh heads to America.

==Cast==
{|class="wikitable"
|-
!Actor/Actress !!Character/Role !!Notes
|- Rajesh Khanna Mohan Kapoor/Madho
|- Hema Malini
|Chandramukhi/Paro
|- Raaj Kumar Choudhury Janak Singh
|- Vinod Khanna
|Dr. Naresh Gupta
|- Priya Rajvansh Karuna Singh as Priya Raajvansh
|- Aruna Irani Saraswati Devi (Satto)
|- Deven Verma Pyarelal
|as Devan Verma
|-
|A. K. Hangal Billi Ram
|- Om Shivpuri Judge
|- Keshto Mukherjee Jagat Ram as Keshto Mukerjee
|- Satyendra Kapoor
|Paros father as Satyen Kapoo
|- Pinchoo Kapoor
|Chandramukhis father
|- Raj Mehra Priest
|- Shammi
|Sarla
|-
|D. K. Sapru Janak Singhs Father as Sapru
|- Sunder
|Villager
|- Tom Alter Major Thomas Walters
|- Julie
|Judy Thomas as Jullie
|- Harbans Darshan M. Arora Munshi
|- Sunder Taneja Rai Sahebs son
|- Kalpana Iyer Dancer (song Chhodo Sanam)
|- Mukri
|
|}

==Crew== Chetan Anand Chetan Anand
*Producer: B. S. Khanna
*Cinematographer: Jal Mistry
*Editor: Keshav Naidu
*Art Director: Sudhendu Roy
*Costume and Wardrobe: Mani J. Rabadi, Kapoor, Amarnath
*Choreographer: Madhav Kishan, Gopi Krishna, Oscar Unger, Vijay
*Music Director: Rahul Dev Burman
*Lyricist: Majrooh Sultanpuri, Qateel Shifai
*Playback Singers: Asha Bhosle, Annette Pintol, Kishore Kumar, Lata Mangeshkar, Muhammad Rafi, Parveen Sultana, Suresh Wadkar

==Soundtrack==
The films music was composed by R. D. Burman and the lyrics written by Majrooh Sultanpuri. The song "Hamein Tumse Pyar Kitna" is heard twice in the film, first time by a female singer (Parveen Sultana) and the second time by a male singer (Kishore Kumar). Kumar was nominated for a Filmfare Award for his rendering of the song, while Sultana walked away with the Filmfare Best Female Playback Award for her version of the song.
{| class="wikitable"
|-
!Song Title !!Singers !!Time
|-
|01. Hamen Tumase Pyar Kitna Kishore Kumar
|1:19
|-
|02. Chhodo Sanam Kishore Kumar, Annette Pinto
|1:13
|-
|03. Tune O Rangeele Lata Mangeshkar
|3:32
|-
|04. Hamen Tumse Pyar Kitna Parveen Sultana
|4:53
|-
|05. Sajti Hai Yun Hi Mehfil Asha Bhosle
|4:41
|-	
|06. Sawan Nahin Bhadon Nahin Asha Bhosle, Suresh Wadkar
|5:27
|-	
|07. Dukh Sukh Ki Har Ek Mala Muhammad Rafi
|5:15
|}

==Awards and nominations==
*Filmfare Best Female Playback Award--Parveen Sultana for the song "Hame Tumse Pyar Kitna" Chetan Anand
*Filmfare Best Cinematographer Award--Jal Mistry
*Filmfare Nomination for Best Male Playback Singer--Kishore Kumar for the song "Hame Tumse Pyar Kitna" 

==References==
 

== External links ==
*  

 
 
 
 
 
 