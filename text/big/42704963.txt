With Buffalo Bill on the U. P. Trail
{{infobox film
| title          = With Buffalo Bill on the U. P. Trail
| image          =
| imagesize      =
| caption        =
| director       = Frank S. Mattison
| producer       = Anthony J. Xydias Sunset Productions
| writer         = Roy Stewart Kathryn McGuire
| cinematography = Bert Longenecker
| editing        = Al Martin
| distributor    = Aywon Films
| released       = March 1, 1926
| runtime        = 6 reels
}} silent film Roy Stewart as Buffalo Bill Cody and directed by Frank Mattison. It was produced by Anthony J. Xydias.  

This film is available from the Prelinger Archive, San Francisco and the Grapevine video, Phoenix.

Several of the cast and crew of this film made later in 1926 General Custer at the Little Big Horn.

==Cast== Roy Stewart - Buffalo Bill Cody
*Kathryn McGuire - Millie Connel
*Cullen Landis - Gordon Kent
*Sheldon Lewis - Major Mike Connel
*Earl Metcalfe - Sheriff
*Jay Morley - Parson Jim Hale
*Milburn Morante - Hearts Farrel (*billed Milburn Moranti)
*Eddie Harris - Mose
*Fred DeSilva - Bill Henry
*Hazel Howell - Katy Hale
*Felix Whitefeather - White Spear
*Dick La Reno - William Rose
*Harry Fenwick - Dr. Roy Webb

==References==
 

==External links==
* 
* 
*   available for free download from  
*   available for free download from  
*  
* 

 
 
 
 
 


 
 