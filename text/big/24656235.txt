Budak Kelantan
 
 
{{Infobox Film
| name           = Budak Kelantan
| image          = Budakklantan.jpg
| director       = Wan Azli Wan Jusoh
| producer       = Ahmad Puad Onah
| writer        = Wan Azli Wan Jusoh
| distributor    = Grand Brilliance
| released       = 2008
| country        = Malaysia Kelantanese dialect)
| budget         = RM 600,000 ($185,844)
| gross          = RM 852,000 ($263,899)
}}
 Central Market and Pertama Complex. It is also a dramatisation of Kelantanese adulthood, both which portrays the similar gangsterism that used to take place in Kuala Lumpur a few decades ago. The film received critical response regarding its violence and graphic language. Furthermore, with dialogue fully in the Kelantanese dialect, it visualised the gangsterism images of Kelantanese which known for most people in Malaysia. Despite its unique approach and being one of the few Malaysian films which can be considered honest  for its relevance to the dark side of Malaysia. The film is considered an important cultural icon, inspiring and considered to be one of the few Malaysian films that has a different concept of realism, gangsterism, drama and interesting narrative story which differs from many Malaysian films.

==Plot==
The film is about 2 guys named Buchek and Jaha, who met in Central Market, Kuala Lumpur after had never seen each other for the last 13 years. Buchek, who had just graduated from Universiti Malaya, working with his other friends in burger stall while waiting to get a decent job. Jaha, who had been studying in Henry Gurney Prisoners School at Melaka has been living in Kuala Lumpur for many years. In the earlier scene of this movies, Jaha is seen to encounter a guy who tries to rape a girl in the dark corner of the street.

Jaha who rides a motorcycle with his friend, quickly stopped and beat that guy with the help of his friends. After a short conversation with the girl, Jaha offers to send the girl to his home, telling her that she will be alright as he found out that the girl is actually running from his home. After arriving home, he offered the girl to stay in comfort of his room because the fact that the house is actually lived by other male housemates. Jaha has started to smooth talk with the girl and manage to knew that the girl is actually has made an adultery with his boyfriend. Jaha who is actually a pimp, try to rape the girl and after being resisted, he instructed his friends outside the room to get in a gangrape the girl. His friends, who helped Jaha to rape the girl, feels pity for her and denied to take turns and leave Jaha alone.

===The day of Jaha meeting Buchek===
After a night in Jahas house, the girl is being offered by Jaha to be his boyfriend and he promised to look her for a job. The girl who at first urged Jaha to send him to her sisters house in Shah Alam by taking bus at Klang Bus Stand, finally accept his offered. The girl eventually taken to a ride by a car by Jahas friend, who is actually the person that Jaha sells the girl as a prostitute, based on the phone conversation while waiting for the car to arrive.

After walking to Central Market, Jaha find Buchek, who cant recognised Jaha at first. Buchek who was waiting for his friend, goes for a drink with Jaha and exchanging stories about each others after havent being met for a long time. Jaha invites Buchek to come to his house. Buchek who have to leave with his friends, accept Jaha offers and follow him while his friend going back alone. On his way walking nearby Kotaraya Complex to Pertama Complex to wait for Jahas friends to come to pick them up, Jaha tell Buchek about his life in about how he always flirting around girls here. Suddenly, after looking at Jaha flirt with a girl who walking by and asking for her phone number, Buchek quickly stopped and seeing a flashback of how Jaha doing that with many other girls. Jaha who noticed that Buchek was standing like seeing a ghost, quickly move back and asking Jaha to walk with him.

After arrived in Pertama Complex, while both guy sits and having conversation, Buchek noticed theres a 2 guys were looking suspiciously and keep watching them from far. Buchek who noticed that, ask Jaha is he rules this place, like having a power in his turf. Jaha answered and tells Buchek if he ever come here and being ignored by Jaha next time, it means that he has some rival looking for him and he doesnt want Buchek to get beat if Jaha seeing talking to Buchek cause the rival will eventually thinks that Buchek is Jahas friends. After coming inside Pertama Complex for some sightseeing, 2 guys were coming after them and by the time Jaha and Buchek wants to run, the other 2 guy who has already followed them from the outside of the place started to beat them in the back.

Going Back To Jahas Place, Jaha and Buchek was riding in car with Jahas friends, Libokbong and after a while, Buchek was asking permission from Jaha to stop in the nearest mosque to pray. Libokbong, who resisted to stop,was scolded by Jaha and forced Libokbong to stop the car after seeing a mosque besides the road. Libokbong laughed at Jahas attitude to let his friend to go for a pray. Jaha answered back by saying that his believed that even though how bad are we were,we must never stopped people to do a good things like praying.
 Coke by Jaha, but refused as he never took drugs in a whole of his life but finally accepted it after Jaha says he will not drink it if Buchek is not. After that, Buchek asked permission to go a prayer as it was nearly prayer time. Soon, they went back to Jahas home.

===Kidnapping girl===
In the early morning, Jaha and Libokbong take Buchek for a ride and tell him its just for a sightseeing. After a minute, Jaha saw a Chinese girl was jogging beside the road. Jaha told Libokbong to move a bit faster and by the time they approaching the girl, Jaha quickly grabbed the girl inside the car. Buchek who didnt know about this helped Jaha to take the girl inside the car while grabbing her and keeping her mouth shuts. Libokbong quickly speed up the car but unfortunately along the freeway he noticed that the gas was running out. Jaha instantly told Libokbong to pull over not far from the gas station and buy the petrol using an empty container. While Jaha was holding the girl with Buchek, Jaha told Buchek to hang over as he need to go to the restroom. After seeing that Jaha has gone, Buchek immediately told the girl to run before his friends coming back. He gave the Chinese girl a money for a cab fare and the Chinese girl asked him a phone number if she can meet him again to pay back the money. Buchek who was freak out of what he is doing, quickly writes his phone number. The Chinese girl, Lee Chen Chen, safely rides a cab after being stopped by Buchek in the middle of the freeway. Jaha and Buchek who was coming back after a few minutes later were shocked after Buchek told them that he had to let the girl go cause he dont want to have anything involve in this kidnapping. Soon after riding back to the road, Jaha who was so pissed for Bucheks act, asked Libokbong to stop the car and told Buchek to get out.

===Meeting with Che Noor===
Jaha was seen walking in Central Markets after getting his motorcycles out of workshop, a few days after his housemate got into accidents with it. He saw a girl walking toward Central Markets front door and trying to flirt with her. He surprised that the girl was meeting with Buchek, who is his friends, named Che Noor. Buchek, who seems guilty for his act of that day, invites Jaha to have a drink with him and Che Noor.After that meeting, Jaha keeps asking forgiveness to Buchek for taking him into his lifestyle and tells Buchek what a lucky man for him to have such nice and beautiful girlfriend.

Jaha was seen writing a letter, telling Jaha about why actually turned into a bad guy living in this big cities and because he feels that he is more good in telling about him in writing. Buchek, who noticed that Jaha had a feeling with his girlfriend after they three meet together, tried to find a way to match his girlfriend with Jaha, hoping that his best friend can possibly become better person by finding love with a girl that he may be in love. Buchek try to bring Jaha and Che Noor in another meet up, but ended up by Che Noor walking back home after knowing that Buchek was leaving to some place after the three meet at Central Market again.

While watching television, Buchek received a call from the Chen that she helped to run away from being kidnapped by Jaha and Libokbong. Chen asked to meet him. Buchek went and the girl told him that she was thankful for what Buchek did and feels that she was like a princess who was being rescued by a charming prince like a fairy tale story. Buchek feels accepted for Chens honest friendship and he feels that he must do something to get Jaha together with Che Noor,who Buchek cant accept her love unlike Che Noor who has long admired him.

===Killing an innocent===
Jaha, who felt frustrated, couldnt help himself from forgetting about Che Noor. Until one night, while having a drink with his friends, he noticed that some people kept looking at him and his friends on the other table. Jaha who felt disturbed, eyeballed those guys and suddenly stood up and shouted at them. After one guy replies in an impolite way, Jaha quickly confronts the guy and keep beating him. The guy ran away and was chased by Jaha and his friends. Jaha was seen grasping a knife from a nearby fruit stall and stabbed the guy after he managed to reach him. After seeing the guy was about to die, Libokbong told him to run and they all kept running.

===The death of Jaha=== Dikir Barat show. While failing to call Jaha when he was about to go to the show, Jaha was actually running from the police who rushed to his neighbourhood to catch him after he was told by Libokbong who gets the alert about the police from his friends. Jaha unfortunately got into accident and dies.

Buchek received a call from Libokbong and was shocked with the news.He quickly told Che Noor how much Jaha was trying to change his life and be with her that he ended up dying looking for a love that he thought he can finally get.

Libokbong met Buchek and gave him a letter which he found inside Jahas pocket, the same pants that he had worn when he died. Libokbong thought that this letter might be written to Buchek. While reading the letter, Chen was seen smiling at Buchek near him.

==Cast==
{| class="wikitable"
|- bgcolor="CCCCCC"
! Actor !! Role
|- Danny X Buchek
|-
| Mohd Asrulfaizal Kamaruzaman  || Jaha
|-
| Md Eyzendy Md Aziz  || Libokbong
|- Bienda || Che Noor
|}

==Reception==
The film received a good reception, even with a relatively lower budget than most Malaysian movies, (RM600,000)  the film manages to draw a profit and has made over RM852,000 gross in Malaysia  despite less promotion in mass media and being the first film directed by young director Wan Azli Wan Jusoh. In the beginning, it was just an experiment by Alternate Studio, a child company under major film distributor Grand Brilliance from Malaysia which aims to have more different genres of film in local industry.
 8TV to recognise the Malaysian entertainment scene has nominated Budak Kelantan for the Breakthrough Film Award.

The movie was also a very few of Malaysian movies who portrays multi-racial topics, with Buchek helping a Lee Chen Chen,a Chinese girl who was almost kidnapped by Jaha, Bucheks best friends and finally ended up with Buchek making friends with Chen. Although it was considered a small plot that didnt help much of strength, it was a few attempts by Malaysian film producers to live the spirit 1Malaysia,  by making more racial integration harmony in Malaysian films.

Apart from that, Budak Kelantan is also among other Malaysian movie that used local dialects just like Anak Mami The Movie, Mami Jarum, Mami Jarum Junior, Anak Mami Kembali, Nana Tanjung & Nana Tanjung 2, which all were using dialects from Penang.

==Cultural Influence==
Kelantanese
 Kelantanese people Kuala Lumpur City Hall to improved the quality and security of Kuala Lumpur except for small cases of snatches or pickpockets.

 Central Market,Kotaraya Complex,Pertama Complex and Klang Bus Stands
 These shopping arcades holds a fond memories of every people who lives in Kuala Lumpur in 80s & 90s as a favourite hangout places and with a strategic location of most buses last stop in Kuala Lumpur. The early days of Kuala Lumpur saw many dark stories of street crime that happens such as snatches, extortions, rapes, drug dealing as well as prostitution by hookers and transvesties in this places.
 Royal Malaysia transvestites as well as young age prostitutes.
 

==See also==
*  

==References==
 

==External links==
*  
* http://www.kakiseni.com/ Malaysian arts, theatre, dance, music, film and culture
* http://www.youtube.com/watch?v=QMWKIaP3J50/ Budak Kelantan - Movie Trailer
* http://malaysia.movies.yahoo.com/Budak+Kelantan/movie/14933/ Budak Kelantan - Malaysian Yahoo Movies

 
 
 
 