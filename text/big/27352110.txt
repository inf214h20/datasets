My Joy
 
{{Infobox film
| name           = My Joy
| image          = My Joy.jpg
| caption        = Film poster
| director       = Sergei Loznitsa
| producer       = Oleh Kokhan
| writer         = Sergei Loznitsa
| starring       = Viktor Nemets
| music          = 
| cinematography = Oleg Mutu
| editing        = Danielius Kokanauskis
| studio         = 
| distributor    = 
| released       =  
| runtime        = 127 minutes
| country        = Ukraine
| language       = Russian
| budget         = € 1.5 million
}}
My Joy ( , Transliteration|translit.&nbsp;Schastye moyo;  , Transliteration|translit.&nbsp;Shchastya moye) is a 2010 Ukrainian road movie directed by Sergei Loznitsa. It is set in the western regions of Russia, somewhere near Smolensk. My Joy was the first Ukrainian film ever to compete for the Palme dOr.

==Plot==
It is summer, and young driver Georgy takes his light truck on a trip to another town with a cargo of flour. He is stopped at a road police post by a pair of rude and corrupt policemen. While they are flirting with the woman they stopped earlier, Georgy manages to grab his papers and leave unnoticed. There he picks up a hitchhiker, an old man who recounts him a disturbing story. Soon after WWII, while returning home from the front, he was blatantly robbed by a station commandant, and later killed him in retaliation. On another stop the old man leaves as abruptly as he appeared.

Later Georgy meets an underage prostitute. He takes pity on the girl and gives her some money out of charity, but she takes that as offence, insults him and leaves.

Later yet, Georgy is lost in the night and decides to camp in the field until dawn. Three locals approach and try to steal something from the car, only to be stopped by Georgy. They distract his attention with some neutral conversation, and hit him on the head with a log.

The scene shifts to the times of WWII. Early in the war, two Soviet soldiers from a defeated unit cautiously feel their way through the occupied land in the deep German rear. They enter a lone house where lives a widowed teacher with his infant son. The teacher is kind to the soldiers and provides them with much-needed food and shelter. However, in their eyes his reticence to fight amounts to downright treason, so they kill him,
rob the house, and continue on their way, leaving the child to his own devices.

The scene shifts back to the present. It is winter, and Georgy lives in the same house that once was the teachers. The blow has left him feeble-minded and mute. He walks around bearded, dilapidated, with blank stare. The woman living in the house keeps him as a sex slave. Meanwhile, she trades his flour on the local market. A policeman approaches and tells her that Georgy and his car are searched for, so she better gets rid of both. Georgy is beaten by the locals and detained by the police, only to be released the next night when another inmate challenges the lone guard to a fight, beats him unconscious, and unlocks the cells.

The woman leaves the place, abandoning Georgy. Homeless, he wanders the village, being driven off by the locals, until he collapses from exhaustion. He is found and picked up by the old man whom he earlier gave a ride.

A military van comes to the village, carrying two servicemen tasked with delivering the body of a deceased soldier to his native place. Their daunting task is not made easier by the fact that one of them, an officer, suffers from delirium tremens. Unable to locate the relatives of the dead soldier, they decide to bribe some random people into signing the papers and leave the body to them. They approach the old man, who at first is suspicious (he even prepares his old pistol). Then the deal seem to arrange well, but shortly after that Georgy walks out of the house to find the old man dead. It is hinted that he may have been axed by the officer, who in his alcoholic delusion mistook him for someone else.

Georgy thoughtlessly grabs the old mans pistol and walks out to the road, where he is picked up by a very talkative truck driver. Meanwhile, on the road police post the same two corrupt policemen stop a car with a police major and his wife. They insult the major, he insults back, a fistfight ensues, so they handcuff and brutally beat him. To produce two fake witnesses of his arrest, they stop another car, which is the truck with Georgy. They easily threaten the driver into signing the papers, but when they turn to Georgy, he reaches for his pistol and shoots dead first both policemen, then everyone else. Still clutching the pistol, he staggers out into the dark.

==Cast==
* Viktor Nemets as Georgy 
* Olga Shuvalova as teenage prostitute 
* Vlad Ivanov as Major from Moscow 
* Dmitri Gotsdiner as train station Superintendent

==Production==
The film was a co-production between Germanys Ma.ja.de, Ukraines Sota Cinema Group and the Netherlands Lemming Film.  The film was shot in Ukraine as a condition for receiving money from the Ukrainian Ministry of Culture, but most of the 1.5 million Euro budget came from Germany. According to the director there are about 140 cuts in the whole film. Vlad Ivanovs Russian was dubbed as he is a Romanian actor. 

== Reviews ==
There was a considerable outcry in Russian media over the films purported Russophobic slant. Film director Karen Shakhnazarov claimed that Loznitsa would like everyone living in Russia to be shot.    But another Russian film director, Andrey Zvyagintsev, called My Joy the best Russian-language film of the decade.   
 Michael Atkinson) reviewed My Joy as "a maddening vision and one of the years must-see provocations.   

==Awards and nominations==
The film was selected to compete for the Palme dOr at the 2010 Cannes Film Festival in May.    At the 7th Yerevan Golden Apricot International Film Festival in July, the film won the Silver Apricot Special Prize.   

==References==
 

==External links==
*  

 
 
 
 
 
 
 