The Thursday
 
{{Infobox film
| name           = The Thursday
| image          = The Thursday poster.jpg
| caption        = Film poster
| director       = Dino Risi
| producer       = Dino De Laurentiis
| writer         = Franco Castellano Giuseppe Moccia Dino Risi
| starring       = Walter Chiari
| music          = Armando Trovajoli
| cinematography = Alfio Contini
| editing        = 
| distributor    = 
| released       =  
| runtime        = 105 minutes
| country        = Italy
| language       = Italian
| budget         = 
}}

The Thursday ( ) is a 1963 Italian comedy film directed by Dino Risi. It was shown as part of a retrospective on Italian comedy at the 67th Venice International Film Festival.   

==Cast==
* Walter Chiari as Dino Versini
* Michèle Mercier as Elsa
* Roberto Ciccolini as Robertino
* Umberto DOrsi as Rigoni
* Alice Kessler as Herself
* Ellen Kessler as Herself
* Silvio Bagolini as Doctor
* Emma Baron as Giulia
* Edy Biagetti
* Olimpia Cavalli as Olimpia
* Consalvo DellArti as Concierge
* Margherita Horowitz as Kleptomaniac
* Salvo Libassi
* Liliana Maccalè (as Siliana Maccalè)

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 


 
 