The Tracker
 
 
{{Infobox film
| name           = The Tracker
| image          = The Tracker poster.jpg
| caption        = Theatrical film poster
| director       = Rolf de Heer
| producer       = Rolf de Heer Julie Ryan
| writer         = Rolf de Heer
| starring       = Gary Sweet David Gulpilil
| music          = Graham Tardif
| cinematography = Ian Jones
| editing        = Tania Nehme
| distributor    = 
| released       = 2002
| runtime        = 98 minutes
| country        = Australia
| language       = English
| budget         = 
| gross = A$818,388 (Australia) 
}} tracker (David Gulpilil) to find the murderer of a white woman. The tagline is "All men choose the path they walk.

==Plot==
 Australian outback, Aboriginal man who has been accused of murder. 

==Cast==
* David Gulpilil as The Tracker
* Gary Sweet as The Fanatic
* Damon Gameau as The Follower
* Grant Page as The Veteran
* Noel Wilton as The Fugitive 

==Production==
 Arkaroola Sanctuary, in South Australias Flinders Ranges. De Heer used an intentionally small film crew, saying that “Its all a much better process ...”. {{cite web
  | title = Production Notes (The Tracker)
  | publisher = Vertigo Productions
  | url = http://www.vertigoproductions.com.au/information.php?film_id=8&display=notes
  | accessdate =13 June 2008}}  The film is intercut with paintings by Peter Coad which portray brutal actions not shown, while the lyrics of the soundtrack (written by De Heer) form part of the narrative, and are sung by Archie Roach with music composed by Graham Tardif. {{cite book
  | last = Ebert
  | first = Robert
  | authorlink = Roger Ebert
  | coauthors = 
  | title = Roger Eberts Movie Yearbook 2007
  | publisher = Andrews McMeel Publishing
  | year = 2006
  | location = 
  | pages = 708–709
  | url = http://books.google.com/?id=LtJgv-f-nl8C&pg=PA708&lpg=PA709&dq=%22the+tracker%22+plot
  | doi = 
  | id = 
  | isbn = 0-7407-6157-9}} 

==Receptions==
Film review aggregator Rotten Tomatoes assessed the film at 88% on its tomatometers with an average 7.1/10 rating.  Renowned film critic Roger Ebert gave the film three-and-a-half stars out of four calling the film "haunting" and the performances "powerful".  David Stratton described the film as "remarkable". 

===Awards and Nominations===

{| class="wikitable"
|-
! Awards
! Category
! Subject
! Result
|- AACTA Awards  (2002 AFI Awards)  AACTA Award Best Film Julie Ryan
| 
|- Rolf de Heer
| 
|- AACTA Award Best Direction
| 
|- AACTA Award Best Original Screenplay
| 
|- AACTA Award Best Actor David Gulpilil
| 
|- AACTA Award Best Editing Tania Nehme
| 
|- AACTA Award Best Cinematography Ian Jones
| 
|- ARIA Music ARIA Award Best Original Soundtrack Album Graham Tardif
| 
|- Australian Screen ASSG Award Best Sound
| 
|- Australian Writers AWGIE Award Best Film - Original Screenplay Rolf de Heer
| 
|- Cinemanila International Film Festival Best Actor David Gulpilil
| 
|- Film Critics FCCA Awards Best Film Julie Ryan
| 
|- Rolf de Heer
| 
|- Best Director
| 
|- Best Screenplay
| 
|- Best Actor David Gulpilil
| 
|- Best Editing Tania Nehme
| 
|- Best Cinemtography Ian Jones
| 
|- Best Music Score Graham Tardif
| 
|- Ghent International Film Festival Grand Prix Award Rolf de Heer
| 
|- Best Screenplay
| 
|- Inside Film Awards Best Feature Film Julie Ryan
| 
|- Rolf de Heer
| 
|- Best Direction
| 
|- Best Script
| 
|- Best Actor David Gulpilil
| 
|- Best Music Graham Tardif
| 
|- Best Cinematography Ian Jones
| 
|- Best Sound
| 
|- Paris Film Festival Press Award Rolf de Heer
| 
|- Screen Music Award Best Original Song Graham Tardif
| 
|- Valladolid International Film Festival Jury Special Prize Rolf de Heer
| 
|- Golden Spike Award
| 
|- Venice Film Festival SIGNIS Award - Honorable Mention
| 
|- Golden Lion
| 
|-
|}

==See also==
* Cinema of Australia
* South Australian Film Corporation
* List of Australian films

==References==
 

==External links==
* 
* 
*   at Vertigo Productions

 
 

 
 
 
 
 
 
 
 
 
 
 
 