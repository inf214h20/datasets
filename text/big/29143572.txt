Hitler's Reign of Terror
{{Infobox Film
| name           = Hitlers Reign of Terror
| image          = 
| image_size     =
| caption        = 
| director       = 
| producer       = 
| writer         = 
| narrator       = Edwin C. Hill
| starring       = 
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       = 
| runtime        = 65 minutes United States
| language       = 
| budget         = 
| preceded_by    = 
| followed_by    = 
| website        = 
}}
Hitlers Reign of Terror is an independently released 1934   scoffed at the film for its prediction that Hitlers Germany was a future threat to world peace. 

==Preparation and Interview with Hitler== Cornelius Vanderbilt, New York French cameramen, political demonstrations. Crown Prince Hohenzollerns are so much easier to see than Hitler?” 
 Nazis obtained a parliamentary Plurality (voting)|plurality, Vanderbilt was able to secure what would have to suffice as the closest he would get to an interview with Hitler.  Only able to edge in one question about the Jewish question|“Jewish problem” amid the chaos, Vanderbilt yelled, “And what about the Jews, Your Excellency.” Hitler shrugged off the question and instructed Vanderbilt to set up a meeting with Dr. Ernst Hanfstaengl.  This meeting never took place. 

==The Film==
 American Newsreel|newsreel footage, and reenactments of the various conversations and interactions that Vanderbilt had with officials while travelling throughout Europe.  Initially, Vanderbilt found it difficult to find a major production company to produce the film. However, he worked out a partnership with two different producers, Joseph Seiden and Samuel Cummins.  Vanderbilt edited the film with Edwin Hill and hired on Mike Mindlin, known for his adult film This Nude World (1933), as the director. 

==Plot== Cornelius Vanderbilt, Prince Louis Samuel Dickstein of New York and Hill give speeches directly to the audience, explaining the dangers of Nazism.

==Reception== Mayfair on Broadway Avenue The Production Motion Picture Producers and Distributors of America (MPPDA).  Rory Norr was sent by the MPPDA to view the film at its opening, and report back on whether he felt the content was appropriate for the big screen.  His conclusions were that the film, “included only a few original ‘reproductions’ of alleged interviews had by Mr. Cornelius Vanderbilt, Jr. with the Kaiser, Mr. Hitler, and others.  A general statement on the screen covered the fact that such interviews were ‘reproductions’ and it was obvious that the actors took the parts of the Kaiser, Hitler, and others in certain scenes.”  As such, he concluded on the question of whether or not the movie should be considered a propaganda film and banned from theaters, “The fact that it is a propaganda picture does not make it necessarily unsuitable for the screen… There is no more reason why a theater owner may not take a given side on a public question than why a newspaper publisher should not adopt definite policy one way or another re Nazism|Hitlerism.” 
 Washington D.C., Department of Commerce yielded the result that, “the film serves no good purpose.” 
 censors followed suit in their fear to offend the Nazis.  The New York State Censor Board, for one, eventually banned the film throughout the state. 

After having passed the review of the Chicago Board of Censors, Hitler’s reign of Terror became the subject of concern for Chicago’s Nazi Consul (representative)|consul, who eventually convinced the city government to halt the release of the film until certain changes were made. 

==What Came of the Film== Royal Belgian Film Archive in Brussels had located a copy of the film in their catalog.  Doherty’s theory is that a Belgian film distributor must have ordered a copy of the film from outside the country before the Nazis invaded Belgium.  Being a foreign film, Doherty continues to surmise, the copy had to clear customs.  Once the Nazis invaded the country, though, the distributor likely did not want to be caught with the film and never picked it up from customs.  As a result, the film lay in the back of a storage closet in Belgium for close to eighty years.  According to an article by The_New_Yorker|The New Yorker, this would make Hitler’s Reign of Terror the, “first-ever anti-Nazi film.” 

==Notes==
 

==References==
*Doherty, Thomas Patrick. Pre-Code Hollywood: Sex, Immorality, and Insurrection in American Cinema 1930-1934. New York: Columbia University Press 1999. ISBN 0-231-11094-4

*Doherty, Thomas Patrick. "Hitler, a "Blah Show Subject"" Hollywood and Hitler, 1933-1939. New York: Columbia UP, 2013. 59-66. Print.

==External links==
* 
* 

 
 
 
 
 