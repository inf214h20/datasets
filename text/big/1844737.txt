The Roots of Heaven
{{Infobox film
| name           = The Roots of Heaven
| image          = The roots of heaven(2).jpg
| caption        =  Original film poster
| director       = John Huston
| producer       = Darryl F. Zanuck
| screenplay     = Romain Gary Patrick Leigh-Fermor
| based on       =  
| starring       = Errol Flynn Juliette Gréco Trevor Howard
| cinematography = Oswald Morris
| music          = Malcolm Arnold Russell Lloyd studio = Darryl F. Zanuck Productions
| distributor    = 20th Century Fox
| released       =  
| runtime        = 121 mins
| language       = English
| awards         =
| budget         = $3.3 million  
| gross          = $3 million
}} 1958 American The Roots of Heaven (Les racines du ciel).

The film starred Errol Flynn, Juliette Gréco, Trevor Howard, Eddie Albert, Orson Welles, Paul Lukas, Herbert Lom and Gregoire Aslan. The film itself was shot on location in French Equatorial Africa. 

==Plot==
Set in French Equatorial Africa, the film tells the story of Morel (Trevor Howard), a crusading environmentalist who sets out to preserve the elephants from extinction as a lasting symbol of freedom for all humanity. He is helped by Minna (Juliette Gréco), a nightclub hostess, and Forsythe (Errol Flynn), a disgraced British military officer hoping to redeem himself.

==Cast==
 
* Errol Flynn as Maj. Forsythe
* Juliette Gréco as Minna
* Trevor Howard as Morel
* Eddie Albert as Abe Fields
* Orson Welles as Cy Sedgewick
* Paul Lukas as Saint Denis
* Herbert Lom as Orsini
* Gregoire Aslan as Habib
* André Luguet as Governor
* Edric Connor as Watari
* Oliver Hussenot as The Baron
* Pierre Dudan as Maj. Scholscher
* Marc Doelnitz as De Vries
* Dan Jackson as Madjumba
* Maurice Cannon as Haas
* Jacques Marin as Cerisot
* Alain Saury as A.D.C.
* Francis de Wolff as Father Fargue
* Frederick Ledebur as Peer Qvist
* Jacqueline Fogt as Madam Orsini (spanked)
 

==Production==
===Development===
20th Century Fox bought the film rights the novel in April 1957 for a price of more than $100,000. TRACY WILL STAR IN LAST HURRAH: Actor to Play Political Boss in Columbia Film, Which John Ford Is Directing Roots of Heaven Planted
By THOMAS M. PRYOR Special to The New York Times.. New York Times (1923-Current file)   29 Apr 1957: 21.   In May, Darryl F. Zanuck announced he would produce the film independently for Fox (he had a contract with the studio to make films for them), and wanted John Huston to direct. Magnani to Do Comedy; Zanuck Seeks Huston; Birdwell Deal Tripled
Schallert, Edwin. Los Angeles Times (1923-Current File)   08 May 1957: B9.   (This was before the novel had even been published in the US, but it had sold over 300,000 in Europe). Of Local Origin
New York Times (1923-Current file)   17 Sep 1957: 39.  ) Zanuck said the theme of the film was "simple... A man comes to the conclusion that if we dont stop killing people w destroy ourselves. And he says, "Why not start with our biggest companions on earth, the elephants, whose only enemy is man?" 
He later added:
 This picture is really great for us - intellectually great. Whether its commercially great, whether people will grab on to it, we must wait and see. If they grab on to a man in love with a bridge, then why shouldnt they grab on to a man in love with an elephant? Zanucks Subject: Roots of Heaven
Scheuer, Philip K. Los Angeles Times (1923-Current File)   12 Oct 1958: F2.   
Huston said he wanted to direct the novel before Zanuck approached him: Selznick   - all those memorandum! - Id sworn never to work with a producer again, but I did want very much to make this particular film. So we me several times and talked it through and finally agreed to try it.  
Huston agreed to direct for a fee he described as "sightly higher" than $300,000. When the irony of a big game hunter like Huston making a movie about a militant elephant conservationist, Huston said "Contrary to prevailing opinion, I never found an elephant big enough to justify the sin of killing one." HUSTON HITS HIGH WITH HEAVEN AND GEISHA
By RICHARD W. NASON. New York Times (1923-Current file)   28 Sep 1958: X9.  
Zanuck headed to the Belgian Congo in late 1957 to scout locations and Gary was hired to write the script. Tyrone Power Eyes Role of Wise Solomon
By Dorothy Manners. The Washington Post and Times Herald (1954-1959)   05 Nov 1957: B9.   A DIPLOMAT CRACKS THE MOVIES
Buchwald, Art. Los Angeles Times (1923-Current File)   24 Jan 1958: B5.  

===Casting===
William Holden was mentioned as a possibility for the lead part of Morel, as was James Mason. Studio Opened to Preminger: Mardios Beach First of Pair; Mason Up for Consuls Roots
Scheuer, Philip K. Los Angeles Times (1923-Current File)   12 Feb 1958: B9.   Holden wanted to make the film but he was under contract to Paramount, who would not let him make the film unless he signed another contract with them, which he refused to do so. NEVER A BAD SHOW: Hollywood Producers Call Bill Holden the All-American Face
Hopper, Hedda. Chicago Daily Tribune (1923-1963)   06 Apr 1958: c20.   

The lead role was taken by Trevor Howard. Errol Flynn signed to play a key support role but was given top billing.  Flynn left the cast of the play The Master of Thornfield to appear in the movie.  UNPRODUCED PLAY BOUGHT FOR FILM: Comedy Is by Samuel Taylor and Cornelia Otis Skinner -- Paramount Retrenches
By THOMAS M. PRYORSpecial to The New York Times.. New York Times (1923-Current file)   26 Feb 1958: 23.   (Flynn and John Huston had famously brawled at a Hollywood party over a decade ago. Kims Gunning for Harlow Biog
Dorothy Kilgallen:. The Washington Post and Times Herald (1954-1959)   13 Mar 1958: D8.  )

Juliette Greco, who had been in Zanucks version of The Sun Also Rises and since become the producers lover, was signed as the female lead. No Work Here, So Holden Is Off to Europe
Hopper, Hedda. Chicago Daily Tribune (1923-1963)   14 Feb 1958: 12.  Eddie Albert and Paul Lukas rounded out the main cast. Orson Welles signed to do a cameo.

The book was eventually published in the US and became a best seller. BEST SELLERS
Los Angeles Times (1923-Current File)   20 Apr 1958: E6.  

===Shooting===
The film was mostly made on location in Africa over five months, in the Belgian Congo and Tchad in the Northern Cameroons, where the elephants were located. The cast and crew suffered from the heat, malaria and other tropical diseases.  Temperatures would routinely reach 134 degrees in the day and 95 degrees at night; people had to shower four or five times a night. On some days it would be a four hour drive to the location and back and all the water had to be flown in. 

It is the memories of the challenging location that Flynn mentions with affection in his memoirs My Wicked, Wicked Ways (1959). The company reported 900 sick calls from a cast and crew of 120 - however Flynn did not fall ill. Heres Switch: Flynn to Play a Teetotaler
Bacon, James. Chicago Daily Tribune (1923-1963)   29 June 1958: l9.   Juliette Greco contracted a serious illness. Juliette Greco, Actress, Ill
New York Times (1923-Current file)   09 June 1958: 26.  

"I would never make a picture there again," said Zanuck of Africa. "NEVER AGAIN": Making a film in Equatorial Africa
A Correspondent. The Manchester Guardian (1901-1959)   16 Aug 1958: 3.   However he was proud that "There is not one dubbed line, transparency plate or process shot in the whole picture." 

The unit then moved to Paris for studio filming. While there, Greco fell ill with a re-occurrence of her illness. Errol Flynn had a re-occurrence of his malaria, requiring hospitalisation as well. Huston at Fontainebleau
Grenier, Cynthia. Sight and Sound27.6 (Fall 1958): 280. 

Orson Welles did his part in two days at a Paris studio. His rate was normally $15,000 but he did it gratis in order to repay Zanuck for helping Welles find the funds to complete the movie Othello (1952 film)|Othello (1952). Hedda Visits Old Friends in Paris Studio
Hopper, Hedda. Chicago Daily Tribune (1923-1963)   11 June 1958: b11.  

Huston later said, "I still dont want to have to work with a producer again but if I had to, Id certainly choose Darryl. Hes been very good, co-operative and decent throughout." 
===Post Production===
The movie was edited in London rather than Paris so that Zanuck could be near Greco, who was making a movie there. Avas Got a Crush on Her Costar
The Washington Post and Times Herald (1954-1959)   26 Aug 1958: B8.  
==Reception==
===Box Office===
The film recorded admissions of 1,266,452 in France. 
===Critical===
The Los Angeles Times thought that "John Huston may have bitten off more than he could chew in "The Roots of Heaven," but much of it makes for thoughtful mastication... itsometimes seems too strange to be real." Roots of Heaven Exotic Adventure: But Its Heros Cause Falters in Film of Romain Gary Novel
Scheuer, Philip K. Los Angeles Times (1923-Current File)   01 Jan 1959: B7.  
==References==
 

==Bibliography==
* Flynn, E. My Wicked, Wicked Ways. G.P. Putnam’s Sons 1959, Pan Books 1961 in association with William Heinemann Ltd, 5th Printing 1979.
* Norman, B. The Hollywood Greats. Arrow Books, 1988 edition.
* Solomon, Aubrey. Twentieth Century Fox: A Corporate and Financial History (The Scarecrow Filmmakers Series). Lanham, Maryland: Scarecrow Press, 1989. ISBN 978-0-8108-4244-1.
* Thomas, T. Behlmer, R. & McCarty, C. The Films of Errol Flynn. Citadel Press. 1969.

==External links==
*  
*  
*  
*  
*  


 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 