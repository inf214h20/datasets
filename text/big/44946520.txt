Jack and Jill (1917 film)
{{Infobox film
| name           = Jack and Jill
| image          = Jack and Jill (1917 film) poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = William Desmond Taylor
| producer       = Oliver Morosco Margaret Turnbull
| starring       = Jack Pickford Louise Huff Leo Houck Don Bailey J.H. Holland Jack Hoxie
| music          = 
| cinematography = Homer Scott
| editing        = 
| studio         = Oliver Morosco Photoplay Company
| distributor    = Paramount Pictures
| released       =  
| runtime        = 50 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =
}}
 Western silent Margaret Turnbull. The film stars Jack Pickford, Louise Huff, Leo Houck, Don Bailey, J.H. Holland and Jack Hoxie. The film was released on November 12, 1917, by Paramount Pictures.  

This film is now lost film|lost. 
 
==Plot==
 

== Cast ==
*Jack Pickford as Jack Ranney
*Louise Huff as Mary Dwyer
*Leo Houck as Young Kilroy
*Don Bailey as Honest George Frazee
*J.H. Holland as Lopez Cabrillo
*Jack Hoxie as Cactus Jim 
*Col. Lenone as Senor Cabrillo
*Beatrice Burnham as Doria Cabrillo

== References ==
 

== External links ==
*  
*  
*  

 
 
 
 
 
 
 
 

 