Color of the Cross 2: The Resurrection
{{Infobox Film |
  name= Color of the Cross 2: The Resurrection |
  image= |
  caption =  |
    writer= Jean-Claude La Marre |
  starring= Jean-Claude La Marre Raquel "Roque" Herring Gerald Webb Andy Garza Quincy Atkinson Adi Benson Harace Carpenter | director=Jean-Claude La Marre |
  music= Budda | 	 
  distributor=Warner Brothers Lightyear Entertainment |
  released=March 5, 2008 |
  runtime= 84 minutes |
  language=English |
  producer=Kristian Bernard Horacio Blackwood Christian Omari |
  budget=|
}}

Color of the Cross 2: The Resurrection is a 2008 direct-to-video religious film directed by, and starring Jean-Claude La Marre. The film is a direct sequel to the film Color of the Cross, and centers on the resurrection of Jesus following his death by crucifixion.

==Cast==
*Jean-Claude La Marre as Jesus Christ
*Raquel "Roque" Herring as Mary Magdalen
*Lamont Clayton as John the Apostle
*David Harbaugh as Luke the Evangelist
*Lature Irvin as Matthew the Evangelist
*Armond Kinard as Simon the Zealot
*Alicia Mallory as the Virgin Mary
*Wes Martens as Joseph of Arimathia
*Rene Parker as Pontius Pilate
*David Novak as Caiaphas
*Art Roberts as Nicodemus
*Andrea Scarduzio as Andrew the Apostle Saint Thomas
*Sebastian Siegel as Judas Iscariot
*Seven Stewart as James the Great Joseph
*Terrence Tatum as James the Lesser
*Leonard Zanders as Simon-Peter

==External links==
* 
* 
* 

 
 
 
 
 
 
 
 


 
 