Mana Desam
{{Infobox film
| name           = Mana Desam
| image          = Mana Desam.png
| image size     =
| caption        =
| director       = L.V. Prasad
| producer       = Krishnaveni (actress)|C. Krishnaveni Mirjapuram Raja
| writer         = Samudrala Raghavacharya
| narrator       =
| starring       = Chittor V. Nagaiah C. H. Narayana Rao C. Krishnaveni
| music          = Ghantasala Venkateswara Rao
| cinematography = M.A. Rehman
| editing        =
| distributor    = 1949
| runtime        = 172 mins
| country        = India Telugu
| budget         =
| preceded by    =
| followed by    =
}} Telugu social directed by Bengali novel by Sarat Chandra Chattopadhyay and runs in the backdrop of Indian freedom struggle. 

== Specialities == Ghantasala as a music director and famous south Indian singer P. Leela to Telugu people.
* Krishnaveni used the all traditional story telling techniques then in the movie. She used styles of Burra katha, Oggu katha, stage dramas like Veedhi Natakalu, Bommalatalu in parts of the movie. She also included all types of traditional songs in Telugu, patriotic songs, dhampudu songs, bhajans and rural songs.
* The movie also portrays the Gandhian values during freedom struggle and their deterioration after India got freedom from British.
* It is first Telugu film based on Bengali story. Many Bengali stories like Devdas and Aradhana were used as plots for Telugu movies afterwards.

==Plot== Congress party but Shoba is opposed to congress views. They both are arrested in political violence during the freedom struggle. Madhu is tortured by the police and loses memory. The movie ends with his recovering from amnesia and reunion of the couple.
Producer Krishnaveni herself played the role of Sobha and Narayana Rao played the role of Madhu.

== Crew == Krishnaveni and her husband Raja of Mirzapuram under MRA Productions.
* Director - L. V. Prasad Ghantasala
* Story - famous Bengal writer Sharat Chandra Chattopadhyays novel
* Dailogues and lyrics - Samudrala Raghavacharya
* Camera - MA Rehman
* Singers - Ghantasala (singer)|Ghantasala, M. S. Ramarao, C. Krishnaveni, P.G. Jikki Krishnaveni, P. Leela, Nagaiah etc.

== Cast ==
* Madhu - C. H. Narayana Rao
* Sobha - Krishnaveni (actress)|Ms. C Krishnaveni
* Police Inspector - N. T. Rama Rao
* Chittor V. Nagaiah Relangi Venkatramiah
* S. V. Ranga Rao|S. V. Rangarao
* Vangara Venkata Subbayya
* Ramanatha Sastry
* Kanchana
* Surabhi Bala Saraswati
* Hemalatha
* Lakshmikantham
* Hero - Ravijangam

==Soundtrack==
* Jaya Janani - patriotic song, singers are Ghantasala and C. Krishnaveni
* Emito Ee Sambhandam Enduko Ee Anubhandam - duet, singers are M. S. Ramarao and C. Krishnaveni
* Chalo Chalo Raja - Singers are M. S. Ramarao and C. Krishnaveni
* Ninnu Nenu Maruvalenura Police yenkatsami - rural style song, singer is Jikki Krishnaveni
* Atta leni Kodalu Uttamuralu - folklore song. singer is Jikki Kirshnaveni
* Vedalipo Tella Dora Vedalipo - patriotic group song
* Nirvedamela kanneridela - Gandian song by Nagaiah
* O Bharata Yuvaka - patriotic group song
* Kallo Ninnu CHoosinane Pilla Vallu Jallumannade - duet, singers are Ghantasala and C. Krishnaveni
* Vaishnava Janato - Traditional Bhajan

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 
 