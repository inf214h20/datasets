Nathalie Granger
{{Infobox Film
| name           = Nathalie Granger
| image_size     = 
| image	=	Nathalie Granger FilmPoster.jpeg
| caption        = 
| director       = Marguerite Duras
| producer       = Jean-Michel Carré Luc Moullet
| writer         = Marguerite Duras
| narrator       = 
| starring       = Lucia Bosé Jeanne Moreau Gérard Depardieu
| music          = 
| cinematography = Ghislain Cloquet
| editing        = Nicole Lubtchansky
| distributor    = 
| released       = October 6, 1972
| runtime        = 83 minutes
| country        = France 
| language       = French
| budget         = 
| preceded_by    = 
| followed_by    = 
}} 1972 cinema French drama film directed by Marguerite Duras.

==Cast==
* Lucia Bosé - Isabelle (as Lucia Bosè)
* Jeanne Moreau - Other Woman
* Gérard Depardieu - Salesman
* Luce Garcia-Ville - Teacher
* Valerie Mascolo - Nathalie Granger
* Nathalie Bourgeois - Laurence
* Dionys Mascolo - Granger

==External links==
*  
*  

 

 
 
 
 
 
 
 

 