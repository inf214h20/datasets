In America (film)
 
 
{{Infobox film
| name           = In America
| image          = In_America_movie.jpg
| alt            =
| caption        = Theatrical release poster
| director       = Jim Sheridan
| producer       = Jim Sheridan Arthur Lappin
| writer         = Jim Sheridan Naomi Sheridan Kirsten Sheridan
| narrator       = Sarah Bolger
| starring       = Samantha Morton Paddy Considine Sarah Bolger Emma Bolger Djimon Hounsou
| music          = Gavin Friday Maurice Seezer
| cinematography = Declan Quinn
| editing        = Naomi Geraghty
| studio         = Hells Kitchen Films East of Harlem (UK) Ltd.
| distributor    = Fox Searchlight Pictures
| released       =      
| runtime        = 105 minutes
| country        = Ireland  United States  United Kingdom
| language       = English
| budget         =
| gross          = $25,382,911 
}} drama film Kirsten focuses on an immigrant Irish familys struggle to start a new life in New York City, as seen through the eyes of the elder daughter.
 Best Original Best Actress Best Supporting Actor for Djimon Hounsou.

==Plot== tourist visa drug addicts, Roman Catholic Johnny questions God and has lost any ability to feel true emotions, which has affected his relationship with his family. Christy believes she has been granted three wishes by her dead brother, which she only uses at times of near-dire consequences for the family as they try to survive in New York.
 cab driver to augment their income and help pay for the girls Catholic school tuition.

On Halloween, the girls become friendly with Mateo when they knock at his door to Trick-or-treating|trick-or-treat. Despite Johnnys reticence about the somewhat imposing and forbidding man, Sarah invites him to their apartment for dinner, and eventually they learn that the man is sad and lonely because he is dying of AIDS.
 born prematurely and in poor health, and is in need of a blood transfusion. Johnny and Sarah are ultimately nervous not only about the babys survival chances, but also of the skyrocketing hospital bills that will now need to be paid following the babys delivery, causing Sarah to have a brief nervous breakdown and blame Johnny for Frankies death, and tearfully berates him.

However, after calming her down, Johnny and Sarah agree to the blood transfusion but without giving the baby "bad blood," as using hospital blood banks was the source of Mateos contraction of HIV. Shortly, it is discovered that Christy has a compatible blood type to donate with, and Mateos death coincides with the first healthy movements of the infant following a blood transfusion from Christy. After the successful operation, the family is startled to learn that Mateo had settled and paid for their astronomical hospital bill before he had died, upon the discovery that Mateo was in possession of a large trust fund he never spent. In return for his generous act, they give the newborn baby girl the middle name of Mateo in gratitude and to honour his memory.

With the birth of the new baby and the death of Mateo, Johnny finally is able to overcome his lack of emotion and put his grieving for Frankie to rest. He also finally catches a break by getting a small role in A Chorus Line on Broadway.  The film ends after a baby shower at the apartment is held for the Sullivan family with many of the apartment tenants present to celebrate, and Christy and the rest of her family overlook the view of the city and look out for Mateo in the nights sky.

==Cast==
* Paddy Considine as Johnny Sullivan
* Samantha Morton as Sarah Sullivan
* Sarah Bolger as Christy Sullivan
* Emma Bolger as Ariel Sullivan
* Djimon Hounsou as Mateo Kuamey

==Production==
The film is dedicated to director/screenwriter Jim Sheridans brother Frankie, who died at the age of ten. In The Making of in America, a featurette on the DVD release of the film, Sheridan explains Christy and Ariel are based on his daughters (and co-writers) Naomi and Kirsten. He says they wanted to make a film showing how people can learn to overcome their pain and live for the future instead of dwelling on the sadness of the past.
 8th Street East Village.

Interiors were filmed at the Ardmore Studios in County Wicklow in Ireland. The fairground scene was filmed on Parnell Street, Dublin.

The soundtrack includes songs performed by The Lovin Spoonful, Culture Club, The Corrs, The Byrds, Kid Creole and The Coconuts, Evan Olson, and The Langhorns.

==Release==
The film premiered at the 2002 Toronto International Film Festival. In 2003, it was shown at the Sundance Film Festival, the Boston Irish Film Festival, the Tribeca Film Festival, the Edinburgh Film Festival, the Hamburg Film Festival, the Warsaw Film Festival, the Dinard Festival of British Cinema, and the Austin Film Festival.

===Box office===
The film opened in the UK on 31 October 2003, where it earned £284,259 on its opening weekend. It opened in limited release in the US on 26 November. It eventually grossed $15,539,656 in the US and $9,843,255 in foreign markets, for a total worldwide box office of $25,382,911   

===Critical reception===
In his review in the New York Times, A.O. Scott called it a "modest, touching film" and added, "Many of   elements . . . seem to promise a sticky bath of shameless sentimentality. But instead, thanks to Jim Sheridans graceful, scrupulously sincere direction and the dry intelligence of his cast, In America is likely to pierce the defenses of all but the most dogmatically cynical viewers . . . Mr. Sheridan is more interested in particular people than in general plights, and what lingers in the mind after you have seen his movies is the rough, radiant individuality of his characters . . . This movie, from moment to moment, feels small, almost anecdotal. It is only afterward that, like Mr. Sheridans other films, it starts to grow into something at once unassuming and in its own way grand." 

Roger Ebert of the Chicago Sun-Times observed, "In America is not unsentimental about its new arrivals (the movie has a warm heart and frankly wants to move us), but it is perceptive about the countless ways in which it is hard to be poor and a stranger in a new land." 

In the San Francisco Chronicle, Walter Addiego stated, "I fought hard against the emotionalism of In America . . . but I lost. Theres no questioning the directors ability to wring moving moments from potentially sentimental and decidedly familiar material: the story of penniless immigrants trying to make it in Manhattan. It got to me. Im still trying to decide whether I was won over or worn down — but why not give Sheridan the benefit of the doubt? . . .   is clearly drawing on deep personal reserves for this picture, and despite a few sequences when the creative hand seems intrusive, does well by his subject. When you see a director going for that lump-in-the-throat mood, instinct takes over and you want to dig in your heels. Sometimes its best just to let yourself be swept away." 

Peter Travers of Rolling Stone rated the film three out of a possible four stars, calling it "forceful, funny and impassioned" and "an emotional wipeout". 

In the St. Petersburg Times, Steve Persall graded the film A and added, "This is a tearjerker for all the right reasons. Because its delicately manipulative and the characters are so precisely emotional. And because Sheridans manner with the material makes crying seem like a cleansing, an affirmation that something so simple and sweet can still move us . . . I loved this unassuming, heartfelt little gem, even if I couldnt stop sobbing for an hour after the show. Its just so beautiful." 

Claudia Puig of USA Today called it "touching, but not cloying, uplifting and hopeful but never sappy and also just plain funny. There is not a false note among the five core performances, nor a false word in Sheridans script. In America is a classic story of losing and finding faith told with heart, humor and emotional heft." 

In The Observer, Philip French said, "The movie lacks conviction from implausible beginning to sentimental end." 

Rotten Tomatoes gave the film a score of 89%. 

==Accolades==
;Wins
* Independent Spirit Award for Best Supporting Actor – Djimon Hounsou
* Independent Spirit Award for Best Cinematography – Declan Quinn
* Satellite Award for Best Film - Drama
* Satellite Award for Best Director – Jim Sheridan
* Satellite Award for Best Supporting Actor - Motion Picture – Djimon Hounsou American Film Institute Audience Award for Best Feature Film Bangkok International Film Festival Golden Kinnaree Award for Best Director Black Reel Award for Best Supporting Actor – Motion Picture – Djimon Hounsou
* BFCA Critics Choice Award for Best Writer
* National Board of Review Award for Best Original Screenplay Producers Guild of America Stanley Kramer Award
* Phoenix Film Critics Society Award for Best Original Song – "Time Enough for Tears"
* Young Artist Award for Best Performance in a Feature Film – Young Actress Age Ten or Younger – Emma Bolger
* Flanders International Film Festival Grand Prix Award – Jim Sheridan
* San Diego Film Critics Society Award for Best Supporting Actor – Djimon Hounsou
* Phoenix Film Critics Society Award for Best Original Screenplay
* Phoenix Film Critics Society Award for Best Performance by a Youth in a Lead or Supporting Role – Female – Sarah Bolger

;Nominations
* Academy Award for Best Actress – Samantha Morton
* Academy Award for Best Supporting Actor – Djimon Hounsou Academy Award for Best Original Screenplay
* Golden Globe Award for Best Screenplay
* Golden Globe Award for Best Original Song – "Time Enough for Tears"
* Independent Spirit Award for Best Film
* Independent Spirit Award for Best Director
* Independent Spirit Award for Best Actress – Samantha Morton
* Independent Spirit Award for Best Supporting Actress – Sarah Bolger
* Satellite Award for Best Actor - Motion Picture Drama – Paddy Considine
* Satellite Award for Best Actress - Motion Picture Drama – Samantha Morton
* Satellite Award for Best Supporting Actress - Motion Picture – Emma Bolger
* Screen Actors Guild Award for Outstanding Performance by a Cast in a Motion Picture
* NAACP Image Award for Outstanding Supporting Actor in a Motion Picture – Djimon Hounsou British Independent Film Award for Best Actor – Paddy Considine
* British Independent Film Award for Best Actress – Samantha Morton
* British Independent Film Award for Best Director – Jim Sheridan
* Broadcast Film Critics Association Award for Best Film
* BFCA Critics Choice Award for Best Actress – Samantha Morton
* Broadcast Film Critics Association Award for Best Director – Jim Sheridan
* BFCA Critics Choice Award for Best Young Performer – Sarah Bolger and Emma Bolger
* Broadcast Film Critics Association Award for Best Song – "Time Enough for Tears"
* Phoenix Film Critics Society Award for Best Film
* Phoenix Film Critics Society Award for Best Director – Jim Sheridan
* Writers Guild of America Award for Best Original Screenplay
* Phoenix Film Critics Society Award for Best Performance by a Youth in a Lead or Supporting Role – Female – Emma Bolger

==References==
 

==External links==
*  
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 