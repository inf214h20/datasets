Coogan's Bluff (film)
 
{{Infobox film
| name           = Coogans Bluff
| image          = CoogansBluff1968MoviePoster.jpg
| caption        = film poster
| director       = Don Siegel
| producer       = Don Siegel Herman Miller
| starring       = Clint Eastwood Lee J. Cobb Susan Clark Don Stroud
| music          = Lalo Schifrin
| cinematography = Bud Thackery
| editing        = Sam E. Waxman
| distributor    = Universal Pictures
| released       =  
| runtime        = 93 min.
| country        = United States
| language       = English
| budget         = $3 million 
| gross          = $3,110,000 
}} Escape from Alcatraz (1979).

Eastwood plays the part of a young veteran deputy sheriff from a rural county in Arizona who travels to New York City to extradite an apprehended fugitive named Jimmy Ringerman, played by Stroud, who is wanted for murder.
 New York Giants baseball club, the Polo Grounds, with a double-meaning derived from the name of the lead character. The television series McCloud (TV series)|McCloud, starring Dennis Weaver, was loosely based on this story.

==Plot==
Arizona deputy sheriff Walt Coogan is sent to New York City to extradite escaped killer James Ringerman. Detective Lieutenant McElroy informs him that Ringerman is recovering from an overdose of LSD, cannot be moved until the doctors release him, and that Coogan needs to get extradition papers from the New York State Supreme Court.

Coogan flirts with probation officer Julie Roth, then bluffs his way to Ringerman, tricks the attendants into turning him over, and sets out to catch a plane for Arizona. Before he can get to the airport, Ringerman’s girlfriend Linny and a tavern owner named Pushie ambush Coogan and enable Ringerman to escape. Detective McElroy is furious. Coogan learns Linnys name and obtains her address from the Julie Roths home files

He tracks Linny to a nightclub and after some time she offers to lead him to Ringerman. Instead she takes Coogan to a pool hall where he is attacked by Pushie and a dozen men in a bloody battle. Coogan holds his own for a while but is eventually overpowered. After hearing sirens the men take off, but not before the beaten Coogan kills Pushie and two others. Detective McElroy finds the bar in pieces and a cowboy hat on the floor.

Coogan finds Linny and threatens to kill her if she does not lead him to Ringerman. She takes him to Ringerman, who is armed with a gun stolen from Coogan. Ringerman gets away on his motorcycle and Coogan commandeers the bike of an unlucky motorcyclist. Coogan gives chase through the park and eventually captures Ringerman.

He hands the fugitive over to McElroy, who once again tells him to go to the DAs office and to let "the system handle this". Some time later Coogan, with Ringerman in cuffs, prepares to leave for the airport via helicopter. Inside the helicopter, Coogan lights a cigarette, and seeing that Ringerman wants a smoke, lights one up for him too.  Coogans last view is Julie Roth waving goodbye from the helipad.

==Cast==
* Clint Eastwood as Deputy Sheriff Walt Coogan
* Lee J. Cobb as Det. Lt. McElroy, NYPD
* Susan Clark as Julie Roth, Probation Officer
* Tisha Sterling as Linny Raven, Ringermans Girlfriend
* Don Stroud as James Ringerman
* Betty Field as Ellen Ringerman
* Tom Tully as Sheriff McCrea, Piute County
* Melodie Johnson as Millie, Coogans Girlfriend James Edwards as Sgt. Wallace, Stakeout Cop
* Rudy Diaz as Running Bear David Doyle as Pushie, Tavern Owner
* Louis Zorich as Taxi Driver
* Meg Myles as Big Red
* Marjorie Bennett as Mrs. Fowler, Little Old Lady
* Seymour Cassel as Joe, Young Hood
* Albert Popwell as Wonderful Digby
* Skip Battin as Omega

==Production==
Before Hang Em High had been released, Eastwood had set to work on Coogans Bluff, a project which saw him reunite with Universal Studios after an offer of $1 million, more than doubling his previous salary. McGillagan (1999), p.165   Jennings Lang was responsible for the deal. Lang was a former agent of Don Siegel, a Universal contract director who was invited to direct Eastwoods second major American film. Eastwood was not familiar with Siegels work but Lang arranged for them to meet at Clints residence in Carmel. Eastwood had seen three of Siegels earlier films, was impressed with his directing and the two became friends, forming a close partnership in the years that followed. McGillagan (1999), p.167 
 Herman Miller and Jack Laird, screenwriters for Rawhide. McGillagan (1999), p.166  It is about a character named Sheriff Walt Coogan, a lonely deputy sheriff working in New York City.
 Mojave desert.  However, Eastwood surprised the team one day by calling an abrupt meeting and professed to strongly dislike the script, which by now had gone through seven drafts, preferring Herman Millers original concept.  This experience would also shape Eastwoods distaste for redrafting scripts in his later career. 

Eastwood and Siegel hired a new writer, Dean Riesner, who had written for Siegel in the Henry Fonda TV film Stranger on the Run.  Eastwood did not communicate with the screenwriter until one day Riesner criticized a scene Eastwood had liked which involved Coogan having sex with Linny Raven in the hope that she would take him to her "boyfriend." According to Riesner, Eastwoods "face went white and gave me one of those Clint looks". McGillagan (1999), p.169 

The two soon reconciled their differences and worked on a script in which Eastwood had considerable input. Don Stroud was cast as the psychopathic criminal Coogan is chasing, Lee J. Cobb as the disagreeable New York City Police Department lieutenant, Susan Clark as a probation officer who falls for Coogan and Tisha Sterling as the drug-using lover of Strouds character.  Filming began in November 1967 even before the full script had been finalized. 

==Reception== macho hero that Eastwood would play in the Dirty Harry films. The script of the film foreshadows the McCloud (TV series)|McCloud television series that starred Dennis Weaver.

==Home media releases==
The DVD version of Coogans Bluff is edited by approximately three minutes in all regions for unknown reasons. The missing scenes include Coogan receiving his assignment to return Ringerman from New York, a short scene in a hospital, and a scene in which Julie talks about Coogans Bluff, a lookout point over the ocean near New York (the real Coogans Bluff is a site on Manhattan Island between Washington Heights and Harlem), tying the location into the films title. The earlier video release did not have these edits, and was released uncut.

==See also==
* New York Airways
* Pan Am Building

==References==
 

===Bibliography===
*  
*  
*  

==External links==
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 