The Strangers in the House (film)
{{Infobox film
| name           = Les Inconnus dans la maison
| image          = Les Inconnus dans la maison.jpg
| image_size     = 
| caption        = Film poster
| director       = Henri Decoin
| producer       = Continental Films
| writer         = Henri-Georges Clouzot, after Georges Simenons novel The Strangers in the House
| starring       = Raimu Marcel Mouloudji Martine Carol Daniel Gélin Noël Roquevert
| music          = Roland-Manuel
| cinematography = Jules Kruger
| editing        = 
| distributor    = 
| released       =  
| runtime        = 95 minutes
| country        = France
| language       = French
| budget         = 
}}
The Strangers in the House (Les Inconnus dans la maison) is a 1942 French drama film by Henri Decoin after the novel by the same name published by Georges Simenon in 1940.

==Cast==
* Raimu : Hector Loursat
* André Reybaz as Émile Manu
* Tania Fédor as Marthe Dossin
* Héléna Manson as Mme Manu
* Gabrielle Fontan as Fine, the old maid
* Marcel Mouloudji as Amédée or Ephraïm Luska (Marcel Mouloudjy)
* Noël Roquevert as commissaire Alfred Binet
* Jacques Grétillat as court president
* Martine Carol as a court spectator (uncredited)
* Jean Négroni
* Daniel Gélin
* Marguerite Ducouret as Angèle, the cook
* Lucien Coëdel as the bar tenant
* Marc Doelnitz as Edmond Dossin
* Juliette Faber as Nicole Loursat
* Jacques Baumer as the attorney Rogissart
* Génia Vaury as Mme Laurence Rogissart
* Jean Tissier as judge Ducup
* Raymond Cordy as a huissier
* Lucien Bryonne as a police officer
* Fernand Flament as a police officer
* Paul Barge as prison gard (uncredited)
* Langlois as uncle Daillat (uncredited)
* Henri Delivry
* Jacques Denoël
* Lise Donat
* Franck Maurice
* Bernard Noël
* Claire Olivier
* Max Révol
* Pierre Ringel
* Yvonne Scheffer
* Simone Sylvestre
* Charles Vissières
* Pierre Fresnay : narrator

== External links ==
*  

 
 
 

 