Dus (1997 film)
{{Infobox film
| name     = Dus
| image    = Dus_poster.jpg
| caption  = Promotional poster.
| director = Mukul S. Anand
| producer = Mukul S. Anand Nitin Manmohan
| writer   = Mukul S. Anand
| starring = Sanjay Dutt Salman Khan Raveena Tandon Shilpa Shetty
| music    = Shankar Ehsaan Loy Sandeep Chowta
| released = Unreleased
| country  = India
| language = Hindi
}}

: For the 2005 Bollywood film see Dus. 1997 unfinished unfinished Bollywood action film directed by Mukul S. Anand, who died during filming, leaving the film incomplete. The film starred Sanjay Dutt, Salman Khan, Raveena Tandon and Shilpa Shetty in leading roles, with Vinod Khanna and Rahul Dev in supporting roles.  Actress Raveena Tandon was playing her first negative role in this film.

==Plot==
The storyline was intended to have been about terrorism and how an anti-terrorist intelligence officer Captain Raja Sethi (Sanjay Dutt) who is part of a secret organisation known as Force 10 is sent on a mission to find an Afghan terrorist named Nasheman (Raveena Tandon). After he is captured and tortured by Nasheman and her right hand man Mast Gul (Rahul Dev), his partner Captain Jeet Sharma (Salman Khan) is sent to rescue him. However by now, Raja Sethi is brainwashed by Mast Gul in believing his country and his friends has abandoned him and he too turns to terrorism. Shilpa Shetty played Salmans love interest and Vinod Khanna played a General.

==Cast==
*Sanjay Dutt as Captain Raja Sethi
*Salman Khan as Captain Jeet Sharma
*Raveena Tandon as Nasheman
*Shilpa Shetty as Journalist
*Vinod Khanna as General
*Rahul Dev as Mast Gul

==Production and abandonment==
The shooting of the film began in Utah in May and continued until the end of August. The scenes in Utah were to have depicted scenes of Kashmir which was the major setting for the film. This was the debut film of Rahul Dev who played Mast Gul. Gul was based a real-life Pakistani militant leader, who used the alias name Mast Gul.  

Around 40% of the films shooting had been completed when Mukul S. Anand suddenly died on 7 September 1997  The film was subsequently shelved and was never completed or released. The song promos of the film are still available to view. Several years after the film was shelved, Mukul Anands brother Rahul announced he would revive the incomplete film but this never materialised. 

==Soundtrack==
{{Infobox album
| Name = Dus - A Tribute to Mukul Anand
| Type = soundtrack
| Artist = Shankar-Ehsaan-Loy
| Cover =
| Released =   1999 (India)
| Recorded = Feature film soundtrack
| Length = Tips
| Producer = Ram Gopal Varma
| Reviews =
| Last album = 
| This album = Dus (1997)
| Next album = Shool (1999)
}}
The music of this film was composed by Shankar-Ehsaan-Loy who made their debut as music directors with this film. Although the film was never completed and released, the album was released in 1999. There were seven songs composed for the film.
Piya and Version 2 of Dus was composed by Sandeep Chowta.

{| border="2" cellpadding="3" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Track # !! Song !! Singer(s)
|- Shankar Mahadevan, Hema Sardesai 
|- Udit Narayan, Mahalakshmi Iyer, Shankar Mahadevan, Dominique Cerejo
|- Sur Mile Asha Bhosle, Udit Narayan, Shankar Mahadevan
|- Sukhwinder Singh, Shankar Mahadevan
|- Chandni Roop Shankar Mahadevan
|- Sujatha Mohan 
|- Dus (Version 2)||Saumya, Anupama Deshpande 
|-
|}

In 2007, Raja Hasan sang a song "Mahiya" from the film in a performance in Sa Re Ga Ma Pa Challenge 2007. The performance was very well appreciated and it suddenly brought limelight to the movie that remained unfinished. 

==New film==
In 2005 Nitin Manmohan who was the co producer of Dus released a new action thriller film titled Dus in which Sanjay Dutt and Shilpa Shetty from the original cast returned. This film also had the same premise of terrorism but otherwise was an altogether different film.

==References==
 

==External links==
* 

 
 
 
 
 