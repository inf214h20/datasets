Our Winning Season
{{Infobox film
| name           = Our Winning Season
| image          = Our Winning Season 1978.jpg
| image_size     =
| alt            =
| caption        = Movie Poster
| director       = Joseph Ruben
| producer       = Samuel Z. Arkoff Joe Roth
| writer         = Nicholas Niciphor
| narrator       =
| starring       = Dennis Quaid Scott Jacoby Joanna Cassidy Charles Fox
| cinematography = Stephen M. Katz
| editing        = Bill Butler
| studio         =
| distributor    = American International Pictures
| released       =  
| runtime        = 92 min.
| country        = United States English
| budget         =
| gross          =
}}
 1978 film directed by Joseph Ruben from a screenplay by Nicholas Niciphor.

==Taglines==
Its your story.

Your senior year lasts the rest of your life.

==Plot==
 
  track star.

==Principal cast==
{| class="wikitable" width="50%"
|- bgcolor="#CCCCCC"
! Actor !! Role
|-
| Dennis Quaid|| Paul Morelli
|- David Wakefield
|-
| Deborah Benson|| Alice Baker
|-
| Robert Wahler || Burton Fleishauer
|-
| Joe Penny || Dean Berger
|-
| Jan Smithers || Cathy Wakefield
|-
| Joanna Cassidy || Shiela
|- Cindy Hawkins
|}

== External links ==
*  
*  

 

 
 
 
 
 
 
 
 
 
 


 