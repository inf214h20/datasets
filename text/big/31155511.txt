Pennmakkal
{{Infobox film 
| name           = Pennmakkal
| image          =
| caption        =
| director       = J. Sasikumar
| producer       = KP Kottarakkara
| writer         = KP Kottarakkara
| screenplay     = KP Kottarakkara
| starring       = Prem Nazir Jayabharathi Manavalan Joseph Sobha
| music          = MS Baburaj
| cinematography = P Balasubramaniam
| editing        = M Umanath
| studio         = Ganesh Pictures
| distributor    = Ganesh Pictures
| released       =  
| country        = India Malayalam
}}
 1966 Cinema Indian Malayalam Malayalam film,  directed by J. Sasikumar and produced by KP Kottarakkara. The film stars Prem Nazir, Jayabharathi, Manavalan Joseph and Sobha in lead roles. The film had musical score by MS Baburaj.   

==Cast==
*Prem Nazir
*Jayabharathi
*Manavalan Joseph
*Sobha
*Friend Ramaswamy
*Kottarakkara Sreedharan Nair Meena
*Susheela

==Soundtrack==
The music was composed by MS Baburaj and lyrics was written by Vayalar Ramavarma.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Chethi Mandaaram Thulasi || P. Leela, B Vasantha, B Savithri || Vayalar Ramavarma || 
|-
| 2 || Daivathinu Praayamaayi || MS Baburaj || Vayalar Ramavarma || 
|-
| 3 || Ee Nalla Raathriyil || K. J. Yesudas, B Vasantha || Vayalar Ramavarma || 
|-
| 4 || Ellaam Shoonyam Brahmam || MS Baburaj || Vayalar Ramavarma || 
|-
| 5 || Kaalan Kesavan || P. Leela, Kamukara, Parameswaran || Vayalar Ramavarma || 
|-
| 6 || Oramma Petta || S Janaki, P. Leela || Vayalar Ramavarma || 
|-
| 7 || Pottithakarnnu || Kamukara || Vayalar Ramavarma || 
|-
| 8 || Pulliman Mizhi || P. Leela, Kamukara || Vayalar Ramavarma || 
|}

==References==
 

==External links==
*  

 
 
 


 