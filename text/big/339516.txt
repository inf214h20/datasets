The Amazing Colossal Man
{{Infobox film
| name = The Amazing Colossal Man
| image = The Amazing Colossal Man.jpg
| caption = film poster by Albert Kallis
| director = Bert I. Gordon Mark Hanna Bert I. Gordon George Worthing Yates (uncredited) based on = novel The Nth Man by Homer Eon Flint (uncredited)
| producer = Bert I. Gordon Samuel Z. Arkoff James H. Nicholson William Hudson
| cinematography = Joseph F. Biroc
| music = Albert Glasser
| costume = Bob Richards
| studio = Malibu Productions
| distributor = American International Pictures
| released =  
| runtime = 80 mins
| country = United States
| language = English
| gross = $848,000 (US) McGee 1996 pp. 105–108.  
}}

The Amazing Colossal Man (aka The Colossal Man) is a 1957 black-and-white science fiction film, directed by Bert I. Gordon and starring Glenn Langan. The film revolves around a man who grows to over 60 feet tall as the result of an atomic accident. It is an uncredited adaptation of the 1928 Homer Eon Flint short novel The Nth Man. 

During the 1960s the title was syndicated to television by American International Television. Both The Amazing Colossal Man and its sequel, War of the Colossal Beast (1958) appeared on Mystery Science Theater 3000.

==Plot== plutonium bomb Desert Rock, Nevada. When the chain reaction for the charge fails its cycle and does not detonate as expected, Lt. Glenn Manning (Glenn Langan) receives orders to keep his men in the protective trench as an explosion is still imminent but its timing is now unknown. Moments later, an unidentified small civilian aircraft crash-lands near the bomb site, and Glenn leaps from the trench and runs into the detonation area to rescue the pilot. Once in the detonation area, the bomb goes off and Glenn is caught in an atomic blast that burns him and immediately disintegrates his clothes and hair. 
 William Hudson) Las Vegas, anxiously awaits a prognosis, but Linstrom refrains from telling her that the consensus is that Glenn will not survive. 

The next morning, however, Linstrom and Coulter are stunned to discover that Glenn’s burns have completely healed, without so much as scarring. They are concerned over the incredible recuperation and begin analyzing the details of Glenn’s unusual case. That same evening, Forrest is upset when Glenn’s security officer informs her that she is prohibited from seeing Glenn again, and she later learns that he has been moved out of the hospital to another facility - an army rehabilitation and research center in Summit, Nevada. Forrest drives out and is admitted entry, and after overhearing Linstrom and Coulter discussing Glenn’s case, Carol lets herself into Glenn’s room. She faints in horror when she sees him lying on a large pad on the floor, mutated into a giant about 16 feet tall.  

Later, Linstrom tells Carol that it is believed that Glenns exposure to the plutonium blast has caused his old cells to stop dying and his new cells to multiply at an accelerated rate, resulting in his growing proportionately 8 feet in height a day. Linstrom admits that he and Coulter do not know if they can stop Glenns growth – and that if they don’t, he will continue to grow until he dies - but assures her that they will not stop their research. 

The following day, Glenn awakens after unsettling dreams of his past with Carol, his time in the Korean War, and the night he was injured in the plutonium blast. Upon realizing his massive size, Glenn is at first frightened, then deeply disturbed. Carol sees him the next morning to comfort him, but he – now over 22 feet tall – is distant and morose. While the public knows that Glenn survived the explosion, the military has kept the truth of his condition from the media.

As Glenn’s body continues to increase in size, Linstrom orders him moved to a tent large enough to provide shelter and recommends that Carol spend time with him. However, despite Carols encouragement, Glenn is angry and bitter. Linstrom eventually reveals that Glenns heart is growing at only half the rate of his body and soon will be unable to support his enormous size – and he will die in a matter of days. That night, Carol and Glenn argue over his continuing despair and pessimism. 
 Highway 93, the military, headed by Col. Hallock (James Seay), conducts a 10-mile-wide search for the now over 50-foot tall Glenn, but with no results. When Carol asks Linstrom if she can help in their search for Glenn, he cautions her that Glenns condition may be affecting his mind. Later, Coulter declares that he has created a special syringe filled with a serum for Glenns bone marrow that will stop his growth. Hallock then briefs the military squads on their expanding search, which will also be conducted by air. 
 Riviera Hotel Royal Nevada, Silver Slipper Sands hotel Pioneer Club’s Vegas Vic sign and heads toward Boulder Dam as military helicopters track his movements. 

Linstrom, Carol and Coulter, who are also traveling by helicopter, attempt to intercept Hallocks troops. After landing at the dam, Coulter and Linstrom immediately take the enormous syringe and plunge it into Glenns ankle. Glenn is startled, then outraged and, removing the syringe, mercilessly spears Coulter with it. Glenn then picks up Carol and starts across the dam. Using a bullhorn to amplify his message, Linstrom pleads with Glenn to spare Carol and although he is utterly disoriented, Glenn complies. When Glenn reaches the middle of the dam, Hallock orders his men to fire on him and Glenn tumbles into the Colorado River to his death.

==Cast==
{| class="wikitable"
! Actor
! Role
|-
| Glenn Langan || Lt. Glenn Manning (credited as Glen Langan)
|-
| Cathy Downs || Carol Forrest
|- William Hudson || Dr. Paul Linstrom
|-
| Larry Thor || Maj. Eric Coulter, MD
|-
| James Seay || Col. Hallock
|-
| Frank Jenks || Truck Driver
|-
| Russ Bender || Richard Kingman
|-
| Hank Patterson || Henry
|-
| Jimmy Cross || Sergeant at reception desk
|-
| June Jocelyn || Nurse Wilson
|-
| Stanley Lachman || Lt. Cline
|-
| Harry Raybould || MP at Main Gate
|-
| Jean Moorhead || Woman in Bathtub
|-
| Scott Peters || Sgt. Lee Carter
|-
| Myron Cook || Capt. Thomas
|-
| Michael Harris || Police Lt. Keller
|-
| Bill Cassady || Lt. Peterson
|-
| Dick Nelson || Sgt. Hansen
|-
| Edmund Cobb || Dr. McDermott
|-
| Paul Hahn || Attendant
|-
| Diana Darrin || Hospital Receptionist
|-
| Lyn Osborn || Sgt. Taylor
|-
| Jack Kosslyn || Lieutenant in briefing room
|-
| William Hughes || Bombsite Control Officer
|- Newscaster
|-
| John Daheim]|| Soldier (uncredited)
|-
| Judd Holdren || Robert Allen (uncredited)
|-
|  Harold Miller]|| Official (uncredited)
|}

==Production==
Jim Nicholson of American International Pictures had the rights to Homer Eon Flints 1928 novel, The Nth Man about a man who was 10 miles high. Nicholson thought it could be adapted to cash in on the success of The Incredible Shrinking Man (released six months earlier in 1957) and originally announced Roger Corman as director. Charles B. Griffith was hired to adapt the novel and he turned it into a comedy. Then Corman dropped out and Bert I. Gordon was hired. Gordon worked on the script with Griffith but the collaboration only lasted a day before Griffith quit. Instead, Griffiths regular writing partner, Mark Hanna stepped in. 

Before Gordon became involved, the film was conceived with Dick Miller in mind for the lead. It was Gordons first movie for AIP. Smith 2009,  p. 11.  Principal photography began late in June 1957. 

==Reception==
Distributed by  , noted: "... Glenn Langan delivers persuasively ... Technical departments are well handled." Holston and Winchester 1997, p. 30. 

In more recent popular internet reviews, on Rotten Tomatoes, The Amazing Colossal Man has had more negative reviews then positive reviews. It holds a 38% from critics and a low 24% from the audience.  On the Internet Movie Database, the film has a low score of 4.2/10. 

==In popular culture== its sequel Mike Nelson portrayed the title character twice in the mid-movie host sections of the shows season 3, episodes 9 and 19. On episode 9, the character seems more aggressive to Joel and the bots when the Satellite of Love hit him and nearly proceeds to attack the trio after Tom Servo unintentionally insulted Glen before leaving when suffering from a brief heart attack, as portrayed in the film.

The Amazing Colossal Man was parodied on Season 1, episode 2, of Robot Chicken in 2005 when a large bald giant, wearing a sarong as a diaper, is struck in the crotch with a wrecking ball as he terrorizes a city, as part of the "Ode to the Nut Shot" sketch. 

In Gordons 1958 film, Attack of the Puppet People, released about six months later, a clip of this film is shown at a drive-in movie theater.

==Home media==
RCA Columbia Home Video released the film on VHS on June 21, 1994.  The Mst3k version was released on VHS by Rhino Home Video on April 30, 1996.

==See also==
* The Incredible Shrinking Man, a 1957 film
* Attack of the 50 Foot Woman, a 1958 film
* The Incredible Melting Man, a 1977 film

==References==
Notes
 

Bibliography
 
* Holston, Kim R. and Tom Winchester. Science Fiction, Fantasy and Horror Film Sequels, Series and Remakes: An Illustrated Filmography. Jefferson, North Carolina: McFarland & Company, 1997. ISBN 978-0-7864-0155-0.
* McGee, Mark. Faster and Furiouser: The Revised and fattened Fable of American International Pictures. Jefferson, North Carolina: McFarland & Company, 1996. ISBN  978-0-7864-0137-6. 
* Smith, Gary A. The American International Pictures Video Guide. Jefferson, North Carolina: McFarland & Company, 2009. ISBN 978-0-7864-3309-4.
* Wingrove, David. Science Fiction Film Source Book. London: Longman Group Limited, 1985. ISBN 978-0-5828-9310-8.
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 