Nana (1926 film)
{{Infobox film
| name           = Nana
| image          = 
| image_size     = 
| caption        = 
| director       = Jean Renoir
| producer       = Pierre Braunberger
| writer         = Pierre Lestringuez Jean Renoir Émile Zola (novel)
| narrator       = 
| starring       = Catherine Hessling Werner Krauss
| music          =  	
| cinematography = 
| editing        = 
| distributor    = 
| released       = 25 June 1926
| runtime        = 150 min
| country        = France French
| budget         = 
| preceded_by    = 
| followed_by    = 
}} novel by Émile Zola. 

==Plot==
A government official, Count Muffat, falls under the spell of Nana, a young actress. She becomes his mistress, living in the sumptuous apartment which he provides for her. Instead of elevating herself to Muffats level, however, Nana drags the poor man down to hers - in the end, both lives have been utterly destroyed.

==Production==
The film stars Renoir’s wife, Catherine Hessling, in an eccentric performance as the flawed heroine Nana.

Jean Renoir’s film is a fairly faithful adaptation of Émile Zola’s classic novel.  The film’s extravagances include two magnificent set pieces – a horse race and an open air ball.  The film never made a profit, and the commercial failure of the film robbed Renoir of the opportunity to make such an ambitious film again for several years.

==Cast==
* Catherine Hessling - Nana 	 	
* Werner Krauss - Count Muffat
* Jean Angelo - Count de Vandeuvres
* Raymond Guérin-Catelain - Georges Hugon

== References ==
*  

==External links==
*  
* 
*  

 

 
 
 
 
 
 
 
 

 