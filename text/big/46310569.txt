Tibet in Song
{{Infobox film
| name           = Tibet in Song
| image          = The filmmaker reunited with his friend.jpg
| alt            = 
| caption        = Ngawang Choephel (right) and a friend prepare a traditional song for Tibet in Song.
| director       = Ngawang Choephel
| producer       = Ngawang Choephel 
| co-producers   = Yodon Thonden, Tara Steele, Don Thompson
| writer         = Ngawang Choephel and Tara Steele	
| screenplay     = 
| starring       = 
| music          = 
| cinematography = 
| editing        = Tim Bartlett
| studio         = Guge Productions
| distributor    = New Yorker Films
| released       = 2010
| runtime        = 86 minutes
| country        = United States
| language       = English Tibetan
| budget         = 
}}

Tibet in Song is a 2010 documentary film written, produced, and directed by Ngawang Choephel. The film celebrates traditional Tibetan folk music while depicting the past fifty years of Chinese rule in Tibet, including Ngawangs experience as a political prisoner. The film premiered on September 24, 2010 in New York City. 

==Synopsis== Fulbright scholar at Middlebury College, who returns to Tibet in 1995 to videotape traditional music and dance.  The films follows his travels throughout the country recording music and understanding the impact of Chinese communist rule on Tibetan culture and everyday life. The movie contends that the Chinese authorities re-purposed traditional Tibetan music to forward their own agenda and propaganda. 

==Production==
Two months into the trip, after hed sent a batch of material back to friends in India, Chinese intelligence agents arrested Mr. Choephel and confiscated his camera, notes, and videotape. He was convicted of spying, without a trial, and sentenced to 18 years in prison. While in prison he continued his research, transcribing songs from prisoners and eventually memorizing songs after his notes were confiscated.  His mother launched a tireless campaign for his freedom, and in January 2002, he was released. 

==Reception==
The film received positive reviews from critics and the Tibetan community in exile. On January 24, 2009 the film was awarded the Special Jury Prize for World Cinema at the Sundance Film Festival and Ngawang Choephel became the first Tibetan to win an award at Sundance. 

==Accolades==
* Cinema for Peace, International Human Rights Award, 2010
* Sundance Film Festival, World Cinema Special Jury Prize, 2009

==References==
 

==External links==
*  
* 

 
 
 
 
 