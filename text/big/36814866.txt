Mother Riley Meets the Vampire
{{Infobox film
| name           =Mother Riley Meets the Vampire
| image          =Mother Riley meets the Vampire.jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| director       =John Gilling
| producer       =John Gilling Stanley Couzins
| writer         = 
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = Arthur Lucan Bela Lugosi
| music          = Lindo Southworth
| cinematography = Stanley Pavey
| editing        = 	
| studio         = Fernwood Productions
| distributor    = Renown Pictures (UK) Blue Chip Films (US)
| released       = July 1952 (UK) September 1952 (US)  1963 (US re-release)
| runtime        =74 minutes
| country        =UK
| language       = English
| budget         = 
| gross          = 
}}
 
Mother Riley Meets the Vampire, also known as Vampire Over London or My Son the Vampire  is a 1952 British comedy horror film directed by John Gilling, starring Arthur Lucan and Bela Lugosi that was filmed at Nettlefold Studios.  

This was the final film of the Old Mother Riley film series and did not feature Lucans former wife and business partner Kitty McShane who he had divorced in 1951. 

In 1963, an recut American version called My Son, the Vampire was released, featuring an introductory segment with a song by American comedian Allen Sherman.

==Plot==
Von Housen seeks to dominate the world from his headquarters in London with an army of 50,000 radar controlled robots powered by uranium.  He believes himself to be a vampire and has several young women abducted, most recently Julia Loretti who has a map to a uranium mine that he needs for his army.  

At the moment, Van Housen only has one functional robot which is shipped to him and through a mistake is shipped to Old Mother Rileys store with Mother Rileys package sent to Van Housen.  Seeing Mother Rileys address in the label, Van Housen sends his robot to abduct Mother Riley to his headquarters. 

==Cast==
*Arthur Lucan as Old Mother Riley|Mrs. Riley
*Bela Lugosi as Von Housen
*María Mercedes as Julia Loretti
*Dora Bryan as Tilly
*Philip Leaver as Anton Daschomb
*Richard Wattis as PC Freddie
*Graham Moffatt as The Yokel
*Roderick Lovell as Douglas
*David Hurst as Mugsy
*Judith Furse as Freda Ian Wilson as Hitchcock, the butler
*Hattie Jacques as Mrs. Jenks
*Dandy Nichols as Mrs. Mott
*Cyril Smith (actor) as Mr. Paine, the Rent Collector
*George Benson as Police Sergeant
*Bill Shine as Mugsys Assistant
*John Le Mesurier as Scotland Yard officer (uncredited)

==Production==
On the suggestion of producer Richard Gordon, Bela Lugosi had travelled to England to appear in a stage play of Dracula which failed.  He needed money to return to the US. Gordon persuaded fellow producer George Minter to use Lugosi in a movie in London. Arthur Lucan had made a number of Old Mother Riley movies and it was felt that Lugosis presence in the cast might give it a chance of success outside England. John Hamilton, The British Independent Horror Film 1951-70 Hemlock Books 2013 p 16-21  

Lugosi was paid $5,000 for his role. The plot was taken from Abbott and Costello Meet Frankenstein.   accessed 18 March 2014 

Gordon says that although John Gilling was credited as producer, George Minter was the real producer. Filming took four weeks. 

Richard Gordon recalled that there were plans to shoot additional scenes with Lugosi and without Arthur Lucan for the American market but the idea was never put into place.

Gordon also stated that the film emphasised that Lugosis character was not a real vampire so it would get a U certificate allowing children, who were Old Mother Rileys biggest audience, to see it. 

==Release==
The film was not a success in the box office and was not released in the US until 1963. 

It was to have been titled Carry On, Vampire for its later American early 1960s release but Anglo-Amalgamated successfully sued with the title changed to My Son, the Vampire as a tie in to Allen Shermans My Son, the Folksinger hit comedy record. 

==Notes==
 

==Reference==
* Frank J. DelloStritto and Andi Brooks, Vampire Over London: Bela Lugosi in Britain (Cult Movies Pr; 1st Edition, September 2000)

==External links==
*  
*  

 
 
 
 
 
 
 

 
 
 