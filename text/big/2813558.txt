They (2002 film)
 

{{Infobox film
| name           = They
| image          = They2002poster.jpg
| caption        = Theatrical Release Poster.
| writer         = Brendan Hood
| starring       = Laura Regan Marc Blucas Ethan Embry Dagmara Dominczyk Jay Brazeau Alexander Gould
| director       = Robert Harmon
| producer       = Ted Field Tom Engelman
| cinematography = Rene Ohashi
| music          = Elia Cmiral
| studio         = Dimension Films Focus Features Good Machine Radar Pictures
| distributor    = Miramax Films
| released       =  
| runtime        = 89 minutes
| country        = United States English
| budget         = $17 million
| gross          = $16,130,385
}}
 thriller film directed by Robert Harmon. The plot centers on a group of four adults named Julia Lund, Sam Burnside, Terry Alba, and Billy Parks and their experience with the phenomenon of night terrors and the impact they had on their lives as children and how they come back to haunt them as adults. 

The film was produced by Ted Field, Tom Engleman, and Wes Craven who served as executive producer. The film stars Laura Regan, Ethan Embry, Dagmara Dominczyk, Jay Brazeau, and Marc Blucas. The title is a reference to the fact that the creatures are only referred to as "They" as their origins are ambiguous. The film was a box office bomb and had lukewarm reviews from critics.

== Plot == apparition in his closet staring at him and pulls the covers on top of himself and turns on a flashlight. As he peeks outside the covers he is captured and spirited away by the mysterious apparition.

In present day 2002, the plot focuses on the story of a Psychology grad student named Julia Lund (Laura Regan) and the events that turned her life upside down. As a child she experienced horrifying night terrors that manifested after witnessing her father commit suicide, but has seemingly overcome the problem. She reunites with a childhood friend, a now a grown up Billy (Jon Abrahams). In the diner Billy is constantly startled by the flickering lights as he is now deathly afraid of the dark. He tells her that he believes their night terrors are caused by something otherworldly as he was kidnapped by mysterious creatures as a child and went missing for two days. He warns her to stay out of the dark, before suddenly committing suicide.

Julia stays over at her paramedic boyfriend Paul Loomis (Marc Blucas) apartment for comfort and to grieve. As Julia is sleeping she is awakened by the ringing of Pauls phone and answers, but with no response. Julia then hears the shower running and investigates but finds no one inside. A mysterious black fluid then erupts from the sink drain and frightens Julia who then opens the bathroom mirror shelf to find an alternate dimension inside with mysterious creatures. Out of curiosity she sticks her hand in only to yank it out coated with the same black matter from the drain which starts to violently break her fingers, she closes the shelf only to encounter a reflection of one of the creatures in the mirror. Paul hears her screams and comes to check on her only for Julia to viciously assault him until she realizes it is him. Puzzled, Paul brings up the possibility that she might have been sleepwalking since she does not appear to remember what happened.

At his funeral, Julia consoles Billys parents Mary and David Parks (Peter LaCroix) and meets up with two of Billys friends and roommates Terry Alba (Dagmara Dominczyk), and Sam Burnside (Ethan Embry) who slowly begin to believe his claims as they also experienced night terrors as children and suspect they are returning. Offended by Sams careless comment Julia walks away and visits Billys childhood room and discovers his drawer filled with Energizer and Duracell batteries. Terry then shows up and apologizes for Sams insensitivity and informs her that Billy used to talk a lot about Julia to her and Sam and about his experiences with the return of his night terrors and why he was obsessed with staying out of the dark hence the drawer filled with batteries.

As Julia is driving in the middle of nowhere her radio starts to malfunction and an unknown creature sprints across the windshield as the car mysteriously stops. As she is attempting to fix the problem she notices a mysterious creature in the nearby lake. Nervous, she manages to fix the car issue and as she is attempting to start the car she is startled by a vision of Billy and stumbles onto the road only to nearly get hit by an oncoming truck. Julia visits Pauls apartment for comfort only to discover him drunk with his friends Troy (Mark Hildreth) and Darren (Jonathan Cherry). Disgusted, Julia leaves his apartment instantly.

At Terry and Sams apartment the remaining trio study Billys diary to learn of his experiences. Terry and Sam then ask Julia if she has experienced any return of the night terrors, which Julia denies. Terry then realizes that Julia doesnt seem to remember the sheer terror she felt as a child and explains the origin of her night terrors which started when she was 5 years old, after witnessing her sister drown in a lake where her family would spend their summers. In one instance, she woke up screaming and her parents comforted her and put her back to sleep. Her mother checked on her a few hours later and she was gone. After searching throughout the entire house her father found her in the dog house, and as he reached in to grab her she stabbed him in the eye with a kitchen knife as she was convinced he wasnt her father and that he was some kind of demon.

Julia is at first skeptical but slowly starts to believe in her friends stories after meeting a little girl named Sarah (Jodelle Micah Ferland), one of Dr. Booths (Jay Brazeau) patients, who also suffers from night terrors which started after her mothers untimely death. Sarah claims "They" are going to get her and eat her in her horrible nightmares and the only thing that keeps them away is lights. She then starts picking at a strange mark on her arm, a similar mark that also appeared on Billys hand, Sams shoulder, and Terrys ankle.

Julia finally believes in the stories as her night terrors return and she begins to doubt her perception of the world around her. It turns out her mental illness is caused by creatures only she can see who are attempting to consume her. At a bathroom in a Chinese restaurant she discovers the mark left by "They" on her forehead and slowly pulls out a black needle from the mark and she starts to panic. She runs to her boyfriend Paul Loomis apartment out of fear. Paul, now completely convinced that Julia is insane, drugs a drink with a sleeping pill and gives it to her. Paul tells her that everything will be okay and she will soon fall asleep, and he attempts to call her psychiatrist Dr. Booth. Realizing he drugged her and knowing she cant risk falling asleep she escapes his apartment and runs to the subway station to vomit the sleeping pill on the tracks, only to get trapped in the station as the closing gates lock her in.

Being trapped she is forced to ride a train home, and she notices she is the only passenger on the train, making her uneasy. The trains lights start to flicker and the vehicle stops completely. She gets off and sees all the light bulbs burst in the train tunnel, then the train automatically starts and abandons her as the creatures assault her. Julia is continuously attacked by the creatures at the sign of darkness in the tunnel but manages to escape. She is finally discovered by a group of engineers who attempt to help her only for Julia to violently assault them with shards of glass, being convinced they are not human.

She is hospitalized at a mental institution by Dr. Booth and her boyfriend Paul Loomis where she is attacked once more and transported into the separate dimension she previously saw, only this time inside of a closet. Here she screams for help towards Dr. Booth and an orderly, both of whom cannot see her. The closet door is shut by Dr. Booth and the creatures proceed to attack her.

==Cast==
* Laura Regan as Julia Lund
** Jessica Amlee as Young Julia
* Marc Blucas as Paul Loomis
* Ethan Embry as Sam Burnside
* Dagmara Dominczyk as Terry Alba
* Jon Abrahams as Billy Parks
** Alexander Gould as Young Billy
* Jay Brazeau as Dr. Booth
* Jodelle Micah Ferland as Sarah
* Desiree Zurowski as Mary Parks
* Mark Hildreth as Troy
* Jonathan Cherry as Darren
* Peter LaCroix as David Parks
* L. Harvey Gold as Professor Crowley
* David Abbott as Professor Adkins

==Production== New Westminster, Burnaby, and Delta, British Columbia|Delta. Despite the film location the movies plot is set in New York.

==Release==
They received its theatrical premiere in the UK on November 1, 2002. The film received its U.S. premiere on November 27, 2002. The film was not released theatrically in New Zealand, Finland, Bulgaria, or Hungary; it was released direct-to-video in each of those countries.

==Home Media==
The film was released on VHS and DVD on June 10, 2003. The film received its Blu-ray release on September 11, 2012 in a double feature with another Wes Craven film Cursed (2005 film)|Cursed.

===Deleted scenes===
There were scenes that were filmed but excluded from the final cut, the first two are available on the Japanese DVD which include:
* After Julia sees Sarahs mark on her arm she leaves Dr. Booths waiting room with him walking in to find her gone. Julia is seen at a hardware store purchasing lighting supplies afterwards. The cashier asks her if she is going camping but she does not respond.
* Julia is seen packing in her bedroom to prepare for Billys funeral while Paul makes her breakfast. The two then share an intimate moment before she leaves for the funeral.
* After Julia pulls out the splinter from her gash she runs over to Sams apartment as he is being attacked by "They", after calling out his name a few times more Sams corpse is thrown through a window and lands on top of her, a monster on his back growls at Julia as it pulls his corpse into the shadows.

These deleted scenes were all included in the Blu-ray release of the film.

===Alternate Endings===
Two alternate endings were shot but neither of them made it to the final cut, they include:
* After the incident in the subway the films plot cuts to nine months later where Julia is shown hospitalized in a mental institution. Julia manages to convince a panel of psychiatrists including Dr. Booth that she has regained her sanity. She then sees one of the creatures climb through an air shaft in the ceiling but continues to deny their existence. She is finally released and proceeds to set up high powered lights all over her apartment room. The camera then pulls out of her bedroom as she is seen sitting on her bed. A door creaks open in her darkened hall and the film cuts to black. (This ending was shown to test audiences which was deleted and re-filmed after test audiences responded negatively to the ending, for some reason this ending is unavailable in any DVD.)
* Julia wakes up in the mental hospital and sees that all the people in her story − Dr. Booth, Sam, Billy, Terry - are patients in the mental hospital and her boyfriend Paul is a doctor working there. The doors to Julias room then break open and one of "them" enters and seemingly attacks her until it is realized that it was all a delusion fabricated by Julias mind and she had been suffering from Schizophrenia throughout the whole movie. (Some versions of the DVD and all Blu-Ray versions have this ending available.)

===Alternate Opening===
An alternate opening shown to test audiences featured a flashback of young Julia sleeping instead of a flashback of Billy. This opening was scrapped for some reason and is unavailable on any DVD.

==Alternate script==
The original script dealt with five recent college graduates named Julia Levin, Terry Alba, Sam Burnside, and Billy Parks who discover that earth is actually run by a race of organic machines that allow the human race to live so that they kill them and harvest their corpses for "spare parts" as their own bodies break down and deteriorate. Shortly thereafter the machines begin to eliminate the students not only killing them but using their influence and abilities to erase the memories of their family and friends making it seem like they never existed.

This draft was written by Brendan Hood and had no affiliation with the new draft despite being credited as screenwriter, everything was written out by the producers whom also hired ten new screenwriters to work on the new script. Out of all the ten screenwriters only one of them is listed in the credits.

==Reception==
===Box Office===
In its opening weekend They grossed about $5,144,090. The film grossed $12,840,842 domestically and $3,289,543 overseas making for a total worldwide gross of $16,130,385 therefore being a box office failure.

===Critical reception===
They received mixed to negative reviews currently holding a rotten 38% rating on Rotten Tomatoes with the sites consensus stating "They fails to sustain the level of creepiness necessary to rise above other movies in the horror genre". On a 0-100 scale Metacritic assigns They a 31 based on 16 reviews indicating "generally unfavorable reviews".

==See also==
* Shadow people
* Sleep paralysis
* Night terror Darkness Falls, a 2003 horror film with a similar premise to this film.

==References==
 

==External links==
*  at the Internet Movie Database
*  

 
 

 
 
 
 
 