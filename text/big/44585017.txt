Jeeo Aur Jeene Do

{{Infobox film
| name           = Jeeo Aur Jeene Do
| image          = Jeeo Aur Jeene Do.jpg
| image_size     =
| caption        = Movie Poster
| director       = Shyam Ralhan Pran
| music          = Laxmikant Pyarelal
| Lyrics         = Sahir Ludhianvi Varma Malik 
| released       =  
| country        = India
| language       = Hindi
}}

Jeeo Aur Jeene Do is a 1982 Bollywood Hindi Action Drama movie directed by Shyam Ralhan. The film stars Jeetendra, Reena Roy, Danny Denzongpa, Vijayendra Ghatge, Kader Khan, Satyendra Kapoor and Jagdeep.

== Cast ==
* Jeetendra  
* Reena Roy  
* Danny Denzongpa 
* Jagdeep  
* Vijayendra Ghatge 
* Kader Khan 
* Satyendra Kapoor 
* Raj Kiran 
* Kaajal Kiran  
* Mehmood Jr. 
* Raza Murad  
* Narendra Nath
* Nutan 
* Lalita Pawar  Pran 
* Jagdish Raj 
* Tej Sapru 
* Sharat Saxena 
* Om Shivpuri

== Music ==
Laxmikant Pyarelal gave the music for this film and Lyrics by Sahir Ludhianvi and Verma Malik.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
!S.No. !! Song!! Singer(s)
|-
|1|| "	Ali baba ali baba" || Kavita Krishnamurthy & Suresh Wadkar
|-
|2|| "Chup chup apni jaan pe" || Mohd. Rafi
|-
|3|| "Churi ki chhun chhun" || Anuradha Paudwal
|-
|4|| "Kurta malmal ka" || Asha Bhosle & Suresh Wadkar
|-
|5|| "Haathapai na karo"  || Asha Bhosle
|-
|6|| "Bulbula pani ka " || Asha Bhosle
|}

==References==
 

==External links==
 

 
 
 
 


 