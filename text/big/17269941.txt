Stalags (film)
{{Infobox film
| name           = Stalags
| image          = Stalags Poster.jpg
| caption        = Film Poster
| director       = Ari Libsker
| producer       = Barak Heymann Ari Libsker
| writer         = Ari Libsker
| starring       = 
| music          = 
| cinematography = 
| editing        = Morris Ben-Mayor
| distributor    = Film Forum
| released       =  
| runtime        = 63 minutes
| country        = Israel
| language       = Hebrew
| budget         = 
| gross          = 
}} trial of Adolf Eichmann. After the authors of the books were accused of distributing antisemitism|anti-Semitic pornography, the popularity of the books declined. The documentary opened in limited release on April 9, 2008.

==Critical reception==
The documentary received mixed reviews from critics. As of May 5, 2008, the review aggregator Rotten Tomatoes reported that 50% of critics gave the film positive reviews, based on 12 reviews.  Metacritic reported the film had an average score of 53 out of 100, based on 5 reviews. 

==References==
 

==External links==
*  
*  
*  
*  
*   at Film Forum (distributor)

 
 
 
 
 
 
 
 


 
 