Flower and Snake
{{Infobox film
| name           = Flower and Snake
| image          = Flower_and_Snake.jpg
| caption        = Poster to the Nikkatsu Roman porno film, Flower and Snake
| director       = Masaru Konuma
| producer       = 
| eproducer      =
| aproducer      =
| writer         = Oniroku Dan (novel)   Yôzô Tanaka 
| starring       = Naomi Tani   Nagatoshi Sakamoto 
| music          = Riichiro Manabe
| cinematography = Shohei Ando
| editing        = 
| distributor    = Nikkatsu
| released       = June 22, 1974 (Japan)
| runtime        = 74 minutes
| rating         =
| country        = Japan
| awards         =
| language       = Japanese
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
  aka Flowers and Serpents (1974) is a Japanese soft-core Sadism and masochism|S&M film starring Naomi Tani, directed by Masaru Konuma and produced by Nikkatsu. Based on a novel by Oniroku Dan (born 1931), Japans best-known author of S&M fiction, Flower and Snake was the first of Nikkatsu’s Roman Porno films to deal with an S&M theme. Together with the later Wife to be Sacrificed (also directed by Konuma in 1974, and starring the same female and male leads) this film is credited with starting the S&M Roman Porno series which helped save Nikkatsu from collapse during the 1970s. Macias, Patrick. (2001). TokyoScope: The Japanese Cult Film Companion. (Review of Flower and Snake by Izumi Evers). Cadence Books, San Francisco. ISBN 1-56931-681-3, p.183. 

== Background ==
In a very successful effort to avoid bankruptcy, Nikkatsu, Japans oldest major film studio, had entered the pink film, or soft-core pornography, genre three years earlier, in 1971, with its popular and critically praised Roman Porno series.  However 1974 was proving to be another difficult year for the studio, with no real box-office hits. 

Since the late 1960s, Naomi Tani had been known as the "Queen of Pink," the most popular actress in the low-budget, independent "pink films" which dominated Japans domestic cinematic market. Nikkatsu had been trying to recruit Tani into their Roman Porno series for years, but she had always refused since the studio had been reluctant to enter the S&M genre, which was Tanis specialty.  Tani finally agreed to join the studio on the condition that her first film be based on the Oniroku Dan S&M novel Flower and Snake.  Tani had already starred in a pink film version of Dans novel entitled  , released by Yamabe Pro.  

Nikkatsu agreed to Tanis conditions, but there remained the problem of convincing Oniroku Dan to allow the studio to film his novel. After lengthy, inconclusive negotiations, Dan invited director Masaru Konuma and scriptwriter Yozo Tanaka  to his home for "further discussions." In the middle of the meeting, the electrical power suddenly went out, and Dan called for someone to light a candle. At this point, Naomi Tani, in traditional Japanese kimono, appeared in the room with a lantern. Konuma later said, "This was Dans theatrical sense - his way of saying yes." Konuma, p.22-23 

Director Konuma and script-writer Tanaka both agreed that Dans novel was unfilmable, and made significant changes to the story. Dan and Naomi Tani both objected to this, and there was considerable friction between the two sides over the story during the writing of the script. Konuma later commented about Naomi Tanis role in this matter, "while we worked on the final script, she was very argumentative. However, I was impressed with her change in attitude after the companys final decision. During the shoot, she never gave us a problem. Instead- she was committed. I admire her professionalism." 

After the film became a huge hit for Nikkatsu, the studio wanted to follow it with a sequel. Oniroku Dan remained bitter and did not agree to participate in the next film, Wife to be Sacrificed (1974),  which became even a greater success. Dan did reconcile with the studio however, and continued to have a strong professional relationship with both Nikkatsu and Naomi Tani. 

== Synopsis ==
Makoto Katagiri (Yasunori Ishizu) is a 30-year-old man living with his mother Miyo (Hiroko Fuji), the latter who operates a pornographic photography business specializing in sadomasochistic content out of her combined home and adult toy store.  As a child, Makoto shot and killed a black American soldier who he found having sex with his mother.  The incident left Makoto psychologically stunted as well as impotent, and as such his mother and her staff treat Makoto much as a child.  Despite this, Makoto has developed a strong interest in BDSM, though he has no outlet for his interests.

Senzô Tôyama (Nagatoshi Sakamoto) is the elderly owner of the company Makoto works for, and also has an interest in BDSM.  However, his new wife Shizuko (Naomi Tani) refuses to let Senzô tie her up or even watch her bathe; instead he vents his frustrations and needs on their housemaid Haru (Hijiri Abe), forcing her to indulge in his need for S&M.  After finding some bondage pornography in Makotos desk, Senzô summons Makoto to his home, where he first asks, then orders Makoto to kidnap and "train" his wife in order to break her pride so that she will submit to his desires.  After some hesitation, Makoto agrees.

While in town, Senzô drugs Shizukos tea with sleeping pills, allowing Makoto to leave with her in his vehicle.  On the way home Makoto stops in a field and attempts to rape the semi-conscious Shizuko, but his arousal brings on a hallucination of the black soldier he killed, forcing him to stop.  At home, Shizuko awakes to find herself tied up and calls for help, attracting the attention of Miyo.  Rather than helping the woman, Miyo seems pleased at her sons actions, and encourages him to rape Shizuko.  When he says he cant because she is too beautiful, Miyo suggests Makoto give Shizuko an enema to overcome that impression.  He does, and after watching her expel it finds he is able to become aroused without issue.  He rapes Shizuko while his mother listens outside his room.

Makoto reports his progress to Senzô regularly, providing tapes and even physical evidence of his wifes continued training.  However, he also finds himself becoming attached to Shizuko.  Makoto returns home one day to find his mother and her staff have been torturing Shizuko while he was at work.  He orders them off of her angrily, telling them not to touch her unless he is present.  Shizuko finds herself becoming more at ease with her situation and Makoto, and later asks him if he will protect her; he says yes, going on to say he wants to spend the rest of his life with her.  He tells his mother this, and when she objects he responds angrily, calling her an old hag.

One day Makoto returns home to find Shizuko tied up and being used by the black soldier from his dreams.  He recoils in horror when the man rises to attack him.  His mother then steps in, paying the man, revealing that she staged the scene with a look-alike to render Makoto impotent once more.  Makoto tries to have sex with Shizuko again, despairing when he finds that his impotence has returned.  Despite this, he keeps training her, even taking her outside for a walk.  Taking a chance, he unties her so that she might use the restroom; after a long, worrying delay, Shizuko returns to the delight of Makoto.  Later the two go to watch a pornographic film, which features a black man who resembles the soldier from Makotos dreams.  Uncomfortable at first, Makoto starts to remember his childhood incident in detail, eventually remembering that it was his mother who shot the man, not him.  Overjoyed, Makoto calls his mother from a public phone booth to tell her he has remembered the truth, letting her listen as he has sex with Shizuka during the call.

Finally, Senzô arrives to reclaim his wife.  Makoto is surprised when Shizuka moves to her husbands side eagerly.  He asks her to divorce him so that they might marry, but she refuses, saying that she is now happy since Makoto made her a "total slut" just as her husband wanted.  However, she offers to let him come with the two of them, and though hesitant, Makoto agrees.  Miyo begs her son not to go, but the three pile into Senzôs vehicle and leave, a distraught Miyo running after the vehicle in vain.  The film ends with Shizuka bound, being ravaged simultaneously by Senzô and Makoto while Haru watches in shock.

== Cast ==
* Naomi Tani  - Shizuko Tôyama
* Nagatoshi Sakamoto - Senzô Tôyama 
* Yasunori Ishizu - Makoto Katagiri 
* Hiroko Fuji - Miyo Katagiri 
* Akira Takahashi
* Hijiri Abe - Haru

== Critical appraisal ==
Flower and Snake is considered important more from a  historical standpoint than for its artistic merits. The later Wife to be Sacrificed is more highly regarded by the critics. Oniroku Dan was not happy with the film, but was able to reconcile with Nikkatsu to continue making many more films from his novels, including Wife to be Sacrificed. Izumi Evers notes that "Nikkatsus somewhat innocent approach inadvertently adds comedy," but sums up her review of Flower and Snake with, "The heroine, who was masochistically trained by men, was actually controlling them the whole time. To me, despite all the humiliations suffered by heroines like Naomi, this is the fundamental truth behind seventies-era Japanese sex films."  The Weissers call it "Well-made garbage," but add that it is one of the best of Nikkatsus films in this genre. Weisser, p.155. 

==Availability==
Media Blasters has announced an all-region DVD release of Flower and Snake on its Tokyo Shock label, scheduled for August 28, 2007. 

==Remake==
 
The film was remade but with a different plot as Flower and Snake in 2004 followed by a sequel Flower and Snake 2 in 2005. Both films were directed by Takashi Ishii and starred Aya Sugimoto. And this was remade in 2010, directed by Yusuke Narita and starring Minako Komukai.

==Notes==
 

== Sources ==
*  
* Macias, Patrick. (2001). TokyoScope: The Japanese Cult Film Companion. (Review of Flower and Snake by Izumi Evers). Cadence Books, San Francisco. ISBN 1-56931-681-3, p.&nbsp;183.
* Weisser, Thomas and Yuko Mihara Weisser. (1998). Japanese Cinema Encyclopedia: The Sex Films. Vital Books : Asian Cult Cinema Publications. Miami. (ISBN 1889288527), p.&nbsp;155.

== External links ==
* 
* 
* , 2004 remake
*  
*  

 

 
 
 
 
 
 
 
 