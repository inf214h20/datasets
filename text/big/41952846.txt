Just Another Margin
{{Infobox film
| name           = Just Another Margin
| image          = Just Another Margin poster.jpg
| image size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Jeffrey Lau
| producer       = 
| writer         = 
| screenplay     = 
| story          = 
| based on       =  
| narrator       =  Alex Fong
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =   
| runtime        = 91 mins
| country        = China
| language       = Mandarin
| budget         = 
| gross          = United States dollar|US$4.63 million
}}
 Alex Fong. It was released on 2 February 2014.  

==Cast==
*Betty Sun
*Ronald Cheng
*Ekin Cheng Alex Fong
*Ivy Chen

==Plot Summary==
This movie takes place in an unspecified time period of China, but it is one where the famous heroes of Liangshan Marsh, the 108 Bandits, are currently active. A strange girl named Xu Jin Ling (Betty Sun) possessing a powerful yueqin, which doubles as a weapon for self-defense, offends the prominent businessman, Mr. Zhao (Guo Degang), by humiliating his niece using said musical instrument. As punishment for this act of insolence, Mr. Zhao arranges the marriage between Jin Ling and their home villages ugliest resident, Mao Dai Long (Suet Lam). However, Mr. Zhaos lecherous and conniving cousin, Shi Wen Sheng (Ronald Cheng), takes notice of the beautiful girl, and plots with Mr. Zhao to be rid of Mao Dai Long so that Jin Ling will become Wen Shengs wife. An unexpected turn of events occur, however, because when Dai Long introduces Jin Ling to his little stepbrother, Mao Song (Cheng Yee Kin), they immediately recognize each other, as they have met each other a long time ago when they were but children. Another series of unexpected events start happening in this seemingly uneventful village, as when Mao Song is helpless to save Dai Long from the crime he was framed by Wen Sheng for and is unable to save Jin Ling from becoming Wen Shengs wife, he runs into two oddly dressed men, to whom the audience is introduced to as two space aliens named Tranzor and Shakespeare of Planet B16, but introduce themselves to the destitute and somewhat suicidal man as two immortal fairies who will help him address his grievances. For the moment, it seemed as if everything was going the way Mao Song wanted, but things then take a turn for the worse when Jin Ling runs away from home since she refuses adamantly to acknowledge the hideous-looking Dai Long as her husband, though Dai Long didnt really care about that, Mr. Zhao and Wen Sheng hire the 108 Bandits, who are portrayed in this movie not as valiant heroes, but as money-grubbing mercenaries, to kill Jin Ling, and an alien martial artist (Hu Ge) who has been living undercover among humanity for decades and serves a powerful warlord of Planet B16 named the Black Emperor, is finally activated and deployed to assassinate Mao Song, for Mao Song is, in reality, a B16 Alien whos the legitimate heir to the leadership of B16, but was abandoned on Earth by his family under the guise of a human child so that the Black Emperor wont be able to find him. 

A great chase occurs with the 108 Bandits being the pursuers and Mao Song and Jin Ling, whose face was horrendously scarred by an acid spray from one of the Bandits, being the quarry. They manage to elude the mercenaries, but Jin Ling attempts to commit suicide by falling off a cliff, as she was unwilling to be married to Mao Song now that her face was rendered hideous, and she was on tenterhooks that Mao Song would have to endure humiliation and being the back end of nasty comments and jokes for marrying such an ugly girl. Mao Song jumped into the dark abyss after her, intending to die alongside her, and it was in this moment that his identity as the leader of Planet B16 had been realized, and when they were seconds away from hitting the bottom, Mao Song sprouted wings of light from his back, flew back up the cliff, and healed Jin Lings face in the meantime, making her face beautiful again. In this scene, everybody, except for Tranzor and Shakespeare who are drunk at a brothel, shows up; Mr. Zhao and Wen Sheng show up, hidden in the forest behind the cliffs edge, the 108 Bandits stand by the cliffs edge, staring in awe and wonder at the space ship that was rising out of the ground behind Mao Song and Jin Ling, Mao Dai Long appears, following the yueqin that transformed into a hovering robot probe, and the alien assassin, who was disguised as a female school teacher, shows up to kill Mao Song. A duel takes place at the bottom of the cliff, with the alien assassin and Mao Song seeming equally matched, but Mao Song at the last moment turned the tide of the battle in his favor by borrowing the strengths and fighting skills of all the 108 Bandits. After defeating this foe, Mao Song flies back up to the ship where he weds his beloved and offers the 108 Bandits new homes and jobs on Planet B16. Mao Dai Long remains in China with two pretty B16 girls to be his wives after they altered his appearance to make him handsome, and Mr. Zhao and Wen Sheng lament on their mistakes.

At the very end of the movie, the people of the village are seen everywhere lighting incense sticks in honor of a jade Buddha statue they erected some time after witnessing the B16 space ship rising from the ground, which bore a strong resemblance to the Buddha when they saw it from a distance.  

==Reception==
As of 16 February, it had grossed United States dollar|US$4.63 million. 

==References==
 

 
 
 

 
 
 
 
 