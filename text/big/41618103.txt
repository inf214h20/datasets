Creature (1999 film)
{{Infobox film
| name           = Creature
| image          = Movie_poster_for_the_documentary_Creature_(1999),_a_film_by_Parris_Patton.jpg
| image_size     = 215px
| border         = 
| alt            = 
| caption        = Theatrical release poster
| film name      = 
| director       = Parris Patton
| producer       = Don Lepore
| writer         = 
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = Stacey Hollywood, Filberto Ascencio, Butch Dean, Dusty Dean
| music          = Chad Smith
| cinematography = John Travers
| editing        = Parris Patton
| studio         = Grapevine Films
| distributor    = 7th Art
| released       =  
| runtime        = 76 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} Stacey "Hollywood" Dean.   

==Synopsis==
Born as Kyle Dean in a conservative area of North Carolina, Stacey "Hollywood" Dean was never truly accepted in her high school and was always called "creature" because of her desire to become a woman. Ostracized, Stacey left her home town to go to Hollywood, hoping to find more acceptance there. Four years later Stacey is returning home to visit with her parents in her new persona as Stacey Hollywood.

==Cast==
*Stacey "Hollywood" Dean as herself
*Filberto Ascencio as Barbarella
*Butch Dean as Himself
*Dusty Dean as Herself

==Reception==
Critical reception has been positive and the documentary received praise from   also gave a positive review and they expected that the film would "garner strong interest from small-screen programmers and enjoy thriving vid life as an inspiration to those similarly inclined, as well as limited illumination for those mystified by the milieu." 

Creature premiered at the 1999 Seattle International Film Festival    and was later nominated for a GLAAD Media Award for Outstanding Documentary.  It received a Best Documentary nomination at the 1999 Chicago International Film Festival  and in 2002 was broadcast on Cinemax.   

==References==
 

==External links==
*  
*  

 
 
 
 
 


 
 