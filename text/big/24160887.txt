Welcome Home (1989 film)
{{Infobox film
| name           = Welcome Home
| image          = Welcomehomeposter89.jpg
| image_size     = 
| caption        = Promotional film poster
| director       = Franklin Schaffner
| writer         = Maggie Kleinman
| narrator       = 
| starring       = Kris Kristofferson JoBeth Williams Sam Waterston Brian Keith Kieu Chinh
| music          = Henry Mancini
| cinematography = Fred J. Koenekamp
| editing        = Robert Swink
| distributor    = Columbia Pictures Rank Organization Warner Home Video (UK VHS) 
| released       = September 29, 1989
| runtime        = 96 minutes
| country        = United States
| language       = English
| budget         = $16,000,000
| gross          = $1,048,322
| preceded_by    = 
| followed_by    = 
}}
Welcome Home is a 1989 drama film directed by Franklin Schaffner. It stars Kris Kristofferson as a Vietnam War veteran who returns to his family after many years and who tries to readjust to life nearly 20 years later.

The film also stars JoBeth Williams, Brian Keith, and Sam Waterston, with the song "Welcome Home" performed by Willie Nelson. It was Oscar winner Schaffners final film. It was released almost three months after his death.

==Plot==
Jake Robbins went off to Vietnam, leaving his wife behind to mourn when he is reported missing, presumed dead. Seventeen years later, he unexpectedly returns.

Having been a prisoner of war, Jake was rescued and ended up in Cambodia having a family. Jakes reappearance is a godsend for his father, Harry, but a mixed blessing for wife Sarah, who has moved on with her life.

While old feelings stir in her, Jake confronts the military on how his disappearance was handled, and, more importantly, on how to track down his missing Southeast Asia wife and child.

==Cast==
*Kris Kristofferson as Jake
*JoBeth Williams as Sarah
*Sam Waterston as Woody
*Brian Keith as Harry
*Thomas Wilson Brown as Tyler
*Trey Wilson as Col. Barnes
*John Marshall Jones as Dwayne
*Ken Pogue as Sen. Camden
*Kieu Chinh as Leang

==Box Office==

The film was not a box office success. 
==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 