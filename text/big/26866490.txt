Lucky Night
{{Infobox film
| name           = Lucky Night 
| image          = Poster of Lucky Night.jpg
| image_size     = 
| caption        = Theatrical release poster
| director       = Norman Taurog 
| narrator       =  Robert Taylor Myrna Loy  
| cinematography = Ray June
| editing        = Elmo Veron 
| distributor    = Metro Goldwyn Mayer
| released       = May 5th, 1939 
| runtime        = 82 minutes 
| country        = United States 
| language       = English   budget = $589,000  .  gross = $1,080,000 
}}
 Robert Taylor and Myrna Loy, directed by Norman Taurog.

== Plot ==  Robert Taylor). They become acquainted and each discovers that the other is also poor. They try to get 50 cents to eat at a restaurant but a man complains to the police. They convince a policeman to give them 50 cents by saying that they are engaged (which they are not).

While walking, they drop the money without knowing it. When their restaurant bill comes to 50 cents, they suddenly realize they must have lost it. Someone leaves a coin on the table, Bill tells Cora to steal it, which she does. Bill spots a slot machine in the restaurant and tells Cora to gamble, which she does and wins. Bill and Cora go to a casino, win a car in a game and make more money gambling.

The two get drunk and wake up to find out they are married. Bill gets a job but still gets the urge to gamble; Cora doesnt care to live that life, so she leaves Bill and goes back to her father. Bill goes to her house to get her back and he succeeds.

== Credited cast ==  Robert Taylor as William Bill Overton 
*Myrna Loy as Cora Jordan Overton 
*Joseph Allen as Joe Hilton  
*Henry ONeill as H. Calvin Jordan, Coras father 
*Douglas Fowley as George, Bills friend 
*Bernard Nedall as Dusty Sawyer  Charles Lane as Mr. Carpenter, Paint Store Owner 
*Bernard Hayes as Blondie, Clerk at Carpenters  
*Gladys Blake as Blackie, Clerk at Carpenters 
*Majorie Main as Mrs. Briggs, the Land Lady 
*Edward Gargan as Police Man in Park 
*Irving Bacon as Bus Conductor 
*Oscar OShea as Lieutenant Murphy

==Box Office==
According to MGM records the film earned $716,000 in the US and Canada and $364,000 elsewhere resulting in a profit of $126,000. 
==References==
 
== See also == 
*Myrna Loy filmography 
*List of Metro-Goldwyn-Mayer films

 

 
 
 
 
 