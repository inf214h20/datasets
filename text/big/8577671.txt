Absurdistan (film)
 
{{Infobox film
| name           = Absurdistan
| image          = Absurdistan_poster.jpg
| caption        = Theatrical poster
| director       = Veit Helmer
| producer       =
| writer         = Veit Helmer Zaza Buadze Gordan Mihic Ahmet Golbol
| starring       = Maximilian Mauff Kristýna Maléřova
| music          = Shigeru Umebayashi
| cinematography = Giorgi Beridze
| production design = Erwin Prib
| editing        = Vincent Assmann
| distributor    =
| released       =  
| runtime        =
| country        = Germany Azerbaijan
| language       = Russian
| budget         =
| gross          =
}}

Absurdistan is a 2008 German-French comedy film written and directed by Veit Helmer.
 World Cinema Dramatic Competition at the 2008 Sundance Film Festival.     It was also entered into the 30th Moscow International Film Festival.   

==Plot==
The film is set in a remote and forgotten desert mountain village in the former Soviet Union, and chronicles a standoff between the sexes: the local women decide to withhold sex until their lazy men fix the pipeline that carries the village’s water supply. Young lovers Aya and Temelko are caught up in the argument and Temelko becomes determined to fix the pipeline so he can be with her.

==Cast==
 
*Kristyna Malérová as Aya
*Max Mauff as Temelko 
*Nino Chkheidze as Ayas Grandmother
*Ivane Ivantbelidze as Dantscho, the shooting gallery owner
*Ani Amiridze as Lenora, Dantschos daughter
*Ilko Stefanovski as Guri, Temelkos Father
*Assun Planas as Temelkos Mother
*Otto Kuhnle as the Barber
*Hijran Nasirova as the Barbers Wife
*Hendrik Arnst as Landlord
*Olga Nefyodova as Landlords Wife
*Adalet Zyadhanov as Policeman
*Matanat Atakishiyeva as Policemans Wife
*Azelarab Kaghat as Baker
*Michaela Bandi as Bakers Wife
*Blagoja Spirkovski-Dzumerko as Cobbler 
*Dace Bonate as Cobblers Wife
*Elhan Guliyev as Bus Driver
*Julietta Koleva as Bus Drivers Wife
*Helder Costa as Doctor
*Monica Calle as Doctors Wife 
*Kazim Abdullayev as Shepherd
*Firangiz Babyeva as Shepherds Wife
*László Németh as Postman
*Sarah Bensoussan as Postmans Wife
*Mubariz Alixanli as Watchmaker
*Khatuna Ioseliani as Watchmakers Wife
*Nurradin Guliyev as Beekeeper
*Elena Spitsina as Beekeepers Wife
*Radomil Uhlir as Butcher
*Suzana Petricevic as Butchers Wife
*Rafiq Azimov as Carpenter
*Nelli Cozaru as Carpenters Wife
*Vlasta Velisavljevic as Veteran
*Gisela Fritsch as Grandmother (voice)
 

==Production==
 , Azerbaijan.]] script after Shaki and Tbilisi. 

==Release==
Absurdistan had its world premiere at the 2008 Sundance Festival where it was nominated for the Grand Prize in the World Cinema Dramatic category. It released in theatres in Germany on 20 March 2008.

==Awards==
* 2008 Special Award  at the Bavarian Film Awards (Veit Helmer) German Film Awards (Erwin Prib)  
* 2009 International Fantasy Film Award at Fantasporto (Veit Helmer)
* 2009 Grand Prize for Best Feature Film at Mediawave, Hungary (Veit Helmer)

==See also==
*Lysistrata, a play by Aristophanes with a similar plot The Source

==References==
 

==External links==
*  
* 

 
 
 
 
 
 
 