Girl Model
{{Infobox film
| name           = Girl Model
| image          = 
| alt            = 
| caption        = 
| director       =  Ashley Sabin, David Redmon
| producer       = 
| writer         =  Ashley Sabin, David Redmon
| starring       = Ashley Arbaugh,  Nadya Vall, Rachel Blais  
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =    
| runtime        = 77 minutes
| country        = 
| language       = 
| budget         = 
| gross          = 
}}

Girl Model is a 2011 documentary film following Ashley, a conflicted model scout recruiting young Siberian girls to model in Japan, and Nadya, a recruited 13-year-old who gets financially taken advantage of during her modeling work in Japan.     It was directed by David Redmon and A. Sabin.  The film holds a 93% rating on Rotten Tomatoes. 

While receiving much praise for its subject matter, it left some critics wondering why the filmmakers didnt question the participants more thoroughly. Ty Burr of the Boston Globe writes, "It’s a valid approach that doesn’t yield as many dividends as the filmmakers hope. You sense there are dots left unconnected, a larger picture we’re not seeing. Are the various agency heads exploiting the models on their own, or is there malevolent collusion? Who’s making money and how? “Girl Model” shows but doesn’t investigate."     Jeannette Catsoulis of the New York Times writes, "Filled with blind eyes and unspoken agreements, “Girl Model” opens a can of worms, then disdains to follow their slimy trails."  Matan Uziel of The Glammonitor writes, "This video gives one a very distributing insight into how wealthy nations prey upon the poor in other countries. It’s just a small slice of the exploitation that goes on. This “meat” market, a prelude to sex trafficking, is creepy and ugly, and shocking, and we must do our best to stop it from happening. The fashion industry may look glamorous from the outside, and it’s deceiving." 

== References ==
 

== External links ==
*   
*  
*   at Rotten Tomatoes

 

 


 