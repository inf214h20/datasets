De Dana Dan
 
 
{{Infobox film
| name           = De Dana Dan
| image          =dedanadan.jpg
| caption        = Theatrical release poster
| director       = Priyadarshan
| producer       = Ganesh Jain Girish Jain Ratan Jain
| story          = Suresh Krissna
| screenplay     = Priyadarshan
| starring       =Akshay Kumar  Katrina Kaif Sunil Shetty Paresh Rawal Sameera Reddy
| music          = Songs:  
| narrator       = Akshay Kumar
| cinematography = N. K. Ekambaram
| editing        = Arun Kumar
| based on = Vettam by Priyadarshan, Udayakrishna & Siby K. Thomas
| distributor    = Venus Records & Tapes Eros Entertainment Baba Arts Limited Production
| released       =  
| runtime        = 167 mins
| country        = India Hindi
| budget         = 
| gross          = 
}}
 Hindi comedy film directed by Priyadarshan. The story is a partial adaptation of Priyadarshans own Malayalam film Vettam. The films cast includes Akshay Kumar, Sunil Shetty, Katrina Kaif and Sameera Reddy in lead roles. 
Filming began on 1 December 2008 at Mehboob Studios in Mumbai.     http://www.slate.com/id/2208198/  The film was released on 27 November 2009 and was declared as Above Average at the Box Office.

It has a huge star cast and all the characters run around after each other (causing hilarious scenes and consequences), hence the name De Dana Dan meaning "one after the other", or "helter skelter" in other words.

==Plot==
Nitin Bankar (Akshay Kumar) and Ram Mishra (Sunil Shetty) are lucky in love, otherwise their life is a big zero as their bank balance. Nitin is stuck as a servant and driver of Kuljeet Kaur (Archana Puran Singh), according to the conditions of a loan which his father had taken to educate Nitin. Kuljeet is the owner of many malls, restaurants, and other places in Singapore, where this whole story is based. Nitin is fed up with Kuljeets dog, Moolchand Ji, who always puts Nitin into trouble.

Ram works for a courier service in Singapore. He had originally come there to work in Chinese films, but he was not selected. Anjali Kakkad (Katrina Kaif), is in love with Nitin and Manpreet Oberoi (Sameera Reddy) is in love with Ram. Both of their girlfriends are rich, and they put a condition – get money or forget us.
 Manoj Joshi). After finding out that Oberoi is one of the richest Indians in Singapore, he lies to Oberoi to fix Nonnys wedding with Manpreet, which finally works out. As he didnt inform Kakkad, Kakkad gets really angry with Harbans. To counter Harbans, Kakkad fixes his daughter Anjalis wedding with someone else.

At the same casino where Harbans met Oberoi, Musha Heerapoorwala (Shakti Kapoor), decides to get married to Anu Chopra (Neha Dhupia), a dancer at that casino. After his brother-in-law finds out, he hires a Mafia Don, Maamu (Asrani), to kill Musha. Maamu sends his best assassin, Kaala Krishna Murali (Johnny Lever) to do the job. To hide from his wife, Musha books a room in Pan Pacific Hotel under the name Suber.

To get rid of all problems and earn some money, Nitin and Ram decide to kidnap Moolchand Ji, as that is the only thing living that Kuljeet, the rich lady, loves. But things go wrong – after they reached Pan Pacific Hotel, the place where they decide to stay, they realise that Moolchand Ji has escaped, causing the police to believe that its Nitin who has been kidnapped. Things worsen when Anjali runs away from home and reaches their room. Initially, Kuljeet refuses to pay the ransom to the kidnappers, but after all her customers decide to go on a strike, she agrees to pay the ransom. As Nitin does not want to go back to Kuljeet, they come up with a plan – they decide to get a dead body from Maamu and throw it onto the railway tracks, along with his driving licence, and then spread the news that the kidnappers have killed Nitin.

Harbans and Oberois families come to Pan Pacific Hotel, where the wedding is going to take place. Harbans and Oberoi click a photo to celebrate their families uniting. To give the advance money to Maamu, Nitin waits at the entrance of Pan Pacific, waiting for Maamus man to come and say the code Im Maamus man. But Kakkad arrives at the hotel, and chases Nitin into Harbans room. Nitin tries to hide under the bed, but there is no place for him. At the same time, Harbans enters his room and to hide from Harbans, Nitin hides inside the wardrobe. Fed up of his calls, Harbans throws his mobile into the wardrobe and locks it, causing Nitin to get stuck in a wardrobe.

Next what happens is De Dana Dan – confusion, mistaken room numbers, mistaken identities. and continuous chases. Kakkad breaks into Paramjeet Singh Lambas (Vikram Gokhale) room, who is the Indian Ambassador. Harbans mistakes Anu for Anjali, which causes great confusion later. Mushas photo, gets exchanged with Oberois photo, so Kaala tries to kill Oberoi. Wilson reaches Harbans room and searches for Harbans, and opens the wardrobe to find Nitin, and mistakes him as Harbans. In an attempt to kill Oberoi, Kaala ends up falling onto Rams and Nitins truck, which they take to pick up the ransom. While picking up the ransom, Nitin gets left behind, and is taken to the hospital, while Ram takes the money and reaches the hotel, and Kaala is assumed to be the kidnapper, as he was on top of the truck. And to top it all, Maamu arrives at the hotel with a dead body that nobody wants. In the end, a bomb enters the water-tank of the hotel, which causes it to burst and make the hotel flood. At the end, Nitin, Anjali, Ram and Manpreet get the money and live happily ever after, even though nobody else is happy.

==Cast==
* Akshay Kumar as Nitin Bankar
* Sunil Shetty as Ram Mishra
* Paresh Rawal as Harbansh Chadda Manoj Joshi as Brij Mohan Oberoi, Manpreets father
* Katrina Kaif as Anjali Kakkad
* Sameera Reddy as Manpreet Oberoi
* Neha Dhupia as Anu Chopra
* Archana Puran Singh as Kuljeet Kaur
* Aditi Gowitrikar as Pammi Chadda
* Asrani as Maamu, DON
* Chunky Pandey as Nonny Chadda
* Johnny Lever as Kaala Krishna Murari
* Shakti Kapoor as Musha Heerapoorwala/Suber
* Rajpal Yadav as Dagdu
* Tinnu Anand as Mr. Kakkad, Anjalis father
* Vikram Gokhale as Paramjeet Singh Lamba
* Supriya Karnik as Mrs. Kamini Lamba
* Sharat Saxena as Inspector Wilson Pereira
 Hera Pheri film. This is the first time the trio has acted together after Phir Hera Pheri in 2006, although Rawal and Shetty appeared in Priyadarshans Hulchul (2004 film)|Hulchul and Chup Chup Ke. Kumar and Rawal also appeared in Priyadarshans Bhagam Bhag and Bhool Bhulaiyaa.

The characters Nitin Bankar and Ram Mishra, played respectively by Akshay Kumar and Sunil Shetty, are names of respective long-lasting spot-boys.  Nitin Bankar is Akshays spotboy, while Ram Mishra is that of Sunil.

An article in Slate Magazine describes the shooting of a flood scene in Goregaon studios with locally hired white people.  The film took 80 days to complete.

===   Theme song ===
Paisa Paisa (complete name: Tu, paisa paisa kyun karti hai, tu paise pe kyun itna marti hai) is the theme song for this film.    The song is produced by Pritam, sung by RDB (Rhythm Dhol Bass)|RDB, and was performed by Akshay kumar and Katrina Kaif.
 RDB even Usher hit song "Yeah". This song was originally sung by Manak-E in Punjabi few years back in his album, however on request of Akshay Kumar the song was re-composed and re-released with a mix Hindi-Punjabi lyrics by RDB (Rhythm Dhol Bass)|RDB. The decision was made to use Paisa Paisa as the films theme song due it being light and up-tempo.   

;Theme song reception
Times of India reported in late November 2009, that released before the film, the song was gaining attention and "rocking the charts".  Before the films release, the tune was already considered one of the films "fast catching numbers" and a version was being used in the Jharkhand election campaign.     Glamsham reported "Paisa Paisa from De Dana Dan is a chartbuster and is igniting the music charts."    In December 2009, Daily News & Analysis reported that Paisa Paisa was a current favourite and expected to be a big number for the party season.    It was later reported, that while the film itself did not do as well as expected at the box office, the song aided in locating a lost, mentally challenged child when police were informed of the missing childs continually humming the Paisa Paisa tune.   

==Release==

===Box office===
De Dana Dan collected Indian Rupee|Rs. 530&nbsp;million in its opening weekend in India, thus making it the tenth biggest opening weekend of all time at the time of its release.  It collected 571.9&nbsp;million at the end of its first week.

Meanwhile, it did very well overseas debuting at No. 9 position in the U.K collecting £    on 48 screens in its opening weekend, with the per screen average working out to £6,417. In the US, it debuted at No. 20 position and collected $    on 69 screens in its opening weekend, with the per screen average working out to $11,194. In Australia, it debuted at No. 12 position and collected $96,597   on 14 screens in its opening weekend, with the per screen average working out to $6,900. At the end of its run in the U.K it collected £ . At the end of its run in the US it collected $944,979. At the end of its run in Australia it collected $ . 

===Home media===
The film was released on a one disc dual layer format DVD. Its running time is 167 minutes.

==Soundtrack==
{{Infobox album  
| Name        = De Dana Dan
| Type        = soundtrack
| Artist      =
| Cover       = De Dana Dan Album Art.jpg
| Released    = 2 November 2009 Feature film soundtrack
| Length      = 51:21
| Label       = Venus Records & Tapes
}}

{{Track listing
| extra_column = Artist(s)
| music_credits = yes
| lyrics_credits = yes
| title1 = Rishte Naate
| extra1 = Rahat Fateh Ali Khan, Suzanne DMello
| music1 = Pritam
| lyrics1 = Sayeed Quadri
| length1 = 4:42
| title2 = Paisa Paisa
| extra2 = Manak-E, Rhythm Dhol Bass|RDB, Selina
| music2 = RDB
| lyrics2 = Manak-E, RDB, Selina
| length2 = 3:54
| title3 = Gale Lag Ja
| extra3 = Javed Ali, Ban Jyotsna
| music3 = Pritam
| lyrics3 = Ashish Pandit
| length3 = 4:12
| title4 = Baamulaiza
| extra4 = Mika Singh, Dominique Cerejo, Style Bhai
| music4 = Pritam
| lyrics4 = Irshad Kamil
| length4 = 4:50
| title5 = Hotty Naughty
| extra5 = Sunidhi Chauhan
| music5 = Pritam
| lyrics5 = Neeraj Shridhar
| length5 = 3:38
| title6 = Rishte Naate
| note6 = Remix
| extra6 = Rahat Fateh Ali Khan, Suzanne DMello
| music6 = Pritam
| lyrics6 = Sayeed Quadri
| length6 = 4:25
| title7 = Baamulaiza
| note7 = Ragga Mix
| extra7 = Mika Singh, Dominique Cerejo, Style Bhai
| music7 = Pritam
| lyrics7 = Irshad Kamil
| length7 = 5:17
| title8 = Gale Lag Ja
| note8 = Version 2
| extra8 = Javed Ali, Ban Jyotsna
| music8 = Pritam
| lyrics8 = Ashish Pandit
| length8 = 4:22
| title9 = Hotty Naughty
| note9 = Remix
| extra9 = Sunidhi Chauhan
| music9 = Pritam
| lyrics9 = Neeraj Shridhar
| length9 = 3:46
| title10 = Baamulaiza
| note10 = Remix
| extra10 = Mika Singh, Dominique Cerejo, Style Bhai
| music10 = Pritam
| lyrics10 = Irshad Kamil
| length10 = 4:24
| title11 = De Dana Dan
| note11 =
| extra11 = Ad Boys
| music11 = Ad Boys Sameer
| length11 = 4:06
| title12 = Paisa Paisa
| note12 = Club Mix
| extra12 = Manak-E, RDB, Selina
| music12 = RDB
| lyrics12 = Manak-E, RDB, Selina
}}

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 