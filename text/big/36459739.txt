Molly Aunty Rocks!
 

{{Infobox film
| name           = Molly Aunty Rocks!
| image          = Molly Aunty Rocks.jpg
| alt            =
| caption        = Teaser poster
| director       = Ranjith Sankar
| producer       =
| writer         = Ranjith Sankar Prithviraj 
| music          = Anand Madhusoodhanan
| cinematography =
| editing        = Lijo Paul
| studio         = Dreams N Beyond
| distributor    = August Cinema
| released       =   
| runtime        = 128 minutes
| country        = India
| language       = Malayalam
| budget         =
| gross          =
}}
Molly Aunty Rocks!  is a 2012 Malayalam film directed by Ranjith Sankar, starring Revathi in the title role along with Prithviraj Sukumaran|Prithviraj. It has been released on 14 September 2012 to positive reviews from critics and audience alike. The positive energy that is maintained throughout is the highlight of this movie, opined Audience.         

==Synopsis==
Molly is an unstoppable woman, who will do only what she feels is right. Molly’s world is unique and she is its unquestioned queen. Pranav is an egoistic bureaucrat of the premier and exclusive Indian Revenue Service, who wants his system and power to rule over the subjects. When both of them clash, sparks fly and egos would not relent. Will Molly rock or will Pranav get her locked?    

==Cast==
* Revathi as Molly Aunty Prithviraj as Pranav IRS
* Lalu Alex as Benny
* K. P. A. C. Lalitha|K.P.A.C. Lalitha
* Mamukkoya as Salim Krishna Kumar as Ravi
* Sharath as Kochachan
* Shivaji Guruvayoor as Menon
* Sunil Sukhada as Shamsudheen
* Lakshmi Priya as Usha
* Rajesh Hebbar as Sunny

==Release and reception==
The film released on 14 September 2012 got a positive reception. Sify rated the film as very good and praised Revathy for her performance. Sify says "Molly Aunty Rocks is a gripping and engaging tale of a woman, who is well aware about what she wants from life. She is egoistic but her thoughts are noble". Sify gives the verdict "Go for it. Good".  Veeyen on Now Running.com rates it 3/5 and says "Ranjith Sankars Molly Aunty Rocks bristles with splendid ideas and perspectives on the world in which we live in. Change, and the dire need for it, as in the directors previous films, is the necessitated issue at hand, and filled with exceptional performances from the leading cast, marvelous wit and loads of positivism, MAR is one of the warmest movies to have hit the screens this year. Molly Aunty, without doubt rocks, and so does her director who has crafted this knockout of a film!". 

India Glitz calls it A Very Special Molly Aunty and rates it 7/10. In the review it says, "This Molly Aunty Rocks can be a definite prescription for family viewers. Without a single double entries or fowl mouthed wits or sequences, you are sure to enjoy a clean movie that is made with noble intentions". 

Paresh C Palicha on Rediff.com rates it 3/5 and says Molly Aunty really Rocks. He says in his review "Hats off to writer-director Ranjit Sankar who has made a superhero out of Revathy. She hogs the limelight here in a way she has not done in the prime of her career.Revathy plays the part with dignity and grace. She remains likeable throughout because of the control she keeps and the special effort that the director has made to keep her a genuine person and not make her an oddity.Director Ranjit Sankar and Revathy make Molly Aunty rock."  Theater balcony came up with a positive review by giving 78%, and also praised the performances and direction.  However, IBNLive termed the movie as average.  Aswin J Kumar of The Times of India was also not impressed with the film and gave it a rating of 2/5. 

Sify.com updated the Box Office status of Molly Aunty Rocks as Molly Aunty is a Winner at BO. It says in the article "Director Ranjith Sankars Molly Aunty Rocks has gained the maximum response from the viewers, among the four Malayalam releases of last weekend. “The collections were average for the first two shows on Friday and things started looking better from the evening shows. Sunday was really great and things are looking absolutely bright right now,” says Ranjith Sankar." 

Anand Hrishikesh on Flixmirror rates it 2.5/5 commenting "It seems the recent trend is in showcasing social issues and Malayalam film sticks to the same old routines that if one film on a particular subject becomes a hit follow the pattern and create similar films. Overall Ranjith Shankars Molly Aunty fails to rock in the hearts of a viewer and does not leave a long lasting impression once you are out of the cinema hall."  

== Accolades ==

{| class="wikitable"
|-
! Ceremony
! Category
! Nominee
! Result
|- 2nd South Indian International Movie Awards SIIMA Award Best Actress Revathi
| 
|-
|}

==References==
 

 
 

 