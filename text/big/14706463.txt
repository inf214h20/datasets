Fool's Paradise (film)
 
{{Infobox film
| name           = Fools Paradise
| image          = Fools_Paradise.jpg John Davidson, Mildred Harris, Conrad Nagel in Fools Paradise
| director       = Cecil B. DeMille
| producer       = Cecil B. DeMille
| writer         = Sada Cowan Beulah Marie Dix
| based on       =  
| starring       = Dorothy Dalton
| cinematography = Alvin Wyckoff Karl Struss
| editing        = Anne Bauchens
| studio         = Famous Players-Lasky 
| distributor    = Paramount Pictures
| released       =   reels (8,681 feet)
| country        = United States  Silent English intertitles
| budget         = $291,367.56
| gross          = $901,937.79
}}
 silent romance film directed by Cecil B. DeMille. The film stars Dorothy Dalton and Conrad Nagel and was based on the short story "Laurels and the Lady" by Leonard Merrick.   

==Cast==
* Dorothy Dalton as Poll Patchouli
* Conrad Nagel as Arthur Phelps
* Mildred Harris as Rosa Duchene
* Theodore Kosloff as John Roderiguez John Davidson as Prince Talaat-Ni
* Julia Faye as Samaran, His Chief Wife
* Clarence Burton as Manuel
* Guy Oliver as Briggs
* Jacqueline Logan as Girda
* Kamuela C. Searle as Kay Baby Peggy as Child (uncredited) William Boyd (uncredited)
* Gertrude Short as Child (uncredited)

==Production notes==
Production on the film began on April 4, 1921 and concluded on June 2, 1921. The films budget was $291,367.56 and it went on to gross $901,937.79 at the box office. 

==Preservation status==
Prints of Fools Paradise are preserved at the George Eastman House, the Library of Congress, and the UCLA Film and Television Archive.   

==References==
 

==External links==
 
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 

 
 