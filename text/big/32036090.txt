On the Run (1988 film)
 
 
 
{{Infobox film
| name = On the Run
| image = 
| caption = 
| director = Alfred Cheung Chin Kar-lok (action) Anthony Chan
| writer = Alfred Cheung Keith Wwong
| starring = Yuen Biao Pat Ha Charlie Chin
| music = Violet Lam
| cinematography = Peter Ngor
| editing = Yu Jun
| distributor = Bo Ho Films Co., Ltd. Golden Harvest Mobile Film Production Ltd Paragon Films
| released =  
| runtime = 88 minutes
| country = Hong Kong
| language = Cantonese
| budget = 
}} 1988 Hong Hong Kong action drama film directed by Alfred Cheung. The film stars Yuen Biao, Pat Ha and Charlie Chin.

==Plot==
Chui Pai (Pat Ha) is a dangerous killer who shot a woman during the night and lives with her daughter. She has been hunted for many years and cannot stop hiding. Hsiang Ming (Yuen Biao) is a police officer has been doing his job poorly. His wife was killed in a restaurant and the police accuse him of his wifes murder. Things become even more complicated for Hsiang as he discovers that she and he are now targeted by detectives led by Superintendent Lui (Charlie Chin), seeking to cover evidence of their own drug crimes. Framed for murder, Hsiang rapidly runs out of options as the killers target his elderly mother and young daughter. Wounded, Hsiang is forced to rely on the assassin Pai, who slowly warms up to him while caring for him and his young daughter. They have to stick together as long as they can until they die or are taken under custody.

==Cast==
*Yuen Biao - Hsiang Ming / Heung Ming
*Pat Ha - Chui Pai
*Charlie Chin - Superintendent Lui
*Chan Cheuk Yan - Lin (Pais daughter)
*Idy Chan - Inspector Lo Huan
*Lee Heung-kam - Hsiangs Mother
*Lawerance Lau - Hsiangs Brother
*Lo Lieh - Hsi
*Philip Ko - Cop
*Yuen Wah - Cop
*Alex To - Hao
*Lam Lap-San - Hui
*Peter Ngor - Unce Lu
*Bowie Lam - Johnny
*May Cheung - Fong
*Peter Pau - Doctor
*Kam Kong Chow - Cop
*Kwan Yung - Cop
*Pang Yun-Cheung - Cop
*Ng Kwok-Kin - Ling (Drug Squad member)
*Yau Kam-Hung

==See also==
*List of Hong Kong films
*Yuen Biao filmography

==External links==
* 

 
 
 
 
 
 
 

 
 