Pernicious
 
{{multiple issues|
 
 
}}
{{Infobox film
| name           = Pernicious
| image          = Pernicious_(2014)_movie_poster.png
| caption        = Original poster
| alt            =
| director       = James Cullen Bressack
| producer       = Rachvin Narula Kulthep Narula Daemon Hillin
| writer         = James Cullen Bressack Taryn Hillin Jackie Moore
| released       =  
| country = United States
| language = English
}}
Pernicious is an Thai-American supernatural horror film directed by James Cullen Bressack, {{cite web
  | title       = Exclusive: Filmmaker James Cullen Bressack Talks Pernicious
  | url         = http://www.dreadcentral.com/news/77319/exclusive-filmmaker-james-cullen-bressack-talks-pernicious-blood-lake-thai-translators/
  | publisher   = Dread Central
  | work        = DC
  | date        = 2013-10-12
  | accessdate  = 2014-05-21
}}  who also wrote the story along with co-writer Taryn Hillin. {{cite web
  | first       = Craig
  | last        = Hunter
  | title       = THN Interviews ‘Hate Crime’ Director James Cullen Bressack
  | url         = http://www.thehollywoodnews.com/2012/12/07/thn-interviews-hate-crime-director-james-cullen-bressack/
  | publisher   = thehollywoodnews.com
  | date        = 2012-12-07
  | accessdate  = 2014-05-21 Jackie Moore. {{cite web
  | title       = Pernicious 
  | url         = http://www.dreadcentral.com/reviews/95015/pernicious-2015/
  | publisher   = Dread Central
  | work        = DC
  | date        = 2013-10-12
  | accessdate  = 2014-05-21
}} 

== Cast ==
* Ciara Hanna as Alex Emily O’ Brien as Julia Jackie Moore as Rachel
* Russell Geoffrey Banks as Colin
* Sohanne Bengana as Vlad

== Production ==
Pernicious is produced by Benetone Hillin Entertainment, a company formed from the partnership of Daemon Hillin and Benetone Films, a leading production service company in Thailand. {{cite journal
|last1=staff
|title=PRODUCTION: PERNICIOUS
|journal=Film in Thailand
|date=4 October 2013
|format=PDF
|pages=11-12
|url=http://www.films.in.th/fit/FITOct2013.pdf|accessdate=18 April 2015}}  The movie was shot completely in Thailand.
Vintage Media International is reported to represent world rights to the movie. {{cite web
  | first       = Jeremy
  | last        = Kay
  | title       = Screen Daily - VMI bites into Pernicious - Vantage Media International (VMI) will represent world rights to Benetone Hillin Entertainment’s (BHE) horror title.
  | url         = http://www.screendaily.com/news/distribution/vmi-bites-into-pernicious/5068272.article?blocktitle=HEADLINES&contentID=40295
  | publisher   = Screen International
  | work        = Screen Daily
  | date        = 2014-03-04
  | accessdate  = 2014-05-21
}} 

==References==
 

== External links ==
 
 
*  
*  

 
 
 