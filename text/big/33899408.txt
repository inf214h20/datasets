Preet Na Jane Reet
{{Infobox film
| name           = Preet Na Jane Reet
| image          = 
| image_size     = 
| caption        = 
| director       = S. Bannerjee
| producer       = 
| writer         = 
| narrator       =  Johnny Walker
| music          = Kalyanji Anandji
| lyrics by      = 
| cinematography = 
| editing        = 
| distributor    = 
| released       = 1966 
| runtime        = 
| country        = India
| language       = Hindi
| budget         = 
}}

Preet Na Jane Reet is a 1966 film directed by S.Banerjee starring Shammi Kapoor and Saroja Devi as the romantic lead.

==Plot==
Ashok (Shammi Kapoor), the son of a rich businessman, Seth Karamchand (Nasir Hussain), telegrams his father that he is returning after several years in Europe to Bombay the following day. The scene shifts to another house. Sharda (Shammi) comes screaming into the room calling her servants but none of them seem to be around. She is upset to see her husband has been sitting quietly and reading the newspaper throughout her screaming. Sharda is a very dominating wife and hardly lets her husband speak.

Sharda knows that Ashok is coming back after 5 years in Europe and that he is the only son of the richest man around. So she feels he’s the most eligible boy in town for her sister, Kavita (Saroja Devi). Sharda wants Kavita to accompany her and receive Ashok at the airport but Kavita refuses. She says she doesn’t want to get married now, and hates rich boys. Sethji, Sharda and Jijaji wait at the airport to greet Ashok. Everybody descends from the flight but they can’t spot Ashok anywhere. Concluding that Ashok didn’t come by that flight, the party returns home.

Kavita and her group of friends are on their way to college when a car, with Ashok behind the wheel, splashes slush on Kavita. He drives back when he realizes what he has done, and the girls begin scolding him. He offers to take Kavita home to change and then drop her off at college. But her friends voice doubts as to his intentions. So he offers her some money to get her saree cleaned or get a new one, but Kavita sharply refuses while her friend punctures his tyre.

Ashok goes to the Desuza, Decruz and Fernandes Garage and is greeted by Fernandes (Johney Walker). Ashok and Fernandes were school buddies but Fernandes wasn’t interested in studies. His uncle took him under his wing, taught him the trade, and gave him a job in his garage. Fernandes fixes his car. Ashok reaches home. His father is annoyed that he didn’t inform him that he wasn’t coming by flight, but one look of regret from his son and the father melts. Ashok explains that his friend, Shamu asked him to drive home instead of taking the flight.

The next day he goes out and sees Kavita and her friends at a petrol pump. He overhears them mention putting up at Ashok Hotel. He follows them. And they all shower him with the same dislike again. He protests that he is a decent man. During the conversation he pokes their tyre with something, leaving them behind with a flat tyre. He drives ahead and when he realizes that they have stopped their car, he gets down and offers help but nobody wants them.

Kavita and group reach Ashok Hotel but the manager says the rooms are all booked. They all get worried because it’s dark already and they don’t know the place. The manager says if they go and talk to the owner, they might get a room because there are rooms reserved for the owners. Kavita goes to speak to the owner, but walks out when she realizes it’s Ashok. Her friends try convincing Ashok, but he says he will only give them a room if Kavita requests it. Her friends emotionally blackmail Kavita into arranging an accommodation for them.

The following morning Sethji turns up at the hotel, demanding the manager for Book of Accounts. When he learns that Ashok’s there, he goes to his room. Kavita and group are at the counter. The manager confides in them that Sethji is very short tempered. This gives her an idea. She sends her friends one by one to Ashok’s room, all of them claiming to be his girlfriends. An enraged Sethji then orders Ashok to pack his bag immediately and leave with him. It’s Ashok’s birthday and Sharda and Jijaji are getting ready for the party. Sharda wants Kavita to accompany them but Kavita refuses. Jijaji convinces her to join them and finally she gives in and goes with them to Sethji’s house. She’s quite surprised to find Ashok there, and ignores him as much as she can.

Ashok falls for her, even though Kavita says she doesn’t want to marry a rich guy who lives on “his father’s crumbs”. Ashok asks Fernandes for a job as a mechanic in his garage, explaining that he needs to get a new image. He manages to convince Kavita that he is Mohan the mechanic, not Ashok the rich loafer. They fall in love, although Ashok continues to annoy Kavita as himself too. Kavita is distressed when her sister insists on fixing her marriage to Ashok, and tells her she will only marry Mohan. Her sister is unimpressed with this idea. When his deception is eventually uncovered, Kavita is quite angry. He points out that she’s loved him by one name and hated him by another, but that he’s the same person beneath the names.

They get engaged. Ashok is invited to a school friend’s wedding. There, the groom’s father is told about the bride’s past (she ran away with another man) and cancels the wedding. Ashok marries the bride, Sheila, to save her and her family from dishonor. He then takes her home. He keeps repeating his belief that Sheila made one mistake, and asks why she should have to suffer for the rest of her life for it? He says this to his father as well when he brings his new wife home, but his father is enraged and throws them out of the house. Ashok takes Sheila to his old digs at the garage, and they settle in to make a new life. Sharda and Jijaji are shocked to hear the news. Kavita blames him for cheating on her, but Jijaji tells her it’s not fair to reach a conclusion without knowing the entire truth. So she goes to Ashok’s house and meets Sheila. Kavita feels guilty for accusing Ashok of infidelity.

Kavita still loves Ashok but Ashok accepts that it was not in his fate to marry Kavita and is content with what he has got. He tries his best to forget his past and to start his life afresh with Sheila. Even her past doesn’t bother him. He doesn’t blame her for coming in between him and Kavita but accepts her whole heartedly and does his best to be a good husband.

==Soundtrack==
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Teri Zulfen Pareshan"
| Mohammed Rafi
|-
| 2
| "Ari O Kanha Bairi Zamana Marega Tana"
| Lata Mangeshkar
|-
| 3
| "Main Bewafa Nahin Hoon"
| Mohammed Rafi
|-
| 4
| "O Janewale Sun Zara"
| Mohammed Rafi, Lata Mangeshkar
|-
| 5
| "Preet Na Jane Reet"
| Lata Mangeshkar
|-
| 6
| "Yahan Bhi To Nahin Hai"
| Mohammed Rafi, Kamal Barot 
|-
| 7
| "Yun Na Dekho Hamen Baar Baar"
| Mohammed Rafi
|}

==References==
 

==External links==
* 

 
 
 
 