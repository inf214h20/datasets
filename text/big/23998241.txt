The Lucky Horseshoe
{{Infobox film
| name           = The Lucky Horseshoe
| image          = The_Lucky_Horseshoe_1925_Poster.jpg 
| border         = yes
| caption        = Theatrical release poster
| director       = John G. Blystone
| producer       = {{Plainlist|
* John G. Blystone William Fox
}} John Stone
| screenplay     = 
| story          = Robert Lord
| starring       = {{Plainlist|
* Tom Mix
* Billie Dove
* Malcolm Waite
}}
| music          = 
| cinematography = Daniel B. Clark
| editing        = 
| studio         = J. G. Blystone Productions
| distributor    = Fox Film Corporation
| released       =  
| runtime        = 5 reels (4,949 ft)
| country        = United States
| language       = English intertitles
| budget         = 
| gross          = 
}} Western silent film directed by John G. Blystone and starring Tom Mix, Billie Dove, and Malcolm Waite.    Based on a story by Robert Lord, the film is about a ranch foreman who assumes responsibility for the ranch following the owners death. He also cares for the owners daughter who is taken to Europe by an aunt. Two year later the woman returns from Europe with her new wealthy fiancée and plans to hold their wedding at the ranch, which the foreman has turned into a successful tourist destination. The foremans feelings for the woman have not been diminshed by the years, and after learning some damaging information about the fiancée, the foreman must find a way to stop the wedding.  

A print is preserved at the Museum of Modern Art, New York.     

==Plot==
Following the death of the owner of the Hunt ranch, foreman Tom Foster (Tom Mix) assumes responsibility for the property, taking also into his care Eleanor Hunt (Billie Dove), the beautiful daughter of the late owner. Although he falls in love with the girl, Tom is too diffident to express his feelings and propose marriage. Soon after, Eleanor is asked to accompany her aunt to Europe. 

Two years later, Eleanor returns from Europe with condescending airs, accompanied by Denman (Malcolm Waite), her wealthy European fiancée. Eleanor announces that she plans to hold the wedding at the ranch, which has been renovated by Tom and transformed into a successful tourist destination. Toms friend, Mack (J. Farrell MacDonald), tells Tom about the rakish exploits of Don Juan, hoping to instill in him a bit of romance. 

Wanting to eliminate any competition, Denman instructs his men to kidnap Tom and keep him prisoner until after the wedding. Tom is knocked on the head and dreams that he is the fabled Juan, fighting like a lion for love. When he wakes up, Tom frees himself from his bonds and rides back to the ranch, where he arrives just in time to prevent the wedding. Afterwards, Tom and Eleanor are married.

==Cast==
* Tom Mix as Tom Foster
* Billie Dove as Eleanor Hunt
* Malcolm Waite as Denman
* J. Farrell MacDonald as Mack
* Clarissa Selwynne as Aunt Ruth Ann Pennington as Dancer
* J. Gunnis Davis as Denmans Valet
* Tony the Horse as Toms Horse
* Gary Cooper (uncredited)

==Reception==
In his review in The New York Times, Mordaunt Hall found the film well suited to Tom Mixs talents, calling it "a most agreeable entertainment."    Hall enjoyed the super-heroism displayed by Mix in one scene—lassoing four or five men and bowling over scores of others—which remains plausible in that it occurs in a dream. As Don Juan, Tom awards the prize for beauty to Elvira Hunt, who appears as a charming Spanish girl. After fighting off and routing his enemies by leaping to a chandelier and then lunging across the great hall, Tom says to the girl, "Come, fair Eleanor; forget that despoiler, as you are mine." She replies, "Oh, Don Juan, you are a man. But will they take me from you?" Hall concluded:
 


==See also==
*Tom Mix filmography

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 