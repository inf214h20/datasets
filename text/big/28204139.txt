Watch (film)
{{Infobox film
| name           = Watch
| image          =
| image size     =
| alt            =
| caption        = Briana Waters
| producer       = Briana Waters
| writer         = Briana Waters
| narrator       =
| starring       = Clandestine Dead Can Dance Joules Graves Timothy Hull Anthea Lawrence Sapphire Spirit of the First Peoples ¡Tchkung!
| cinematography =
| editing        = Briana Waters Gary Varnell
| studio         =
| distributor    = Greenerella Productions
| released       = 2001
| runtime        = 66 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
| preceded by    =
| followed by    =
}} Briana Waters, Washington logging Cascade Mountain Fossil Creek. The film served as Waters senior project at Evergreen State College.   

==Summary==
The film opens with the Plum Creek Timber Company attempting to exchange ownership of   of land to the federal government in exchange for   considered more suitable for commercial logging, in what will become known as the I-90 land exchange. This exchange, if approved will give Plum Creek ownership of   from the Gifford Pinchot National Forest, which includes Watch mountain and land surrounding Fossil Creek, near Randle.    Residents of the town, in addition to young activists, express concern that Plum Creek logging operations will destroy old growth forest in the area and damage the local eco-system of the creek, resulting in mudslides.   
 Cowlitz tribe. 

In November 1999, Plum Creek agrees to remove the disputed areas from the draft of their land exchange agreement, and the finalized exchange grants them rights to only   of land,     primarily east of Cascades, with Watch mountain and Fossil Creek excluded from the deal.  The film ends with the activists tearing down their own platforms in the old growth canopy, and gathering celebrate their victory. 

==References==
 

==External links==
*  
*   - A website made by supporters of Waters with regards to her imprisonment. Copies of the documentary may be ordered through this site.

 
 
 
 
 
 
 

 