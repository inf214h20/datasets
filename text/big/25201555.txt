List of horror films of 2007
 
 
 
 
A list of horror films released in 2007 in film|2007.

{| class="wikitable sortable" 2007
|-
! scope="col" | Title
! scope="col" | Director
! scope="col" class="unsortable" | Cast
! scope="col" | Country
! scope="col" class="unsortable" | Notes
|-
!   | 100 Tears
| Marcus Koch || Georgia Chris, Joe Davison, Raine Brown, Jack Amos ||   ||  
|-
!   | 13 Hours in a Warehouse
| Dav Kaufman || Cody Lyman, Rachel Grubb, Meisha Johnson ||   ||  
|-
!   | 1408 (film)|1408
| Mikael Håfström || John Cusack, Samuel L. Jackson, Mary McCormack ||   ||  
|-
!   | 28 Weeks Later
| Juan Carlos Fresnadillo || Robert Carlyle, Rose Byrne, Jeremy Renner ||     ||  
|- 30 Days of Night
| David Slade || Josh Hartnett, Melissa George ||   ||  
|-
!   | 9 Lives of Mara
| Balaji K. Kumar || Jennifer Gimenez, Patrick Bauchau, Pollyanna McIntosh ||   ||  
|-
!   | A Feast of Flesh Mike Watt || Amy Lynn Best, Rachelle Williams, Debbie Rochon ||   ||  
|-
!   |  
| Greg Strause, Colin Strause || Reiko Aylesworth, Steven Pasquale, Shareeka Epps ||   ||  
|-
!   | Alone (2007 film)|Alone
| Banjong Pisanthanakun, Parkpoom Wongpoom || Marsha Wattanapanich, Ruchanu Boonchooduang, Vittaya Wasukraipaisan ||   ||
|-
!   | Amateur Porn Star Killer Shane Ryan || Jan Gould, Michiko Jimenez, Shane Ryan ||   ||  
|-
!   | American Zombie
| Grace Lee || Austin Basis, Kasi Brown, Alice Amter ||   ||  
|-
!   | The Appeared
| Paco Cabezas || Pablo Cedrón, Leonora Balcarce, Héctor Bidonde ||     ||  
|-
!   | Apartment 1303
| Ataru Oikawa || Noriko Nakagoshi, Arata Furuta, Yuka Itaya ||   ||  
|-
!   | August Undergrounds Penance
| Fred Vogel || Cristie Whiles, Shelby Vogel, Jerami Cruise ||   ||
|-
!   |   Scott Harper || Dedee Pfeiffer, Kevin Kazakoff, Wittly Jourdan ||   ||
|-
!   | Barricade (2007 film)|Barricade&nbsp;– Welcome to Hell
| Timo Rose || Raine Brown, Joseph Zaso, Thomas Kercmar ||   || Direct-to-video
|-
!   |   Jamie Dixon || Tomas Arana, Pollyanna McIntosh, David Chokachi ||   || Television film
|-
!   | Believers (film)|Believers Johnny Messner ||   || Direct-to-video
|- Beneath the Surface
| Blake Reigle || Dominique Geisendorff, Brett Lawrence, Tiffany Fox ||   ||  
|- Beverly Hills Massacre
| Alec Cizak || Alicia Klein, Gray George ||   ||  
|- Big Bad Wolf
| Lance W. Dreesen || Sarah Aldrich, Trevor Duke, Christopher Shyer ||   ||  
|-
!   | Black Swarm
| David Winning || Jayne Heitmeyer, Robert Englund, Rebecca Windheim ||   ||  
|- Black Water
| Andrew Traucki, David Nerlich || Fiona Press, Maeve Dermody, Ben Oxenbould ||   ||  
|- Blood and Chocolate
| Katja von Garnier || Agnes Bruckner, Olivier Martinez, Hugh Dancy ||      ||  
|- Blood Oath
| Kevin Kangas || Enrique Camacho, Jessica Cardinale, Natalie Hart ||   || Direct-to-video
|- Blood Ties
| Sean Delgado || Natalie Dolishny, Lance J. Holt, Pete Punito ||   || Short film   
|- Blood Ties
| Yee-Wei Chai || James Jordan Tay, Clarissa Soon, Loo-Pin Loke ||   ||  
|-
!   | BloodMonkey Robert Young || F. Murray Abraham, Laura Aikman, Zach McGowan ||   || Direct-to-video
|-
!   | Bloodlines (2007 film)|Bloodlines
| Stephen Durham || Tracy Kay, Grace Johnston ||   || Direct-to-video 
|-
!   | Bone Eater Adrian Alvarado, Bruce Boxleitner, Randy Flagler ||   ||  
|-
!   | Boogeyman 2
| Jeff Betancourt || Danielle Savre, David Gallagher, Mae Whitman ||   || Direct-to-video
|-
!   | Book of Lore
| Chris LaMartina || Matt Benicewicz, Lauren Meley, Jon Gonzales ||   ||  
|-
!   | Borderland (film)|Borderland
| Zev Berman || Rider Strong, Brian Presley, Martha Higareda, Jake Muxworthy ||   ||  
|-
!   | B.T.K. (film)|B.T.K.
| Michael Feifer || Kane Hodder, Amy Lyndon, Daniel Bonjour ||   ||  
|- Buried Alive
| Robert Kurtzman || Tobin Bell, Leah Rachel, Germaine De Leon ||   || Direct-to-video 
|- The Caretaker
| Bryce Olson || Judd Nelson, Carole Russo, Jennifer Tilly ||   ||  
|-
!   | Carnies (film)|Carnies
| Brian Corder || Reggie Bannister, Lynn Ayala, Lee Perkins ||   ||
|-
!   | Captivity (film)|Captivity
| Roland Joffé || Elisha Cuthbert, Daniel Gillies, Pruitt Taylor Vince ||    ||  
|-
!   | Catacombs (2007 film)|Catacombs David Elliot Alecia Moore ||   ||
|- The Cellar Door
| Matt Zettell || Michelle Tomlinson, James DuMont, Algernon DAmassa ||   ||  
|- The Chair
| Brett Sullivan || Nick Abraham, Michael Capellupo ||   ||  
|-
!   | Chicago Massacre
| Michael Feifer || Tony Todd, Debbie Rochon, Corin Nemec ||   || Direct-to-video 
|-
!   | Closet Space
| Mel House || Melanie Donihoo, James LaMarr, Evan Scott ||   ||  
|- The Cook
| Gregg Simon || Penny Drake, Nina Fehren, Mark Hengst ||   ||  
|- Crazy Eights
| James Koya Jones || Traci Lords, Frank Whaley, Dina Meyer ||   ||  
|-
!   | Croc (film)|Croc
| Stewart Raffill || Michael Madsen, Sherry Phungprasert, Peter Tuinstra ||   ||
|-
!   | Curse of the Wolf
| Len Kabasinski || ||   ||  
|-
!   | The Daisy Chain
| Aisling Walsh || Samantha Morton, Eva Birthistle, Steven Mackintosh ||    ||  
|-
!   | Dead Mary Robert Wilson || Dominique Swain, Marie-Josée Colburn, Michael Majeski ||   || Direct-to-video
|- The Bug
| Martijn Smits || Niels Verkooyen, Ger Van Der Grijn, Carry Tefsen ||   ||
|-
!   | Dead Silence
| James Wan || Ryan Kwanten, Amber Valletta, Donnie Wahlberg ||   ||  
|- Dead Tone
| Deon Taylor, Brian Hooks || Rutger Hauer, Jud Tylor, Antwon Tanner ||   ||  
|-
!   | Death of a Ghost Hunter
| Sean Tretta || Davina Joy, Patti Tindall, Lindsay Page ||   ||  
|-
!   | The Deaths Of Ian Stone
| Dario Piana || Mike Vogel, Christina Cole ||    ||  
|-
!   |  
| Jeffery Scott Lando || Tobin Bell, Sam Easton, Lindsay Maxwell ||   || Direct-to-video 
|-
!   | Demon Resurrection
| William Hopkins || Damian Ladd, Bashir Solebo, Joe Zaso ||   ||  
|-
!   | Devils Diary (2007 film)|Devils Diary
| Farhad Mann || Alexz Johnson, Miriam McDonald ||   || Television film 
|-
!   | Devil Girl
| Howie Askins || Jessica Graham, Willow Hale, Vanessa Kay ||   ||  
|-
!   | Domain of the Damned
| Stacy Davidson || Leon Blum, Melissa Bubela, Tiernan Estridge ||   ||
|- Donkey Punch Tom Burke ||   ||  
|-
!   | Doomed to Consume
| Jason Stephenson || Joe Knetter, Rachel Grubb, Sonja Beck ||   ||  
|-
!   | Dreams of the Dead
| John Orrichio || Cathy Loch, Tony Rugnetta ||   ||  
|-
!   | Drive-Thru (film)|Drive-Thru
| Brendan Cowles, Shane Kuhn || Leighton Meester, Nicholas DAgosto ||   ||  
|-
!   |  
| Michael Feifer || Kane Hodder, Michael Berryman, Priscilla Barnes ||   || Direct-to-video 
|-
!   | El Custodio del Mal
| Fernando Saenz || Arturo Martinez Jr., Karla Barahona, Ruth Mendoza ||   ||  
|-
!   | Exte Megumi Sato, Chiaki Kuriyama, Miku Satô ||   ||  
|-
!   | The Evil Offspring Jim Lewis, John Anton, Rudy Hatfield ||   ||
|-
!   | Eye of the Beast Gary Yates || James Van Der Beek, Alexandra Castillo, Ryan Rajendra Black ||   ||
|-
!   | Fall Down Dead
| Jon Keeyes || Udo Kier, David Carradine, Dominique Swain ||   ||
|-
!   | Fear of Clowns 2 Frank Lama ||   || Direct-to-video
|-
!   | Fear(s) of the Dark Charles Burns, Marie Caillou, Pierre di Sciullo, Lorenzo Mattotti, Richard McGuire || Aure Atika, Gil Alma, Nicole Garcia ||   || Animation
|- The Ferryman Chris Graham || John Rhys-Davies, Amber Sainsbury, Tamer Hassan ||   || Direct-to-video 
|-
!   | The Final Curtain
| Jeff Burton || Niki Huey, Edward Conna, Reggie Bannister ||   ||
|-
!   | Fist of the Vampire
| Len Kabasinski || Darian Caine, Brittney Card, Cheyenne King ||   ||
|-
!   | The Flesh Keeper
| Gerald Nott || Arianne Martin, Clint Glenn, Parrish Randall ||   || Direct-to-video 
|-
!   |   Scott Thomas || Dale Midkiff ||   || Direct-to-video
|- Forest of Death Danny Pang || Shu Qi ||   ||  
|- Forest of the Dead Chris Anderson ||   ||  
|-
!   | Fraternity Massacre at Hell Island Mark Jones || Anthony Flessas, Sarah Ewell, Michael Gravois ||   ||
|-
!   | Freakshow (film)|Freakshow
| Drew Bell || Rebekah Kochan, Dane Rosselli, Diego Barquinero ||   ||  
|-
!   | Frontière(s)
| Xavier Gens || Samuel Le Bihan, Maud Forget, Estelle Lefébure ||    ||  
|-
!   | Gimme Skelter Scott Phillips || Mark Chavez, Elske McCain, Peter Fishburn, Kurly Tlapoyawa ||   ||  
|- The Girl Next Door Gregory Wilson || Blythe Auffarth, Daniel Manche, William Atherton ||   ||  
|-
!   | God of Vampires
| Rob Fitz || Dharma Lim, Shy Theerakulsit, Ben Wang ||   ||  
|-
!   | Gone (2007 film)|Gone
| Ringan Ledwidge || Scott Mechlowicz, Amelia Warner, Yvonne Strahovski ||    ||
|-
!   | Gothkill
| JJ Connelly || Juliya Chernetsky, Tom Velez, Erica Giovinazzo ||   ||  
|-
!   | Greetings (2007 film)|Greetings
| Kenneth Colley || Kristy Cox, Henry Dunn , Maria Long ||   ||  
|- Grim Reaper
| Michael Feifer || Cherish Lee, Benjamin Pitts, Mike Korich ||   || Direct-to-video
|-
!   | Grindhouse (film)|Grindhouse Freddy Rodriguez, Kurt Russell, Zoë Bell ||   ||  
|-
!   | Grizzly Rage
| David DeCoteau || Tyler Hoechlin, Kate Todd, Graham Kosakoski ||   ||  
|- Hallowed Ground
| David Benullo || Ned Vaughn, Jaimie Alexander, Jim Cody Williams ||   ||  
|-
!   | Halloween (2007 film)|Halloween
| Rob Zombie || Malcolm McDowell, Tyler Mane, Scout Taylor-Compton, Danielle Harris, Daeg Faerch ||   ||  
|-
!   | Hallows Point
| Jeffrey Lynn Ward || Christa Campbell, Arnie Pantoja, Kimberly McVicar ||   ||  
|- The Hanged Man
| Neil H. Weiss || Adam Hatley, Shanola Hampton, Ralph Hatley ||   ||
|- Hannibal Rising
| Peter Webber || Gaspard Ulliel, Gong Li ||     ||  
|- Hansel & Gretel
| Pil-Sung Yim || Jeong-Myeong Cheon, Ji-hee Jin, Hee-soon Park ||   ||
|-
!   | Haunted (2007 film)|Haunted
| Alper Mestçi || Burak Özçivit, Kurtulus Sakiragaoglu, Bigkem Karavus ||   ||
|- Haunted Forest
| Mauro Borrelli || Sevy Di Cione ||   ||  
|- The Haunted School Amanda Lee, Tien You ||   ||
|-
!   |  
| Alex Zamm || Brittany Curran, Tobin Bell, Emily Osment ||   ||  
|-
!   | The Haunting of Danbury House
| John Orrichio, Karl Petry || William Schineller, Cathy Loch, Carrie Nagy ||   ||  
|-
!   | The Haunting of Marsten Manor
| Dave Sapp || Brianne Davis, Ken Luckey, Ezra Buzzington ||   ||  
|-
!   | The Haunting of Sorority Row
| Bert Kish || Agim Darshi, Meghan Ory, Jessica Horas ||   ||  
|-
!   | Higurashi no Naku Koro ni
| Chiaki Kon || Soichiro Hoshi, Mela Lee, Grant George ||   || Animation
|-
!   | The Hills Have Eyes 2
| Martin Weisz || Michael McMillian, Jessica Stroup, Daniella Alonso ||   ||  
|- The Hitcher Dave Meyers || Sean Bean, Sophia Bush, Zachary Knighton ||   ||  
|-
!   | Hoboken Hollow
| Glen Stephens || C. Thomas Howell, Lin Shaye, Dennis Hopper ||   ||  
|-
!   |  
| Eli Roth || Lauren German, Roger Bart, Heather Matarazzo, Bijou Phillips ||   ||  
|-
!   | House (2007 film)|House
| Robby Henson || Michael Madsen, Reynaldo Rosales, Heidi Dippold ||   ||  
|-
!   | House of Fears
| Ryan Little || Corri English, Sandra McCoy, Corey Sevier ||   ||  
|-
!   | The House of the Demon
| George L. Ortiz || Rashida Abdul-Jabbar, Gabriel McIver, Katrina Ellsworth ||   ||  
|- Hunting Season
| Nathan Wrann || ||   ||  
|- I Am Legend
| Francis Lawrence || Will Smith, Alice Braga, Salli Richardson-Whitfield ||   ||  
|-
!   | Ice Spiders Tibor Takács || Vanessa A. Williams, Patrick Muldoon, Carleigh King ||   || Television film 
|-
!   | In Love with the Dead Danny Pang || Stephy Tang, Shawn Yue ||   ||
|- In the Spiders Web
| Terry Winsor || Sohrab Ardeshir, Lance Henriksen, Jane Perry ||   || Television film
|-
!   | Inside (2007 film)|Inside
| Alexandre Bustillo, Julien Maury || Alysson Paradis, Béatrice Dalle ||   ||  
|-
!   | Its My Party and Ill Die If I Want To
| Tony Wash || Adrienne Fischer, Tom Savini, Alicia Kenney ||   ||  
|- The Invasion
| Oliver Hirschbiegel, James McTeigue || Jeremy Northam, Nicole Kidman, Daniel Craig ||    || Film remake 
|-
!   |  
| Jon Knautz || Robert Englund, Trevor Matthews, Rachel Skarsten ||   ||  
|-
!   | Kaidan (2007 film)|Kaidan
| Hideo Nakata || Teisui Ichiryûsai, Hitomi Kuroki, Kumiko Asô ||   ||  
|- Kiss of the Vampire
| Joe Tornatore || Costas Mandylor, Martin Kove, Katie Rich ||   ||  
|-
!   | Lake Dead
| George Bessudo || Kelsey Crane, Jim Devoti, Tara Gerard ||   ||  
|-
!   | Lake Placid 2 John Schneider, Alicia Ziegler ||   || Television film 
|-
!   | The Last Gateway
| Demian Rugna || Kevin Schiele, Rodrigo Aragon, Saloma Boustani, Patricio Schwartz, Hugo Halbrich ||   ||  
|- Left for Dead
| Albert Pyun || Oliver Kolker, Brad Krupsaw, Victoria Maurette ||    ||  
|-
!   | The Legend of Bloody Mary
| John Stecenko || Paul Preiss, Stephen Macht, Elissa Dowling ||   ||  
|-
!   | The Legend of Sorrow Creek Matt Turner, Christina Caron ||   || Direct-to-video 
|-
!   | Lights Camera Dead
| Tim Reaper || Richard Christy, Monica Moehring, Rob Rozier ||   ||
|-
!   | Little Red Devil
| Tommy Brunswick || Daniel Baldwin, James Russo, Dee Wallace ||   ||  
|- Living Hell Richard Jefferies || Erica Leerhsen, Judy Herrera, Johnathon Schaech ||   ||  
|- The Living and the Dead
| Kristijan Milic || Borko Peric, Robert Roklicer, Nermin Omic ||    ||
|-
!   | Long Pigs
| Chris Power, Nathan Hynes || Phyllis Cooper, Anthony Alviano, Kelly McIntosh ||   ||  
|- The Mad
| John Kalangis || Shauna MacDonald ||    ||
|-
!   | Maneater (2007 film)|Maneater Gary Yates || Gary Busey, Ian D. Clark, Diana Reis ||   || Television film
|-
!   | Mega Snake Tibor Takács || Siri Baruc, Nick Harvey, Laura Giosh ||   || Television film 
|- The Messengers Danny Pang, Oxide Pang Chun || Kristen Stewart, Dylan McDermott, Penelope Ann Miller ||   ||  
|- The Mist
| Frank Darabont || Thomas Jane, Marcia Gay Harden, Laurie Holden ||   ||  
|-
!   | The Mother of Tears Philippe Leroy ||    ||  
|-
!   | Mrs. Amsworth
| Frank Sciurba || Magenta Brooks, Christy Sullivan, Jim Nalitz ||   ||  
|-
!   |  
| Kim Tae-kyeong || Jo An, Cha Ye-ryeon, Ahn Thu ||    ||
|-
!   | Mutation&nbsp;– Annihilation
| Timo Rose || Thomas Kercmar, Andreas Pape, Olaf Ittenbach ||   ||  
|-
!   | My Name is Bruce
| Bruce Campbell || Bruce Campbell, Ted Raimi, Ellen Sandweiss ||   ||  
|-
!   | Nature of the Beast
| Rodman Flender || Eric Mabius, Dave Nichols, Gabriel Hogan ||   ||  
|-
!   | Negative Happy Chainsaw Edge
| Takuji Kitamura || Itsuji Itao, Yôsuke Asari, Hiromi Shinjô ||   ||
|- The Orphanage
| Juan Antonio Bayona || Belen Rueda, Fernando Cayo, Roger Princep ||    ||  
|-
!   | Ouija (2007 film)|Ouija
| Topel Lee || Judy Ann Santos, J.C. de Vera, Iza Cazaldo ||   ||
|-
!   | P2 (film)|P2 Rachel Nichols, Simon Reynolds, Wes Bentley ||   ||
|- Paper Dolls David Blair, Adam Pitman || Rob Benitz, Gill Gayle, Andra Carlson ||   ||
|-
!   | Paranormal Activity
| Oren Peli || Katie Featherston, Micah Sloat ||   || Direct-to-video 
|-
!   | Parkway
| Fereydoun Jeyrani || Anahita Nemati, Bita Farahi, Nima Shahrokhshahi ||   ||   
|-
!   | Pop Skull
| Adam Wingard || Lane Hughes ||   ||  
|-
!   | The Poughkeepsie Tapes
| John Erick Dowdle || Stacy Chbosky, Ivar Brogger, Samantha Robson ||   ||  
|-
!   | Prey (2007 film)|Prey
| Darrell James Roodt || Carly Schoeder, Peter Weller, Bridget Moynahan ||    ||  
|-
!   | Prey for the Beast Brett Kelly || Mark Courneyea, Anastasia Kimmett, Keri Draper ||   ||  
|-
!   | Premonition (2007 film)|Premonition
| Mennan Yapo || Sandra Bullock, Julian McMahon, Shyann McClure, Courtney Taylor Burness, Kate Nelligan, Nia Long, Amber Valletta ||   ||
|-
!   | Primeval (film)|Primeval
| Michael Katleman, Gideon Emery || Brooke Langton, Jürgen Prochnow ||   ||  
|-
!   |  
| Michael Hurst || Claire Lams, Lance Henriksen, Amy Manson ||    ||  
|- Putevoy obkhodchik
| Igor Shavlak || Dmitriy Orlov, Yuliya Mikhailova, Oleg Kamenshchikov ||   ||    
|- The Rage
| Robert Kurtzman || Reggie Bannister, Andrew Divoff, Rachel Scheer ||   ||  
|- The Raven
| David DeCoteau || Andre Velts, Litha Booi, Traverse Le Goff ||    ||  
|-
!   | Razortooth
| Patricia Harrington || Kathleen LaGue, Doug Swander, Josh Gad ||   ||
|-
!   | The Reaping Stephen Hopkins || Hilary Swank, David Morrissey, Idris Elba ||   ||  
|-
!   | REC (film)|REC
| Jaume Balagueró, Paco Plaza || Manuela Velasco, Ferran Terraza, Jorge-Yamam Serrano ||   ||  
|-
!   | The Redsin Tower
| Fred Vogel || Bethany Newell, Perry Tiberio, Jessica Kennedy ||   ||
|-
!   | Return to House on Haunted Hill Victor Garcia || Cerina Vincent, Andrew Lee Potts, Amanda Righetti ||   || Direct-to-video 
|-
!   |  
| Russell Mulcahy || Milla Jovovich, Oded Fehr, Ali Larter ||   ||  
|-
!   | Revamped
| Jeff Rector || Christa Campbell, Martin Kove, Carel Struycken ||   ||  
|-
!   |  
| Sebastian Gutierrez || Lucy Liu, Michael Chiklis, Carla Gugino ||   ||  
|-
!   | Rogue (film)|Rogue
| Greg McLean || Radha Mitchell, Michael Vartan, John Jarratt ||    ||
|-
!   | Saw IV Scott Patterson ||   ||  
|-
!   | The Seamstress David Kopp ||   ||  
|-
!   | Secrets of the Clown
| Ryan Badalamenti || Tami Badalamenti, Kelli Clevenger, Micheal Kott ||   ||  
|-
!   | Seed (2007 film)|Seed
| Uwe Boll || Will Sanderson, Ralf Möller, Michael Paré ||   ||  
|-
!   | Shadows (2007 film)|Senki
| Milcho Manchevski || Sabina Ajrula, Vesna Stanojevska, Borce Nacev ||       ||
|- Shadow Puppets
| Michael Winnick || James Marsters, Jolene Blalock, Tony Todd ||   ||  
|-
!   | Sick Girl
| Eben McGarr || Leslie Andrews, Stephen Geoffreys, John McGarr ||   ||  
|-
!   | The Shadow Within
| Silvana Zancolo || Hayley J Williams, Rod Hallett, Georgia Mitchell ||   ||
|-
!   | Shrooms (film)|Shrooms
| Paddy Breathnach || Lindsey Haun, Alice Greczyn, Max Kasch ||     ||  
|-
!   | Sick Nurses
| Piraphan Laoyont, Thodsapol Siriwiwat || Ase Wang, Philip Hersh, Libby Brien ||   ||  
|-
!   | Sigma Die!
| Michael Hoffman Jr. || Reggie Bannister, Heather Zagone, Nikie Zambo ||   ||  
|- The Signal
| Jacob Gentry, David Bruckner, Dan Bush || A.J. Bowen, Anessa Ramsey, Justin Welborn ||   ||  
|-
!   | Skin Crawl
| Justin Wingenfeld || Misty Mundae, Rodney Gray, Debbie Rochon ||   || Direct-to-video
|-
!   | Skinwalkers (2006 film)|Skinwalkers
| James Isaac || Jason Behr, Elias Koteas, Rhona Mitra ||   ||  
|- Skull & Michael Burke, Jared DiCroce ||   || Direct-to-video 
|-
!   | Slasher (2007 film)|Slasher
| Frank W. Montag || Thomas Kercmar, Patrick Dewayne, Vivien Walter ||   ||  
|-
!   | Socket (film)|Socket Matthew Montgomery ||   ||  
|-
!   | Solstice (film)|Solstice
| Daniel Myrick || Shawn Ashmore, Amanda Seyfried, Hilarie Burton, Elisabeth Harnois ||   || Direct-to-video 
|- Somebody Help Me
| Christopher B. Stokes || Marques Houston, Omarion, Jessica Friedman ||   || Direct-to-video 
|- Something Beneath
| David Winning || Kevin Sorbo, Brittany Scobie, Brendan Beiser ||   ||  
|-
!   | Species&nbsp;– The Awakening
| Nick Lyon || Edy Arellano, Helena Mattsson, Marco Bacuzzi ||   || Direct-to-video 
|-
!   | Splatter Disco Richard Griffin || Ken Foree, Sarah Nicklin, Debbie Rochon ||   ||  
|-
!   |  
| Ernie Barbarash || Rob Lowe ||   || Direct-to-video 
|-
!   | Sublime (film)|Sublime
| Tony Krantz || Tom Cavanagh, Kathleen York, Lawrence Hilton-Jacobs ||   ||
|-
!   |  
| Tim Burton || Johnny Depp, Helena Bonham Carter, Alan Rickman ||   ||
|-
!   | The Suicide Song (Densen Uta) Hiroshi Abe ||   ||  
|-
!   | Sympathy (2007 film)|Sympathy
| Andrew Moorman || Marina Shtelen, Aaron Boucher, Steven Pritchard ||   ||
|-
!   | The Tattooist
| Peter Burger || Jason Behr, Caroline Cheong ||   || Direct-to-video 
|-
!   | Teeth (film)|Teeth
| Mitchell Lichtenstein || Jess Weixler, John Hensley ||   ||  
|-
!   |  
| Joe Castro || Shane Ballard, Emma Bing, Bart Burson, David Alan Graf, Brinke Stevens ||   || Direct-to-video
|-
!   | Timber Falls
| Tony Giglio || Brianna Brown, Josh Randall, Beth Broderick ||   ||  
|- Tooth And Nail Mark Young || Michael Madsen, Rider Strong, Vinnie Jones ||   ||  
|-
!   | Torment
| Steve Sessions || Suzi Lorraine, Ted Alderman, Jeff Dylan Graham ||   ||  
|-
!   | Putevoy obkhodchik|Trackman
| Igor Shavlak || Yuliya Mikhailova, Oleg Kamenshchikov, Aleksei Dmitriyev ||   ||  
|-
!   | Trick r Treat
| Michael Dougherty || Dylan Baker, Rochelle Aytes, Anna Paquin, Brian Cox ||   ||
|-
!   | Undead or Alive
| Glasgow Phillips || Chris Kattan, James Denton ||   ||  
|-
!   | Underbelly (film)|Underbelly
| Matt A. Cade || Felicia Bianca Lopez, Hawk Storm, Jennifer Harlow ||   ||  
|-
!   | Unearthed (film)|Unearthed
| Matthew Leutwyler || Emmanuelle Vaugier, Luke Goss, Beau Garrett ||   ||  
|-
!   | Unholy (2007 film)|Unholy
| Daryl Goldberg || Adrienne Barbeau ||   || Direct-to-video 
|- The Unseeable
| Wisit Sasanatieng || Siraphan Wattanajinda, Tassawan Seneewongse, Suporntip Chuangrangsri ||   ||  
|-
!   | Vacancy (film)|Vacancy
| Nimród Antal || Luke Wilson, Ethan Embry, Kate Beckinsale ||   ||  
|-
!   | Someone Behind You|Voices
| Ki-hwan || Jin-seo, Ki-woo Lee ||   ||
|-
!   | They Wait
| Ernie Barbarash || Vicky Huang, Cheng Pei-pei, Jaime King ||   ||
|- War of the Living Dead 2&nbsp;– Girls, Zombies and RocknRoll!
| Mark E. Poole || Jason Crowe, Tucky Williams, Chris Albro ||   ||  
|-
!   |  
| Gregory C. Parker, Christian Pindar || Art Longley, Kevin Shea, Tamara Malawitz ||   ||  
|- When Night Falls
| Alex Galvin || Tania Nolan, Rosella Hart, Mike Bodnar ||   ||
|-
!   | Whisper (film)|Whisper
| Stewart Hendler || Joel Edgerton, Sarah Wayne Callies, Michael Rooker ||    ||
|-
!   |  
| Patrick Lussier || Nathan Fillion, Katee Sackhoff, Craig Fairbrass ||    ||
|- Wind Chill
| Greg Jacobs || Emily Blunt, Ashton Holmes ||    ||  
|- The Wizard Of Gore
| Jeremy Kasten || Crispin Glover, Brad Dourif ||   ||  
|-
!   | The Woods Have Eyes
| Anthony Indelicato || Frank Adonis, Darla Delgado, Ashley Totin ||   ||
|-
!   |   Joe Lynch || Erica Leerhsen, Texas Battle, Henry Rollins ||   || Direct-to-video 
|-
!   |  
| Kenta Fukasaku || Ami Suzuki, Nao Matsushita, Ayuko Iwane ||   ||  
|-
!   | Zibahkhana
| Omar Ali Khan || Osman Khalid Butt, Ashfaq Bhatti, Rubya Chaudhry ||   || Direct-to-video
|}

==References==
 
 
 
 

 
 
 
 