Songs and Saddles
{{Infobox film
| name           = Songs and Saddles
| image_size     =
| image	=	Songs and Saddles FilmPoster.jpeg
| caption        =
| director       = Harry L. Fraser Arthur Alexander Max Alexander (producer) Alfred Stern (associate producer)
| writer         = Arthur A. Brooks (story) Harry L. Fraser (screenplay)
| narrator       =
| starring       = See below
| music          =
| cinematography = Robert E. Cline Harry Forbes
| editing        = Charles Henkel Jr.
| studio = Road Show Pictures, Inc.
| distributor    =
| released       = 1 June 1938
| runtime        = 59 minutes
| country        = USA
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}

Songs and Saddles is a 1938 American film directed by Harry L. Fraser.

== Cast ==
*Gene Austin as Gene Austin
*Lynne Berkeley as Carol Turner
*Henry Roquemore as Lawyer Jed Hill
*Walter Wills as Pop Turner
*Ted Claire as Mark Bower
*Joan Brooks as Lucy
*Karl Hackett as Banker George Morrow Charles King as Road boss Falcon
*John Merton as Henchman Rocky Renaut
*Candy Hall as Musician Slim
*Coco Heimel as Musician Porky John Elliott as Sheriff John Lawton
*Ben Corbett as Henchman Sparks
*Bob Terry as Henchman Klinker
*Lloyd Ingraham as Judge Harrison

== Soundtrack ==
*Gene Austin with Candy Hall and Coco Heimel - "Song of the Saddle" (Written by Gene Austin)
*Gene Austin - "Im Comin Home" (Written by Gene Austin)
*Gene Austin with Candy Hall and Coco Heimel - "I Fell Down and Broke My Heart" (Written by Gene Austin)
*Gene Austin - "Why Cant I Be Your Sweetheart Tonight?" (Written by Gene Austin)
*Gene Austin - "The Man From Texas" (Written by Gene Austin)

== External links ==
* 
* 

 

 
 
 
 
 
 
 
 
 


 
 