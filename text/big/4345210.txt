Liegen lernen
{{Infobox film
|name=Learning to Lie Original title:Liegen lernen
|director=Hendrik Handloegten
|writer=Hendrik Handloegten Frank Goosen
|starring=Fabian Busch Susanne Bormann Fritzi Haberlandt Birgit Minichmayr Florian Lukas
|producer=Maria Köpf
|distributor=X-film Corp.
|released=  
|runtime= 87 min.
|language=German
}}

Liegen lernen ( ) is a German film released in 2003. It is based on the novel of Frank Goosen from the year 2000.

== Plot ==
Beginning in West Germany in 1982, 18-year old school boy Helmut falls in love with fellow pupil Britta. He starts working for a Peace movement to get to know Britta. Britta, however, suddenly moves to San Francisco to live with her father and whilst there, finds a new boyfriend. Helmut studies literature and politics in his home town and has a relationship with another girl from his former school, now studying medicine on the same university but they break up after having an affair with her room mate. Helmut begins a lot of short affairs with different women but still searches for his first girl.

Years later, in 1989, the Berlin wall falls and the Cold war ends. Helmut is lying in his girlfriends bed when his old school friend Mücke calls him, who saw Britta in Berlin. Helmut hurries to Berlin and finds Britta, but she has changed and has become arrogant. Mücke tells him afterwards that he had an affair with Britta and that Britta has had a lot of affairs. Worried by this, Helmut begins a lot of new affairs and his parents get divorced at that time. 
In 1998, Helmut settles down with a girl who wants a baby with him, but Helmut travels one more time to Berlin to meet Britta for one last time.

== The novel ==
The film is based on the novel Liegen lernen by Frank Goosen from the year 2000. While the novel takes place in Bochum, the film was set in Düsseldorf. The novel has a lot of biographical moments from Goosen, but the story is typical for a West German youth in the 1980s.  The novel became very popular through the great 80s revival in western Germany around the year 2000.

==Cast==
* Fabian Busch as Helmut
* Susanne Bormann as Britta
* Fritzi Haberlandt as Gisela
* Sophie Rois as Barbara
* Anka Sarstedt as Gloria
* Birgit Minichmayr as Tina
* Florian Lukas as Mücke
* Tino Mewes as the long Schäfer
* Sebastian Münster as Beck
* Beate Abraham as Helmuts Mother
* Wilfried Dziallas as Helmuts father
* Uwe Rohde as Uwe
* André Meyer as Rüdiger
* Jean Pierre Cornut as Professor Mutter
* Heinz Schubert as uncle Bertram

==Crew==
* Direction: Hendrik Handloegten
* Script: Frank Goosen and Hendrik Handloegten
* Production: Maria Köpf
* Music: Dieter Schleip
* Camera: Florian Hoffmeister
* Editing: Elena Bromund

==External links==
*  
*  
*  

 
 
 
 
 
 
 