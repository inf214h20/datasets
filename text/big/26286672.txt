Face the Music (film)
 
 
 
{{Infobox film
| name           = Face the Music 
| image          = The Black Glove poster.jpg
| image_size     = 
| caption        = Theatrical release poster
| director       = Terence Fisher
| producer       = Michael Carreras
| writer         = 
| narrator       = 
| starring       = Alex Nicol
| music          = 
| cinematography = Walter J. Harvey
| editing        = Maurice Rootes
| studio         = Hammer Film Productions
| distributor    = Lippert Pictures (USA) Exclusive Films (UK)
| released       = 29 January 1954
| runtime        = 84 min.
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
| amg_id         = 
}}
Face the Music (released in the U.S. as The Black Glove) is a 1954 British crime drama film directed by Terence Fisher. It stars Alex Nicol and Eleanor Summerfield. 

==Cast==
*Alex Nicol as James Bradley
*Eleanor Summerfield as Barbara Quigley
*John Salew as Maxie Maguilies Paul Carpenter as Johnny Sutherland
*Geoffrey Keen as Maurie Green  
*Ann Hanslip as Maxine Halbard  
*Fred Johnson as Detective Sergeant MacKenzie  
*Martin Boddey as Inspector Mulrooney   Arthur Lane as Jeff Colt

==References==
 

==External links==
* 

 
 

 
 
 
 
 
 
 


 