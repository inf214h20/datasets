Crimson Tide (film)
{{Infobox film
| name           = Crimson Tide
| image          = Crimson tide movie poster.jpg
| caption        = Theatrical release poster
| director       = Tony Scott
| producer       = Don Simpson Jerry Bruckheimer
| screenplay     = Michael Schiffer Quentin Tarantino  (uncredited) 
| story          = Michael Schiffer Richard P. Henrick
| starring       = Denzel Washington Gene Hackman 
| music          = Hans Zimmer
| cinematography = Dariusz Wolski
| editing        = Chris Lebenzon Bruckheimer
| Buena Vista Pictures
| released       =  
| runtime        = 116 minutes
| country        = United States
| language       = English
| budget         = $53 million 
| gross          = $157,387,195
}} submarine action ultranationalists threaten to launch nuclear missiles at the United States and Japan. It focuses on a clash of wills between the new executive officer (Denzel Washington) and the seasoned commanding officer (Gene Hackman) of a nuclear missile submarine, arising from conflicting interpretations of an order to launch their missiles. The title is derived from a play on words, invoking a former communist maritime threat, and the nickname of the University of Alabama is the Crimson Tide, the submarine in the film is named the USS Alabama.

The film was scored by Hans Zimmer, who won a Grammy Award for the main theme, which makes heavy use of synthesizers in place of traditional orchestral instruments.

==Plot== armed conflict nuclear war if either the American or Russian governments attempt to confront him.
 Captain Frank Lieutenant Commander Ron Hunter (Washington), who has an extensive education in military history and tactics, but no combat experience.

During their initial days at sea, tension between Ramsey and Hunter becomes apparent due to a clash of personalities: Hunter is more analytical and cautious, while Ramsey is more intuitive and impulsive. This becomes evident when Ramsey orders a mock missile launch drill while Hunter is tending to an out-of-control grease fire in the ships galley.  Hunter eventually responds to the drill, but not before hesitation due to disagreement.  The fire is quenched, but the ships mess chief, Marichek, suffers a coronary and dies.  Ramsey terminates the drill upon receiving this news.  Afterwards, Ramsey explains to Hunter that chaos is the perfect opportunity to test the crews abilities, but Hunter fears crew morale will suffer due to constant testing and failure.  The next day, Hunter observes two crewmen fighting over a seemingly trivial matter, confirming his fear.  He tells Ramsey, but Ramsey is unconcerned, even going so far as to embarrass Hunter over the ships 1MC and telling the crew to stay focused on the mission.

The Alabama eventually receives an Emergency Action Message, ordering the launch of ten of its missiles against the Russian nuclear installation, based on satellite information that the Russians missiles are being fuelled. Before the Alabama can launch its missiles, a second radio message begins to be received, but is cut off by the attack of a Russian   loyal to Radchenko.

The radio electronics are damaged in the attack and cannot be used to decode the second message. With the last confirmed order being to launch, Captain Ramsey decides to proceed. Hunter refuses to concur - which is procedurally required to launch - because he believes the partial second message may be a retraction. Hunter argues that the Alabama is not the only American submarine in the area, and if the order is not retracted, other submarines will launch their missiles as part of the fleets standard redundancy doctrine. Ramsey argues that the other American submarines may have been destroyed.

When Hunter refuses to consent to launch, Ramsey tries to relieve him of duty and to replace him with a different officer. Instead, Hunter orders the arrest of Ramsey for attempting to circumvent protocol. The crews loyalty is divided between Hunter and Ramsey, but Hunter initially takes command. The Alabama is attacked again by the Russian submarine. The Alabama destroys it, but not before the Russian submarine fires two torpedoes.  One torpedo misses, but the other hits the Alabama, flooding the bilge bay and disabling the submarines communications and main propulsion.

The crew tries desperately to restore what has been damaged.  Three crewmen are attempting to stop the bilge bay flooding, but Hunter reckons that, if the bay is not sealed, too much water will be taken on to enable the sub to rise once propulsion is restored.  Hunter orders the crewmen to exit and the bilge bay to be sealed.  Unfortunately, the men are unable to exit the bilge bay in time, and the compartment is sealed with them inside, forcing them to drown.  Shortly before the Alabama reaches crush depth, the propulsion systems come back on line, and Hunter orders the sub to proceed to near-surface level.  Once repairs are complete, this would allow the Alabama to re-establish communications and receive the cut-off message.
 Lieutenant Dougherty, who is loyal to Ramsey, organizes a mutiny against Hunter with Lieutenants Zimmer and Westegard.  They try to recruit Lieutenant "Weps" Ince, but Weps is hesitant, having been friends with Hunter for a long time and feeling Hunter may be right.  Ramsey and the others draw small arms and recover the bridge from Hunter.  Ramsey then proceeds with the missile launch order, and engages the missile lock key at the bridge.

Hunter escapes his arrest and gains the support of Weps in the missile control room, further delaying the launch. Ramsey leaves the bridge and tries to force Weps to open the safe containing the firing trigger.  Weps initially refuses, but relents after Ramsey threatens to shoot another crew member.  Meanwhile, Hunter and the crew members siding with him stage another retaking of the bridge.  Hunter removes the missile key just as Ramsey pulls the trigger to fire a missile.  Ramsey returns to the bridge and demands Hunter hand over the missile key, but Hunter is resolute.  With the radio team reporting their repairs are almost ready, the two men agree to a compromise; they will wait until the deadline to see if the radio can be repaired.  After several tense minutes, communications are restored and they finally see the full message from the second transmission. It is a retraction ordering that the missile launch be aborted, because Radchenkos rebellion has been quelled.  A chastened Ramsey leaves the bridge.

After returning to base, Ramsey and Hunter are put before a naval tribunal to answer for their actions. The tribunal concludes that both men were simultaneously right and wrong, so Hunters actions were lawfully justified.  Unofficially, the tribunal chastises both men for failing to resolve the issues between them. Thanks to Ramseys personal recommendation, the tribunal agrees to grant Hunter command of his own sub, while allowing Ramsey to save face via an early retirement. Both men then reconcile their differences and part ways.

==Cast==
* Denzel Washington as Lieutenant Commander Ron Hunter 
* Gene Hackman as Captain Frank Ramsey
* George Dzundza as Chief of the Boat Walters
* Viggo Mortensen as Lieutenant Peter "Weps" Ince
* James Gandolfini as Lieutenant Bobby Dougherty
* Matt Craven as Lieutenant Roy Zimmer
* Rocky Carroll as Lieutenant Darik Westergard
* Danny Nucci as Petty Officer Danny Rivetti
* Michael Milhoan as Chief of the Watch Hunsicker
* Steve Zahn as Seaman William Barnes
* Rick Schroder as Lt. Paul Hellerman
* Lillo Brancato, Jr. as Petty Officer Third Class Russell Vossler
* Ryan Phillippe as Seaman Grattam
* Daniel von Bargen as Vladimir Radchenko
* Jason Robards as Rear Admiral Anderson, Board of Inquiry President (uncredited)

==Production==
{{listen
|filename=Roll tide wiki.ogg
|title=Hans Zimmer - "Roll Tide"
|description=listen to a clip from the score of the 1995 film Crimson Tide.
}}
 Grammy Award in 1996, and Zimmer has described it as one of his personal favorites. 

The film has uncredited additional writing by Quentin Tarantino, much of it being the pop-culture reference-laden dialogue.    
 Triomphant class SNLE (SSBN).

Because of the U.S. Navys refusal to cooperate with the filming, the production company was unable to secure footage of a submarine submerging. After checking to make sure there was no law against filming naval vessels, the producers waited at the submarine base at Pearl Harbor until a submarine put to sea. After a submarine (coincidentally, the real USS Alabama) left port, they pursued it in a boat and helicopter, filming as they went. They continued to do so until it submerged, giving them the footage they needed to incorporate into the film. 

==Reception==

===Box office===
Crimson Tide earned $18.6 million in the United States on its opening weekend, which ranked #1 for all films released that week. Overall, it earned $91 million in the U.S. and an additional $66 million internationally, for a total of $157.3 million.   

===Critical reception===
The film received mostly positive reviews from critics. Review aggregator Rotten Tomatoes reports that 87% of 46 critics have given the film a positive review, with a rating average of 7.5 out of 10.    A number of critics cited Hackman and Washingtons performances, and enjoyed the films snappy, pop culture inflected dialogue.

Roger Ebert of the Chicago Sun-Times wrote, "This is the rare kind of war movie that not only thrills people while theyre watching it, but invites them to leave the theater actually discussing the issues," and ultimately gave the film three-and-a-half stars out of four.  Meanwhile, Mick LaSalle of the San Francisco Chronicle wrote, "Crimson Tide has everything you could want from an action thriller and a few other things you usually cant hope to expect." 

Owen Gleiberman of Entertainment Weekly wrote that, "what makes Crimson Tide a riveting pop drama is the way the conflict comes to the fore in the battle between two men. ... The end of the world may be around the corner, but what holds us is the sight of two superlatively fierce actors working at the top of their game." 

In contrast, Janet Maslin of The New York Times criticized the films "blowhardiness" and superficial treatment of apocalyptic fears. She noted that there is "... something awfully satisfying about the throbbing missiles and cathartic explosions that constitute this films main excitement," but felt that "... nothing else here delivers a comparable thrill." 

===Awards=== Academy Awards, Sound (Kevin Kevin OConnell, Rick Kline, Gregory H. Watkins and William B. Kaplan) and Sound Editing (George Watters II).    

==References==
 

==External links==
 
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 