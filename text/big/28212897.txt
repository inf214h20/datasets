That Girl in Yellow Boots
 
 
{{Infobox film
| name           = That Girl in Yellow Boots
| image          = That Girl in Yellow Boots.jpg
| director       = Anurag Kashyap
| producer       = Anurag Kashyap   Guneet Monga
| writer         = Anurag Kashyap Kalki Koechlin
| starring       = Kalki Koechlin Naseeruddin Shah Benedict Taylor
| cinematography = Rajeev Ravi
| editing        = Shweta Venkat Mathew
| studio         = Sikhya Entertainment
| distributor    = IndiePix Films
| released       =  
| runtime        = 99 Minutes
| country        = India
| language       = Hindi and English
| duration       = 99 minutes
}}

That Girl in Yellow Boots is a 2011 Indian thriller film by director Anurag Kashyap, starring Kalki Koechlin and Naseeruddin Shah.  The film was first screened at the Toronto International Film Festival in September 2010, followed by the Venice Film Festival after it played in several festivals worldwide including the South Asian International Film Festival.  The commercial release however took place a year later in September 2011, both in India as well as in the US 

==Plot==
That Girl in Yellow Boots is a thriller tracing Ruth (Kalki Koechlin), a British woman who comes to Mumbai to search for her father – a man she hardly knew but cannot forget. Without a work permit, desperation drives her to work at a massage parlour. Torn between several schisms, Mumbai becomes the alien but yet strangely familiar backdrop for Ruths quest. She struggles to find her independence and space even as she is sucked deeper into the labyrinthine politics of the citys underbelly. She starts dating a drug addict Prashant (Prashant Prakash). A city that feeds on her misery, a love that eludes her and above all, a devastating truth that she must encounter. And everyone wants a piece of her. After numerous encounters with people, almost all of whom are depicted as needing to be serviced by her, she discovers that her father is one amongst her regular clients, who knew all along that she was his daughter. In what is possibly seen as a commentary on the cult of godmen in India, her father is shown as one such member of a religious cult, and views having sex with his daughter as an expression of his love. The film ends with Ruth hanging up her yellow boots, her quest having come to a shocking end.

==Cast==
* Kalki Koechlin as Ruth
* Naseeruddin Shah as Diwakar
* Gulshan Devaiya as Chittiappa
* Shiv Kumar Subramaniam
* Mushtaq Khan
* Ronit Roy (Cameo)
* Makrand Deshpande (Cameo)
* Piyush Mishra(Cameo)
* Rajat Kapoor (Cameo)
* Divya Jagdal as Divya
* Kumud Mishra as Lynn
* Prashant Prakash as Prashant
* Pooja Swaroop as Maya
* Kartik Krishnan

==Production==

===Development===
Lead actress Kalki Koechlin who also co-wrote the film with Anurag Kashyap mentioned, "A lot of these characters were based loosely on figures that I had seen growing up in India ...Growing up as a white-skinned woman in India, I was always the odd one out – there was a certain alienation that came with that, and you end up alienating yourself because everyone comes to you like the white girl, the easy, "Baywatch," loose-moraled white girl." 
 I Am (2010) by Onir, and he himself had been a victim of child abuse for 11 years.   At the writing stage Koechlin and Kashyap disagreed on the ending initially, as Koechlin wanted an optimistic ending, unlike Kashyap who wanted to portray that "...you dont always get solutions to your problems". 

The film had difficulty finding funding because it dealt with controversial themes like child abuse and drug addiction and "differed so vastly from his previous work". As Kashyap put it, "I wanted to break the formula that many directors and actors find themselves in." 

===Filming===
The film was shot in just 13 days. It was primarily framed in tight spaces, like apartments, massage parlors, and rickshaws leading to a "claustrophobic sense of unease that permeates the entire film".  Many of the cast members had previously worked together in theatre productions; this familiarity allowed the director to shoot the film a shorter period of time. He admitted that he never "directed" any of the actors during the filming, "Ive never told any actor what to do, only what not to do. You have to trust your actors, and I know mine inside and out."  Director Anurag Kashyap found the entire filming emotionally draining and tough, especially because it was made mostly on borrowed money. 

==Release==
After travelling to 2010 Toronto International Film Festival, 67th Venice International Film Festival in September 2010 and International Film Festival of Los Angeles (IFFLA),  at its New York premiere on 24 August 2011, at the Asia Society, director Anurag Kashyap said, "I hope you feel the film, because you will not enjoy it."       The films commercial release, however, took over a year as it was delayed to coincide with its US release to avoid internet piracy.  Indian distributors were not keen on the film, as without big Bollywood stars they did not find it viable for an international release; they mainly cater to an NRI (Non-resident Indian) audience. Finally US based-distributor Indie Pix came on board for paving the way for a US release with 30 prints, all in non-NRI theatres, a rare feat for a Bollywood film. Meanwhile the film was also sold in Scandinavian countries, Turkey, Southern Europe, and New Zealand. Its satellite rights were sold in many countries.  The film thus became Kashyaps first worldwide release, as it was released in 40 US theatres on 2 September by IndiePix Films, on the same day of its India release. Previously, after its showing at the London Indian Film Festival, Britain-based Mara Pictures picked up the film there for UK release in last quarter of 2011.     Kashyap later told BBC News that he received a negative backlash from financial backers because of the films sexual content: "A lot of people involved with the film were embarrassed about the film. A lot of people we thanked in the film who actually lent us money, they said, Please take our names from the film, because they dont want somebody to see and say You gave the money to make this film!" 

===Marketing===
Prior to its India release, the first look of the film was unveiled to the press on 11 August 2011. 
MTV India started a "That Girl with Yellow Boots contest" asking for audition tapes from aspiring actors, the winner of which would act in future Anurag Kashyaps films.  In the run up to the film, its lead Kalki Koechlin appeared at an event, colour-coordinated, complete with yellow boots. 

===Critical reception===
The film opened to mostly positive reviews. Roger Ebert of the Chicago Sun-times gave it 3.5 out of the 4 stars, and he also noted that  The films value is in its portrait of Ruth, and her independence as a solo outsider in a vast, uncaring city.   EF News International, wrote, "Kalki Koechlin carries That Girl in Yellow Boots on her shoulders and does so with great panache and élan."  Shivesh Kumar of IndiaWeekly awarded the movie 3.5 out of 5 stars.   

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 