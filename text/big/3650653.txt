The Sword in the Stone (film)
{{Infobox film
| name           = The Sword in the Stone
| image          = SwordintheStonePoster.JPG
| caption        = Original theatrical release poster
| director       = Wolfgang Reitherman
| producer       = Walt Disney
| screenplay     = Bill Peet
| story          = Bill Peet
| based on       =   Sebastian Cabot Norman Alden Martha Wentworth
| music          = George Bruns
| editing        = Donald Halliday Walt Disney Productions Buena Vista Distribution
| released       =  
| runtime        = 79 minutes
| country        = United States
| language       = English
| budget         = $4 million  
| gross          = $22.2 million 
}} musical fantasy fantasy comedy Buena Vista Walt Disney Mary Poppins The Jungle Book (1967), and Bedknobs and Broomsticks (1971).
 novel of the same name, first published in 1938 as a single novel. It was later republished in 1958 as the first book of T. H. Whites tetralogy The Once and Future King.

== Plot ==
  King of Dark Ages.
 Kay was hunting, causing Kay to launch his arrow into the forest. In retrieving the arrow, Arthur lands in the cottage of Merlin the wizard, who declares himself Arthurs tutor and returns with the boy to his home, a castle run by Sir Ector, Arthurs foster father. Ectors friend, Sir Pellinore, arrives with news that the annual jousting tournament will be held on New Years Day in London, and the winner will be crowned king. Ector decides to put Kay through serious training for the tournament and appoints Arthur as Kays squire.
 pike but squirrels to learn about gravity. Arthur is nearly eaten by a gray wolf|wolf, but is saved by a female squirrel that falls in love with him. After they return to human form, Ector accuses Merlin of using black magic on the dishes. Arthur defiantly defends Merlin, but Ector refuses to listen and punishes Arthur by giving Kay another squire, Hobbs.

Resolving to make amends, Merlin plans on educating Arthur full-time. However, Merlins knowledge of future history causes confusion, prompting Merlin to appoint Archimedes as Arthurs teacher. When Arthur imagines what it would be like to fly, Merlin transforms him into a sparrow and Archimedes teaches Arthur how to fly. However, during their lesson Arthur is attacked by a hawk and falls into the witch Madam Mims chimney. Mims magic uses trickster|trickery, as opposed to Merlins scientific skill. Merlin arrives to rescue Arthur just as Mim is about to destroy him. She then challenges Merlin to a Wizards Duel. Despite Mims cheating, Merlin outsmarts her by transforming into a germ that infects her with a Chickenpox-like disease illustrating that knowledge is more important than strength.

On Christmas Eve, Kay is knighted, but Hobbs comes down with the mumps, forcing Ector to reinstate Arthur as Kays squire. Merlin, however, is disappointed and angry that Arthur again is choosing being a squire over being educated. When Arthur tries to reason with the wizard, Merlin angrily launches himself to Bermuda. On the day of the tournament, Arthur realizes that he has left Kays sword at an inn, which is now closed for the tournament. Archimedes sees the Sword in the stone, which Arthur removes almost effortlessly, unknowingly fulfilling the prophecy. When Arthur returns with the sword, Ector recognizes it as the legendary "Sword in the Stone" and the tournament is halted. Demanding Arthur to prove that he pulled it, Ector replaces the sword in its anvil. Thinking anyone can pull the sword now, Kay and others try to retrieve the sword, but it appears as stuck as ever. As another knight declares this to be unfair Pellinore urges the crowd to allow Arthur to try it again, and once again he removes the sword, revealing that he is Englands new king.

Arthur, crowned king, sits in the throne room with Archimedes, feeling unprepared to take the responsibility of royalty. Overwhelmed by the cheering crowd outside, Arthur calls out to Merlin for help, who returns from Bermuda (and the 20th century) and is elated to find that Arthur is the King that he saw in the future. Merlin tells the boy that he will lead the Knights of the Round Table, becoming one of the most famous figures in history and even in motion pictures.

== Cast and characters ==
 
* Rickie Sorensen, Richard Reitherman and Robert Reitherman as Arthur, also known as Wart. He is Disneys adaptation of legendary British leader King Arthur. Arthur was voiced by three actors, leading to noticeable changes in voice between scenes. Also, the three voices have Brooklyn-esque accents, sharply contrasting with the English setting and the accents spoken by all other characters in the film. Frank Thomas, Ollie Johnston, and John Lounsbery. Kahl designed the character, refining the storyboard sketches of Bill Peet. Merlin can be recognized by his massive beard, which gets caught in most of his machines, and a pair of glasses he wears. He is the worlds most powerful wizard.
* Junius Matthews as Archimedes, Merlins crotchety, yet highly educated pet owl, who has the ability of speaking and is the comic relief of the film. Archimedes accompanies Arthur during training, and it is he who alerts Merlin after Arthur falls into Madam Mims cottage and she almost kills him. Archimedes stays with Arthur while Merlin travels to 20th-century Bermuda. Sebastian Cabot as Sir Ector, the ruler of King Uther Pendragons castle and the foster father of Arthur. He does not believe in magic until Merlin casts a blizzard before him, thus allowing the wizard to educate Arthur in the castle, even though Ector has forbidden it. Though he loves Arthur, Ector often treats him harshly. Cabot also provides the brief narration at the beginning and end of the film.
* Norman Alden as Sir Kay, the older foster brother of Arthur. He is inept at jousting and sword fighting. Though he loves Arthur, he often treats him with contempt. Nine Old Frank Thomas. Kahl animated her initial interaction with Arthur, while Thomas oversaw her part of the Wizards Duel with Merlin. Wentworth also voiced the Granny Squirrel, a dim-witted, elderly female squirrel that develops an attraction to Merlin.
* Alan Napier as Sir Pellinore, a friend of Sir Ector who announces the tournament where Arthur is revealed as king.
* Thurl Ravenscroft as Sir Bart, also known as the Black Knight, one of the first to recognize the sword pulled by Arthur from the stone. James MacDonald as The Wolf, an unnamed, starving wolf that wants to eat Wart. He was defeated and never seen again after getting trapped in a log in the squirrel scene.
* Ginny Tyler as The Little Girl Squirrel, a young female squirrel that Wart come across. She immediately develops an attraction to him. After she saves him from the wolf and Wart returns to human form, she breaks down into tears and runs away. She is last seen watching Wart and Merlin leave the forest, heartbroken, and crying as the screen fades to black.
* Barbara Jo Allen as Scullery Maid

== Production == Ken Anderson and Marc Davis who aimed to produce a feature animated film in a more contemporary setting. Both of them had visited the Disney archives, and decided to adapt the satirical tale into production upon glancing at earlier conceptions dating back to the 1940s.  Anderson, Davis, Milt Kahl, and director Wolfgang Reitherman spent months preparing elaborate storyboards for Chanticleer, and following a silent response following a story reel presentation, a voice from the back of the room said, "You cant make a personality out of a chicken!" The voice belonged to Bill Peet.  When the time came to approve one of the two projects, Walt replied to Andersons pitch with "Just one word—shit!"  Meanwhile, work on The Sword in the Stone were solely done by veteran story artist Bill Peet. After Disney had seen the 1960 Broadway production of Camelot (musical)|Camelot, he approved the project to enter production.  Ollie Johnston stated, "  got furious with Bill for not pushing Chanticleer after all the work he had put in on it. He said, I can draw a damn fine rooster, you know. Bill said, So can I."  Peet recalled "how humiliated they were to accept defeat and give in to The Sword in the Stone...He allowed to have their own way, and they let him down. They never understood that I wasnt trying to compete with them, just trying to do what I wanted to work. I was the midst of all this competition, and with Walt to please, too."   

Writing in his autobiography, Peet decided to write a screenplay before producing storyboards, though he found the narrative "complicated, with the Arthurian legend woven into a mixture of other legends and myths" and finding a direct storyline required "sifting and sorting".    After Walt received the first screenplay draft, he told Peet that it should have more substance. On his second draft, Peet lengthened it by enlarging on the more dramatic aspects of the story, in which Walt approved of through a call from Palm Springs, Florida.    

== Reception == sixth highest theatrical rentals of $4.75 million.  However, it received mixed reviews from critics, who thought it had too much humor and a "thin narrative".    Rotten Tomatoes reports that 71% of critics gave positive reviews based on 24 reviews with an average score of 6/10. Its consenus states that "A decent take on the legend of King Arthur, The Sword in the Stone suffers from relatively indifferent animation, but its characters are still memorable and appealing."  Nell Minow of Common Sense Media gave the film four out of five stars, writing, "Delightful classic brings Arthur legend to life". 

In his book The Best of Disney, Neil Sinyard states that, despite not being well known, the film has excellent animation, a complex structure, and is actually more philosophical than other Disney features. Sinyard suggests that Walt Disney may have seen something of himself in Merlin, and that Mim, who "hates wholesome sunshine", may have represented critics. 

=== Accolades === Best Score—Adaptation or Treatment in 1963, but lost against Irma La Douce. 
 Top 10 Animated Films list. 

== Soundtrack ==
* "The Sword in the Stone" (Sung by Fred Darian)
* "Higitus Figitus" (Sung by Merlin)
* "Thats What Makes the World Go Round" (Sung by Merlin and Arthur)
* "A Most Befuddling Thing" (Sung by Merlin)
* "Mad Madam Mim" (Sung by Mim)
* "Blue Oak Tree" (Ending of the song, sung by Sir Ector and Sir Pellinore; beginning of the song deleted)
* "The Magic Key" (Deleted song)
* "The Sand of Time" (Deleted score)

== Other media == Disneyland Park. He also hosts the Sword in the Stone ceremony in the King Arthur Carrousel attraction  in Fantasyland at Disneyland. The song "Higitus Figitus" has most recently been used for Change4Lifes latest advert promoting their Disney-associated "10 minute shake up" program.

=== Comics ===
 
 Black Pete Captain Hook in several stories; in others, with Phantom Blot. In many European Disney comics, she lost her truly evil streak, and appears morbid yet relatively polite.

Mim has appeared in numerous comics produced in the United States by Studio Program in the 1960s and 1970s,  often as a sidekick of Magica. Most of the stories were published in Europe and South America. Among the artists are Jim Fletcher, Tony Strobl, Wolfgang Schäfer, and Katja Schäfer. Several new characters were introduced in these stories, including Samson Hex, an apprentice of Mim and Magica. 

=== Video games ===
Madam Mim appears in the video game World of Illusion as the fourth boss of that game.

Merlin is a supporting character in the   to counter the threat of Maleficent (Disney)|Maleficent, and he constructs a door leading to Disney Castles past (Timeless River) for the trio to explore and stop Maleficent and Petes plans. In the prequel, Kingdom Hearts Birth by Sleep, Merlin encounters Terra, Aqua and Ventus, and grants them each access to the Hundred Acre Wood. The prequel also reveals that it was Terra who gave him the book in the first place after finding it in Radiant Garden.

== See also ==
 
* List of films based on Arthurian legend

== References ==
 

== External links ==
 
*  
*  
*  
*  
*  
*  
*  

 
 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 