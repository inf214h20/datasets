IThemba
 

{{Infobox film
| italic title   = no
| name           = iThemba
| image          =
| caption        = Film poster
| director       = Elinor Burkett 
| producers      = Elinor Burkett and Errol Webber
| music          = Liyana
| cinematography = Errol Webber
| editing        = Errol Webber
| released       =  
| runtime        = 72 minutes
| country        = Zimbabwe
| language       = English
}} 
iThemba is a feature-length documentary film shot in Zimbabwe, directed and produced by Elinor Burkett and produced by Errol Webber, who also shot and edited the film. It premiered at the International Documentary Film Festival Amsterdam in November 2010. 

The film follows the members of the Zimbabwean band Liyana, a group of eight musicians with physical disabilities who navigate a country where many of their neighbors consider them to be cursed. 
The funny and talented young people take viewers with them as they travel across the city of Bulawayo and into remote villages, to rural bottle shops and urban marketplaces, inside the huts of traditional healers and the neighborhoods of the urban poor – into an Africa rarely seen by outsiders, a place where tradition is not necessarily gentle, where it threatens to trap the unfortunate, and where a few fight back.

Meet Marvelous Mbulo, the lady-killing lead singer, whose wit provides the films heart. Listen to Prudence Mabhena, Liyanas musical heart. And travel Zimbabwe with the wise-cracking Energy Maburutse, whose humor belies the seriousness of his situation.

The film is filled with an endless flow of the band’s jokes and satire and their amazing Afro-fusion melodies, most composed by members of Liyana. 

Shot during and in the wake of the Zimbabwean presidential election, 2008 and the country’s economic meltdown by an American, Zimbabwean and Jamaican team, the film unfolds against the backdrop of enormous political tension and the daily struggle to find a bank that actually had cash, to buy food although the store shelves were empty, and to navigate streets pocked with wheelchair-mangling potholes. 

==Title==
The title of the film means Hope in isiNdebele, one of the two major languages of Zimbabwe. It is drawn from the film’s title song.

==Cast==
In alphabetical order
::Farai Mabhande
::Prudence Mabhena
::Marvelous Mbulo
::Energy Maburutse
::Honest Mupatsi
::Tapiwa Nyengera
::Goodwell Nzou
::Vusani Vuma

==See also==
*Music by Prudence, a documentary film about Prudence Mabhena

== References ==
 
 
http://www.kinggeorge6.org
http://sites.google.com/site/liyanakg6
http://www.nytimes.com/2006/05/02/world/africa/02zimbabwe.html

== External links ==
*  
*  
*  
*  

 
 
 
 
 
 
 