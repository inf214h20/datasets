Lantana (film)
 
{{Infobox film
| name           = Lantana
| image          = Lantanaposter01.jpg
| caption        = Theatrical release poster Ray Lawrence
| producer       = Jan Chapman
| screenplay      = Andrew Bovell
| based on       =   Rachael Blake  Vince Colosimo  Russell Dykstra  Daniela Farinacci  Peter Phelps  Leah Purcell  Glenn Robbins Paul Kelly
| cinematography = Mandy Walker 
| editing        = Karl Sodersten Jan Chapman Films Palace Films
| released        =   
| runtime        = 121 minutes 
| country        = Australia
| language       = English
| gross          = $15.7 million   
}}
  Ray Lawrence Best Film Best Adapted Screenplay.

Lantana is set in suburban Sydney and focuses on the complex relationships between the characters in the film. The central event of the film is the disappearance and death of a woman whose body is shown at the start of the film, but whose identity is not revealed until later. The films name derives from the plant Lantana, a weed prevalent in suburban Sydney.

==Plot==
An unknown womans body is seen caught in the lantana bush, missing a shoe.  Rachael Blake) have sex in a motel room. They part ways, and we see that Leon and his wife Sonja (Kerry Armstrong) attend Latin dance classes that the recently separated Jane is also taking. 
Leon does not enjoy the classes, and is seen savagely beating a drug dealer during a bust. He has emotional issues, but wont confront or admit to them. Sonja sees a therapist, Valerie (Barbara Hershey) who has just published a book on her own daughters murder 18 months ago. She and her husband, John, (Geoffrey Rush) are barely on speaking terms; he later refers to their marriage as held together by their grief. She feels threatened by another patient of hers, Patrick Phelan, who is having an affair with a married man and is forcing Valerie to confront her own issues in her marriage to John.

Hoping to see Leon again, Jane purposely bumps into him outside the police station, and they have sex again, despite Leons reservations about taking it any further. Her next door neighbour, Nik, is upset that she is seeing someone because he is friends with her estranged husband Pete, who wants to return home. Jane pairs up with Sonja in the next salsa class, which angers Leon; he ends their arrangement, which upsets Jane. She invites Nik over for coffee at the behest of his wife, Paula, with whom she is friendly, and offers him money as they are struggling. Paula starts to dislike Jane because of it.

Valerie is coming home late one night and drives off the road. She is stranded, and makes several calls to John, who does not answer. Finally, she is seen approaching a car coming along the road, but never makes it home. Leon is the investigating detective on the case, and looks into her office and notes. Surprised at seeing his wifes name and file, he takes a recording of their sessions.

Leon arrives home late but Sonja is not asleep: he asks her about her therapy sessions with Valerie, they discuss their relationship and he tells her of his affair which had just ended, but that he still loves her. Sonja is very upset and feels betrayed. Leon sleeps on the couch, in the morning Sonja says hell be lucky if she returns home that night. Leon goes to Johns house to interrogate him as the main suspect in his wifes disappearance. Leon starts a discussion about love, marriage and having affairs but lies to John when asked if he ever had an affair. Leon goes to Janes house on police duty, because she has placed a call. Her neighbours are Paula (Daniella Farinacci), a nurse, and Nik (Vince Colosimo). Jane was up late one night, watching Nik arrive home and throw something in the bushes across the road from her house: later she finds it is a womans shoe. Leon and his partner arrive at Janes and declare that the shoe was Valeries.

The police take Nik to the station and he leaves his children with Jane. Police call Paula to come in from her work. Neither Nik nor Paula knows that Jane made the police call. Although Paula does not like Jane, she calls her to thank her for minding their children. The police interrogate Nik but he refuses to answer questions about Valerie, repeatedly asking to see Paula. After seeing his wife, Nik calms down and talks with Leon and his partner. Valerie had car trouble and Nik was driving past; he agreed to give her a lift to her home but she panicked when he took a back road short cut and left his truck. Valerie had run off leaving behind her shoe. Paula goes to Janes house to get her children, where she tells her that Nik is innocent of Valeries disappearance. Jane asks how she knows that, and Paula simply replies that he told her. Jane asks if she can spend more time with the kids, but Paula forbids it, after seeing how Jane went over into her house and tidied it. Leon, his partner, Nik, and John go to the place where Valerie jumped out of the truck. They find her body where she had accidentally fallen down a ravine. Leon listens to the rest of the therapy tape where his wife had said that she still loved him and he bursts into tears.

Leon returns home and sees his wife outside. Jane salsa dances alone, drinking and smoking, while Pete leaves her. Patrick is pained to see his lover happily spending time with his wife and kids. Nik and Paula appear to have recovered from the whole affair, and seem to have been the only honest and happy people of them all. The movie ends with Sonja and Leon dancing together seductively. Leon, who at the beginning of the film finds dancing with his wife difficult, now appears to be doing well. He looks Sonja directly in her eyes and dances just as she always wanted. Sonja struggles to initially return Leons gaze but does so just before the movie ends.

==Cast==
*Anthony LaPaglia as Leon Zat
*Geoffrey Rush as John Knox
*Barbara Hershey as Valerie Somers
*Kerry Armstrong as Sonja Zat Rachael Blake as Jane OMay
*Vince Colosimo as Nik DAmato
*Russell Dykstra as Neil Toohey
*Daniella Farinacci as Paula DAmato
*Peter Phelps as Patrick Phelan
*Leah Purcell as Claudia Weis
*Glenn Robbins as Pete OMay

==Reception==

===Box office===
Lantana opened on December 14, 2001 in North America in 6 theaters and grossed $66,701 with an average of $11,116 per theater and ranking #39 at the box office. Its widest release was 108 theaters and it ended up earning $4,623,189. The film earned $11,124,261 internationally (including $6,125,907 in Australia) for a total of $15,747,450.  

===Critical response===
Lantana received positive reviews from critics and has a certified fresh score of 90% on Rotten Tomatoes based on 103 reviews with an average rating of 7.4 out of 10. The critical consensus states "Lantana is an intricately plotted character study that quietly shines with authenticity."  The film also has a score of 84 out of 100 on Metacritic based on 29 critics indicating "universal acclaim". 

Writer and critic Roger Ebert compared the film to Short Cuts and Magnolia (film)|Magnolia in terms of how it developed the connections between the lives of strangers.  It premiered in the UK on Channel 4 in December 2006. British critic Philip French described the film as a "thoughtful, gripping movie" based around the themes of "trust in its various forms, betrayal, forgiveness and grief".   

===Accolades===

{| class="wikitable"
|-
! Award
! Category
! Subject
! Result
|- AACTA Awards|AACTA Award AACTA Award Best Film Jan Chapman
| 
|- AACTA Award Best Director Ray Lawrence Ray Lawrence
| 
|- AACTA Award Best Adapted Screenplay Andrew Bovell
| 
|- AACTA Award Best Actor Anthony LaPaglia
| 
|- AACTA Award Best Actress Kerry Armstrong
| 
|- AACTA Award Best Supporting Actor Vince Colosimo
| 
|- AACTA Award Best Supporting Actress Rachael Blake Rachael Blake
| 
|- AACTA Award Best Editing Karl Sodersten
| 
|- AACTA Award Best Original Music Score Paul Kelly Paul Kelly
| 
|- AACTA Award Best Sound Syd Butterworth
| 
|- Andrew Plain
| 
|- Robert Sullivan
| 
|- AACTA Award Best Production Design Kim Buddee
| 
|- AACTA Award Best Costume Design Margot Wilson
| 
|-
|}

==See also==
* Cinema of Australia

==References==
 

==External links==
*  
* 

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 