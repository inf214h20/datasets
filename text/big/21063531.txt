Kōchiyama Sōshun (1936 film)
{{Infobox film
| name           = Kōchiyama Sōshun
| image          =
| caption        = Japanese movie poster
| director       = Sadao Yamanaka
| producer       = Nikkatsu
| writer         = Shintarō Mimura Sadao Yamanaka
| starring       =
| music          = Gorō Nishi
| cinematography = Harumi Machii
| editing        =
| distributor    =
| released       =  
| runtime        = 82 minutes
| country        = Japan
| awards         =
| language       = Japanese
| budget         =
}} Japanese film directed by Sadao Yamanaka. It is an entry in the jidaigeki film genre.     It is one of three surviving films by Sadao Yamanaka.

== Cast ==
* Chojuro Kawarasaki as Kōchiyama Sōshun
* Kanemon Nakamura as Kaneko
* Shizue Yamagishi as Oshizu
* Setsuko Hara as Onami
* Daisuke Kato as Kenta (billed as Enji Ichikawa)

==Production==
The original idea for Kōchiyama Sōshun came from a Kabuki play by Kawatake Mokuami, known as Kochiyama to naozamurai. In the play, the two title characters are petty criminals from the Ueno district of Edo.  Yamanaka changed some of the characters from the play to be more good-natured, in keeping with his film aesthetic.    He also modernized the Kabuki play by casting actors from the Zenshin-za Group, which aimed to bring modern acting techniques to traditional Kabuki plays. 

==See also==
* Kōchiyama Sōshun

== References ==
 

== External links ==
*  
*  

 
 
 
 
 
 


 