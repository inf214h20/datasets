Joe Strummer: The Future Is Unwritten
 
 
{{Infobox film
| name           = Joe Strummer: The Future Is Unwritten
| image          = Joe Strummer - The Future Is Unwritten.jpg
| image_size     =
| caption        =
| director       = Julien Temple
| producer       = Anna Campeau Alan Moloney Amanda Temple Orlagh Collins Stephan Mallmann Susan Mullen
| writer         =
| narrator       =
| starring       =
| music          = Ian Neil Steve Isles Amanda Street
| cinematography = Ben Cole Mark Reynolds Tobias Zaldua
| distributor    = Vertigo Films IFC Films {{cite web url        = http://www.imdb.com/title/tt0800099/companycredits  title      = Joe Strummer: The Future Is Unwritten (2007) – Company credits  accessdate = 3 November 2007 publisher  = Internet Movie Database
}} 
| released       = 20 January 2007 (Sundance Film Festival)
| runtime        = 123 min. (Sundance Film Festival)
| country        = Ireland, United Kingdom
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
}}
Joe Strummer: The Future Is Unwritten is a 2007 documentary film directed by Julien Temple about Joe Strummer, the lead singer of the British punk rock band The Clash, that went on to win the British Independent Film Awards as Best British Documentary 2007. {{cite video people     = Temple, J., Amanda Temple, Anna Campeau, and Alan Moloney
 | date      = 2008 title      = Joe Strummer The future is unwritten medium     = Documentary, Rockumentary publisher  = Sony BMG Music Entertainment location   = New York, NY, United States oclc       = 233652709
}}  {{cite web url        = http://www.imdb.com/title/tt0800099/awards  title      = Joe Strummer: The Future Is Unwritten (2007) – Awards  accessdate = 18 December 2008 publisher  = Internet Movie Database
}}  The film premiered 20 January 2007 at the 2007 Sundance Film Festival. {{cite web url        = http://www.imdb.com/title/tt0800099/releaseinfo  title      = Joe Strummer: The Future Is Unwritten (2007) – Release dates  accessdate = 3 November 2007 publisher  = Internet Movie Database
}}  It was also shown at the Dublin Film Festival on 24 February 2007. 

It was released in the United Kingdom on 18 May 2007 and in Australia on 31 August 2007.  The film opened in limited release in the United States on 2 November 2007. 

==Cast==
*Brigitte Bardot – Herself (archive footage) Michael Balzary (Flea) – Himself
*Bono – Himself
*Steve Buscemi – Himself
*Terry Chimes – Himself
*John Cooper Clarke – Himself
*John Cusack – Himself
*Peter Cushing – Winston Smith (archive footage)
*Johnny Depp – Himself
*Matt Dillon – Himself
*Tymon Dogg – Himself (archive footage)
*Bobby Gillespie – Himself
*Alasdair Gillies – Himself
*Iain Gillies – Himself
*Topper Headon – Himself
*Damien Hirst – Himself
*Mick Jagger – Himself (archive footage)
*Jim Jarmusch – Himself Mick Jones – Himself Steve Jones – Himself
*Anthony Kiedis – Himself
*Don Letts – Himself
*Keith Levene – Himself Bernie Rhodes – Himself
*Martin Scorsese – Himself
*Joe Strummer – Himself

Special Thanks: Terence Dackombe

==Critical reception==
The film was well received by critics.  As of 18 October 2009 on the review aggregator Rotten Tomatoes, 89% of critics gave the film positive reviews, based on 61 reviews. {{cite web url        = http://www.rottentomatoes.com/m/joe_strummer_the_future_is_unwritten/  title      = Joe Strummer: The Future is Unwritten – Rotten Tomatoes  accessdate = 3 November 2007 publisher  = Rotten Tomatoes
}}  On Metacritic, the film had an average score of 79 out of 100, based on 19 reviews. {{cite web url        = http://www.metacritic.com/film/titles/joestrummer  title      = Joe Strummer: The Future Is Unwritten (2007): Reviews  accessdate = 3 November 2007 publisher  = Metacritic
}} 

Marc Savlov of The Austin Chronicle named it the 8th best film of 2007. {{cite web url        = http://www.metacritic.com/film/awards/2007/toptens.shtml  title      = Metacritic: 2007 Film Critic Top Ten Lists  accessdate = 5 January 2008 publisher  = Metacritic archiveurl = archivedate = 2 January 2008}}  Stephanie Zacharek of Salon.com|Salon named it the 9th best film of 2007. 

==Box office performance==
As of 31 January 2008 box office takings totalled $US 1,108,740. {{cite web url        = http://www.boxofficemojo.com/movies/?page=main&id=joestrummer.htm  title      = Joe Strummer: The Future Is Unwritten (2007)  accessdate = 31 May 2011 publisher  = Box Office Mojo
}} 

==Awards==
* Nominated Grand Jury Prize in the World Cinema – Documentary category at the Sundance Film Festival 2007 
* Winner of Best British Documentary at the British Independent Film Awards 2007 
* Nominated Best Single Documentary at the Irish Film and Television Awards (IFTA) 2008 

==References==
 

==External links==

* 
* 
* 
* 
* 
*  at sundance.org

 
 

 
 
 
 
 
 
 