The Casebook of Eddie Brewer
 
 }} Ian Brooker Peter Wight, Erin Connolly, Louise Paris, Glen Hill, Natalie Wilson, Bella Hamblin, Rob Stanley, Alison Belbin, and Sean Connolly. The film was shot in and around Birmingham in 2010-11. The music score was composed by Jamie Robertson.

Over the course of a few weeks, a TV documentary film crew follows old-school paranormal investigator, Eddie Brewer (Ian Brooker), as he investigates a couple of disturbing and baffling local cases. He visits a suburban house where a neurotic mother is convinced that something malevolent has entered her home and attached itself to her ten-year-old daughter, Lucy; and a dilapidated Eighteenth Century building, Rookery House, where weird and disquieting noises have been heard in the cellar. A lonely figure, Brewer must not only contend with sceptics and rivals in his own field who denounce his methods and try to undermine his investigations, but he is dogged by personal guilt over the death of his wife, Sarah. Eddie Brewer faces the greatest challenge of his life when he at last confronts the source of these paranormal manifestations during an all night vigil at the old house. For Brewer it is not just a matter of belief – it becomes a matter of survival.

“Something is calling. One man is listening.”

The film was premiered at the Flatpack Festival, Birmingham in March 2012. The film has gone on to be an Official Selection at fifteen other film festivals including Fright Night Film Festival, Louisville, Kentucky, the Unreal Film Festival, Memphis, the LA Indie Festival, Mayhem Horror Festival, Nottingham, Freakshow Horror Film Festival, Orlando, Florida, Dark Carnival Horror Film Festival, Columbus, Indiana, and the Nevermore Film Festival, Durham, North Carolina. It has won six awards including Best New Feature Film at the 2012 Shock and Gore Film Festival at the Electric Cinema, Birmingham, Best Sci-fi or Horror Award at the 2012 Film Festival of Colorado, Best Independent Movie at the Festival of Fantastic Films, Manchester, Best in Fest at the Unreal Film Festival, Memphis, and Best Supernatural Movie at Midnight Black Festival of Darkness, LA. Lead actor, Ian Brooker, won Best Actor in a Feature at Buffalo Screams Horror Film Festival at Buffalo, New York where the film was also nominated for Best International Film.

==External links==
*  
*  

 

 
 
 
 
 