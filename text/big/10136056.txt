Signs of Life (1989 film)
{{Infobox Film
| name           = Signs of Life
| image          = Signs-of-life-movie-poster-1989-1010252003.jpg
| image_size     = 
| caption        = 
| director       = John David Coles
| producer       = Cary Brokaw  Lindsay Law  Andrew Reichsman  Marcus Viscidi
| writer         = Mark Malone Arthur Kennedy
| music          = Howard Shore Elliot Davis
| editing        = William A. Anderson Angelo Corrao
| distributor    = Avenue Pictures
| released       = May 5, 1989
| runtime        = 95 minutes
| country        = United States English
| budget         = 
| gross          = $96,000 (domestic)
| preceded_by    = 
| followed_by    = 
}} American director director John Arthur Kennedy (in his last  film appearance).

Kathy Bates, Mary-Louise Parker, Will Patton, and Kate Reid also are featured.
 Mystic Marine Aquarium in Mystic, Connecticut. It is the story of a Maine man who is losing his boat and his business.

The film earned director Coles a Deauville Film Festival award, but was a financial loss.

==Cast==
* Beau Bridges as John Alder
* Vincent DOnofrio as Daryl Monahan Arthur Kennedy as Coughlin Kevin J. OConnor as Eddie
* Will Patton as Owens dad
* Kate Reid as Mrs. Wrangway
* Georgia Engel as Betty
* Kathy Bates as Mary Beth
* Mary-Louise Parker as Charlotte

==External links==
* 
*  

 
 
 


 