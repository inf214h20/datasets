Wassup Rockers
{{Infobox film
|name=Wassup Rockers
|image=Poster of the movie Wassup rockers.jpg
|director=Larry Clark
|producer=Larry Clark Kevin Turen Henry Winterstern
|writer=Larry Clark Matthew Frost
|starring=Jonathan Velasquez Francisco Pedrasa Milton Velasquez Yunior Usualdo Panameno Eddie Velasquez Luis Rojas-Salgado Carlos Velasco
|cinematography=Steve Gainer
|editing=Alex Blatt
|released= 
|runtime=111 minutes
|country=United States Spanish
|gross=$634,074 
}}
Wassup Rockers is a 2005 film directed by Larry Clark.

==Plot==
Wassup Rockers is about a group of Guatemalan American and Salvadoran American teenagers in South Los Angeles who, instead of conforming to the hip hop culture of their gang-infested neighborhood, wear tight pants, listen to punk rock, and ride skateboards. Avoiding the violence of their dangerous home turf is an everyday challenge. Janice Dickinson makes an appearance in the film as a rich alcoholic divorcee whose Spanish-speaking maid help Los Rockers & fashion designer Jeremy Scott appears as a photographer.

==Critical response== thumbs up" Ebert & Roeper. But his co-host, Richard Roeper, gave the movie a "thumbs (way) down", emphasizing Larry Clarks apparent fascination with shirtless, adolescent males. Roeper argued, "When a colleague told me I was about to see a new film from Larry Clark, the director of Bully and Kids, I said, I wonder how many scenes will pass before we get shirtless teenage boys? Thats one of Clarks rather disturbing obsessions."

==See also==
*Kids (film)|Kids (1995, Larry Clark)
*Bully (2001 film)|Bully (2001, Larry Clark)
*Ken Park (2002, Larry Clark)

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 


 