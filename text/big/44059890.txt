Vilakkum Velichavum
{{Infobox film
| name           = Vilakkum Velichavum
| image          =
| caption        =
| director       = P. Bhaskaran
| producer       = Pavamani
| writer         = Thoppil Bhasi (dialogues)
| screenplay     = Thoppil Bhasi
| starring       = P. Bhaskaran Prem Nazir Sheela Adoor Bhasi
| music          = G. Devarajan
| cinematography = Vipin Das
| editing        = G Venkittaraman
| studio         = Prathap Chithra
| distributor    = Prathap Chithra
| released       =  
| country        = India Malayalam
}}
 1978 Cinema Indian Malayalam Malayalam film,  directed by P. Bhaskaran and produced by Pavamani. The film stars P. Bhaskaran, Prem Nazir, Sheela and Adoor Bhasi in lead roles. The film had musical score by G. Devarajan.   

==Cast==
*P. Bhaskaran
*Prem Nazir
*Sheela
*Adoor Bhasi
*Sreelatha Namboothiri
*T. R. Omana
*Bahadoor
*K. P. Ummer Seema

==Soundtrack==
The music was composed by G. Devarajan and lyrics was written by P. Bhaskaran.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Dukhamaanu Saaswatha || K. J. Yesudas || P. Bhaskaran ||
|-
| 2 || Pandu Pandoru || P. Madhuri || P. Bhaskaran ||
|-
| 3 || Vaadiya Maruvil || K. J. Yesudas || P. Bhaskaran ||
|-
| 4 || Velicham Vilakkine || K. J. Yesudas || P. Bhaskaran ||
|}

==References==
 

==External links==
*  

 
 
 


 