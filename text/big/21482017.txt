Chichi Koishi
 
{{Infobox film
| name           = Chichi Koishi 父恋し
| image          =
| caption        = Japanese movie poster
| director       = Shunkai Mizuho
| producer       = Takeshi Ogura
| writer         = Kihan Nagase
| starring       =
| music          =
| cinematography =
| editing        =
| distributor    = Shochiku 1951
| runtime        = 67 min.
| country        = Japan
| awards         =
| language       = Japanese
| budget         =
| preceded_by    =
| followed_by    =
| imdb_id        =  
|
}} Japanese film released in 1951. The film was directed by Shunkai Mizuho and stars Hibari Misora.    

==Cast==
* Masao Wakahara - Yūji Makita
* Kuniko Miyake - Sanae Shimura
* Hibari Misora - Sanaes daughter Emiko
* Ryūji Kita - Sanaes father Taishaku
* Chiyoko Fumiya
* Shinyō Nara - doctor
* Mutsuko Sakura
* Ichirō Shimizu - Murai
* Akira Takaya - travelling druggist
* Kentarō Taki

==Plot==
Yūji Makita, a guitar player and singer played by Masao Wakahara, comes to a portside onsen (spa resort) town. Thirteen years prior, he had had a love affair there with Sanae Shimura (played by Kuniko Miyake) which ended poorly; Sanae has since moved to Tokyo, leaving behind her daughter Emiko (played by Hibari Misora) and father Taishaku (Ryūji Kita).

Waiting for her mother to return, Emiko often sits by the water singing a song her mother taught her. As Yūji composed this song himself, and never shared it with anyone but Sanae, he realizes that Emiko must be his daughter. He sends a false telegram to Sanae, saying that Emiko is sick, and that she should come home. Sanae returns, escaping from a man named Murai, from whom she had borrowed money when her father was ill.

Sanae spots Yūji upon her return to the port town, but he disappears again, leaving a note saying that he will visit again, for his daughters sake. Sanae leaves for Tokyo the very next day, with Emiko, but fails to find Yūji and returns home.

Some time later, they discover Yūjis name in the newspaper, which says that he has been selected to compete in a competition of music composers. Yūji is in the hospital, however, bedridden, and so, in the end, Emiko takes his place in the contest, her parents listening on a radio in the hospital. 

==References==
 

 
 
 
 


 