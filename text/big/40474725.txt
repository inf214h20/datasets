A Corner in Cotton
{{Infobox film
| name           = A Corner in Cotton
| image          = File:A Corner in Cotton -1.JPG
| alt            = 
| caption        = Scene from film
| film name      = 
| director       = Fred J. Balshofer
| producer       = 
| writer         = 
| screenplay     = Charles A. Taylor 
| story          = Anita Loos
| based on       =  William Cliford Frank Bacon 
| narrator       = 
| music          = 
| cinematography = 
| editing        = 
| studio         = Quality Pictures 
| distributor    = Metro Pictures
| released       = February 1916 
| runtime        = Five reels
| country        = United States Silent
| budget         = 
| gross          = 
}} silent film melodrama produced in 1916 by Quality Pictures and distributed by Metro Pictures. The movie was filmed at studios in New York and California and on locations near Savannah, Georgia. A Corner in Cotton was directed by Fred J. Balshofer, with the assistants of Howard Truesdell and adapted for film by Charles A. Taylor from a story by Anita Loos.
   Retrieved September 6, 2013  The film was released on February 21, 1916 with Marguerite Snow in the starring role. 

==Cast==
Marguerite Snow ...  Peggy Ainslee  William Cliford ... Richard Ainslee  Frank Bacon ... Col. Robert Carter 
Helen Dunbar ... Mrs. Carter 
Wilfred Rogers ... John Carter 
Zella Caull (as Zella Call) ... Isabel Rawlston  
Howard Truesdale (as Howard Truesdell) ... Charles Hathaway  
Lester Cuneo ... Willis Jackson 
John Goldsworthy (as J.H. Goldsworthy) ... Algie Sherwood	

Source IMDB   Retrieved September 6, 2013 
	
==Plot== cotton broker, and John Carter, the son of a Southern cotton mill owner. Peggy grows weary of society life and decides to help improve the lot of the poor by becoming involved with the Settlement movement. She later travels south to investigate working conditions at Carter’s cotton mill. Peggy manages to gain employment there, but soon attracts the unwanted advances of the mill foreman. John saves her from the would-be masher and the couple eventually fall in love. This becomes a problem with their fathers who had become business antagonist. In the end Peggy and John married after she foils her father’s attempt to ruin Carter by cornering the market in cotton and then persuades the two men to settle their differences. 

==Resources==
 

 
 
 
 
 
 
 
 

 