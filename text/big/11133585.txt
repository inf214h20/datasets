Man to Man (2005 film)
{{Infobox film
| name           = Man to Man
| image          = 
| caption        = 
| director       = Régis Wargnier
| producer       = Aïssa Djabri Farid Lahouassa William Boyd Régis Wargnier
| starring       = Joseph Fiennes Kristin Scott Thomas Lomama Boseki Cécile Bayiha Iain Glen Hugh Bonneville
| music          = Patrick Doyle
| cinematography = Laurent Dailland
| editing        = Yann Malcor
| distributor    = 
| released       =  
| runtime        = 122 minutes
| country        = France South Africa United Kingdom
| language       = English
| budget         = 
| gross          = 
}} historical drama William Boyd.

==Plot==
In 1860, Victorian scientists capture a pygmy couple during an expedition in Central Africa. They are transported back to the United Kingdom for further study as part of research involving the theory of the evolution of man. However, the primitive outlook of the pygmies and the sophisticated methods used by the scientists, as well as the complications of an adapting to a foreign environment, make their anthropological study all the more difficult. Ultimately, as the pygmies become more absorbed to the public, major disagreements erupt culminating in a bloody and tragic confrontation.

== Cast ==
* Joseph Fiennes as Jamie Dodd
* Kristin Scott Thomas as Elena Van Den Ende
* Iain Glen as Alexander Auchinleck
* Hugh Bonneville as Fraser McBride
* Lomama Boseki as Toko
* Cécile Bayiha as Likola
* Flora Montgomery as Abigail McBride

== Reception ==

 

== Awards ==

 

== References ==
 

==External links==
*  
*  
 

 
 
 
 
 
 
 
 
 
 


 
 