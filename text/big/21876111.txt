Doctor Blood's Coffin
{{Infobox film
| name           = Doctor Bloods Coffin
| image          = Doctor-bloods-coffin-poster.jpg
| director       = Sidney J. Furie
| writer         = {{plainlist|
* James Kelly
* Peter Miller
}}
| based on       = original story and screenplay by Nathan Juran (as "Jerry Juran")
| starring       = {{plainlist|
* Kieron Moore
* Hazel Court Ian Hunter
}}
| producer       = George Fowler
| music          = Buxton Orr
| cinematography = Stephen Dade
| editing        = Antony Gibbs
| studio         = Caralan Productions Ltd.
| distributor    = United Artists
| released       =  
| runtime        = 92 minutes
| country        = United Kingdom
| language       = English
}}
Doctor Bloods Coffin is a 1961 British horror film directed by Sidney J. Furie.   Kieron Moore stars as a mad scientist who reanimates the dead.

==Plot==
Peter Blood (Kieron Moore) is a young doctor who experiments with bringing the dead back to life. His early subject is the deceased husband of Linda Parker (Hazel Court), a woman he is attracted to. Hidden in a Cornish tin mine, he conducts his ghastly experiments using curare to remove living, beating hearts from undeserving people in order to bring the deserving dead back to life. 

==Cast==
* Kieron Moore as Dr. Peter Blood 
* Hazel Court as Nurse Linda Parker  Ian Hunter as Dr. Robert Blood, Peters Father 
* Kenneth J. Warren as Sergeant Cook 
* Gerald Lawson as Mr. G. F. Morton 
* Fred Johnson as Tregaye 
* Paul Hardtmuth as Professor Luckman 
* Paul Stockman as Steve Parker, Lindas Husband 
* Andy Alston as George Beale, Tunnel Expert

==Production==
Nathan Juran wrote the script and sold it to Sidney J Furie, who had it rewritten. Furie set up the film at Caralan Productions and signed a distribution deal with United Artists. It was one of the last movies to be shot at Nettleford Film Studios. John Hamilton, The British Independent Horror Film 1951-70 Hemlock Books 2013 p 111-115 
 Cornish locations St Just.

==Reception==
Writing in The Zombie Movie Encyclopedia, academic Peter Dendle identified the film as having one of the first representations of a modern zombie: undead, decaying, and violent.   Glenn Kay, who wrote Zombie Movies: The Ultimate Guide, criticized the slow pacing and lack of zombie action.  He concluded, "Its all pretty lame stuff." 

==DVD==
Doctor Bloods Coffin was released to DVD by MGM Home Video on October 18th, 2011 via MGMs Limited Edition Collection DVD-on-demand service as a Region 1 widescreen DVD.


==References==
 

==External links==
*  
*  

 
 

 
 
 
 
 
 