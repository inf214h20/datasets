Hitler Brothers
{{Infobox film
| name           = Hitler Brothers
| image          =
| caption        =
| director       = Sandhya Mohan
| producer       = E Unni Krishnan Udaykrishna Udayakrishna-Siby Sibi K Thomas
| screenplay     = Udaykrishna Sibi K Thomas
| starring       = Jagathy Sreekumar Kalabhavan Navas Prem Kumar A. C. Zainuddin
| music          = S. P. Venkatesh
| cinematography = Madhu Adoor
| editing        = K Rajagopal
| studio         = Swetha Films
| distributor    = Swetha Films
| released       =  
| country        = India Malayalam
}}
 1997 Cinema Indian Malayalam Malayalam film, directed by Sandhya Mohan and produced by E Unni Krishnan. The film stars Jagathy Sreekumar, Kalabhavan Navas, Prem Kumar and A. C. Zainuddin in lead roles. The film had musical score by S. P. Venkatesh.   

==Cast==
 
*Jagathy Sreekumar as Adv 
*Kalabhavan Navas as Bus Conductor 
*Prem Kumar as Sundaran
*A. C. Zainuddin as Keshavan Kutty
*RajKumar
*Vijayakumar as Dr Gopan
*Augustine as Govindan
*Bindu Varappuzha
*Harishree Ashokan as Bhaktha Valsalan
*Indrans as Balaraman
*Janardanan as Thrivikraman
*Jose Pellissery as Shankaran Kutty
*Kuthiravattam Pappu as Nanappan
*Mala Aravindan as Ramankutty
*Paravoor Bharathan as Achuthan Kutty
*Salu Koottanad
*Shaju
*Sharmily as Sundari
*Vanitha Vijaya Kumar
*Babu Antony as Narendran
 

==Soundtrack==
The music was composed by S. P. Venkatesh and lyrics was written by Kaithapram. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Kannaadi Maalika || KS Chithra, MG Sreekumar || Kaithapram || 
|-
| 2 || Panimathi || KS Chithra, Biju Narayanan || Kaithapram || 
|}

==References==
 

==External links==
*  

 
 
 
 
 