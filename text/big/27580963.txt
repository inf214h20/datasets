Eternal Flame (music video)
{{Infobox film
| name           = Eternal Flame
| image          = Eternal_Flame_Video.jpg
| caption        = Eternal Flame Production Still
| director       = Leah Meyerhoff
| producer       = Ari Krepostman
| writer         = Leah Meyerhoff
| starring       = Joan Wasser Rainy Orteca Ben Perowsky
| music          = Joan As Police Woman
| cinematography = Lyle Vincent
| editing        = Alina Pineda
| distributor    = MTV Europe
| released       =  
| runtime        = 4 minutes
| country        = United States
| language       = English
| budget         = 
}}
Eternal Flame is a music video for Joan As Police Woman directed by Leah Meyerhoff and currently airing on MTV Europe.

==Synopsis==
 
Welcome to a world where mermaids dance with octopi, fire breathers rule the night, and the Statue of Liberty knows how to sing. Girls are buried up to their necks in sand while boys do back flips and dazzle dancers perform in a puppet show without strings. A circus-themed burlesque music video for Joan As Police Woman, Eternal Flame is the ultimate fire hazard. 

==Cast==
Joan Wasser &mdash; Singer / Statue of Liberty 
Rainy Orteca &mdash; Bass Player 
Ben Perowsky &mdash; Drummer 
Genaro Martinez &mdash; Dazzle Dancer 
Heidi Turzyn &mdash; Dazzle Dancer 
Jennifer Capriccio &mdash; Dazzle Dancer 
Joe Komara &mdash; Dazzle Dancer

==Festivals==
*Backseat Film Festival
*Boston Underground Film Festival
*Coney Island Film Festival
*Ladyfest London
*Ladyfest Munich
*Ladyfest Ruhr
*Les Gai Cine Mad
*NewFest
*Milan LGBT Film Festival
*Q Film Festival
*Rome International Film Festival
*San Antonio Underground Film Festival

==Reviews==
NPR says "The breezy gospel groove of "Eternal Flame" features tasty ebowed guitar, as well as a variety of vocal flourishes courtesy of Wasser and one ground-rumbling male bass singer"   and Stephen Trouss of Pitchfork Media says "Wasser was also Jeff Buckleys partner from 1994 up to his death, and though its undoubtedly creepy to speculate about someones private grief, this loss feels key to her subsequent development. The very 6/8 and turbid harmonies of "Eternal Flame" conjure up ghosts of Buckleys own galloping "Grace". And when the lyric reaffirms "I cant be the lighter of your eternal flame," you cant help but hear a response to one song we know Buckley wrote about her, "Everybody Here Wants You", and its line "Youre just the torch/ To put the flame to all our guilt and shame." 

==References==
 

==External links==
* 
* 
* 
* 
* 

 
 
 