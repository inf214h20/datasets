Putravati
 

{{Infobox film
| name           = Putravati - Marathi Movie
| image          = Putravati Marathi Movie.jpg
| alt            =  
| caption        = Theatrical release poster
| director       = Bhaskar Jadhav
| producer       = M.B.Joshi
| screenplay     = 
| story          = 
| starring       = Nilu Phule Asha Kale Sukanya Kulkarni Avinash Narkar
| music          = Sridhar Phadke
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Marathi
| budget         = 
| gross          = 
}}
Putravati is a Marathi movie released on 19 April 1996.  The movie has been produced by M.B.Joshi and directed by Bhaskar Jadhav. The plot of the movie is a glowing tribute to womanhood and a soul stirring saga about family values, hope and faith of a woman for whom any sacrifice is supreme.

== Synopsis ==
Police Commissioner Krishnakant Deshmukh and his family are very religious and god fearing. His son Kapil is in love with Swati and despite their horoscopes not matching, he marries her, much to the annoyance of his parents. Days pass by but Swati can’t bear a child. Many medical tests are performed on her but of no use.

One day Swati meets Dr.Ashwani, a woman known to perform miracles, and under her treatment the much needed miracle happens. Soon the child Mohan is born and becomes the apple of everyones eye. But there is a crisis in the family and the same Mohan is now detested by everyone except Swati.

== Cast ==

The cast includes Nilu Phule, Asha Kale,  Sukanya Kulkarni, Avinash Narkar & Others.

==Soundtrack==
The music is provided by Sridhar Phadke.

== References ==
 
 

== External links ==
*  
*  

 
 
 


 