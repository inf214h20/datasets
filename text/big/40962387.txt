The Slim Princess (1915 film)
 

{{Infobox film
| name           = The Slim Princess
| image          =
| caption        =
| director       = E. H. Calvert
| producer       =
| writer         =
| narrator       =
| starring       = Francis X. Bushman Ruth Stonehouse Wallace Beery
| cinematography = Jackson Rose
| editing        =
| distributor    = Essanay Film Manufacturing Company
| released       =  
| runtime        =
| country        = United States
| language       = English
| budget         =
}}
 remade into a 1920 film starring Mabel Normand. The farcical plot involves a princess of a fictional country, loosely based upon Turkey, in which obese women are prized and the normal-sized protagonist is widely regarded as being too slender. 

==Cast==
Francis X. Bushman	 ...	
Alexander H. Pike 
Ruth Stonehouse	 ...	
Princess Kalora 
Wallace Beery	 ...	
Popova 
Harry Dunkinson	 ...	
Count Selim Malagaski 
Terza Bey	 ...	
Princess Jeneka 
Bryant Washburn	 ...	
Rawley Plumston 
Lester Cuneo	 ...	
The Only Koldo

==References==
 

==External links==
*   in the New York Times
*   at Turner Classic Movies
*   in the Internet Movie Database
*   at Fandango.com
*   at Looking for Mabel Normand
*   at Allmovie.com
*   at Hollywood.com

 
 
 
 
 

 