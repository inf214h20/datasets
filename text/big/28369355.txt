Meetings with Remarkable Men (film)
 
{{Infobox film
| name           = Meetings with Remarkable Men
| image          = MeetingsWithRemarkableMen.jpg
| caption        = VHS cover
| director       = Peter Brook
| producer       = Stuart Lyons
| writer         =  , Jeanne de Salzmann
| starring       = Dragan Maksimović Terence Stamp
| music          = Laurence Rosenthal
| cinematography = Gilbert Taylor
| editing        = John Jympson
| distributor    = Enterprise Pictures Ltd
| released       =  
| runtime        = 108 minutes
| country        = United Kingdom
| language       = English
| budget         = 
}}
Meetings with Remarkable Men is a 1979 British film directed by Peter Brook {{cite book
|last=Brook
|first=Peter
|authorlink=Peter Brook
|title=The shifting point, 1946-1987
|url=http://books.google.com/books?id=1PtTAAAAMAAJ
|accessdate=14 April 2011
|date=September 1987
|publisher=Harper & Row same name by Greek-Armenian mystic, George Gurdjieff|G. I. Gurdjieff, first published in English in 1963. Shot on location in Afghanistan (except for dance sequences, which were filmed in England), it starred Terence Stamp, and Dragan Maksimović as the adult Gurdjieff. The film was entered into the 29th Berlin International Film Festival and nominated for the Golden Bear.   

The plot involves Gurdjieff and his companions search for truth in a series of dialogues and vignettes, much as in the book. Unlike the book, these result in a definite climax—Gurdjieffs initiation into the mysterious Sarmoung Brotherhood. The film is noteworthy for making public some glimpses of the Gurdjieff movements. {{cite book
|last1=Panafieu
|first1=Bruno De
|last2=Needleman
|first2=Jacob
|last3=Baker
|first3=George
|title=Gurdjieff
|url=http://books.google.com/books?id=GV0dhZxB91EC&pg=PA28
|accessdate=14 April 2011
|date=September 1997
|publisher=Continuum International Publishing Group
|isbn=978-0-8264-1049-8
|pages=28–
|quote=A brief glimpse of the dances appears at the very end of the motion picture about Gurdjieff, Meetings with Remarkable Men, produced and directed in 1978 by Peter Brook, with a screenplay by Peter Brook and Jeanne de Salzmann}} 

==Selected cast==
*Dragan Maksimović as G. I. Gurdjieff
*Terence Stamp as Prince Lubovedsky
*Mikica Dimitrijevic as Young Gurdjieff
*Warren Mitchell as Gurdjieffs father
*Athol Fugard as Professor Skridlov
*David Markham as Dean Borsh
*Natasha Parry as Vitvitskaia
*Colin Blakely as Tamil   
*Gregoire Aslan as Armenian Priest Tom Fleming as Father Giovanni
*Andrew Keir as Head of Sarmoung Monastery
*Donald Sumpter as Pogossian
*Gerry Sundquist as Karpenko Martin Benson as Dr Ivanov 
*Bruce Purchase as Father Maxim
*Roger Lloyd-Pack as Pavlov

==Further reading==
* Meetings with remarkable men: my impressions of the film, by Kathryn Hulme. Remar Productions, 1979.
* Meetings with remarkable men: One mans search becomes a film, by Pamela Lyndon Travers.
==References==

 

==External links==
* 
* 
*  

 

 
 
 
 
 
 
 
 
 
 
 
 