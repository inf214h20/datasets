Will (2012 film)
{{Infobox film
| name           = Will
| image          = 
| alt            = 
| caption        = 
| director       = Ellen Perry
| producer       = 
| writer         = Zack Anderson Ellen Perry
| starring       = Damian Lewis Jane March Bob Hoskins Kristian Kiehling
| music          = Nigel Clarke Michael Csányi-Wills
| cinematography = Oliver Stapleton
| editing        = Derek Burgess Lesley Walker
| studio         =  
| distributor    = 
| released       = 4 November 2012 
| runtime        = 98 minutes
| country        = United Kingdom
| language       = English
| budget         = £35 million
| gross          = £24 million (including DVD sales and direction)
}}
Will is a 2012 British sports drama directed by Ellen Perry and starred Damian Lewis, Perry Eggleton and Bob Hoskins. This is also Bob Hoskins last film before his retirement the same year the movie was released and death in 2014.  

==Plot== Liverpool play AC Milan 2005 Champions League Final at the Atatürk Olympic Stadium in Istanbul.

Brennan is Liverpools number one fan, able to recite facts ad infinitum about the club and at a public school in the south of England since his father Gareth (Damian Lewis) is emotionally unable to care for him following the death of Wills mother. Gareth appears one day out of the blue with tickets for Liverpools trip to the Champions League Final. Unknown to Will his father has health problems and suddenly dies and in his belief that the adults in his life are conspiring to quash his wish to get to the match to honour his father, two of his mates at school start Will on his way for reason of their own. His being missing becomes world-wide news and he encounters others that either support Liverpool or the game of football. One of these is Alek, in Paris, who stopped playing the sport following a tragic incident due to his own actions in his hometown during the Bosnian War.

Alek is initially reluctant to get involved for various reason but a friend does his best to encourage Alek to recover from his personal demons by helping Will reach his destination. The two are off on the final leg of the journey stopping along the way in Aleks hometown where they elude police. Although they reach Istanbul they do not have tickets but in the quest to get them Liverpool stars Kenny Dalglish, Steven Gerrard and Jamie Carragher, recognise him who come to the rescue and makes possible for Will to realise his dream and much more than he could have imagined if his fathers original plans had come to be.  

==Partial cast==
* Damian Lewis - Gareth 
* Perry Eggleton - Will
* Kieran Wallbanks - Simon
* Brandon Robinson - Richie
* Jane March - Sister Noell 
* Bob Hoskins - Davey 
* Kristian Kiehling - Alek
* Alice Krige - Sister Carmel 
* Rebekah Staton - Nancy 
* Mark Dymond - Detective 
* Branko Tomović - Avdo Bilic 
* Canan Erguder - Mina Bilic 
* Malcolm Storry - Finch 
* Neil Fitzmaurice - Fitzy
* John May - Barney 
* Jamie Carragher - Himself
* Steven Gerrard - Himself
* Kenny Dalglish - Himself
* Bahattin Cam - The villager looking at the stranger in the car

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 
 

 
 