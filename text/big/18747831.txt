The Millennial Bee
 
{{Infobox film
| name           = The Millennial Bee
| image          = A Thousand-year-old Bee.jpg
| caption        = Promotional poster
| director       = Juraj Jakubisko
| producer       = 
| writer         = Juraj Jakubisko Peter Jaros (novel)
| narrator       = 
| starring       = Jozef Kroner Stefan Kvietik Michal Dočolomanský Jana Janovská
| music          = Petr Hapka
| cinematography = Stanislav Dorsic
| editing        = Judita Fatulová Patrik Pass
| studio         = 
| distributor    = 
| released       =  
| runtime        = 162 minutes
| country        = Czechoslovakia West Germany Czech
| budget         = 
| gross          = 
}} Best Foreign Language Film at the 57th Academy Awards, but was not accepted as a nominee. 

==Cast==
* Jozef Kroner as Martin Pichanda
* Stefan Kvietik as Samo
* Michal Docolomanský as Valent
* Jana Janovská as Ruzena
* Eva Jakoubková as Kristína
* Ivana Valesová as Mária
* Pavol Mikulík as Julo
* Igor Cillík as Svanda
* Jirí Císler as Belányi

== Film awards ==
*   XL. Venice Film Festival 1984   • Golden Phoenix for The best art direction and cinematography   • Catholic Prize
*   FEST Belgrade 1984   • UNICEF Prize
*   IV. IFF Sevilla 1984   • Grand Prize 
*   Czechoslovak Journalists’ Prize
*   XXII. Film Festival Banská Bystrica 1984   • Grand Prize

==See also==
* List of submissions to the 57th Academy Awards for Best Foreign Language Film
* List of Czechoslovak submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*  
*   at Slovak Film Database

 
 
 
 
 
 
 


 