Marshall University: Ashes to Glory
{{Infobox Film
| name = Marshall University: Ashes to Glory
| image = 
| director = Deborah Novak
| producer = John Witek
| writer = Deborah Novak Joe Witek
| music = Jay Flippin
| distributor = West Virginia Public Broadcasting Service  
| released = November 18, 2000
| runtime = 119 minutes
| country = USA
| language = English 
|}}
 2000 Documentary documentary about plane crash football team,  most of its coaching staff, and a number of school officials and Huntingtonians), and the efforts of new head coach Jack Lengyel and the coaching staff (which included members of the previous staff), to rebuild the team and help heal the city of Huntington, West Virginia|Huntington, West Virginia.
 1971 season.  This team was dubbed the Young Thundering Herd and led by the few upperclassmen who didnt make the trip.  Several players from other Marshall sports programs rounded out the teams roster.  In the preseason "Green and White Game" versus the Marshall alumni (coached by new athletics director Joe McMullen), the Young Thundering Herd defeated the alums 26-0.
 Reggie Oliver fullback Terry Bowling Green Bowling Green Falcons football|Falcons.  These were the only two victories of the year for the Thundering Herd.
 NCAA Division 1992 and 1996 Marshall Thundering Herd football team|1996, the latter with an unbeaten season.
 Angel Award Worldfest Houston.
 Miami University.  The Herd went on to defeat the RedHawks 51-31. 

==Extras==
DVD extras include extended interviews, a "making of" documentary as well as "shorts" on the 1970 and the 1971 teams and one about Huntington in the 1970s.

==We Are Marshall lawsuit== federal court in California accusing Warner Bros. and others associated with the We Are Marshall film of fraud, copyright infringement and breach of contract.
  Novak, who directed Marshall University: Ashes to Glory, is a Huntington native and Marshall alumnus. In October 2008, a federal judge dismissed the lawsuit in a summary judgment in favor of Warner Bros. 

==See also==
*Southern Airways Flight 932
*We Are Marshall

==References==
 

==External links==
* 
* 
* 

 
 

 
 
 
 
 