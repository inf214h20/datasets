The Paper (film)
{{Infobox film
| name = The Paper
| image = The Paper movie poster.jpg
| caption = Theatrical release poster
| director = Ron Howard
| producer = Brian Grazer David Koepp
| writer = David Koepp Stephen Koepp
| starring = Michael Keaton Glenn Close Marisa Tomei Randy Quaid Robert Duvall
| music = Randy Newman
| cinematography = John Seale Mike Hill
| studio = Imagine Entertainment
| distributor = Universal Pictures
| released =  
| runtime = 112 min.
| country = United States English
| budget = $6 million
| gross = $48,424,341
}} 1994 American comedy-drama film directed by Ron Howard and starring Michael Keaton, Glenn Close, Marisa Tomei, Randy Quaid and Robert Duvall. It received an Academy Award nomination for Best Original Song for "Make Up Your Mind", which was written and performed by Randy Newman.

The film depicts a hectic 24 hours in a newspaper editors professional and personal life. The main story of the day is the murder of a couple of visiting businessmen by two boys. The reporters discover evidence suggesting a police cover-up of evidence of the suspects innocence, and rush to scoop the story in the midst of professional, private and financial chaos.

==Plot==
The film takes place during a 24-hour period. Henry Hackett (Michael Keaton) is the metro editor of the New York Sun, a fictional  New York City tabloid. He is a workaholic who loves his job, but the long hours and low pay are leading to discontent. He is at risk of experiencing the same fate as his publisher, Bernie White (Robert Duvall), who put his work first at the expense of his family.

The papers owner, Graham Keighley (Jason Robards), faces dire financial straits, so he has Alicia Clark (Glenn Close), the managing editor and Henrys nemesis, impose unpopular cutbacks. Henrys wife Martha (Marisa Tomei), a fellow Sun reporter on leave and about to give birth, is fed up because Henry seems to have less and less time for her, and she really dislikes Alicia Clark. She urges him to seriously consider an offer to leave the Sun and become an assistant managing editor at the New York Sentinel, a fictional newspaper based on The New York Times, which would mean more money, shorter hours, more respectability... but might also be a bit boring for his tastes.

Minor subplots involve Alicia, Bernie and Sun columnist Michael McDougal (Randy Quaid). McDougal is threatened by an angry city official named Sandusky (Jason Alexander) whom McDougals column had been tormenting for the past several weeks. Their drunken confrontation in a bar leads to gunfire, which gets Alicia shot in the leg through the wall. Alicia, who is having an affair with Sun reporter Carl (Bruce Altman) and has expensive tastes, schemes to get a raise in her salary. Bernie reveals to Henry that he has recently been diagnosed with prostate cancer, which causes him to spend time tracking down his estranged daughter Deanne White (Jill Hennessey), in an attempt to reconcile before his time is up.

Meanwhile, a hot story is circulating the city, involving the murder of two businessmen in Brooklyn and two African American teenagers arrested for the crime, which both Henry and McDougal believe to be false charges (due to overhearing NYPD discuss the arrest on the Sun offices police scanner). Because of this story, Henry is wrought with tough decisions, deadlines and personal crises (including his interpersonal issues with Alicia). He becomes obsessed with getting to the bottom of the case, getting others from the Sun staff to investigate along with him. He goes so far as to blow his job offer at the Sentinel after he steals information about the case from the editors desk notes and reports it during a Sun staff meeting.

Martha does some investigating for him and discovers (through her friend in the Justice Department) that the businessmen murdered were bankers who stole a large sum of money from their largest investor: a trucking company that happens to have ties to the Mafia. With this new evidence, Henry begins to believe that it was all a setup and the Brooklyn boys were likely just caught in the midst of it somehow. He is so determined to get the correct story that he leaves a dinner with Martha and his parents to go to the police station with McDougal (as they need police confirmation that the boys were not responsible for the murder before printing the story).

They corner McDougals police contact, an officer named Richie, in the station bathroom and through repeated interrogation (and the promise of his anonymity in the story) get him to admit that the kids are indeed innocent and just happened to be walking by the scene of the crime when they were caught. The reason for their arrest was largely due to city officials insistence that the media portray the NYPD as being on top of such high-profile crimes immediately in order to keep NYC tourism from dropping. Henry and McDougal race back to the Sun office, excited about their exclusive for the paper.

They discover that Alicia has okay-ed the papers original front-page headline and story stating that the teens were guilty, despite Henry and McDougal having just returned with the evidence proving otherwise. This results in a physical fight between her and Henry, after he tries to stop the presses, which are already printing the papers with the wrong information.

Martha is rushed to the hospital for an emergency C-section due to uterine hemorrhaging, Alicia, accidentally shot by Sandusky in the bar and in the same hospital, has a change of heart, calls the Sun and the original headline corrected to Henrys suggestion: "They Didnt Do It." The new editions are printed just in time for the following morning circulation. The movie ends with Martha giving birth to a healthy baby boy, and a morning news radio report states that because of the Sun s exclusive story, the Brooklyn teens were released from jail with no charges pressed, closing out a wild 24 hours.

==Cast==
*Michael Keaton as Henry Hackett
*Robert Duvall as Bernie White
*Glenn Close as Alicia Clark
*Marisa Tomei as Martha Hackett
*Randy Quaid as Michael McDougal
*Jason Robards as Graham Keightley
*Jason Alexander as Marion Sandusky
*Spalding Gray as Paul Bladden
*Catherine OHara as Susan
*Lynne Thigpen as Janet
*Jack Kehoe as Phil
*Roma Maffia as Carmen
*Amelia Campbell as Robin
*Clint Howard as Ray Blaisch
*Geoffrey Owens as Lou
*Jill Hennessey as Deanne White William Prince as Howard Hackett
*Augusta Dabney as Augusta Hackett
*Bruce Altman as Carl

==Production==
Screenwriter Stephen Koepp, a senior editor at Time (magazine)|Time magazine, collaborated on the screenplay with his brother David and together they initially came up with "A Day in the Life of a Paper" as their premise. David said, "We wanted a regular day, though this is far from regular." {{cite news
 | last = Schaefer
 | first = Stephen
 | coauthors =
 | title = New edition competes with small screen, too
 | work = Boston Herald
 | pages =
 | language =
 | publisher =
 | date = March 27, 1994
 | url =
 | accessdate =  }}  They also wanted to “look at the financial pressures of a paper to get on the street and still tell the truth.”  After writing the character of a pregnant reporter married to the metro editor (that Marisa Tomei ended up playing in the film), both of the Koepps wives became pregnant. Around this time, Universal Pictures greenlighted the project.

For his next project, Ron Howard was looking to do something on the newspaper industry. Steven Spielberg recommended that he get in touch with David Koepp. Howard intended to pitch an idea to the writer, who instead wanted to talk about how much he loved the script for Parenthood (1989 film)|Parenthood. The filmmaker remembers, “I found that pretty flattering, of course, so I asked about the subject of his work-in-progress. The answer was music to my ears: 24 hours at a tabloid newspaper." {{cite news
 | last = Arnold
 | first = Gary
 | coauthors =
 | title = Tabloid press gets the Ron Howard touch in The Paper
 | work = Washington Times
 | pages =
 | language =
 | publisher =
 | date = March 27, 1994
 | url =
 | accessdate =  }}  Howard read their script and remembers, “I liked the fact that it dealt with the behind-the-scenes of headlines. But I also connected with the characters trying to cope during this 24-hour period, desperately trying to find this balance in their personal lives, past and present.” {{cite news
 | last = Uricchio
 | first = Marylynn
 | coauthors =
 | title = Opie’s Byline: Paper Director Ron Howard was drawn to Keaton’s Style, Newsroom’s Buzz
 | work = Pittsburgh Post-Gazette
 | pages =
 | language =
 | publisher =
 | date = March 25, 1994
 | url =
 | accessdate =  }} 
 Daily News (which would provide the inspiration for the fictional newspaper in the film). He remembers, “Youd hear stuff from columnists and reporters about some jerk theyd worked with ... I heard about the scorned female reporter who wound up throwing hot coffee in some guys crotch when she found out he was fooling around with someone else." {{cite news
 | last = Kurtz
 | first = Howard
 | coauthors =
 | title = Hollywoods Read on Newspapers; For Decades, a Romance With the Newsroom
 | work = Washington Post
 | pages =
 | language =
 | publisher =
 | date = March 27, 1994
 | url =
 | accessdate =  }}  It was these kinds of stories that inspired Howard to change the gender of the managing editor that Glenn Close would later play. Howard felt the Koepps script featured a newsroom that was too male-dominated. {{cite news
 | last = Schwager
 | first = Jeff
 | coauthors =
 | title = Out of the Shadows
 | work = Moviemaker
 | pages =
 | language =
 | publisher =
 | date = August 13, 1994
 | url = http://www.moviemaker.com/magazine/editorial.php?id=213
 | accessdate =2007-04-16 }}  The writers agreed and changed the characters name from Alan to Alicia but kept the dialogue the same. According to David Koepp, "Anything else would be trying to figure out, How would a woman in power behave? And it shouldnt be about that. It should be about how a person in power behaves, and since that behavior is judged one way when its a man, why should it be judged differently if its a woman?" 

Howard met with some of the top newspapermen in New York, including former Post editor Pete Hamill and columnists Jimmy Breslin and Mike McAlary (who inspired Randy Quaid’s character in the movie). They told the filmmaker how some reporters bypass traffic jams by putting emergency police lights on their cars (a trick used in the movie). Hamill and McAlary also can be seen in cameos. 

Howard wanted to examine the nature of tabloid journalism. "I kept asking, Are you embarrassed to be working at the New York Post? Would you rather be working at The Washington Post or The New York Times? They kept saying they loved the environment, the style of journalism.”  The model for Keaton’s character was the Daily News  metro editor Richie Esposito. Howard said, “He was well-dressed but rumpled, mid-to-late 30s, overworked, very articulate and fast-talking. And very, very smart. When I saw him, I thought, thats Henry Hackett. As written." 

The director also was intrigued by the unsavory aspect of these papers. "They were interested in celebrities who were under investigation or had humiliated themselves in some way. I could see they would gleefully glom onto a story that would be very humiliating for someone. They didnt care about that. If they believed their source, they would go with it happily.” 

In addition to being influenced by Ben Hecht and Charles MacArthur’s famous stage play The Front Page, Howard studied old newspaper movies from the 1930s and 1940s. Howard said, “Every studio made them, and then they kind of vanished. One of the reasons I thought it would make a good movie today is that it feels fresh and different.” {{cite news
 | last = Dowd
 | first = Maureen
 | coauthors =
 | title = The Paper Replates The Front Page for the 90’s
 | work = The New York Times
 | pages =
 | language =
 | publisher =
 | date = March 13, 1994
 | url = http://www.nytimes.com/1994/03/13/movies/film-the-paper-replates-the-front-page-for-the-90-s.html?scp=2&sq=%22Ron+Howard%22&st=nyt
 | accessdate = 2010-03-05 }} 

One of Howard’s goals was to cram in as much information about a 24-hour day in the newspaper business as humanly possible. He said, “Im gonna get as many little details right as possible: a guy having to rewrite a story and it bugs the hell out of him, another guy talking to a reporter on the phone and saying, Well, its not Watergate for Gods sake. Little, tiny – you cant even call them subplots – that most people on the first screening wont even notice, probably. Its just sort of newsroom background.’” {{cite news
 | last = Carr
 | first = Jay
 | coauthors =
 | title = Director Ron Howard goes to press with The Paper
 | work = Boston Globe
 | pages =
 | language =
 | publisher =
 | date = October 10, 1993
 | url =
 | accessdate =  }} 

==Reception==

===Box office===
The Paper was given a limited release in five theaters on March 18, 1994 where it grossed $175,507 on its opening weekend. It later expanded its release to 1,092 theaters where it made $7 million over that weekend. The film went on to gross $38.8 million in North America and $9.6 million in the rest of the world for a total of $48.4 million worldwide. {{cite news
 | last =
 | first =
 | coauthors =
 | title = The Paper
 | work = Box Office Mojo
 | pages =
 | language =
 | publisher =
 | date =
 | url = http://www.boxofficemojo.com/movies/?id=paper.htm
 | accessdate = 2010-03-05 }} 

===Critical response===
The Paper received positive reviews from critics and holds an 88% rating on  , Jay Carr wrote, "It takes a certain panache to incorporate the ever-present threat of your own extinction into the giddy tradition of the newspaper comedy, but The Paper pulls it off. Theres no point pretending that Im objective about this one. I know its not Citizen Kane, but it pushes my buttons". {{cite news
 | last = Carr
 | first = Jay
 | coauthors =
 | title = The Paper gets the story right
 | work = Boston Globe
 | pages =
 | language =
 | publisher =
 | date = March 25, 1994
 | url =
 | accessdate =  }}  Peter Stack of the San Francisco Chronicle wrote, “In the end, The Paper offers splashy entertainment thats a lot like a daily newspaper itself – hot news cools fast.” {{cite news
 | last = Stack
 | first = Peter
 | coauthors =
 | title = Extra! Extra! Paper Really Delivers!
 | work = San Francisco Chronicle
 | pages =
 | language =
 | publisher =
 | date = March 25, 1994
 | url =
 | accessdate =  }}  Entertainment Weekly gave the film a "B" rating and Owen Gleiberman praised Michael Keatons performance: "Keaton is at his most urgent and winning here. His fast-break, neurotic style – owlish stare, motor mouth – is perfect for the role of a compulsive news junkie who lives for the rush of his job", but felt that the film was "hampered by its warmed-over plot, which seems designed to teach Henry and the audience lessons". {{cite news
 | last = Gleiberman
 | first = Owen
 | coauthors =
 | title = The Paper
 | work = Entertainment Weekly
 | pages =
 | language =
 | publisher =
 | date = March 18, 1994
 | url = http://www.ew.com/ew/article/0,,301442,00.html
 | accessdate =  2010-03-05 }} 

However, in her review for The New York Times, Janet Maslin was critical of the film. "Each principal has a problem that is conveniently addressed during this one-day interlude, thanks to a screenplay (by David Koepp and Stephen Koepp) that feels like the work of a committee. The films general drift is to start these people off at fever pitch and then let them gradually unveil lifes inner meaning as the tale trudges toward resolution." {{cite news
 | last = Maslin
 | first = Janet
 | coauthors =
 | title = A Day With the People Who Make the News
 | work = The New York Times
 | pages =
 | language =
 | publisher =
 | date = March 18, 1994
 | url = http://movies.nytimes.com/movie/review?_r=2&res=9E0DE1DF163CF93BA25750C0A962958260 Jurassic Park and his brother Stephen (of Time magazine) are witty and on target in terms of character, but their message in terms of male and female relations is a prehistoric one." {{cite news
 | last = Kempley
 | first = Rita
 | coauthors =
 | title = Stop the Presses! Roll The Cameras! Its The Paper
 | work = Washington Post
 | pages =
 | language =
 | publisher =
 | date = March 25, 1994
 | url = http://www.washingtonpost.com/wp-srv/style/longterm/movies/videos/thepaperrkempley_a0a41b.htm
 | accessdate = 2007-05-08 }} 

==References==
 

==External links==
*  
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 