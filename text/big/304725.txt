Problem Child (film)
 
{{Infobox film
| name = Problem Child
| image = Problem Child.JPG
| alt = 
| caption = Theatrical release poster
| director = Dennis Dugan
| producer = {{Plainlist|
* Robert Simonds
* Executive:
* Ron Howard
* Brian Grazer}} Scott Alexander Larry Karaszewski
| starring = {{Plainlist|
* John Ritter
* Jack Warden Michael Oliver
* Gilbert Gottfried
* Amy Yasbeck
* Michael Richards}}
| narrator = Michael Oliver
| music = Miles Goodman
| cinematography = Peter Lyons Collister
| editing = {{Plainlist|
* Tom Finan
* Daniel P. Hanley
* Mike Hill}}
| studio = Imagine Entertainment Universal Pictures
| released =  
| runtime = 81 minutes  
| country = United States
| language = English
| budget = $10 million 
| gross = $72.2 million  
}} Michael Oliver.

==Plot==

The film opens with a woman leaving a bassinet on the porch of a fancy home; the baby, Junior, promptly pees on the woman who picks him up. From there, he is repeatedly discarded at various homes throughout many years by guardians who have grown tired of his destructive behavior—which includes throwing a rattle at the window, giving a cat soap to eat, using a vacuum cleaner to suck up the fish tank, and demolishing a mobile home with a bulldozer in retaliation for his favorite toys being stepped on—until he is eventually deposited at a Catholic orphanage, where he continues to wreak havoc on the strict nuns.

Benjamin "Ben" Healy is a pleasant but brow-beaten husband working for his father, Big Ben, a tyrannical sporting goods dealer who is running for mayor. Recently, he has discovered that his father intends to sell his store and the land to a Japanese company rather than leave it to him; when he asks why, Big Ben reveals that it is because his son "stubbornly refuses to follow his example" by adopting an honest work ethic instead of a ruthless drive to usurp. He would love to have a son, but his selfish, gold-digging wife, Flo, has been unable to conceive. He approaches less-than-scrupulous adoption agent Igor Peabody with his dilemma, and Igor presents them with a cute 7-year-old boy, Junior.

However, Junior is hardly a model child; apparently mean-spirited and incorrigible. He leaves a path of serious destruction in his wake, and is even pen pals with Martin Beck, a notorious serial killer called the Bow Tie Killer. He sets his room on fire using a clown lamp and throws Fuzzball, the family cat, at Big Ben and he falls down the stairs. He messes up a camping trip with the neighbors by peeing on the campfire, and manipulating a practical joke played on the kids by their father, Roy, by luring a real black bear to the campsite. He makes Ben believe a bear is attacking when it is really Roy in a bear costume who gets hit with a frying pan. Hearing Junior laugh, Ben finds out he is responsible for making him hit Roy. Next day, at a birthday party, Lucy, the snobby birthday girl, and her friends are very cruel to him and ban him from watching the magic show. However, he wants revenge and sneaks a water sprinkler into her room, forces a boy to pin a tail on a womans butt instead of the donkey, cuts off another girls braid with scissors, puts a frog in the punch bowl, replaces the piñata candy with pickles including the juice, throws all the presents in the pool, and replaces the birthday candles with firecrackers. This leads Ben to punish him and take away his allowance. Seeing Junior upset, Ben gives him his most precious possession, a dried prune that belonged to his grandfather (he thought it resembled Franklin D. Roosevelt|Roosevelt), telling him it signifies a bond between two people.

Finally, Junior displays his effective but unethical method for winning in a Little League baseball game where he beats up rival players with a bat after they tease him. Ben is having serious doubts about him, and decides to take him back to the orphanage. However, upon hearing he was returned thirty times, he decides to keep and love him, something no one has ever done. However, Junior becomes upset that his parents were going to send him back and despite Ben stating that he will not, he drives Flos car into Big Bens store, and Bens bank account is wiped out to pay for the damage. He is on the verge of cracking until Martin (believing that Junior is the criminal J.R.) arrives at the house, posing as his uncle, and decides to kidnap his faithful correspondent, along with Flo, for ransom.

While Ben first sees this as good riddance to the browbeating Flo and the trouble making Junior, he soon notices signs that the latter is not the monster he appeared. In his drawer is the prune carefully wrapped up and through a series of pictures he drew, he depicts Big Ben and Flo as deformed monsters with hostile surroundings, but Ben as a happy person in a pleasant background, revealing that he really did value him as a father figure all along. Realizing that his behavior was simply a response to how he himself had been treated, and that it has simply been bad luck that he has dealt with too many cruel and selfish people at such a young age, Ben undertakes a rescue mission to retrieve him back from Martin.

He then confronts Big Ben (who is preparing to make a TV appearance for his mayoral campaign) to loan him the ransom money. When he callously refuses, Ben activates the camera that unknowingly puts Big Ben on live TV, where he ends up revealing his true nature on the news. Afterward, Ben steals Roys car and goes to rescue Junior. He catches up with Martin and Junior at the circus. Junior is rescued after escaping from Martin through a trapeze act and calls Ben "Dad" for the first time. Martin drives away, but the Healys are now on his trail. After a collision, Flo (who was stuffed in a suitcase) is thrown into the air and lands in the back of a farm truck loaded with pigs. Martin is arrested, but while being led away, he grabs an officers sidearm and fires at Junior, but Ben shields him and takes the shot. Thinking he is dead, Junior apologizes for all the bad things he did and tells him he will never be naughty again and he loves him. He wakes up and tells Junior he loves him, too, and realizes the bullet ricocheted off his good-luck prune, which was in his pocket. Junior asks Ben if he really believed that he was going to stop misbehaving, but Ben tells Junior he wants him to be himself. Junior then removes his bow tie and throws it over the bridge, perhaps as a sign that he has changed his ways not to be like Martin, but be himself. He is then carried home by his new father. The film ends with Flo in the truck looking out from the suitcase, only to be met by the rump of a pig.

The title track to the film, performed by The Beach Boys, plays over the closing credits.

==Cast==
* John Ritter as Ben Healy, a caring person who wants to have a child. He adopts Junior and bonds with him. He is hated by his wife, neighbors, and father. Michael Oliver as Junior, the main protagonist and the mischievous prankster in the titular film. He was returned to the orphanage thirty times as he always causes trouble wherever he goes. He is often surrounded by selfish and mean people which gives him the need to punish them with his pranks, thus making himself be hated by those around him. He has a devious but inventive mind and is also good with electricity and machinery. He desires to have a family and he eventually meets Ben, who he develops a bond with as Ben is the only one who is ever nice to him.
* Jack Warden as Big Ben Healy, Bens rich, but selfish father. He hates Junior, having called him the devil upon their first meeting. He runs for mayor of Cold River but lost due to insulting the citizens on TV (the result of Ben purposely turning the camera on when Big Ben refuses to help him rescue Junior).
* Gilbert Gottfried as Igor Peabody, the adoption agent at the orphanage who thinks that Junior at first is an angel but turns out he is wrong and cons Ben and Flo into taking him. Later he reveals that Junior was returned to the orphanage 30 times.
* Amy Yasbeck as Flo Healy, Bens wife. The only reason she wanted to adopt was so she could get invited to neighbors parties and dinners. She is quite selfish and unfaithful to Ben as she makes love to Martin in the kitchen. She serves as the secondary antagonist in the film.
* Michael Richards as Martin Beck, an escaped convict who goes by the moniker "The Bow Tie Killer" for his affinity for a bow tie. He has cravings for a clown-themed snack called Smiley Pies, even though he (like Junior) hates clowns. He serves as the main antagonist in the film.
* Peter Jurasik as Roy, Ben and Flos neighbor. He thinks that he is a Super Dad due to his wife being pregnant with their 6th child. He scared the kids with a bear costume, was mistaken for a real bear by Ben, and hit with a frying pan.
* Colby Kline as Lucy, a selfish, mean, and spoiled 6-year-old daughter. She begrudgingly invites Junior to her birthday party at her mothers insistence. She tells him that he is not allowed to watch the magic show for touching her presents, but he gets his revenge by ruining the party.
* Dennis Dugan (cameo) as All American Dad

==Production== Farmers Branch, Fort Worth, Irving, Texas|Irving, and Mesquite, Texas|Mesquite.

During a 2014 interview on Gilbert Gottfrieds Amazing Colossal Podcast, screenwriters   or The Omen, Alexander and Karaszewski thought it had potential as a comedy, envisioning a dark, adult satire of the then-popular trend in films where cute kids teach cynical adults how to love, as seen in Baby Boom, Parenthood (film)|Parenthood (directly spoofed by the films poster  ), Look Whos Talking, Uncle Buck, Mr. Mom, Kindergarten Cop and 3 Men and a Baby. However, the studio insisted upon turning it into a childrens film, a conversion which necessitated numerous reshoots and rewrites, leading to a difficult production that left all involved disappointed and anticipating it to bomb. The film defied these expectations, becoming a surprise hit and Universals most profitable film of 1990 but was still so embarrassing for Alexander and Karaszewski (Alexander even cried after the cast and crew screening) that the two tried to distance themselves from the film, which proved difficult. Studios were initially reluctant to hire them or take them seriously based on their work on such a prominent disreputable film but, as the years went by, they would eventually come to work with executives who were children when it first came out, grew up watching its frequent TV airings and were excited to be meeting its writers. Looking back, they still feel it is "a mess," but take some pride in being involved with one of the "very few   childrens films THAT black and THAT crazy" (citing the scene where Flo commits adultery with an escaped serial killer while her husband is catatonic and contemplating murdering his seven-year-old son in the next room as an example) adding "and its funny." 

==Release==

===Box office===
The film debuted at No. 3.  It went on to be a commercial success at the box office, grossing $53 million domestically and $72 million worldwide.

===Critical reception===
The film received widely negative reviews upon its release. On the film review aggregator site Rotten Tomatoes, it received a critics rating of 4% based on 28 reviews, and an audience rating of 41%.  On Metacritic, the film has a 27/100 rating based on 12 critics, indicating "generally unfavorable reviews". 

Although the film was rated PG, it is still heavily censored when shown on television due to the remarks made about adoption, which critics saw as insensitive.    Problem Child was not screened for critics prior to its release. 

Hal Hinson, writing for  . Its basically about tearing stuff up, and after a while you grow tired of seeing variations on the same joke of a cute kid committing horrible atrocities." 

===Protests over posters===
One of the posters for the film showed a cat in a tumble dryer, with the implication being that Junior had put it inside.   A group named In Defence Of Animals organised protests against the posters, and some cinemas took them down in response.  The group also objected to a scene in the film in which Junior splinters a cats legs. 

==Accolades==
For the film (as well as The Adventures of Ford Fairlane and Look Whos Talking Too), Gilbert Gottfried was nominated for a Golden Raspberry Award for Worst Supporting Actor, but lost to Donald Trump in Ghosts Cant Do It.

===Home media===
The film was more successful on home video.  The VHS version adds an extra bit just before the closing credits, in which Junior interrupts the film, to tell the audience that hell be back next summer for Problem Child 2. Then he disappears and a loud noise is heard, followed by Ben shouting "Junior!", him laughing, then the closing credits roll. The VHS version was released on January 31, 1991.
 US on March 2, 2004, as a package entitled Problem Child Tantrum Pack. These films were presented in open-matte full screen only.  However, no home video release thus far features the deleted footage shown on TV airings of the film.
 Kicking & Screaming, and Major Payne) in anamorphic widescreen (being the films first widescreen Region 1 DVD release) on August 5, 2008. 

==Legacy==

===Sequels===
The film inspired two  , was released theatrically in 1991; the second,  , was a television film aired on the USA Network in 1995. The first one brought back the original cast in their original roles and picked up where the first film ended. However, Yasbeck was given a new role with a new dynamic totally opposite to her original character. Junior in Love, the third and final film, recast Ben and Junior with William Katt and Justin Chapman, while Gottfried and Warden reprised their roles as Mr. Peabody and Big Ben.

===Animated series===
  animated TV series that aired in 1993. Gottfried was the only original cast member to be featured as a voice-over actor, making him the only cast member involved in all three films as well as the cartoon (Warden was in all three films, but not the TV series). NBC has ordered a pilot for a TV series based on the film.

==TV version==
Twelve minutes worth of deleted footage were featured in most, if not all, television airings of the film. None of the following scenes have ever been available on DVD.   The first TV version aired on September 15, 1991, on NBC|NBC-TV. The profanity in it was re-dubbed with milder obscenities and phrases.
* A scene where Ben and Flo are leaving for church and their neighbor scolds them for the mess Fuzzball made in her yard and he is forced to pick it up. This also explains why he intentionally drives over there near the end of the film, on his way to rescue Junior, fixing a plot hole. This is then followed by an extra establishing shot of the orphanage. This would have taken place right after Flo agrees to adopting a child, and says "Okay! Lets get a kid."
* A scene at church where Ben explains his troubles to the priest. This would have taken place right after Martin throws a dumbbell at the warden in prison.
* A longer version of Ben and Flos first meeting with Igor in his office. In this missing footage, he inquiries as to whether they cared about what hair and eye color the kid they wanted to adopt had. A brief outtake of this scene can also be seen in early trailers for the film. The regular version picks up where it left off, right after Igor takes down a big book and puts it on his desk, following his line "A child that you can take into your home, and love."
* A scene with Junior packing as he talks to the Mother Superior. 
* A short scene right before Ben and Flo meet Junior for the first time, where they get ready for him.
* A longer version of Martins ink blot examination. Normally, the scene plays out with the man who was examining him kicks out the warden for interrupting him, and, after resuming the examination, Martin, out of nowhere, says "I see...BLOOD!", proceeding to strangle the psychiatrist to death. But in this version, after resuming, the camera then does a zoom-in to segue into a hallucination scene from Martin. During it is a flashback from a prior event, in which he thinks he was being blamed for a crime that someone else did. He, accompanied by his priest, is about to be sent to the electric chair, but not before receiving a cake in the form of a bow-tie from "JR" (whom he doesnt know is Junior). On the way to the chair, he grabs the priest, puts a cake knife in front of his neck, and shoves the warden to the chair. He then flees from the chamber, locks the door, and yells at the warden, "Dont wait up for me, warden! Ill get the lights!", as it ends, and the regular film continues afterwards.
* A short scene shortly after Junior exists the house in which Roy yells for Ben to hurry up.
* A slightly-longer shot of the campsite, just before the movie shows Ben and Junior at Campsite #32.
* A scene following the camping trip in which Ben and Junior have a conversation about the latters painting, depicting Roy being knocked out. This would have taken place right after Ben sees that Junior taught their talking parrot, Polly, to say "Dickhead", and right before the scene at Lucys birthday party. During this missing footage, we also learn more about how Junior got to know Roy better than Ben, saying to him, "He had it coming. Roy only pretends to be your friend. He doesnt like you, and his family doesnt like me."
* A scene in which Junior causes havoc with a RC helicopter by terrorizing the milkman and paperboy. The copter then crashes into the front doors glass. A photo of this scene is shown on the back of the original VHS release, and also appears in early trailers for the film, as well. It would also explain why, in the next scene, the glass was virtually destroyed, fixing another plot hole. Junior is also wearing an outfit from Big Bens Sporting Goods, after he crashed Flos car into it, resulting in a lot of damage. This entire scene is a parody of Apocalypse Now, right down to the use of Richard Wagners Ride of the Valkyries, and Junior saying "I love the smell of spilled milk in the morning. It smells like victory!".
* A scene in which Martin calls up Ben after he finds out Junior and Flo have been kidnapped.
* A scene where Ben arrives at the circus and uses a backpack to pretend $100,000 is inside it. The Mother Superior can be seen here in her actual overall last appearance in the film.
* A chase scene between Ben and Martin. During this scene, the latter bumps into a woman named Cindy from Leavenworth, dressed up as the Bearded Lady, who asks him if he recognizes her. He shoves her out of his way and she calls out to him, saying, "You told me you loved me! You called me your little kumquat!".

==References==
 

==External links==
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 