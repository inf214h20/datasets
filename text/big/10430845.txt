Almost an Angel
{{Infobox film
| name           = Almost an Angel
| image          = Almost an angel film.jpg
| caption        = Theatrical release poster
| director       = John Cornell
| producer       = John Cornell Paul Hogan (executive producer)
| writer         = Paul Hogan
| starring       = Paul Hogan Elias Koteas Linda Kozlowski
| music          = Maurice Jarre
| cinematography = Russell Boyd
| editing        = David Stiven
| distributor    = Paramount Pictures
| released       = December 19, 1990
| runtime        = 95 minutes
| country        = US English
| budget         = $25 million
| gross          = $6,939,946 (USA)
}}

Almost an Angel is a 1990 American comedy film directed by John Cornell and starring Paul Hogan.  The original music score was composed by Maurice Jarre. The films tagline is: "The guy from down under is working for the man upstairs."
 Crocodile Dundee movies (which were also from Paramount Pictures), but the film was a critical and commercial failure.

==Plot== electronic surveillance systems, stands before his release from yet another stint in prison. Following a fellow inmates suggestion, he decides to switch to bank robbery instead, with a special twist of his own design: first by having the security cameras record TV shows he would connect them to with a modified remote control, then entering disguised as a celebrity; the confusion over this unexpected appearance would serve to confound a detailed description.

Terrys first heist is successful, but shortly afterwards he witnesses a young boy about to be overrun by a van; he impulsively pushes the child away and is himself hit. While in the hospital, he has a nebulous experience (which may have been caused by Highway to Heaven playing on the rooms TV) in which he meets God (Charlton Heston; this is used as a pun later on) who introduces himself as Terrys probation helper. Though Terry has lived a sinful life, his last deed, impulsive as it was, has earned him a second chance to save his soul - by doing Gods work as an angel in training.

After reawakening, Terry tries another hold-up (this time as Rod Stewart), but a stroke of bad luck and a gang of amateurs interfere. During this, one of the thugs tries to shoot but fails to kill him (he had loaded his gun with blanks). Thinking himself to be an angel now, Terry reconsiders his ways, seeks advice in a church, and then he follows several signs to another town. In a bar, he meets Steve Garner (Elias Koteas), an embittered young man confined to a wheelchair by a terminal sickness. In order to bring Steve out of his self-pity, Terry engages him in a fist-fight on equal terms, sitting fixed on a stool. Steve, taken with Terrys acceptance of him as a person, not a cripple, strikes up a friendship with Terry and offers him a place to stay at the youth center for children and teens, which he runs with his sister Rose (Linda Kozlowski).

Rose is at first suspicious about Terry, but Terry proves himself by slyly intimidating two drug dealers into leaving the centers area and helping out as much as he can, and Rose gradually falls in love with him. The center itself, however, is in financial difficulties, since its backer George Bealeman (  Rev. Bartons (Ben Slack) telecast (which Bealeman watches feverently), and fitting the cross at the rooftop of the centers church with lighting effects, triggered by his universal remote.

At the evening where Bealeman drops by, however, two police detectives close in on Terry. Steve, who happens to overhear them, rushes off in his wheelchair to warn Terry, but during the flight he injures himself critically, slowly bleeding to death. Just after Bealeman has left, he arrives at the center, and while Rose runs to calls an ambulance, Steve delivers his warning. Afraid of death, Steve feels lost, but is reassured he will find a place in Heaven when Terry uses the remote to trigger the lighted cross, creating a sign from God. No longer afraid of death, Steve dies in the arms of Terry and Rose.

Terry then announces that he has to leave, and trying to comfort Rose, he reveals that he is "almost an angel". Rose is understandably skeptical, but after Terry leaves, she checks his universal remote which he had left her as a keepsake, only to discover that it contains no batteries, and the cross nevertheless begins to shine brilliantly on its own. She runs after Terry and calls out to him. Distracted, Terry slips and falls right before a speeding truck and is about to be run over. Rose is shocked to witness that the truck passes right though him, proving he was really an angel all along. Having passed his angels exam, Terry continues on his quest to do Gods work (though not without promising to return), and Rose is left comforted at last.

==Cast==
Paul Hogan	 ... 	Terry Dean 
Elias Koteas	... 	Steve Garner 
Linda Kozlowski	... 	Rose Garner 
Doreen Lang	... 	Mrs. Garner 
Douglas Seale	... 	Father 
Ruth Warshawsky	... 	Irene Bealeman 
Parley Baer	... 	George Bealeman 
Michael Alldredge	... 	Det. Sgt. Freebody 
David Alan Grier	... 	Det. Bill 
Larry Miller	... 	Teller 
Travis Venable	... 	Bubba 
Robert Sutton	... 	Guido 
Ben Slack	... 	Rev. Barton, TV Evangelist 
Troy Curvey, Jr.	... 	Tom the Guard 
Eddie Frias	... 	Young Guard Trainee 
Peter Mark Vasquez	... 	Thug 
Lyle J. Omori	... 	Thugs Crony 
Joseph Walton	... 	Prisoner #1 
Steven Brill	... 	2nd Male Teller 
Richard Grove	... 	Uniformed Cop 
Susie Duff	... 	Mother 
Justin Murphy	... 	Small Boy 
Gregory J. Barnett	... 	Driver (Van) (billed as Greg Barnett) 
Ray Reinhardt	... 	Doctor 
Laurie Souza	... 	Young Nurse 
Hank Worden	... 	Pop, Hospital Patient

==Reception==
The film had a mostly negative reception.      It currently holds a 29% rating on Rotten Tomatoes.

===Box office===
The film was also a commercial failure.  It grossed just under $7 million in ticket sales on a $25 million budget.

==References==
 

==External links==
*   at Box Office Mojo
*   at Rotten Tomatoes
*   at The Numbers
*  
*  

 
 
 
 
 
 
 