Shopgirl
{{Infobox film
| name           = Shopgirl
| image          = Shopgirl.jpg
| border         = yes
| alt            = 
| caption        = Theatrical release poster
| director       = Anand Tucker
| producer       = {{Plainlist|
* Ashok Amritraj
* Jon Jashni
* Steve Martin
}}
| screenplay     = Steve Martin
| based on       =  
| narrator       = Steve Martin
| starring       = {{Plainlist|
* Steve Martin
* Claire Danes
* Jason Schwartzman
}}
| music          = Barrington Pheloung
| cinematography = Peter Suschitzky David Gamble
| studio         = {{Plainlist|
* Touchstone Pictures
* Hyde Park Entertainment
}}
| distributor    = {{Plainlist| Buena Vista Pictures  
* 20th Century Fox  
}}
| released       =  
| runtime        = 104 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = $11,588,205 
}} romantic comedy-drama novella of the same name. The film is about a complex love triangle between a bored salesgirl, a wealthy businessman, and an aimless young man.
 Buena Vista Pictures, Shopgirl was released on October 21, 2005 and received generally positive reviews from film critics. The film went on to earn $11,112,077  and was nominated for four Satellite Awards, including Best Picture and Best Adapted Screenplay.

==Plot== Beverly Hills. Her quiet, orderly existence - filled with both the mundane (futon furniture and an aging pickup truck) and the serious (a large student loan and a supply of antidepressants) - is disrupted by the sudden appearance of two disparate men.
 typographer who enters Mirabelles life first (in a laundromat). Mirabelle, aching for any kind of meaningful contact with someone else, gives him a chance, but it quickly fizzles after a half-hearted date (that she pays for) followed by a woefully underwhelming sexual encounter.

Ray Porter (Steve Martin) is a considerably older, suave, well-dressed, wealthy, divorced logician. Ray charms Mirabelle over the course of a few dates, one of which ends at his house. Mirabelle offers herself to him, and the morning after they have sex Ray tells her that he does not intend for their relationship to be serious due to his constant traveling between L.A. and Seattle. Each has a different understandings of this talk: Ray tells his psychiatrist that Mirabelle knows that he is going to see other people, and Mirabelle tells her acquaintances that Ray wanted to see her more. 
 roadie for the band Hot Tears. Jeremy attempts to have one last liaison with Mirabelle before leaving, but she spurns him due to her relationship with Ray. While on tour, the bands lead singer introduces Jeremy to the world of self-improvement and how to better relate to the opposite sex. Mirabelle becomes increasingly devoted to Ray, who showers her with expensive gifts (such as paying off her student loans) instead of emotional affection. When Mirabelles depression hits her hard (she has ceased to take her antidepressants because Ray makes her happy), he takes her to the doctor and cares for her, further deepening her reliance on him. Ray invites Mirabelle on a trip to New York, and Ray has her fitted in the dress shop at Armani. 

During a business trip, Ray has dinner with an old girlfriend. During dinner she propositions him and he accepts. Ray confesses the liaison to Mirabelle.  Devastated, Mirabelle ends the relationship, abandons her trip to New York and visits Vermont instead. While she is basking in the warmth and familiarity of home, Ray calls to apologize for hurting her and asks her to meet him in New York. She accepts. He takes her to a large party where she is the youngest guest, and she feels alone and out of place. When they return to the hotel room, Ray wants to be intimate, but Mirabelle rejects him.

After returning to California, Mirabelle meets Jeremy by chance on the way to an art gallery show, and they arrive together at the show.  Her coworker Lisa (who has been suspicious of the new clothes Mirabelle has been wearing to work) mistakes Jeremy for Ray. Jeremys path of self-improvement has changed him, a fact obvious to everyone but Mirabelle. After the show, Mirabelle goes home with Ray, and Lisa goes home with Jeremy thinking hes Ray. The next morning Ray devastates Mirabelle by announcing that he plans on looking for a bigger house in case he meets someone and decides to have kids.  Jeremy calls Lisa thinking they have made a connection, but quickly finds out she has no interest in anything but Ray Porter (and his money). 

Mirabelle permanently ends her relationship with Ray. She quits her job at Saks and takes one as a receptionist in an art gallery. Jeremy pursues her again. They eventually fall in love. Mirabelle is invited to show her work at a gallery, and Ray attends the opening with his new girlfriend. Mirabelles interaction with him is noticeably strained. At the end of their conversation, Ray apologizes for how deeply he hurt her and admits that he did love her. Mirabelle is touched by his admission, but turns away nearly in tears and turns to Jeremy for solace.  Ray remarks that he had kept her "at arms length" to avoid the pain of their inevitable breakup, but that he is hurt by the loss regardless.

==Cast==
* Steve Martin as Ray Porter
* Claire Danes as Mirabelle Buttersfield
* Jason Schwartzman as Jeremy Bridgette Wilson-Sampras as Lisa Cramer
* Sam Bottoms as Dan Buttersfield
* Frances Conroy as Charlotte Buttersfield
* Rebecca Pidgeon as Christie Richards
* Samantha Shelton as Loki
* Gina Doctor as Del Ray
* Clyde Kusatsu as Mr. Agasa
* Romy Rosemont as Loan officer
* Joshua Snyder as Trey Bryan Rachel Nichols as Treys girlfriend
* Shane Edelman as Chet
* Mark Kozelek as Luther

==Production notes==
In Steve Martins original novella, Mirabelle was employed by Neiman Marcus. According to Evolution of a Novella: The Making of Shopgirl, a bonus feature on the DVD release, Saks Fifth Avenue actively pursued participation in the film by presenting a proposal to the producers and director and promising full cooperation with filming schedules. The gloves in the counter are not from Saks, but a boutique in Toronto, where some of the film was shot.

According to Martins book Born Standing Up, there are many parallels to Martins own life.  Early in his career, he lost a girlfriend to an older, suave gentleman resembling Ray Porter, real-life Mason Williams. Williams had a house that matches the description of Ray Porters, it overlooked Los Angeles from roughly the same vantage point and the descriptions of the two houses are the same. Williams was an actuary at one point, whereas Porter was a logician. Martin and Williams both vied for the attention of a girlfriend, Nina. The relationship ended when Martin, much like the Schwartzman character, goes on a cross country tour as a roadie. These parallels make the novella somewhat autobiographical.  

In addition, character Mirabelle is partially based on artist Allyson Hollingsworth who was a consultant to the movie, and who had a relationship with Martin in the 90s. Photographs and drawings attributed to Mirabelle in the movie are by Hollingsworth.  

Martin had Tom Hanks in mind for the role of Ray Porter at the time he was writing the screenplay, but director Anand Tucker felt that Martin was so close to the material and had such a strong understanding of the character that Martin should play the part himself. After auditioning numerous actresses, he knew Claire Danes was perfect for the role of Mirabelle as soon as she began reading lines with Martin. He found Jeremy much more difficult to cast, and remembered Jason Schwartzman (but not his name) from his performance in Rushmore (film)|Rushmore only two weeks before filming was scheduled to begin. 

The apartment building used for Mirabelles residence is located at 1630 Griffith Park Boulevard in Los Angeles.

The songs "Lily & Parrots", "Carry Me Ohio" and "Make Like Paper" were written and performed by Mark Kozelek. Tucker remembered him from his appearance in Almost Famous and cast him as the lead singer of Hot Tears. Both "Carry Me Ohio" and "Lily and Parrots" were tracks on Ghosts of the Great Highway, the first CD released by Kozeleks real-life band Sun Kil Moon. "Make Like Paper" was a track from Songs for a Blue Guitar, an album by Kozeleks earlier band Red House Painters.
 
The film premiered at the Toronto Film Festival in September 2005. It was shown at the Chicago International Film Festival and the Austin Film Festival before going into limited release in the US. 

The film grossed $10,284,523 in the US and $1,303,682 in foreign markets for a total worldwide box office of $11,588,205.   

==Critical reception==
At Rotten Tomatoes, the film has a score of 61% (90 "Fresh" and 58 "Rotten" reviews), for an average rating of 6.3 out of 10,  and is rated a 62 (based on 37 reviews) at Metacritic. 

In his review in the New York Times, A.O. Scott called the film "elegant and exquisitely tailored . . . both funny and sweetly sad" and added, "  is a resolutely small movie, finely made and perhaps a bit fragile. Under the pressure of too much thought, it might buckle and splinter; the characters might look flimsy, their comings and goings too neatly engineered, their lovability assumed rather than proven. And its true that none of them are perfect. From where I sit, though, the film they inhabit comes pretty close."   

Mick LaSalle of the San Francisco Chronicle described it as "a film of wisdom, emotional subtlety and power . . . directed with a rare combination of delicacy and decisiveness."  

In Variety (magazine)|Variety, Joe Leydon observed, "Martin hits all the right notes while subtly conveying both the appealing sophistication and the purposeful reserve of Ray. But he cannot entirely avoid being overshadowed by Danes endearingly vulnerable, emotionally multifaceted and fearlessly open performance. (In a few scenes, she appears so achingly luminescent its almost heartbreaking to watch her.) The two stars bring out the very best in each other, particularly in a poignant final scene."  

Carina Chocano of the Los Angeles Times said the film is "like Pygmalion (play)|Pygmalion for the upper-middle-brow business class flier. Which isnt to say its bad. On the contrary, its smart, spare, elegant and understated . . . Danes can fill a scene with one wounded glance, and her body language alone conveys a richness of character that makes an otherwise not very expressive character mesmerizing."  
 Lost in Bringing Down the House, this is the Martin who writes for The New Yorker with erudition and wit."  

Steve Persall of the St. Petersburg Times graded the film C and called it "too slight to be considered a movie yet padded enough to pose as a feature-length work . . . The blessing and curse of cinema is its ability to compress ideas into simple images. When the ideas are this simple, cinema crushes them to dullness. Mirabelles unremarkable life simply doesnt deserve big screen treatment. Any author other than a Hollywood favorite like Martin likely wouldnt get it done."  
 New York, Ken Tucker stated, "The challenge of the movie consists of making you believe that these two people, separated by age and status, could fall in love. Shopgirl succeeds in this with a confidence so sure and serene that you feel through much of the movie as though you’re listening to a fairy tale, an effect enhanced by the voice-over narration provided in soothing tones by Martin-as-Ray."  
 The Hours) Lost in Translation drained of its wryly observed humor, Shopgirl is worth a browse. But it isnt always easy to buy."  

==Awards==
* Satellite Award for Best Picture - Musical or Comedy – Nominated
* Satellite Award for Best Adapted Screenplay – Nominated
* Satellite Award for Best Actress - Motion Picture Musical or Comedy (Claire Danes) – Nominated
* Satellite Award for Best Supporting Actor - Motion Picture (Jason Schwartzman) – Nominated
* Costume Designers Guild Award for Best Costume Design - Contemporary Film (Nancy Steiner) – Nominated

==References==
 

==External links==
*  
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 