Kelinlar qoʻzgʻoloni (film)
{{Infobox film
| name = Kelinlar qoʻzgʻoloni/Келинлар қўзғолони 
| image = Bunt_nevestok.jpg
| alt=
| caption = A Theatrical poster in Russian for Kelinlar qoʻzgʻoloni 
| director = Melis Abzalov
| producer = 
| writer = Said Ahmad
| starring =  
| music = Mirhalil Mahmudov
| cinematography = 
| editing = 
| studio = Uzbekfilm
| distributor = 
| released =  
| runtime = 70 minutes
| country = Uzbek SSR
| language = Uzbek
| budget = 
| gross = 
}}
 Uzbek comedy Soviet period.  Like Melis Abzalovs previous film Suyunchi, Kelinlar qoʻzgʻoloni tells the story of an authoritative grandmother.

==Plot==
Farmon bibi (played by Tursunoy Jaʼfarova) is a wise and loving, but strict mother who lives with the families of her seven sons in one house. Nigora, the wife of her youngest son, rebels against Farmon Bibi and the other wives sympathize with her. Toward the end of the film, Farmon bibi changes her attitude and gives in to the demands of her daughters-in-law.

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 


 
 