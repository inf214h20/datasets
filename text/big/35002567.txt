Afrikaaps
 
 
 
{{Infobox film
| name           = Afrikaaps
| image          = 
| alt            =  
| caption        = 
| director       = Dylan Valley
| producer       = Plexus Films
| screenplay     = Dylan Valley Khalid Shamis
| starring       = 
| music          = 
| cinematography = Dylan Valley
| editing        = Khalid Shamis
| studio         = 
| distributor    = 
| released       =   
| runtime        = 52 minutes
| country        = South Africa
| language       = 
| budget         = 
| gross          = 
}}
Afrikaaps is a South African 2010 documentary film.

== Synopsis ==
This documentary focuses on a theatre piece entitled Afrikaaps within a film. It is based on the creative processes and performances of the stage production. Using hip-hop the film and the stage play attempt to reclaim Afrikaans&nbsp;– so long considered a language of the oppressor&nbsp;– as a language of liberation. Present from the beginning of the project, Dylan Valley captures revealing moments of the casts and production crews personal narratives that transcend what happens on stage.

== Awards ==
* Encounters International Documentary Festival, South Africa, 2010

== References ==
 

 
 
 
 
 
 


 
 