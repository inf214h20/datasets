Pump Up the Volume (film)
{{Infobox film
| name           = Pump Up the Volume
| image          = Pump Up The Volume.jpg
| image_size     =
| caption        = Theatrical poster
| director       = Allan Moyle
| producer       = Syd Cappe Sara Risher Sandy Stern Nicolas Stiliadis
| writer         = Allan Moyle
| starring       = Christian Slater Samantha Mathis
| music          = Cliff Martinez
| cinematography = Walt Lloyd
| editing        = Larry Rock
| distributor    = New Line Cinema
| released       = August 22, 1990
| runtime        = 105 min.
| country        = United States, Canada English
| gross          = $11,541,758 (USA)
}}
Pump Up the Volume is a 1990 comedy-drama film written and directed by Allan Moyle and starring Christian Slater and Samantha Mathis.

== Plot summary == FM pirate Everybody Knows" cassettes by such alternative musicians as The Jesus and Mary Chain, Camper Van Beethoven, Primal Scream, Soundgarden, Ice-T, Bad Brains, Concrete Blonde, Henry Rollins, and The Pixies.  By day, Mark is seen as a loner, hardly talking to anyone around him; by night, he expresses his outsider views about what is wrong with American society. When he speaks his mind about what is going on at his school and in the community, more and more of his fellow students tune in to hear his show. 

Nobody knows the true identity of "Hard Harry" or "Happy Harry Hard-on," as Mark refers to himself, until Nora Diniro (Mathis), a fellow student, tracks him down and confronts him the day after a student named Malcolm commits suicide after Harry attempts to reason with him. The radio show becomes increasingly popular and influential after Harry confronts the suicide head-on, exhorting his listeners to do something about their problems instead of surrendering to them through suicide—at the crescendo of his yelled speech, an overachieving student named Paige Woodward (who has been a constant listener) jams her various medals and accolades into a microwave and turns it on.  She then sits, watching the awards cook until the microwave explodes, injuring her. While this is happening, other students act out in cathartic release.
 FCC is called in to investigate. During the fracas, it is revealed that the schools principal (Annie Ross) has been expelling "problem students," namely, students with below-average SAT scores, in an effort to boost the districts test scores while still keeping their names on the rolls (a criminal offense) in order to keep the government money.

Realizing he has started something huge, Mark decides it is up to him to end it. He dismantles his radio station and attaches it to his mothers old jeep, creating a mobile transmitter so his position cant be triangulated. Pursued by the police and the FCC, Nora drives the jeep around while Mark broadcasts. The harmonizer he uses to disguise his voice breaks, and with no time left to fix it, Mark decides to broadcast his final message as himself. They finally drive up to the crowd of protesting students, and Mark tells them that the world belongs to them and that they should make their own future. The police step in and arrest Mark and Nora. As they are taken away, Mark reminds the students to "talk hard." As the film ends, the voices of other students (and even one of the teachers) speak as intros for their own independent stations, which can be heard broadcasting across the country.

== Main cast ==
* Christian Slater as Mark Hunter
* Samantha Mathis as Nora Diniro
* Mimi Kennedy as Marla Hunter
* Scott Paulin as Brian Hunter
* Cheryl Pollak as Paige Woodward
* Annie Ross as Loretta Creswood
* Ahmet Zappa as Jaime
* Billy Morrissette as Mazz Mazzilli
* Seth Green as Joey
* Robert Schenkkan as David Deaver
* Ellen Greene as Jan Emerson
* Andy Romano as Mr. Murdock
* Anthony Lucero as  Malcolm Kaiser
* Lala Sloatman as Janie James Hampton as Arthur Watts

==Production== Times Square, a new wave comedy, was taken away from him and re-edited, Allan Moyle retired from directing and began working on screenplays. One of them, about a teenager who runs his own pirate radio station for other teenagers, came to the attention of SC Entertainment, a Toronto-based company, and put into development.    He was persuaded to direct his own screenplay. Moyle wrote it without a specific actor in mind but his development deal specified that the project would be canceled if a suitable actor could not be found. The director needed an actor who had to have "glee, to be ineffably sweet and at the same time demonic."  Christian Slater met with Moyle and producer Sandy Stern and displayed all these qualities. Moyle has described the films protagonist as an amalgam of Holden Caulfield and Lenny Bruce  and the "Hard Harry" persona as a guy who "has to get credibility as an outsider. As the last angry man on the planet, he has to use the foulest language he can think of. He even pretends to masturbate on the air. Hes obsessed with sex and death."  The school in the film, Hubert Humphrey High, was based on a Montreal high school where director Allan Moyles sister used to teach that, according to Moyle, had a principal "who had a pact with the staff to enhance the credibility of the school scholastically at the expense of the students who were immigrants or culturally disabled in some way or another."   

Slater disagreed with Moyle who wanted to bring in a tap dance instructor to help orchestrate a scene that begins with "Hard Harry" faking masturbation on the air and ends with him breaking into a manic dance by himself. Slater wanted to do something more spontaneous based on his instincts.   

==Reception==
Pump Up the Volume failed to catch on at the box office. When it was released on August 24, 1990, in 799 theaters, it grossed USD $1.6 million in its opening weekend. It went on to make $11.5 million in North America.   

The film received generally positive reviews from critics and is currently rated 78% at Rotten Tomatoes. In his review for the New York Times, Stephen Holden wrote, "Much like Heathers, Pump Up the Volume doesnt know how to draw out its premise, once that premise has been thoroughly explored. As the film accelerates toward its conclusion, the strands of its clever plot are too hastily and perfunctorily resolved . . . Working within the confines of the teen-age genre film, however, Pump Up the Volume still succeeds in sounding a surprising number of honest, heartfelt notes".    USA Today gave the film three-and-a-half stars out of four, praising the films conclusion: "the ending, though in part contrived, doesnt cop out".   

===Awards=== Jesus of Montreal. Reportedly, some audience members booed when the film was named the winner.  Moyles film also won the Audience Award at the Deauville Film Festival.   

 
 
 
 
 
 

==Media==
 

===Soundtrack===
{{Infobox album  
| Name        = Pump Up the Volume
| Type        = Soundtrack
| Longtype    =
| Artist      = Various Artists
| Cover       = PUTVOST.jpg
| Released    = August 14, 1990
| Recorded    =
| Genre       = Soundtrack
| Length      =
| Label       = MCA
| Producer    =
| Last album  =
| This album  =
| Next album  =
}}
{{Album ratings
| rev1 = Allmusic
| rev1Score =   
}}
Music being central to the plot of a film about a young pirate radio station DJ, the soundtrack featured a diverse number of artists.  The official soundtrack release had the following tracks:
 Everybody Knows" (Leonard Cohen) – Concrete Blonde
# "Why Cant I Fall in Love?" – Ivan Neville
# "Stand! (song)|Stand" (Sly and the Family Stone) – Liquid Jesus
# "Wave of Mutilation (UK Surf)" – Pixies Peter Murphy
# "Kick Out the Jams" (MC5) – Bad Brains with Henry Rollins Above the Law
# "Heretic" – Soundgarden
# "Titanium Exposé" – Sonic Youth
# "Me and the Devil Blues" (Robert Johnson) – Cowboy Junkies
# "Tale O The Twister" – Chagall Guevara
 1969 song by Sly & the Family Stone.

Peter Murphys exclusive track was later included on a special reissue of his 1988 album, Love Hysteria, while Sonic Youths song appeared on their 1990 release, Goo (album)|Goo.
 Deep Six. Concrete Blonde revisited "Everybody Knows" on their 2003 album, Live in Brazil. The original, upbeat version of "Wave of Mutilation" appears on Doolittle (album)|Doolittle, the third studio album by Pixies.
 Blank Generation, and "Talk Hard" by Stan Ridgway, the original version of which has never been released (though Ridgway has released a live version of the song).

Not as prominently featured is a legendary early track by the Beastie Boys entitled "The Scenario". Although the song appears only briefly in Pump Up the Volume, it is notable because it never appeared in any official release, however is available on hard to find bootleg recordings. The song was cut from the Beasties Def Jam album Licensed to Ill after being deemed too explicit. Christian Slaters character explains this when he introduces it on the air saying, "Now heres a song from my close personal buddies, the Beastie Boys...a song that was so controversial they couldnt put it on their first album."

==References==
 

==External links==
 
*  
*  
*   

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 