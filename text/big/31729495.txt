Battle of the Brides
{{Infobox film
| name           = Battle of the Brides
| image          =
| caption        = Film Poster (U.S. release)
| director       = Victor Vu
| producer       = Pham Viet Anh Khoa,  Ngoc Hiep
| writer         = Victor Vu,  Hong Phuc|
| screenplay     = 
| story          = 
| based on       =  
| starring       = 
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 100 minutes
| country        = Vietnam
| language       = Vietnamese
| budget         = 500,000
| gross          = $1,900,000(VIETNAM)  $64,572(USA)
}}
 Vietnamese comedy film directed by Victor Vu,  produced by Saiga Films and Vietnam Studio, in association with Galaxy Studios, Phuong Nam Phim, Saigon Movies Media and HK Films. Battle of the Brides was released on January 28, 2011 in Vietnam and broke box office records, becoming the country’s highest grossing movie of all time.  However, in the United States the movie was a Box office bomb, just grossing only $64,572.

== Plot ==
In the heart of Saigon, Thai and Linh are  getting married.  But the wedding is suddenly cut short when four other brides show up – threatening to take the grooms life. It turns out that Thai is the biggest player in the city, and has been dating all five women at the same time up until the day of his wedding. Trang is an overly jealous flight attendant, Mai Chau is a doctor who loves to party, Quyen is a sexually aggressive chef while Huynh Phuong is one fiery actress. 

As Thai confesses and desperately tries to explain himself to Linh, his multiple lovers unleash their furious vengeance to teach him a lesson he will never forget.

== Cast ==
* Huy Khanh as Thai
* Ngoc Diep as Linh
* Le Khanh as Quyen
* Ngan Khanh as Trang
* Phi Thanh Van as Huynh Phuong
* Van Trang as Mai Chau

== Reception ==
 
In Vietnam it soon became one of the most highest grossing movies ever.  The local Vietnamese audience found the film to be socially relevant in its bleak, yet comedic, depiction of relationships in modern day Saigon.  Meanwhile in the USA audience often complained that the movie was just "another sleazy movie that the Vietnamese made". It has become an in office boom  due to the movie have too many special effect.

== References ==
 

== External links ==
*   on the Internet Movie Database
*  
*    

 

 
 
 
 
 


 