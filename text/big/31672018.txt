The House of Peril
  silent drama film directed by Kenelm Foss and starring Fay Compton, Roy Travers, Flora le Breton and A.B. Imeson.  It is an adaptation of the novel Chink in the Armour by Marie Belloc Lowndes. The film follows Sylvia Bailey, a wealthy widow who travels to a French gambling resort where she encounters assorted characters.

==Cast==
* Fay Compton - Sylvia Bailey
* Roy Travers - Bill Chester
* A.B. Imeson - Comte de Virieu
* Flora le Breton - French Maid
* Madeline Seymour - Anna Wolsky
* Wallace Bosco - Polperro
* Nelson Ramsey - Herr Wachner
* Irene Tripod - Frau Wachner
* Blanche Walker - Maid George Bellamy - Gambler
* Hubert Carter - Gambler
* Jeff Barlow - Gambler Lewis Gilbert - Gambler
* Tom Coventry - Gambler
* Madge Tree - Gambler

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 


 