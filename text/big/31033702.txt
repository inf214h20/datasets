The Naked Street
{{Infobox film
| name           = The Naked Street
| image          = Neked Street 1955 Poster.jpg
| image_size     = 
| alt            = 
| caption        = Theatrical release poster
| director       = Maxwell Shane
| producer       = Edward Small
| screenplay     = {{plainlist|
* Maxwell Shane
* Leo Katcher
}}
| story          = Leo Katcher
| starring       = {{plainlist|
* Farley Granger
* Anthony Quinn
* Anne Bancroft
}}
| music          = {{plainlist| Ernest Gold
* Emil Newman
}}
| cinematography = Floyd Crosby
| editing        = Grant Whytock
| studio         = Edward Small Productions
| studio         = World Pictures
| distributor    = United Artists
| released       =  
| runtime        = 84 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
The Naked Street is a 1955 American crime film noir directed by Maxwell Shane.  The drama features Farley Granger, Anthony Quinn and Anne Bancroft.

==Plot==
Phil Regal, a tough racketeer, pulls strings to get his sisters punk boyfriend out of the death house.

==Cast==
* Farley Granger as Nicholas Nicky Bradna
* Anthony Quinn as Phil Regal
* Anne Bancroft as Rosalie Regalzyk
* Peter Graves as Joe McFarland
* Else Neft as Mrs. Regalzyk
* Sara Berner as Millie Swadke
* Jerry Paris as Latzi Franks
* Mario Siletti as Antonio Cardini
* James Flavin as Attorney Michael X. Flanders
* Whit Bissell as Dist. Atty. Blaker
* Joe Turkel as Shimmy
* Joyce Terry as Margie Harry Tyler as I. Barricks
* Jerry Hausner as Louie

==Reception==
When the film was released, The New York Times film critic, Bosley Crowther, panned the film, writing, "The whole spectacle is dismal and uninspiring. The only cheerful thing that occurs is that the sister and wife, played by Anne Bancroft, falls in love with and marries a newspaper man."   TV Guide rated it 3/5 stars and called it a "solid, fast-paced crime tale". 

==References==
 

==External links==
*  
*  
*  
*   film clip at YouTube

 

 
 
 
 
 
 
 
 
 
 