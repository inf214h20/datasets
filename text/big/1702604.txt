The Mirror Crack'd
 
 
{{Infobox film
| name           = The Mirror Crackd
| image          = The Mirror Crackd - poster.jpg
| image_size     =
| caption        = Theatrical release poster
| director       = Guy Hamilton Richard Goodwin
| screenplay     = Jonathan Hales Barry Sandler
| based on       =  
| narrator       = Edward Fox Geraldine Chaplin Tony Curtis John Cameron
| cinematography = Christopher G. Challis
| editing        = Richard Marden
| studio = EMI Films GW Films
| distributor    = Associated Film Distribution
| released       =  
| runtime        = 105 minutes
| country        = United Kingdom
| language       = English
| gross          = $11,000,000 
}}
 British mystery Edward Fox, Rock Hudson and Pierce Brosnan in his film debut.
 mystery was adapted by Jonathan Hales and Barry Sandler. Scenes were filmed at Twickenham Film Studios, Twickenham, London, UK, and on location in Kent.

==Plot== costume movie Elizabeth I reception held by the movie company in a manor house, Gossington Hall, to meet the celebrity|celebrities. Lola and Marina come face to face at the reception and exchange some potent and comical insults, nasty one-liners, as they smile and pose for the cameras. The two square off in a series of clever verbal cat-fights throughout the movie.
 anonymous death threats. After her initial exchange with Lola at the reception, she is cornered by a gushing, devoted fan (aficionado)|fan, Heather Badcock (Maureen Bennett), who bores her with a long and detailed story about having actually met Marina in person during World War II. After recounting the meeting they had all those years ago, when she arose from her sickbed to go and meet the glamorous star, Babcock drinks a cocktail that was made for Marina and quickly dies from poisoning. The incident is unfortunate for Marinas mental state, and she is beside herself. Everyone is certain she was the intended murder victim. Once filming begins on the movie, she discovers that apart from threatening notes made up of newspaper clippings, her cup of coffee on the set has also been spiked with poison, sending her into fits of terror. The police detective from Scotland Yard investigating the case, Inspector Dermot Craddock (Edward Fox), is baffled as he tries to uncover who is behind the attempt on the life of the actress and the subsequent murder of the innocent woman. The suspected are Ella Zielinsky (Geraldine Chaplin), Jasons production assistant who is secretly having an affair with him and would like Marina out of the way, and the hotheaded actress Lola Brewster.
 Wendy Morgan), who was working as a waitress the day of the murder, the determined elderly sleuth begins to piece together the events of the fatal reception and solves the mystery. By the time she has collected all the evidence to indicate who committed the crime, however, another death occurs at Gossington Hall, which explains who was the killer: Marina Rudd, who has apparently committed suicide.

In the films denouement, Miss Marple explains the murders that have occurred. Heather Babcocks story was Marinas initial motive. Ms. Babcock suffered from German measles — a rather harmless disease to most adults, but problematic for a pregnant woman. Heather Babcock innocently infected Marina when she met her during World War Two. Marina was pregnant at the time; the disease caused her child to be born with mental retardation. Upon hearing Heather cheerfully tell this story, Marina was overcome with rage and poisoned her without thinking. She then spread the idea that she was the intended victim, delivering the death threats and poisoning her own coffee. Ella, who made phone calls to various suspects from the pay phone, accidentally guessed correctly, prompting Marina to murder her. As Marina is now dead, she will not be brought to justice. Jason confesses to Miss Marple that he had put poison in her hot chocolate so as to save her from being prosecuted. However, Marina didnt touch the hot chocolate he made for her and took the poison herself directly.  

==Probable real-life inspiration==
Christies inspiration for the motive likely came from an incident in the real-life of American film star Gene Tierney. In June 1943, while pregnant with her first daughter, Tierney contracted German measles during her only appearance at the Hollywood Canteen. Due to Tierneys illness, her daughter was born deaf, partially blind with cataracts, and severely developmentally disabled. Some time after the tragedy surrounding her daughters birth, the actress learned from a fan who approached her for an autograph at a tennis party that the woman (who was then a member of the womens branch of the Marine Corps) had sneaked out of quarantine while sick with German measles to meet Tierney at her only Hollywood Canteen appearance. In her autobiography, Tierney related that after the woman had recounted her story, she just stared at her silently, then turned and walked away. She wrote, "After that I didnt care whether ever again I was anyones favourite actress."  

Biographers theorise that Christie used this real-life tragedy as the basis of the plot of The Mirror Crackd from Side to Side.    The incident, as well as the circumstances under which the information was imparted to the actress, is repeated almost verbatim in Christies story. Tierneys tragedy had been well-publicized.

==Title==
The title is part of a line from The Lady of Shalott by the English poet Alfred Tennyson, 1st Baron Tennyson|Alfred, Lord Tennyson:

:Out flew the web and floated wide—
:The mirror crackd from side to side;
:"The curse is come upon me", cried
:The Lady of Shalott.

==Cast==
* Angela Lansbury as Miss Jane Marple
* Elizabeth Taylor as Marina Rudd
* Rock Hudson as Jason Rudd
* Tony Curtis as Martin Fenn
* Kim Novak as Lola Brewster
* Geraldine Chaplin as Ella Zelinsky Edward Fox as Inspector Craddock Charles Gray as Bates Richard Pearson as Doctor Haydock
* Pat Nye as Mayoress
* Pierce Brosnan as actor playing Jamie (uncredited)
 Anthony Steel, Nigel Stock, John Bennett and Allan Cuthbertson are among the actors who appear in Murder At Midnight, a black and white teaser movie shown at the beginning of the film.

==Production==
Natalie Wood was originally meant to play the role eventually depicted by Elizabeth Taylor. 
===Filming locations=== Shoreham are Ashford district of Kent, and the traditional thatched houses and village shops made it a perfect filming location.  

==See also== Murder at Midnight (1931)
*The Mirror Crackd from Side to Side (novel) The Mirror Crackd (TV movie)
*Murder, She Wrote (a US television mystery series starring Angela Lansbury in a similar role to Miss Marple)

==References==
 

==External links==
* 

 
 

 
 
 
 
 
 