The Great Plane Robbery (1940 film)
{{Infobox film
| name           = The Great Plane Robbery
| image          = File:TheGreatPlaneRobbery-Poster.png
| image_size     = 
| border         = 
| alt            = 
| caption        = Theatrical release poster
| film name      = 
| director       = Lewis D. Collins
| producer       = Larry Darmour
| screenplay     = Albert DeMond
| story          = Harold Greene Jack Holt Stanley Fields Noel Madison
| music          = Lee Zahler
| cinematography = James S. Brown, Sr.
| editing        = Dwight Caldwell
| studio         = Larry Darmour Productions
| distributor    = Columbia Pictures
| released       =  
| runtime        = 53 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 Stanley Fields Hal Erickson found it ironic that Holt, who in real life had a fear of flying, starred in so many aviation-oriented films.   It was written by Albert DeMond from a story by Harold Greene. 

==Plot==
Special agent Mike Henderson (Jack Holt) has been assigned by an insurance company to protect gangster Joe Colson (Noel Madison). Joe has recently been released from prison, just three months before his life insurance policy, worth half a million dollars, is due to expire.

After Mike arrives at Leavenworth, Kansas, on the day of Colsons release, he discovers that two gangsters, Eddie (Paul Fix) and Nick (Harry Cording), are there as well, waiting to see Joe. They greet him and then take Joe aboard an aircraft, with Mike following closely behind. That night, Eddie and Nick storm the cockpit and force the pilot to land near a lodge where Frankie Toller (Stanley Fields), Joes successor, awaits. When they arrive, Frankie divulges that he will hold Joe prisoner until Joe reveals where his fortune is stashed. Later, he sees Mike and inducts him into the gang, believing he is one of Joes old friends.

The aircrafts disappearance becomes a worldwide sensation. The next morning, the gang discovers that an insurance company detective was a passenger on the missing aircraft. The gang assumes it is salesman Homer Pringle (Hobart Cavanaugh). Mike, managing to convince Frankie that Joe is completely broke, proposes that they force Homer to call the insurance company with an offer to ransom Joe. Accordingly, Mike and two others go into town, where Mike stages the fake killing of Homer by shooting him with blanks. Mike and the gang members return to Frankie, while Homer telephones the police. As Frankie prepares to flee, the police arrive and arrest him and the rest of the gang. Afterward, the passengers board the aircraft and finally reach their destination. Meanwhile, Joe is taken into custody for a different offense, assuring Mike that he will outlive his policy.

==Cast==
  (1929).]] Jack Holt as Mike Henderson Stanley Fields as Frankie Toller
* Vicki Lester as Helen Carver  
* Noel Madison as Joe Colson
* Granville Owen as Jim Day
* Theodore Von Eltz as Rod Brothers
* Hobart Cavanaugh as Homer Pringle
* Milburn Stone as  Krebber
* Paul Fix as Eddie Lindo
* Harry Cording as Nick Harmon
* John Hamilton as Dr. Jamison
* Doris Lloyd as Mrs. Jamison
* Lane Chandler as Bill Whitcomb

==Production==
Principal photography, under the working title of Keep Him Alive, took place from April 14 to 24, 1940.  

==Reception==
The Great Plane Robbery received negative reviews. In his book Aviation in the Cinema, Stephen Pendo called The Great Plane Robbery, a "poor Jack Holt adventure."  Bosley Crowther, in his review for The New York Times, wrote that "Holt has come up against a lot of dull, witless scripts, but it is doubtful that any was as colorless and static as The Great Plane Robbery ... this one is recommended only to Jacks most rabid admirers."  

In a later review, Hal Erickson commented, "The first half of the film is a mini-Grand Hotel, giving way to three climactic reels of nonstop action and suspense.  (1937)." Erickson, Hal.    AllRovi. Retrieved: September 20, 2014.   A review in the book VideoHounds Golden Movie Retriever gave The Great Plane Robbery one and a half stars. 

==References==
===Notes===
 
===Citations===
 
===Bibliography===
 
* Pendo, Stephen. Aviation in the Cinema. Lanham, Maryland: Scarecrow Press, 1985. ISBN 0-8-1081-746-2.
* Schaefer, Jack. The Movies and the People who Make Them, Volumes 2-3. New Haven, Connecticut: Theatre Patrons, Inc., 1940.
*   Chicago: Gale Group, 2006. ISBN 0-78768-980-7.
 

==External links==
*  
*  

 
 
 
 
 
 
 
 