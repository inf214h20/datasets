Wabash Avenue (film)
{{Infobox film
| name           = Wabash Avenue
| image          = Wabashavenue1950.jpg
| image_size     = 
| caption        = 
| director       = Henry Koster
| producer        = William Perlberg
| writer         = Charles Lederer Harry Tugend
| narrator       = 
| starring       = Betty Grable Victor Mature
| music          = Cyril J. Mockridge
| cinematography = Arthur E. Arling Robert L. Simpson
| distributor    = 20th Century Fox
| released       = May 24, 1950
| runtime        = 92 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = $2,050,000 (US rentals) 
| preceded_by    = 
| followed_by    = 
}} 1950 American musical film Coney Island.

==Plot==

Ruby Summers is a burlesque queen in a successful dance hall in 1892 Chicago. The owner of the dance hall Mike has cheated his ex-partner Andy Clark out of a half interest in the business. Andy schemes to potentially ruin Mike and also hopes to make Ruby a classy entertainer, as well as his own girl.

==Cast==
*Betty Grable as Ruby Summers
*Victor Mature as Andy Clark
*Phil Harris as Mike Stanley
*Reginald Gardiner as English Eddie James Barton as Harrigan
*Barry Kelley as Bouncer Margaret Hamilton as Tillie Hutch
*Jacqueline Dalya as Cleo
*Robin Raymond as Jennie
*Hal K. Dawson as Healy
*Dorothy Neumann as Reformer
*Alexander Pope as Charlie Saxe
*Henry Kulky as Joe Barton
*Marie Bryant as Elsa
*Collette Lyons as Beulah
*George Beranger as Wax Museum Attendant

==Background== Coney Island.  The Kahn biopic was made at Warner Bros. in 1951 as I’ll See You in My Dreams (1951 film)|I’ll See You in My Dreams, with Danny Thomas as Kahn.  
 Coney Island My Blue Heaven was also among the highest grossing films of that year. 
 Best Original Song for the number Wilhelmina

==References==
 

==External links==
* 
*  

 

 

 
 
 
 
 
 
 