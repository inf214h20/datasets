Dead Man Running
{{Infobox film
| name           = Dead Man Running
| image          = Dead man running.jpg
| caption        = Dead Man Running theatrical poster
| alt            = 
| director       = Alex De Rakoff
| producer       = Pikki Fearon
| writer         = Alex De Rakoff
| screenplay     = John Luton
| story          = John Luton Curtis "50 Cent" Jackson Monet Mazur Brenda Blethyn
| cinematography = Ali Asad
| editing        = Alan Strachan
| music          = Mark Sayfritz
| distributor    = Revolver Entertainment
| released       =  
| runtime        = 92 minutes
| country        = United Kingdom
| language       = English
| budget         = $1 million 
| gross          = $735,875   
}} Football players Ashley Cole and Rio Ferdinand served as executive producers.

== Plot == Curtis 50 Cent Jackson) decides to take matters into his own hands, travelling to London to make an example of debtor Nick (Tamer Hassan). Thigo gives Nick just 24 hours to pay back the £100,000 he owes, and, just to make sure, Thigo holds Nicks wheelchair-using mother (Brenda Blethyn) hostage. Since Nick is already financially challenged, he is forced to be creative in order to come up with the money. At the same time, Thigo sabotages Nicks efforts in order to be sure that he can make an example of Nick to the other debtors.

== Cast ==
*Tamer Hassan as Nick
*Danny Dyer as Bing
*50 Cent as Thigo
*Brenda Blethyn as Mother
*Monet Mazur as Frankie Phil Davis as Johnny Sands Alan Ford as Sol
*Ashley Walters as Fitzroy

== Release ==
The film was released in the United Kingdom on 30 October 2009.  It grossed $681,354 in the UK and $735,875 in total foreign gross.   Phase 4 Films released it in the US. 

== Reception == Time Out London called it "budget-conscious, simplistically plotted and often cringingly performed".   Philip French of The Guardian wrote that it is "a little uncertain in tone, but brisk and likely to go down well with the patrons of Albert Squares Queen Vic."   Peter Bradshaw, also of The Guardian, rated it 2/5 stars and wrote, "For all the sub-Guy Ritchie cliches, it has its moments" and "is not as bad as it could have been."   Robert Hanks of The Independent wrote, "Is there any way of stemming the flow of post-Guy Ritchie cockney crime comedies? Would, say, sticking Danny Dyers head on a pike somewhere in Bethnal Green be enough of a deterrent?"   Derek Elley of Variety (magazine)|Variety wrote that the film "recycles Cockney crimer cliches to moderately entertaining results." 

== References ==
 

== External links ==
*  
*  

 
 
 
 
 
 