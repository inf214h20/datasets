American Yearbook
{{Infobox film
| name = American Yearbook
| image =
| caption =
| director = Brian Ging
| producer = Brian Ging Jason F. Brown
| writer = Brian Ging
| starring = Nick Tagas Jon Carlo Alvarez
| music = Jeremiah Jacobs
| cinematography = Dan Coplan
| editing = Brian Ging
| released = June 15, 2004
| runtime = 97 minutes
| country = United States English
| budget = $500,000
}} public release date.

==Plot==
Will Nash (Nick Tagas) is the kid-next-door, a typical upbeat high schooler, but his dreams of being a photographer quickly fade, as he is relentlessly terrorized by school bullies Ian (Chris Peter) and Jason (Ryan Nixon). Will adores Amanda ( . Amanda pleads with Will to set revenge aside, but can Will stop what he and Chance have started, or are Wills emotions too strong for him to just walk away?

==Cast==
*Nick Tagas as William "Will" Nash
*Jon Carlo Alvarez as Chance Holden
*Chris Peter as Ian Blake
*Ryan Nixon as Jason Clarke
*Giovannie Pico as Amanda Hunter
*Jennifer Noble as Kristy Palmer
*Daniel Timko as Brandon Holden

==See also==
 
* List of American films of 2004 Bang Bang Youre Dead, a 2002 TV film about a school shooting Zero Day, a 2003 film about a school shooting.
* Elephant (2003 film)|Elephant, another 2003 film about a school shooting.
* Duck! The Carbine High Massacre, a 2000 film about a school shooting. The Only Way, another 2004 film about a school shooting.

==External links==
*  

 
 
 
 
 
 
 
 
 


 