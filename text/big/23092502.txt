Autumn Ball
{{Infobox film 
| name           = Autumn Ball
| image          = Sügisball (2007).jpg
| caption        = 
| director       = Veiko Õunpuu 
| producer       = Katrin Kissa
| writer         = Screenplay: Veiko Õunpuu Novel: Mati Unt
| narrator       = 
| starring       = 
| music          = Ülo Krigul
| cinematography = Mart Taniel
| editing        = Veiko Õunpuu
| distributor    = 
| released       =  
| runtime        = 127 minutes Estonia
| language       = Estonian
| budget         = 
| gross          = 
}}
 Estonian drama drama film 1979 novel of the same name. The film depicts six desolate people of different yet similar fates in characteristically Soviet pre-fabricated housing units (khruschevka).  

==Cast==
* Rain Tolk as Mati
* Taavi Eelmaa as Theo
* Tiina Tauraite as Ulvi
* Maarja Jakobson as Laura
* Mirtel Pohla as Jaana
* Sulevi Peltola as Augusti Kaski
* Iris Persson as Lauras daughter
* Juhan Ulfsak as Maurer
* Ivo Uukkivi as Lauras ex-husband
* Katariina Lauk as female conference visitor
* Paul Laasik as television repairman

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 