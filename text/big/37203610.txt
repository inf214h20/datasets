Life Goes On (2009 film)
{{Infobox film
| name = Life Goes On
| image = Life Goes On 2009 film poster.jpg
| caption = Film poster
| director = Sangeeta Dutta
| producer = 
| writer = 
| starring = Girish Karnad  Sharmila Tagore  Om Puri  Soha Ali Khan 
| cinematography = 
| editing = 
| released =  
| runtime = 120 minutes
| country = United Kingdom
| language = English
| budget = 
}}
Life Goes On is a 2009 film by Sangeeta Datta. It shows how a Hindu family in Britain copes with the death of the wife and mother.

==Plot==
The film is set in modern London. The central character is Sanjay, a Hindu doctor, respected in his community. After the sudden death of his wife Manju, he struggles to relate to his three daughters. The drama follows the week from Manjus death to her funeral.

As well as dealing with grief and family ties, the film addresses inter-faith issues as Sanjay discovers that the youngest daughter Dia has a Muslim boyfriend, Imtiaz.

Wandering the streets of London the night before the funeral, Sanjay recalls his own childhood, when he left home with his parents during the partition of India.

The story recalls Shakespeare’s King Lear. 

==Cast==
* Girish Karnad as Sanjay
* Sharmila Tagore as Manju
* Om Puri as Alok
* Soha Ali Khan as Dia
* Rez Kempton as Imtiaz
* Neerja Naik as Tuli
* Christopher Hatherall as John
* Mukulika Banerjee as Lolita
* Misha Crosby as Abbas
* Tom Reed as Tom
* Sue Parker-Nutley as Mrs Goldsmith

==Reception==
The film won the Best Feature Film Award at the Pravasi International Film Festival in Delhi, and the Best Feature Film Audience Appreciation Award at the London Asian Film Festival. 

Xan Brooks in the Guardian considered the acting "stiff and self-conscious". 

==References==
 

==External links==
 
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 