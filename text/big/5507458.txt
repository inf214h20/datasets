Grass (1999 film)
 
{{Infobox film
| name           = Grass
| image          = GrassOfficialPoster.jpg
| caption        = Promotional artwork for Grass
| director       = Ron Mann
| producer       = Keith Clarkson
| writer         = Solomon Vesta
| screenplay     = 
| story          = 
| based on       =  
| narrator       = Woody Harrelson
| starring       = 
| music          = 
| cinematography = Robert Fresco
| editing        = Robert Kennedy
| studio         = 
| distributor    = Unapix Home Entertainment
| released       =  
| runtime        = 80 minutes
| country        = Canada
| language       = English
| budget         = 
| gross          = 
}}
 Toronto Film marijuana in the 20th century. The film was narrated by actor Woody Harrelson.

==Overview== war on marijuana."
 Harry Anslinger First Lady President George H.W. Bush accelerated the War on Drugs. The film ends during the Bill Clinton administration, which had accelerated spending even further on the War on Drugs.

Grass is almost completely composed of archival footage, much of which is from public domain United States|U.S. propaganda films and such feature films as Reefer Madness made available by the Prelinger Archives. Woody Harrelson agreed to narrate the film free-of-charge. 

The art director and poster designer of the film was Paul Mavrides.

==Critical reception==
The film was generally well received by critics, scoring 64 out of 100 in Metacritic,  and 71% fresh on Rotten Tomatoes.  The film became a cult hit within the cannabis subculture.

The film has also won Canadas Genie Award for Best Documentary.

==See also==
* Decriminalization of marijuana in the United States
* Legal and medical status of cannabis
* Legal history of marijuana in the United States Marijuana rescheduling in the United States

==References==
 

==External links==
* 
* 
* 
*   at Box Office Mojo

 
 
 
 
 
 
 
 