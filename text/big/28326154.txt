For You Alone
{{Infobox film
  | name           = For You Alone
  | image          = foryoualone.jpg
  | caption        = Promotional poster
  | director       = Geoffrey Faithfull
  | producer       = F.W. Baker
  | writer         = Kathleen Butler   Montgomery Tully
  | starring       = Lesley Brook Dinah Sheridan Jimmy Hanley
  | music          = Harry Bidgood Ernest Palmer
  | editing        = 
  | distributor    = Butchers Film Service
  | released       =  
  | runtime        = 105 minutes
  | country        = United Kingdom
  | language       = English
  }}

For You Alone is a 1945 British World War II romance melodrama, one of only two films directed by cinematographer Geoffrey Faithfull, starring Lesley Brook, Dinah Sheridan and Jimmy Hanley.  The film was made by a smaller studio, Butchers Films &ndash; more known for turning out quickly and cheaply shot B-movies &ndash; and had a lack of (at the time) star names, but appears to have been a relatively sophisticated and well-financed production.  On its release, three weeks after V-E Day, the film became a huge popular success, seeming to catch the mood of a nation just beginning to emerge from war.

For You Alone is often categorised as a musical, and indeed was promoted as such, but this is somewhat misleading;  in crucial concert scenes the film features singers Heddle Nash and Helen Hill with accompaniment from the London Symphony Orchestra; however, none of the films actors sing.

==Plot==
John Bradshaw (Robert Griffith), a young naval officer, attends a lunchtime concert at Westminster Central Hall where he meets Reverend Peter Britton (G.H. Mulcaster) and his daughter Katherine (Brook).  After the concert the three share a taxi, and after seeing her father off on the train to a conference Katherine agrees to have tea with John.  They enjoy each others company and later go to see a film, followed by dinner and a stroll along the Thames Embankment.  John impulsively tells Katherine that he has fallen in love with her, but she reminds him that they hardly know one another, and since her brothers death in the Far East she has to devote herself to her father.

The couple finally part, agreeing to meet again the following day.  However Katherine receives a telegram at her hotel, stating that her brother Dennis (Hanley) has turned up alive and will be arriving home the next day.  She returns home early the next morning, leaving a note of explanation for John.  Unfortunately John forgets the name of Katherines hotel, so does not receive the note and is distraught when she fails to turn up for their rendezvous.  Meanwhile back at home, Katherine finds that Dennis is accompanied by Max Borrow (Manning Whiley), an old admirer who still wants to marry her.  He has sustained serious eye injuries while saving Dennis life, and Katherine as a result feels she must accept him.  Dennis himself immediately rekindles his courtship with local schoolteacher Stella White (Sheridan).

John remembers that Katherines father is due to return to London from the conference and waits at the station until he arrives.  They learn from the hotel why Katherine departed so hurriedly, and Rev. Britton invites John back to their village where he knows the local squire is looking for help in cataloguing his library.  John is deeply upset to discover Katherine is engaged, and also  resentful towards Dennis and Stella for their obvious happiness together.  Katherine finally admits to John the reason she and Max are engaged, and John agrees to not pursue matters unless Max can be cured.

Max goes off for a medical examination, and John is recalled to his ship.  As he is about to leave, a fire breaks out in a storage shed where children are playing.  Max, having been told that his sight is safe, arrives back while the drama is in progress, and John is injured as he rescues the children.  Katherines reaction leaves Max in no doubt as to her feelings.  That evening he tells her that he knows the situation, and will release her from her obligation to him so that she may marry John.

==Cast==
 
 
*  Lesley Brook as Katherine Britton
*  Dinah Sheridan as Stella White
*  Jimmy Hanley as Dennis Britton
*  Robert Griffiths as John Bradshaw
*  G.H. Mulcaster as Rev. Peter Britton
*  Hay Petrie as Sir Henry Markham
*  Olive Walter as Lady Markham
 
*  Manning Whiley as Max Borrow
*  Irene Handl as Miss Trotter George Merritt as P.C. Blundell
*  Muriel George as Mrs. Johns
*  Aubrey Mallalieu as Eye Specialist
*  Heddle Nash as Himself
*  Helen Hill as Herself
 

==Later history== 75 Most Wanted" list of missing British feature films. 

==See also==
*List of lost films

==References==
 

==External links==
*  , with extensive notes
*  
*  

 
 
 
 
 
 
 
 