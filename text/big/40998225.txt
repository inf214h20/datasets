My Prairie Home (film)
{{Infobox film
| name           = My Prairie Home
| image          = My Prairie Home poster.jpg
| border         = 
| alt            = 
| caption        = Promotional poster
| film name      = 
| director       = Chelsea McMullan
| producer       = Lea Marin
| writer         = Chelsea McMullan
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = Rae Spoon
| music          = Rae Spoon
| cinematography = Maya Bankovic Derek Howard
| editing        = Avril Jacobson
| studio         = National Film Board of Canada
| distributor    = 
| released       =  
| runtime        = 77 minutes
| country        = Canada
| language       = English
| budget         = 
| gross          = 
}}
My Prairie Home is a 2013 Canadian documentary film about transgender singer/songwriter Rae Spoon, directed by Chelsea McMullan. It features musical performances and interviews about Spoon’s troubled childhood, raised by Pentecostal parents obsessed with the Rapture and an abusive father, as well as Spoons past experiences with gender confusion. The film was shot in the Canadian Prairies, including the Royal Tyrrell Museum of Palaeontology in Drumheller. My Prairie Home was produced by Lea Marin for the National Film Board of Canada (NFB).            

McMullan has said she first found out about Spoon around 2007, when she was making a western-themed NFB film set in the B.C. Interior. She was searching for "subversive" country-folk soundtrack music when someone suggested Spoon.    According to Spoon, the idea for the documentary came out of a discussion with McMullan in 2010 about the musician’s perceived lack of marketability, a criticism Spoon sometimes receives when applying for music video funding. 

Spoon has stated that it had initially been difficult for to open up so much about personal details, so McMullan suggested writing it down before they talked. Spoon did so, and ended up writing the book First Spring Grass Fire, which was published in the fall of 2012.  The book was a nominee for the 2013 Lambda Literary Awards in the Transgender Fiction category,  and Spoon was awarded an Honour of Distinction from the Dayne Ogilvie Prize for LGBT writers in 2014. 

== Release == streaming to Canadian audiences.   

The film was a shortlisted nominee for the  , January 13, 2014. 

The film was accompanied by a soundtrack album, also titled  , June 19, 2014. 

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 
 
 
 
 