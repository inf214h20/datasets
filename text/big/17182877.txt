Condottieri (1937 film)
{{Infobox film
| name           = Condottieri
| image          = 
| image_size     = 
| caption        = 
| director       = Luis Trenker
| producer       = 
| writer         = Kurt Heuser   Mirko Jelusich   Luis Trenker  
| narrator       = 
| starring       =  Luis Trenker   Loris Gizzi   Laura Nucci   Carla Sveva
| music          = Giuseppe Becce 
| cinematography = Walter Hege   Carlo Montuori   Klaus von Rautenfeld
| editing        = Giorgio Simonelli ENIC
| distributor    =ENIC
| released       = 1937
| runtime        = 88 minutes
| country        = Italy Italy
| budget         = 
}}
 Giovanni de Medici, a celebrated Condottieri of the sixteenth century. A separate German-language version was also made.

The film received 9.6 million  , the film was an attempt to harness history to support the Fascist regimes current policies. Condottieri drew parallels between the dictator Benito Mussolini and the historical figure of de Medici, portraying both as unifying Italy.  The films elaborate sets were designed by Virgilio Marchi.

==Cast==
*Luis Trenker: Giovanni de Medici
*Loris Gizzi: Malatesta
*Laura Nucci: Tullia delle Grazie		
*Carla Sveva: Maria Salviati		
*Ethel Maggi: Caterina Sforza
*Mario Ferrari: Cesare Borgia	
*Angelo Ferrari		
*Giulio Cirino: Rüschli
*Sandro Dani: DArgentière	
*Tito Gobbi: Nino
*Augusto Marcacci: Daniello		
*Nino Marchetti: Corrado
*Lando Muzio: Pedro
*Ernesto Nannicini	
*Umberto Sacripante: Sanzio
*Carlo Tamberlani: Duke of Urbino
*Gino Viotti: Pope
*Giuseppe Addobbati: Duke of Imola
*Friedl Trenker: Giovanni 
*Oreste Bilancia
*Alberto Emmerich Carlo Fontana
*Tullio Galvani
*Luis Gerold: Barbo
*Alberto Minoprio
*Carlo Simoneschi
*Virgilio Botti
*Mara Danieli
*Claudio Ermelli
*Carlo Duse

==References==
 

== Bibliography ==
* Gundle, Stephen. Mussolinis Dream Factory: Film Stardom in Fascist Italy. Berghahn Books, 2013.
* Ricci, Steven. Cinema and Fascism: Italian Film and Society. University of California Press, 2008.

== External links ==
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 


 