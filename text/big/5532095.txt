Alibi Ike
{{Infobox film
| name           = Alibi Ike
| image          = Alibi Ike FilmPoster.jpeg
| border         = yes
| alt            = 
| caption        = Lobby card
| director       = Ray Enright
| producer       = Edward Chodorov
| writer         = 
| screenplay     = William Wister Haines
| story          = 
| based on       =  
| starring       = {{Plainlist|
* Joe E. Brown
* Olivia de Havilland
* Ruth Donnelly
* Roscoe Karns
}}
| music          = 
| cinematography = Arthur L. Todd Thomas Pratt
| studio         = 
| distributor    = Warner Bros.
| released       =  
| runtime        = 72 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} Joe E. Brown and Olivia de Havilland. Based on the short story "Alibi Ike" by Ring Lardner, the film is about an ace baseball player nicknamed "Alibi Ike" due to his penchant for making up excuses. After falling in love with the beautiful sister-in-law of the team manager, he is kidnapped by gangsters who want him to throw the World Series.
 A Midsummer Nights Dream.

==Cast==
* Joe E. Brown as Frank X. Farrell
* Olivia de Havilland as Dolly Stevens
* Ruth Donnelly as Bess
* Roscoe Karns as Carey
* William Frawley as Cap
* Eddie Shubert as Jack Mack Paul Harvey as Lefty Crawford
* Joe King as Johnson, the owner
* G. Pat Collins as Lieutenant
* Spencer Charters as Minister
* Gene Morgan as Smitty

==Production==

===Short story=== only .356. King Cole.

===Cameo appearances=== Wally Hood, Don Hurst, Smead Jolley, Lou Koupal, Bob Meusel, Wally Rehg, and Jim Thorpe.

==References==
 

==External links==
 
*  
*  
*  
*    

 

 
 
 
 
 
 
 
 
 
 
 