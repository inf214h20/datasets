The Real Glory
{{Infobox film
| name           = The Real Glory
| image          = The Real Glory 1939 Poster.jpg
| image_size     = 200px
| caption        = Theatrical release poster
| director       = Henry Hathaway
| producer       = Samuel Goldwyn
| screenplay     = Jo Swerling Robert Presnell, Sr.
| based on          =  
| starring       = Gary Cooper David Niven Andrea Leeds Reginald Owen Broderick Crawford Alfred Newman
| cinematography = Rudolph Maté
| editing        = Daniel Mandell
| studio         = Samuel Goldwyn Productions
| distributor    = United Artists
| released       =  
| runtime        = 96 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}} invasion of American occupation of the Philippines at the beginning of the twentieth century. 

==Plot==
 Moro guerrillas Russell Hicks) and Hartley (Reginald Owen), and Lieutenants McCool (David Niven) and Larsen (Broderick Crawford) - who are to train the native Philippine Constabulary to take over the burden. Army doctor Lieutenant Canavan (Gary Cooper) is sent along to keep them healthy. They are welcomed by a skeptical Padre Rafael (Charles Waldron).

Alipang starts sending fanatical juramentados to assassinate the officers and goad them into attacking before the natives are fully trained. Hatch is the first victim, leaving Manning to take command. Mannings wife (Kay Johnson) and Hartleys daughter Linda (Andrea Leeds) arrive for a visit at the worst possible time; a horrified Mrs. Manning witnesses her husbands murder. Hartley takes charge, but Canavan disagrees with his by-the-book, overly-cautious approach. Disobeying orders, Canavan sets out for Alipangs camp guided by Miguel (Benny Inocencio), a young Moro boy he has befriended. "Mike" (as Canavan calls him) infiltrates the camp and learns that Alipang has sent another assassin, this time for Hartley. Canavan and Mike intercept the man and take him back a prisoner.

Linda and Canavan fall in love, much to the disappointment of McCool and Larsen. When Hartley insists she leave Mysang with Mrs. Manning, she refuses and helps out at the hospital.

Alipang then dams the river on which the villagers depend. Hartley refuses to send a detachment into the jungle to blow it up (he is concealing the fact that he is slowly going blind from an old head wound). The people have to rely on an old well, but the contaminated water causes a cholera epidemic. Finally, Hartley has no choice but to send Larsen and some men to destroy the dam. They do not return.

The Datu (Vladimir Sokoloff), a supposedly friendly Moro leader, offers to guide Hartley and his men to the dam, but he is actually leading them into an ambush. Canavan learns of the Datus treachery from Mike, the sole survivor of Larsens detachment, and races to warn Hartley. Canavan forces the Datu to take him to the dam. The Datu is killed in a booby trap, but Canavan manages to dynamite the dam anyway. Then, he and the men raft back to the village, which is under attack by Alipangs men.

McCool is killed leading the defense, but Canavan and the rest return in time to turn the tide. Alipang is killed by Filipino Lieutenant Yabo (Rudy Robles). Their mission accomplished, the Hartleys and Canavan depart, leaving the village in Yabos care.

==Cast==
* Gary Cooper as Dr. Bill Canavan
* David Niven as Lieutenant Terence McCool
* Andrea Leeds as Linda Hartley
* Reginald Owen as Captain Steve Hartley
* Broderick Crawford as Lieutenant Larsen. Crawford was cast against type as a good-natured orchid fancier.
* Kay Johnson as Mrs. Mabel Manning Russell Hicks as Captain George Manning
* Vladimir Sokoloff as The Datu
* Benny Inocencio as Miguel (Mike)
* Charles Waldron as Padre Rafael
* Rudy Robles as Lieutenant Yabo
* Tetsu Komai as Alipang
* Roy Gordon as Colonel Hatch
* Henry Kolker as The General

==References==
===Notes===
 

===References===
*  
*  
*  
*  
*  
*  
*  
* 

==External links==
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 