You're Never Too Young
{{Infobox film
| name = Youre Never Too Young
| image = yourenevertooyoung.jpg
| director = Norman Taurog
| music = Walter Scharf
| writer = Fanny Kilbourne Edward Childs Carpenter Sidney Sheldon
| starring = Dean Martin Jerry Lewis Diana Lynn Nina Foch Raymond Burr Paul Jones
| distributor = Paramount Pictures
| released =  
| runtime = 102 minutes
| language = English
| gross = $3.4 million (US)  2,202,595 admissions (France) 
}}

Youre Never Too Young (1955 in film|1955) is a comedy film starring the team of Martin and Lewis, released on August 25, 1955 by Paramount Pictures, and co-starring Diana Lynn, Nina Foch, and Raymond Burr.

==Plot==
Wilbur Hoolick, a barbers apprentice, doesnt have enough money for train fare. Wilbur pretends to be an eleven-year-old in order to purchase a ticket for half price. Meanwhile, a valuable diamond has been stolen and the thief hides it in Hoolicks pocket without his knowledge.

On the train, Wilbur gets the impression that the thief is a jealous husband. He hides in the compartment of Nancy Collins, a teacher at a private girls school. Feeling sorry for "young" Wilbur traveling alone, she allows him to stay there for the duration of the train ride.

During a stop-over, another teacher, Gretchen Brendan, boards the train and finds out that Nancy is sharing her compartment with "a man." Gretchen hurries to the school to let Nancys fiancee, Bob Miles, in on this news. In order to protect Nancys job and reputation, Wilbur must continue the charade of pretending to be a child. He accompanies "Aunt Nancy" to school. The jewel thief follows them in order to retrieve the stolen diamond.

Along the way, Wilbur falls in love with Nancy, although she still thinks of him as a little boy. Eventually the thief retrieves the jewel and a chase ensues. In the end he is captured and Wilburs identity is revealed.

==Cast==
* Dean Martin as Bob Miles
* Jerry Lewis as Wilbur Hoolick
* Diana Lynn as Nancy Collins
* Nina Foch as Gretchen Brendan
* Raymond Burr as Noonan
* Tommy Ivo as Marty
* Nancy Kulp as Martys mother
* Tor Johnson as a train passenger (unconfirmed; scene deleted but visible in the films trailer)

==Production==
Youre Never Too Young was filmed from October 18 to December 27, 1954. This film is a remake of another Paramount film, The Major and the Minor (1942 in film|1942), directed by Billy Wilder — his first film as director—and co-written by Wilder and Charles Brackett.

==Re-release==
In 1964, Paramount re-released Youre Never Too Young with another Martin and Lewis film, The Caddy (1953 in film|1953).

==Trivia== My Friend Irma (1949), as well as its sequel, My Friend Irma Goes West (1950).  She is also featured in The Major And The Minor, which Youre Never Too Young is based on.

==DVD release==
The film was included on a five-film DVD set, the Dean Martin and Jerry Lewis Collection: Volume Two, released on June 5, 2007.

==References==
 

== External links ==
* 

 
 

 
 
 
 
 
 
 