Pupendo
{{Infobox film
| name           = Pupendo
| image          = 
| alt            =  
| caption        = 
| director       = Jan Hřebejk
| producer       = 
| writer         = 
| screenplay     = 
| story          = 
| starring       = Bolek Polívka Eva Holubová
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 120 minutes
| country        = Czech Republic
| language       = Czech
| budget         = 
| gross          = 
}} Czech comedy / drama film directed by Jan Hřebejk

== Plot ==
Pupendo shows the difficulty of life in Czechoslovakia during the 1980s. Artist Bedřich Mára (Bolek Polivka) is unable to find much secure work due to his public antagonism toward the ruling Communist Party. He has a wife and two children. Life begins to change when art historian Alois Fábera (Jiři Pecha) begins working on a piece about Bedřich, leading to a job offer from a Party official. Things are looking up, until the wrong people hear portions of the historians writing.

== Cast ==
* Bolek Polívka - Bedrich Mára
* Eva Holubová - Alena Márová
* Jaroslav Dušek - Míla Brecka
* Jiří Pecha - Alois Fábera
* Vilma Cibulková - Magda Brecková
* Lukáš Baborský - Matej Brecka

==External links==
* 

 
 
 
 

 