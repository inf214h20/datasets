Mermaid (2007 film)
{{Infobox Film name = Mermaid image = Mermaidfilm2007.jpg caption = Theatrical release poster director = Anna Melikyan producer = writer = Anna Melikyan narrator = starring = Mariya Shalayeva Yevgeni Tsyganov Mariya Sokova music = cinematography = editing =
| studio = Film Company "Magnum" distributor = Central Partnership released = 22 November 2007 (Russia) runtime = 100 minutes country = Russia
|language = Russian budget = $1,700,000 (estimated)
}}
Mermaid (  film directed and written by Anna Melikyan. It is a loose adaptation of Hans Christian Andersens The Little Mermaid.  It was a box office success in Russia, won numerous awards and was selected as Russias official submission to Foreign-Language Film category for the 2009 Academy Awards. 

==Plot==
The film is a modern interpretation of Andersens fairy tales. 

6-year-old Alice lives with her mother and grandmother near the sea and waits for the return of her father. Alice dreams of becoming a ballerina and going to ballet school, but it does not work out. Alice notices that her mother is having an affair with their neighbor. Walking in on them having sex she shouts: "Traitor! Go to hell!", and in retaliation, she burns down the house. After the eclipse of the sun, Alice stops talking. Alice is then sent to a school for mentally retarded children, where she discovers an ability to fulfill her desires, such as making apples fall from the tree. After wishing to move away and causing a hurricane, she, her mother, and grandmother move to Moscow, having sold her grandmothers jewelry to pay for the tickets.

Alice tries to enter the university, but does not earn enough points on the entrance exam, trying to fulfill her desire to "want to learn." But an admitted student dies in a car accident, and she enters in his place. Alice gets a job walking around the city dressed as a mobile phone. Through the costume she observes the life of the capital, the city communicates with it in the language of advertising signs: "Everything depends on you," "Do not be afraid of their desires," "Follow your own star" ... 

On her 18th birthday, Alice sees a man jump off a bridge. She jumps after him, saves him, and realizes that she falls in love with him.  She begins to talk again.  The man, Sasha, owns a large two-story apartment, selling lots on the moon. Alice settles into his apartment as a maid, cleaning up after parties thrown at the apartment. Alice dyes her hair green in an attempt to get Sashas attention, but fails.

One night at a party, Alice meets a girl named Rita. Rita tells Alice to make a guy fall in love with you, all you have to do is take a cigarette, write his name on it, and smoke it. They both decided to do so, not knowing they had both written the name of the man on it.  The spell works for Alice. That day, she went with Sasha to buy coffee and starred in commercials as a "moon girl" for his business. During their celebration of drinking, Alice tells him that she has never eaten pineapple and search Moscow for fresh pineapple, ending up robbing a fruit truck, and then play the game "Dead Man Laughing."

Alice sees Rita in Sashas apartment. Rita asks Alice if she can fry potatoes because its his favorite dish and her nails are too long to make them. While leaving for work, Alice asks: "Rita... what are you doing here?", and continuously sobs during her work shift.

That night, she walks in on Rita and Sasha having sex and shouts: "Traitor! Go to hell!", while still in her uniform. That night, she has a dream that Sasha is on a plane that crashes.  Alice calls him and tries under the pretext of her grandmothers death to beg him not to go on the business trip. Because of a small car accident, Sasha is late to the airport and then learns that the plane on which he was to fly had in fact crashed. Sasha sits in a cafe and talks about his housekeeper and a plane crash, his friends say, "This is destiny." Sasha sees Alice passing by and runs out of the cafe after her, but loses sight of her. Alice is hit by a car. At this time Sasha meets Rita, she asks, "Who are you looking for?" Sasha hesitantly replies, "You."

==Cast==
* Mariya Shalayeva as Alice Titova
* Yevgeni Tsyganov as Aleksandr Sasha Viktorovich
* Mariya Sokova as Alices mother
* Anastasiya Dontsova as Young Alice
* Irina Skrinichenko as Rita

==References==
 

==External links==
* 
* 

 

 

 
 
 
 
 
 
 