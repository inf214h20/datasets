Madea's Big Happy Family (film)
{{Infobox film
| name           = Madeas Big Happy Family
| image          = Madeas Big Happy Family Poster.jpg
| image_size     = 215px
| alt            = 
| caption        = Theatrical release poster
| director       = Tyler Perry
| producer       = Tyler Perry Reuben Cannon Roger M. Bobb
| writer         = Tyler Perry Shad "Bow David Mann Cassi Davis Tamela Mann Lauren London Isaiah Mustafa Rodney Perry Shannon Kane Teyana Taylor
| music          = Aaron Zigman
| cinematography = Alexander Gruszynski
| editing        = Maysie Hoy
| studio         = Tyler Perry Studios Lionsgate
| released       =  
| runtime        = 106 minutes
| country        = United States
| language       = English
| budget         = $25 million   
| gross          = $54,161,287   
}} play of the same name and the eleventh film in the Tyler Perry film franchise, and the fifth in the Madea franchise.

==Plot==
 
Shirley goes to visit Dr. Evans with Aunt Bam about her cancer and finds out that it has gotten worse. She asks Aunt Bam to call her children so she can tell them. Cora and Mr. Brown are at the hospital, to get Mr. Brown a check-up. Dr. Evans tells them he has to do a colonoscopy on Mr. Brown, and they find a growth that needs to be removed surgically. Meanwhile, Madea drives her car through a restaurant because they stopped serving breakfast for the day when she wanted a biscuit sandwich.

Shirleys children Byron, Tammy, and Kimberly arrive at the dinner. Byron arrives with his girlfriend Renee and his baby Byron Jr. Tammy arrives with her husband Harold and their two kids. Tammy and Harold fighting about directions. Kimberly arrives with her husband Calvin. Tammy and Kimberly start to argue, and Byrons ex-girlfriend Sabrina arrives. Sabrina is Byrons baby momma, and she likes calling Byron a drug dealer, since he was one when he was young and got arrested because of it, and she loudly extends his name, just to annoy him and his family (she also lies, and uses her sons child support money, and supplies for herself, and is also trying to goad Byron back into selling drugs, so she can get more money for herself). Sabrina also happens to be the manager of Smax, the restaurant that Madea crashed into. Everybody leaves for their own reasons and Shirley doesnt get the chance to tell them about her cancer. During the night, Byron and Renee sneak inside Shirleys house to sleep there, but Byron gets arrested by the police for back child support. Shirley goes to Kimberlys house to ask her to bail him out, but Kimberly refuses. Calvin instead helps Shirley bail him out. 

Aunt Bam tells Madea about the cancer and the family’s situation and Madea promises to gather all Shirley’s children for dinner that evening. She goes to Harold’s garage where Tammy works and tells her to go to the family dinner. Tammy gets called by a client, so Madea disciplines Tammy’s children for their disrespect, putting fear into them. She then goes to Byron’s workplace, but she finds him outside, since he was fired for being late as he spent the night in jail. She proceeds to Kimberly, whom she finds her showing clients a house and she yells at her to attend the dinner. Meanwhile, Mr. Brown loses a lot of blood during his surgery and the doctor asks Cora to donate some. When she does, she finds out that she doesnt have the same blood type as him, implying that he may not be her real father.

At dinner, Tammy and Kimberly have a vicious argument that leads to Tammy revealing that Kimberly is Byron’s birth mother, having had him when she 13 years old. Byron storms out of the house, angry this was kept from him. For the first time, Harold put his foot down towards Tammy and tells her to respect him more. Tammy and Harold then sort their problems out: Harold acted weak because Tammy acted like she was the dominant one in the relationship, and the resultant bickering and power struggle between them led their children to show no respect for their parents or anyone else. After dinner, Kimberly and Calvin continue to fight, which results in Calvin leaving and taking their son with him, much to Kimberlys dismay. When Byron and Renee go to the drug store, they look at the TV where they see Sabrina on "Maury" humiliating Byron and asking for her child support. This pushes Byron over the edge and he decides to go back to dealing drugs. However later on, while Byron is back on the corner, he learns Shirley is in the hospital via a phone call and leaves. 

Overtime, Shirley’s condition worsens greatly and she goes to the hospital; she tells Byron, Tammy, Harold, and Calvin, who arrive to see her, that she loves them all, including Kimberly who isn’t present, and she dies. After Shirleys funeral service, Madea confronts the family, saying that Shirley was a peaceful woman and she didnt want all the fighting going on. 
 raped by an uncle when she was 12, which resulted in her pregnancy with Byron, and was why she became so hostile and aggressive towards everyone, because she didnt forgive the man that raped her. Kimberly sorts her issues with Calvin, feeling that she doesnt know how to begin, and finally hugs him and apologizes while they agree to acquire some professional help. Byron breaks up with Renee, because he just buried his mother while Renee keeps thinking only about herself, and trying to get him to deal drugs again.

The film ends with Madea, Mr. Brown, and Cora on The Maury Povich Show to find out if Mr. Brown is in fact Coras father. Madea says he is, but it turns out hes not. This surprises Madea who runs off-stage, sobbing hysterically (this, however, is non-canon to the rest of the franchise as Mr. Brown is still portrayed as Coras father).

==Cast==
* Tyler Perry as Mabel "Madea (Mabel Simmons)|Madea" Simmons and Joe Simmons (Madeas brother)
* Loretta Devine as Shirley  Shad "Bow Wow" Moss as Byron
* Tamela Mann as Cora
* Cassi Davis as Betty Ann "Aunt Bam" Murphy
* Lauren London as Renee
* Shannon Kane as Kimberly
* Isaiah Mustafa as Calvin
* Natalie Desselle as Tammy
* Rodney Perry as Harold David Mann as Leroy "Mr. Brown" Brown
*Teyana Taylor as Sabrina
* Philip Anthony-Rodriguez as Dr. Evans
* Chandra Currelley as Sister Laura

==Cameo==
* Maury Povich as himself

==Release==
===Box office===
Madeas Big Happy Family was released on April 22, 2011. It opened with $25,068,677 on its opening weekend, ranking #2 at the box office behind Rio (2011 film)|Rio.  By the end of its run, the $25 million  film grossed $53,345,287 in the United States. 

===Critical reception===
The film has received mixed reviews from critics. Based on 40 reviews collected by Rotten Tomatoes, the film has an overall approval rating of 38% with an average rating of 4.6/10.  Another review aggretator, Metacritic, gave the film an average rating of 45 based on 15 reviews, indicating "mixed or average reviews". 

==Home media==
Madeas Big Happy Family was released on  . 

==References==
 

==External links==
*  
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 