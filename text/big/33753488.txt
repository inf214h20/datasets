In Your Eyes (2014 film)
{{Infobox film
| name           = In Your Eyes
| image          = In Your Eyes Tribeca poster.jpg
| alt            =  
| caption        = Movie poster
| director       = Brin Hill
| producer       = Kai Cole   Michael Roiff
| writer         = Joss Whedon Steve Harris   Mark Feuerstein
| music          = Tony Morales
| cinematography = Elisha Christian
| editing        = Steven Pilgrim
| studio         = Bellwether Pictures   Night & Day Pictures
| released       =  
| runtime        = 106 minutes   
| country        = United States
| language       = English
| gross          = 
}} Steve Harris and Mark Feuerstein. It is the second feature by Bellwether Pictures. In Your Eyes, set in New Mexico and New Hampshire, follows Dylan and Rebecca. They live on opposite sides of the country, but are able to sense what the other is feeling - despite being strangers.

The film had its world premiere at the 2014 Tribeca Film Festival on April 20. Immediately afterwards, it was Simultaneous release|self-distributed online instead of taking on theatrical distribution.

==Plot==
The film opens with a young Rebecca Porter (Zoe Kazan) about to go sledding in New Hampshire, while across the country in New Mexico a young Dylan Kershaw (Michael Stahl-David) is at school with a group of his friends. Suddenly, Dylan is able to experience everything that Rebecca experiences, and at the exact moment that Rebecca crashes her sled rendering her unconscious, Dylan is thrown from his desk and knocked out cold.

Twenty years later Rebecca is married to a successful doctor, Phillip Porter (Mark Feuerstein) while Dylan has just recently gotten out of prison. One night Rebecca attends a dinner party with her husband, and Dylan is at a local bar trying to stay out of trouble. However, a man who he had been playing pool with earlier hits him on the back with a pool stick. The impact flings Rebecca to the floor, which she cannot explain to the guest of the dinner party, and leads to her husband chastising her for her behavior.

The next day Dylan and Becky connect once more and they learn that if they speak aloud they can hear one another, they can see what the other is looking at, and feel what the other is feeling. They establish that they are not figments of their imagination, and they are indeed actual people. They set a meeting time to talk later on that night, and they slowly begin to get to know one another through several scenes where they talk, show each other their surroundings, share their dreams and shared experiences, and finally stepping in front of mirrors for a visual introduction. 

Throughout their experiences (Beckys fundraiser, Dylans date, Dylan is fired from his job) they grow closer. Becky realizes that Dylan is in love with her and she herself is falling in love with him so she breaks off their communication much to their despair. 

Later Dylan feels that Becky is in trouble and he violates his parole by taking a plane to New Hampshire to rescue Becky from the mental institution. Unable to rent a car at the airport Dylan steals one while guiding Becky through her escape from the facility. Becky avoids detection until reaching the front door where she runs into her husband and subsequently punches him in the face before fleeing the facility, while Dylan ensues in a police chase. They both end up on foot running through the woods towards the train. They run alongside the train and climb into an empty box car where they finally unite in person.

==Cast==
* Zoe Kazan as Rebecca Porter   
* Michael Stahl-David as Dylan Kershaw   
* Nikki Reed as Donna    Steve Harris as Giddons  
* Mark Feuerstein as Phillip Porter    Steve Howey as Bo Soames   
* David Gallagher as Lyle Soames   
* Michael Yebba as Chief Booth  
* Reed Birney as Dr. Maynard  
* Joe Unger as Wayne  
* Tamara Hickey as Dorothy  
* Jennifer Grey as Diane   

Abigail Spencer was originally set to play the lead in the film, but the role was later given to Kazan.      

==Production== Claremont and digitally with film as a satisfying trait for both him and his Cinematographer|DP, Elisha Christian.    The screenplay was written by Joss Whedon in the early 90s, and went through multiple rewrites over the course of two decades.    To Hill, "the most interesting aspect of the script was its theme of connection–and what it means to connect in today’s society".   

==Music==
Tony Morales composed the score for In Your Eyes.    It was recorded at Emoto Studios in 2013.    On collaborating with Brin Hill and Joss Whedon, Morales said, "I primarily worked with Brin on the music for  . Joss had a vision for the score that we were able to run with. As the sound of the score was coming together, Joss and the other producers were all part of the approval process".    A hybrid of string instruments, piano sounds, and a variety of electronic music were used by Morales to convey a sense of "mystery" that could "weave in and out" of the "romantic energy".    All music was released by Lakeshore Records.    The score and soundtrack were digitally available on June 10, 2014.      

{{tracklist
| headline        = In Your Eyes (Original Motion Picture Score)
| collapsed       = yes
| title1          = In Your Eyes
| length1         = 3:38
| title2          = Connected
| length2         = 1:19
| title3          = You’re Real
| length3         = 1:51
| title4          = It’s Snowing
| length4         = 1:11
| title5          = 10pm Date
| length5         = 1:36
| title6          = Did You Ever Go Sledding?
| length6         = 1:20
| title7          = Mirror
| length7         = 1:42
| title8          = Kinda Personal
| length8         = 1:59
| title9          = Look under the Hood
| length9         = 1:08
| title10         = Rebecca Visits Phil
| length10        = 0:59
| title11         = Phillip and Rebecca
| length11        = 1:02
| title12         = I Should Go
| length12        = 0:54
| title13         = Quirks and Insecurities
| length13        = 2:30
| title14         = Rebecca Is Having an Affair
| length14        = 1:59
| title15         = Break Up
| length15        = 4:22
| title16         = Rebecca Put In Hospital
| length16        = 1:22
| title17         = Time to Go To Work
| length17        = 3:46
| title18         = On His Way
| length18        = 5:33
| title19         = Make a Break for It
| length19        = 2:22
| title20         = Together At Last
| length20        = 2:44
| total_length    = 43:17
}}

{{tracklist
| headline        = In Your Eyes (Original Motion Picture Soundtrack)
| collapsed       = yes
| writing_credits = yes
| extra_column    = Artist(s)
| title1          = Go Get Another Dream
| extra1          = Andrew Johnson
| length1         = 3:48
| title2          = Temptation
| extra2          = Ray Beadle
| length2         = 4:10 Resurrection Fern
| extra3          = Iron & Wine
| length3         = 4:50 The Riot’s Gone
| extra4          = Santigold
| length4         = 3:30
| title5          = Crumblin’
| extra5          = Noah Maffit, Jessica Freedman
| writer5         = Joss Whedon   
| length5         = 3:42
| title6          = Trouble I’m In
| extra6          = Twinbed
| length6         = 3:20
| title7          = Glad I Found You
| extra7          = Eddie Ray
| length7         = 2:41
| title8          = In The Dark
| extra8          = Opus Orange
| length8         = 3:21
| title9          = Fired Up
| extra9          = Matt Andersen
| length9         = 3:46
| title10         = Stand In The Water Wildlife
| length10        = 4:17
| title11         = The Break Up
| extra11         = Tony Morales
| length11        = 4:22
}}

==Release==
In Your Eyes premiered on April 20 at the 2014 Tribeca Film Festival.      In place of theatrical distribution, the film was put up for simultaneous release. The venture was announced after its debut screening in a video message from Joss Whedon, who said that the Tribeca premiere was "not just the premiere of the film. It is the worldwide release date".       Brin Hill described the experimental method as "bittersweet, because its uncharted territory. But there’s something thrilling about this.   There’s something exciting about just making it available everywhere to everyone at once".   

===Critical reaction===
According to Rotten Tomatoes, 63% of critics have given the film positive feedback (based on 16 reviews).   

Kurt Loder of Reason (magazine)|Reason.com complimented the director for making "a lustrous film out of Whedons ingenious story",    while Mark Adams of Screen International|ScreenDaily lauded it by saying, "It has the perfect balance of humour and tenderness, with just a dash of danger and even melodrama on the side".    John DeFore of The Hollywood Reporter praised Brin Hills ability to depict "the couples growing intimacy", but felt that "the forces keeping both from being fulfilled" lacked development.    Eric Kohn of Indiewire wrote that In Your Eyes "successfully offers the lightweight alternative to Whedons bigger projects: Its cheesy and slight, but persistently smart and entertaining within those narrow parameters".   
 The Shining".    Peter Debruge of Variety (magazine)|Variety said that the films "setup never really matures beyond a sentimental teenage fantasy".   

===Home media===
In Your Eyes was released on DVD on February 10, 2015.   

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 