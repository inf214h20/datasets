Skate or Die (film)
 
Skate or Die is a French action film directed by Miguel Courtois released in 2008 in cinema|2008.

== Plot Summary == skaters who witness the killing of three people in a parking lot. Unfortunately for them, the killers notice them and pursue them through the streets of Paris. By taking refuge in a police station, the two young men understand that their pursuers are actually corrupt inspectors.

== Technical details ==
* French Title : Skate or Die
* Director : Miguel Courtois
* Writers : Chris Nahon and Clelhio Favretto
* Production : Sébastien Fechner 
* Distribution : Pathé Distribution
* Format : colour
* Release Date : June 11, 2008

== Cast ==
* Mickey : Mickey Mahut
* Idriss : Idriss Diop 
* Dany : Elsa Pataky  Philippe Bas 
* Sylla : Passi 
* Sylvie : Rachida Brakni  Antonio Ferreira
* Police officer on reception : Nicolas Gabion
* Coronor : Bernard Le Coq
* Police officer : Vincent Desagnat

== External links ==
 
*  

 

 
 
 
 
 


 
 