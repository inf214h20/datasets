Kidulthood
 
 
 
{{Infobox film
| name = Kidulthood
| image = KidulthoodPoster.jpg
| image_size = 215px
| alt =
| caption = Theatrical release poster
| director = Menhaj Huda
| producer = Menhaj Huda Amir Madani George Isaac Damian Jones
| writer = Noel Clarke
| starring = Aml Ameen Red Madrell Adam Deacon Jaime Winstone Femi Oyeniran Madeleine Fairley Cornell John Kate Magowan Pierre Mascolo Rafe Spall Noel Clarke The Angel
| cinematography = Brian Tufano
| editing = Victoria Boydell
| distributor = Revolver Entertainment
| released =  
| runtime = 91 minutes
| country = United Kingdom
| language = English
| budget = £600,000
| gross = £453,876
}}
Kidulthood (rendered as KiDULTHOOD) is a 2006 British drama film about the life of several teenagers in Ladbroke Grove and Latimer Road area of inner west London. It was directed by Menhaj Huda and written by Noel Clarke, who also stars in the film and directed the sequel, Adulthood (film)|Adulthood.  The majority of the characters in the film generally behave in a violent and lawless manner. They are portrayed as being reckless and antisocial young people who commit crimes such as petty theft and serious violence. The film also showcases how the characters engage in recreational drug taking and some sexual behaviour.

==Plot==
Set in 2002, the film follows two days in the lives of a group of 15-year olds from a mixed-income area of West London. The story focuses mainly upon the antihero Trevor, known as "Trife" (Aml Ameen), and Alisa (Red Madrell), his sometime partner. One of the themes of the movie is Alisas teenage pregnancy|pregnancy. She states that Trife is the father. Trife is most displeased with these developments and he makes it known through absconding from typical fatherly duties to his young flame.
 Game Boy he had stolen earlier, they then proceed to steal Sams cannabis and Jay has sex with Sams girlfriend, Claire (Madeleine Fairley). When Sam returns and threatens them, the boys hit him with a keyboard, overpower him, attack him, and escape along with Claire. In the process, they knock Sams mother to the floor, which is later used as a pretext for revenge by Sam.

Alisa and Becky arrive at a shopping centre, having sold their drugs to buy new dresses. They meet up with Moony and Jay; Jay tells Alisa that Trevor doesnt want her or the baby, and that she should get out of Trevors life, this could well be an ironic gesture to Trifes absconding from his fatherly duties unto her. Alisa decides to return home alone as Becky wants to stay with the boys. In the meantime, Trevor has gone to meet up with his Uncle Curtis (Cornell John). He sees Katies brother, Lenny (Rafe Spall) at Curtiss house, but they do not speak. Trevor tells Curtis that he wants to work for him, and is then issued with a revolver, which Trevor had previously made by drilling the barrel of a starting pistol on a pillar drill at school. Trevor is then taken downstairs to a tied up man, Andreas, who is being tortured for failing to stick to an agreement about payment. Andreas is earlier seen purchasing drugs from Curtis. Curtis orders Trevor to cut the mans face with a Stanley knife. Trevor carries out the order, but then flees from the house, throws the gun into the river and goes to find Alisa. Alisa is on her way home when she sees a classmate, who persuades her to come to a party with him to cheer her up.

Trevor arrives at the party, finds Alisa, and confesses he loves her. Alisa tells him that she never slept with Sam, and that the baby definitely belongs to Trevor. Sam later arrives at the party and beats Trevor up with a baseball bat. Alisa runs into the house to get Mooney and Jay. In the ensuing fight Trife attacks Sam when he tries to hurt Alisa and beats him to the ground, but Alisa tells him to stop. As he is leaving Sam picks up the bat and hits Trife, who falls to the ground, critically injured. Katies older brother arrives, carrying a gun and asking for Sam. He is about to execute Sam but stops momentarily and asks for a reason why he shouldnt kill him. Trife uses his last breaths to shout "because he isnt worth it". Lenny begins to walk away but Sam insults him so he turns and fires, but the gun explodes in his hand. He gets back into his car and drives away. Trevor dies before the ambulance and police arrive.

==Cast==
* Aml Ameen as Trevor
* Red Madrell as Alisa
* Adam Deacon as Jay
* Noel Clarke as Sam
* Jaime Winstone as Becky
* Femi Oyeniran as Moony
* Madeleine Fairley as Claire
* Cornell John as Curtis
* Rafe Spall as Lenny
* Nicholas Hoult as Blake
* Rebecca Martin as Katie
* James Witherspoon as Kilpo
* Ortis Deley as Derek
* Stephanie Di Rubbo as Shaneek

==Production== Ladbroke Grove Royal Oak stations. 

==Sequel==
The sequel, Adulthood (film)|Adulthood, was released in June 2008, which was written and also directed by Noel Clarke.

==Critical reaction==
Kidulthood has received a generally positive critical response. Writing in  , however, said "the only people who should be shocked by this film are people who have never been teenagers. What Kidulthood does is take all the violence, sex and intoxication experienced in a teenage year and condense it into a single day, because thats far more marketable than a film about eight kids spending four hours sitting on the swings wondering what to do".  The Daily Mirror described it as being "as potent as a shot of vodka before breakfast &ndash; a harrowing, uncompromisingly bleak but thoughtful look at the anguish of being young and poor in Britain". 

==See also==
 
 
*Adulthood (film)|Adulthood
*Anuvahood
 
*West 10 LDN
*4.3.2.1
 

==References==
 

==External links==
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 