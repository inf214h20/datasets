Chato's Land
{{Infobox film
| name           = Chatos Land
| image          = Chatos land Poster.jpg
| image_size     = 
| alt            = 
| caption        = Theatrical release poster
| director       = Michael Winner
| producer       = Michael Winner Gerry Wilson
| narrator       = 
| starring       = Charles Bronson Jack Palance
| music          = Jerry Fielding
| cinematography = Robert Paynter
| editing        = Michael Winner
| studio         = Scimitar Films
| distributor    = United Artists
| released       =  
| runtime        = 110 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} western Technicolor Gerry Wilson. 

==Plot==
The film opens as the half-Apache Chato orders a drink at a bar. The bartender ignores him and serves the local sheriff who has arrived after Chato. The sheriff calls Chato a "Redskin_(slang)|redskin" and tells him the bar is for whites only. He moves behind Chato while hurling a stream of abuse at him. The sheriffs taunts escalate, and he draws his gun while saying that he is going to kill Chato. Chato whirls around and shoots the sheriff in the gut, killing him. He rides out of town on his Appaloosa. 
 Confederate officer, posse to hunt down Chato. As Whitmore rides across the country, the posse grows in number at each stop. It includes local ranchers and townspeople, along with a Mexican who is used as a scout and tracker.

Chato calmly watches the posses progress, staying one step ahead of them. From a hilltop, he fires on them, drawing them into an ill-advised ascent. As the posse struggles to climb the hill, Chato descends the other side and scatters their horses. He seems generally nonplussed by their presence. At one point, he kills a rattlesnake, chops off its rattle, and wraps the rattle in the snakes skin. He puts the bundle in his coat pocket without explanation. 

The posses motivations are diverse. Some are motivated by a basic sense of justice, while a disturbing contingent seems to merely want to kill someone. As they continue to be outwitted by Chato, their divisions become more pronounced. When they come across a set of empty wickiup|wickiups, the overtly racist members of the posse gleefully burn them. 

In a valley, Chato spies a woman filling a water jug. As they smile at each other, it becomes clear that she is his wife. He greets his son and gives him the rattlesnake toy from his pocket. They enter Chatos hogan, happy to be reunited. Chato resumes his normal life, busying himself with breaking horses during the day. 

The posse eventually discovers his home and brutally gang rape Chatos wife. They hogtie her naked outside the hogan. Chatos friend creates a diversion which allows Chato to rescue his wife. During the confusion, Chatos friend is shot and wounded. The sadistic members of the posse hang him upside down and set him on fire while he is still alive. Disgusted by such barbarity, Whitmore shoots the burning man through the head, putting him out of his misery. 

As he prepares to avenge his dead friend and his violated wife, Chato abandons his European dress and dons his native moccasins and loin cloth. He lures the posse members into individual traps, killing them one at a time. The posse grows more fractious until the more sadistic members murder Whitmore and the peaceful holdouts. Chato finishes off the rest.

==Cast==
 
 
* Charles Bronson as Pardon Chato
* Jack Palance as Capt. Quincey Whitmore
* James Whitmore as Joshua Everette
* Simon Oakland as Jubal Hooker
* Richard Basehart as Nye Buell
* Ralph Waite as Elias Hooker
* Richard Jordan as Earl Hooker
* Victor French as Martin Hall
* Sonia Rangan as Chatos wife
  William Watson as Harvey Lansing
* Roddy McMillan as Gavin Malechie Paul Young as Brady Logan
* Lee Patterson as George Dunn
* Peter Dyneley as Ezra Meade Hugh McDermott as Bartender
* Raúl Castro as Mexican scout (as Raul Castro)
* Verna Harvey as Shelby Hooker
 

==Reception==
===Critical response===
When released Vincent Canby panned the film calling it a "...long, idiotic revenge Western...It was directed by Michael Winner in some lovely landscapes near Almeria, Spain. Just about everybody gets shot or knifed, and one man dies after Chato lassos him with a live rattlesnake." 

TV Guide, echoing Canby, wrote, "A great cast is primarily wasted in this gory, below-average, and overlong film. The script could have been written for a silent film to fit with Bronsons traditional man-of-few-words image (in fact, more grunts and squint than words)...As usual Bronson must rely upon the conviction that there are viewers who find silence eloquent." 

A more recent Film4 review was more positive observing that Chatos Land "...though no masterpiece, is an effective and frequently disturbing piece of filmmaking. A tough, cynical western with well-paced direction and a fine performance from Charles Bronson and the cast of vagabonds out to get him. A quality film from Michael Winner." 

===1970s political overtones===
Film critic Graeme Clark discussed an often discussed contemporary political theme of the film when it was released in the early 1970s, writing, "There are those who view this film as an allegory of the United States presence in Vietnam, which was contemporary to this storyline, but perhaps that is giving the filmmakers too much credit. Granted, there is the theme of the white men intruding on a land where they are frequently under fire, and ending up humiliated as a result, but when this was made it was not entirely clear that America would be on the losing side as the conflict may have been winding down, but was by no means over." 

Film4, is more assertive in their review, "The cruelty of the posse is well conveyed by an able (and supremely ugly) group of actors headed up by Jack Palance and Simon Oakland. Some of their acts, such as the brutal rape of Chatos squaw and the burning of an Indian village, have an unpleasant edge which Winner does not shy away from. Parallels with the contemporary situation in Vietnam cant have been lost on the original audience. 

==Media Releases== Region One DVD in 2001  and on Region Two in 2004. 

==Soundtrack==
A CD of the films soundtrack was released on January 15, 2008 by Intrada Records (Intrada Special Collection Vol. 58). 

===Track listing===
 
 
* 1. Titles - 4:41
* 2. Peeping Tom In The Bushes - 0:44
* 3. Mind Your Ma; Whiskey And Hot Sun - 1:29
* 4. Coop Falls - 1:24
* 5. Pain In The Water Bags; Burning Rancheros - 1 & 2 4:47
* 6. Peeping Tom On The Ridge; First Stampede - 3:04
* 7. Indian Convention - 1:35
* 8. The Snake Bite - 1:21
* 9. Chato Comes Home - 1:52
* 10. Indian Rodeo; Chato Bags Horse - 2:21
* 11. Junior Blows The Whistle - 0:42
 
* 12. Fire And Stampede; Joan Of Arc At Stake - 3:54
* 13. Mr. & Mrs. Chato Split; Massas In The Cold Cold Ground - 1:26
* 14. Hot Pants - 2:46
* 15. Rainbow On The Range - 0:58
* 16. Ride Like Hell - 0:50
* 17. Big Stare Job; Here-There-Everywhere - 2:19
* 18. Attack In Gorge - 1:53
* 19. One Big Pain In The Neck - 2:35
* 20. Lansing Scalped - 1:46
* 21. Elias Gets The Snake; Malechie Gets Shot; Finis - 5:06
 

==References==
 

==External links==
*  
*  
*  
*   film trailer at Rotten Tomatoes
*   at The Spaghetti Western Database

 

 
 
 
 
 
 
 
 
 
 