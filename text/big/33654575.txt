Zero (2010 film)
 

{{Infobox film
| name           = Zero
| image          = 
| alt            =  
| caption        = 
| director       = Christopher Kezelos
| producer       = Christine Kezelos
| writer         = Christopher Kezelos
| based on       =  
| narrator       = Nicholas McKay
| starring       = 
| music          = Kyls Burtland
| cinematography = 
| editing        = 
| studio         = Zealous Creative
| distributor    = 
| released       =   
| runtime        = 12:32
| country        = Australia
| language       = English
| budget         = 
| gross          = 
}}

Zero is a 2010 stop motion animation written and directed by Christopher Kezelos and produced by Christine Kezelos.

==Awards, nominations and screenings==

===Awards===
* 2010 LA Shorts Fest: Best Animation   
* 2010 Rhode Island International Film Festival: (tied with) First Place - Best Animation    
* 2010 Grand OFF - World Independent Film Awards: Best Animation    
* 2010 ATOM Awards: Best Animation
* 2010 Naples International Film Festival: Best Short Film
* 2010 Flickerfest International Film Festival: Best Achievement in Sound
* 2010 Australian Cinematographers Society: Golden Tripod - Experimental & Specialised (National)
* 2009 Australian Cinematographers Society: Gold Award - Experimental & Specialised (NSW/ACT)
* 2010 Bondi Short Film Festival: Best Script
* 2010 Bondi Short Film Festival: Best Design
* 2011 Shorts Film Festival: Bronze Award
* 2011 Lady Filmmakers Film Festival: Ladies Kick Butt
* 2011 Sandfly Film Festival: Best Animation
* 2011 Sandfly Film Festival: Best Australian Short
* 2011 Sandfly Film Festival: Best at Sandfly

===Nominations=== AFI Awards: Nominated for Best Short Animation
* 2010 Inside Film Awards: Nominated for Best Short Animation
* 2010 APRA Screen Music Awards: Nominated for Best Music in a Short Film
* 2010 Australian Screen Sound Awards: Nominated for Best Sound in an Animated Short Film or Program

===Screenings===
* 2010 Palm Springs International ShortFest & Film Market: Official Selection
* 2010 Savannah Film Festival: Official Selection
* 2010 Hiroshima International Animation Festival: Official Selection
* 2010 Seoul International Cartoon Animation Festival: Official Selection (SICAF Choice)
* 2010 Anima Mundi International Animation Festival of Brazil: Official Selection (Panorama) - 2010 Bradford Animation Festival: Official Selection
* 2010 St Kilda Film Festival: Official Selection
* 2010 Lucania Film Festival: Official Selection
* 2010 Australian Effects and Animation Festival: Official Selection
* 2010 Anim’est International Animation Film Festival: Official Selection
* 2010 Bumbershoot - Seattle Music & Arts Festival: Official Selection
* 2010 North Country Film Festival: Official Selection
* 2010 ANIMANIMA - International Festival of Animation: Official Selection (Panorama)
* 2010 Anaheim International Film Festival: Official Selection
* 2010 Foyle Film Festival: Official Selection
* 2010 Montreal Stop Motion Film Festival: Official Selection
* 2010 Science Fiction + Fantasy Short Film Festival: Official Selection
* 2010 Animated Dreams Film Festival: Official Selection
* 2010 Etudia&Anima Festival: Official Selection
* 2010 Dawn Breakers International Film Festival: Official Selection
* 2010 Open Media Festival: Official Selection
* 2010 Adelaide Film Festival: Official Selection

==References==
 

==External links==
*  
* 

 
 
 
 