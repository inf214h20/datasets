Descent (2007 film)
 
{{Infobox film
| name           = Descent
| image          = Descent poster.jpg
| alt            = 
| caption        = Release poster
| director       = Talia Lugacy
| producer       = Rosario Dawson Morris S. Levy Talia Lugacy
| writer         = Talia Lugacy Brian Priest
| starring       = Rosario Dawson Chad Faust Marcus Patrick
| music          = Alex Moulton 
| cinematography = Christopher LaVasseur
| editing        = Frank Reynolds 
| distributor    = City Lights Pictures
| released       =  
| runtime        = 105 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = $15,233 
}} thriller film directed by Talia Lugacy and produced by and starring Rosario Dawson.

==Plot==
Maya is an up-and-coming artist and college student. In the winter of her senior year, Maya attends a fraternity party and talks to Jared. She accepts his invitation to dinner at a nice restaurant, then goes to his apartment, just to talk. Jared soon reveals his true self when he suddenly assaults her. Jared brutally rapes Maya while uttering racist slurs in her ear. When he is finished, the sociopath Jared then threatens Maya that if she ever tells anyone what really went on in his apartment, he will naturally deny everything by putting back on his nice-guy facade and turn it all back at her.
 depression and Substance dependence|addiction. At night, shes someone else: a beauty at the nightclub scene, dancing, seductive, sniffing cocaine, becoming the friend of a heavy hitter. She thinks about her racial identity. Maya later meets and seeks out the help of a DJ she meets at a club, named Adrian, whom she confides in what happened to her and he agrees to help her get revenge on her attacker.

One evening, Maya meets Jared once again and lures him to her apartment with the intent on having sex with him again. Jared willingly complies. She turns the tables on him by tying him to her bed and blindfolding him. She allows Adrian to rape Jared several times. Echoing what Jared said to Maya, Adrian threatens Jared with public humiliation should he ever report this incident to anyone. In the final shot, as Maya watches Adrian brutally sodomize Jared, she seems slightly regretful and recalls her saying shed enjoy her revenge, and then sheds a tear, seeming to indicate she is still not over her psychological issues.

==Cast==
* Rosario Dawson as Maya
* Chad Faust as Jared
* Marcus Patrick as Adrian
* James A. Stephens as Professor Byron
* Vanessa Ferlito as Bodega girl
* Tracie Thoms as Denise
* Alexie Gilmore as Seline
* Jonathan Tchaikovsky as Tyler
* Nicole Vicius as Melanie
* Scott Bailey as Upstairs guy

==Release==
Descent was released in two alternate cuts: a 105-minute uncut NC-17 rated version and a 95-minute R-rated version. The notable difference between the two is that the edited release omits about seven minutes of the second rape scene.

==Reception==
Descent received mixed to negative reviews, currently holding a 36% rating on review aggregator website  , the film has a 45/100 rating based on 10 critics, indicating "mixed or average reviews". 

==References==
 

==External links==
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 