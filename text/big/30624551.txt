Faust (2011 film)
{{Infobox film
| name           = Faust
| caption        = 
| image          = Faust FilmPoster.jpeg
| director       = Alexander Sokurov
| producer       = Andrey Sigle
| writer         = Alexander Sokurov Marina Koreneva Yuri Arabov
| based on       =    
| starring       = Johannes Zeiler Anton Adasinsky Isolda Dychauk Hanna Schygulla
| music          = Andrey Sigle
| cinematography = Bruno Delbonnel
| casting director= Kristin Diehle
| editing        = Jörg Hauschild
| studio         = Proline Film
| distributor    = 
| released       =  
| runtime        = 134 minutes
| country        = Russia
| language       = German
| budget         = € 8 million
| gross          = $2,121,258    
}}

Faust is a 2011 Russian film directed by Alexander Sokurov. Set in the 19th century, it is a free interpretation of the Faust legend and its literary adaptations by Johann Wolfgang von Goethe and Thomas Mann. The dialogue is in German. The film won the Golden Lion at the 68th Venice International Film Festival.

==Plot==
Heinrich Faust (Johannes Zeiler) is driven by his longing for enlightenment. He seeks to understand the very nature of life and how it makes the world go round. Driven by his burning desire for cognition, he even unearths corpses and rummages in their guts just to localize the home of the soul.

While he keeps on telling himself "in the beginning was the word", he gets to know the racketeer Mauricius, Anton Adassinsky playing a wordly version of  . Fausts obscure new friend takes him to the twilight zones of their small town.

In a bath, his attention is caught by the young Margarete (Isolda Dychauk), also known as "Gretchen". Later the two new friends are entangled in a pub brawl, Faust accidentally kills Gretchens brother. Faust becomes obsessed with Gretchen, who appears to embody the beauty of blooming life. He indulges himself in thinking that studying her would be reasonable as a part of his research about what makes all the difference between life and death. When the aging Faust has become irreversibly infatuated with Gretchen, Mephistopheles offers him to let him have her.

Faust cannot resist the idea of spending a night with Gretchen. Yet Mauricius demands nothing less than Fausts soul in return. Faust even has to sign the contract with his own blood. Now living on borrowed time, Faust can hit on Gretchen, but he is haunted by penitence and fear.  Finally Faust cannot bear Mauricius nihilistic comments anymore. Overwhelmed with wrath, he buries Mauricius under rocks and finds himself lost in the middle of nowhere.

==Cast==
* Johannes Zeiler as Faust
* Anton Adasinsky as Moneylender (Mephistopheles)
* Isolda Dychauk as Gretchen
* Georg Friedrich as Wagner
* Hanna Schygulla as the Moneylenderswife
* Antje Lewald as Gretchens mother
* Florian Brueckner as Valentin 
* Sigurdur Skulasson as Fausts father 
* Maxim Mehmet as Valentins friend
* Eva-Maria Kurz as Fausts cook

==Themes==
The film is the final part in a series of films where Alexander Sokurov explores the corrupting effects of power. The previous installments are three biographical dramas: about   is very important. He saw it as a film that can introduce the Russian mentality into European culture; to promote integration between Russian and European culture." 

==Production==
The project, described in 2005 as "loosely based on works by Goethe and Thomas Mann", was announced by Sokurov in 2005 as "a very colourful, elegant picture with a lot of Strauss music and a smell of chocolate."  The eight-million euro film was produced by the St. Petersburg-based company Proline Film and received support from the Mass Media Support Fund of Russia. 

Filming started 17 August 2009 in the Czech Republic, where it continued for two months. Locations included the castles of Točník, Lipnice nad Sázavou and Ledeč nad Sázavou|Ledeč, as well as the town Kutná Hora. Studio scenes were shot at Barrandov Studios in Prague.  Photography also took place in Germany. In October the team moved to Iceland for several days of filming resulting in some astonishing shots of geysers. 

==Release==
The film premiered on 8 September 2011 in competition at the 68th Venice International Film Festival.    Three days later it was screened in the Masters section of the 2011 Toronto International Film Festival. 

===Reception===
Jay Weissberg wrote in  ,   and Herri met de Bles." 
 Goethe play, “Faust” is mesmerizing, at times predictably if divertingly bewildering and beautiful, with images that burn into your memory, like that of an embracing couple falling into a lake in a vision of desire and the abyss that invokes “L’Atalante” but is definitely Sokurovian." 

At the closing ceremony of the Venice Film Festival, Faust was honoured with the festivals highest prize for best film, the Golden Lion. The jury president was the American filmmaker Darren Aronofsky, who said when he presented the winner: "There are some films that make you cry, there are some films that make you laugh, there are some films that change you forever after you see them; and this is one of them." 

==References==
 

==External links==
*  
*  
*  
*   Filming locations with real photos at Movieloci.com

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 