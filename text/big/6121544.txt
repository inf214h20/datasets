Angel on My Shoulder (film)
{{Infobox film
| name           = Angel on My Shoulder
| image          = Angel on My Shoulder film poster.jpg
| image size     =
| caption        = Original theatrical poster
| director       = Archie Mayo
| producer       = Charles R. Rogers
| story         = Harry Segall 
| screenplay = Roland Kibbee
| narrator       =
| starring       = Paul Muni Anne Baxter Claude Rains
| music          = Dimitri Tiomkin
| cinematography = James Van Trees
| editing        = Asa Boyd Clark
| studio = Premier Productions
| distributor    = United Artists
| released       =  
| runtime        = 100 minutes
| country        = United States
| language       = English
| budget         =
| preceded by    =
| followed by    =
}}
Angel on My Shoulder is a 1946 American fantasy film about a deal between the Devil and a dead man.

The film was an independent production, produced by Charles R. Rogers and David W. Siegel, directed by Archie Mayo, written by Harry Segall (who also wrote the screenplay for Here Comes Mr. Jordan and the play Heaven Can Wait) and Roland Kibbee, and released by United Artists. The film was Mayos last before his retirement.

It starred Paul Muni, Anne Baxter and Claude Rains. The producer changed the original title, Me and Satan, when he concluded that the public would not see a film about the Devil.   

==Plot==
After his release from prison, gangster Eddie Kagle (Paul Muni) is killed by his partner in crime, Smiley Williams (Hardie Albright). Kagle ends up in Hell, where "Nick" (Claude Rains) offers him a chance to escape hell and avenge his own death in exchange for help with a problem. Kagle looks exactly like Judge Frederick Parker, an upright man who is causing Nick distress because he is entirely too honest.  Nick fears that Parker may cause him more anxiety in future, as he is running for governor of his state. Nick wants to destroy Parkers reputation and Kagle readily agrees to have his soul transferred into Parkers body.

As soon as Kagle appears as Parker, odd things begin to happen.  Kagle pursues his goal with evil intent (though often at cross purposes with the Devil), but everything he does to ruin the judges reputation somehow results in making Parker look better. Along the way, Kagle falls in love with Barbara Foster (Anne Baxter), the judges fiancée, causing him to question his whole outlook on life and eventually rebel against Nick.

Nick presents Kagle the opportunity to shoot Williams, but instead Kagle confronts the man with the truth. Shocked and frightened, Williams backs away and falls out an open window to his death. 

Exasperated and defeated, Nick takes Kagle back to Hell, leaving Judge Parker in a much better position than before. Nick threatens to make the reformed mans punishment even more painful than usual, but Kagle blackmails his would-be tormentor; in return for not revealing Nicks blunders, Kagle wants to be made a Trustee#Other uses|trustee.  Nick has no option but to agree to Kagles demands.

==Cast==
*Paul Muni as Eddie Kagle/Judge Fredrick Parker
*Anne Baxter as Barbara Foster
*Claude Rains as Nick
*Onslow Stevens as Dr. Matt Higgins
*George Cleveland as Albert
*Erskine Sanford as Minister
*Marion Martin as Mrs. Bentley
*Hardie Albright as Smiley Williams
*James Flavin as Bellamy

==Copyright status and remakes==
The film is now in the public domain in the United States, because the producers, Rogers and Siegel, neglected to renew the copyright in 1973.
 remade for John Berry, ABC on 11 May 1980.   

An adaptation of the film was written and produced by Todd Baker around 2004.   

==References==
 

==External links==
 
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 