Ustaad
{{Infobox film
| name = Ustaad
| image = Ustaad.jpg
| image_size =
| alt =
| caption =
| director = Sibi Malayil Ranjith
| Ranjith
| narrator = Saikumar
| Vidyasagar  (songs)  Rajamani  (film score) 
| cinematography = Anandakuttan
| editing = L. Bhoominathan
| studio = Country Talkies
| distributor = Swargachitra & PJ Entertainments UK
| released =  
| runtime = 130 minutes
| country = India
| language = Malayalam
| budget         = 
| preceded_by =
| followed_by =
}}
 Ranjith and dual life Ranjith under the banner of Country Talkies. The film was well received at the box office and became a huge hit.

==Plot==
Usthad (Mohanlal) was a don in Mumbai.Now he decides to stop everything and settle down in his home town with his sister Padmaja (Divya Unni).Before he goes back he sends his people Sami(Janardhanan (actor)|Janardhanan) and Sethu (Ganesh Kumar) to Yousuf shah associates where he wants to withdraw his share.But Yousuf shah was not ready to give him a single pie.When Sulaiman(Mafia Sasi), the manager of yousuf,make problem,Ustad come directly and beats him and takes the money.

Back in Kozhikode, his hometown, Ustaad is merely Parameshwaran, an adorable brother, and guardian to his younger sister Padmaja (Divya Unni). He maintains a low profile as a businessman who has ventured into real estate.

Padmaja has fallen in love with her friend and dance teacher Nandan (Vineeth), who is the son of Koliyodan Shekharan (Narendra Prasad), a businessman who has not been doing so well, mainly because of the extravagant lifestyle of his older son Koliyodan Giri (Saikumar (Malayalam actor)|Saikumar). Mohan Thampy (N. F. Varghese), Shekharans son-in-law, is a business tycoon and a politician as well. Thampy originally was helped financially by Shekharan, but over time, he got more involved with the underworld, and they have a strained relationship.

Varsha (Vani Viswanath), a newly appointed police commissioner, does not like Parameshwaran. Meanwhile, Padmaja wants to marry Nandan, to which Parameshwaran gives his approval. The night before the wedding, Parameshwaran is arrested by Varsha for gold smuggling. He learns that his arrest was staged by Yousuf Shah and Thampy. Varsha later realizes her mistake, and vows to support Parameshwaran.

Thampy devises a plan against Ustaad.He force Padmaja to vow her properties to help her father-in-law who has a large debt to Thampy.As a mediator,Thampy gives some money to Giri and advise him to keep it on Nandans office room and will inform the police and make Nandan and Padmaja arrest for money laundering. This enrages Ustaad.He finds Thampy who is in the protection of a minister.He make him speak all the truth in front of varsha.Later Yousuf threat Ustaad by telling him that he will kill his sister by bomb and breaks his right hand.Ustaad hunts down Yousuf Shah, who flees to Dubai, and finally defeats him in the desert. He flies back to join his sister and family.

==Cast==
* Mohanlal as Ustaad / Parameshwaran
* Divya Unni as Padmaja
* Vineeth as Nandan Saikumar as Giri Indraja as Kshama Janardhanan as Swamy Innocent as Kunji Palu
* N. F. Varghese as Mohan Thampy
* Vani Viswanath as Police Commissioner
* Narendra Prasad as Koliyod Shekharan, who is Nandans father
* K. B. Ganesh Kumar as Parameswarans friend Augustine as Ali Abu
* Chithra as Ambika
* Manianpilla Raju as Advocate Kunchan as Appootty Siddique as Irani Rajeev as Yousuf Shah
* Jomol as sister of Yousaf Shah
* Sudheesh as Husband of Yousaf Shahs sister (Jomol)
* Cochin Haneefa as Union Minister

==Soundtrack== Vidyasagar and Thej Mervin (Thirchayilla). The lyrics were by Gireesh Puthenchery and Kannan Pareekutty (Theerchayilla).

{|class="wikitable" width="60%"
! Track !! Song Title !! Singer(s) !! Other notes
|-
| 1 || Vennila Kombile || K. J. Yesudas|Dr. K. J. Yesudas ||
|- Chorus ||
|-
| 3 || Theerchayilla || Mohanlal || Lyrics: Kannan Pareekutty Music: Thej Mervin
|-
| 4 || Chandramukhi || M. G. Sreekumar, Srinivas (singer)|Srinivas, Sujatha Mohan|Sujatha, Radhika Thilak ||
|-
| 5 || Vennila Kombile || Srinivas ||
|-
| 6 || Naadodi Poonthinkal || M. G. Sreekumar,  
|-
| 7 || Chandramukhi ||  Instrumental ||
|}

==References==
 

==External links==
*  

 
 
 
 