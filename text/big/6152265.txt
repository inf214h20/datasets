Thunderball (film)
 
 
{{Infobox film  
| name           = Thunderball
| image          = Thunderball - UK cinema poster.jpg
| caption        = British cinema poster for Thunderball, artwork by Robert McGinnis Terence Young
| producer       = Kevin McClory
| story          = Kevin McClory Jack Whittingham Ian Fleming  
| based on       =   John Hopkins
| starring       = Sean Connery Claudine Auger Adolfo Celi Luciana Paluzzi Rik Van Nutter
| cinematography = Ted Moore John Barry
| editing        = Peter R. Hunt   Ernest Hosler
| studio         = Eon Productions
| distributor    = United Artists
| released       =  
| runtime        = 130 minutes
| country        = United Kingdom
| language       = English
| budget         = $9 million
| gross          = $141.2 million
}}
 James Bond MI6 agent James Bond. novel of was based Terence Young John Hopkins.
 CIA agent Domino Derval, units and about a quarter of the film consisting of underwater scenes.  Thunderball was the first Bond film shot in widescreen Panavision and the first to have over a two-hour running time.

Thunderball was associated with a legal dispute in 1961 when former Ian Fleming collaborators Kevin McClory and Jack Whittingham sued him shortly after the 1961 publication of the novel, claiming he based it upon the screenplay the trio had earlier written in a failed cinematic translation of James Bond. The lawsuit was settled out of court and Bond film series producers Albert R. Broccoli and Harry Saltzman, fearing a rival McClory film, allowed him to retain certain screen rights to the novels story, plot and characters,    and for McClory to receive sole producer credit on this film.

The film was a success, earning a total of $141.2 million worldwide, exceeding the earnings of the three previous Bond films. In 1966, John Stears won the Academy Award for Best Visual Effects  and production designer Ken Adam was also nominated for a BAFTA award.    Thunderball was the most financially successful film of the series after adjusting for inflation. Some critics and viewers showered praise on the film and branded it a welcome addition to the series, while others complained of the repetitively monotonous aquatic action and prolonged length. In 1983, Warner Bros. released a second film adaptation of the novel under the title Never Say Never Again, with McClory as executive producer.

==Plot==
James Bond—MI6 agent 007 and sometimes simply "007"—attends the funeral of Colonel Jacques Bouvar, a   and his Aston Martin DB5.
 M to spinal traction machine, but is foiled by Fearing, whom Bond then seduces. Bond finds a dead bandaged man, François Derval. Derval was a French NATO pilot deployed to fly aboard an Avro Vulcan loaded with two atomic bombs for a training mission. He had been murdered by Angelo, a SPECTRE henchman surgically altered to match his appearance.

Angelo takes Dervals place on the flight, sabotaging the plane and sinking it near the Bahamas. He is then killed by Emilio Largo (SPECTRE No. 2) for trying to extort more money than offered to him. Largo and his henchmen retrieve the stolen atomic bombs from the seabed. All double-0 agents are called to Whitehall and en route, Lippe chases Bond. Lippe is killed by SPECTRE agent Fiona Volpe for failing to foresee Angelos greed. SPECTRE demands £100 million in white flawless uncut diamonds from NATO in exchange for returning the bombs. If their demands are not met, SPECTRE will destroy a major city in the United States or the United Kingdom. At the meeting, Bond recognises Derval from a photograph. Since Dervals sister, Domino Vitali|Domino, is in Nassau, Bahamas|Nassau, Bond asks M to send him there, where he discovers Domino is Largos mistress.

Bond takes a boat to where Domino is snorkelling. After Bond saves her life, the two have lunch together. Later, Bond goes to a party, where he sees Largo and Domino gambling. Bond enters the game against Largo and wins, and subsequently takes Domino to a dance. Recognising each other as adversaries, Bond and Largo begin a tense cat-and-mouse game of attempting to get the drop on each other while still pretending ignorance of their adversarys true nature.

Bond meets Felix Leiter and Q (James Bond)|Q, and is issued a collection of gadgets, including an underwater infrared camera, a distress beacon, underwater breathing apparatus, a flare gun and a Geiger counter. Bond attempts to swim underwater beneath Largos boat, but is nearly killed. Bonds assistant Paula is later abducted by Largo for questioning and kills herself before Bond can rescue her.

Bond is kidnapped by Fiona, but escapes. He is chased through a Junkanoo celebration and enters the Kiss Kiss club. Fiona finds and attempts to kill him, but is shot by her own bodyguard. Bond and Felix search for the Vulcan, finding it underwater. Bond meets Domino scuba-diving and tells her that Largo killed her brother, asking for help finding the bombs. She tells him where to go to replace a henchman on Largos mission to retrieve them from an underwater bunker. Bond gives her his Geiger counter, asking her to look for them on Largos ship. She is discovered and captured. Disguised as Largos henchman, Bond uncovers Largos plan to destroy Miami Beach.
 Disco Volante, sky hook-equipped U.S. Navy aeroplane rescues them.

==Cast== James Bond (007): An MI6 agent assigned to retrieve two stolen nuclear weapons.
*  s Number Two, he creates a scheme to steal two atomic bombs.
* Claudine Auger as Dominique "Domino" Derval (voice dubbed by Nikki van der Zyl):  Largos mistress. In early drafts of the screenplay Dominos name was Dominetta Palazzi. When Claudine Auger was cast as Domino the name was changed to Derval to reflect her nationality.    The characters wardrobe reflects her name, as she is usually dressed in black and/or white.
* Luciana Paluzzi as Fiona Volpe: SPECTRE agent, who becomes François Dervals mistress and kills him before being sent to Nassau.
* Rik Van Nutter as Felix Leiter: CIA agent who helps Bond.
* Bernard Lee as M (James Bond)|M: The head of MI6.
* Guy Doleman as Count Lippe: SPECTRE agent who tries to kill Bond in the health clinic.
* Martine Beswick as Paula Caplan: Bonds ally in Nassau who is kidnapped by Vargas and Janni.
* Molly Peters as Patricia Fearing: a physiotherapist at the health clinic.  Earl Cameron as Pinder, Bond and Felix Leiters assistant in The Bahamas.
* Paul Stassino as François Derval and Angelo Palazzi: Derval is a NATO pilot, who is also Dominos brother. He is killed by SPECTRE agent Angelo Palazzi, who impersonates him. Palazzi is later killed by Largo.
* Desmond Llewelyn as Q (James Bond)|Q: MI6s "quartermaster" who supplies Bond with multi-purpose vehicles and gadgets useful for the latters missions. Foreign Secretary: British Minister who briefs the "00" agents for Operation Thunderball and has doubts about Bonds efficiency.
* Lois Maxwell as Miss Moneypenny: Ms secretary.
*  , emphasising his devotion as a killer. He is killed by Bond with a spear gun on the beach.
* George Pravda as Ladislav Kutze: Emilio Largos chief nuclear physicist who aids his boss with the captured bombs. Michael Brennan as Janni: One of Largos thugs who is usually paired with Vargas.
* Anthony Dawson as Ernst Stavro Blofeld, voiced by Eric Pohlmann (both un-credited): The head of SPECTRE
* Bill Cummings as Quist: Another of Largos inefficient thugs who, after failing to assassinate 007, is thrown into a shark pool under orders from his boss.
* André Maranne, best known for portraying Sergeant François Chevalier in the Pink Panther films, cameos as SPECTRE #10.

==Production==

===Legal disputes===
 
Originally meant as the first James Bond film, Thunderball was the centre of legal disputes that began in 1961 and ran until 2006.    Former Ian Fleming collaborators Kevin McClory and Jack Whittingham sued Fleming shortly after the 1961 publication of the Thunderball novel, claiming he based it upon the screenplay the trio had earlier written in a failed cinematic translation of James Bond.  The lawsuit was settled out of court; McClory retained certain screen rights to the novels story, plot, and characters. By then, James Bond was a box office success, and series producers Broccoli and Saltzman feared a rival McClory film beyond their control; they agreed to McClorys producers credit of a cinematic Thunderball, with them as executive producers.   

The sources for Thunderball are controversial among film aficionados. In 1961, Ian Fleming published his novel based upon a television screenplay that he, and others developed into the film screenplay; the efforts were unproductive, and Fleming expanded the script into his ninth James Bond novel. Consequently, one of his collaborators, Kevin McClory, sued him for plagiarism; they settled out of court in 1963. 
 official credits John Hopkins, the screenplay is also identified as based on an original screenplay by Jack Whittingham and as based on the original story by Kevin McClory, Jack Whittingham, and Ian Fleming.  To date, the novel has twice been adapted cinematically; the 1983 Jack Schwartzman-produced Never Say Never Again, features Sean Connery as James Bond, but is not an Eon production.

===Casting=== Billy Liar in 1963. Upon meeting her personally, however, he was disappointed and turned his attentions towards Raquel Welch after seeing her on the cover of the October 1964 issue of Life magazine. Welch, however, was hired by Richard Zanuck of 20th Century Fox to appear in the film Fantastic Voyage the same year instead. Faye Dunaway was also considered for the role and came close to signing for the part. {{cite web Triple Cross (1966). One of the actresses that tried for Domino, Luciana Paluzzi, later accepted the role as the redheaded femme fatale assassin Fiona Kelly who originally was intended by Maibaum to be Irish. The surname was changed to Volpe in coordination with Paluzzis nationality. 

===Filming===
  Terence Young, From Russia With Love and Thunderball. Years later, Young said Thunderball was filmed "at the right time",  considering that if it was the first film in the series, the low budget (Dr. No cost only $1 million) would not have yielded good results.    Thunderball was the final James Bond film directed by Young.
 Silverstone racing The Bahamas (where most of the footage was shot), and Miami.    Huntington Hartford gave permission to shoot footage on his Paradise Island and is thanked at the end of the film.

 
On arriving in Nassau McClory searched for possible locations to shoot many of the key sequences of the film and used the home of a local millionaire couple, the Sullivans, for Largos estate.  Part of the SPECTRE underwater assault was also shot on the coastal grounds of another millionaires home on the island. The most difficult sequences to film were the underwater action scenes; the first to be shot underwater was at a depth of 50 feet to shoot the scene where SPECTRE divers remove the atomic bombs from the sunken Vulcan bomber. Peter Lamont had previously visited a Royal Air Force bomber station carrying a concealed camera which he used to get close-up shots of secret missiles (those appearing in the film were not actually present).  Most of the underwater scenes had to be done at lower tides due to the sharks in the Bahamian sea. 

Connerys life was in danger in the sequence with the sharks in Largos pool. He had been in fear of this risk when he read the script. He insisted that Ken Adam build a special Plexiglas partition inside the pool, but, despite this, it was not a fixed structure and one of the sharks managed to pass through it. Connery had to abandon the pool immediately, seconds away from attack.  Another dangerous situation occurred when special effects coordinator John Stears brought in a supposed dead shark carcass to be towed around the pool. The shark, however, was not dead and revived at one point. Due to the dangers on the set, stuntman Bill Cummings demanded an extra fee of £250 to double for Largos sidekick Quist as he was dropped into the pool of sharks. 

The climactic underwater battle was shot at Clifton Pier and was choreographed by Hollywood expert Ricou Browning, who had worked on many films previously such as Creature From the Black Lagoon in 1954. He was responsible for the staging of the cave sequence and the battle scenes beneath the Disco Volante and called in his specialist team of divers who posed as those engaged in the onslaught. Voit provided much of the underwater gear in exchange for product placement and film tie-in merchandise. Lamar Boren, an underwater photographer, was brought in to shoot all of the sequences. United States Air Force Lieutenant-Colonel Charles Russhon, who had already helped alliance Eon productions with the local authorities in Turkey for From Russia With Love (1963) and at Fort Knox for Goldfinger (1964), stood by and was able to supply the experimental rocket fuel used to destroy the Disco Volante. Russhon, using his position, was also able to gain access to the United States Navys Fulton surface-to-air recovery system, used to lift Bond and Domino from the water at the end of the film.    Filming ceased in May 1965 and the final scene shot was the physical fight on the bridge of the Disco Volante. 
 Disco Volante. volatile liquid, Stears doused the entire yacht with it, took cover, and then detonated the boat. The resultant massive explosion shattered windows along Bay Street in Nassau roughly 30 miles away.    Stears went on to win an Academy Award for his work on Thunderball.
 Nassau stating his frustration with the harassment that came with the role; "I find that fame tends to turn one from an actor and a human being into a piece of merchandise, a public institution. Well, I dont intend to undergo that metamorphosis."  In the end he gave only a single interview, to Playboy, as filming was wrapped up, and even turned down a substantial fee to appear in a promotional TV special made by Wolper Productions for NBC The Incredible World of James Bond.  According to editor Peter R. Hunt, Thunderballs release was delayed for three months, from September until December 1965, after he met Arnold Picker of United Artists, and convinced him it would be impossible to edit the film to a high enough standard without the extra time.  

===Effects===

 
 rocket belt Bond uses to escape the château actually worked, and was used many times, before and after, for entertainment, most notably at Super Bowl I and at scheduled performances at the 1964–1965 New York Worlds Fair.   
 underwater jet pack scuba (allowing the frogman to manoeuvre faster than other frogmen). Designed by Jordan Klein, green dye was meant to be used by Bond as a smoke screen to escape pursuers.  Instead Ricou Browning, the films underwater director, used it to make Bonds arrival more dramatic.   
 sky hook, used to rescue Bond at the end of the film, was a rescue system used by the United States military at the time. At Thunderballs release, there was confusion as to whether a rebreather such as the one that appears in the film existed; most Bond gadgets, while implausible, often are based upon real technology. In the real world, a rebreather could not be so small, as it has no room for the breathing bag, while the alternative Open-circuit scuba|open-circuit scuba releases exhalation bubbles, which the film device does not. It was made with two CO 2  bottles glued together and painted, with a small mouthpiece attached.  For this reason, when the Royal Corps of Engineers asked Peter Lamont how long a man could use the device underwater, the answer was "As long as you can hold your breath." 
 Bob Simmons Pinewood which he used to film the silhouetted title girls who appeared naked in the opening sequence, which was the first time actual nudity (although concealed) had ever been seen in a Bond film. 

On 26 June 2013 Christies auction house sold the Breitling SA Top Time watch worn in the film by Sean Connery for over £100,000; given to Bond by Q, it was also a Geiger counter in the plot.   

===Music===
  John Barry, From Russia Don Black Tom Jones who, according to Bond production legend, fainted in the recording booth when singing the songs final note. Jones said of it, "I closed my eyes and I held the note for so long when I opened my eyes the room was spinning."  The song, Maurice Binders titles, and the lengthy holding of the final note were parodied by Weird Al Yankovics title sequence for Spy Hard with instrumental backing by Jimmie Haskell.

Country musician Johnny Cash also submitted a song to Eon productions titled "Thunderball", but it went unused. 

==Release and reception== Doctor Zhivago Live and Let Die (1973) assumed the record.  After adjusting its earnings to 2011 prices, it has made approximately $1 billion, making it the second most financially successful Bond film after Skyfall. 
 Best Production Golden Screen Award in Germany and the Golden Laurel Action Drama award at the 1966 Laurel Awards. The film was also nominated for an Edgar Best Foreign Film award at the Edgar Allan Poe Awards. 

===Contemporary reviews=== David Robinson of The Financial Times criticised the appearance of Connery and his effectiveness to play Bond in the film remarking: "Its not just that Sean Connery looks a lot more haggard and less heroic than he did two or three years ago; but there is much less effort to establish him as connoisseur playboy. Apart from the off-handed order for Beluga, there is little of that comic display of bon viveur-manship that was one of the charms of Connerys almost-a-gentleman 007." 

===Reflective reviews===
According to Danny Peary, Thunderball "takes forever to get started and has too many long underwater sequences during which its impossible to tell whats going on. Nevertheless, its an enjoyable entry in the Bond series. Sean Connery is particularly appealing as Bond – I think he projects more confidence than in other films in the series. Film has no great scene, but its entertaining as long as the actors stay above water." 
 Time Out polled several film critics, directors, actors and stunt actors to list their top action films;  Thunderball was listed at number 73. 

==See also==
 
* Outline of James Bond

==References==
 

==Sources==
  Casino Royale Sony and MGM.
*  
*  
* }}
 

==External links==
 
*  
*  
*  
*  
*  
*  
*  
* http://www.telegraph.co.uk/culture/film/jamesbond/9864054/James-Bond-film-Thunderball-nearly-given-X-rating-by-censors.html

 
 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 