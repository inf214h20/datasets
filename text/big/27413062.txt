Star Maps (film)
{{Infobox film
| name = Star Maps
| image = StarMapsfilm.jpg
| director = Miguel Arteta
| producer = Matthew Greenfield
| writer = Miguel Arteta Matthew Greenfield
| starring = Douglas Spain Efrain Figueroa Kandeyce Jorden Martha Veléz Robin Thomas Zak Penn
| cinematography = Chuy Chavez
| editing = Jeff Betancourt Tom McArdle Tony Selzer
| studio = King Films Flan de Coco Films
| distributor = Fox Searchlight Pictures
| released =  
| runtime = 86 minutes
| country = United States
| language = English Spanish
}}
Star Maps (1997) is the directorial debut of Miguel Arteta that was first presented at the Sundance Film Festival. It was a critical hit, receiving five Independent Spirit Award nominations, including Best First Feature and Best First Screenplay. 

==Plot== allegorical tale hustling as a way to meet Hollywood insiders.  Things start to look up when he hooks up with the producer of the popular daytime soap opera Carmel County. However his overbearing father and his mentally unstable mother threaten to get in the way of his dreams.

==Cast==
*Douglas Spain... Carlos Amado 
*Efrain Figueroa... Pepe Amato
*Kandeyce Jorden... Jennifer Upland
*Lysa Flores... Maria Amato  
*Annette Murphy... Letti
*Martha Veléz... Teressa Amato
*Robin Thomas... Martin
*Vincent Chandler... Juancito Amato 
*Al Vincente... Fred Marin
*Zak Penn... Carmel County Writer Mike White... Carmel County Writer
*Michael Peña... Star Map Boy

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 

 