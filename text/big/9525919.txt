Atrapadas
{{Infobox film
| name = Atrapadas
| image =  Atrapadas.jpg
| caption = Theatrical release poster
| image_size = 200px
| director = Aníbal Di Salvo
| producer = Carlos Luis Mentasti Luis A. Scalella
| writer = Aníbal Di Salvo José P. Dominiani
| starring = Leonor Benedetto
| music = Luis María Serra
| cinematography = Carlos Torlaschi
| editing = Darío Tedesco
| distributor = Trans World Entertainment
| released = August 16, 1984 (Argentina)
| runtime = 90 min.
| country = Argentina Spanish
| budget =
}} thriller drama film written and directed by Aníbal Di Salvo, set in a prison|womens prison. It stars Leonor Benedetto.

== Synopsis ==
A corrupt prison system comes under scrutiny in this action drama about a racketeering lesbian inmate, crooked wardens, drug traffickers, and another female prisoner who challenges the system. Silvia (Leonor Benedetto) refuses to cooperate with Susana (Camila Perissé) when she comes around to induce her to take drugs, an act which gets her severely beaten and her younger sister murdered. Outraged at Susanas collusion with a handful of corrupt wardens and drug barons, Silvia vows revenge, and, with the help of some inmates and a decent warden, she escapes from prison for one night to carry out a plan that is meant to eliminate the drug traffickers, Susana, and their prison minions — on both side of the bars.

== Cast ==
* Leonor Benedetto ... Silvia
* Betiana Blum ... Martina 
* Cristina Murta ... La Galíndez 
* Camila Perissé ... Susana Nieto 
* Mirta Busnelli .... Graciela González (as Mirtha Busnelli) 
* Rita Terranova ... Maricarmen 
* Juan Leyrado ... Daniel
* Mónica Galán 
* Esther Goris 
* Paulino Andrada
* Susana Cart
* Miriam Perazolo
* Elvia Andreoli ... Olga
* Patricia Bermudez
* Clotilde Borella
* Olga Bruno
* Perla Cristal
* Julio Fedel
* Golde Flami
* Inés Murray ... La Judía 
* Carlos Olivieri
* Adriana Parets ... Celadora
* Dorys Perry ... La Rusita
* Gerardo Romano ... Nacho 
* Edgardo Suárez ... El Negro
* Hernán Zabala

== External links ==
*  
*  

 
 
 
 
 
 

 
 