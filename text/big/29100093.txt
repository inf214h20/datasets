Abhinandana
{{Infobox film
| name = Abhinandana
| image = Abhinandana.jpg Ashok Kumar
| producer = R.V. Ramana Murthy
| writer = Ashok Kumar Karthik Shobana Sarath Babu Rajya Lakshmi J.V. Somayajulu
| music = Illayaraja
| cinematography = Ashok Kumar
| release date = 1st January 1988
| editing  =  
| country  = India
| language = Telugu
}} Nandi Award for Second Best Feature Film.

==Plot==
Set in Kodaikanal, the story revolves around Rani (Shobhana), an aspiring dancer, who meets Raja (Karthik Muthuraman), an aspiring painter and singer. They fall in love with each other as their mutual love for art unfolds at an arts institute/college. Ranis father (J.V. Somayajulu) wishes to get his daughter married soon. But, Rani is bent on convincing her father to accept Raja as her future spouse. The drama unfolds when Ranis pregnant sister Kamala (Rajya Lakshmi) and her husband (Sarath Babu) along with their two kids visit from Chennai. Kamalas husband, who owns a recording studio, leaves for Chennai after a short stay in Kodaikanal.

After Kamalas accidental death, her father wishes to get Rani married to her brother—in-law and take care of the two kids. Rani has to choose between Raja and looking after her sisters family. Raja cant take this and takes to drinking as he loses hope of uniting with Rani. Unaware of Rajas connection to Rani, Kamalas widower meets Raja. There he notices Rajas artistic skills and offers him an opportunity to sing for an album at his recording theater. He introduces Raja to Rani at Ranis home before requesting Raja to live there until Rajas recording is complete.

The movie becomes dramatic as Ranis brother-in-law finds out that Rani is in love with Raja. How the individuals try to compromise and sacrifice for each others aspirations and feelings forms the crux of the story.

==Cast== Karthik as Raja
*Shobana as Rani 
*Sarath Babu
*Rajya Lakshmi as Kamala
*J.V. Somayajulu

==Soundtrack==
Music was composed by Ilayaraja. "Ade Neevu" was reused from "Ore Raagam" from Amudhagaanam and "Manchu Kurise" was reused from "Andharangam Yaavume" from Aayiram Nilave Vaa.

{{Tracklist
| headline     = Songs
| extra_column = Playback
| all_music    = Ilayaraja
| all_lyrics   = Acharya Atreya 

| title1  = Ade Neevu Ade Nenu Ade Geetam Padana
| extra1  = S. P. Balasubramanyam

| title2  = Chukkalanti Ammayi Chakkanaina Abbayi
| extra2  = S. Janaki

| title3  = Chukkalanti Ammayi Chakkanaina Abbayi
| note3   = Sad
| extra3  = S.P. Balasubramanyam

| title4  = Eduta Neeve Edalo Naa Neeve
| extra4  = S.P. Balasubramanyam

| title5  = Manchu Kurise Velalo Malle Virisedenduko
| extra5  = S. P. Balasubrahmanyam, S. Janaki

| title6  = Prema Entha Madhuram Priyuralu Anta Kathinam
| extra6  = S.P. Balasubramanyam

| title7  = Prema Ledani Premincharadani
| extra7  = S.P. Balasubramanyam

| title8  = Rangulalo Kalavo Eda Pongulalo Kalavo
| extra8  = S.P. Balasubramanyam, S. Janaki
}}

==Awards==
 
|- 1988
| K. Ashok Kumar
| Nandi Award for Best Feature Film - Rajata (Silver) Nandi
|  
|-
| S.P. Balasubramaniam (for singing "Rangulalo Kalavo")
| Nandi Award for Best Male Playback Singer
|  
|- Karthik Muthuraman|Karthik
| Nandi Special Jury Award
|  
|}

==References==
 
==External links==
*  

 
 
 
 