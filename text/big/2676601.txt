Dickie Roberts: Former Child Star
{{Infobox film
| name           = Dickie Roberts: Former Child Star
| image          = Dickie Roberts Former Child Star film.jpg
| caption        = Theatrical release poster
| director       = Sam Weisman Fred Wolf
| writer         = David Spade Fred Wolf 
| starring       = David Spade Mary McCormack Craig Bierko Rob Reiner
| music          = Christophe Beck Waddy Wachtel
| cinematography = Thomas E. Ackerman
| editing        = Roger Bondelli
| studio         = Happy Madison Productions
| distributor    = Paramount Pictures
| released       =  
| runtime        = 98 minutes
| country        = United States
| language       = English
| budget         = $17 million
| gross          = $23,794,648
}}
Dickie Roberts: Former Child Star is a 2003 American comedy film directed by Sam Weisman and starring David Spade (who also co-wrote the film) and Mary McCormack. Spade portrays a child actor who fell into obscurity as an adult, and who attempts to revive his career. 

The film was a marginal financial success, but critical reviews were mostly negative.

== Plot == child star sitcom called The Glimmer Gang with his catchphrase "This is Nuckin Futs!". His career subsequently halted after his 6th birthday. Since his heyday, he has been reduced to parking cars at Mortons Restaurant Group|Mortons and appearing on Celebrity Boxing, where he is matched with Emmanuel Lewis. In the public eye and to his girlfriend Cyndi who apparently leaves him during a roadside incident, Dickie is washed up.
 Tom Arnold to hook him up with Reiner. After he is kicked out because hes not an alcoholic, Dickie fakes being wasted and crashes what turns out to be a Lamaze class. However, Brendan Fraser (in an uncredited cameo appearance) is in the class and he agrees to call Reiner for Dickie.

Reiner bluntly tells Dickie that the part is not within his abilities because it requires knowing how a regular person lives. Unfortunately, Dickie never had a real childhood; he grew up in the limelight, and then his mother abandoned him when his show was cancelled. Desperate to prove to Reiner that hes right for the part, Dickie sells his raunchy autobiography to raise $30,000. With the money, he pays a family to "adopt" him for a month, as he believes he will "watch and learn". 

Once Dickie hires his "family", things get off to a rocky start, as George, the bread winning father, insists that they need the money, despite the rightful reservations of the other family members.  Grace, the mother, comes to pity and gradually provides him with surrogate guidance, realizing the lesson from Blakes Backyard itself: sometimes all of the things you need are in your own backyard. Dickie learns much about himself and life in general, and begins to act as a third parent. He helps the familys son score a date with his dream girl and helps the daughter join the pep squad. Cyndi returns to him and is admired by George, who turns out to be an inept subject of fidelity.

Sidney lands an audition for Dickie by donating a kidney to Reiner after Reiner is savagely beaten by a psychotic street douche whom Dickie provoked while unknowingly driving Reiners vehicle; Dickie is awarded the part, proving that "In Hollywood, Sometimes Your Dreams Can Come True...Again".  After George and Cyndi abandon the household together, Dickie gives up the part to be with the family he has come to love.  

The movie ends with a faux E! True Hollywood Story report on Dickie, who now turns his real story into a new sitcom that uses all of his old friends, as well as his new family (including the mother, whom he has married). The closing credits are a take-off on Relief albums listed as "To help former child stars". The song includes The Brady Bunch s Maureen McCormick singing "But if one more person calls me Marcia, Ill bust his fucking head", and many references to old television sitcoms.
 Barry Williams, Dustin Diamond and Danny Bonaduce.

== Cast ==
*David Spade as Dickie Roberts
*Mary McCormack as Grace Finney
*Scott Terra as Sam Finney
*Jenna Boyd as Sally Finney
*Jon Lovitz as Sidney Wernick
*Alyssa Milano as Cyndi
*Rob Reiner as himself
*Craig Bierko as George Finney

===Cameos===
*Leif Garrett
*Doris Roberts
*Emmanuel Lewis Tom Arnold Barry Williams
*Danny Bonaduce
*Corey Feldman
*Brendan Fraser
*Dick Van Patten
*Sasha Mitchell
*Dustin Diamond
*Florence Henderson
*Rachel Dratch
*Aerosmith
*Butch Patrick
*Gary Coleman
*Maureen McCormick
*Todd Bridges Christopher Knight
*Michael Buffer
*Corey Haim
*Fred Berry
*Peter Dante
*Jeff Conaway
*Erin Moran
*Jay North
*Jonathan Loughran
*Tony Dow
*Barry Livingston
*Adam Rich
*Eddie Mekka
*Ron Palillo
*Jeremy Miller
*Haywood Nelson
*Rodney Allen Rippy
*Marion Ross
*Ernest Lee Thomas
*Charlene Tilton
*Jay Leno
*David Soul

== Lawsuit ==
Paramount Pictures was sued for trademark infringement and dilution after this film was released. Paramount had not requested permission from Wham-O for using the Slip n Slide in this movie.   The lawsuit claimed that the movie, which portrayed unsafe use of a Slip n Slide, might encourage others to use it in an unsafe manner.  The lawsuit was dismissed by a California court. 

==Reception==
Dickie Roberts was a modest financial success, earning over $22 million against an estimated budget of $17 million 

Critical reception was mostly negative. Rotten Tomatoes describes the film as "rotten," with a 23% rating.  While critics generally agreed that the premise had potential and appreciated the involvement of actual former child stars, reactions to Spades humor were mixed, and the attempts to make the film genuinely uplifting and sentimental in its second half were seen as contrived and unnecessary.  Roger Ebert gave the movie two-out-of-four stars, noting "Dickie Roberts: Former Child Star has a premise that would be catnip for Steve Martin or Jim Carrey, but David Spade (who, to be fair, came up with the premise) casts a pall of smarmy sincerity over the material."  However, some critics still noted the film as an improvement over previous David Spade features, such as Joe Dirt.

== References ==
 

== External links ==
 
*  
*  
*   at Rotten Tomatoes
*  

 

 
 
 
 
 
 
 
 
 