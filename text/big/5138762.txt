Battlefield Baseball
{{Infobox Film
| name           = Battlefield Baseball
| image          = Battlefieldbaseball.jpg
| caption        = 
| director       = Yūdai Yamaguchi
| producer       = Ryuhei Kitamura
| writer         = Gatarō Man
| starring       = {{plainlist|
* Tak Sakaguchi 
* Atsushi Itō
* Hideo Sakaki
}}
| music          = 
| cinematography = 
| editing        = Shuichi Kakesu
| distributor    = Klock Worx Co.
| released       =  
| runtime        = 87 minutes
| country        = Japan
| language       = Japanese
| budget         = 
}}
  is a 2003 Japanese film directed by Yūdai Yamaguchi. The film is written by Gatarō Man, based on his manga series of the same name, and stars Tak Sakaguchi, Atsushi Itō, and Hideo Sakaki. It was produced by Ryuhei Kitamura.
 high school sports—and the violence and brutality of a horror film.  The films bizarre—sometimes almost incoherent—plot, blood and gore, and unique comedy have given it something of a "cult film|cult" popularity in the West. 
 bats and baseball (ball)|balls, however.

The film was released on Region 1 DVD by Subversive Cinema.

== Plot ==
 
Its every high school baseball teams dream to go to the legendary Koshien Stadium High school baseball in Japan|Tournament. For the first time in years, Seido High School has a chance&mdash;star player Gorrila Matsui has finally given the team an opportunity to succeed. Most delighted at this prospect is Principal Kocho. His hopes are dashed, however, when the Head teacher reveals to him that the first game will be played against the infamous Gedo High School. 

Gedo is notorious for its brutal killings during games. They hardly play at all, instead engaging in a form of martial arts combat called "fighting baseball". Their matches erupt into brutal battlefields, Gedo slaughtering its opponents in any way possible, and the competing team vainly struggling for their lives. The Gedo team is almost inhuman in its slaughter, and their bizarre weapons and attire coupled with their green-grey skin only increase this reputation. Understandably, Kocho is distressed&mdash;not only at the likely murder of his students, but at the loss of yet another chance to win the Koshien Stadium Tournament.

Much to his delight, however, is the appearance of mysterious newcomer Jubeh, played by Tak Sakaguchi. Jubeh rescues Four Eyes (Megane, so named because of his eyeglasses.), played by Atsushi Itō, from a gang of expelled students, beating Bancho (Japanese for "boss" or "leader") and catching Kochos eye.  During various points in the film, a crowd of people appears, usually engaging in spontaneous celebration or watching some event. Among the crowd is a schoolgirl dressed in a traditional sailor-fuku, an injured man with a cast on his arm, and a nude man with his crotch covered by a fig leaf. According to the audio commentary included on the DVD release, the nude man is played by director Yudai Yamaguchi.   Another recurring element in the film is a drunk man, usually seen laughing uproariously at whatever event has just occurred. He is always accompanied by his dog. At the end of the film, it is revealed through narration that the drunk died of alcohol poisoning during the final confrontation between the antagonists and protagonists. He also states that the drunk was "  master", implying that the dog has been the narrator of the entire film. Andy Klein of the L.A. City Beat called this the films "best joke".    He begs and pleads with Jubeh to join the team to help them defeat Gedo, but he steadfastly refuses. During this time Kocho and Jubeh are also confronted by a resurrected Bancho, though his "face changed". Bancho joins the team, explaining that the injuries that prevented him from playing baseball were cured by Jubehs punches.
 pitching skill, explaining how he became so skilled he was a danger to himself and others. Only his father, with an absurdly huge catchers mitt, will allow Jubeh to pitch. However, when an accidental ball to the head kills him, Jubeh vows never to pitch again.

Despite this sorowful story, Four Eyes insists that Jubeh join the team, explaining his own plight. His mother hates baseball, and would be furious if he were to play. As a result, he must keep his presence on the Seido team a secret. Four Eyes sheer love for the game touches Jubeh deeply, and he joins the team. 

However, when the game against Gedo starts, Jubeh is nowhere to be found. As a result, the team is, predictably, slaughtered by the Gedo students.  Jubeh gets there in time to hear Gorillas last words.  Finding a body he believes to be Four Eyes, Jubeh laments the loss, before realising that its in fact a Gedo trap. The "body" explodes, hurling Jubeh across the field, sending him to an early grave.

Jubeh finds himself in a sepia-toned small town street in the afterlife. He sees a man he has not seen for some time&ndash;his father. He convinced Jubeh to embrace his pitching skills, and defeat Gedo once and for all. Jubeh pledges to do so as he watches his father ride off. Returning from the afterlife, he introduces his pitch, dubbed the "Super Tornado". Bancho also returns, again, this time as a child. He too met Jubehs father in the afterlife, and was given his catchers mitt.

In the meantime, somehow Four Eyes mother has discovered that he was on the baseball team. As punishment, she locks him up in a cage, and watches over him day and night. Jubeh comes to rescue him, fighting Four Eyess mother. He confronts her, asking her why she hates baseball so much. She reveals that her husband was killed by a baseball pitcher. All three come to a sudden realization&mdash;They are family. In jubilation, Four Eyes mother gives him permission to play. 

Kocho, Bancho, Jubeh, and Four Eyes, having formed a new Seido team, confront Gedo. The Gedo coach mocks the size and strength, or more accurately the lack thereof, of the new team. Much to his surprise however, several new team members show up&mdash;Head Teacher and Gorilla, now cyborgs, having been resurrected by "advanced technology", one of the schools cheerleaders, and Four Eyes mother all team up to defeat Gedo. 

Ultimately, all except Jubeh and the Gedo coach have been knocked unconscious. They duel with elaborate baseball bats, before Jubeh is knocked to the ground.  Then, the Gedo coach unveils his ultimate weapon: the Gedo Poison Bat. Filled with a combination of deadly poisons and healing herbs, the effect is to create a horrific pain that will last for "hundreds of years." The coach slams it down onto Jubeh&mdash;However, Four Eyes has sacrificed himself, jumping in the way of the coachs attack. Jubeh, furious at the loss of his brother, leaps up and bitterly attacks the coach. However, before he can kill the coach, the Gedo players plead for his life, explaining how he brought them out of orphanages and became like a father to them. Seeing their heartfelt plea, Jubeh cant help but allow the coach to live.

Happily, the coach gains a newfound respect for life, and throws a bottle of antidote to Jubeh. A crowd gathers,  and the sun begins to shine. However, this peace is soon is interrupted by one member of the Gedo team, his head wrapped in bandages. He guns down everyone on the field, firing randomly and hitting everything in sight, including his own teammates. Jubeh manages to escape only through his own dexterity and the gunmans limited ammo. Looking around in outrage, Jubeh sheds a tear. As cherry blossoms fall, everyone on the field returns to life, before Jubeh attacks the bandaged gunman, literally knocking the muscles off his bones.

Joyously, the crowd celebrates, and the narrator states that they lived happily ever after&mdash;Including the one person on the field that day who wasnt resurrected by Jubehs tears. 

== Reception ==
The reaction to the film has been for the most part lukewarm. Though many praised its irreverent and unique style, others found the purposefully ludicrous plot hard to follow and indicative of a lack of effort on the screenwriters part. However, the film did win the Grand Prize at the 14th Yubari International Fantastic Film Festival held in February 2003. Director Yūdai Yamaguchi was a guest at the festival. 

The film has been compared with other Japanese films featuring baseball prominently. For instance, one reviewer compared it to 1992s Mr. Baseball, explaining that it was better than that film due to a relative lack of actual baseball.  The parodic aspect of the film takes so much precedence that the clichés of baseball films are skewered more than baseball itself.  . 

One of the key aspects of the film is its intentional strangeness. The strangest aspects of the plot (the frequent reappearances of a crowd  and an alcoholic man,  Head Teacher and Gorilla returning as cyborgs, etc.) are more prominent than the plot in the minds of most reviewers.  Erik Lundergaar   from the Seattle Times. 
 high art, it is a good martial arts/horror/comedy film&mdash;they say that it is interesting and exciting enough to keep one watching until the end credits.   The films humour is often highly praised, lightening up what would otherwise be a superficial and generic action film.  The film has received some criticism, largely that it is ludicrous and "stupid", or that its plot does not sustain its length.  However, others argue that this is not only irrelevant&mdash;The film is intended to be ludicrous and stupid&mdash;it is actually one of the strengths of the film. 

== References ==
 

== External links ==
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 