Gouravam (2013 film)
 
 
 
{{Infobox film
| name = Gouravam
| image = Gouravam poster.jpg
| image_size = 
| caption = Movie poster
| director = Radha Mohan
| producer = Prakash Raj
| writer = Radha Mohan B.V.S.Ravi Viji
| starring = Allu Sirish Yami Gautam Prakash Raj Harish
| music = S. Thaman
| cinematography = Preetha
| editing =I J Alen
| studio = Duet Movies
| released =    
| country = India
| language = Telugu Tamil
| budget = 
| gross =   
}}
 Telugu and Tamil languages, the film was produced by Prakash Raj under the banner of Duet Movies. It marks the acting debut of Allu Sirish, brother of Allu Arjun, and also stars Yami Gautam in the lead.  Gouravam deals with the issue of honour killing.  The film released on 19 April 2013 with a linear narrative and a Realistic ending in Tamil and with a Non linear narrative and Dramatic ending in Telugu.  The film received mainly negative reviews from critics and audience.

==Plot==
Arjun (Allu Sirish) is a rich kid who has all the luxuries in life. One day, his dad sends him to a village called T Vennure for some business work. As Arjuns best friend Shanmugam also hails from that village, Arjun happily accepts to go there. On landing in the village, Arjun comes to know that his friend Shanmugam has eloped with Rajeswari, the daughter of Pashupathy (Prakash Raj), a rich land lord in the village who is hell bent to save his familys prestige always, and is missing from the past six months. After meeting Shanmugams father (L.B. Sriram) and finding out some bitter truths about Shanmugam, Arjun decides to stay in the village and find his missing friend. A young lawyer Yazhini (Yami Gautam) assists him in his work. After finding a strong opposition from Pasupathys son Saravanan (Harish) and Palani (Brahmaji), Arjun along with his friend Venky (Sricharan (actor)|Sricharan) and Yazhini assemble all their college friends as a group along with the neighbour village youth as a group to find the mystery behind Shanmugam and Rajeswaris absence from the village, only to know that they were killed and buried at the grounds of a Damaged Monument. Arjun stays back there to find out the reason behind the couples death. After finding a help from Pasupathys wife (Pavitra Lokesh) and Daughter in Law (Lakshmi Priyaa Chandramouli), Arjun files a case with the help of Yazhini. The case, though interrupted many times due to Saravanans men, including the towns police, remains steady due to the efforts of Arjun and his friends. A fine day, on the request of Pasupathys wife, Palani promises to help Arjun in the mystery. As promised he helps them by narrating the incidents happened actually.

Till here, both the versions were identical. But from this point of story, the rest of the film is different in both the versions.

The Tamil Version goes like this.

Palani starts telling the truths to Arjun, Yazhini, Venky and his group. The night Rajeswari eloped with Shanmugam, Pasupathy orders Saravanan and Palani to bring her back home. They find her with Shanmugam in a Bus and take them to a Cold Storage. There while Saravanan mercilessly thrashes Shanmugam, Rajeswari stops him and holds Shanmugams hand and tells that she cant live without him. In a fit of rage, Saravanan cuts Rajeswaris hand with a Sword. While Palani could know what was happening, the couple is killed by Saravanan. Palani confesses the same in front of the magistrate which leads her to issue an arrest warrant on Saravanan as well as Pasupathy, who was aware of the happenings. Saravanan, frustrated, goes on a killing spree to kill Arjun and his friend, only to be thrashed mercilessly by Arjun. Saravanan surrenders himself to Police whereas Pasupathy kills himself by a Gun before the police reach him. The movie thus ends on a happy note with Arjuns friends returning to home and an Injured Arjun talking to the Media about the Honor killing that took place there.

The Telugu Version goes like this

After knowing the truth from Ram Babu (Palanis character in Telugu), Arjun and Venky rush to the court along with Ram Babu, only to be attacked by Jagapathi (Saravanans character in Telugu) in the mid way. A Combat takes place between Arjun and Jagapathi and while both were exhausted almost, Arjuns friends rush to the spot thus making Jagapathi and his men escape from there. Now Ram Babu and a Blood-stained Arjun go to Pasupathis home to tell the truth. Ram Babu starts narrating what happened. The night Rajeswari eloped with Shankar, Pasupathi orders Jagapathi and Ram Babu to bring her back home. They find her with Shankar in a Bus and take them to a Cold Storage. There while Jagapathi mercilessly thrashes Shankar, Rajeswari stops him and holds Shankars hand and tells that she cant live without him. In a fit of rage, Jagapathi cuts Rajeswaris hand with a Sword. While Ram Babu could know what was happening, the couple is killed by Jagapathi. Paupathi and his family, who were not at all aware about this, experience a bitter shock. Later on Jagapathi arrives who confesses the truth saying he did it to save their familys Prestige. A completely changed Pasupathi, kills Jagapathi by shooting him after realising his mistake. He then surrenders to the Police Voluntarily. Arjun depressed by the events is solaced by Shankars father. This incident brings a change in the upper caste persons and thus integrating themselves with the Lower caste persons.

==Cast==

===Tamil===
 
*Allu Sirish as Allu Arjun
*Yami Gautam as Yazhini
*Prakash Raj as Pasupathy
*Ankit Patel as Krishnan
* Harish Uthaman as Saravanan
*Nassar as Soundarapandyan
*Elango Kumaravel as Maasi Sricharan as Venky
*L.B. Sriram as Shanmugams father
* Lakshmi Priyaa Chandramouli as Saravanans wife
* Madhu as Arjuns Friend
* Pavitra Lokesh as Pasupathys wife
* Brahmaji as Palani
* Vignesh as Shanmugam
* Priya as Rajeshwari
* Kaajal Pasupathi as Sumathi
* Margaret Divya
 

===Telugu===
 
*Allu Sirish as Allu Arjun
*Yami Gautam as Yamini
*Prakash Raj as Pasupathi
*Harish Uthaman as Jagapathi
*Nassar as Yaminis father
*Elango Kumaravel as Baachi
*Sricharan as Venky
*L.B. Sriram as Shankars father
*Pavitra Lokesh as Pasupathis wife
*Brahmaji as Ram Babu
*Vignesh as Shankar
*Priya as Rajeshwari
*Lakshmi Priyaa Chandramouli as Jagapathis wife
*Margaret Divya

===Hindi===
*Allu Sirish as Allu Arjun

 

==Production== Thaman signed Nagarjuna agreed to finance the film under their home production house, Annapoorna Studios. 
 Ponniyin Selvan, had it announced that the film would be produced under the banner of Duet Movies.  Harish will be seen in a pivotal role in Radha Mohans Gauravam. 

Filming for Gouravam started in Chennai on 25 June 2012 and continued in Mysore.  

==Soundtrack==
{{Infobox album
| Name     = Gouravam (Tamil/Telugu)
| Type     = Soundtrack
| Artist   = S. Thaman

|  Cover    =   Feature film soundtrack
| Length   = 
| Label    = Prakash Raj Music
| Producer = Thaman S
| Last album = Greeku Veerudu (2013)
| This album = Gouravam (2013)
| Next album = Vaalu (2013)
}}

Gouravams soundtrack comprises 4 tracks composed by S. Thaman. The album was released on 31 March 2013.

Tamil Soundtrack
{{Track listing extra_column = Singer(s) music_credits = no lyrics_credits = no all_lyrics = Madhan Karky

|title1= Ondraai Ondraai
|extra1=  Haricharan & Suchitra
|length1= 
|title2= Maname Maname Ranjith & Vardhani Thaman
|length2= 
|title3= Mannadacha Pandhu 
|extra3= Gaana Bala
|length3=
|title4= Oru Grammam
|extra4= Gaana Bala
|length4= 
}}

Telugu Soundtrack
{{Track listing extra_column = Singer(s) music_credits = no lyrics_credits = no all_lyrics = Chandrabose

|title1= Okkatai Okkatai
|extra1= Haricharan & Suchitra
|length1= 
|title2= Manasa Manasa
|extra2= Mallikarjun Rao & Vardhani Thaman
|length2= 
|title3= Chethinundhi Mannu Thesi
|extra3= Muralidhar
|length3=
|title4= Oka Gramam
|extra4= Deepu
|length4=
}}

== Release == IPL cricket match between Sunrisers Hyderabad Team and Pune Warriors India in Hyderabad on 5 April 2013. Prakash Raj, Allu Sirish, Yami Gautam, Radha Mohan, Singer Geeta Madhuri and Allu Aravind graced the event  The distribution rights were acquired by Vendhar Movies Madhan.

==Controversies== Dharmapuri caste violence and is supportive of honour killing, which was subsequently denied by director Radha Mohan: "The film is not based on the Dharmapuri incident.I request all those who are alleging to watch the movie first and then comment." A governmental intervention would be met by a special screening of the film. 

==Reception==
Gouravam opened to Mixed Response from the critics. While the underlining points of the film are content, performances of the young and senior actors and the message, the main drawback is lack of entertainment values. Nonetheless, people have praised Allu Sirish for choosing an off-beat film to make his debut as an actor rather than opting for commercial potboiler. 

The Hindu gave a review stating "A good premise, but the film fails to build it up to give us a riveting watch".  The Times of India gave a review stating "Gouravam is proof that you need more than good intentions to make a movie about a social evil. The film is a social drama that unfolds as a mystery but the catch is that unless you have been living under a rock, you would know its revelations even as its sets up its plot. And, what could have been a suspenseful drama, a whodunit of sorts, becomes a very predictable film that is only fitfully entertaining."  IndiaGlitz for the Tamil version gave a rating 3/5 terming it as "An Honourable attempt"  and for the Telugu version gave no rating and called it "Lousy and illiterate" 

APHerald.com completely panned the movie stating "Its not even worth watching once. Save your time and money skipping this movie.", giving a rating of 1/5.  123telugu.com gave a review of rating 2.75/5 stating "Gouravam is an intense and thought provoking film with a unique story. Allu Sirish has done a decent job with his debut."  way2movies.com gave a review stating "Powerful and thought-provoking story makes it worth a watch"  Oneindia Entertainment gave a review stating "Gouravam has an intense and unique subject, which provokes your thought. It also has got best performance from the lead actors. But slow and dragging narration may keep away a section of audience from the theatres. If you are mega family fan, you will never be their fan with Sirishs intense performance." 

==References==
 

==External links==
*  

 

 
 
 
 
 
 