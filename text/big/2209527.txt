Dillinger (1945 film)
{{Infobox film
| name           = Dillinger
| image          = Dillinger.jpg
| caption        = Theatrical release poster
| director       = Max Nosseck Frank and Maurice King
| writer         = William Castle (uncredited) Philip Yordan
| starring       =  Lawrence Tierney Edmund Lowe Anne Jeffreys Elisha Cook Jr. Eduardo Ciannelli
| music          = Dimitri Tiomkin
| cinematography = Jackson Rose
| editing        =
| distributor    =
| studio         = Monogram Pictures
| released       =  
| runtime        = 70 minutes
| country        = United States
| language       = English
| budget         = $65,000
| gross          = $4,000,000 (USA)
|
}}
Dillinger is a 1945 gangster film telling the story of John Dillinger. 
 You Only Live Once. The film was released on DVD by Warner Bros. for the Film Noir Classic Collections 2 in 2005 even though the film is generally regarded as not being film noir.  Some sequences were shot at Big Bear Lake, California.

==Awards==
Philip Yordan was nominated for the Academy Award for Writing Original Screenplay, earning Monogram Pictures its only nomination.

==Plot summary==

The story begins with a newsreel summing up the gangster life of John Dillinger in detail. At the end of the newsreel, Dillingers father (Victor Kilian) walks onto the stage and speaks to the movie audience about his sons childhood back in Indiana. He talks of John’s childhood as having been ordinary and not very eventful, but concedes that his son had ambitions and wanted to go his own way. The young Dillinger left his childhood town to find his fortune in Indianapolis, but soon ran out of money. The scene fades to a restaurant, where he is on a date and finds himself humiliated by the waiter who refuses to accept a check for the meal; unable to pay for the meal, he excuses himself, runs into a nearby grocery store and robs it for  $7.20 in cash. He makes the clerk at the store believe he has a gun in his hand under the jacket. 

John is soon arrested for this felony, and he is sentenced to prison. When incarcerated he finds new acquaintances and becomes good friends with his cell mate, Specs Green. Specs is an infamous bank robber and has a gang of his own, Marco Minnelli, Doc Madison and Kirk Otto, who are also in the same prison. John is impressed by Specs and his experience and intelligence, and he begins to look up to him as a kind of father figure. 

Since John has a much shorter sentence he decides to be the gang’s help on the outside when he is released, and facilitates their escape. As soon as John is free, he resumes his criminal career by holding up a movie theater box office. Before he does, he manages to flirt with the female clerk, Helen Rogers, with the result that she refuses to identify him in the subsequent line-up after the robbery, even though she recognizes him from a police photo. Instead she goes on a date with John.

John continues his criminal spree of robberies for money to finance the escape of Specs gang. When he has enough, he devises a plan to smuggle a barrel of firearms to the gang at their quarry job site. The plan succeeds, and the gang manage to escape. They include John in their gang and start a crime wave, committing a long series of robberies in the American Midwest. 

Specs sends John to scout for new targets because he is the only one not recognized by the witnesses at the quarry at the time of the gang’s escape. John gets to check out the Farmer’s Trust Bank. He goes to the bank and poses as a potential customer to get inside the office. He reports back to the gang about the security system, and tells them it’s very sophisticated; too sophisticated. 

Specs still wants to hit the bank, and getting tired of John’s ego and trigger happiness he decides to get help from outside the gang. John suggests another way to get into the bank - with gas bombs. John convinces the rest of the gang of his way and they rob the bank. When they arrive at the hideout, John demands Specs double share of the loot. After John is captured but escapes from jail he kills Specs and takes his place as the leader of the gang. Running low on cash, they decide to rob a mail train. In the process gang member Otto Kirk is killed. 

The gang part for a few weeks, to lay low for a while, and John and Helen go on a big shopping spree. They meet up with the rest of the gang at a cabin lodge owned by the Otto’s, Kirk’s relatives. They stay there for a while; when the Kirks call the police Dillinger kills them; later they realize that the police are closing in on them, so they plan to head West and start a new series of bank robberies.  Nearly broke, Dillinger and his girlfriend decide to watch a movie at the Biograph theater which is playing Manhattan Melodrama and a Mickey Mouse cartoon Mickeys Gala Premiere showing "Gallopin Romance". Coming out of the theater, Dillinger sees the police coming after him. In a gunfight he is killed in an alley, his only money is $7.20-the same amount from his first robbery.

==Cast==
*Lawrence Tierney as John Dillinger
*Edmund Lowe as Specs Green
*Anne Jeffreys as Helen Rogers
*Eduardo Ciannelli as Marco Minelli (as Eduardo Cianelli)
*Marc Lawrence as Doc Madison
*Elisha Cook, Jr. as Kirk Otto Ralph Lewis as Tony
*Elsa Janssen as Mrs. Otto
*Ludwig Stössel as Mr. Otto
*Constance Worth as Blonde

==See also==
* List of American films of 1945

==External links==
 
* 
* 
* 

==References==
 

 

 
 
 
 
 
 
 
 
 
 