Jamboree (1957 film)
 
{{Infobox film
| name           = Jamboree
| image          = Jamboree23.jpg
| image_size     = 225px
| caption        = Mexican movie poster
| director       = Roy Lockwood
| producer       = Max Rosenberg Milton Subotsky
| writer         = Leonard Kantor Milton Subotsky
| narrator       =
| starring       = Dick Clark Frankie Avalon Fats Domino Charlie Gracie Buddy Knox Jerry Lee Lewis Carl Perkins Slim Whitman
| music          = Neal Hefti
| cinematography = Jack Etra
| editing        = Robert Broekman
| company        = Vanguard Pictures Warner Brothers
| released       =  
| runtime        = 71 minutes
| country        = United States
| language       = English
| budget         =
}}

Jamboree, known as Disc Jockey Jamboree in the United Kingdom, is the name of a black and white 1957 rock and roll film, directed by Roy Lockwood. Its story is about a boy and girl, Pete Porter and Honey Wynn, played by Paul Carr and Freda Holloway, who become overnight sensations as a romantic singing duo who run into trouble when their squabbling managers, played by Kay Medford and Bob Pastine, try to turn them into solo acts. Against this backdrop in cameo performances appear some of the biggest names of rock and roll in the 1950s lip-syncing to their recordings.

==Overview== Radio Luxembourg WINS in Rock Around Congressional Hearings into payola practices and radio broadcasting eventually ruined Freeds career, while Clarks career continued uninterrupted.

Jamboree was essentially a music film in the manner of music videos that followed many years later on MTV where the storyline was secondary to the musical performances, with the amateurish acting taking a definite back seat to the musical performances. However, this movie is of historical importance due to the cameo performances by various musical acts that are featured.

==Featured stars== Jack Jackson Decca records Radio Luxembourg; Jack Payne Glad All Joe Williams on vocals. Connie Francis overdubbed her vocals for Freda Holloway.

Brazilian singer Cauby Peixoto has a cameo appearance in the film under the name "Ron Coby". Cauby had a brief rock and roll phase is in his career, recording "RocknRoll em Copacabana".

  (second from left) performing "Glad All Over" with (left to right) Clayton Perkins, W.S. "Fluke" Holland, and Jay Perkins]]

The films premise for Perkins performance, during which he plays a Gibson Les Paul Goldtop with a Bigsby tremolo bar, which ostensibly occurs in a studio used by "Pop Records", is that he is "cutting (a record) at 2:30. He rarely ever uses all of his time."  Pete and Honey, "Americas Sweethearts", are to use Perkins unused time. The Carl Perkins band consisted of Carl Perkins on vocals and lead guitar, Jay Perkins on rhythm guitar, Clayton Perkins on upright bass guitar, and W.S. "Fluke" Holland on drums. Perkins was initially reluctant to appear in the film, but did so for the $1,000 it would earn him. He was given the choice of performing "Glad All Over" or "Great Balls of Fire", and thought "both of em was junk",  but performed "Glad All Over", leaving "Great Balls of Fire" for Jerry Lee Lewis. Released as a single in November along with the movie, Jerry Lee Lewis "Great Balls of Fire" was number 2 on the national charts for four weeks. Perkins "Glad All Over" sank "without fanfare".  "Glad All Over" was recorded by The Beatles, however, who performed the song in their concerts and first on the BBC program Pop Go The Beatles on August 20, 1963.  The Beatles recorded a second version of "Glad All Over" for the Saturday Club program at Londons Playhouse Theatre, which was broadcast on August 24, 1963. George Harrison and Paul McCartney sang the lead vocals. The Beatles Pop Go The Beatles version appeared on the   sang lead on "Glad All Over" when it was performed on the 1985 HBO/Cinemax cable concert special " ". "Glad All Over" was more suitable to Carl Perkins style than "Great Balls of Fire".  "Glad All Over" backed with "Lend Me Your Comb" would be the last single released on Sun Records by Carl Perkins in 1957. In an ironic twist, Carl Perkins "Glad All Over" became one of the most well-known and most influential songs to emerge from the movie. 
 

Dick Clark is the host of the "second hour" of a "United Charities"  ) in London, England; Werner Goetze (Bayerischer Rundfunk) Munich, and Chris Howland (Westdeutscher Rundfunk) Cologne, Germany are shown introducing "Pete and Honey" records on the air. Finally, performances are the entertainment at a "Music Operators" convention supper. Music Operators of America was an influential group of jukebox owners. In 1950 there were fifty-five hundred jukebox operators servicing four hundred thousand jukeboxs in the US. They bought 150 records a week, while the average phonograph owner bought fewer than 10 per year. 

==References==
 

==External links==
* 

 
 
 
 