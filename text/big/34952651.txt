Ein Shams (film)
{{Infobox film
| name           = Ein Shams
| image          =
| caption        =
| director       = Ibrahim El-Batout
| producer       = Ein Shams Films
| writer         =
| starring       = Hanan Youssef Ramadan Khater Hanan Adel Samar Abdel Wahab Mariam Abodouma
| distributor    =
| released       =  
| runtime        = 90 minutes
| country        = Egypt Morocco
| language       =
| budget         =
| gross          =
| screenplay     = Ibrahim El-Batout Tamer El Said
| cinematography = Hesham Farouk Ibrahim
| sound          = Mohab Mostafa Ezz
| editing        = Ahmad Abdalla
| music          = Amir Khalaf
}}

Ein Shams  is a 2007 film.

== Synopsis == Pharaonic era and a sacred location marked by the visit of Jesus and the Virgin Mary, Ein Shams has become one of Cairos poorest and most neglected neighbourhoods. Through the eyes of Shams, an eleven-year-old girl who lives in this neighbourhood, the film captures the sadness and magic that envelops everyday life in Egypt. In a series of heart-rending events, the diverse characters of the film showcase the intricacies of Egypts political system and social structure, and give a glimpse into the grievances of the Middle East region and the complex relationships of its nations.

== Awards ==
* Taormina 2008 
* Cine árabe Róterdam 2008 
* Cartago 2008

== References ==
 
*  

 
 
 


 