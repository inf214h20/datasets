Hearts in Dixie (film)
{{Infobox film
| name           = Hearts In Dixie
| image          = Hearts_in_dixie.jpg Paul Sloane
| writer         = Walter Weems
| starring       = Stepin Fetchit Clarence Muse Eugene Jackson Bernice Pilot Fox Film Corporation
| released       =  
| runtime        = 71 minutes
| producer       =
| music          =
| cinematography =
| editing        =
| country        = United States
| language       = English
| budget         =
|}} Paul Sloane. William Fox was producer. 

==Synopsis==
Hearts in Dixie unfolds as a series of sketches of life among American blacks. It featured characters with dignity, who took action on their own, and who were not slaves.  The plot focuses on Grandfather Nappus (Clarence Muse), his daughter, Chloe (Bernice Pilot), her young son, Chinaquapin (Eugene Jackson), and her husband, Gummy (Stepin Fetchit). To make certain his grandson Chinaquapin does not end up like his father or become tainted by the superstitions that dominate the community, the grandfather decides to send the boy away. One particularly tender scene shows Nappus love for his grandson, whom he sends North for schooling. The film ends with the youngsters departure aboard a riverboat. 

==Cast==
*Stepin Fetchit: Gummy
*Clarence Muse: Nappus
*Eugene Jackson: Chinquapin
*Bernice Pilot: Chloe
*Clifford Ingram: Rammey
*Mildred Washington: Trallia
*Zack Williams: Deacon
*Gertrude Howard: Emmy Dorothy Morrison: Melia
*Vivian Smith: Violet
*A.C.H. Billbrew: Voodoo Woman
*Richard Carlyle: White Doctor
*The Billbrew Chorus

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 


 