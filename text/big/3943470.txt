Two Much
{{Infobox Film
| name           = Two Much
| image          = Two-Much film.jpg
| caption        = DVD cover
| director       = Fernando Trueba
| producer       = Cristina Huete
| writer         = Fernando Trueba David Trueba Le Jumeau written by Élisabeth Rappeneau
| starring       = Antonio Banderas Melanie Griffith Daryl Hannah Danny Aiello Joan Cusack Eli Wallach
| music          = Michel Camilo
| cinematography = José Luis Alcaine
| editing        = Nena Bernard Fernando Trueba Producciones Cinematográficas S.A. Sogetel Buena Vista Pictures (US) Metro-Goldwyn-Mayer (non-US)
| released       = December 1, 1995 (Spain) March 15, 1996 (United States|U.S.)
| runtime        = 118 minutes
| country        = United States Spain
| language       = English
| budget         = $24 million
| gross          = $1,141,556
}} romantic screwball French comedy Le Jumeau, which was also based on Westlakes novel. Directed by Fernando Trueba, Two Much stars Antonio Banderas, Melanie Griffith, Daryl Hannah and Danny Aiello. It was released in the United States by Touchstone Pictures and Metro-Goldwyn-Mayer in Other Countries. Lew Soloff performed music for the film.

==Plot==
Art Dodge (Antonio Banderas), a former artist, is struggling to make ends meet with his art gallery, ignoring bills and delaying to pay his assistant Gloria (Joan Cusack) and his artist Manny (Gabino Diego). To survive, he is reading the obituaries and trying to convince the widows that the deceased purchased a painting shortly before dying.

Things take an ugly turn when Art is trying this scam with mobster Gene (Danny Aiello) whose father just died. Not only does Gene not fall for it, but he tries to have his henchmen beat Art up. Art barely escapes by hiding in the Rolls Royce of Betty Kerner (Melanie Griffith), Genes estranged two-time ex-wife and wealthy heiress. Betty is excited about helping the handsome stranger, and the two end up shortly thereafter making love. Betty being very impulsive, she wants to marry Art in two weeks. Because of the heiress fortune the news immediately makes the tabloids. Stuck between Betty who wont change her mind and Gene who still loves his ex-wife, Art doesnt like the idea of getting married with such short notice but decides to play along for now.

One morning, at Bettys mansion, Art seductively enters her shower naked, only to realize its not Betty whos in there but her sister Liz (Daryl Hannah), an art professor. If Art is attracted to Liz, she stays very cold and distant, seeing him as nothing more than a gigolo who hit the jackpot. Art decides to invent a fake twin brother Bart (who wears glasses and has his hair down instead of wearing a ponytail) who is allegedly a painter who just got back from Italy. Bart and Liz instantly hit it off while Gene still tries to romance Betty. Bart and Liz cant stop talking about everything, he plays with her dog and even invites her to Mannys studio when hes not in, pretending to show her his art. When Lizs favorite painting in the studio turns out to have been actually made by Art (who gave it to Manny as an "advance" on what he owes him), Bart gives her the painting.

Thanks to his imaginary twin brother, Art manages to pursue a romance with both sisters. Because the two "twin brothers" must never be in the same place at the same time, it however involves a lot of running around, coming up with a lot of excuses and enlisting a very reluctant Glorias help. One evening he needs to go out two separate dates with both Betty and Liz. At the restaurant with Betty, he decides to drug her wine, much to the horror of the sommelier (Vincent Schiavelli). This allows him to cut the date short and put a very sleepy Betty to bed. He then goes out with Liz (who chooses the very same restaurant) and ends up making love to her. The next morning, Art/Bart has to run back and forth between the two sisters bedrooms (whose two bathrooms share a private swimming pool) as hes supposed to be with them both at the same time.

In the evening before the wedding, Art spots Genes two henchmen around his house and manages to escape them thanks to his dads help (Eli Wallach). He tries to spend the night at Glorias but he discovers she started dating Manny. Manny however gives him the keys to his studio where he can spend the night. At the studio, Art starts to paint again when he is interrupted by Genes henchmen who found him and start beating him up, before Gene shows up. When Art proposes Gene to leave town, Gene tells him to go ahead with the wedding and threatens to break one bone for each tear Betty cries. After they leave, Liz arrives to the studio, thinking Bart got beat up. When Bart tells her he is Art, that he fell in love when he saw her in the shower and tries to kiss her, Liz thinks Art is trying to make a pass at her, not realizing Bart doesnt exist.

On the wedding day, Liz tells Bart his "brother" tried to kiss her, and that the wedding should be called off. Bart needs to "confront" Art in a study alone, with Liz and Gene listening outside -and, unbeknownst to anyone, also by Betty through the phone. When Gene enters the study, he confronts a lonely Art, and again threatens him if he doesnt marry Betty. He tells him that what Art or even himself want is irrelevant, and that the only thing that matters is Bettys happiness. During the wedding ceremony, Betty, shaken by Genes selfless devotion, calls the wedding off and falls in Genes arms acknowledging she still love him too. Gene and Betty elope. In the general confusion, Liz sees her dog wanting to play with Art and realizes Art and Bart are the same person. Bart then go see Liz, telling her a fake excuse to "go back to Italy" (which she of course doesnt buy), adding hes not worthy of her.

A few month later, Arts gallery has experienced a dramatic turnaround (Gloria owns and manages it, and Art is the artist) and is now very successful. At the inauguration of his work, Art notices Liz who still has feeling for him but is not sure who he is really. Art manages to convince her he is the one she had feelings for, and the movie ends with the two happily walking in the street, hand in hand.

==Cast==
* Antonio Banderas as Art Dodge, "Bart Dodge"
* Melanie Griffith as Betty Kerner
* Daryl Hannah as Liz Kerner
* Danny Aiello as Gene
* Joan Cusack as Gloria
* Eli Wallach as Sheldon Dodge
* Gabino Diego as Mane
* Austin Pendleton as Dr. Huffeyer
* Allan Rich as Reverend Larrabee
* Vincent Schiavelli as Sommelier
* Phil Leeds as Lincoln Brigade
* Sid Raymond as Lincoln Brigade

==Reception==
The film received negative reviews from critics and currently holds a 14% rating on Rotten Tomatoes.

==Awards== Golden Raspberry Award nominations for theirs as both Worst Actress and Worst Supporting Actress respectively.

==Other==
* Banderas and Griffiths on-screen relationship also developed off-screen, leading to their 1996 marriage.
* This movie is the remake of a French movie called Le jumeau ("The twin"). It borrows the main plot of a man struggling with bills, inventing a fake twin to seduce two wealthy sisters, as well as the "morning after", need-to-wake-up-next-to-both-sisters scene. Tamil as Naam Iruvar Namakku Iruvar (1998) with Prabhu Deva playing the lead role.

== External links ==
*  
*  
*  
*  
*   at antoniobanderasfans.com

 

 
 
 
 
 
 
 
 
 
 
 