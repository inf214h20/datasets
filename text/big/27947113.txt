The Divine Emma
 
{{Infobox film
| name           = The Divine Emma
| image          = Bozska Ema.jpg
| alt            = 
| caption        = Theatrical release poster
| film name      = {{Infobox name module
| original       = Božská Ema}}
| director       = Jiří Krejčík Jan Syrový
| writer         = 
| screenplay     = Zdeněk Mahler
| starring       = Božidara Turzonovová
| music          = Zdeněk Liška
| cinematography = Miroslav Ondříček
| editing        = Miroslav Hájek
| studio         = Barrandov Studios
| distributor    = 
| released       =  
| runtime        = 110 minutes Czechoslovakia
| Czech
| budget         = 
| gross          = 
}} Czech drama Best Foreign Language Film at the 54th Academy Awards, but was not accepted as a nominee. 

==Plot== USA in 1914, and her subsequent involvement in the Czech patriotic resistance against Austria-Hungary during the WWI.

==Cast==
* Božidara Turzonovová - Emmy Destinn (sung by Gabriela Beňačková)
* Juraj Kukura - Victor
* Miloš Kopecký - Samuel
* Jiří Adamíra - Colonel
* Václav Lohniský - Train dispatcher

==See also==
* List of submissions to the 54th Academy Awards for Best Foreign Language Film
* List of Czechoslovak submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 