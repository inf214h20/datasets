The Thin Line
 
{{Infobox film
| name           = The Thin Line
| image          = 
| caption        = 
| director       = Michal Bat-Adam
| producer       = 
| writer         = Michal Bat-Adam
| starring       = Gila Almagor
| music          = 
| cinematography = Nurith Aviv
| editing        = 
| distributor    = 
| released       =  
| runtime        = 94 minutes
| country        = Israel
| language       = Hebrew
| budget         = 
}}
 Best Foreign Language Film at the 53rd Academy Awards, but was not accepted as a nominee. 

==Cast==
* Gila Almagor as Paula
* Alexander Peleg as Nadav (as Alex Peled)
* Liat Pansky as Nili
* Aya Veirov as Maya
* Avner Hizkiyahu as Dr. Greber
* Svetlana Mazovetskaya as Dressmaker (as S. Mazovetzkaya)
* S. Greenspan as Degarit
* Kina L. Hanegbi as Bina
* Irit Mohr-Alter as Zila
* Miri Fabian as Nurse

==See also==
* List of submissions to the 53rd Academy Awards for Best Foreign Language Film
* List of Israeli submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 