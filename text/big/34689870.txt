I Don't Want to Go Back Alone
{{Infobox film
| name           = I Dont Want to Go Back Alone
| image          = I_Dont_Want_to_Go_Back_Alone.jpg
| image size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Daniel Ribeiro
| producer       = {{plainlist |
* Diana Almeida
* Daniel Ribeiro
}} 
| writer         = Daniel Ribeiro
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = 
| music          = {{plainlist |
* Juliano Polimeno
* Tatá Aeroplano
}}
| cinematography = Pierre de Kerchove	 
| editing        = Cristian Chinen	 	
| studio         = Lacuna Films
| distributor    = 
| released       =   
| runtime        = 17 minutes
| country        = Brazil
| language       = Portuguese
| budget         = BRL 80,000 
| gross          = 
}}
 
I Dont Want to Go Back Alone ( ) is a 2010 Brazilian short film directed by Daniel Ribeiro. The short won the 2011 Iris Prize.

==Plot==
Leonardo is a 15 year-old blind student with his one friend in class, Giovana, sitting next to him. Seated behind Leonardo is the new student Gabriel. After class Giovana invites Gabriel to walk home with herself and Leonardo; she customarily links arms with him for support even though his house is further from the school than hers. Later Giovana teases Leonardo about never confiding in her about romance and suggests he receive math tutoring from Gabriel. Over time the three grow closer, walking home and playing games together. Leonardo becomes more self-conscious about appearance, asking questions about what he and Gabriel look like. Gabriel volunteers to take over escorting Leonardo so Giovana doesnt have to backtrack, though she possessively says thats not necessary.

A school project requires same-sex pairs, leading Leonardo to work with Gabriel instead of Giovana. During a more serious conversation about blindness, Gabriel points out Giovanas attraction to Leonardo, but Leonardo says he does not reciprocate. When they arrive at his house, Leonardo changes shirts in front of Gabriel, who is stunned before removing his own sweatshirt. Gabriel tells Leonardo he left his sweatshirt at Leonardos house but must leave school early for a dentists appointment. After the other students have left, Leonardo admits to Giovana he is in love with Gabriel. Doubtful about the homosexual romance, Giovana does not provide a positive response before a phone call summons her to her grandmothers birthday, leaving Leonardo to walk home alone with a white cane. When he hears someone come in the house, he chastises Giovana for leaving him and expresses his doubts about confessing his love for Gabriel; however, the visitor is actually Gabriel, who silently kisses him on the lips and leaves with his sweatshirt. Later Giovana arrives apologizing for taking so long; Leonardo discovers the sweatshirt is gone and smiles at the realization Gabriel was the one who kissed him.

==Cast==
*Guilherme Lobo as Leonardo
*Fabio Audi as Gabriel
*Tess Amorim as Giovana

==Awards and accolades==
{|class="wikitable" border="1" align="centre"
|-
! Film Festival !! Category !! Year !! Outcome
|- Outfest [http://www.outfest.org/fest2011/awards.html Outstanding Dramatic Short Film || 2011 || 
|- Iris Prize Iris Prize || 2011 || 
|- Cork Film OutLook Award for Best LGBT Short Film||2011|| 
|- Reel Affirmations Best Male Short || 2011 || 
|}

==Feature film==
 
In December 2012, the films official Twitter announced that a feature film based on the short film, titled Todas as Coisas mais Simples", was in production, after over two years of fundraising and preparation of the script. Filming began and completed in early 2013. The film premiered in 2014.   2013-01-28. 

On September 2013, director Daniel Ribeiro, announced the final title would be "Hoje Eu Quero Voltar Sozinho", which translates to "Today I Want to Go Back Alone". Ribeiro reported, "Throughout the development of the movie a new plot element arose: Leonardos independence. Its a naturally common theme in any teens life, but specially in Leonardos as, being blind, hes often overprotected by those around him, and he wants to do some things on his own, without depending on others. Therefore, the original title was no longer valid, after all "going back alone" became one of the main characters objectives."

The official English title of the film will be "The Way He Looks".

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 