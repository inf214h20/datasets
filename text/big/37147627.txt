Vellachi
 
{{Infobox film
| name           = Vellachi
| image          = Vellachi still on the movie sets.jpg
| alt            =  
| caption        = Vellachi still on the movie sets
| director       = Velu Viswanath
| producer       = K. Ananthan
| writer         = Velu Viswanath
| starring       =  
| music          = Bhavatharini
| cinematography = Sai Natraj
| editing        = Hari Palanivel
| studio         = Prasad Studios
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Tamil
| budget         = 
| gross          =
}} Tamil film directed by debut director Velu Viswanath featuring Pintu and Suchitra Unni in the lead roles. This film is a village based script, completely filmed in Vellore district.

==Cast==
* Pintu as Ganesh
* Suchitra Unni as Vellachi
* Ganja Karuppu
* Krishnamoorthy
* Sevvalai

==Production==
Produced by Geethalaaya Movies, Ilayaraja’s daughter Bhavatharini is wielding the music baton after a long time. Bhavatharini has already composed music for few films. She is now scoring the tunes for Vellachi, which will be directed by debut director Velu Vishwanath. The shoot went on floor by June 2012, and post-production works are under progress. The movie stars Pintu Pandu, son of comical actor Pandu. Suchitra Unni and Ganja karuppu are the other leads in the movie. Cinematography is by Sai Natraj, while editing is by Hari Palanivel.

==Soundtrack==
{{Infobox album
| Name = Vellachi
| Longtype =
| Type = Soundtrack
| Artist = Bhavatharini
| Cover = 
| Released = 5 December 2012
| Recorded =  Feature film soundtrack
| Length =  Tamil
| Label = 
| Producer = Bhavatharini
| Reviews =
| Last album = 
| This album = Vellachi (2012)
| Next album = 
}}
{{Track listing extra_column = Singer(s) music_credits = no all_lyrics = Na. Muthukumar

|title1= Machango Mamango
|extra1= Velmurugan
|length1= 3:47
|title2= Adi Sirukki
|extra2= Haricharan, Manikka Vinayagam
|length2= 5:03
|title3= Poiya Pochey Enkadhal
|extra3= Yuvan Shankar Raja
|note3= 
|length3= 4:17
|title4= Ennadi Penney
|extra4= M. L. R. Karthikeyan
|length4= 4:12
}}

== References ==
 
==External links==
*  
* 

 
 
 
 


 