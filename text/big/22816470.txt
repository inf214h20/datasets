Wooden Staircase
{{Infobox film name = Wooden Staircase image =Wooden_Staircase.jpg image_size = caption = Final Scene of the film director = Vidas Rasinskas producer = Arunas Stoskus Vidas Rasinskas Sigitas Puckorius writer = Vidas Rasinskas Alvydas Bausys narrator = starring = Evaldas Jaras Gabrielle Odinis Virginija Kelmelyte Dalia Melenaite Vaclovas Bledis music = Gustav Mahler Faustas Latenas cinematography = Jonas Tamasevicius sound = Romualdas Federavicius editing = Vanda Surviliene Liudmila Volkova distributor = released = October 13th, 1993 runtime = 93 min. version 56 min. version country = Lithuania Russia language = Lithuanian budget = gross = }} 
Wooden Staircase ( ,  ) is a feature film of Lithuanian-Canadian screenwriter and film director Vidas Rasinskas.
 
==Plot summary==
Wooden Staircase is a poetic drama of Time and Fate, a metaphor, leading to introspection. Two young people, connected through Time and Fate, are looking for consensus and comprehension between themselves. There is a mystic protagonist, Thomas. He embodies the famous writer Thomas Mann’s spirit, living beyond the time. Thomas lets the young protagonist, Vilius, go back to his past and compels him to face the results of his former mistakes. A rendezvous with the past has a large influence on Vilius present.

== Recognition ==
The premiere of Wooden Staircase, (93 min. version) was at the International Film Festival Cottbus, Germany in 1993. 
The film Wooden Staircase, (56 min. version) won the competition in Moscow and was selected as the best student work of the year. It was presented for the 22nd Student Academy Award in 1995.

== References ==
*Grazina Arlickaite, Lithuanian Films 1990-1998 (Lietuvių filmai 1990-1998), Lithuania, 1998 
*Astrida Baranovskaya, Stairs, K Ekranu, Russia, 1994
*Piotr Niemiec, Die holzerne Treppe, Germany, 1993
*Virginija Januseviciene, Staircase Lead to the World of Metaphors, Lietuvos rytas, Lithuania, 1993
*Richard Miller, 22nd Student Academy Awards, USA, 1995

== External links ==
* 
* 
* 
* 
* 

 
 
 

 