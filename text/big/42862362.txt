The Measure of All Things
{{Infobox film
| name           = The Measure of All Things
| image          = TMAT poster.jpg
| alt            = 
| caption        = Film poster
| director       = Sam Green Ymusic
| producer       = 
| writer         = 
| screenplay     = 
| starring       = 
| music          = Mark Dancigers
| cinematography = Andrew Black Peter Sillen
| editing        = 
| studio         = ArKtype C41
| distributor    = 
| released       =  
| runtime        = 65 minutes
| country        = United States
| language       = English 
}}
The Measure of All Things is a 2014 documentary film co-directed by Sam Green and Ymusic.   The film had its premiere at the 2014 Sundance Film Festival on January 20, 2014. 

==Synopsis== Guinness Book of World Records.
  
==Reception==
The Measure of All Things received positive reviews from critics. Dennis Lim in his review for The New York Times said that "(it is) a singular experience, and a collective one, with the potential for human connection and human error."  Susan Gerhard  of Fandor, praised the film by saying that "Sam Green has long taken joy at unpacking utopian promises of the past, many deluded, others inspired, and offering them up for re-evaluation and appreciation." 

==References==
 

==External links==
*  
*  
*  

 
 
 
 