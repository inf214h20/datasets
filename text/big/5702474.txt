Friends & Lovers (film)
{{Infobox Film name           = Friends & Lovers image          = Friends and Lovers DVD cover.jpg caption        =  director  George Haas producer       = Josi W. Konski writer         =  George Haas	  story          = Neill Barry George Haas starring       = Stephen Baldwin Claudia Schiffer Robert Downey Jr. music          = Emilio Kauderer cinematography = Carlos Montaner editing        = Barry B. Leirer studio         = Laguna Entertainment distributor    = Lions Gate Entertainment released       =   runtime        = 103 minutes country        = United States language       = English budget         =  gross          = $33,543 
}}
 George Haas twentysomethings on a ski trip. It stars Stephen Baldwin, Claudia Schiffer and Robert Downey, Jr..

==Plot==
As Christmas season approaches, Ian is invited by his father, Richie, to join him for a skiing holiday in Park City, Utah. Ian convinces several of his friends to come along. There is Jon, who brings along his German girlfriend, Carla, but has competition for her from a German ski instructor, Hans. Another friend, David, is gay and wants to lose his virginity. Keaton discovers his sister Jane is pregnant and has no plans to tell the man who might be the father. Keaton also has issues with his friend, Lisa, who wants their relationship to become romantic.

==Cast==
* Stephen Baldwin as Jon
* Danny Nucci as Dave
* George Newbern as Ian Wickham
* Alison Eastwood as Lisa
* Claudia Schiffer as Carla
* Robert Downey Jr. as Hans
* Neill Barry as Keaton McCarthy
* Suzanne Cryer as Jane McCarthy
* David Rasche as Richard "Richie" Wickham
* Ann Magnuson as Katherine Leon as Tyrell

==Critical reaction==
The film has overwhelmingly negative reviews. Rotten Tomatoes reported that 7% of critics gave positive reviews based on 15 reviews (1 "Fresh", 14 "Rotten") with an average score of 2.2/10,  while Roger Ebert considered it not just an example of a bad film, giving it a half star, but incompetent filmmaking,  going so far as to suggest in his oral review that it could be shown in film class as an example of what not to do.  

==Soundtrack==
The soundtrack to Friends & Lovers was released on April 20, 1999.

{{Track listing
| collapsed       = no
| extra_column    = Artist
| total_length    = 47:20 

| title1          = Friends and Lovers
| length1         = 3:34
| extra1          = Ali Olmo

| title2          = Too Shy
| length2         = 2:24
| extra2          = Mike Riojas

| title3          = Cant Keep Still
| length3         = 2:48
| extra3          = Mike Riojas

| title4          = Stuck in the Middle
| length4         = 4:08
| extra4          = Orphan

| title5          = Anything for You
| length5         = 3:26
| extra5          = Ali Olmo

| title6          = You Belong to the Moon
| length6         = 3:41
| extra6          = Orphan

| title7          = Someone Like You
| length7         = 2:58
| extra7          = Mike Riojas

| title8          = Father and Son
| length8         = 2:39
| extra8          = Peter Tomashek

| title9          = Jacuzzi
| length9         = 2:24
| extra9          = Peter Tomashek

| title10         = David, and Manny
| length10        = 1:24
| extra10         = Peter Tomashek

| title11         = Hallways and Stairs
| length11        = 1:34
| extra11         = Peter Tomashek

| title12         = Meditation
| length12        = 1:14
| extra12         = Peter Tomashek

| title13         = Hans Theme
| length13        = 0:47
| extra13         = Peter Tomashek

| title14         = Getting Ready
| length14        = 2:33
| extra14         = Peter Tomashek

| title15         = Keaton and Jane
| length15        = 2:20
| extra15         = Peter Tomashek

| title16         = Bedroom Bug
| length16        = 1:13
| extra16         = Peter Tomashek

| title17         = Country Motel
| length17        = 1:20
| extra17         = Peter Tomashek

| title18         = Everything Bout Sex
| length18        = 1:19
| extra18         = Peter Tomashek

| title19         = Morning Confessions
| length19        = 4:02
| extra19         = Peter Tomashek

| title20         = Pointed Head
| length20        = 0:59
| extra20         = Peter Tomashek

}}

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 