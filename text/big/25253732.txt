King of Bollywood
 
 
{{Infobox film
| name           = King of Bollywood
| image          =King_of_Bollywood_(film).jpg
| caption        = 
| director       = Piyush Jha
| producer       =
| writer         = Piyush Jha, Deepa Gahlot  
| starring       = Om Puri, Sophie Dahl, Murli Sharma
| music          = Smoke
| cinematography =
| editing        =
| distributor    =
| released       =  
| runtime        =
| country        = United Kingdom India
| language       = Hindi English
| budget         =
| gross          =
}} Hindi film industry.

==Plot==
British journalist Crystal Chaurasia decides to make a documentary about a faded Bollywood star of yesteryear Karan Kumar. She follows KK as he tries to make his comeback with a new move. Meanwhile his son Rahul disapproves of his efforts, but he increasingly comes to like Crystal.

==Cast==
* Om Puri as Karan Kumar ("KK")
* Sophie Dahl as Crystal Chaurasia
* Diwakar Pundir as Rahul
* Kavita Kapoor as Mandira Kumar
* Manoj Pahwa as Ratnesh, Karan Kumars secretary
* Murli Sharma as Sunny

==Release==
The film released worldwide on September 24, 2004.

==Music==
The music was composed by Smoke while Piyush Jha and Sudhakar Sharma wrote the lyrics. Kay Kay)
# "King Of Bollywood" (Chetan Shashital)
# "Tu Hai Sehari Babu, Babu (Shreya Ghoshal, Shaan (singer)|Shaan)
# "Road Dancer, Road Dancer" (Kunal Ganjawala)

==References==
 

==External links==
*   at Bollywood Hungama

 
 
 
 
 