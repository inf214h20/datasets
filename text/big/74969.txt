Multiple Sidosis
 
Multiple Sidosis (styled Multiple SIDosis) is a 1970 short film in which a single performer creates an entire multi-part performance of the song "Nola". It is an example of a kind of one-man band|one-man-band musical performance.
 overdubbed and visually composited to create the final piece.

The overdubbing technique has been used before and since in professional recording studios, to allow a single performer to create an entire multi-instrument song. Digital technology has made the technique much easier for amateurs to employ today, but no such labour-saving devices were available to Laverents.

In 2000, Multiple SIDosis was selected for preservation in the United States National Film Registry by the Library of Congress as being "culturally, historically, or aesthetically significant".

==External links==
*  
*  

 
 
 

 