War of the Planets (1966 film)
{{Infobox film
| name           = War of the Planets
| image          = War of the Planets 1966 Italian poster.png
| image size     = 235px
| alt            = 
| caption        = Italian theatrical release posted
| director       = Antonio Margheriti (as "Anthony Dawson")
| producer       = Joseph Fryd Antonio Margheriti
| writer         = 
| screenplay     = Ivan Reiner Renato Moretti
| story          = Ivan Reiner Renato Moretti
| based on       =  
| narrator       = 
| starring       = {{plainlist|
*Tony Russel
*Lisa Gastoni
*Franco Nero
*Carlo Giustini (as "Charles Justin")
*Enzo Fiermonte
*Linda Sini
}}
| music          = Angelo Francesco Lavagnino|A.F. Lavagnino Richard Pallton Angel Coly
| studio         = Mercury Film International Southern Cross Films
| distributor    = 
| released       =   (Italy)
| runtime        = 105 minutes
| country        = Italy Italian
| budget         = 
| gross          =
}}
 Italian film directed by Antonio Margheriti and starring Tony Russel and Lisa Gastoni, released theatrically in Europe in 1966 in film|1966. (Russels name is misspelled in the opening credits as "Tony Russell".) The films story and screenplay were written by Ivan Reiner and Renato Moretti.

This is the second film in the Gamma-One series. It follows Wild, Wild Planet (aka I Criminali della Galassia ("Criminals of the Galaxy")) and precedes War Between the Planets (aka Il Pianeta Errante ("Planet on the Prowl")) and Snow Devils (aka La Morte Viene dal Pianeta Aytin ("Death Comes From The Planet Aytin")).

==Plot==
The film begins on New Years Eve in the middle of the 21st century.  After space station Alpha-Two reports impossible "negative radiation" readings and loses contact with United Democracies (U.D.) headquarters, Captain Tice and his crew are sent to investigate. They find the Alpha-Two crew immobilized, some dead, before coming under attack themselves by green glowing energy beings. The creatures immobilize Tices team and the space station disappears entirely. On space station Gamma-One, Commander Halstead sends ships to investigate the remaining space stations and evacuates all but a skeleton crew. Meanwhile on Earth, the creatures have possessed Captain Dubois and use him to break into the Institute for Advanced Sciencess nuclear reactor. The possessed Dubois sends the U.D. a message offering "symbiotic partnership" for "the good of the whole".

As the energy beings seize each of the stations and surround the Earth, Dubois relays the demands of the energy beings. Halstead and his crew are taken to Mars where they find the missing stations and the alien base at an automated uranium mine. Exploring the area, they discover the corpses of several Delta-Two crew members who failed to merge with the energy beings because of their "passion and emotion". Dubois reveals that the beings are "Diaphinoids" from the Andromeda Galaxy who need humans as host bodies. Halstead and his team are forced to watch a "hosting" ceremony which results in several more deaths. They rescue a pair of female station crew then start a melee with their captors. Opening a panel in the outer wall, they escape across the surface of Mars to a nearby spacecraft while the air vents from the base, killing all inside. The U.D. fleet arrives to bomb the base but Halsteads ship cant liftoff on its own. Unwilling to let the Diaphinoids escape, Halstead demands the fleet drop its bombs even though it will almost certainly kill them too. Luckily, they are able to use the blast from the U.D. attack to help loft the ship safely into orbit. Back on Earth, Halstead is awarded the U.D. Medal of Honor... and court martialed for dereliction of duty. 

==See also==

* List of films featuring space stations

==References==
{{reflist|refs=
   
}}

==External links==
*  

 
 
 
 
 
 