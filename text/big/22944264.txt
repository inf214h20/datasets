Penny Princess
{{Infobox film
| name           = Penny Princess
| image_size     = 
| image	=	Penny Princess FilmPoster.jpeg
| caption        = 
| director       = Val Guest Frank Godwin
| writer         = Val Guest
| narrator       = 
| starring       = Dirk Bogarde Yolande Donlan Reginald Beckwith
| music          = Ronald Hanmer
| cinematography = Geoffrey Unsworth
| editing        = 
| distributor    = General Film Distributors 1952
| runtime        = 91 min.
| country        = U.K.
| language       = English
| budget         = 
| preceded_by    = 
| followed_by    = 
| website        = 
}} 1952 British Technicolor comedy written and directed by Val Guest for his own production company, Conquest Productions.  The film stars his future wife Yolande Donlan, who was Guests production company partner, and features Reginald Beckwith, the other partner in Conquest Productions. Dirk Bogarde plays the leading man.    

Penny Princess was filmed in Catalonia, the first British production to be filmed in Spain. 

==Plot==
The fictional European microstate of Lampidorra has "no taxes, no quotas, no tariffs, no forms to fill in". Its two thousand residents make their money from the national (and legal) profession of smuggling to and from its neighbors: France, Italy, and Switzerland. However, the country falls on hard times and becomes bankrupt.

Like the later The Mouse that Roared, the small state seeks the financial support of the United States   in the guise of a rich American who buys the whole country for $100,000.  When he dies shortly afterward, Lampidorra is inherited by his distant relative, Lindy Smith (Yolande Donlan), a Macys shopgirl.

On the way to her new realm, Lindy meets Tony Craig (Dirk Bogarde), an inexperienced British salesman trying to sell cheese to the Swiss. When she arrives in Lampidorra, Lindy is met by the ruling   (Erwin Styles), who is a shoemaking|cobbler, the Burgomeister (Kynaston Reeves), who is a policeman, and the Minister of Finance (Reginald Beckwith), who is a blacksmith. As her first royal decree, she outlaws smuggling. However, this exacerbates the financial crisis, as her inheritance will be tied up for at least six months by legalities.  
 teetotaler Lindy gets a bit tipsy when she samples Lampidorran "schneese", a cheese made with Schnapps. She decides it would make a terrific export and has Tony brought to her to help market it. The alcoholic cheese is a sensation, but the other European nations soon respond to the threat to their own cheese industries by imposing tariffs. Lampidorra turns to its traditional smuggling expertise to avoid paying them. 

Tony falls in love with Lindy and proposes, but an intercepted telegram from his employer leads Lindy to wrongly suspect he is just after the secret recipe for schneese. The misunderstanding is eventually cleared up. In the end, Lindy finally receives her full inheritance, allowing her to bail out her subjects and depart with Tony.

==Cast==
*Yolande Donlan as Lindy Smith
*Dirk Bogarde as Tony Craig
*A. E. Matthews as Selby, Tonys employer
*Reginald Beckwith as Minister of Finance / Blacksmith
*Mary Clare as Maria
*Edwin Styles as Chancellor / Cobbler
*Kynaston Reeves as Burgomaster / Policeman
*Desmond Walter-Ellis as Alberto, Captain of the guard
*Peter Butterworth as Julien / Postman / Farmer
*Raf De La Torre as Italian Attaché

==Production==
Val Guest attempted to obtain Montgomery Clift, Cary Grant, Robert Cummings   
and William Holden for the male cheese salesman lead, but they all turned him down.  Eager for comedy after his performance in Hunted (film)|Hunted, Dirk Bogarde accepted his first light comedy role, though he thought the film "as funny as a babys coffin". 

The film was released by Universal Pictures in American and was titled Fromage à Gogo in France.

==Notes==
 
 

 

==External links==
* 

 
 
 
 
 
 
 