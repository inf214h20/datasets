Where No Vultures Fly
 
 
{{Infobox film
| name           = Where No Vultures Fly
| image          =Where No Vultures Fly.jpg
| image_size     =
| caption        =Movie poster Harry Watt
| producer       = Michael Balcon Leslie Norman Ralph Smart based on = story by Harry Watt
| narrator       = Anthony Steel Dinah Sheridan
| music          = Alan Rawsthorne
| cinematography = Paul Beeson Geoffrey Unsworth
| editing        = Jack Harris Gordon Stone studio = Ealing Films African Film Productions
| distributor    = General Film Distributors (UK) Universal-International (US)
| released       = November 1951
| runtime        =
| country        = United Kingdom South Africa
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
}} Harry Watt Anthony Steel conservationist Mervyn Cowie.   Time, 1952-08-25.  The films opening credits state that "the characters in this film are imaginary, but the story is based on the recent struggle of Mervyn Cowie to form the National Parks of Kenya."  , British Film Institute.  Come What May: In Lightest Africa ...
By John Allan May. The Christian Science Monitor (1908-Current file)   04 Apr 1952: 13.  The title Where No Vultures Fly denotes areas where there are no dead animals.   New York Times, 1952-08-19. 
 West of Zanzibar.

== Plot == poacher  (Harold Warrender).  

==Featured cast==
{| class="wikitable" width="50%"
|- bgcolor="#CCCCCC"
! Actor !! Role
|- Anthony Steel || Bob Payton
|-
| Dinah Sheridan || Mary Payton
|-
| Harold Warrender || Mannering
|- Meredith Edwards || Gwyl
|-
| William Simons || Tim Payton
|-
| Orlando Martins || MKwongi
|}

==Production==

===Development===
The movie was one of a series of "expeditionary films" Harry Watt made, like The Overlanders, where he would find the story from visiting a location. "These expeditionary films are really journalistic jobs", he wrote later. "You get sent out to a country by the studio, stay as long as you can without being fired and a story generally crops up."   

Watt got the idea of the film after a chance remark from a game warden in Tanganyika. He was shooting zebras and when Watt wondered if it was necessary, the warden remarked that Watt "talk like Martyn Cowie". This prompted the director to track down Cowie in Nairobi, who inspired the story. 

The movie was a coproduction between Ealing and South Africas African Films, with half the financing coming from South Africa. (Africa Films was a South African theatre chain.) A REPORT ON FILM PRODUCTION IN SOUTH AFRICA: Exports Bolster Dark Continents Small But Hopeful Industry
By J. A. BROWNJOHANNESBURG.. New York Times (1923-Current file)   13 Apr 1952: X5.  U.S. CONCERN SELLS ODEON SHARES: South African Buyer
Our Financial Staff. The Manchester Guardian (1901-1959)   14 July 1953: 2. 

===Shooting===
Watt took a full unit to Africa and based it at Ambaceli, south of Nairobi. They built a complete village of huts for the crew to live in. 

Anthony Steel contracted malaria during filming on location in Africa. 

==Reception== A Place in the Sun and Outcast of the Islands.  

===Box Office===
It was the second most popular film at the British box office in 1952. 

In 1957, the film and its sequel were listed among the seventeen most popular movies the Rank organisation ever released in the US. BRITAINS MOVIE SCENE: AN AMERICAN FILM EVOLVES IN THE ORIENT
By STEPHEN WATTS. New York Times (1923-Current file)   24 Mar 1957: 123. 

==References==
 

== External links ==
*  
*  
*  at BFI Screenonline

 

 
 
 
 
 
 
 
 


 