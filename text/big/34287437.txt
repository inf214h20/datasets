Sightings: Heartland Ghost
{{Infobox film
| name           = Sightings: Heartland Ghost
| image          = 
| caption        = 
| writer         = Karen Clark
| based on       = Sightings (TV series)|Sightings (1993 TV series)
| starring       = Beau Bridges Nia Long Miguel Ferrer Gabriel Olds Thea Gill Matthew Currie Holmes Rachel Hayward
| director       = Brian Trenchard-Smith
| producer       = Roee Sharon Peter Bernstein
| cinematography = Bert Dunk
| editing        = Stephen R. Myers
| distributor    = Paramount Home Entertainment
| released       = October 27, 2002
| runtime        = 105 mins
| language       = English
| budget         = 
| gross          = 
| country        = United States
}}
Sightings: Heartland Ghost is a 2002 film based on the TV series Sightings (TV series)|Sightings and inspired by true events. The film was written by Phil Penningroth and directed by Brian Trenchard-Smith. 

==Plot==
In this creepy ghost story, a Kansas couples claim that their Victorian house is haunted prompts a visit from the crew of "Sightings", a TV show exploring paranormal events. Nobody believes the couples story - especially cynical producer Beau Bridges, but show psychic Miguel Ferrer begins feeling the presence of several entities, including a little girl.

==Cast==
*Beau Bridges as Derek
*Nia Long as Lou
*Miguel Ferrer as Allen
*Gabriel Olds as Jeff
*Thea Gill as Pam
*Matthew Currie Holmes as Nolan
*Rachel Hayward as Jamie
*Trevor Roberts as Arnold

==Original Segments==
The following are the original Sightings (TV series)|Sightings episodes to feature the Heartland Ghost recurring segments.
{| class="wikitable plainrowheaders" style="width:100%;"
|-style="color:black"
! style="background: #fada00;"|No.
! style="background: #fada00;"|# Title
! Original air date Production #
{{Episode list
| Title = Sightings
| OriginalAirDate =  
| EpisodeNumber = 42
| EpisodeNumber2 = 01
| ProdCode = 3001
| ShortSummary = Heartland Ghost, Gulf Breeze Encounters, A Sculptors Gift, Black Holes. 
| LineColor = fada00
}}
{{Episode list
| Title = Sightings
| OriginalAirDate =  
| EpisodeNumber = 43
| EpisodeNumber2 = 02
| ProdCode = 3002
| ShortSummary = Heartland Ghost Update, Shared Memory Abduction, Watching The Skies, Schoolroom Angel, Colorado Cattle Mutilations, Ancient Creatures.
| LineColor = fada00
}}
{{Episode list
| Title = Sightings
| OriginalAirDate =  
| EpisodeNumber = 49
| EpisodeNumber2 = 08
| ProdCode = 3008
| ShortSummary = U.F.O. Confrontation: Iran, Death In The Desert, Heartland Ghost Investigation, Gift Of Life, Heartland Ghost Investigation.
| LineColor = fada00
}}
{{Episode list
| Title = Sightings
| OriginalAirDate =  
| EpisodeNumber = 50
| EpisodeNumber2 = 09
| ProdCode = 3009
| ShortSummary = Heartland Ghost Investigation, Nessie, Height 611, To Swim With Dolphins. 
| LineColor = fada00
}}
{{Episode list
| Title = Sightings
| OriginalAirDate =  
| EpisodeNumber = 57
| EpisodeNumber2 = 16
| ProdCode = 3016
| ShortSummary = Heartland Aftermath, Curses, Sprites And Blue Jets, Gaias Revenge, Outback Abduction, Heartland Online.
| LineColor = fada00
}}
{{Episode list
| Title = Sightings
| OriginalAirDate =  
| EpisodeNumber = 64
| EpisodeNumber2 = 23
| ProdCode = 3023
| ShortSummary = My Past Life, Heartland Ghost, Clear Intent, Snake Church, Transformed By The Light. 
| LineColor = fada00
}}
{{Episode list
| Title = Sightings
| OriginalAirDate =  
| EpisodeNumber = 103
| EpisodeNumber2 = 10
| ProdCode = 5062
| ShortSummary = The Heartland Ghost, Echo Missile Encounter, IN THE NEWS, Crop Circle Video Mystery, Healing With Music, Millennium Watch - Quatrain 72. 
| LineColor = fada00
}}
|}

==Releases==
The film was released on October 27, 2002 and released on DVD on March 2, 2004. 

==References==
 

 
 

 
 


 