Damnation (film)
{{Infobox Film
| name           = Damnation
| image          = Old_Man_with_his_Head_in_his_Hands_(At_Eternitys_Gate).jpg
| image_size     = 
| caption        = 
| director       = Béla Tarr
| producer       = 
| writer         = 
| narrator       = 
| starring       = 
| music          = Mihály Vig
| cinematography = 
| editing        = 
| distributor    = 
| released       = 
| runtime        = 116 minutes
| country        = Hungary Hungarian
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}} 1988 Cinema Hungarian film directed by Béla Tarr. The screenplay was co-written by Tarrs frequent collaborator, László Krasznahorkai.

The movie is a favourite of many, including Susan Sontag. 

==Plot==
Damnation tells the story of Karrer (Miklós B. Székely), a depressed man in love with a married torch singer (Vali Kerekes) from a local bar, the Titanik. The singer broke off their affair, because she dreams of becoming famous. Karrer is offered smuggling work by Willarsky (Gyula Pauer), the bartender at the Titanik. Karrer offers the job to the singers husband, Sebestyén (György Cserhalmi). This gets him out of the way, but things dont go as Karrer plans. Betrayals follow. Karrer despairs.

==Reception== Michael Atkinson of Village Voice called the film "a serotonin-depleted ordeal, and yet seemingly a sketchbook of vibes and ideas to come, with some of the most magnificent black-and-white images shot anywhere in the world."  Jonathan Rosenbaum wrote in the Chicago Reader, "The near miracle is that something so compulsively watchable can be made out of a setting and society that seem so depressive and petrified."  Writing for Slant Magazine, Jeremiah Kipp argued, "In terms of creating a strong cinematic world, Tarr has few equals." 

==References==
 

==External links==
*  
*  
*  
* 

 

 
 
 
 
 
 


 
 