Veeralipattu (2007 film)
{{Infobox film
| name           = Veeralipattu
| image          = Veeralipattu.jpg
| caption        = 
| director       = Kukku Surendran
| producer       = Sunil Surendran
| screenplay     = Ashok Sasi
| story          = Neeraj Menon Murali Padmapriya
| music          = Viswajith
| cinematography = Manoj Pillai
| editing        = Hariharaputhran
| distributor    = Maruthi Film Factory
| released       =  
| runtime        = 
| country        = India
| language       = Malayalam
| budget         = 
| gross          = 
}} Murali and Padmapriya in the lead roles.

The film was produced by Sunil Surendran under the banner of Open Channel and was distributed by Maruthi Film Factory. The film was dubbed and released in Tamil as Nagercoil.

== Cast ==
* Prithviraj Sukumaran as Hari Murali as Madhavan
* Padmapriya
* Jagathy Sreekumar
* Suraj Venjaramoodu as Pavithran
* Madampu Kunjukuttan as Patteri
* Indrans
* Sreejith Ravi
* Jaffar Idukki
* Krishnan Lakshmi
* Rekha
* Ansi John

== Songs ==
The songs for the film were composed by Viswajith; the lyrics were written by Vayalar Sarath Chandra Varma. The background music was scored by Mohan Sithara.

The soundtrack was distributed by Manorama Music.

# "Alilayum": Vineeth Sreenivasan, Manjari Babu
# "Alilayum": Manjari Babu
# "Ilaneerin": Anvar Sadath, K. S. Chithra

== Awards == Murali

== External links ==
*  
* http://popcorn.oneindia.in/title/1715/veeralipattu.html
* http://www.nowrunning.com/movie/3617/malayalam/veeralipattu/index.htm
* http://www.indiaglitz.com/channels/malayalam/preview/9333.html

 
 
 

 