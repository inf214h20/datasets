The ABCs of Death
 
 
{{Infobox film
| name           = The ABCs of Death
| image          = Abcsofdeath.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Various directors See below
| producer       = Ant Timpson Tim League
| writer         = See below
| music          = Various composers
| cinematography = Various cinematographers
| editing        = Various editors Drafthouse Films Timpson Films
| distributor    = Magnet Releasing
| released       =  
| runtime        = 124 minutes  
| country        = United States New Zealand
| language       = English Spanish Japanese Thai
| gross          = $21,832 
}} anthology Horror horror comedy VOD January 31 and in theaters March 8. 
 Horror Movie", and also state that a sequel, ABCs of Death 2, would be released in late 2014 (it subsequently was). 

==Plot==
The film is divided into twenty-six individual chapters, each helmed by a different director assigned a letter of the alphabet. The directors were then given free rein in choosing a word to create a story involving death. The varieties of death range from accidents to murders. 

A contest was held for the role of the twenty-sixth director. The winner was UK-based director Lee Hardcastle, who submitted the claymation short for T. 

==Segments ==
 
*A is for Apocalypse (directed and written by Nacho Vigalondo): A womans long-running attempt to commit murder grows more desperate and brutal as time runs out.
*B is for Bigfoot (directed and written by Adrian Garcia Bogliano): A babysitter tells his young charge that if she doesnt go to sleep, Bigfoot will get her--not knowing that a real monster lurks outside.
*C is for Cycle (directed and written by Ernesto Diaz Espinoza): A man investigates a mysterious noise, only to find himself trapped in a nightmarish cycle of murder.
*D is for Dogfight (directed and written by Marcel Sarmiento): A man is forced into a boxing ring to battle a vicious dog.
*E is for Exterminate (directed and written by Angela Bettis): A spider takes revenge on the man who tried to kill it.
*F is for Fart (directed and written by Noboru Iguchi): A darkly humorous short in which a schoolgirl dies by smelling her imaginary friends fart rather than during a deadly gas attack.
*G is for Gravity (directed and written by Andrew Traucki): A POV story of a man committing suicide by drowning.
*H is for Hydro-Electric Diffusion (directed and written by Thomas Malling): A surreal short in which a humanoid dog is first seduced, then tortured, by a seductive fox-woman.
*I is for Ingrown (directed and written by Jorge Michel Grau): The interior monologue of a woman in the process of being murdered.
*J is for   is forced to wait in agony for his assistant to decapitate him.
*K is for Klutz (directed and written by  , which refuses to be flushed.
*L is for Libido (Directed and written by Timo Tjahjanto): A bizarre sexual contest where the loser in each round is killed.
*M is for Miscarriage (directed and written by Ti West): A woman has a miscarriage in a toilet.
*N is for Nuptials (directed and written by Banjong Pisanthanakun): A talking bird ruins a marriage proposal by driving the would-be bride into a murderous jealous rage.
*O is for Orgasm (directed and written by Bruno Forzani and Héléne Cattet): A man and a woman have sex in a series of extreme close-up shots. 
*P is for Pressure (directed and written by Simon Rumley): A desperate prostitute with three children to feed agrees to star in a crush film.
* Q is for Quack (directed and written by   by killing a live animal.
*R is for Removed (directed and written by Srdjan Spasojevic): A mans skin is used to make 35mm film.
*S is for Speed (directed and written by Jake West): A kidnapper and her victim outrun a mysterious man trying to murder them both.
*T is for Toilet (directed and written by Lee Hardcastle): A little boy is afraid of the bathroom toilet.
*U is for Unearthed (directed and written by Ben Wheatley): The point-of-view of a vampire as he is chased down and staked by an angry mob.
*V is for Vagitus (The Cry of a Newborn Baby) (directed and written by  , women must petition the government for permission to bear children.
*W is for WTF! (directed and written by Jon Schnepp): A series of random and vulgar scenes, including the filmmakers deciding what to do with the letter W.
*X is for XXL (directed and written by Xavier Gens): An overweight woman falls into despair at her size and attempts to remove her fat with a knife.
*Y is for Youngbuck (directed and written by Jason Eisener): A pedophile teaches a young boy to hunt deer.
*Z is for Zetsumetsu (Extinction) (directed and written by Yoshihiro Nishimura): An abstract series of events dealing with revisionist views of Japanese relations with the West.

==Reception==
Critical reception for The ABCs of Death has been negative. Rotten Tomatoes reports that 35% of critics gave the film a positive review with an average score of 4.8/10, based on 65 reviews. The consensus says the film "is wildly uneven, with several legitimately scary entries and a bunch more that miss the mark."  The Nerdist Podcast|Nerdist calls it "a midnight movie for folks with a sick sense of humour".  The Austin Chronicle says it "soars to such artistic heights, and such tasteless depths, on a global scale, no less, bodes well for the future of cinema fantastique and otherwise",  while Inside Pulse says the movie has a "brilliant concept but not great execution". Many reviewers criticized the film shorts unevenness.   
 Dread Central gave a mixed review for the film, saying the film is "full of installments that are more bad than good" but that it was an "easy watch" overall.  Film School Rejects gave The ABCs of Death a B rating, praising D is for Dogfight while saying that "M is for Miscarriage is almost insulting in its laziness".  Screen Crush gave an overall positive review, saying that it was "a good time at the movies". 

Dave Canfield writing for influential American film magazine Magills said of the film: "The ABCs of Death is for anyone who loves horror since it is easy to skip through segments that are not to taste. Any viewer should be prepared to laugh pretty hard; feel tense; get grossed out like they would at any halfway decent horror film. But that same viewer now has a chance to find out about some of the best directors working in horror today."

== Sheila Kearns case ==
After showing the film to a group of high school Spanish students, former Columbus Ohio substitute teacher Sheila Kearns was found guilty of four counts of disseminating matter harmful to juveniles.  As of Thursday, January 16th, 2015, Kearns has been convicted of four felony counts of disseminating matter harmful to juveniles.  On 5 March 2015, she was sentenced to 90 days in jail and probation for three years.     

==References==
 

==External links==
* 
* 
* 
* 
* 
* 

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 