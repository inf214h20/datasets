Benvenuti al Sud
{{Infobox film
| name           = Benvenuti al Sud
| image          = Benvenuti al Sud.jpg
| border         = yes
| caption        = 
| director       = Luca Miniero
| producer       = Marco Chimenz Francesca Longardi Giovanni Stabilini Riccardo Tozzi Oliver Berben Martin Moszkowicz
| writer         = Dany Boon Alexandre Charlot Franck Magnier (Bienvenue chez les Chtis) Massimo Gaudioso
| starring       = Claudio Bisio Alessandro Siani Angela Finocchiaro Valentina Lodovini Fulvio Falzarano
| music          = Umberto Scipione
| cinematography = Paolo Carnera
| editing        = 
| studio         = 
| distributor    = Medusa Film
| released       =  
| runtime        = 106 minutes
| country        = Italy
| language       = Italian, Western Lombard, Neapolitan, French
| budget         = 
| gross          = 
}} Italian comedy film directed by Luca Miniero. The film has a sequel, Benvenuti al Nord.
 Italian mayor murdered for his anti-crime campaign. 

==Plot==
The plot is similar to that of the film  )  in northern Italy, near Milan, is banished to Castellabate,  a town near Naples in southern Italy, for two years.

Alberto Colombo, postmaster of Usmate Velate in Brianza, fails to obtain a transfer to Milan as precedence is given to a handicapped worker. So Alberto pretends to be paralyzed in order to get the coveted transfer, and thereby fulfill the desire of his wife Silvia to live in the capital of Lombardia and satisfy his own ambitions in life, including those he has for the future of his son Chicco, who would be able to study in an "American School" there. However, his deception is discovered when he himself gives the game away by standing up to greet the inspector sent to check the veracity of his claim to be physically handicapped in order to obtain the desired transfer. As a punishment, Alberto is thus transferred South to become postmaster  of the provincial village of Castellabate - described in the movie as being near  Naples.
 dismissal for gorgonzola to high protection, Kosovo War. Following the instructions from navigation, then along a stretch of the busy Salerno-Reggio Calabria, being stuck in a long traffic jam. The trip ends late at night, when it reaches the village on the hill, greeted by pouring rain. A Castellabate, thanks to the postman Mattia and colleagues Mary Costabile Costabile small and great, having committed some events, Alberto ends up appreciating the beauty and the habits of the town of Campania, noting also a low crime rate, pleasant places, not too muggy and warm sympathy of the people, finally discovering that his ideas on the south were often only prejudices. In the quiet village but for the joy of having found new friends and the effort of trying to imitate the habits of the locals will have to contend with the often gruff and unyielding policeman of the place, and this will take a fine for having thrown its waste from the window or for breaking into a bar with the moped service, completely drunk.

Meanwhile, he keeps hidden reality to Silvia, who seems open to change and biased toward the south: thinking they can improve their relationship, says exactly the opposite of what you are going through, condescending stereotypes to which the wife and her circle of friends are loyal: tells of being evil and live in an unsanitary and dangerous. One day, Silvia decides to visit him, thus Alberto in great embarrassment with friends, who will clear the lies told to confess to his wife on the South They, however, although at first bitter and angry with the director, decide to help unbeknownst to him so that his wife, who came to Castellabate, see fulfilled all the lies you hear from her husband organized a skit that shows the village dangerous, dilapidated and chaotic. In the end the truth will out, and his wife, discovered the whole staging, is furiously angry, threatening the separation . Alberto therefore plays a role in bringing to Mattia colleague and ex-girlfriend Maria, who later married Matthias and from which, at the end of the film, will be expecting a child. This reveals the groundlessness of the allegations of the wife of his relationship with the beautiful colleague Maria Silvia eventually forgive Alberto, and moved to the next two years in the South, next to her husband and his son Chicco. At the end of the stay in the South will prove to be a fantastic experience, it will be difficult for Alberto to forget, so much so that when it finally comes to Alberto then the long-awaited letter of transfer to Milan, they almost regretted it. The film ends with the scene of Alberto, happy to return home, but at the same time sad for having to leave the beautiful and peaceful Castellabate, traveling to its new destination with the whole family.

==References==
 

==External links==
* 

 
 
 
 
 
 