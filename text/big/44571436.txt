The Stationmaster (film)
{{Infobox film
| name           = The stationmaster
| image          =     The_Stationmaster_(1972).jpg


| image_size     =  Movie poster  
| alt            = 200px
| caption        =  
| director       = Sergei Solovyov

| producer       = 
| writer         = Sergei Solovyov  Alexander Pushkin

| narrator       = Gennady Shumsky (on behalf of Ivan Belkin)


| starring       = Nikolai Pastukhov Nikita Mikhalkov Marianna Kushnerova

| music          = Isaac Schwartz

| cinematography =  Leonid Kalashnikov


| editing        = 
| studio         = Mosfilm
| distributor    = 
| released       = 1972
| runtime        = 68 min
| country        =     Russian
| budget         = 
| gross          = 
}}


Based on the novel Alexander Pushkin.


A touching story about the sanctity of parental love and disappointments: young Duniasha against the will of the father leaves home with a young rake. Old mans life is destroyed, and a daughter, disobey parents, unhappy ...

== Cast ==
 
* Nikolai Pastukhov – Samson Vyrin, the stationmaster
* Marianna Kushnerova – Dunya, daughter of Samson Vyrin
* Nikita Mikhalkov – Minsky, rittmeister
* Valentina Ananyina  – brewers wife
* Anatoliy Borisov  – doctor
==External links==
*  

* Film   online cinema  Mosfilm

 
 
 
 
 
 
 