Pavitra Paapi
 
{{Infobox film
| name           = Pavitra Paapi
| image          = Pavitra Paapi.jpg
| image_size     = 
| caption        = 
| director       = Rajendra Bhatia 
| producer       = 
| writer         = Nanak Singh
| narrator       = 
| starring       =
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       = 1970
| runtime        = 
| country        = India Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}}
 1970 Bollywood drama film directed by Rajendra Bhatia. The film stars Balraj Sahini and Tanuja. The movie was based on a Punjabi novel by famous writer Nanak Singh

==Plot Synopsis==
Guilt-ridden by Pannalal s suicide, Kedarnath helps their family and steals money from his employer for Pannalals daughters marriage.

==Plot==
Pannalal is employed in a clock repair shop owned by  Lala Attarchand and is replaced by Kedarnath,who comes to the town of Punjab in search of such work, as according to him, his ancestors were in the same business. 
On this Pannalal curses him as being the cause of his family becoming destitute and writes a letter to him saying that this has driven him to suicide and disappears.
Kedarnath is guilt ridden and goes out of his way to help, his wife Maya, and his two daughters, Veena and Vidya. Kedarnath rents a room with them, writes letters to her on behalf of her husband and teaches Veena during his free time making him fall in love with her.
But when Maya tells him that Veenas marriage is already arranged
he helps in the marriage of Veena with the son of Daulatram. When Maya tells him that she has no money to pay for the marriage expenses, he steals cash from his employer, and tells Maya that the money is from Pannalal. He then goes away from the village .
Veenas in laws turn out to be rogues except for her father-in-law who counsels his family that they should not torture the daughter of somebody.
Meanwhile Pannalal returns to his family and visits Veenas in-laws to find her in agony and brings her back.
Kedar who now lives in Delhi, regularly sends money orders to repay his debt to Lala Attarchand. Pannalal finds out Kedars whereabouts and requests him to come back and save dying Veena.
But Kedar instead makes Veenas Husband realize his mistakes and unites the pair without appearing in person.
Film ends with Kedarnath going away.

==Cast==
*Balraj Sahni ...  pannalal
*Tanuja ...  Veena 
*Parikshat Sahni ...  Kedarnath (as Ajay Sahni) 
*Achala Sachdev ...  Maya (as Achla Sachdev) 
*Abhi Bhattacharya ...  Writer (Man who helped Kedarnath) 
*I. S. Johar ...  Lala Attarchand 
*Neetu Singh ...  Vidya (as Baby Sonia) Manorama ...  Mrs. Lala Attarchand
*Jayshree T. ...  Dancer in white dress (in song "Sada Sadak Dil") (as Jaishree) 
*Madhumati   
*Gulshan Bawra ... Adarshans worker
*Upendra Trivedi   
*Jugnu   
*Paul Mahendra    Fatima

==Music==
Prem Dhawan has composed both the lyrics and music for this movie. "Teri Duniya Se" sung by Kishore Kumar stands out prominently for its raw grief and pathos. This song was played across radio and television channels when Kishore died on 13 October 1987

==External links==
*  

 
 
 
 


 
 