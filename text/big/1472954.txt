Miles of Fire
 
{{Infobox film
| name           = Miles of Fire
| image          =
| image_size     =
| caption        =  Miles of Fire
| director       = Samson Samsonov
| producer       = Semyon Maryakhin
| writer         = Nikolai Figurovsky
| narrator       =
| starring       = Igor Savkin Margarita Volodina Vladimir Kenigson
| music          = Nikolai Kryukov
| cinematography = Fyodor Dobronravov
| editing        = Zoya Veryovkina
| distributor    = Mosfilm
| released       =  
| runtime        = 85 mins
| country        = Soviet Union
| language       = Russian
| budget         =
}} Red Western filmed by Samson Samsonov in 1957.  Often considered the earliest of the Red Westerns (or Osterns), it was made before the term was coined.  The film is a Russian civil war drama, focusing on the conflict between the Reds and the Whites.
 The Cricket that won two prizes at the Venice Film Festival.  He was later awarded the title of Peoples Artist of the USSR.

==Plot summary== White Guard Chekist Zavarzin is in a hurry to travel south.  In a flash of inspiration, he decides to use tachankas or machine gun carts to reach his destination, and attracts an unusual group of equally desperate fellow travellers.
 Western films like John Fords classic Stagecoach (1939 film)|Stagecoach, because of the diverse set of characters thrown together in desperate circumstances.  Zavarzins companions on his journey include the doctor Shelako, the nurse Katya and a mysterious white guard officer Beklemishev, disguised as a veterinary surgeon.  This formula gives the film an extra psychological dimension as the characters progress towards their destination echoes the resolution of their problems and transitions in relationships.

==Main cast==
*Igor Savkin (Grigory Fyodorovich Zavarzin) 
*Margarita Volodina (Katerina Gavrilovna)
*Mikhail Troyanovsky (Dr. Shelako)
*Vladimir Kenigson (Sergei Beklemishev)

==External links==
* 
*  , the official Mosfilm channel

 

 
 
 
 
 
 
 
 
 
 

 
 