The Switch (2010 film)
{{Infobox film
| name           = The Switch
| image          = Switchposter10.jpg
| alt            =  
| caption        = Theatrical release poster
| director       =  
| producer       =  
| screenplay     = Allan Loeb
| based on       =  
| starring       =  
| music          = Alex Wurman
| cinematography = Jess Hall
| editing        = John Axelrad
| studio         = Mandate Pictures Echo Films
| distributor    = Miramax Films  (theatrical) 
| released       =   
| runtime        = 101 minutes
| country        = United States
| language       = English
| budget         = $19 million 
| gross          = $49,830,607 
}} romantic comedy Thomas Robinson. Patrick Wilson, Juliette Lewis, and Jeff Goldblum appear in key supporting roles.
 artificial insemination Baby Mama, Miramax film Disney before the former was sold to Filmyard Holdings on December 3, 2010.

== Plot == neurotic long-time best friend Wally Mars (Bateman), she chooses to do so alone because she cant wait any longer. She also wants a face-to-face sperm donor, disdaining using a sperm bank. Wally suggests he be the donor, but Kassie sees him as a bit too neurotic, pessimistic, and self-absorbed, and besides, as best friends, "that would be weird." Wally has always had feelings for Kassie, and they dated six years ago, but his friend Leonard (Goldblum) points out he missed his chance when she put him in the "friend zone". 

Kassie selects as sperm donor a handsome and charming (and married) assistant professor, Roland (Wilson). Kassie organizes an "insemination party", where Wally meets Roland and takes an instant dislike to him. Roland is then called upon and produces his sperm in the bathroom, leaving it in a sample cup. Wally uses the bathroom and sees the sample. Drunk and doped up after taking a pill provided him at the party by Kassies friend, Debbie (Lewis), and not liking the idea of Kassie being inseminated with this sperm, Wally plays with the cup and accidentally spills it into the sink. Panicking, he replaces the sperm with his own. The next day at work, still hangover|hungover, he remembers nothing. The insemination is successful. Wally is then upset when Kassie tells him she is returning to her family home in Minnesota, as she thinks that would be a better environment to raise a child in, instead of New York City. She leaves, and Wally maintains a non-fulfilling, dreary existence.
 New York along with precocious-but-neurotic son Sebastian (Robinson). She wants to reconnect with Wally, and is eager to introduce her son to him. After an awkward first meeting, Wally eventually forms a bond with this loveable and seemingly mini-version of himself, and Sebastian starts to become close to Wally, but the bad news is that Roland is in the picture too: Kassie has started dating him because he is now divorced and, as she thinks he is Sebastians father, and a nice guy, maybe it would work. 

After Wally notices the similarities between himself and Sebastian, and after talking to his friend Leonard, Wally realizes what happened seven years earlier. Just before Roland proposes to Kassie, Wally reveals to Kassie that Sebastian is his son, along with his true feelings for her. She is shocked and angry and does not want to see him again. Some time passes. One day, as he is leaving work, Wally finds Kassie waiting for him on the street outside his office. She tells him Sebastian really misses him and needs him. Wally admits he misses and needs Sebastian too. Kassie then discloses she isnt with Roland any longer, and that she loves him, even with all of his idiosyncrasies.  Wally proposes to her, Kassie accepts, and they kiss. The final scene shows a happily married Wally and Kassie throwing Sebastians eighth birthday party.

== Cast ==
* Jennifer Aniston as Kassie Larson
* Jason Bateman as Wally Mars Thomas Robinson as Sebastian Larson Patrick Wilson as Roland
* Juliette Lewis as Debbie Epstein
* Jeff Goldblum as Leonard
* Caroline Dhavernas as Pauline
* Scott Elrod as Declan
* Bryce Robinson as older Sebastian Larson
* Diane Sawyer as herself (cameo)

== Production ==
Aniston revealed in the DVD extras that she had known Bateman since she was age 25, and the producers and directors noted their good chemistry in working together. 

==Release==
=== Critical reception === Razzie Award Worst Actress for her performance in the film.

  About a The Kids Are All Right, adding: "Though the film never quite rises to the level of either, the filmmakers show enough restraint to keep things interesting, Aniston and Bateman keep things both light and dark when they should, and Robinsons Sebastian steals everyones heart."  

 , Jason Bateman."  Owen Gleiberman from Entertainment Weekly gave the film a B rating and called it "a pleasant surprise. Its a by-the-numbers movie, but the dots that get connected feel new." 
 New York Daily News called the film a "Judd Apatow lite, Farrelly brothers special blend. Just call it When Harry Met Sally and Her Ovum.  Andrew Barker from Variety (magazine)|Variety felt that The Switch was "an unfunny, manipulative romance about two unlikable people and their prop of a son   The pic mangles the premise of its source material." 

=== Commercial success ===
Even though it gained mixed to lukewarm reviews from critics, The Switch proved to be a moderate financial success. Budgeted at $19 million, it grossed $49.8 million worldwide, 55.7% of which came from its domestic run.    91 days in US theatres, it opened in 2,012 theaters and was ranked seventh after its opening weekend, averaging $4,193 per venue.  On January 18, 2011, Maple Pictures released the film on DVD and Blu-ray Disc|Blu-ray in Canada, while Lionsgate released it in the United States on March 15, 2011. It grossed $7.7 million in US DVD sales. 

== Soundtrack ==
{{Infobox album
| Name = The Switch
| Type = soundtrack
| Artist = Various Artists
| Cover = The_Switch_Cover.jpg
| Released =  
| Genre = Film soundtrack
| Label = Rhino
}}

{{tracklist
| headline     = Track listing
| extra_column = Singer(s)
| total_length =
| title1       = Opening Titles
| extra1       = Alex Wurman
| length1      = 1:54 Instant Replay
| extra2       = Dan Hartman
| length2      = 3:25
| title3       = Freakshow on the Dance Floor
| extra3       = The Bar-Kays
| length3      = 6:34 I Cant Wait
| note4        = edited version
| extra4       = Nu Shooz
| length4      = 3:40
| title5       = The Bomb (These Sounds Fall Into My Mind)
| note5        = Pop Radio Mix
| extra5       = Sunrider
| length5      = 2:43
| title6       = Here Comes the Sun
| extra6       = Fat Larrys Band
| length6      = 5:22
| title7       = Pushin On Alice Russell
| extra7       = Quantic Soul Orchestra
| length7      = 3:19
| title8       = Little L
| extra8       = Jamiroquai
| length8      = 3:57
| title9       = Lice
| extra9       = Alex Wurman
| length9      = 2:49
| title10      = Open Your Heart
| extra10      = Lavender Diamond
| length10     = 3:11
| title11      = Sea Green, See Blue
| extra11      = Jaymay
| length11     = 6:17
| title12      = Bluebird of Happiness
| note12       = Ulrich Schnauss Remix
| extra12      = Mojave 3
| length12     = 9:56
| title13      = All the Beautiful Things Eels
| length13     = 2:22
| title14      = Numbered Days
| extra14      = Eels
| length14     = 3:42
| title15      = Lovers Carvings
| extra15      = Bibio
| length15     = 3:54
}}

== References ==
 

== External links ==
;Official
*    
;Database
*  
*  
*  
*  

 
 
 
 
 
 
 
 