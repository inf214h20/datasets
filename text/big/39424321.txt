The Texas Rangers (1951 film)
{{Infobox film
| name           = The Texas Rangers
| image size     = 
| image	=	
| caption        = 
| director       = Phil Karlson
| producer       = Bernard Small Edward Small (uncredited)
| writer         = Richard Schayer
| based on       = story by Frank Gruber George Montgomery Gale Storm Jerome Courtland
| music          = 
| cinematography = 
| editing        = 
| distributor    = Columbia Pictures
| studio         = Edward Small Productions
| released       = July 1951
| runtime        = 
| country        = USA
| language       = English
| budget         = 
}}
The Texas Rangers is a 1951 Western film. 

==Plot==
Outlaw Sam Bass terrorises Texas. Johnny Carver and Buff Smith are released from jail by the head of the Texas Rangers to help capture him.

==Cast== George Montgomery as Johnny Carver
*Gale Storm as Helen Fenton
*Jerome Courtland as Danny Bonner 
*Noah Beery Jr. as Buff Smith William Bishop Sam Bass
*John Litel as Maj. John D. Jones
*Douglas Kennedy as Dave Rudabaugh
*John Dehner as John Wesley Hardin Ian Macdonald as The Sundance Kid
*John Doucette as Butch Cassidy
*Jock OMahoney as Duke Fisher
*Joseph Fallon as Jimmy

==Production==
George Montgomery had previously made two Westerns for Edward Small. Disney Again to Wed Cartoons, Live Action; Montgomery Does Ranger
Schallert, Edwin. Los Angeles Times (1923-Current File)   22 July 1950: 9.  The film was produced by Smalls son Bernard. THE SCREEN IN REVIEW: Edward Smalls Texas Rangers, With George Montgomery as Jailbird, Bows at Holiday At the Victoria At the Squire
By BOSLEY CROWTHER. New York Times (1923-Current file)   14 July 1951: 7. 

==References==
 

==External links==
*  at IMDB
 
 

 
 
 
 
 

 