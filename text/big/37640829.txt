Budak Nafsu
{{Infobox film
| name           = Budak Nafsu
| image          = Budak Nafsu poster.jpg
| image_size     = 
| border         = 
| alt            = 
| based on       = 
| caption        = Theatrical poster
| director       = Sjumandjaja
| producer       = Ram Soraya
| screenplay     = Sjumandjaja
| story          = Titie Said
| starring       = {{plainlist|
*Jenny Rachman
*El Manik
}}
| music          = Idris Sardi
| cinematography = Soetomo Gandasubrata
| editing        = Norman Benny
| studio         = Soraya Intercine Film
| distributor    = 
| released       =  
| runtime        = 94 minutes
| country        = Indonesia
| language       = Indonesian
| budget         = 
| gross          = 
}}
Budak Nafsu (literally Slave to Lust, also known as Fatima) is a 1983 Indonesian film directed by Sjumandjaja and adapted from the 1981 novel Fatima by Titie Said. Starring Jenny Rachman and El Manik, it follows a mother who is forced to serve as a comfort woman for Japanese men stationed in British Malaya in an effort to save her daughter. The film was a commercial success, although critics have emphasised its sexual aspects.

==Plot== Japanese occupation forces to save her of her daughter, thus resigning herself to the fate of a comfort woman. She is one of hundreds of women sent from the Dutch East Indies (now Indonesia) to Japanese occupation of Malaya|Japanese-occupied British Malaya and forced to work in a brothel. There she meets Takashi (El Manik), a kindly Japanese commander who falls in love with her. Fatima moves in with him, which protects her from the other men.
 proclaimed its independence, Fatimas ship is captured by Dutch soldiers from the Netherlands Indies Civil Administration (NICA).

Fatima is forced to work in a NICA brothel for the remainder of the Indonesian National Revolution, although she remains combative. After the revolution, Fatima is released into poverty and over the years finds that even Indonesians misuse her. She falls ill and is brought to a hospital, where she is treated by the daughter she had left years earlier, now a doctor. The aged Fatima soon leaves the hospital and wanders about, looking at the neon signs advertising Japanese products.

==Production== forced labourers beaten to death.  He adapted Budak Nafsu from the novel Fatima (1981) by Titie Said; he had initially planned on keeping the original title, and the film has also been released as Fatima. 

The film starred Jenny Rachman in the leading female role of Fatima and El Manik as Takadeshi. More minor roles were taken by Sofia WD, Roy Marten, and Maruli Sitompul. Many of these actors had previous experience with the director. Sjumandjaja also had a cameo. According to El Manik, on one occasion the actor expressed concern that a Japanese commander would be unable to speak Indonesian fluently, as Sjumandjajas screenplay called for, and that there should be Japanese-language dialogue. Sjumandjaja told him to write the dialogue himself if he wanted. 

The crew included Norman Benny as film editing|editor, Soetomo Gandasubrata as cinematography|cinematographer, and Djufri Tanissan as artistic director; music was provided by Idris Sardi.  Budak Nafsu was the first film produced by Raam Lalchand Pridhani (credited as Ram Soraya) and his Soraya Intercine Film.  

==Themes==
In his 1991 typology of Indonesian cinema, the American visual anthropologist Karl G. Heider placed Budak Nafsu in a "Japanese period" genre, characterised by violence in excess of works showing the colonial period and the use of female nudity "only as part of the violence, never as a consequence of love".  He also outlined a standard plot for these works, one reflected in Budak Nafsu. Other examples of the genre include Kadarwati, Kamp Tawanan Wanita (both 1983), and Lebak Membara (1982). 

==Release and reception==
Budak Nafsu was released in 1983 and was a commercial success.  Heider described Budak Nafsu as "little more than a sex exploitation vehicle",  and several other critical reviews have emphasised the vulgarity of the film.  

A 35&nbsp;mm copy of Budak Nafsu is available at Sinematek Indonesia. 

==Awards==
Budak Nafsu was nominated for nine Citra Awards at the 1984 Indonesian Film Festival, winning four. 

{| class="wikitable plainrowheaders" style="font-size: 95%;"
|-
! scope="col" | Award Year
! scope="col" | Category
! scope="col" | Recipient
! scope="col" | Result
|-
! scope="row" rowspan="9" | Indonesian Film Festival
| rowspan="9" | 1984 Citra Award Best Picture
| 
| 
|- Best Director
| Sjumandjaja
| 
|-
| Best Screenplay
| Sjumandjaja
|  
|- Best Leading Actor
| El Manik
|  
|- Best Leading Actress
| Jenny Rachman
|  
|-
| Best Cinematography
| Soetomo Gandasubrata
|  
|-
| Best Artistic Direction
| Djufri Tanissan
|  
|-
| Best Editing
| Norman Benny
|  
|-
| Best Musical Direction
| Idris Sardi
|  
|}

==Footnotes==
 

==Works cited==
 
*{{cite web
 |title=Budak Nafsu
 |language=Indonesian
 |url=http://filmindonesia.or.id/movie/title/lf-b016-83-877986_budak-nafsu-fatima
 |work=filmindonesia.or.id
 |publisher=Konfidan Foundation
 |location=Jakarta
 |accessdate=14 November 2012
 |archiveurl=http://www.webcitation.org/6CAXKbThU
 |archivedate=14 November 2012
 |ref= 
}}
*{{cite book
 |url=http://books.google.ca/books?id=m4DVrBo91lEC
 |title=Indonesian Cinema: National Culture on Screen
 |isbn=978-0-8248-1367-3
 |author1=Heider
 |first1=Karl G
 |year=1991
 |publisher=University of Hawaii Press
 |location=Honolulu
 |ref=harv
}}
*{{cite book
 |url=http://books.google.co.id/books?id=pTisnp71EoIC
 |title=Lubang Hitam Kebudayaan
 |isbn=978-979-672-896-1
 |location=Yogyakarta
 |publisher=Kanisius
 |ref=harv
 |author1=Budiman
 |first1=Hikmat
 |year=2002
}}
*{{cite web
 |title=Kredit Budak Nafsu
 |trans_title=Credits for Budak Nafsu
 |language=Indonesian
 |url=http://filmindonesia.or.id/movie/title/lf-b016-83-877986_budak-nafsu-fatima/credit
 |work=filmindonesia.or.id
 |publisher=Konfidan Foundation
 |location=Jakarta
 |accessdate=14 November 2012
 |archiveurl=http://www.webcitation.org/6CAXXUUoG
 |archivedate=14 November 2012
 |ref= 
}}
* {{cite news
  | last = Marselli
  | first = 
  | date = 27 July 1986
  | language = Indonesian
  | title = In Memoriam Syumanjaya Cinta segi Tiga : film-wanita -dirinya
  | trans_title = In Memoriam Syumanjaya Love Triangle: Film-Women-Himself
  | work = Kompas
  | page = 42
  | url = http://kepustakaan-tokoh.perfilman.pnri.go.id/sjumandjaja/uploaded_files/pdf/clipping/news_articles/normal/KOMPAS_19860727.PDF
  | accessdate = 11 February 2012
  | archiveurl = http://www.webcitation.org/65MxQWAFB
  | archivedate = 11 February 2012
  | ref =  
  }}
* {{cite news
  | work = Vista
  | date = 19 July 1990
  | language = Indonesian
  | title = Mengenang Sjumandjaja
  | trans_title = Remembering Sjumandjaja
  | pages = 46–53
  | volume = 90
  | url = http://kepustakaan-tokoh.perfilman.pnri.go.id/sjumandjaja/uploaded_files/pdf/clipping/news_articles/normal/VISTA_90_1990.PDF
  | accessdate = 11 February 2012
  | archiveurl = http://www.webcitation.org/65N0n2F85
  | archivedate = 11 February 2012
  | ref =  
  }}
*{{cite web
 |title=Penghargaan Budak Nafsu
 |trans_title=Awards for Budak Nafsu
 |language=Indonesian
 |url=http://filmindonesia.or.id/movie/title/lf-b016-83-877986_budak-nafsu-fatima/award
 |work=filmindonesia.or.id
 |publisher=Konfidan Foundation
 |location=Jakarta
 |accessdate=14 November 2012
 |archiveurl=http://www.webcitation.org/6CAWflQae
 |archivedate=14 November 2012
 |ref= 
}}
*{{cite web
 |title=PT Soraya Intercine Film
 |language=Indonesian
 |url=http://filmindonesia.or.id/movie/name/nmo4b9bcde7df535_pt-soraya-intercine-film/filmography
 |work=filmindonesia.or.id
 |publisher=Konfidan Foundation
 |location=Jakarta
 |accessdate=14 November 2012
 |archiveurl=http://www.webcitation.org/6CAZUTU0Q
 |archivedate=14 November 2012
 |ref= 
}}
*{{cite web
 |title=Raam Lalchand Pridhani
 |language=Indonesian
 |url=http://filmindonesia.or.id/movie/name/nmp4b9bce2918e70_ram-soraya/filmography
 |work=filmindonesia.or.id
 |publisher=Konfidan Foundation
 |location=Jakarta
 |accessdate=14 November 2012
 |archiveurl=http://www.webcitation.org/6CAZI2xFm
 |archivedate=14 November 2012
 |ref= 
}}
* {{cite news
  | last = Winarno
  | first = Ateng
  | date = 14 August 1977
  | title = Sjuman: Kepahitan Masa Kecil Terbawa Dalam Karya-karyanya
  | trans_title = Sjuman: The Bitterness of His Youth Shows in His Films
  | work = Suara Karya
  | url = http://kepustakaan-tokoh.perfilman.pnri.go.id/sjumandjaja/uploaded_files/pdf/clipping/news_articles/normal/SK_19770814.PDF
  | accessdate = 11 February 2012
  | archivedate = 11 February 2012
  | archiveurl = http://www.webcitation.org/65NEInSCN
  | ref =  
  }}
* {{cite news
  | last = W.S.
  | first = Titiek
  |date=July 1985
  | title = Indonesia Kehilangan Seorang Pengabdi Seni: In Memoriam Syumanjaya
  | trans_title = Indonesia Has Lost a Man Dedicated to the Arts: In Memoriam Syumanjaya
  | work = Sinematek Indonesia
  | url = http://kepustakaan-tokoh.perfilman.pnri.go.id/sjumandjaja/uploaded_files/pdf/clipping/news_articles/normal/SI_NO_76-122_1985.PDF
  | accessdate = 11 February 2012
  | archivedate = 11 February 2012
  | archiveurl = http://www.webcitation.org/65MynnD7Q
  | ref =  
  }}
 

==External links==
* 

 

 
 

 
 
 
 