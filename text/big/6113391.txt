Good Day for a Hanging
 
{{Infobox film
| name           = Good Day for a Hanging
| image          = Good Day for a Hanging - 1959- Poster.png
| image_size     =
| caption        = Theatrical Poster
| director       = Nathan H. Juran
| producer       = Charles H. Schneer
| writer         = John H. Reese Daniel B. Ullman Maurice Zimm
| narrator       =
| starring       = Fred MacMurray Robert Vaughn
| music          =
| cinematography = Henry Freulich
| editing        = Jerome Thoms
| distributor    = Columbia Pictures
| released       =  
| runtime        = 85 min.
| country        = United States
| language       = English
| budget         =
}}
Good Day for a Hanging (1959) is a western concerning how a town views the  upcoming hanging of a young man accused of murdering the Marshal during a robbery. Directed by Nathan H. Juran, it stars Fred MacMurray as the reluctant new Marshal and Robert Vaughn as the young man accused of the crime.

==Plot==
Eddie Campbell watches a stagecoach as it rides toward a Nebraska town. Other members of his gang are waiting there to rob the bank. Ben Cutler, owner of the stage line, is on board, and is returning home to be wed to Ruth Granger.

During the holdup, a bank teller is killed. Ben joins the posse. His daughter, Laurie, is in love with Eddie and doesnt believe him to be truly bad. Eddie shoots the Marshal, however. He is wounded by Ben and brought back to town to stand trial.

Ben, who had once been a lawman but gave up the profession after his daughter was born is offered a temporary job as Marshal. Selby, a publicity-seeking lawyer who defends Eddie, insinuates that Ben was just acting in vengeance because his client had been intimate with Bens daughter. Ben punches him for this slur on Lauries character.

Eddie is found guilty due to Bens eyewitness testimony. He is sentenced to hang. Townspeople begin to have their doubts, even Ruth, partly due to Eddies manipulation of their emotions. Laurie tries to smuggle a gun to Eddies cell, but her father finds it.

Ben must ride to the states capital after a plea for clemency from the governor is made. As soon as he is gone, Eddies gang busts him out of jail. He physically assaults Laurie, revealing his true nature. Ben returns just in time, though, and shoots a fleeing Eddie atop the gallows.

==Cast==
* Fred MacMurray as Ben
* Robert Vaughn as Eddie
* Joan Blackman as Laurie
* Margaret Hayes as Ruth
* Denver Pyle as Deputy Ed 
* James Drury as Dr. Paul
* Wendell Holmes as Tallant
* Edmon Ryan as Attorney Selby
* Phil Chambers as Deputy William
* Kathryn Card as Molly
* Emile Meyer as Hiram

==External links==
* 
* 

 

 
 
 
 
 
 
 
 


 