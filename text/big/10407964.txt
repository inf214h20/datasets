For the Love of Mike
{{Infobox film 
| name           = For the Love of Mike
| image          = For-the-love-of-mike-1927.jpg
| caption        = movie poster
| director       = Frank Capra Robert Kane
| writer         = J. Clarkson Miller Leland Hayward
| based on       =   George Sidney Hugh Cameron
| music          =
| cinematography = Ernest Haller
| editing        =
| studio         = Robert Kane Productions
| distributor    = First National Pictures
| released       =  
| runtime        = 75 min
| country        = United States
| awards         =
| language       = Silent English Intertitles
| budget         =
}} silent romantic drama film. Directed by Frank Capra, it starred Claudette Colbert (in her film debut) and Ben Lyon. 
{{cite news
|url=http://movies.nytimes.com/movie/91978/For-the-Love-of-Mike/overview
|title=For the Love of Mike (1927)
|publisher=New York Times
|accessdate=2009-08-18
| first=Mordaunt
| last=Hall}}  
{{cite web
|url=http://ftvdb.bfi.org.uk/sift/title/1491
|title=For the Love of Mike (1927)
|publisher=British Film Institute
|accessdate=2009-08-17}}  It is now considered to be a lost film. 

==Plot==
A baby boy is found abandoned in a Hells Kitchen, Manhattan|Hells Kitchen tenement and subsequently is raised by three men: a German delicatessen owner (Sterling), a Jewish tailor (Sidney), and an Irish street cleaner (Cameron). They adopt the boy and raise him as their own. The timeline jumps 20 years into their future. The now-grown Mike (Lyon) resists going to college because he does not wish to be a financial burden to his adoptive fathers, however a pretty Italian girl, Mary (Colbert) working at the delicatessen convinces him to go.

Mike enrolls at Yale and gains a reputation as a sports hero.  He disavows his three fathers, which leads to the Irishman giving him a thrashing in front of the boys best friends.  He begins to associate with gamblers and ends up owing them money. To settle his debts, they demand he purposely lose the schools big rowing match with Harvard. His three fathers and the girl come to support him during the race, and he defies the gamblers and wins the race. His three fathers then come forward to confront and deal with the gamblers. 

==Cast==
*Ben Lyon as Mike
*Claudette Colbert as Mary George Sidney as Abraham Katz
*Ford Sterling as Herman Schultz Hugh Cameron as Patrick OMalley
*Richard "Skeets" Gallagher as Coxey Pendleton
*Rudolph Cameron as Henry Sharp
*Mabel Swor as Evelyn Joyce

==Background==
For the Love of Mike is based on the story Hells Kitchen by John Moroso. 
{{cite book
|authors=Robert Sklar, Vito Zagarrio
|title=Frank Capra: authorship and the studio system
|editor=Robert Sklar, Vito Zagarrio
|publisher=Temple University Press
|year=1998
|edition=illustrated
|pages=51
|isbn=1-56639-608-5
|oclc=9781566396080
|url=http://books.google.com/books?id=s3ewZWu0YJEC&pg=PA51&dq=%22For+the+Love+of+Mike%22,+Capra&ei=qmuLSomoBY_4lQTQ6vWBCg#v=onepage&q=%22For%20the%20Love%20of%20Mike%22%2C%20Capra&f=false}}   Frank Capra himself referred to the film as his first flop. 
{{cite book
|last=Capra
|first=Frank
|title=The name above the title: an autobiography
|publisher=Da Capo Press
|year=1997
|edition=illustrated
|pages=73–78 ,93, 166
|isbn=0-306-80771-8
|oclc=9780306807718
|url=http://books.google.com/books?id=x_E09IWRomMC&pg=PA73&lpg=PA73&dq=%22For+the+Love+of+Mike%22,+Frank+Capra&source=bl&ots=3-weoMKT8s&sig=vNNfzqy9eYam2fEi3ty4WMO1l_k&hl=en&ei=h0WLSp70IpTiswPfhrnHDQ&sa=X&oi=book_result&ct=result&resnum=1#v=onepage&q=%22For%20the%20Love%20of%20Mike%22%2C%20Frank%20Capra&f=false}}  
{{cite book
|last=McBride
|first=Joseph
|title=Frank Capra: the catastrophe of success
|publisher=Simon & Schuster
|year=1992
|edition=illustrated
|isbn=0-671-73494-6
|oclc=9780671734947
|url=http://books.google.com/books?id=Ia1aAAAAMAAJ&q=%22For+the+Love+of+Mike%22,+%22lost+film%22&dq=%22For+the+Love+of+Mike%22,+%22lost+film%22&ei=wmaLSrSLGJPOlQSP65ieCg}}  Having recently ended his association as writer for actor Harry Langdon, this became Capras first opportunity to direct a New York based production. 
{{cite web
|url=http://www.allmovie.com/work/91978
|title=For the Love of Mike
|work=Allmovie
|first= Hal Robert Kane had contracts to provide First National Pictures with 10 films, and had planned each to be financed with profits from the preceding. 

For the Love of Mike was the tenth in this package, funding was limited, and agent Leland Hayward convinced Capra into deferring his salary until the end of production.   Capra was never paid for his participation.   The film was considered a commercial failure despite a strong cast and decent production values. 

==Preservation status==
The film had distribution problems and is now believed to be a lost film, with no print currently known to exist.  
{{cite web
|url=http://www.classicfilmguide.com/index.php?s=essays&item=26
|title=Claudette Colbert
|publisher=Classic Film Guide
|accessdate=2009-08-19}} 

The film is marked as being the screen debut of Claudette Colbert and her only silent film appearance.  
{{cite book
|last=Rausch
|first=Andrew J.
|title=Hollywoods All-Time Greatest Stars: A Quiz Book
|publisher=Citadel Press
|year=2003
|isbn=0-8065-2469-3
|oclc=9780806524696
|url=http://books.google.com/books?id=8NtQldfmjUgC&pg=PA38&dq=%22Claudette+Colbert+%22,+first+film&ei=kkuKSonvNYuskATAhpWWCg#v=onepage&q=%22Claudette%20Colbert%20%22%2C%20first%20film&f=false
|accessdate=August 17, 2009}}  
{{cite news
|url=http://news.google.com/newspapers?id=k6UQAAAAIBAJ&sjid=KZcDAAAAIBAJ&pg=5662,2122688&dq=for-the-love-of-mike+capra
|title=Studio Flashes
|date=July 17, 1937
|publisher=The Age
|accessdate=2009-08-18}}     After the film received poor reviews and failed financially, Colbert vowed, "I shall never make another film". 
{{cite book
|last=Stempel
|first=Tom
|title=Screenwriter, the life and times of Nunnally Johnson
|publisher=A. S. Barnes
|year=1980
|edition=illustrated
|isbn=0-498-02362-1
|oclc=9780498023620
|url=http://books.google.com/books?id=vKlZAAAAMAAJ&q=%22For+the+Love+of+Mike%22,+Capra&dq=%22For+the+Love+of+Mike%22,+Capra&ei=jWiLSsuGApPclQSqpOmgCg}}  However, two years later, she signed with Paramount Pictures. 
{{cite web
|url=http://www.tcm.com/tcmdb/participant.jsp?spid=36555
|title=Biography for Claudette Colbert
|publisher=Turner Classic Movies
|accessdate=2009-08-19}} 

==Reception==
Mordaunt Hall of New York Times wrote that the film "makes no pretensions of being anything but a movie. It seemed to satisfy the audience at the Hippodrome yesterday afternoon, for there was laughter and, at the end, applause", and concluded "Claudette Colbert, who was seen in Kenyon Nicholsons play "The Barker," lends her charm to this obstreperous piece of work. She seems quite at home before the camera". 
{{cite news
|url=http://movies.nytimes.com/movie/review?res=9E0CE4D9103FE03ABC4C51DFBE66838C639EDE
|title=The Screen; Ben Lyon, the Athlete
|last=Hall
|first=Mordaunt
|date=August 24, 1927
|publisher=New York Times
|accessdate=2009-08-18}} 

==See also==
*List of lost films

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 