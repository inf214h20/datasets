Company and the Crazy
{{Infobox film
| name           = Company and the Crazy
| image          = 
| caption        = 
| director       = Mario Almirante
| producer       = 
| writer         = Camillo Bruto Bonzi Gino Rocca
| starring       = Vasco Creti
| music          = 
| cinematography = Massimo Terzano
| editing        = 
| distributor    = 
| released       = 3 November 1928
| runtime        = 
| country        = Italy
| language       = Silent
| budget         = 
}}
 Italian film directed by Mario Almirante. The film features an early onscreen performance from Vittorio De Sica.   

==Cast==
* Vasco Creti - Momi Tamberlan
* Carlo Tedeschi - Bortolo Cioci
* Alex Bernard - Piero Scavezza
* Elena Lunda - Irma
* Lili Migliore - Ginetta
* Cellio Bucchi - Conte Bardonazzi
* Vittorio De Sica - Prof. Rosolillo
* Giuseppe Brignone - Sioria
* Felice Minotti
* Giuseppe Migliore
* Andrea Miano
* Amilcare Taglienti

==References==
 

==External links==
* 

 
 
 
 
 
 