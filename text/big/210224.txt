Love with the Proper Stranger
{{Infobox film
| name           = Love with the Proper Stranger
| image          = Love With The Proper Stranger.jpeg
| caption        = Film poster
| director       = Robert Mulligan
| producer       = Robert Mulligan Alan J. Pakula
| writer         = Arnold Schulman Steve McQueen  Edie Adams  Herschel Bernardi Harvey Lembeck Tom Bosley
| music          = Elmer Bernstein
| cinematography = Milton R. Krasner
| editing        = Aaron Stell
| distributor    = Paramount Pictures
| released       =  
| runtime        = 102 minutes
| country        = United States
| language       = English
| budget         = $8.5 million
| gross          = $3,600,000  (rentals)  
}} Mulligan Productions and Boardwalk Productions and released by Paramount Pictures.  It was directed by Robert Mulligan and produced by Alan J. Pakula from a screenplay by Arnold Schulman.
 Steve McQueen, Edie Adams, Herschel Bernardi and Harvey Lembeck. The film also marked the screen debut of Tom Bosley and features a brief, uncredited appearance by the directors younger brother Richard Mulligan, who later became a well known television actor.
 Jack Jones.

==Plot== Steve McQueen). When she tracks him down he doesnt remember her. She does not expect him to marry her; all she wants is enough money to pay for an abortion. Meanwhile, Angie is being pressured by her older brothers, played by Herschel Bernardi and Harvey Lembeck, to marry the unappealing cook Anthony (Tom Bosley).

Rocky scrapes up money for the crude backroom abortion. But when he and Angie meet the abortionist, who turns out not to be a doctor, Rocky refuses to let her go through with the dangerous procedure. The maturity he shows in doing this brings them closer. After meeting her brothers, Rocky decides to "take his medicine" by marrying her. Angie is insulted and refuses. Angie wants a love relationship, with "bells and banjos."

As an act of independence Angie moves out of the family home. She begins dating Anthony, who offers to marry her. By acting aloof she attracts Rocky, whom she invites to dinner. At dinner he makes advances on her and is rejected. Angie says she doesnt want to make the last mistake. They quarrel and she throws him out. The next day, Rocky waits for her outside Macys, ringing bells and playing a banjo, and wins her over.

==Accolades==

The film was nominated for five Academy Awards for:    Best Actress in a Leading Role (Natalie Wood) Best Art Direction-Set Decoration Black-and-White (Hal Pereira, Roland Anderson, Sam Comer, Grace Gregory) Best Cinematography, Black-and-White (Milton R. Krasner) Best Costume Design, Black-and-White (Edith Head) Best Writing, Story and Screenplay - Written Directly for the Screen (Arnold Schulman).
The film was also nominated for two Golden Globes Awards for Steve McQueen and Natalie Wood in the Best Actor and Best Actress categories.

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 
 
 