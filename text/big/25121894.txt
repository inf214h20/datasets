Música en espera
{{Infobox film
| name           = Música en espera
| image          = Música en espera.jpg
| alt            =  
| caption        = Film poster
| director       = Hernán A. Golfrid
| producer       = Daniel Botti Daniel Burman
| writer         = Julieta Steinberg Patricio Vega
| starring       = Natalia Oreiro Diego Peretti Norma Aleandro Carlos Bermejo
| music          = Guillermo Guareschi
| cinematography = Lucio Bonelli
| editing        = Luis Barros Alejandro Brodersohn
| studio         = 
| distributor    = 
| released       =  
| runtime        = 106 minutes
| country        = Argentina
| language       = Spanish
| budget         = 
| gross          = 
}}
Música en espera (Music on hold) is an Argentine film protagonized by Natalia Oreiro and Diego Peretti. It premiered on March 19, 2009, and was the most seen movie in its starting week. 

==Plot==
The story starts with musician Ezequiel (Diego Peretti) having only a few days left to prepare the soundtrack of a movie in production. He calls the bank to request a delay in his debt payment and is transferred from one office to another. When he is transferred to Paulas office he finds the inspiration he needs to finish his soundtrack in her hold music. However, he only hears it for a moment. 

Paula (Natalia Oreiro) is 9-months pregnant and was left by her boyfriend Santiago early in her pregnancy. Her mother (Norma Aleandro), deceived by Paula into thinking that she is still with her boyfriend, arrives from Spain in order to meet him and be at the birth. 

Ezequiel meets Paula at the bank. After they talk about his debt he requests to hear her hold music, but finds a different song playing as the songs are randomly assigned each day. He and Paula make a deal: he will pretend to be Santiago in front of Paulas mother while she helps him to locate the music. After many failed attempts he never does find the inspiring hold music, but instead finds inspiration during Paulas birth. He successfully composes the soundtrack. Ezequiel and Paula finally become a couple.

== Cast ==
* Diego Peretti
* Natalia Oreiro
* Norma Aleandro
* Carlos Bermejo
* Rafael Spregelburd
* Pilar Gamboa
* Atilio Pozzobón
* María Ucedo
* Elvira Villariño
* Rafael Ferro

==References==
 

 
 
 
 
 
 
 

 