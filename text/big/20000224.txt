Jewel of the Sahara
{{Infobox Film | name = Jewel of the Sahara
     | image = Jewelofthesahara01.jpg
     | caption  = United States theatrical poster
     | director  = Ariel Vromen
     | executive producer  = Ariel Vromen
     | producers  = Kevin Keyser
     | screenplay  = Ariel Vromen Alon Aranya Kennedy Taylor
     | Cinematographer = Gerardo Mateo Madrazo
     | starring  = Gerard Butler
     | genre    = Comedy Short  Fantasy
     | released  = June 28, 2001 (USA)
     | runtime  =  17 min.
     | country  = United States
     | language  = English 
  }}

Jewel of the Sahara is a 2001 film starring Gerard Butler, Clifford David as the old Francois Renard and Peter Franzén  as the young Francois Renard. ‘Jewel of the Sahara’ is comedy / short / fantasy film directed by Ariel Vromen. He also wrote the screenplay, was the executive producer and edited the film. The movie was made by Keyser Productions.

==Plot==
Set in a French Foreign Legion Camp circa 1954, the film follows the fantasies of a British Captain, desperately missing his home and wife. The captain is caught in an embarrassing situation caused by a combination of the monotonous, hot dreary surroundings, not grasping the workings of the Foreign Legion, and his smoldering desire created by his wifes lustful love letters, all of which is befuddled by his use of drugs.

==Cast==

*Gerard Butler  - Captain Charles Belamy
*Clifford David  - Old Francois Renard
*Peter Franzén  - Young Francois Renard
*Ori Pfeffer  - Mahmud
*Ralph Lister  - Harold Belamy
*Nicholl Hiren  - Sargent
*Gian Saragosa  - Gamal
*Ari Averbach
*Andre Alfa  - Legionnaire #8
*Gahl Sasson  - Legionnaire #1
*Rodrigo Madrazo  - Legionnaire #2
*Tomer Almagor  - Legionnaire #3
*Lionel Renard  - Legionnaire #4 (as Leonal Renard)
*Gili Pinchuck  - Legionnaire #5
*Santiago Barriero  - Legionnaire #6
*Kevin Keyser  - Legionnaire #7

==Crew==

*producer – Kevin Keyser
*associate producer – Tony Lunn
*executive producer – Ariel Vromen
*writers – Alon Aranya (story), Kennedy Taylor (story),  Ariel Vromen
*original music – Gahl Sasson
*cinematography – Gerardo Mateo Madrazo
*film editing – Ariel Vromen
*casting – Ariel Vroment
*production design – Erica Vilardi
*costume design – Erica Vilardi

==External links==
* 
* http://movies.gerardbutlerangels.com/Jewel.html

*Image Gallery: http://www.gerard-butler.net/gallery/v/career_movies/Jewel+of+The+Sahara/

 
 
 
 
 

 