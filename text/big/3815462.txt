The Creature Walks Among Us
 
{{Infobox film| name =The Creature Walks Among Us
  | image =The Creature Walks Among Us.jpg
  | caption = film poster by Reynold Brown John Sherwood
  | producer = 	William Alland
  | writer = Arthur A. Ross
  | starring =Jeff Morrow Rex Reason Leigh Snowden
  | cinematography = Maury Gertsman
  | music =
  | editing =
  | distributor = Universal-International
  | released = United States: April 26, 1956
  | runtime = 78 min
  | country = United States
  | language = English
  | budget =
}}
The Creature Walks Among Us is the third and final installment of the Creature from the Black Lagoon horror film series from Universal-International, following 1955s Revenge of the Creature. The film was released April 26, 1956, in the United States.

==Plot==
Following the Gill-mans escape from Ocean Harbor, Florida, a team of scientists led by the deranged and cold-hearted Dr. William Barton capture him in the Everglades. During the capture, the creature is badly burned in a fire.  While bandaging the Gill-man, the doctors notice that he is shedding his gills and even breathing using a kind of lung system.  Now that the creature has more human-like skin, he is given clothing. The doctors attempt to get the Gill-man used to living among humans. Though his life is saved, he is apparently unhappy, staring despondently at the ocean. Barton ruins the plans when, in a murderous rage, he kills guide Jed Grant, who had made romantic advances toward his wife, Marcia.  Realizing what he has done, Barton then tries to put the blame on the Gill-man.  The Gill-man, witnessing the killing, and apparently comprehending that he is being blamed for the murder, goes on a rampage.  After ripping down the confining electric fence, he kills Barton and then slowly walks back to the sea. He is last seen on a beach, advancing towards the ocean, but with his human-like lungs, the Gill-mans return to the ocean will lead to death from drowning.

==Characters==

===William Barton===
Dr. William Barton leads an expedition to capture the Gill Man. He is seen as a mentally unstable scientist and an apparently abusive husband to his wife Marcia, as he becomes very jealous and paranoid when Marcia is with other men. At the films climax, he beats guide Jed Grant to death after learning he tried to rape Marcia. As punishment for his acts, the Gill Man chases him through his house up to the second floor and throws him to his death.

===Marcia Barton=== Gill Man, Marcia goes along.
 paranoid about the two.
 dive to scuba gear. This forces Jed and Tom to abandon their hunt for the Gill Man to swim back and save her.
 surgical transformation by William, Tom and their colleagues Dr. Borg and Dr. Johnson, Marcia mistakenly believes that after such a success her husband would be in better spirits. On the contrary, after taking some criticism from Tom about the morality of the procedure, William explodes and yells at his wife during a celebratory dinner party aboard the Vagabondia.
 unconscious before being recaptured by Tom. Later, the tension-filled expedition returns to California.

After William murdered Jed for trying to woo his wife, and Williams own demise at the hands of the enraged Gill Man, Marcia decides that she had had enough, and moves as far away from their house and memories of William as possible, despite developing a mild romantic interest in Tom.

===Jed Grant=== Gill Man. An extremely self-assured, amorous man, Grant becomes instantly infatuated with Bartons wife, Marcia Barton|Marcia, feeling that she should leave her abusive and mentally unstable husband and have an affair with him instead.
 surgical transformation of the Gill Man by Barton and his team, Jed, who is supposed to be watching over the recuperating creature in the operating room aboard the Vagabondia III, leaves his post and tries to get close to Marcia. This quickly develops into attempted sexual assault, but Jed is stopped when the Gill Man escapes from the operating room and strikes him across the midsection, knocking him unconsciousness|unconscious.

For unknown reasons, Marcia never tells anyone about the attempted sexual assault by Jed. Thus, Jed is welcomed as a guest at the Bartons house in California, though Barton, who suspects that Marcia and Jed were having an affair, first tried to force Jed to leave. When Jed pushes the right button by muttering that Marcia hated her husband, an enraged Barton kills Jed by clubbing him to death with a pistol.

==Cast==
* Jeff Morrow (Dr. William Barton)
*Rex Reason (Dr. Thomas Morgan)
*Leigh Snowden (Marcia Barton)
*Gregg Palmer (Jed Grant)
*Ricou Browning (Gillman – In Water)
*Don Megowan (Gillman – On Land)

==Production notes==
Unlike the previous two Creature films, The Creature Walks Among Us was not filmed in 3-D film|3-D. The underwater scenes were filmed at Wakulla Springs in North Florida, today a state park.

==External links==
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 