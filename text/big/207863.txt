Creature Comforts
 
 
{{Infobox television|
| show_name = Creature Comforts
| image =  
| caption =
| picture_format =   (1989)   (2003–2007)
| runtime = 10 minutes
| creator = Nick Park
| country = United Kingdom
| language = English ITV (UK) CBS/Animal Planet (US)
| first_aired = Original Film: 15 July 1989 Series: 1 October 2003
| last_aired =  18 June 2007
| status = Ended
| num_series = 2 + 1 film
| num_episodes = 27
| producer = Aardman Animations
| related =
| list_episodes = List of Creature Comforts episodes

}}
Creature Comforts is a franchise originating in a 1989 British humorous animated short film of the same name. The film describes how animals feel about living in a zoo, featuring the voices of the British public "spoken" by the animals. It was created by Nick Park and Aardman Animations. The film later became the basis of a series of television advertisements for the UK electricity boards, and in 2003 a television series in the same style was released. An American version of the series was also made.

==The original film== Vox Pop Lip Synch for Channel 4. The film won Nick Park the Academy Award for Best Animated Short Film in 1990.
 puma and a young hippopotamus who complain about the cold weather, the poor quality of their enclosures and the lack of space and freedom. By contrast, a tarsier and an armadillo praise their enclosures for the comfort and security they bring, and a family of polar bears talk about both the advantages and disadvantages of zoos for the welfare of animals. Rather than the subject being one-sided or biased towards one view-point, there is a strong balance of opinions in the film, with some interviewees who are happy with their living situation, some who are not, and some who have a neutral opinion.

The voices of each character were performed by residents of both a housing estate and an old peoples home. Stop motion animation|Stop-motion animation was then used to animate each character, and the answers given in the interviews were put in the context of zoo animals. The polar bears were voiced by a family who owned a local shop, while the mountain lion was voiced by a Brazilian student who was living in the UK, but missed his home country.

==The advertisements==
In 1990 Nick Park worked with Phil Rylance and Paul Cardwell to develop a series of British television advertisements for the electricity boards "Heat Electric" campaign. The creative team of advertising agency GGK had seen the original Creature Comforts film and were hugely impressed by it. They were convinced that a series of short films modelled on the original film would be ideally suited to television advertising – as long as the advertising was handled with sufficient sensitivity to preserve the integrity and charm of Parks work. The initial result of their collaboration was three 30-second Creature Comforts advertisements, made in the same style as the original film. This led to a series.

Although there had been a tradition of vox pop advertisements going back to the soap powder adverts of the 1960s, the Creature Comforts series was distinctive in its juxtaposition of real-life dialogue and animated creatures. The series featured a variety of endearing plasticine animals, including a tortoise, a cat, a family of penguins and a Brazilian parrot. The characters were seen in their own domestic settings, chatting to an unseen interviewer behind a large microphone.

The characters dialogue was obtained by taking tape recordings of everyday people talking about the comfort and benefits of the electrical appliances in their homes and then using extracts of these – complete with pauses, false starts, repetitions, hesitations and unscripted  use of language (such as "easily turn-off-and-on-able"). The selected interviewees spoke in a range of down-to-earth regional accents, and the overall effect was of natural conversation.  The adverts warm and cosy tone reflected the warmth and homeliness of central heating.
 Animal Magic.

The campaign was a great success  and its run was extended over three years. The advertisements received critical acclaim within the advertising industry – with Park, Rylance and Cardwell picking up many top creative awards in Europe and America, including "Best Commercial of the Year" in the 1991 British Television Advertising Awards and "Most Outstanding European Campaign" in the 1991 D&AD Europe Awards. In fact Creature Comforts was subsequently voted by media professionals (in leading trade outlets Marketing and Brand Republic) as one of the top television advertisements of the last 50 years. 
 . Retrieved 2010-08-01. 
 ITV in 2005.  Finally, in a YouGov survey during 2006, Creature Comforts topped the list of the UKs alltime favourite animated or puppet characters used in adverts. 

The Creature Comforts advertisements have now attained a place in popular culture, and are probably better remembered than the original film that spawned them.  However, it is claimed that many members of the public mistakenly remember the commercials as advertising gas heating, the main competitor to electricity. 

=== Influences ===

The Creature Comforts advertisements were produced in the period 1990 to 1992 and in some ways they were indicative of the shape of things to come in British television advertising. Many commentators believe that there was a fundamental shift in television advertising from the unbridled consumerism and egoism of the 1980s to what is sometimes  termed a more "caring" approach in the 1990s. The Creature Comforts advertisements are cited as an early example of this phenomenon. 

The format of the Creature Comforts advertisements was so successful that it was replicated in other campaigns in the following decades. In later years, however, members of the public became increasingly conscious of the potential uses of their vox pop interviews.  This made it difficult to recapture the spontaneity and innocence of the early Creature Comforts advertisements. Although lookalike animations became relatively commonplace in television advertisements, they were usually scripted and rarely possessed the painstaking attention to detail of the original advertisements. 

===Credits===
* Director: Nick Park
* Creative Director: Nick Fordham
* Art Directors: Phil Rylance, Newy Brothwell
* Writers: Paul Cardwell, Kim Durdant-Hollamby

==The series== ITV by The Netherlands on Veronica (television channel)|Veronica, on pay-TV channel US.TV, and on Internet peer-to-peer TV Joost Aardman Animations Channel.
 The Twelve CBC on 26 December 2005. 

Humour pervades all aspects of the series, for example:
*A highly philosophical speech given by an amoeba. her neighbourhood, the sewer. aquaphobic sharks and walruses, birds afraid of heights, etc.)
*Background details such as:
**Insects swarming into a gap in the paving stones when a slug mimics a bird call.
**Grey aliens blinking in unison.
**A lab mouse being interviewed while another mouse with a human ear on his back walks by.

The series gently mocks the constructed performance sometimes given by members of the general public when being interviewed for television Vox populi|vox-pops and documentaries. This includes the attempts to present a cogent but simple conclusive answer to a general question - a sound bite - and the attempts to present a cheery spin on a complex issue while the subject attempts to hide their personal issues and problems with the issue.

The series was recently  repeated on Gold (TV channel)|Gold.

==Regular characters==
The following characters are among those who make regular appearances throughout the series. These animals are always portrayed by the same interviewees to maintain consistency throughout the series.

===Introduced in series 1===
* Fluffy - a cynical, cage-bound hamster from Catford.
* Pickles - an optimistic Labrador who works as a guide dog for a blind man. She is often seen sitting by her owners side, but the mans face is never shown on screen.
* Clement - an old bloodhound who talks about his past life experiences.
* Trixie and Captain Cuddlepuss - Trixie is a dog and Captain Cuddlepuss is a cat. They sit on a red sofa and frequently argue about trivial things. They are the most recurring regular characters in the series.
* Gary and Nigel - two garden slugs who mostly talk about plants and gardening.
* Sid and Nancy - two rats who live in a garden shed.
* Frank - an elderly tortoise who originally appeared in the Heat Electric television adverts. CGI animated character.
* Mazulu and Toto - two monkeys. In the first series, Mazulu and Toto are shown as performing monkeys sitting in a cage and wearing matching pink and blue outfits, but in the second series they are shown without their outfits and living in a forest with other wild monkeys. seagulls who stand on a landfill site.
* Spanner and Trousers - two stray dogs who sit in a skip.

===Introduced in series 2===
* Victor - a mouse who speaks in a thick Geordie accent and lives in a doll house.
* Derek - An elderly Shar Pei with a Welsh accent. He sits next to a small Shar Pei puppy who does not speak.
* Brian and Keith - two Staffordshire bull terriers who are brothers.
* Muriel and Catherine - a pair of elderly bats who roost in a belfry.
* Behzad - an Arabian horse who has several different jobs, including a police horse, a member of the Queens Guard at Buckingham Palace, and a faux Christmas reindeer.

==Miscellaneous==
A special short was aired in the UK as part of Red Nose Day 2007. 

==American version==
 Americanized version CH system.

CBS also created a web presence with the help of the Creature Comforts staff. A behind-the-scenes collaborative account/blog of each episode was posted in conjunction with the 3 short-lived airings. 

The American version was co-produced by Aardman Animations and The Gotham Group.
 cancelled by CBS due to low ratings.  Its remaining episodes were later premiered on Animal Planet in 2008 (see below).

A standard DVD of the shows seven episodes was released on 9 October 2007 by Sony, now entitled Creature Comforts America. Currently there is no Blu-ray version, even though the show was mastered in 1080 HD 16x9. 

On 8 February 2008, the show won an Annie Award for "Best Animated Television Production" of 2007. 

In Australia, public broadcaster ABC Television began airing the American season in Australia on 18 February 2008, having aired the original British version since its inception on both ABC1 and the digital only ABC2.

On 24 April 2008, Animal Planet picked up the first season of the American version. It was broadcast in both SD letterbox and native HD formats. Episodes 1&2 premiered on 24 April, Episodes 3&4 premiered on 1 May and Episodes 5&6 premiered on 9 May.

Creature Comforts was nominated for an Emmy Award for "Outstanding Animated Program (For Programming Less Than One Hour)" but on 13 September 2008, it lost out to The Simpsons. Teresa Drilling, one of the shows many animators, won an individual Emmy Award for "Outstanding Individual Achievement in Animation".

===US version staff===
* Executive Producers: Kit Boss, Miles Bullough, Peter McHugh, David Sproxton, Peter Lord, Nick Park
* Producers: Kenny Micka, Gareth Owen
* Story Editors: Chad Carter, June Raphael, Casey Wilson
* Writers: Kit Boss, Chad Carter, Michael Dougan, Ben Stout, June Raphael, Casey Wilson
* Directors: David Osmand, Merlin Crossingham

==Creature Discomforts==
A series of four ads highlighting disability and featuring the voices of disabled people telling of their experiences premiered on ITV on Christmas Day 2007. Four more ads featuring new characters debuted in Summer 2008.  

==DVD releases==
{| class="wikitable"
|-
! DVD title
! Country of release Region
! Date of release
! DVD company
! Catalog Number
! Notes
|-
| Creature Comforts
| United Kingdom
| 1
| 28 November 2000
| Image Entertainment
| ID0106CUDVD
| The original 1989 film presented in widescreen. Also includes the other Aardman animations Wats Pig, Not Without My Handbag and Adam
|-
| Creature Comforts &mdash; Series 1, Part 1
| United Kingdom
| 2
| 17 November 2003
| Momentum Pictures
|
| The first half of Series 1, the original 1989 film, and other extras
|-
| Creature Comforts &mdash; Series 1, Part 2
| United Kingdom
| 2
| 5 April 2004
| Momentum Pictures
|
| The second half of Series 1, featuring many extras including ITV1 idents, Heat Electric ads, and more
|-
| Creature Comforts &mdash; The Complete First Season
| United States
| 1
| 27 September 2005
| Sony Pictures Home Entertainment
| 08694
| Features the original 1989 film in full-screen as an extra
|-
| Creature Comforts &mdash; Complete Series 1
| United Kingdom
| 2
| 31 October 2005
| Momentum Pictures
|
| 2-disc set of the first series.
|-
| Creature Comforts &mdash; Series 2, Part 1
| United Kingdom
| 2
| 21 November 2005
| Momentum Pictures
|
| First half of Series 2, plus many making of extras.
|-
| Creature Comforts &mdash; Series 2, Part 2
| United Kingdom
| 2
| 20 February 2006
| Momentum Pictures
|
| Second half of Series 2, plus extras
|-
| Creature Comforts &mdash; The Complete Second Season
| United States
| 1
| 24 October 2006
| Sony Pictures Home Entertainment
| 14823
| 2-disc set of the Second Series and the "Merry Christmas, Everybody" DVD release  
|-
| Creature Comforts &mdash; Merry Christmas Everybody
| United States
| 1
| 24 October 2006
| Sony Pictures Home Entertainment
|
| Also included in the Second Series DVD release
|-
| Creature Comforts &mdash; The Complete First and Second Seasons
| United States
| 1
| 24 October 2006
| Sony Pictures Home Entertainment
|
| 2-Disc set of the First and Second Series
|-
| Creature Comforts &mdash; Complete Series 2
| United Kingdom
| 2
| 6 November 2006
| Momentum Pictures
|
| 3-disc set of the Second Series and "Merry Christmas Everybody"
|-
| Creature Comforts America &mdash; The Complete First Season
| United States
| 1
| 9 October 2007
| Sony Pictures Home Entertainment
|
| The seven episodes made for the cancelled American version
|}

The TV Series (UK Version) is now available to watch on the official Aardman YouTube channel. ( )

==See also==
*List of Creature Comforts episodes

==References==
 

==External links==
 
 
*   Official website
*  
*   at AtomFilms
*  
*  
*  
*  

 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 