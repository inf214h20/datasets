Colonel Wolodyjowski (film)
{{Infobox film
| name           = Colonel Wolodyjowski aka Pan Wołodyjowski
| image          = Pan Wolodyjowski plakat.jpg
| director       = Jerzy Hoffman
| writer         = Jerzy Hoffman, Jerzy Lutowski
| starring       = Tadeusz Łomnicki, Daniel Olbrychski, Mieczyslaw Pawlikowski, Magdalena Zawadzka
| based on       =  
| music          = Andrzej Markowski
| cinematography = Jerzy Lipman
| released       =  
| runtime        = 160 minutes
| language       = Polish
}} Pan Wołodyjowski, by the Polish writer Henryk Sienkiewicz. The film was also serialized on Polish television, as The Adventures of Sir Michael (Polish: Przygody pana Michała). 

It was entered in the 6th Moscow International Film Festival, where Tadeusz Łomnicki won the award for Best Actor.   

The story is set during the Ottoman Empires invasion of Poland in 1668-72.

==Cast==
* Tadeusz Łomnicki as Jerzy Michał Wołodyjowski 
* Magdalena Zawadzka as Basia 
* Mieczysław Pawlikowski as Onufry Zagłoba
* Hanka Bielicka as Makowiecka 
* Barbara Brylska as Krzysia Drohojowska 
* Irena Karel as Ewa Nowowiejska 
* Jan Nowicki as Ketling Hassling of Elgin 
* Daniel Olbrychski as Azja Tuhaj-Bejowicz 
* Marek Perepeczko as Adam Nowowiejski  Jan Sobieski 
* Władysław Hańcza as Nowowiejski 
* Gustaw Lutkiewicz as Lusnia 
* Tadeusz Schmidt as Snitko 
* Andrzej Szczepkowski as Bishop Lanckoroński 
* Leonard Andrzejewski as Halim
* Bogusz Bilewski as Officer 
* Wiktor Grotowicz as Potocki 
* Tadeusz Kosudarski as Nobleman  Andrzej Piątkowski as Turk 
* Ryszard Ronczewski as Bandit 
* Tadeusz Somogi as Envoy 
* Witold Skaruch as Monk

==See also== With Fire and Sword The Deluge

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 


 
 