The Search for General Tso
 
The Search for General Tso is a documentary film that premiered at the 2014 Tribeca Film Festival. It was directed by Ian Cheney and produced by Amanda Murray and Jennifer 8. Lee.    Sundance Selects acquired it in December 2014, and was released January 2, 2015, in theaters and on demand. 

==Development==
Around 2004, Cheney and his best friend were driving across America when they stopped at a Chinese restaurant "with red booths and neon signs" and ordered General Tsos chicken. The experience prompted them to investigate the history of Chinese food in America. 
 Chinese food in the United States and around the world, titled The Fortune Cookie Chronicles,    documenting the process on her blog. She reported the unlikely, but true, story of how a batch of fortune cookies created 110 Powerball lottery winners. {{cite news 
|title=Who Needs Giacomo? Bet on the Fortune Cookie |author=Jennifer 8. Lee |date=May 11, 2005 |url=http://www.nytimes.com/2005/05/11/nyregion/11fortune.html
|newspaper=The New York Times}}  To the surprise of many non-Chinese readers, she reported that fortune cookies are found in many countries but not China and that fortune cookies may have originated in Japan. {{cite news |title=Solving a Riddle Wrapped in a Mystery Inside a Cookie |author=Jennifer 8. Lee |date= January 16, 2008
|url=http://www.nytimes.com/2008/01/16/dining/16fort.html |work=  to promote the book.   The book was #26 on The New York Times Best Seller list|The New York Times Best Seller list. 

The book research is the basis of Lees documentary collaboration with Cheney. In addition to premiering at Tribeca, the film played at the Seattle International Film Festival,  AFI Docs,  and the Independent Film Festival of Boston. 

==Synopsis==
The films opening explores theories about General Tso, before moving to China,    where few recognize the eponymous dish.  The film then traces Tsos real-life history in the Qing Dynasty as well as the history of Chinese immigration to America.  Interviewed are a number of notable figures in Chinese food, such as Cecilia Chiang of The Mandarin and a world record-holder for restaurant menus.    Also interviewed are Chef Peng Chang-kuei, who invented the dish in China, and David Leong, who coined the Americanized version. 

==Critical reception==
Film reviews were generally positive, with critics finding the premise amusing and the conclusion thought-provoking. John Foundas of Variety (magazine)|Variety called it "a finger-lickin good foodie docu" and John DeFore of The Hollywood Reporter predicted, "Festival auds should eat it up."  

==References==
 

== External links ==
*  
*  
*  
*  
*  
*  
*  

 
 
 
 
 
 
 