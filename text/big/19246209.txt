Bhalobasa Bhalobasa (2008 film)
 
{{Infobox film
| name = Bhalobasa Bhalobasa
| image = Bhalobasabhalobasa.jpg
| caption = Movie Poster
| director = Rabi Kinagi
| based on =  
| writer =
| starring = Sabyasachi Chakrabarty Hiran Chatterjee Srabanti Malakar
| producer =Ashoke Kumar Dhanuka
| distributor = Eskay Movies
| cinematography =
| editing =
| released = 2 October 2008
| country = India
| budget =  
| gross = 
| preceded_by =
| followed_by =
| runtime = 155 minutes
| language = Bengali
| music = S. P. Venkatesh Devi Sri Prasad
}}

Bhalobasa Bhalobasa (translation: Love Love) is a 2008 Bengali film by Rabi Kinagi. This movie is the second film of both Hiran Chatterjee and Srabanti Malakar. After five-week running success prompted producer Ashok Dhanuka added one more song "Bol Raja Chai Kemon Rani" to the film after shooting it right after Kali Puja. This is the first Bengali film which  was shot at Salzburg in Austria.    {{cite web
|url=http://www.telegraphindia.com/archives/archive.html|title=On song, Hiran|publisher=www.telegraphindia.com|accessdate=2008-11-08|date=6 November 2008|last=|first=| archiveurl= http://web.archive.org/web/20081112044743/http://www.telegraphindia.com/archives/archive.html| archivedate= 12 November 2008  | deadurl= no}}   This film is the remake of the Telugu film Bommarillu directed and co-written by Bhaskar (director)|Bhaskar, starring Prakash Raj, Genelia DSouza and Siddharth Narayan. {{cite web
|url=http://www.tollynewz.com/newz/1-tollynewz/82-most-expensive-bengali-movie-.html|title=Most expensive Bengali movie
|publisher=www.tollynewz.com|accessdate=2009-02-09|last=|first=}}    The film primarily revolves around a father and son relationship with the fathers dote on his son ironically leaving a bitter taste with the latter. The Title Track music of this film  is also taken from Bommarillu.

==Plot==
The film begins with a baby taking his initial steps on a beach while the father is supporting him. The narrator in the background (Prasenjit) states that, it is right for a father to support his child in his infancy, but questions whether the father should continue to hold the child’s hand even after he is 24 years old. As the credits roll, a visibly angry Siddhu (Hiran Chatterjee) begins uttering abuses at all the fathers in the world. When inquired about his disgust, he says that his father, Aravind (Sabyasachi Chakrabarty), gives him more than what he asks for. He cites instances where his choices of dressing, hairdo and many others are stashed away by his father’s. However, he vows that the two things that will be of his choice would be, his career and the woman he would marry.
 engaged to Subbulakshmi (Locket Chatterjee) against his wishes. He speaks with her only to realize that she is a "daddy’s girl" and she not being to his liking. However, with Arvind’s final say, they eventually get engaged.

While contemplating on his options in a temple, Siddhu accidentally meets Priya (Srabanti Malakar), an engineering student. Seeing her chirpy nature and vibrance, Siddhu begins to like her. He makes attempts to know her by meeting her on a regular basis. In the process, he starts liking her cherubic and ever-friendly nature and as someone who does what she loves. As days go by, he realizes about so many small things in her company that gave him happiness. He realizes that he has fallen in love with her.

Alongside this, Siddhu applies for a bank loan to start out on his dream of building his career. When his love for Priya deepens, he wishes to propose to her. He confesses to her that he is engaged to get married to Subbulakshmi against his wishes, but what he really wants is her. On knowing of him being engaged, Priya gets dejected, but comes back a day later and asks him to do what he wishes for and accepts his proposal. At this juncture, the ecstatic Siddhu is seen by a furious Arvind. Siddhu is admonished back home and he expresses his disinterest in marriage with Subbulakshmi. When asked for his reason to like Priya, Siddhu replies saying that if Priya can stay with their family for a week, then all their questions shall be answered. He convinces Priya to stay at his house after lying to her father, Kanaka Rao (Sushanta Dutta) that she is going on a college tour.

When Priya is introduced to Siddhu’s family, she gets a lukewarm welcome. As she settles down in the house, one after the other begins to like her. Even though getting used to the living habits of the authoritarian Arvinds household was difficult, Priya stayed put for Siddhus sake. In the meanwhile, Arvind reprimands Siddhu when he knows of his bank loan and his plans, only to further enrage Siddhu. One day the entire family along with Priya attends a marriage ceremony. A cheerful Priya cheers up the ceremony with her playful nature. Coincidentally, Kanaka Rao who happens to be around, recognizes Siddhu as the drunken young man whom he encountered on an earlier occasion. Priya realizes her fathers presence and quickly exits to avoid his attention. After saving their grace, Siddhu admonishes Priya for her antics at the marriage. A sad and angry Priya moves out of the house saying that she does not find Siddhu the same and that she cannot put on an act if she stays in their house. After getting back to her house, she rebuilds the trust her father has in her while Siddhu is left forlorn. Lakshmi confronts Arvind on Siddhu’s choices and wants. In the process, Siddhu opens up his heart and leaving Arvind to repent on his foolishness. Siddhu requests Subbulakshmi and her parents to call off the impending marriage. While they relent, Arvind manages to convince Kanaka babu about Siddhu and Priyas marriage. In return, Kanaka  wants to know more about Siddhu by having him live in house for a week. Arvind agrees with this and as the story returns to the pre-credits scene, the viewers are left to assume about the happy marriage of the protagonists.

==Cast==
* Hiran Chatterjee - Siddhu
* Srabanti Malakar - Priya
* Sabyasachi Chakrabarty - Aravind (Siddhus dad)
* Sushanta Dutta&nbsp;— Kanaka Rao (Priyas dad)
* Laboni Sarkar&nbsp;— Lakshmi (Siddhu’s mom)
* Locket Chatterjee&nbsp;— Subbulakshmi, the fiancé of Siddhu
* Subhasish Mukherjee&nbsp;— Satya (Servant)
* Kamalika Banerjee

==Crew==
* Producer:Ashoke Kumar Dhanuka
* Director:Rabi Kinagi
* Story: Bhaskar
* Production Design:
* Dialogue:
* Lyrics:  Goutam Susmit
* Editing:
* Cinematography:
* Music Director: S. P. Venkatesh

==Music==
The music of Bhalobasa Bhalobasa, composed by S. P. Venkatesh, was released in India on 22 September 2008  The soundtrack has been at the number one spot on the music charts for several consecutive weeks.  Media partner of Bhalobasa Bhalobasa is Bengali Music Channel Sangeet Bangla.

{{Infobox album |  
 Name =Bhalobasa Bhalobasa|
 Type = Album |
 Artist = S. P. Venkatesh|
 Cover = Bhalobasabhalobasa1.jpg|
 Released =  22 September 2008 (CD release)| Feature film soundtrack |
 Length = |
 Label =  V Music |
 Producer =  |
 Reviews = |
 Last album = Bondhoo  (2007) |
 This album = Bhalobasa Bhalobasa (2008) |
 Next album = -   (-)|
}}

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Song !! Singer(s)!! Duration
|-
| "Bol Raja Chai Kemon Rani"
| Priya , Trijoy, Niladri
| 4:10
|-
| "Boloto Lokey Basle Bhalo" Shaan
| 5:45
|-
| "Halka Halka Ei Ektu Melamesha"
| Shaan and Chorus
| 5:45
|-
| "Ektu Lajja Chokhe Ektu Lajja Mukhe"
| Alka Yagnik, Shaan
| 5:41
|-
| "Gurujana Bole Prem Karona"
| Priya, Amit Ral, Md.Aziz
| 4:10
|-
| "Bhenga Gelo Aaj Swapno Amar"
| Kumar Sanu
| 5:10
|}

==Critical reception==
The film received generally positive reviews and was a hit at the box office. 

==References==
 

==External links==
*  
*  

 
 

 
 
 
 
 
 