Un couple épatant
 
 
{{Infobox film
| name           = Un couple épatant 
| image          =
| caption        = 
| director       = Lucas Belvaux
| producer       = Diana Elbaum Patrick Sobelman
| writer         = Lucas Belvaux
| starring       = Ornella Muti François Morel (actor)|François Morel Gilbert Melki
| music          = Riccardo del Fra
| cinematography = Pierre Milon
| editing        = Valérie Loiseleux
| distributor    = Diaphana Films  
| released       =  
| runtime        = 97 minutes
| country        = France Belgium
| language       = French
}}
Un couple épatant ( ; also known as Trilogy: Two) is a 2002 French-Belgian film written and directed by Lucas Belvaux.

This is the second installment of a series Trilogy, which constitutes a  , a  , a melodrama.

Belvaux referred in the DVD commentary that main idea behind Trilogy is that the main characters in a particular story are the secondary characters of others, in such sense the three films happen at the same time and share a series of common scenes and plot points, complementing each other, but also have their own perspective and style. The audience is left with piecing the films together, which Belvaux avoided, since editing the three films into one single narrative would have resulted in a very long film with no style of its own.

==Plot==
  will projects that he dictates in a pocket tape recorder that he carries around, the first will distributes his assets 50/50% to his wife and children, 98% of his company shall go to his children and the remaining 2% to Claire and some moneys that he has in a Swiss bank-account shall go to the Fund for Cancer Research.
 keys to a chalet Alain owns in the nearby mountains, which he used for a romantic escapade.
 Jaguar with exhaust changed as Alain had said earlier.

The next day after a debate at school, Cécile asks Pascal to tail Alain because he is hiding something from her. Manise tells her that it is preferable to be honest and ask her husband directly but she refuses. Meanwhile in his office Alain has a sudden spasm and begins dictating his ever growing symptoms in his recorder, he leaves the office (which Claire reports to Cécile, who has been keeping a record of all his comings and goings) and goes to the park where he meets Louise, who tells her father that Henry dumped her because she cheated on him. Manise is close by and all he sees is that Alain is meeting with a younger woman, but when he reports it to Cécile she recognizes her daughter and is relieved that there is not another woman. At school Agnès is very worried because Manise had to arrest Jeanne, a colleague teacher and friend.

Manise asks Cécile to check the bank accounts for unusual charges, she does so despite Claires warnings that Alain shall notice, they find nothing. During  .
 withdrawal syndrome. Before they can leave Alain is assaulted by thugs. Alain then goes to Claires and forces a confession from her. Alain goes back home and tells Cécile that he must go to Paris for a couple of days.
  lover that he took to the chalet, looks out the window and sees Manise talking to Dr. Colinet in the parking lot. He flees but no taxi shall take him.

As Cécile is about to enter the school where she works, Manise tells her that Alain did not take the   back. When backing up to the main road, Alain spots his wifes car with another car behind her. He follows and hears a man inside his chalet singing in Italian. He then assumes that his wife is connected to the Mafia and are plotting to kill him because of some of his inventions, Claire does not see the "connection". Meanwhile Dr. Colinet has reported Alains disappearance to Manise. Alain and Claire go to the police but see Manise and Dr. Colinet walking out of the station together and conclude that they are together in the plot with his wife.
 prescription is signed by Dr. Georges Colinet, thus Vincent is in the plot too. Cécile comes back home and finds that Louise has moved back in, when she discovers the reason (Louise had cheated on her boyfriend and he dumped her) she is so angry that she slaps her daughter and also draws a conclusion that Agnès is the one having an affair with Alain. She picks Agnès at the street and drives up to the chalet but finds that a man named Pierre is there and it seems that Agnès story is true. Cécile confronts Georges about the truth related to her husbands whereabouts but he does not betray anything and acts surprised when she tells him that Alain is not in Paris.

Alain has interrogated Vincent and although he does not get a confession, he ties Vincent to Claires bed. They go to Alains place where he connects his recording machine to the telephone to spy on Cécile, he is discovered by Louise who then agrees to call him if anything strange happens related to Cécile. During this conversation it is revealed that the only reason that his children sang "Les roses blanches" to him is because he sang that song all the time regardless of the occasion, because it was the only one he knew.
 wife beater. As they are about to leave, Manise tells Cécile that he is in love with her. She becomes very angry and slaps him and explodes with all her cruelty to him and leaves.

Meanwhile, Alain is taking Vincent to the hospital but when he sees Dr. Colinet, he goes bezerk and crashes Vincent into the wall and beats Georges until the orderlies finally stop him. After he is restrained Alain gets a call from Louise, Cécile has not shown up for work, he calls the police, Manise goes back for Cécile and they go to her house. Alain finally reveals the reason for his secrecy and she is more at ease but not before she consults with Georges about the real gravity of his situation, a conversation that is spied by Alain through the window.

==Cast==
{| class="wikitable"
|-
! style="background-color:tan;" width="150" | Actor
! style="background-color:silver;" width="200" | Role
|-
| Ornella Muti || Cécile Coste, High school teacher
|-
| François Morel (actor)|François Morel || Alain Coste, an inventor
|-
| Valérie Mairesse || Claire, Alains personal assistant|PA
|-
| Dominique Blanc || Agnès Manise, High school teacher, wife of Inspector Manise
|-
| Gilbert Melki || Pascal Manise, Police Inspector
|-
| Lucas Belvaux || Bruno Le Roux, posing as Pierre, an unemployed bump
|-
| Catherine Frot || Jeanne Rivet, High school teacher and friend of Cécile
|-
| Yves Claessens || Freddy, a bar owner
|-
| Bernard Mazzinghi || George, surgeon and friend of the Costes
|-
| Raphaële Godin || Louise Coste
|-
| Patrick Depeyrrat || Patrick
|-
| Vincent Colombe || Rémy Coste
|-
| Pierre Gérard || Olivier
|-
| Jean-Baptiste Montagut || Henri
|-
| Anne Delol || Nurse
|- Taxi driver
|}

==External links==
* 

 
 

 
 
 
 
 
 
 
 
 