Running Wild (1992 film)
{{Infobox Film |
  name = Running Wild |
  image = Running wild.jpg |
  director = Duncan McLachlan |
  producer = John Varty Duncan McLachlan|
  writer = Andrea Buck Duncan McLachlan John Varty|
  starring = Brooke Shields Martin Sheen David Keith|
  music = Grant Innes McLachlan |
  editing = John Letegan Duncan McLachlan|
  cinematography = Justin Fouche|
  distributor =  Columbia TriStar Home Video|
  released = 1992 |
  runtime = 94 min. |
  country = | English |
  budget = |
    }}
 1992 film|movie starring Brooke Shields, Martin Sheen and David Keith. The movie was written by Andrea Buck, Duncan McLachlan and John Varty.  

==Plot==
A journalist for a struggling television station travels to Africa to meet conservationist and filmmaker John Varty, who has been following a mother leopard for several years. She believes this would make an interesting story for the stations viewers. However, things dont work out as planned as one of the stations executives is trying to stop her filming idea and the unfortunate death of the mother leopard.

==Main cast==
*Brooke Shields ...  Christine Shaye
*Martin Sheen ...  Dan Walker
*David Keith ...  Jack Hutton
*Norman Anstey ...  Van Heerden
*Dale Dicky ...  Judith
*Renée Estevez ...  Aimee
*Greg Latter ...  Stevens
*John Varty ...  Himself

==References==
 

==External links==
* 

 
 
 
 


 