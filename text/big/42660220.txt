La vida de Jose Rizal
{{Infobox film
| name           = La vida de Jose Rizal
| image          = 
| caption        =
| director       = Edward Gross
| producer       = Harry Brown
| writer         = Edward Gross
| starring       = Honorio Lopez Titay Molina Chananay
| music          = 
| cinematography = Charles Martin
| editing        = 
| distributor    = Rizalina Photoplay Company
| released       =  
| runtime        = 
| country        = Philippines
| language       = 
| budget         = 
}}
La vida de Jose Rizal (The life of Jose Rizal), released in 1912 in film|1912, was the first feature film produced in the Philippines. It was however not the first film released in the country—a rival film, El fusilamiento de Dr. Jose Rizal was released on August 22, one day earlier.  La Vida is a silent film that depicts the life of José Rizal, the countrys national hero, from his birth to his execution in Luneta.   

Americans Harry Brown (producer), Charles Martin (cinematographer), and Edward Gross (scenerist), founded the Rizalina Photoplay Company in 1912 to produce the film, which was adapted from a 1905 stage play by Gross.  Containing 22 scenes, the movies film had a length of 5,000 feet. 

The film, together with its rival, garnered financial success and the producers were encouraged to produce other Filipino-themed films. 

==Cast==
* José Rizal - Honorio Lopez
* María Clara - Titay Molina
* Teodora Alonso Realonda - Chananay

==References==
 

==External links==
*  

 
 
 


 