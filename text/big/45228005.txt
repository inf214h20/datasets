Sri Madvirata Parvam
{{Infobox film
| name           = Sri Madvirata Parvam
| image          =
| caption        =
| writer         = Kondaveeti Venkatakavi  
| story          = N. T. Rama Rao
| screenplay     = N. T. Rama Rao 
| producer       = N. T. Rama Rao
| director       = N. T. Rama Rao
| starring       = N. T. Rama Rao   Vanisri   Nandamuri Balakrishna
| music          = Susarla Dakshinamurthi
| cinematography = M. A. Rehman
| editing        = K. Babu Rao   N. S. Prasad
| studio         = Ramakrishna Cine Studios   
| released       =  
| runtime        = 2:35:29
| country        =   India
| language       = Telugu
| budget         =
| gross          = 
}}

Sri Madvirata Parvam is a 1979 Telugu cinema|Telugu, Mythological film produced & directed by N. T. Rama Rao on his Ramakrishna Cine Studios banner. Starring N. T. Rama Rao, Vanisri, Nandamuri Balakrishna in the lead roles and music composed by Susarla Dakshinamurthi.        NTR Performed five pivotal roles in the film as Lord Krishna, Arjuna, Duryodhana, Brihannala & Kichaka after the film Daana Veera Soora Karna (1977) in which he had done three different roles.

==Plot==
The film was a picturization of Virata Parva from Mahabharata.

==Cast==
 
*N. T. Rama Rao as Lord Krishna, Arjuna, Duryodhana, Brihannala & Kichaka (Five roles)
*Vanisri as Draupadi
*Nandamuri Balakrishna as Abhimanyu  Satyanarayana as Bhima & Ghatotkacha (Duel role) Prabhakar Reddy as Yudhisthira
*Mukkamala as Virata Mikhilineni as Bhishma Dhulipala as Shakuni Rajanala as Drona Uttara Kumarudu Uttara
*Chhaya Devi as Hidimbi
*Prabha as Satyabhama 
*Pushpalata as Sudeshna  Vijayalalitha as Urvashi
 

==Soundtrack==
{{Infobox album
| Name        = Sri Madvirata Parvam
| Tagline     = 
| Type        = film
| Artist      = Susarla Dakshinamurthi
| Cover       = 
| Released    = 1979
| Recorded    = 
| Genre       = Soundtrack
| Length      = 27:24
| Label       = Sea Records
| Producer    = Susarla Dakshinamurthi
| Reviews     =
| Last album  =   
| This album  = 
| Next album  = 
}}

Music composed by Susarla Dakshinamurthi. Music released on Sea Records Audio Company. 
{|class="wikitable"
|-
!S.No!!Song Title !!Singers !!length
|- 1
|Ramani Pilichindira Vani Jayaram
|4:37
|- 2
|Jeevithame Krishna Sangeetamu
|M. Balamuralikrishna
|3:43
|- 3
|Aadave Hamsagamana
|M. Balamuralikrishna
|5:22
|- 4
|Nirajanam Jayanirajanam
|P. Susheela
|2:22
|- 5
|Muddiste Pulakintha Madhavapeddi Ramesh,P.Susheela
|3:25
|- 6
|Neku Nene Sari SP Balu,Madhavapeddi Ramesh,P.Susheela
|3:30
|- 7
|Manasayara Matipoyara SP Balu,S. Janaki
|4:25
|}

==Others==
* VCDs and DVDs on - Universal Videos, SHALIMAR Video Company, Hyderabad

==References==
 

 
 
 


 