Stallion Road
 

{{Infobox film
| name           = Stallion Road
| caption        =
| image	=	Stallion Road FilmPoster.jpeg
| director       = James V. Kern Raoul Walsh (uncredited)
| producer       = 
| writer         = Stephen Longstreet (screenplay and original novel)
| starring       =  Frederick Hollander
| cinematography = Arthur Edeson
| editing        = David Weisbart
| distributor    = Warner Brothers
| released       = 12 April 1947
| runtime        = 97 min.
| country        = United States
| language       = English 
| budget         =
}}

Stallion Road is a 1947 American film starring Ronald Reagan, Alexis Smith and Zachary Scott, and directed by James V. Kern.

==Synopsis==
Stallion Road is a sweeping romantic melodrama set in the 1940s California pitting a veterinarian (Reagan) and a romance novelist (Scott) in a bid for the affections of a beautiful rancher (Smith). Reagan and Smith assume roles that were apparently once earmarked for Humphrey Bogart and Lauren Bacall after their 1946 hit The Big Sleep. A delightfully witty script brings the best out of the stars and Scott shines as the cynical, world weary, but extremely charming novelist. Reagan exerts his usual tough, laconic screen persona which gels nicely with that of the rugged yet endearing rancher played by Smith. The love triangle between Reagan, Smith and Scott is played out with sophistication, wit, humour and heartfelt. Adding to the strengths of the film is a wonderful set of supporting actors. Stallion Road is a melodrama that adheres to the formulas that made this type of film so popular and powerful in the golden age of Hollywood.

== Cast ==

*Ronald Reagan as Larry Hanrahan
*Alexis Smith as Rory Teller
*Zachary Scott as Stephen Purcell
*Peggy Knudsen as Daisy Otis
*Patti Brady as Chris Teller Harry Davenport as Dr. Stevens
*Angela Greene as Lana Rock
*Frank Puglia as Pelon
*Ralph Byrd as Richmond Mallard
*Lloyd Corrigan as Ben Otis
*Fernando Alvarado as Chico
*Matthew Boulton as Joe Beasley

==External links==
* 
*  

==See also==
* Ronald Reagan films

 
 
 
 
 
 
 


 