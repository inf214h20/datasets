Captain America (serial)
 
 

{{Infobox film
| name           = Captain America
| image          = Captain-america serial poster.jpg John English
| producer       = William J OSullivan
| writer         = Story: Royal Cole   Joe Simon Russell Hicks John Davidson
| cinematography =John MacBurnie Earl Turner
| music          = Mort Glickman
| distributor    =Republic Pictures
| released       =   
| runtime         = 15 chapters / 243 minutes 
| country = United States
| language = English
| budget          = $182,623 (negative cost: $222,906) 
| awards          = 
}} Republic black-and-white serial film loosely based on the Timely Comics (today known as Marvel Comics) character Captain America. It was the last Republic serial made about a superhero.  It also has the distinction of being the most expensive serial that Republic ever made. It also stands as the first theatrical release connected to a Marvel character; the next theatrical release featuring a Marvel hero would not occur for more than 40 years.

The serial sees Captain America, really District Attorney Grant Gardner, trying to thwart the plans of The Scarab, really museum curator Dr. Cyrus Maldor - especially regarding his attempts to acquire the "Dynamic Vibrator" and "Electronic Firebolt", devices that could be used as super-weapons.

In a rare plot element for Republic, the secret identity of the villain is known to the audience from the beginning, if not to the characters in the serial.  The studios usual approach was the use of a mystery villain who was only unmasked as one of the other supporting characters in the final chapter.

==Plot==
A rash of suspicious suicides among scientists and businessmen, all found holding a small scarab, gets the attention of Mayor Randolph.  He demands that Police Commissioner Dryden and District Attorney Grant Gardner get to the bottom of the case, while openly wishing that Captain America, a masked man who has helped defeat crime in the past, were around to solve the mystery. Gail Richards, Grant Gardners secretary, investigates and realises someone knows of the "Purple Death", a hypnotic chemical responsible for the suicides. However he then pulls out a gun and takes her into another room. He then orders an associate to tie her up. The D.A. realises she is there and forces the man to take him to her. He finds her tied up and gagged. He frees her but it is threatened that the purple death will be dropped killing them all. But the D.A. shoots him then gets out of the room with Gail.
 Mayan ruins.  One of the few remaining survivors, Professor Lyman, turns to his friend Dr. Maldor for support.  Dr. Maldor, however, reveals that he is the man responsible for the deaths.  He wants revenge because he planned and organised the expedition but everyone else claimed the fame and fortune.  However, Lyman has developed the "Dynamic Vibrator" - a device intended for mining operations but one that can be amplified into a devastating weapon.  Using his purple death Dr. Maldor forces Lyman to disclose the location of his plans.

Captain America intervenes as the Scarabs heavies attempt to steal the plans and this leads to a sequence of plots by the Scarab to acquire a working version, as well as other devices, while trying to eliminate the interfering Captain before he succeeds in discovering Dr. Maldors true identity or defeats him.

==Cast==
*Dick Purcell as Captain America. The character drastically differs from his comic book counterpart, who was a frail soldier named Steve Rogers who underwent a super-soldier experiment to enhance his physiology to the peak of human perfection. These elements are completely omitted and the characters identity is changed to a District Attorney named Grant Gardner. Purcell was cast as the hero despite, as described by Harmon and Glut, having an average and slightly overweight physique.   He died a few weeks after filming was completed; he collapsed in the locker room at a Los Angeles country club.  In the opinion of film historian Raymond Stedman, the strain of filming Captain America had been too much for his heart. 
*Lorna Gray as Gail Richards, Grant Gardners secretary
*Lionel Atwill as Dr Cyrus Maldor/The Scarab
*Charles Trowbridge as Police Commissioner Dryden Russell Hicks as Mayor Randolph
*George J. Lewis as Bart Matson John Davidson as Gruber
*Stanley Price as Purple Death chemist

==Production==
Captain America was budgeted at $182,623 although the final negative cost was $222,906 (a $40,283, or 22.1%, overspend).  It was the most expensive of all Republic serials (as well as the most over budget). {{cite book
 | last = Mathis
 | first = Jack
 | title = Valley of the Cliffhangers Supplement 
 | year = 1995
 | publisher = Jack Mathis Advertising
 | isbn = 0-9632878-1-8
 | pages = 3, 10, 74–75
 | chapter =  Harry Fraser’s only work at Republic. {{cite book
 | last = Cline
 | first = William C.
 | title = In the Nick of Time
 | year = 1984
 | publisher = McFarland & Company, Inc.
 | isbn = 0-7864-0471-X
 | chapter = 4. The Plotters of Peril (The Writers)
 | page = 61
 }} 

The Captain America costume was really grey, white and dark blue as these colours photographed better in black and white.  The costume also lost the wings on the head, the pirate boots became high shoes and the chainmail became normal cloth.  Miniature flags were added to the gloves and the belt buckle became a small shield. {{cite book
 | last = Harmon
 | first = Jim
 |author2=Donald F. Glut  
 | authorlink = Jim Harmon
 | title = The Great Movie Serials: Their Sound and Fury 
 | year = 1973
 | publisher = Routledge
 | isbn = 978-0-7130-0097-9
 | pages = 255, 258–259, 263
 | chapter = 10. The Long-Underwear Boys "Youve Met Me, Now Meet My Fist!"
 }} 

Republic was notorious for making arbitrary changes in their adaptations.  This occurred with Captain America more than most.  Timely Comics|Timely, the owner of Captain America, was unhappy with the omission of Steve Rogers, the lack of an army setting and his use of a gun.  Republic responded in writing that the sample pages provided by Timely did not indicate that Captain America was a soldier called Steve Rogers, nor that he did not carry a revolver.  They also noted that the serial was well into production by this point and they could not return to the original concept without expensive retakes and dubbing.  Finally they pointed out that Republic was under no contractual obligation to do any of this. 
 Spy Smasher.  For example: Private Steve Rogers. 
*The "Super-Soldier Serum" origin is not used. shield does not appear, replaced by a standard gun. Nazis are not part of the story in any way.
*His sidekick, Bucky, does not appear.

The reason for the differences appears not to be arbitrary, but that the script for the serial originally featured an entirely different licensed lead character and it was only decided later to replace the original character with Captain America.  Film historians  . This idea, however, is highly questionable considering that Republic owned the Copperhead character and could have done as they pleased with him without any licensing issues. 
 Captain Marvel and Spy Smasher).  Due to the fact that the lead in Captain America is a crime-fighting district attorney aided by a female secretary who knows his identity, and that the serial includes a chapter entitled "The Scarlet Shroud" in which nothing scarlet appears, film restoration director Eric Stedman suggests that it is more likely that the script was originally developed to feature Fawcetts comic book hero Mr. Scarlet, secretly D.A. Brian Butler, whose comic book appearances had proved unpopular and who had actually disappeared from comic book covers and been relegated to being a backup feature between the time the serial was planned and the final film produced. 

Writer Raymond William Stedman believes that the differences between the comic-book and film versions of Captain America were "for the better" as, for example, the hero did not have to sneak out of an army base every time he needed to change identities. {{cite book
 | last = Stedman
 | first = Raymond William
 | title = Serials: Suspense and Drama By Installment
 | year = 1971
 | publisher = University of Oklahoma Press
 | isbn = 978-0-8061-0927-5
 | chapter = 5. Shazam and Good-by 
 | page = 131
 }} 

===Stunts===
*Dale Van Sickel as Captain America (doubling Dick Purcell)
*Bert LeBaron as Dr Maldor/The Scarab (doubling Lionel Atwill)
*Helen Thurston as Gail Richards (doubling Lorna Gray)
*Ken Terrell Bart Matson/Dirk (doubling George J. Lewis & Crane Whitley)
*John Bagni Fred Graham
*Duke Green
*Eddie Parker
*Allen Pomeroy Tom Steele

Dale Van Sickel was the "ram rod" of the stunt crew, doubling Dick Purcell as Captain America.  Ken Terrell doubled George J. Lewis and Fred Graham doubled Lionel Atwill.  Additional stunts were performed by Duke Green and Joe Yrigoyen.  Tom Steele only appeared in chapter one as he was busy on The Masked Marvel. 

===Special effects===
All the special effects in Captain America were created by Republics in-house team, the Lydecker brothers.

==Release==
Captain Americas official release date is 5 February 1944, although this is actually the date the seventh chapter was made available to film exchanges.   The serial was re-released on 30 September 1953, under the new title Return of Captain America, between the first runs of Canadian Mounties vs. Atomic Invaders and Trader Tom of the China Seas. 

==Critical reception==
Captain America is regarded as the "apex of the traditional action film fight...  opinion of many cliffhanger enthusiasts."   Stedman wrote that this was a "much better serial than either Batman (serial)|Batman or The Masked Marvel"  Dr Maldor is, in Clines opinion, Lionel Atwills best serial role. {{cite book
 | last = Cline
 | first = William C.
 | title = In the Nick of Time
 | year = 1984
 | publisher = McFarland & Company, Inc.
 | isbn = 0-7864-0471-X
 | chapter = 7. Masters of Menace (The Villains)
 | page = 113
 }} 

==References to the serial==
In Captain America V1 issue 219 (March 1978), it is revealed that a Captain America serial also exists within the Marvel Continuity. In this version, Captain America himself plays the role (in secret), taking the place of the stunt man who was shot during production due to the prop master being the Nazi spy Lyle Decker. Like the real-life serial, Caps shield is replaced with a standard gun, his identity is changed, and his sidekick Bucky is absent.
 Civil War event, Captain America (Steve Rogers) was seemingly killed off. News channel CNN produced a special on the death, showing the serial with Grant Gardner as Captain America while it was focusing on the death of Steve Rogers. In Issue 27  of Captain America, the movie poster is seen in The Captain America Museum.

Steve Rogers ex-girlfriend in the Ultimate Marvel continuity is named after Gail Richards, Grant Gardners secretary.

The 2011 film   features the title character starring in a serial early in his career.

==Chapter titles==
# The Purple Death (25min 40s)
# Mechanical Executioner (15min 38s)
# The Scarlet Shroud (15min 33s)
# Preview of Murder (15min 33s)
# Blade of Wrath (15min 33s)
# Vault of Vengeance (15min 33s)
# Wholesale Destruction (15min 34s)
# Cremation in the Clouds (15min 33s)
# Triple Tragedy (15min 33s)
# The Avenging Corpse (15min 33s)
# The Dead Man Returns (15min 33s)
# Horror on the Highway (15min 34s)
# Skyscraper Plunge (15min 33s)
# The Scarab Strikes (15min 32s)
# The Toll of Doom (15min 33s)
 Source:   {{cite book
 | last = Cline
 | first = William C.
 | title = In the Nick of Time
 | year = 1984
 | publisher = McFarland & Company, Inc.
 | isbn = 0-7864-0471-X
 | chapter = Filmography
 | page = 237
 }} 

==See also==
* List of film serials by year
* List of film serials by studio
* List of films in the public domain

==References==
 

==External links==
* 
* 

 
{{succession box  Republic Serial Serial 
| before=The Masked Marvel (1943)
| years=Captain America (1944) The Tiger Woman (1944)}}
 

 
 
 

 
 
 
 
 
 
 
 
 
 