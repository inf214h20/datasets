High School Musical: El Desafio Mexico
 
High School Musical: El Desafίo is a  , with the special participation of the musical duo Jesse & Joy. As is filmed in Mexico, most of the soundtrack are of the genre Reggeton.

==Plot==
Over the summer, Cristóbal (Cristóbal Orellana), the captain of the school football team, Los Borregos, discovers that his neighbor and classmate, Mariana (Mariana Magaña), has changed a lot over the summer. Meanwhile, Luli (Mar Contreras) is still as vain as ever and overshadows her poor brother, Fernando (Fernando Soberanes), and his companions, whom she refers to as "The Invisibles." A new school year begins at High School Mexico (HSM, initially interpreted), and the school has announced a "Battle of the Bands" contest hosted by Jesse & Joy. Working against the clock and with limited resources, the guys put forces for the big day. Cristobal, Mariana, and a few of their peers, come together and form a band called "Fair Play". In an effort to win, Luli dares the impossible task of separating Cristobal from his friends. But only one band will be the winner; the one who understands that teamwork, personal development, and hard work will make them better artists and also better people.

==Cast of characters==
{| class="wikitable"
! colspan="3" | Main Cast
|-
! width="150" | Character
! width="250" | Actor
! width="400" | Notes
|-
| Cistóbal Rodriguez || Cristóbal Orellana || The male protagonist; Soccer team captain for the Borregos.
|-
| Mariana Galindo || Mariana Magaña || The female protagonist; Shy and brainy, bust suddenly a talented singer.
|-
| Luli Casas Del Campo || Mar Contreras || The female antagonist; The undisputed star of the school.
|-
| Fernando Casas Del Campo || Fernando Soberanes || The male antagonist until recruited to choreograph for Cristobals band.
|- Jorge Blanco
|-
| Stephie || Stephie Camarena
|-
! colspan="3" | Supporting Cast
|-
! width="150" | Character
! width="250" | Actor
! width="400" | Notes
|-
| Juan Carlos  || Juan Carlos Flores
|-
| Faby || Fabiola Paulin
|-
| César || César Viramontes
|-
| Pau || Paulina Holguin
|-
| Caroline || Carolina Ayala
|-
| Mr. Rodrigues || Juan Manuel Bernal || The father of Cristobal.
|-
| Mrs. Galindo|| Lumi Cavazos || The mother of Mariana.
|-
| Mrs. Casas Del Campo || Lisa Owen || The mother of Luli and Fernando.
|-
| Mr. Casas Del Campo || Victor Martin Hugg || The father of Luli and Fernando.
|-
| Mr. Guerrero || Álvaro Guerrero || The school principal.
|-
| Angelina || Carmen Beato
|-
| Joy  || Joy Huerta Uckey
|-
| Jesse || Jesse Huerta Uckey
|-
| Marifé || Carla Medina
|-
| Cook  || Roger González
|}

==Other media==
===Soundtrack===
The August 15 launch of the CDs songs film.titulado The songs were originally proposed for the same movie Argentina, composed by Fernando Lopez Rossi, but with different arrangements (e.g. reggaeton), and included a Bonus: Up Doo sung by antagonists. In addition to the Mexican version was the cover "Dime Ven" Motel of banda and the song "Life is an Adventure" performed by Mariana and Fernando, consisting of Jesse and Joy for the movie.

# El Verano Terminó  ( ) 
# Siempre Juntos  ( ) 
# La Vida Es Una Aventura  (Mariana Magaña, Fernando Soberanes) 
# Yo Sabia  (Cristóbal Orellana, Mariana Magaña) 
# A Buscar El Sol  (Mariana Magaña) 
# Hoy Todo Es Mejor  (Cristóbal Orellana) 
# Dime Ven  ( ) 
# Superstar  (Mar Contreras, Paulina Holguin, Fabiola Paulin, Carolina Ayala) 
# Mejor Hacerlo Todos Juntos  (Cristóbal Orellana, Mariana Magaña Mariana, Fernando Soberanes, Juan Carlos Flores, Jorge Blanco, Stephie Camarena, Cesar Viramontes) 
# Actuar, Bailar, Cantar  ( ) 
# Doo Up  (Mariana Magaña, Fernando Soberanes)   

===DVD===
The DVD of the film was released on December 2, 2008, 5 months sooner than previously advertised.

# The Summer Ended (Music Video)
# Segments of "Express" from Disney Channel
# Commercial: "The Challenge Behind the Dream" from Disney Channel

===Novela===
High School Musical: El Desafío was transcribed into the form of a novela, and was released by Random House. The book contains 8 pages of stills from the film.

==External links==
*  
*  

 
 
 