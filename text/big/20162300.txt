Bokator (film)
{{Infobox film
| name           = Bokator
| image          = Bokator poster 1.jpg
| caption        = Australia Threatical poster
| director       = Tim Pek
| producer       =
| writer         = Antonio Graceffo
| narrator       =
| starring       = Sam Kim Sean Antonio Graceffo
| music          =
| cinematography =
| editing        =
| distributor    = Transparent Picture
| released       = TBA 2007
| runtime        = 80 minutes
| country        = Cambodia Khmer  English
| budget         =
| preceded_by    =
| followed_by    =
| website        =
}}

BOKATOR (Khmer:ល្បុក្កតោ) is a Khmer  , while the second part is a mini-film starring martial arts and adventure writer Antonio Graceffo, called “Brooklyn Bokator". 

==Tag Lines==
Before there was Muay Thai, there was Bokator. 

==Plot==
A boxer from Brooklyn with a bad attitude and a fat belly gets beaten up by an old man. Seeking revenge, he returns to a boxing trainer, played by his real-life coach Paddy Carson, asking his coach to get him in shape so he can beat up the old man.

His coach, Patty, tells him, “If an old man beats you, then you must not fight him, you must learn from him.”

“As always, I was honored to play in a Khmer movie. I am so grateful for all of the email and support that has come to me from Khmer people around the globe,” says Graceffo, who receives countless emails daily. “The actual acting was pretty funny. I play a big out of shape boxer from Brooklyn. It wasn’t much of a stretch. The story is, in a lot of ways, based on my own experience of coming to Cambodia to train. For example, in the beginning of the film, my character doesn’t speak Khmer, and he gets a little sick when his training brothers ask him to eat spiders. By the end, he gets used to all of that and he learns to respect the spirit of Angkorian warrior.”

==Release date==
The release date of the Bokator film was set at 2007 but was delayed because Tim, the director, was working on another Khmer film, called “The Red Sense.” It was shot in Australia.  The film is currently in post production, to be released later in 2008. 

Reviews of the film was atrocious and its poor production value and acting are hard to watch for the general viewer.

==Bokator surviving master joins in film==
San Kim Saen survived the Khmer Rouge years, and then returned to Cambodia. He is the man credited with reviving the dying Khmer Martial Art of Bokator. Today he works closely with writers and film makers in an effort to document his country’s art and share it with the world as well as starring in Bokator. 
In addition to the independent film, San Kim Saen and his two American students have worked together on two shows for the History Channel, “Human Weapon,” and “Digging for the Truth.” Bokator will also be featured on the brand new web TV show, Martial Arts Odyssey, hosted by Antonio Graceffo. The show follows Graceffo around the world as he explores new and often obscure martial arts. The pilot is currently running on youtube.com. 

==Origin== Khmer martial art that may be a predecessor to the Southeast Asian kickboxing styles. History indicates that Bokator or an early form thereof was the close quarter combat system used by the ancient armies of Angkor 1000 years ago. 

==Controversy==
Many spectators and Cambodians argue that Bokator was just a term coined by San Kim Saen. Their argument is that the actual name of the ancient art is really called Kbach kun boran Khmer. Also, they argue that Labokator/Boxkator/Bokator is actually the art of twin sticks much like Eskrima of the Philippines.

San Kim Saen was a hapkido teacher who suddenly became a bokator master. He patched together several teachings from what he could find to fabricate a new recreation called Bokator and it was publicized and mouthpieced by Antonio Graceffo.

==References==
 

==External links==
* 

 
 
 
 
 