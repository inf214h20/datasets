Top Bet
 
 
 
{{Infobox film
| name           = The Top Bet
| image          =
| caption        =
| director       = Jeffrey Lau Corey Yuen
| producer       =
| writer         = Jeffrey Lau Ng See-Yuen Corey Yuen
| narrator       =
| starring       = Anita Mui Carol Cheng
| music          =
| cinematography =
| editing        =
| distributor    =
| released       =  
| runtime        = 111 min
| country        = Hong Kong Cantonese
| budget         =
}} Hong Kong comedy film directed by Jeffrey Lau and Corey Yuen and starring Anita Mui, Carol Cheng and Man Tat Ng. It is a sequel to Lau and Yuens 1990 film All for the Winner.

== Plot ==
After Shing (Stephen Chow) used ESP to win Hung Kwong in the Gambling King Competition, Mui, Shings sister, was ordered by the ESP Clan to capture him back as he was not supposed to use ESP for gambling. Meanwhile, Shing was on a world tour and Mui cannot locate him in Third Uncles (Ng Man Tat) home.

==Cast==
* Anita Mui - Mei / Sings sister
* Carol Cheng - The Queen of Gambling
* Ng Man-tat - Blackie Tat
* Lowell Lo - Tai
* Wan-Yin Angelina Lo - Auntie Luke Paul Chun - Hung Kwong
* Lau Shun - Yim Chun
* Jeffrey Lau - Chan Chung
* Corey Yuen - Fishy Shing
* Kenny Bee - Bee
* Fung Woo - Referee at gambling competition
* Shing Fui-On - Brother Shaw/Voice of Auntie Luke (Male Counterpart)
* Yuen Wah - Sifu Wu Lung Lo (cameo)
* Stephen Chow - Sing (cameo)

==External links==
 

 
 
 

 
 
 
 
 
 


 
 