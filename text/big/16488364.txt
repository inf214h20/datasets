The Wrecking Crew (2000 film)
{{multiple issues|
 
 
}}
 
{{Infobox film
| name           = The Wrecking Crew
| image          = The Wrecking Crew FilmPoster.jpeg
| director       = Albert Pyun
| producer       = Paul Rosenblum Tom Karnowski Mark Allen Ice-T
| writer         = Hannah Blue
| screenplay     = 
| story          = 
| narrator       = 
| starring       = Snoop Dogg Ice-T Ernie Hudson Jr. Vincent Klyn Romany Malco T.J. Storm
| music          = 
| cinematography = Philip Alan Waters
| editing        = Errin Vasquez
| studio         = Filmwerks
| distributor    = 
| released       =  
| runtime        = 80 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
The Wrecking Crew is a 2000 film directed by Albert Pyun and starring Snoop Dogg, Ice-T and Ernie Hudson Jr..

==Plot==
A high-level government hit squad is sent into the streets in order to complete a deadly mission.

==Trivia==
* The director, Albert Pyun shot this movie simultaneously with two other "urban"-features, Urban Menace and Corrupt, making sure his producers got the most out of their money.
* Shot in Eastern Europe (presumably Czechoslovakia) on a shoestring budget.
* It was mostly created in order for the rappers Snoop Dogg, Ice-T, Big Pun and others to launch a movie simultaneously with their latest rap-albums. 

== External links ==
*  
*   at the Disobiki.

 

 
 
 
 
 
 
 

 