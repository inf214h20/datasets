The Angelmakers
{{Infobox film
| name           = The Angelmakers
| image          =
| image_size     =
| caption        =
| director       = Astrid Bussink
| producer       =
| writer         =
| narrator       =
| starring       =
| music          =
| cinematography =
| editing        =
| distributor    =
| released       = 2005
| runtime        =
| country        =
| language       =
| budget         =
| preceded_by    =
| followed_by    =
| website        =
}}

The Angelmakers is a 2005 documentary film|documentary, the debut film of filmmaker Astrid Bussink, which provides insight into the epidemic of arsenic murders by women, known as The Angel Makers of Nagyrév, which brought worldwide attention to the area in 1929. The documentary won the First Appearance competition at the International Documentary Film Festival Amsterdam, as well as several other awards. 
 Hungarian village of Nagyrév, alternating between portraits of the surrounding landscape and first-hand narrations by the elderly inhabitants.

Some women poisoned unwanted husbands based on their oppression, drunkenness or laziness, some because the wives had taken lovers, some because the husbands had returned home disabled from World War I.  Unwanted babies were also poisoned.
A web of stories unfolds through the characters memories which recapture old but ever-lasting tales of life, death and the struggle between the sexes. One of them is the midwifes story as well as one of the narrators revelation that the flypaper murders were a widespread practice not only in the particular area but on a national level. The film tries to give some insight in the domestic battles that the women of the village have to fight.

==References==
 

==External links==
*  
*  
*  
*   (in Hungarian)
*  The New York Times, 1929-09-13
*  The New York Times, 1929-12-14
* The New York Times, 1930-03-16

 
 
 
 
 
 
 
 
 

 
 