Sweatshop (film)
{{Infobox film
| name           = Sweatshop
| image	         = Sweatshop_DVD_Cover.jpg
| image size     =
| alt            = 
| caption        = 
| director       = Stacy Davidson
| producer       = Laura Bryant Ted Geoghegan
| writer         = Stacy Davidson Ted Geoghegan
| screenplay     = 
| story          =
| based on       = 
| narrator       = 
| starring       = Ashley Kay, Melanie Donihoo, Peyton Wetzel 
| music          = Dwayne Cathey
| cinematography = Stacy Davidson
| editing        = Stacy Davidson
| studio         = Bloodline Entertainment Odyssee Pictures Starving Kappa Pictures Upstart Filmworks
| distributor    = 
| released       = 
| runtime        = 90 minutes
| country        = United States English
| budget         = 
| gross          = 
| preceded by    = 
| followed by    = 
}} American horror film directed by Stacy Davidson.  It follows a group of early aged adults who are stalked and viciously murdered by a humongous welder with an equally massive sledgehammer and his two women followers after breaking into an abandoned factory to throw a rave in order to make profits off pimping out their friends.

==Critical reaction==
Dread Central awarded 4/5, saying it managed to "create a fully immersive, cohesive mini-universe".  Horror.com found it "effective and original" despite some longueurs and questionable acting.  Horror Talk was more critical, awarding 1.5/5, criticising the story and acting, and noting the low budget.  About.com gave 2/5. 

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 
 


 