The Marihuana Story
{{Infobox film
| name = The Marihuana Story
| image = 
| image size =
| caption =
| director = León Klimovsky
| producer = Carmelo Vecchione
| writer = Wilfredo Jiménez Bernio Mason
| narrator =
| starring = Pedro López Lagar and Fanny Navarro
| music = Juan Lhert and Anatole Pietri
| cinematography = Alberto Etchebehere
| editing = José Serra
| distributor =
| released = 27 September, 1950
| runtime = 98 minutes
| country = Argentina
| language = Spanish
| budget =
| preceded by =
| followed by =
}}
The Marihuana Story ( ) is a 1950 Argentine film directed by León Klimovsky. It was entered into the 1951 Cannes Film Festival.   

==Cast==
* Pedro López Lagar – Dr. Pablo Urioste
* Fanny Navarro – Marga Quiroga
* Golde Flami – Aída
* Nathán Pinzón – Sopita
* Eduardo Cuitiño – Gang Boss
* Alberto de Mendoza – Lt. DeLuca
* Gilberto Peyret – Inspector Olivera
* Roberto Durán – Diego
* Héctor Quintanilla – Chevrolet
* Pilar Gómez – Amelia
* Angel Prio
* Cecilia Ingenieros
* Elsa Márquez
* Juan Carrara
* Jesús Pampín – Hombre en congreso
* Alberto Rinaldi
* Alberto Barcel – Dr, Piñeyro
* Warly Ceriani – Director
* Mauricio Espósito
* Leticia Lando
* Gloria Castilla
* Domingo Mania – Dr. Portal
* Juan Fava
* Adolfo Lauria
* China Navarro
* Osvaldo Cabrera
* Rafael Diserio – Dueno de boite
* Jorge Villoldo – Portero
* Alfredo Almanza

==Production==
The relative financial success of the American exploitation film She Shoulda Said No! (1949), a morality tale involving the use of marijuana, prompted producers in 1951 to import The Marihuana Story from Argentina. 

==References==
 

== External links ==
* 

 
 
 
 
 
 
 
 

 