Peking Express (film)
{{Infobox film   
| name           = Peking Express 
| image          = Peking Express poster.jpg
| caption        = Theatrical release poster
| director       = William Dieterle 
| producer       = Hal B. Wallis
| writer         = John Meredyth Lucas   Jules Furthman Harry Hervey
| starring       = Joseph Cotten  Corinne Calvet   Edmund Gwenn Marvin Miller
| music          = Dimitri Tiomkin
| cinematography = Charles Lang
| editing        = Stanley Johnson
| distributor    = Paramount Pictures
| released       =  
| runtime        = 95 minutes
| country        = United States
| language       = English
| budget         = 
| gross = $1.1 million (US rentals) 
| preceded by    = 
| followed by    = 
}} Shanghai Express (1932), remade as Night Plane from Chungking (1943). It was directed by William Dieterle and produced by Hal B. Wallis, from a screenplay by John Meredyth Lucas, based on the original screenplay by Jules Furthman and Harry Hervey. The music score was composed by Dimitri Tiomkin, the cinematography was by Charles Lang, the art direction by Franz Bachelin and Hal Pereira and the costume design by Edith Head.

The film stars Joseph Cotten, Corinne Calvet and Edmund Gwenn with Marvin Miller.

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 
 
 

 