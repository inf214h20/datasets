The Four Companions (film)
{{Infobox film
| name =  The Four Companions 
| image =
| image_size =
| caption =
| director = Carl Froelich
| producer =  Carl Froelich
| writer =  Jochen Huth (play and screenplay)
| narrator =
| starring = Ingrid Bergman   Sabine Peters   Carsta Löck   Ursula Herking
| music = Hanson Milde-Meissner   
| cinematography = Reimar Kuntze   
| editing =  Gustav Lohse      
| studio = Tonfilmstudio Carl Froelich  UFA
| released = 1 October 1938  
| runtime = 96 minutes
| country = Germany  German 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}}
The Four Companions (German: Die vier Gesellen) is a 1938 German drama film directed by Carl Froelich and starring Ingrid Bergman, Sabine Peters and Carsta Löck. Jochen Huth adapted the script from his own play. The film was intended as a star vehicle to launch Bergmans career in Germany. 

==Synopsis==
After graduation, four female art students attempt to set up their own advertising agency.

==Cast==
* Ingrid Bergman as Marianne Kruge 
* Sabine Peters as Käthe Winter 
* Carsta Löck as Lotte Waag 
* Ursula Herking as Franziska 
* Hans Söhnker as Stefan Kohlund 
* Leo Slezak as Professor Lange 
* Erich Ponto as Regierungsrat Alfred Hintze 
* Heinz Welzel as Feinmechaniker Martin Bachmann 
* Rudolf Klicks as Direktor der graphischen Berufsschule 
* Karl Haubenreißer as Direktor der Seidenstrumpffabrik 
* Lotte Braun as Sekretärin des Fabrikdirektors 
* Wilhelm P. Krüger as Maurermeister am Neubau 
* Willi Rose as Straßenbahnschaffner 
* Max Rosenhauer as Hauswart 
* Ernst G. Schiffner as Geschäftsmann, der keinen Auftrag erteilt 
* Hans Juergen Weidlich as Berliner Jüngling an der Litfaßsäule 
* Kurt Mikulski as Ausstellungsdiener 
* Fritz Lafontaine as 1. Grafikschüler 
* Gustaf Dennert as 2. Grafikschüler  
* Zarah Leander as Singer
* Traute Bengen 
* Hugo Froelich

== References ==
 

== Bibliography ==
* Ascheid, Antje. Hitlers Heroines: Stardom & Womanhood In Nazi Cinema. Temple University Press, 2010.
* Chandler, Charlotte. Ingrid: Ingrid Bergman, A Personal Biography. Simon and Schuster, 2007. 
* Lunde, Arne. Nordic Exposures: Scandinavian Identities in Classical Hollywood Cinema. University of Washington Press, 2010.

== External links ==
*  

 

 
 
 
 
 
 
 
 
 
 
 