Love Jones (film)
{{Infobox film
| name           = Love Jones
| image          = LoveJonesMovie.jpg
| caption        = Theatrical release poster
| director       = Theodore Witcher Nick Wechsler
| writer         = Theodore Witcher
| starring       = Larenz Tate Nia Long Bill Bellamy Isaiah Washington Lisa Nicole Carson
| music          = Darryl Jones Wyclef Jean
| cinematography = Ernest Holzman
| editing        = Maysie Hoy
| distributor    = New Line Cinema
| released       =  
| runtime        = 108 minutes
| country        = United States English
| budget         = United States dollar|$10,000,000 (approximately)
| gross          =  $12,782,749 (Worldwide)     
}} feature film debut. It stars Larenz Tate, Nia Long, Isaiah Washington, Bill Bellamy, and Lisa Nicole Carson.

Two of the poems recited by Nia Longs character, Nina, were written by Sonia Sanchez and are included in her book Like the Singing Coming Off the Drums: Love Poems. 

While the film received favorable critical reviews, the film was not a financial success but it remains a cult following because of its realistic characters and unorthodox take on the romance genre. It is also Theodore Witchers only directorial work to date.

== Synopsis ==
In Chicago, IL Darius Lovehall (Larenz Tate) is a poet who is giving a reading at the Sanctuary, an upscale nightclub presenting jazz and poetry to a bohemian clientele. Shortly before his set, he meets Nina Mosley (Nia Long), a gifted photographer who recently lost her job. They exchange small talk, and Darius makes his interest clear when he retitles his love poem "A Blues For Nina". A mutual attraction is sparked between them, and Darius invites himself back to her place to ask her out. They have sex on the first date, but neither Darius or Nina are sure what to do next. Nina has just gotten out of a relationship and isnt sure if she still cares for her old boyfriend. Darius isnt sure whether or not to admit that he really cares for Nina.

==Production==
The producers of the film said that they wanted to make a modern film about African-American life that did not use violence and recreational drugs as elements in the story. 

== Cast ==
{| class="wikitable"
|- bgcolor="CCCCCC"
! Actor !! Role
|-
| Larenz Tate || Darius Lovehall
|-
| Nia Long || Nina Mosley
|-
| Isaiah Washington || Savon Garrison
|-
| Lisa Nicole Carson || Josie Nichols
|-
| Bill Bellamy || Hollywood
|-
| Leonard Roberts || Eddie Coles
|-
| Bernadette Speakes as Bernadette L. Clarke || Sheila Downes
|-
| Khalil Kain || Marvin Cox
|-
| Cerall Duncan || Troy Garrison
|-
| David Nisbet || Publisher
|- Simon James || Roger Lievsey
|-
| Oona Hart || Model — Lievsey Studio
|-
| Jaqueline Fleming || Lisa Martin
|-
| Manao DeMuth || Ninas Assistant
|-
| Marie-Françoise Theodore || Tracey Powell
|}

== Reception ==
The film currently holds a 67% approval rating at Rotten Tomatoes.

 . Nia Long was Brandi, one of the girl friends, in Boyz N the Hood. Love Jones extends their range, to put it mildly". 

James Berardinelli also awarded the film 3 out of 4 stars for ReelReviews, and he determined that "There are several reasons why this film works better than the common, garden-variety love story. To begin with, the setting and texture are much different than that of most mainstream romances. The culture, in which post-college African Americans mingle while pursuing careers and relationships, represents a significant change from what were used to. The Sanctuary, the intimate Chicago nightclub where Darius and Nina meet, is rich in its eclectic, bluesy atmosphere. And Love Joness dialogue is rarely trite. When the characters open their mouths, it usually is because they have something intelligent to say, not because theyre trying to fill up dead air with meaningless words". 

== See also ==
 
* Love Jones (soundtrack) — soundtrack to the film.

== References ==
 

== External links ==
*  
*  
*  

 
 
 
 
 
 