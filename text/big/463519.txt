Impromptu (1991 film)
{{Infobox film
| name           = Impromptu
| image          = Impromptuposter.jpg
| caption        = Theatrical release poster
| image_size     = 250px
| director       = James Lapine
| producer       = Stuart Oken Daniel A. Sherkow
| writer         = Sarah Kernochan
| starring       = Judy Davis Hugh Grant Mandy Patinkin Bernadette Peters Julian Sands Emma Thompson
| music          = Frédéric Chopin Franz Liszt Ludwig van Beethoven
| cinematography = Bruno de Keyzer Michael Ellis
| studio         = 
| distributor    = Sovereign Pictures
| released       = 12 April 1991
| runtime        = 91 min.
| country        = United Kingdom United States
| language       = English
| budget         =
| gross          =
}} period drama film directed by James Lapine, written by Sarah Kernochan, produced by Daniel A. Sherkow and Stuart Oken, and starring Hugh Grant as Frédéric Chopin and Judy Davis as George Sand. The film was shot entirely on location in France as a British production by an American company. The main location used was at the Chateau des Briottières outside of Angers, in the Loire Valley.  The film was rated PG-13 by the MPAA.

==Plot==
Since getting divorced, Baroness Amantine-Lucile-Aurore Dupin, previously Baroness Dudevant, the successful and notorious writer of sensational romance novels now living under the pseudonym George Sand, in Paris, has been in the habit of dressing like a man. In her romantic pursuit of the sensitive Chopin, whose music she fell in love with before seeing him in person, George/Aurora is advised that she must act like a man pursuing a woman, though she is also advised to avoid damaging his health by not pursuing him at all. With this advice Sand is deterred by a fellow countrywoman who pretends to be smitten with Chopin, the mistress of Franz Liszt, the Countess Marie dAgoult.  Whether the Countess is really in love with Chopin is unlikely; she seeks only to prevent a relationship between Chopin and Sand.

Sand meets Chopin in the French countryside at the house of the Duchess dAntan, a foolish aspiring socialite who invites artists from Paris to her salon in order to feel cosmopolitan. Sand invites herself, not knowing that several of her former lovers are also in attendance. A small play is written by Alfred de Musset satirizing the aristocracy, Chopin protests at his lack of manners, de Musset bellows and a fireplace explosion ensues.

Chopin is briefly swayed by a beautifully written love letter ostensibly from dAgoult, a letter actually written by, and stolen from, Sand. Eventually Sand wins over Chopin when she proves that she wrote the letter, reciting its words to him passionately, and after buying a copy of her memoir he finds the text of the letter in the book.

Chopin is then challenged to a duel by one of Sands ex-lovers. He faints during the face-off. Sand finishes the duel for him and nurses him back to health in the countryside, solidifying their relationship.

Near the end of the movie, Sand and Chopin dedicate a volume of music to the countess, although this only suggests that she has had an affair with Chopin, causing a falling-out with her lover Liszt. Sand and Chopin depart for Majorca, relieved to escape the competitive nature of artistic alliances and jealousies in Paris.

== Cast ==
Source: New York Times 
  
*Judy Davis - George Sand (Aurora)
*Hugh Grant - Frédéric Chopin
*Mandy Patinkin - Alfred de Musset
*Bernadette Peters - Marie DAgoult
*Julian Sands - Franz Liszt
*Ralph Brown - Eugène Delacroix
*Georges Corraface - Felicien Mallefille
*Anton Rodgers - Duke DAntan
*Emma Thompson - Duchess DAntan
*Anna Massey - George Sands Mother

and
*David Birkin - Maurice
*Nimer Rashed - Didier
*Fiona Vincente - Solange
*John Savident - Buloz
*Lucy Speed - Young Aurora
*Elizabeth Spriggs - Baroness Laginsky
*Jezabelle Amato - Innkeepers Wife
*Claude Berthy - Chopins Valet
*André Chaumeau - Priest
*Nicholas Hawtrey - Philosopher
*Isabelle Guiard - Princess
*Fernand Guiot - Butler
*Sylvie Herbert - Sophie
*Annette Milsom - Ursule
*Jean-Michel Dagory - Innkeeper
*François Lalande - Local Doctor
*Ian Marshall de Garnier - Editor
*Dale Scott-Giry - Woman at Party
*Stuart Seide - Clerk

== Soundtrack listing ==
Chopin: Impromptu No. 1 in A-flat major (Op. 29) Ballade No. 1 in G minor (Op. 23) Polonaise in A major "Military" (Op. 40, No.1) Etude in E minor "Wrong Note" (Op. 25, No. 5) Prelude in G-sharp minor (Op. 28, No. 12) Prelude in D-flat major "Raindrop"  (Op. 28, No. 15) Etude in G-flat major "Butterfly" (Op. 25, No. 9) Nouvelle Etude No. 1 in F minor Etude in C-sharp minor (Op. 10, No. 4) Waltz in D-flat major "Minute" (Op. 64, No. 1)
* Fantaisie-Impromptu|Fantasy-Impromptu in C-sharp minor (Op. 66) Nocturne in F major (Op. 15, No. 1) Etude in A-flat major "Aeolian Harp" (Op. 25, No. 1)

Liszt: Apres dune lecture de Dante (from Années de Pèlerinage, 2nd year) Transcendental Etude No. 4 "Mazeppa"
* Grand galop chromatique

Beethoven: Symphony No.6 in F major "Pastoral"

==Production==
Sarah Kernochan, Lapines wife, had written the film in 1988 during a lay-off due to a Writers Guild strike. Kernochan explained the film: "How do complicated people find a simple way of loving?" The producer Stuart Oken liked the project; his concern was to give Lapine "a chance to realize his vision and become a movie director." Oken brought the project to his friend and fellow producer, Dan Sherkow, who secured financing and distribution for the picture. Corbett, Patricia. New York Times, 7 January 1990, pH13 

For the cast, Lapine wanted "to use people he had worked with before." He cast actors who "didnt look like, but embodied the characters." Judy Davis and Mandy Patinkin could "hardly look more unlike the cultural icons they portray." Lapine hired a piano coach and a music consultant to advise both Grant and Sands on various piano techniques. 

Due to Common Market legalities, the film was incorporated as a British production with co-production by Ariane, a French company and distribution by a United States company Sovereign Films. The budget was $6 million. 

==Television==
Impromptu was broadcast on PBSs Masterpiece Theatre in 1993. 
   
==Reception==
The New Yorker reviewer wrote that the film was "an ebullient and absurdly entertaining account of the famous love affair of George Sand and Frédéric Chopin. ...The historical figures in this movie are cartoons, but they’re cartoons with recognizable human qualities, and the actors look as if they were having a wonderful time charging around in their period costumes. Hugh Grant’s Chopin is a brilliant caricature of the Romantic ideal of the artist; he gives the character an air of befuddled unworldliness, and punctuates his readings with delicately timed tubercular coughs. Judy Davis plays Sand—a great actress in a great role." 

The reviewer for The Houston Chronicle wrote that the film is "a zingy, impudent little essay on gender, with the exquisitely confusing George Sand at its center." Millar, Jeff.  Houston Chronicle, 3 May 1991 

==Awards and nominations==
Source: New York Times 

Independent Spirit Awards
*Best Female Lead—Judy Davis (WINNER)
*Best Supporting Female—Emma Thompson (nominee)

Impromptu won the award as Audience Favorite at the Houston WorldFest Film Festival. 

== References ==
 

== External links ==
*  
*  
*   Chateaux France

 

 
 
 
 