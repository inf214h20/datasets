Qurbani Rang Layegi
{{Infobox film
| name           = Qurbani Rang Layegi
| image          = QurbaniRangLayegi.jpg
| image_size     =
| caption        =
| director       = Raj N. Sippy
| producer       = K.K. Talwar
| writer         = Kader Khan (dialogue) Prayag Raj (story)
| narrator       =
| starring       = Sanjay Dutt   Poonam Dhillon Padmini Kolhapure
| music          = Laxmikant-Pyarelal
| cinematography = Anwar Siraj
| editing        = Waman B. Bhosle  Gurudutt Shirali	 
| distributor    = 
| released       = 
| runtime        = 
| country        = India Hindi
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}
 Indian Bollywood film directed by Raj N. Sippy and produced by K.K. Talwar. It stars Sanjay Dutt, Poonam Dhillon and Padmini Kolhapure in pivotal roles. 

==Cast==
* Sanjay Dutt...Raj Kishan
* Poonam Dhillon...Poonam
* Padmini Kolhapure...Basanti
* Kajal Kiran...Chutki 
* Shakti Kapoor...Vicky
* Gurbachchan Singh...Tony

==References==
 

==External links==
* 

 
 
 
 

 