Rubber Racketeers
{{Infobox film
| name           = Rubber Racketeers
| image_size     =
| image	=	Rubber Racketeers FilmPoster.jpeg
| caption        = Harold Young Maurice King (producer)
| writer         = Henry Blankfort (original screenplay)
| narrator       =
| starring       = See below
| music          =
| cinematography = L. William OConnell Jack Dennis
| distributor    = Monogram Pictures
| released       = 26 June 1942
| runtime        = 67 minutes
| country        = USA
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}
 Harold Young and starring Ricardo Cortez and Rochelle Hudson.

== Plot summary ==
A man (Ricardo Cortez) recently released from prison gets involved with the black market in synthetic rubber during wartime. The synthetic rubber is blamed for several people being killed in car accidents due to blowouts.

== Cast ==
*Ricardo Cortez as Gilin
*Rochelle Hudson as Nikki
*William Henry as Bill Barry
*Barbara Read as Mary Dale John Abbott as Dumbo
*Dick Rich as Mule
*Dewey Robinson as Larkin
*Sam Edwards as Freddy Dale
*Kam Tong as Tom
*Milburn Stone as Angel Pat Gleason as Curley
*Alex Callam as Butch
*Alan Hale Jr. as Red
*Dick Hogan as Bert
*Marjorie Manners as Lila

== Soundtrack ==
 

== External links ==
* 
* 

 
 
 
 
 
 
 
 
 
 


 