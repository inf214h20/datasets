Si Buta Lawan Jaka Sembung
{{Infobox film
| name           = Si Buta Lawan Jaka Sembung 
| image          = Si_buta_lawan_jaka_sembung_poster.jpg
| caption        = Indonesian Film Poster
| director       = Dasri Yacob
| producer       = Sabirin Kasdani
| writer         = 
| narrator       = 
| starring       = Barry Prima, Advent Bangun
| music          = 
| cinematography = 
| editing        = 
| distributor    = Rapi Films
| released       =  
| runtime        = 
| country        = Indonesia Dutch
| budget         = 
}}

Si Buta Lawan Jaka Sembung (also titled The Warrior II and The Warrior Against Blind Swordsman for international distribution) is a 1983 Indonesian fantastique martial arts movie and a sequel to 1981 film Jaka Sembung.
 alias Si Buta dari Gunung Iblis ("The Blind Man from Iblis Mountain")), Sri Gudhi Sintara (Dewi Magi), and Zurmaini (Roijah alias Bajing Ireng).

==Synopsis== Dutch colonial army in West Java holds a martial arts tournament to select the best warrior to confront local warrior-freedom fighter Parmin (nicknamed Jaka Sembung). The winner is an obscure blind swordsman and the Dutch commander De Mandes (Gino Makasutji) commissions him. However, the swordsman is sympathetic with Jaka Sembungs cause and things get more complicated when magician seductress Dewi Magi as well as her guru (W. D. Mochtar) intervene on behalf of the Dutch.

==External links==
* 

 
 
 
 
 
 
 
 


 
 