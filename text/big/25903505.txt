Chalo America
Chalo America is a 1999 Indian film Directed by Piyush Jha.

==Plot summary==
The film chronicles the misadventures of three Lukkhas (Slackers) with just one dream preying on their minds - to get to the kingdom of Coca Cola and Clinton. They make attempt after attempt to flee from the drudgery and boredom in dreary India to the land of the free and the home of the brave - the United States. They remain undeterred by their failure and woeful incompetence to get there - be it by acquiring a visa or a wife, or smuggling themselves into boats. Their hopes flag, but never quite fade away.

What is funny about the film is that it strikes a nerve. It is a comic illustration of an aspiration that is latent within many, if not all, of us. It is a portrayal of an obsessive fascination for the other, the conviction that the astroturf is greener on the other side, that comes through. This is an ironical reflection of the same sentiment that is in a film like Kuch Kuch Hota Hai, in which the gloss and glitz is nothing if not Yankee. The film ends with some wisdom gained, but not too much to stop being human.

Deven Bhojanis portrayal of the cute Gujju-turned-mod is by far the highlight of the film. It is wonderful to see his talents reined in well by Jha, and hence being used to the full, unlike a lot of the work he does, or is probably asked to do, on television.

Aside from that, it is the little moments, like putting desi cigarettes in a pack of Marlboros, that lend the film a charm of its own. It is encouraging to see that such alternative films are being made, with the express purpose of entertainment, while remaining entirely independent of any pretensions to art cinema.

==Cast==
{| class="wikitable"
|-
| Aashish Chaudhary || Surendra "Sunny" Pagnis
|-
| Mandar Shinde || Rajat
|-
| Deven Bhojani || Akshay Bandelia
|-
|}

==Production/ Financing Company==

The National Film Development Corporation of India (NFDC)
http://www.nfdcindia.com/view_film.php?film_id=17

==References==
 

*Official Selection by the Directorate of Film Festivals, Government of India for the Indian Panorama section at the 30th International Film Festival of India-1999 (IFFI99). View Government of India official notification: http://pib.nic.in/archieve/lreleng/lyr98/l1198/r021198.html.

Chalo America is referenced in the leading Indian news magazine India Today: http://www.india-today.com/itoday/11011999/cinema.html

*Recently screened at the Atlanta Indo-American Film Festival 2009: http://www.iafs.us/AIAFF/Web/Forms/frmFilmSchedules2009.aspx

==External links==
* 
*  


 
 


 