El Topo
 
{{Infobox film
| name           = El Topo
| image          = El Topo poster.jpg
| caption        = Theatrical release poster
| director       = Alejandro Jodorowsky
| producer       = Juan López Moctezuma Moshe Rosemberg Roberto Viskin
| writer         = Alejandro Jodorowsky
| starring       = Alejandro Jodorowsky Brontis Jodorowsky Mara Lorenzio David Silva Paula Romo Jacqueline Luis
| music          = Alejandro Jodorowsky
| cinematography = Raphael Corkidi
| editing        = Federico Landeros
| distributor    = ABKCO Records
| released       =  
| country        = United States Mexico 
| runtime        = 125 minutes
| language       = Spanish
| budget         =
}} maimed and dwarf performers, and heavy doses of Christian symbolism and Eastern philosophy, the film is about the eponymous character – a violent, black-clad gunfighter – and his quest for Enlightenment (spiritual)|enlightenment.

==Plot== redemption and Regeneration (theology)|rebirth.

===Part 1===
The first half opens with El Topo (played by Jodorowsky himself) traveling through a desert on horseback with his naked young son, Hijo. They come across a town whose inhabitants have been slaughtered, and El Topo hunts down and kills the perpetrators and their leader, a fat balding Colonel.  El Topo abandons his son to the monks of the settlements mission and rides off with a woman whom the Colonel had kept as a slave. El Topo names the woman Mara, and she convinces him to defeat four great gun masters to become the greatest gunman in the land.  Each gun master represents a particular religion or philosophy, and El Topo learns from each of them before instigating a duel. El Topo is victorious each time, not through superior skill but through trickery or luck.

After the first duel, a black-clad woman with a male voice finds the couple and guides them to the remaining gun masters. As he kills each master, El Topo has increasing doubts about his mission, but Mara persuades him to continue. Having killed all four, El Topo is ridden with guilt, destroys his own gun and revisits the places where he killed those masters, finding their graves swarming with bees. The unnamed woman confronts El Topo and shoots him multiple times in the manner of stigmata. Mara then betrays him and rides off with the woman, while El Topo collapses and is carried away by a group of dwarves and mutants.

===Part 2=== meditating on the gun masters "four lessons". The outcasts dwell in a system of caves which have been blocked in — the only exit is out of their reach due to their deformities. When El Topo awakes, he is "born again" and decides to help the outcasts escape. He is able to reach the exit and, together with a dwarf girl who becomes his lover, performs for the depraved cultists of the neighboring town to raise money for dynamite.

Hijo, now a young monk arrives in the town to be the new priest, but is disgusted by the perverted form of religion the cultists practice. Despite El Topos great change in appearance Hijo recognizes him and threatens to kill him on the spot for abandoning him as a child, but agrees to wait until he has succeeded in freeing the outcasts.  Hijo grows impatient at the time the project is taking, and begins to work alongside El Topo to hasten the moment when he will kill him.  At the point when Hijo is ready to give up on finishing the tunnel, El Topo breaks through into the cave. The tunnel has been completed but Hijo finds that he cannot bring himself to kill his master.

The outcasts come streaming out, but as they enter the town they are shot down by the cultists.  El Topo helplessly witnesses his community being slaughtered and is shot himself. Ignoring his own wounds he massacres the cultists, then takes an oil lamp and [[Self-immolation| beehive like the gun masters graves.

As the film ends, El Topos son, girlfriend and baby ride off on horseback, the son now wearing his fathers clothes.

==Cast==
* Alejandro Jodorowsky as El Topo
* Brontis Jodorowsky as Hijo (son of El Topo) as a boy
* Alfonso Arau as Bandit #1
* José Luis Fernández as Bandit #2
* Alf Junco as Bandit #3
* Jacqueline Luis as El Topos wife
* Mara Lorenzio as Mara
* Paula Romo as The Woman in Black
* David Silva as The Colonel
* Héctor Martínez as Master #1
* Juan José Gurrola as Master #2
* Víctor Fosado as Master #3
* Agustín Isunza as Master #4
* Robert John as Hijo (Son of El Topo) as a man
* Gypsy, Mother of Master #2 Bertha Lomelí
* José Antonio Alcaraz as Sheriff
* José Legarreta as Dying Man

== Reception == Best Foreign Language Film at the 44th Academy Awards, but was not accepted as a nominee. 

 , whose self-conscious conflation of the roles of charlatan and ringmaster of the unconscious Jodorowsky apes, the film is a breathtaking concoction of often striking, but more often ludicrous, images. The result is a movie that, though it impressed many at the time of its original release, in retrospect is clearly a minor, albeit often very funny work." 

Some critics, including Gary Arnold of The Washington Post and Times-Herald, were offended by the films visuals. Arnold wrote of the film: "Theres not enough art to justify the sickening reality of Jodorowskys artistic method. The meaning of the film is not to be found in the mystical camouflage of the gunfighter-turned-guru-and-martyr (for what, one wonders? Evidently self-agrandizement rather than the well-being of his congregation of the deformed), but in the picturesque horrors and humiliations". 

The visuals were the main point of contention amongst El Topo s critics, who debated if the sequences and montage were meaningful or merely exploitative. Concerning the symbolism within the film,   of the Chicago Tribune commented on how the visuals were perceived within the framework of drug culture. Siskels review states: "Under the influence, El Topo becomes a violent, would-be erotic freakshow, and that, I suppose, can be very heavy. For others, it is enough to make one yawn". 

Other critics, however, remain more enthusiastic about the film. For example, Roger Ebert includes El Topo in his Great Movies series. 

Peter Schjeldahl, writing for the New York Times, described the film as "a very strange masterpiece". His review states: "On first blush it might seem no more than a violent surreal fantasy, a work of fabulous but probably deranged imagination. Surreal and crazy it may be, but it is also (one realizes the second time through) as fully considered and ordered as fine clockwork". 

The out-of-print El Topo: A Book of the Film contains a lengthy interview with a director that attempts to explain some of the films visuals. It also contains the screenplay of the film. Both appear on the Subterranean Cinema blog.

==Influence== No More Heroes.  Gore Verbinski cited it as an influence on Rango (2011 film)|Rango. 

==Release==
There was no original intention to show El Topo in Mexico, where it was filmed and produced. Rosenbaum, 1992. p. 93  Ben Barenholtz, an owner of a local theater called The Elgin, saw a private screening of El Topo at the Museum of Modern Art.  Barenholtz recalled that despite several audience members walking out, he was fascinated by El Topo. On a failing attempt to purchase the American rights to the film, Barenholtz convinced the producer to have the film shown at midnight at The Elgin.  Barenholtz chose the late showings of 1am on Friday and at Midnight during the week which would give audiences a sense of "self-discovery".  The film premiered on December 17, 1970 and ran continuously seven days a week until the end of June 1971. 

The film was distributed across the United States with the assistance of Allen Klein, manager of The Beatles.  The film was shown late at night like it was at The Elgin. It has been argued  that without support from people like John Lennon and Allen Klein, the film would not have found a sizeable audience.

==Home video releases== bootlegged videos. Its official DVD release was on May 1, 2007. Its first Blu-ray release was on April 26, 2011. 

==Sequel==
Since at least the early 1990s, Jodorowsky has been attempting to make a sequel to El Topo. In 1996, a teaser poster was released,  but, apparently, no shooting was actually done. The original working-title, The Sons of El Topo (Los hijos del Topo), was changed (sometime between 1996 and 2002) to Abelcaín, due to disputes over ownership with Allen Klein. Additionally, the name of the character El Topo (The Mole) was changed to "El Toro" (The Bull). Jodorowsky said of this: "I am now working on a Franco-Canadian production called Abelcaín, which is a new version of the same project. The character El Topo has become El Toro. A single slash added on letter P changed a subterranean rat into a charging bull. For a true artist, difficulties become opportunities. And clouds become solid present." 
 Marilyn Manson was attached to star in the film, but that Jodorowsky was having great difficulty raising money for the project.  In an interview for The Guardian in November 2009, Jodorowsky stated that his next rumoured project, a "metaphysical western" entitled King Shot, is "not happening" and instead he is to begin work on Son of El Topo, in collaboration with "some Russian producers". 

==See also==
* Acid Western
* List of submissions to the 44th Academy Awards for Best Foreign Language Film
* List of Mexican submissions for the Academy Award for Best Foreign Language Film Midnight movie
* Weird West

==References==
 

==External links==
*  
*  
*   film review by Roger Ebert

 

 
 
 
 
 
 
 
 
 
 
 
 
 