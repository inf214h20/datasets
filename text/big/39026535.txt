CIA II: Target Alexa
{{Infobox film
| name           = CIA II: Target Alexa
| image          =
| image_size     =
| caption        =
| director       = Lorenzo Lamas
| producer       = PM Entertainment Group
| writer         = Michael January Kathleen Kinmont
| narrator       = John Savage
| music          =
| cinematography =
| editing        =
| distributor    =
| released       =  
| runtime        = 90 minutes
| country        = United States English
| budget         =
}} action adventure film, starring by Lorenzo Lamas and Kathleen Kinmont. It was directed by Lorenzo Lamas. 

== Plot ==
When Alexa was captured, Graver, the best CIA agent, tried to turn her against her latest employer who had diplomatic immunity. Graver used Alexa to find microchip. Alexa is unable to turn on her own, so Graver must find the key to unlock her conviction.

== Cast ==
* Lorenzo Lamas as Mark Graver
* Kathleen Kinmont as Alexa John Savage as Franz Kluge
* John Saint Ryan as Ralph Straker (as John Ryan)
* Lori Fetrick as Lana
* Pamela Dixon as Chief Robin
* Al Sapienza as Raines (as Alex Statler)
* Sandee Van Dyke as Tanya
* Michael Chong as Col. Trang
* Daryl Keith Roach as Wilson (as Daryl Roach)
* Gary Wood as Rick
* Larry Manetti as Radcliffe
* Branscombe Richmond as General Mendoza
* Ned Van Zandt as Store Clerk
* Richard Ehrlich as Lab Tech

== See also ==
*  

== References ==
 

==External links==
*  

 
 
 
 
 


 