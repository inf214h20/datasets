Il minestrone
{{Infobox film
| name           = Il minestrone
| image          = Minestrone roberto benigni sergio citti 007 jpg wjub.jpg
| caption        = Film poster
| director       = Sergio Citti
| producer       = 
| writer         = Sergio Citti Vincenzo Cerami
| starring       = Roberto Benigni Ninetto Davoli Franco Citti
| music          = Nicola Piovani
| cinematography = Nino Baragli
| editing        = 
| distributor    = 
| released       =  
| runtime        = 104 minutes 170 minutes (Italian original cut)
| country        = Italy
| language       = Italian
| budget         = 
}}

Il minestrone is a 1981 Italian comedy film directed by Sergio Citti. It was entered into the 31st Berlin International Film Festival.   

==Plot==
Francis and John are two tramps on the outskirts of Rome, forced to rummage through the trash forever in search of food.  They meet and make friends and through a misunderstanding are arrested.  They are locked up in the security cell of the police station with The Maestro, a poor but well-dressed specialist in running away from the restaurants, taverns and eateries of Rome after having gorged without paying. Upon release, the three come together late in the evening and go to a restaurant where, at the end of a memorable binge (tagliatelle, tripe with tomato sauce, oxtail, asparagus, mozzarella with tomatoes, lamb with potatoes), and concoct a clever ruse to escape and cheat the owner Attilio. In running away, they fall into a cold and smelly mud puddle and take refuge in a cattle car to dry off and warm up. They fall asleep and wake to find themselves in a small town in Tuscany, and then begin to wander in the countryside of Poggibonsi (Siena). They try to scrounge food from the old woman Beatrice, but she does not even know how to feed the seven children living with her, and she is forced to sacrifice his old turtle. By and by they come across a restaurant owned by two hunters, who are in the habit of serving the customers only after enjoying their own comfortable meal and who keep their waiter chained up like a dog. The three friends release the waiter and decide to leave the restaurant without eating, sensing the danger of being caught and shot. After further wanderings, they are nearly lynched by a group of laborers after being caught eating the temporarily unguarded soup at the laborers table. Left tied up while the mob goes off for a while, they are freed by the waiter who they had recently saved, who joins them.  The waiter leads them to an old inn but when they arrive they are dismayed to learn that it has been turned into a funeral home.

The next day, they come upon Gildo (Farnese), a failed industrialist who lost his entire fortune and who is about to commit suicide, saving him just in time, and after an initial skirmish, Gildo and his family join them. Gildo leads the group to the house of his father, a Marquis who initially humiliates him in front of the group by refusing to be moved by their plight while he samples bits from a vast spread of delicious dishes.  The Marquis later accepts Gildo after forcing him to abandon his wife and children to their fate. For the rest of them, the search for food continues, and the group is joined by other strange characters that they come across. After a sumptuous and surreal "imaginary lunch", they find a strange box on a beach and gorge on the "gelatin" in it, which is unfortunately merely packing material for a missile. Poisoned by the ingestion of this non-edible substance, they are all admitted to a clinic, where they meet a holy man (Gaber), who convinces them to follow him. But he is not of sound mind, and he takes them quite by chance to a mountain pass, where the orchestra performs the Tyrolean leitmotif of the film (music was created and directed by Nicola Piovani), while the closing credits roll.

==Cast==
* Roberto Benigni - Maestro
* Franco Citti - Francesco
* Ninetto Davoli - Giovanni
* Daria Nicolodi - Eleonora, wife of Gildo
* Fabio Traversa - Cameriere
* Francesca Rinaldi - Bambina, child of Gildo
* Olimpia Carlisi - Becchina, venditrice di bare
* Giorgio Gaber - The Prophet
* Franco Iavarone - Food Taster (as Franco Javarone)
* Giulio Farnese - Gildo
* Carlo Monni
* Daniela Piperno

==References==
 

==External links==
* 

 
 
 
 
 
 