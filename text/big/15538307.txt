Murder on a Honeymoon
{{Infobox film
| name           = Murder on a Honeymoon
| image          = Murder_on_a_Honeymoon_1935_poster.jpg
| image_size     = 250px
| caption        = theatrical poster
| director       = Lloyd Corrigan
| producer       = Kenneth Macgowan
| based on       =  
| screenplay     = Seton I. Miller Robert Benchley
| starring       = Edna May Oliver James Gleason
| music          = Alberto Colombo (musical director)
| cinematography = Nick Musuraca
| editing        = William Morgan
| distributor    = RKO Radio Pictures
| released       =  
| runtime        = 74 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}

Murder on a Honeymoon is an American 1935   

==Plot== Catalina Island off the California coast, a passenger named Roswell T. Forrest (Brooks Benedict) gets sick. Hildegarde Withers (Edna May Oliver) and the others aboard are startled when he is found dead upon landing. It appears to be murder to Miss Withers, but she has a tough time convincing local Police Chief Britt (Spencer Charters) and coroner Dr. ORourke (Arthur Hoyt). 

When she contacts her friend, Police Inspector Oscar Piper (James Gleason), for more information about the deceased, he recognizes the name: the man was a vital witness in a case against a crime syndicate and had a price on his head of $10,000. He flies from New York to assist her in investigating the case and protect her from mob retribution.

When he arrives, the pair argue over which of the people aboard the plane is the killer:

*Joseph B. Tate (Leo G. Carroll), a famous Hollywood director
*struggling actress Phyllis La Font (Lola Lane), who is angling for a part in Tates next movie
*honeymooners Kay (Dorothy Libaire) and Marvin Deving (Harry Ellerbe) rum runner, and
*pilots Dick French (Chick Chandler) and Madden (Matt McHugh).  

Withers suspects poisoning &ndash; Forrest had been given a drink, a cigarette, and even a dose of smelling salts by Withers herself &ndash; but before this can be confirmed, the body is stolen. While Piper questions those involved, Withers discovers that McArthur (Morgan Wallace), the gangster who had offered the reward for Forrests death, has registered at the hotel under the flimsy alias of Arthur Mack. When she eavesdrops on his telephone conversation, she learns that he will be leaving an envelope for someone. She purloins it from the mailbox and finds $10,000 inside.

More murders occur. Marvin Deving is shot and killed just before he can reveal some information to Piper. Meanwhile, Withers and Piper learn that the first victim was not Forrest, but his bodyguard Tom Kelsey. He and the real Forrest (George Meeker) had switched identities. 
After McArthur confronts Withers at gunpoint, trussing her up and putting her in the closet, from which she is rescued by Piper, McArthur is also found dead.  Although it is staged to look like a suicide, Withers notices that the pistol in his hand is not his own.

When an employee complains that the fish in the hotel pond are all dead, Withers finds a pack of cigarettes discarded nearby; one of the cigarettes had fallen into the water, poisoning and killing the fish. With the murder weapon found, all the pieces come together. Withers takes Piper to see the grieving Kay. She offers the widow a cigarette, then casually mentions where she got it. When Kay refuses to smoke it, Withers tells Piper that McArthurs gun must be in the room. Kay pulls it out and tells them that she will have to kill them both now, but Withers manages to distract her, enabling Piper to disarm her. It turns out that the Devings thought they had been doublecrossed by McArthur when they did not receive their reward, unaware that Withers had taken it. When Marvin tried to betray McArthur in return, he was killed by his employer, and Kay then did in McArthur.

==Cast==
  
*Edna May Oliver as Hildegarde Withers
*James Gleason as Inspector Oscar Piper. Gleason played the character in all six films.
*Lola Lane as Phyllis La Font
*George Meeker as Roswell T. Forrest, posing as Tom Kelsey
*Dorothy Libaire as Kay Deving
*Harry Ellerbe as Marvin Deving
*Chick Chandler as Pilot Dick French
*Willie Best as Willie, the porter (billed as "Sleep n Eat")
 
*Leo G. Carroll as Director Joseph B. Tate
*DeWitt Jennings as Captain Beegle
*Spencer Charters as Chief Britt
*Arthur Hoyt as Dr. ORourke
*Matt McHugh as Pilot Madden
*Morgan Wallace as McArthur / Arthur Mack
*Brooks Benedict as Tom Kelsey, posing as Roswell T. Forrest
 

==Production==
The working title for Murder on a Honeymoon was "Puzzle of the Pepper Tree",   on TCM.com  which was the title of the 1933 Stuart Palmer novel it was based on. 
 Santa Catalina Island, a well-known resort 26 miles off the California coast. 

==References==
Notes
 

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 