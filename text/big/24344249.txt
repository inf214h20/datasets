Tomorrow (1972 film)
{{Infobox film
| name           = Tomorrow
| image          = Tomorrow film poster.jpg
| image_size     = 
| alt            = 
| caption        = 
| director       = Joseph Anthony 
| producer       = Gilbert Pearlman Paul Roebling
| writer         = Story:  
| narrator       = 
| starring       = Robert Duvall Olga Bellin
| music          = Irwin Stahl
| cinematography = Allan Green
| editing        = Reva Schlesinger
| studio         = 
| distributor    = Filmgroup Productions
| released       = 9 April 1972 (USA)
| runtime        = 103 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}} 1972 film directed by Joseph Anthony.  The screenplay was written by Horton Foote, adapted from a play he wrote which was based on a 1940 short story by William Faulkner.  The PG film was filmed in Alcorn County, Mississippi and  the Bounds and Oakland Community of Itawamba County, Mississippi. Though released in 1972, it saw limited runs in the U.S. until re-released about ten years later.

The opening courthouse scenes of Tomorrow were shot at the historic Jacinto Courthouse in Alcorn County, Mississippi. The courthouse built in 1854, has been refurbished, and is listed in the National Register of Historic Places. 

The majority of the movie was filmed in the Bounds Community of Itawamba County, at the sawmill on the Chester Russell farm.  Chester Russell was the grandfather of Tammy Wynette (Virginia Wynette Pugh) whose father died when she was nine months old. Wynette lived most of her young years with her grandparents on their farm, until she married in 1960. The sawmill building where much of the movie was shot, was built just for the filming of the movie.  Chester Russell was one of the jury and can be seen when the jury is deliberating in the opening courhouse scenes of the movie. 

  

Lead actor Robert Duvall calls Tomorrow one of his personal favorites of all the films hes done. 

==Plot==
An isolated and lonely farmer in rural Mississippi takes in a pregnant drifter who has been abandoned by the father of her child.

==Main cast==
{| class="wikitable"
|-
! Actor
! Role
|-
| Robert Duvall || Jackson Fentry
|-
| Olga Bellin || Sarah Eubanks
|-
| Sudie Bond || Mrs. Hulie
|-
| Peter Masterson || Douglas
|-
| Johnny Mask || Jackson
|-
|  William Hawley || Papa Fentry
|}

==Critical reception==
Vincent Canby of The New York Times overall did not care for the film but thought that it was well-intentioned:
 

==Trivia== American indie band Grandaddy sampled the film for their song Fentry.

==References==
 

== External links ==
*  
*  

 

 
 
 
 
 
 
 
 
 
 


 