Crawlspace (2013 film)
{{Infobox film
| name           = Crawlspace 
| image          = File:Crawlspace 2013 movie poster.jpg
| alt            = 
| caption        = 
| film name      = 
| director       = Josh Stolberg
| producer       = Mike Karz Jaime Burke
| writer         = Josh Stolberg Nick Taravella Steven Weber
| music          = Kaveh Cohen
| cinematography = Michael Fimognari
| editing        = Byron Wong
| studio         = Karz Entertainment Vuguru
| distributor    = 
| released       =  
| runtime        = 90 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} VOD distribution starting June 5, 2014.  Crawlspace focuses on a family that is terrorized by the former occupant of their new home.

==Synopsis==  Paul James) are sorting out items for a garage sale, the garage door inexplicably opens and closes. 
 Steven Weber) climbs down. He washes up and helps himself to some food, and he retrieves a stuffed unicorn that Kayla had thrown out. Webbers intrusions into the Gates lives grow more elaborate and sinister as the film progresses. He watches them closely through the air vents, and he even creeps into Kaylas room one night to film her sleeping. 

The Gates elderly neighbor warns Kayla and Shane that they need to get out of the house. She has witnessed Webber moving around when the family is gone during the day. After her warning, Webber sneaks into the neighbors house and attacks her. He stuffs her vacuum cleaners hose extension into her mouth and then reverses the air flow, emptying the vacuums contents down her throat. 

Webbers next victim is Taylors babysitter, who was not watching as the boy played with Shanes paintball gun. At the garage sale, Webber reveals himself to Kayla and Shane, but poses as a customer. When Webber does not leave after the sale is over, Shane approaches him aggressively. Webber punches Shane in the face and walks off the property. 

The next day, he is fixing the garbage disposal under the sink when his ex-wife comes to the house for a visit. She is under the impression that Webber is living in their old home until the phone rings and the answering machine plays the Gates message. Webber attacks his ex-wife and shoves her head into the exposed blades of the garbage disposal. He wraps her body in plastic wrap and stores it in the crawlspace with his other victims. 

The family calls an exterminator because they have begun to notice the decomposition odor. Chuck the Exterminator (David Koechner) investigates the crawlspace and discovers Webbers sleeping area, which he has plastered with pictures of the Gates as well as his own family. Chuck then discovers the bodies of Webbers victims. Webber attacks Chuck and strangles him with some Christmas lights. 

Fed up with all the suspicious activity at the house, Kayla installs a webcam, which she is monitoring at a coffee shop when she sees Webber disable it. Recognizing him as the man who attacked Shane at the garage sale, she calls the police, who identify the man as the former owner of the house. Tim confesses that not only did his bank foreclose on the Webbers house after their children died, but that he did not help the Webbers keep the house because he wanted it for his own family. 

The Gates prepare for a long night in the house, with everyone taking some measure of precaution. Tim holds on to a pistol, while Shane keeps watch with his paintball gun. The next morning, Webber begins his final assault on the Gates. His first target is Derek, who he kills by ramming Susans curling iron through his mouth. He puts Ambien in Shanes Mountain Dew, and he knocks Susan out with the ladder from the crawlspace. Tim confronts Webber with his pistol, but Webber has removed the bullets. He knocks Tim out and turns his attention to Kayla, who is in the crawlspace looking for Taylor. 

Webber has locked Taylor in a storage chest. Kayla manages to free him before Webber catches up to her. She tackles Webber, and the two of them fall through the floor. When they come to, their struggle climaxes in the kitchen. Susan has also regained consciousness and shoots Webber with Shanes paintball gun. Webber responds by throwing a kitchen knife into Susans chest. The paintball canister rolls over to Kayla, who puts it onto one of the gas burners on the stove. Susan manages to shoot Webber three times and then the canister explodes, sending a burning Webber flying through the air. 

The film ends as a family moves into the elderly neighbors house. The mother is on the phone telling a friend that the house was a steal because of some domestic tragedy next door. She explains that the owner went psycho and disappeared. Unknown to her, Webber is hiding in her attic, watching her talk on the phone. The film ends as he turns to the camera to reveal the burns that he suffered during the explosion.

==Cast==
*Raleigh Holmes as Kayla Gates
*Lori Loughlin as Susan Gates
*Jonathan Silverman as Tim Gates Steven Weber as Aldon Webber
*David Koechner as Chuck the Exterminator
*Sterling Beaumon as Shane Gates Paul James as Derek
*Nicole Moore as Mae
*Shannon Welles as Mrs. Milz
*Leila Charles Leigh as Patty Webber
*Xander Stolberg as Taylor Gates
*Larry Sullivan as Police Officer J. Forester
*Steve Crest as Police Officer Fallon

==Reception== Hider in the House,  and Shock Till You Drop commented that while Crawlspace was overly familiar, "at the end of the day, it pays respect to the notion that even the safest place in our world may still have secrets."  Starburst (magazine)|Starburst gave it seven out of ten stars and praised the film for the different ways that Webers character murdered his victims. 

==References==
 

==External links==
*  

 
 