Frankenweenie (2012 film)
 
{{Infobox film
| name           = Frankenweenie
| image          = Frankenweenie (2012 film) poster.jpg
| alt            =
| caption        = Theatrical release poster
| director       = Tim Burton
| producer       = Tim Burton Allison Abbate
| screenplay     = John August
| story          = Tim Burton
| based on       =  
| starring       =  Catherine OHara Martin Short Martin Landau Charlie Tahan Atticus Shaffer Winona Ryder
| music          = Danny Elfman
| cinematography = Peter Sorg
| editing        = Chris Lebenzon Mark Solomon
| studio         = Walt Disney Pictures Walt Disney Studios Motion Pictures
| released       =  
| runtime        = 87 minutes  
| country        = United States
| language       = English
| budget         = $39 million   
| gross          = $81.5 million 
}} Ed Wood Sleepy Hollow).
 black and resurrect him parodies related to the book, past film versions of the book and other literary classics.
 Academy Award; Golden Globe; Annie Award for Best Film in each respective animated category.    

==Plot==
Young filmmaker and scientist Victor Frankenstein lives with his parents, Edward and Susan Frankenstein, and his beloved dog, Sparky, in the quiet town of New Holland. Victors intelligence is recognized by his classmates at school, his somber next-door neighbor, Elsa Van Helsing, mischievous, Igor (fictional character)|Igor-like Edgar "E" Gore, obese and gullible Bob, overconfident Toshiaki, creepy Nassor, and an eccentric girl nicknamed Weird Girl, but communicates little with them due to his relationship with his dog. Concerned with his sons isolation, Victors father encourages him to take up baseball and make achievements outside of science. Victor hits a home run at his first game, but Sparky, pursuing the ball, is struck by a car and killed.
 the effect raise the dead. The two reanimate a dead goldfish, which turns invisible due to an error with the experiment. Edgar brags about the undead fish to Toshiaki and Bob, which, in panic of losing the upcoming science fair, inspires them to make a rocket out of soda bottles, which causes Bob to break his arm and Mr. Rzykruski to be blamed and fired due to his accused influencing and reviling the townsfolk for questioning his methods when he steps up for self-defense. So, the gym teacher replaces Mr. Rzykruski.

Eventually, Edgars fish disappears when he tries to show it to a skeptical Nassor (who was told by Toshiaki), and when Edgar is confronted by Toshiaki, Nassor and Bob on the baseball field at school, he accidentally reveals Victors actions, inspiring them to try reanimation themselves. Victors parents discover Sparky in the attic and are frightened, causing the dog to flee. Victor and his parents search for Sparky while the classmates invade the lab, discovering Victors reanimation formula. The classmates separately perform their experiments, which go awry and turn the dead animals into monsters — Mr. Whiskers holds a dead bat while it is electrocuted, turning him into a monstrous cat with vampire wings and bloodlust. Edgar turns a dead rat he found in the garbage into a wererat, Nassor revives his mummified hamster Colossus and Toshiakis turtle Shelley is covered in a growth formula and turns into a giant Gamera-like monster. Bobs Sea-Monkeys grow into amphibious humanoids. The monsters break loose into the town fair where they wreak havoc.

After finding Sparky at the towns pet cemetery, Victor sees the monsters attacking the fair and goes to help his classmates deal with them — the Sea-Monkeys explode after eating salt-covered popcorn, and Colossus is stepped on by Shelley, while the rat and Shelley are returned to their original, deceased forms after being electrocuted. During the chaos, Persephone, Elsa Van Helsings pet poodle, is grabbed by Mr. Whiskers and carried to the town windmill with Elsa and Victor chasing after. The townsfolk blame Sparky for Elsas disappearance and chase him to the windmill, which Mayor Burgermeister accidentally ignites with his torch. Victor and Sparky enter the burning windmill and rescue Elsa and Persephone, but Victor is trapped inside. Sparky rescues Victor, only to be dragged back inside by Mr. Whiskers. A final confrontation ensues, and just as Mr. Whiskers has Sparky cornered, a flaming piece of wood breaks off and impales him, killing him instantly. He gives one dying screech, and the windmill collapses on Sparky, killing him again. To reward him for his bravery, the townsfolk gather to revive Sparky with their car batteries, reanimating him once more. Persephone, who has a hairstyle similar to Elsa Lanchesters Bride of Frankenstein, comes to Sparky as the two dogs share their love.

==Voice cast==
 , Winona Ryder, Martin Landau, Charlie Tahan and Allison Abbate at the films premiere at the Fantastic Fest in Austin, Texas.]]
* Charlie Tahan as Victor Frankenstein, a young scientist who brings his dog (and best friend) Sparky back to life. Santa Claus Is Comin to Town.   
* Catherine OHara as Susan Frankenstein, Victors mother / Gym Teacher, an unnamed teacher who replaced Mr. Rzykruski / Weird Girl, an eccentric girl who is one of Victors classmates and obsessed with the psychic predictions of her cat, Mr. Whiskers 
* Martin Landau as Mr. Rzykruski, the eccentric but wise science teacher at Victors school who speaks in a thick Eastern European accent. His teachings inspire Victors effort to resurrect Sparky, and he acts as a mentor to Victor.  The character was inspired by Burtons childhood icon, Vincent Price. 
* Winona Ryder as Elsa van Helsing, a kind next-door neighbor, and one of Victors classmates.   
* Frank Welker as Sparky, Sea Creatures Edgar "E" Gore, a kyphosis|hunch-backed child (inspired by Igor (fictional character)|Igor) and one of Victors classmates. Hes the first to know of Victors success in bringing Sparky back to life.   
* Robert Capron as Bob, an obese boy who is one of Victors classmates.   
* Conchata Ferrell as Bobs mother, an obese and stereotypical suburban housewife who dotes upon her son. She believes in the status quo, and that her misguided actions are in Bobs best interest. 
* James Hiroyuki Liao as Toshiaki, Victors rival-like former enemy and one of his classmates. 
* Tom Kenny as Fire Chief / Soldier / Man in Crowd 
* Christopher Lee as Dracula (in stock footage from Dracula (1958 film)|Dracula).

==Production==

===Development=== Alice in Family Dog for Sparky. 

===Filming===
 ]]
Filming began at Three Mills Studios in July 2010.  The crew created three giant sound stages, including Victors cluttered family attic, a cemetery exterior, and a high school interior. The sound stages were then divided into 30 separate areas to deal with the handcrafted, frame-by-frame style of filmmaking. Compared to other stop-motion animation sets, Frankenweenie s set is much larger. As IGN notes, the main character Sparky had to be "dog-size compared to the other human characters, but also large enough to house all the elements of the mechanical skeleton secreted within his various foam and silicon-based incarnation". On the other hand, the mechanics are small and delicate, and in some instances they had to have Swiss watchmakers create the tiny nuts and bolts. Around 200 separate puppets were used, with roughly 18 different versions of Victor. The puppets also have human hair, with 40–45 joints for the human characters and about 300 parts for Sparky.         

===Music===
 
In early 2011 it was announced that Danny Elfman would score Frankenweenie, with work already started on pre-production music. 

Prior to the films release, an "inspired by" soundtrack album, Frankenweenie: Unleashed!, as well as   on September 25, 2012.  Frankenweenie: Unleashed! contains bonus content that includes a custom icon and an app that will load a menu to view more the bonus content, provide input, or buy more music from Disney Music Group. 

==Release==
 ]] John Carter replacing the film for the once planned March 9, 2012 release.  The film premiered on September 20, 2012, on the opening night of Fantastic Fest, an annual film festival in Austin, Texas.  The film opened the London Film Festival on October 10, 2012, in the United Kingdom. 

===Marketing===
In the lead up to the films release in October 2012, there was a traveling art exhibition detailing the work that has gone into creating the film. During the exhibition it was possible to see sets and characters that were used for the stop motion feature film. 

From September 14, 2012, to November 5, 2012, Disney California Adventure offered exclusive scenes from the film during Muppet*Vision 3Ds nighttime operating hours. 

At Disneyland, Sparkys tombstone was added to the pet cemetery outside of Haunted Mansion Holiday, a seasonal attraction that features characters from Burtons The Nightmare Before Christmas. 

===Home media===
Frankenweenie was released on DVD, Blu-ray, and Blu-ray 3D on January 8, 2013.  The film is accompanied with a two-minute short animated film, titled Captain Sparky vs the Flying Saucers. 

==Reception==

===Critical response===
The film has received mostly positive reviews from critics. Based on 198 reviews, the film currently holds a "Certified Fresh" rating of 87% on review aggregator Rotten Tomatoes, with an average rating of 7.6/10. The sites critical consensus reads, "Frankenweenie is an energetic stop-motion horror movie spoof with lovingly crafted visuals and a heartfelt, oddball story."  Metacritic, which assigns a weighted average score out of 100 to reviews from mainstream critics, gives the film a score of 74 based on 38 reviews. 

Justin Chang of Variety (magazine)|Variety reacted positively to the film, saying that it "evinces a level of discipline and artistic coherence missing from the directors recent live-action efforts."  Todd McCarthy of The Hollywood Reporter gave the film a mediocre review by explaining that while the various creative elements of the film "pay homage to a beloved old filmmaking style", the film mostly feels "like second-generation photocopies of things Burton has done before."  Roger Ebert gave the film three out of four stars, while regarding the film as "not one of Burtons best, but it has zealous energy" and that "the charm of a boy and his dog retains its appeal".    Chris Packham of The Village Voice gave the film a positive review, saying "Frankenweenie, scripted by John August, and based on a screenplay by Lenny Ripps from Burtons original story, is tight and brief, hitting all the marks youd expect from an animated kids film, and enlivened by Burtons visual style. The man should make more small movies like this one."  Christy Lemire of the Associated Press gave the film three out of four stars, saying "Revisiting the past - his own, and that of the masters who came before him - seems to have brought this filmmakers boyish enthusiasm back to life, as well."  Kerry Lengel of The Arizona Republic gave the film three out of five stars, saying "Its all perfectly entertaining, but never really reaches the heights of hilarity, perhaps because everything about the plot is underdeveloped."  Lisa Schwarzbaum of Entertainment Weekly gave the film an A-, saying "The resulting homage to Frankenstein in particular and horror movies in general is exquisite, macabre mayhem and a kind of reanimation all its own." 

Michael Phillips of the   gave the film three and a half stars out of five, saying "Frankenweenie is enlivened with beguiling visuals and captivating action sequences. The science is murky at best, but the underlying themes are profound, and the story is equal parts funny and poignant. Its Burtons most moving film."  Rafer Guzman of Newsday gave the film two and a half stars out of four, saying "Its a quintessential Burton film, but also more Disney than a lot of Disney films."  Amy Biancolli of The San Francisco Chronicle gave the film four out of four stars, saying "The overall effect is great cinema, good fun, a visual feast for pie-eyed Burton fans - and a terrifically warped reminder of just how freaky a PG film can be." 

Elizabeth Weitzman of the   gave the film a negative review, saying "Burton half succeeds in making this revamped Frankenweenie its own distinctive creature, pieced together from the essential bits of the 29-minute original. But he just doesnt know when to stop, and his overgrown creation gets the better of him."  Betsy Sharkey of the Los Angeles Times gave the film three out of five stars, saying "There are so many horror auteurs Burton wants to thank that the film is absolutely bursting at the seams with knowing nods."  A. O. Scott of The New York Times gave the film three out of five stars, saying "While Frankenweenie is fun, it is not nearly strange or original enough to join the undead, monstrous ranks of the classics it adores." 

Ty Burr of The Boston Globe gave the film four out of four stars, saying "Frankenweenie is a mere 87 minutes long, which turns out to be just the right length; theres not enough time for Burton to go off the rails as he does in so many of his films."  Tom Long of The Detroit News gave the film a B+, saying "Frankenweenie may just be a wacky horror cartoon, but its an awfully good wacky horror cartoon. Frighteningly good, you might say."  Lou Lumenick of the New York Post gave the film three and a half stars out of four, saying "Frankenweenie is still the most Tim Burton-y of the directors films, and not just because it contains a vast catalog of references to his own movies - everything from Edward Scissorhands to the underrated 1989 Batman."  Richard Corliss of Time (magazine)|Time gave the film a positive review, saying "This 3-D, black-and-white "family" comedy is the years most inventive, endearing animated feature."  Stephen Whitty of the Newark Star-Ledger gave the film four out of four stars, saying "The stop-motion animation - a favorite tool of Burtons - is given loving attention, and the character design is full of terrific touches, such as the hulking flat-topped schoolmate who looks a bit like a certain man-made monster."  Michael OSullivan of The Washington Post gave the film three out of four stars, saying "Designed to appeal to both discriminating adults and older kids, the gorgeous, black-and-white stop-motion film is a fresh, clever and affectionate love letter to classic horror movies."  Moira MacDonald of The Seattle Times gave the film three out of four stars, saying "Older kids, horror-movie buffs and Burton fans will likely enjoy this oddly gentle tale of a boy and his dog." 

===Box office===
Frankenweenie grossed $35,291,068 in North America, and $46,200,000 in other countries, for a worldwide total of $81,491,068.  In North America, the film opened at number five in its first weekend, with $11,412,213, behind Taken 2, Hotel Transylvania, Pitch Perfect and Looper (film)|Looper.  In its second weekend, the film dropped to number seven grossing an additional $7,054,334.  In its third weekend, the film dropped to number nine grossing $4,329,358.  In its fourth weekend, the film dropped to number 12 grossing $2,456,350. 

===Accolades===
{| class="wikitable" style="width:100%;"
|+List of awards and nominations
! Award
! Category
! Recipient(s)
! Result
|-
| 85th Academy Awards    Best Animated Feature Film of the Year
| Tim Burton
| rowspan="8"  
|-
| American Cinema Editors 
| Best Edited Animated Feature Film
| Chris Lebenzon, A.C.E. & Mark Solomon
|-
| rowspan="5" | Annie Awards   Best Animated Feature
|
|-
| Production Design in an Animated Feature Production
| Rick Heintzich
|-
| Voice Acting in an Animated Feature Production
| Atticus Shaffer
|-
| Voice Acting in an Animated Feature Production
| Catherine OHara
|-
| Writing in an Animated Feature Production
| John August
|-
| BAFTA Awards    Best Animated Film
| rowspan="4"| Tim Burton
|-
| Boston Society of Film Critics
| Best Animated Film
|  
|- Critics Choice Awards 
| Best Animated Feature
| rowspan="4"  
|-
| Chicago Film Critics Association
| Best Animated Feature
|-
| Cinema Audio Society
| Outstanding Achievement in Sound Mixing for Motion Pictures Animated
|-
| Dallas-Fort Worth Film Critics Association
| Best Animated Film
| rowspan="10"| Tim Burton
|-
| Florida Film Critics Circle
| Best Animated Feature
|  
|- Golden Globe Awards  Best Animated Feature Film
| rowspan="2"  
|-
| Houston Film Critics Society
| Best Animated Film
|-
| Kansas City Film Critics Circle
| Best Animated Film
| rowspan="4"  
|-
| Los Angeles Film Critics Association
| Best Animation
|-
| Nevada Film Critics Society
| Best Animated Movie
|-
| New York Film Critics Circle
| Best Animated Film
|-
| Online Film Critics Society
| Best Animated Feature
| rowspan="5"  
|-
| Phoenix Film Critics Society
| Best Animated Film
|-
| Producers Guild of America
| Outstanding Animated Theatrical Motion Pictures
| Allison Abbate, Tim Burton
|-
| San Diego Film Critics Society
| Best Animated Film
| rowspan="3"| Tim Burton
|-
| Satellite Awards 
| Best Motion Picture, Animated or Mixed Media
|- Saturn Awards   Best Animated Film
| rowspan="2"  
|- Best Music
| Danny Elfman
|-
| Southeastern Film Critics Association
| Best Animated Film
| rowspan="4"| Tim Burton
| rowspan="4"  
|-
| St. Louis Gateway Film Critics Association
| Best Animated Film
|-
| Toronto Film Critics Association
| Best Animated Feature
|-
| Washington DC Area Film Critics Association
| Best Animated Feature
|}

==See also==
* List of films featuring Frankensteins monster
* List of black-and-white films produced since 1970

==References==
 

==External links==
 
*  
*  
*  
*  
*  
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 