Sambaram
{{Infobox film
| name           = Sambaram
| image          =
| image_size     =
| caption        =
| director       = Kondapalli Dasaradh Kumar
| producer       = Teja
| writer         = Teja
| starring       = Nitin Nikitha Seeta Benarjee Giri Babu SV Krishna Reddy Paruchuri Venkateswara Rao Rallapally Suman Setty
| music          = R. P. Patnaik
| cinematography = Prasad
| editing        = KV Krishna Reddy
| distributor    = Chitram Movies
| released       = 31 July 2003
| runtime        = 172 mins.
| country        = India
| language       = Telugu
| budget         =
| preceded_by    =
| followed_by    =
}} Telugu film released in 2003 in film|2003, starring Nitin and Nikitha in lead roles. Kondapalli Dasaradh Kumar directed this film.

== Plot ==
Ravi (Nitin) and Geeta (Nikita) are childhood friends. Ravi is a carefree guy who roams around with friends without concentrating on studies. Geeta is a sincere student who passes engineering studies. Ravi has love feelings for her and everybody in the town is aware of it except for Geeta. When she is asked by Nitins sister-in-lawSeeta when she is planning to marry Ravi, she expresses her surprise and says that they are just friends and she has no feelings for him. She shows her materialistic attitude by saying that she wants her husband to be financially and academically more stronger than her. Ravi is devastated after listening to
her logic. He realizes the importance of being a responsible family member and takes up odd jobs to earn money. He finally gets the visa to work in Dubai. He wants to leave for Dubai as it gives him an opportunity to prove his money-earning ability and it also it gives him the despite of being away from her lady love Geeta. Meanwhile, Geetas father expires and she starts realizing that it takes more than academics and finances to make a guy qualified to be a husband. And by that time Ravi is already in the airport. Would Geeta express her love to Ravi before he leaves for Dubai?

==Cast==
{|class="wikitable"
|-
! Actor/Actress !! Role
|- Nitin
|Ravi
|- Nikitha
|Geeta
|- Seeta
|Ravis sister-in-law
|-
|- Paruchuri Brothers|Paruchuri Gopala Krishna
|Ravis father
|- Benarjee
|Ravis brother
|- Giri Babu
|Geetas dad
|- SV Krishna Reddy Lecturer
|-
|M. S. Narayana
|
|- Rallapalli (actor)|Rallapalli
|
|- Venu Madhav Venu Madhav
|
|}

== Opening weekend ==
The movie opened to a terrific opening at the Box office due to Hero Nitin and director Dasaradh. The Movie collected a phenomenal INR 48,40,000 during its opening weekend in India. 

==References==
 

==External links==
*  
 

 
 
 

 