Aashiq Hoon Baharon Ka
{{Infobox film
| name           = Aashiq Hoon Baharon Ka
| image          = Aashiq Hoon Baharon Ka.jpg
| image size     =
| caption        =
| director       = J. Om Prakash
| producer       = Raj Bhatija  K.K. Talwar
| writer         =
| narrator       =
| starring       = Rajesh Khanna  Zeenat Aman
| music          = Laxmikant-Pyarelal
| cinematography =
| editing        =
| distributor    =
| released       = 1977
| runtime        =
| country        = India
| language       = Hindi
| budget         =
}} 1977 Indian Bollywood romance film drama directed by J. Om Prakash.

==Plot==
Ashok successfully completes his studies and becomes a M.Sc graduate. His mother wants him to pursue his higher studies in Switzerland but Ashok hesitates as more money is required. But his moher convinces him and sends him to Swiss. Ashok stays in the place arranged by the friend of his principal who owns a hotel and also a step-father of a spoilt Vikram who hates Ashok. He meets Veera an arrogant girl who tries to steal his car while on his way to car race. Ashok wins a car race and sends the money which he won in the race to his mother. Veeras father meets Ashok and proposes a business deal with him in atomic science field. Ashok accepts the deal as he requires Uranium to continue his research. Veera and Ashok fall in love which is disliked by Vikram as he wants to marry her and grab all her property. Hence he frames a Uranium theft crime with the help of Ashoks assistant. Ashok is jailed by Veeras father and Veera also believes her father. But Ashok proves his innocence by making Vikram accept his acts by himself. Veera pleads guilty for her acts and apologies to Ashok. Veeras father accepts their love on a condition that he should stay in Swiss forever to which Ashok refuses and Veera also supports Ashok. Vikram kidnaps Veera and demands huge sum of money from her father for her release. Veeras father unwillingly seeks Ashoks help to bring back Veera. Ashok fights with Vikram and rescues Veera and both unite finally.

==Cast==
*Rajesh Khanna as  Ashok Sharma
*Zeenat Aman as  Veera Rai
*Danny Denzongpa as  Vikram (Jamunadas son)
*Preeti Ganguli as  Mary John
*Julie as  Olga
*Pinchoo Kapoor as Mr. John
*Sulochana Latkar as  Ashoks mother
*Nadira as Heera (Jamundas wife)
*Om Prakash as Mr. Jamunadas Rehman as Mr. Chandidas Rai
*Asrani as Murlidhar

==Soundtrack==
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Tera Dil Kya Kehta Hai"
| Kishore Kumar
|-
| 2
| "Aashiq Hoon Baharon Ka"
| Kishore Kumar
|-
| 3
| "I Am In Love"
| Kishore Kumar, Lata Mangeshkar
|-
| 4
| "Mere Gore Galon Ka"
| Lata Mangeshkar
|-
| 5
| "Mashriq Se Jo Aaye"
| Kishore Kumar
|}
==External links==
*  

 
 
 
 
 

 