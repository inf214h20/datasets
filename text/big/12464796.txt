Three Texas Steers
{{Infobox film
| name           = Three Texas Steers 
| image          = Three Texas Steers poster.jpg
| caption        = Theatrical poster
| director       = George Sherman
| producer       = William A. Berke
| writer         = William Colt MacDonald Ray "Crash" Corrigan Max Terhune Carole Landis
| music          =  Ernest Miller
| editing        = Tony Martinelli
| distributor    = 
| released       =  
| runtime        = 56 minutes
| country        = United States 
| language       = English
| budget         = 
}}
 Three Mesquiteers" Western B-movie    starring John Wayne and Carole Landis. Wayne played the lead in eight of the fifty-one films in the series. The director was George Sherman.

==Cast==
* John Wayne as Stony Brooke Ray "Crash" Corrigan as Tucson Smith
* Max Terhune as Lullaby Joslin
* Carole Landis as Nancy Evans
* Ralph Graves as George Ward
* Roscoe Ates as Sheriff Brown
* Collette Lyons as Lillian
* Billy Curtis as Hercules, the Midget Ted Adams as Henchman Steve
* Stanley Blystone as Henchman Rankin David Sharpe as Tony
* Ethan Laidlaw as Henchman Morgan
* Lew Kelly as Postman

==See also==
* John Wayne filmography

==References==
 

==External links==
*  

 
 

 
 
 
 
 
 
 
 
 
 
 