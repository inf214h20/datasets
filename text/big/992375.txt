Festival Express
{{Infobox film
| name           = Festival Express
| image          = festival_express.png
| caption        = The movie poster
| director       = Bob Smeaton
| producer       = Gavin Poolman John Trapman
| starring       = Janis Joplin Grateful Dead The Band Delaney and Bonnie Buddy Guy
| music producer = Eddie Kramer
| cinematography = Peter Biziou Bob Fiore Clarke Mackey
| editor         = Eamonn Power
| prod company   = Apollo Films PeachTree Films
| distributor    = THINKFilm
| released       =  
| runtime        = 90 minutes
| country        = United Kingdom
| language       = English
| budget         = 
}} Delaney & Bonnie & Friends.   The film combines live footage shot during the 1970 concerts, as well as footage aboard the train itself, interspersed with present-day interviews with tour participants sharing their often humorous recollections of the events.   

The film, released by  , The Truman Show). The original 1970 footage was filmed by director Frank Cvitanovich. A DVD release followed the films 2003 theatrical run.    

==The Festival== John Dawson, as well as Janis Joplin.  

The event, initially billed as the Transcontinental Pop Festival, was promoted by Eaton-Walker Associates (consisting of Thor Eaton, George Eaton, and Ken Walker) and was produced and financed by Industrial and Trade Shows of Canada (ITS) division of MacLean-Hunter Publishing Company (Hugh F. Macgregor developed and conceived the project from beginning to end) and was to encompass the following cities:   

{| class="wikitable" style="text-align: center; width: 75%"
|+  Transcontinental Pop Festival Venues 
|-
! width="8%" | Date !!  width="8%" | City !!  width="12%" | Venue !!  width="5%" | Time !!  width="8%" | Admission !!  width="5%" | Attendance !! width="12%" | Comments
|-
| June 24, 1970 (National Holiday (Quebec)|St. Jean-Baptiste Day)
| Montreal|Montreal, QC
| Autostade
| 12PM-12AM (planned)
| $12 ($10 advance) (planned)
| N/A
| Originally planned for June 20–21, but was changed to June 24; show was cancelled by the city in mid-June, 1970, a few weeks prior to event  
|-
| June 27–28, 1970
| Toronto|Toronto, ON CNE Stadium (aka CNE Grandstand and CNE Exhibition Stadium)
| 12PM-12AM
| One Day - $10 ($9 advance) Two Day - $16 ($14 advance)
| 37,000   
|
|-
| July 1, 1970 (Canada Day)
| Winnipeg|Winnipeg, MB  Winnipeg Stadium 
| 12PM-12AM 
| $12 ($10 advance) 
| 4,600  
|
|-
| July 4–5, 1970
| Calgary|Calgary, AB 
| McMahon Stadium 
| 12PM-12AM 
| One Day - $10 ($9 advance) Two Day - $16 ($14 advance) 
| 20,000  
|
|-
| July 4–5, 1970
| Vancouver|Vancouver, BC  PNE Empire Stadium
| N/A 
| N/A 
| N/A 
| Venue could not be secured from the city and Vancouver was dropped from the tour in mid-April, 1970 
|}
 Capilano Stadium, for the event, but this was denied by the Vancouver City Council over several concerns, including inadequate sanitary and food facilities, challenges with policing the event, and vagrancy.     Therefore, Vancouver was dropped from the tour, and Calgary was subsequently added. The event in Calgary was initially to be held in an open field, Paskapoo Ski Hill (to later become Canada Olympic Park), but the city requested it be held at McMahon Stadium instead, as it would permit better organization and security. 

The tour ultimately began in Toronto at the CNE Grandstand, which was plagued with about 2500 protestors who objected to what they viewed as exploitation by price-gouging promoters. The opposition was organized by the May 4th Movement (M4M), the left-rebel group that grew out of the May 4, 1970   fame). There are some reports indicating a free concert was also performed on the second day, albeit to a much smaller crowd of about 500, as many of the protesters paid admission to the event on the second day. Many people spent the night and following day sleeping in the park until the second show at CNE Grandstand ended at 12:30am on June 29.     
 Prime Minister Trudeau.  The event was not plagued with protest or any appreciable violence, however. 

In Calgary, the third and final stop, the police wished to avoid the protests that were witnessed in Toronto and their presence seemed to subdue the crowds outside the stadium, though there were many complaints about the ticket prices.  It was estimated that about 1000 people managed to sneak in on Saturday by climbing fences (a few rushed the gates) early in the day, but security was tightened and on Saturday afternoon and Sunday fewer people had sneaked in for free.  However, there was a heated altercation between promoter Ken Walker and Calgary mayor Rod Sykes after Sykes strongly suggested to Walker on Sunday afternoon that he open the gates and let the kids in for free after the show was well underway. Walker, who was livid about the mayors intrusion and his reference to Walker as "Eastern scum" "trying to skim" the young people of Calgary, claimed to have punched the mayor in the mouth, and boasted that he still had a scar on his hand to prove it.      
 American Beauty; the Bands performance showed them at the pinnacle of their powers;  for Janis Joplin, it would turn out to be some of her last performances, as she died about three months later. In the film, musician Kenny Gradney, who performed with Delaney & Bonnie, commented on the atmosphere during the tour, "It was better than Woodstock, as great as Woodstock was." Mickey Hart of the Grateful Dead further said, "Woodstock was a treat for the audience, but the train was a treat for the performers."   

==Songs==
 
 

===Performed in the film===
* "Go to Heaven|Dont Ease Me In", Grateful Dead
* "Friend of the Devil", Grateful Dead
* "Slippin and Slidin", The Band
* "Comin Home Baby", Mashmakhan
* "Money (Thats What I Want)", Buddy Guy Blues Band
* "Lazy Day", The Flying Burrito Brothers
* "The Weight", The Band Cry Baby", Janis Joplin John Dawson,  Janis Joplin, Jerry Garcia and Bob Weir Rock & Roll Is Here to Stay", performed by Sha Na Na New Speedway Boogie", Grateful Dead Great Speckled Bird (with Jerry Garcia and Delaney Bramlett)
* "I Shall Be Released", The Band
* "Tell Mama", Janis Joplin
* "Cold Jordan/Jordan", performed by Jerry Garcia
 

===Additional songs on DVD===
* "Casey Jones", Grateful Dead (opening credits) 13 Questions", Seatrain
* "Tom Rush (1970 album)|Childs Song", Tom Rush
* "Thirsty Boots", Eric Andersen
* "As the Years Go By", Mashmakhan Great Speckled Bird
* "Hoochie Coochie Man", Buddy Guy Blues Band Hard to Handle", Grateful Dead Easy Wind", Grateful Dead
* "Move Over", Janis Joplin
* "Kozmic Blues", Janis Joplin

===Other Festival Express performances=== Long Black Veil" and "Rockin Chair", from July 5, 1970 in Calgary appear on The Bands anthology album, A Musical History. 
 

==Other festival performers==
These festival performers were not featured in the film or DVD extras:
* Robert Charlebois
* Delaney & Bonnie (Delaney Bramlett sits in with Great Speckled Bird during "C.C. Rider" and Bonnie Bramlett can be seen on the train) The Ides of March James and The Good Brothers Mountain (member Leslie West can be seen jamming at the beginning of the film)
* Ten Years After (only performed in Toronto - fantastic performances of Im Goin Home and Slow Blues In C were filmed, but lead guitarist and singer Alvin Lee wouldnt approve their appearance in the film, saying he thought his guitar was out of tune) (the source for this is Gavin Poolman, producer of the film, in May 2011) Traffic (only Traffic was on the train but the bands record company wouldnt allow them to appear in the film. Two performances were filmed anyway, however Steve Winwoods management refused permission for these to appear in the film) (the source for this is Gavin Poolman, producer of the film, in May 2011) John Dawson is seen in the notorious "Aint No More Cane" scene, sitting on the couch with Rick Danko and Janis Joplin, as they work through several drunken verses of the tune. Buddy Cage can also be seen, performing as a member of Great Speckled Bird.

==Film production==
Because the Festival Express tour turned out to be a complete financial disaster, the film project was shelved soon afterwards, as the promoters sued the filmmakers, and the footage mysteriously disappeared. Some of the films reels turned up in the garage of the original producer Willem Poolman, where they had been stored for decades and used at various times as goal posts for ball hockey games played by his son Gavin. The plan to resurrect the film was started in 1999 by executive producer Garth Douglas and story consultant James Cullingham, who found many more reels in the Canadian National Film Archives vault, where it had been kept in pristine condition and unknown to the world. Garth got in touch with Gavin, who had grown up to become a London-based film producer. Gavin produced the film together with his old high school friend John Trapman (who had played in some of those ball hockey games), and Bob Smeaton, double Grammy Award-winning director of the The Beatles Anthology was brought on board. The music tracks were mixed at Torontos MetalWorks Studios, and produced by Eddie Kramer, Jimi Hendrixs producer, and engineer for Led Zeppelin, Woodstock (film)|Woodstock, and Derek & The Dominos Live In Concert.

The film was produced by London-based Apollo Films (now owned by  ) together with PeachTree Films from Amsterdam.

==Release==

===Premieres and festivals=== Miami Film Festival, Wisconsin Film Festival, NatFilm Festival, Karlovy Vary Film Festival, Maine International Film Festival, Flanders International Film Festival, the IN-EDIT Barcelona International Music Documentary Film Festival, Hohaiyan Music Film Festival, Rio Film Festival, Vienna International Film Festival and the São Paulo International Film Festival.

The film was released theatrically on July 23, 2004 in the United States, as well as in Canada, the United Kingdom, Australia, New Zealand, Japan, The Netherlands, Luxembourg, Belgium, and Scandinavia. 

===DVD release=== Region 1 was released on November 2, 2004 by New Line Home Video. 
 Region 1 was released in 2014 by Apollo Media Ltd.

===Box-office reception===
The film earned $1.2 million at the US Box Office, and the DVD went straight in at number 1 on the Music Video & Concert DVD top-sellers charts at Amazon.com, Barnes & Noble, Tower Records, etc., and has had an average customer review rating of 4.5 stars out of 5.  According to Rotten Tomatoes, Festival Express was the second most critically acclaimed film released in 2004. 

==Legacy==
 South by Southwest Film Conference and Festival (SXSW Film) in Austin, Texas.  

==Miscellany== Robert Hunter was on the trip, and soon after wrote "Might as Well", a song filled with imagery from the legendary trip that was often played live by Grateful Dead, but released as a studio tune on the 1976 Jerry Garcia solo album Reflections (Jerry Garcia album)|Reflections.    

* NFL quarterback Joe Kapp can be seen in football pads and sweats during a crowd shot of the Deads performance of "New Speedway Boogie." 

==References==
 

==External links==
*  
*  
*  
*  
*  
 
 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 