An Elephant Called Slowly
{{Infobox Film
| name           = An Elephant Called Slowly
| image          = AnElephantCalledSlowly.JPG
| image_size     =
| caption        = DVD cover James Hill James Hill Bill Travers James Hill Bill Travers
| narrator       = 
| starring       = Bill Travers Virginia McKenna
| music          = Bert Kaempfert Howard Blake
| cinematography = Simon Trevor
| editing        = Andrew Borthwick
| distributor    =U.K.:  
| released       = U.K.: 1969 Japan: 10 April 1971 U.S.A. (New York City): 17 April 1971
| runtime        = 91 min.
| country        = U.K. English
| budget         = 
| preceded_by    = 
| followed_by    = 
}} James Hill, and has been released to VHS and DVD.

==Plot and cast== Land Rover which seems to have a mind of its own.
 Swahili for "Slowly Slowly"). The couple visit Game Wardens George Adamson and Charles Mutiso (Ali Twaha) who suggest the couple have been "adopted" by the three elephants and recommend they make friends.  At home, pole pole has moved in and made herself comfortable; the couple create a wallow on the grounds for her, travel to the river for a swim, and take evening walks with her.  When their house-sitting duty comes to a close, Travers and McKenna are confident the three elephants will join a herd in the vicinity. Cast includes Joab Collins as Henry, and Raffles Harman.

==Soundtrack==
The film contains several instrumental numbers by Bert Kaempfert:
#"A Swingin Safari"
#"Market Day"
#"Afrikaan Beat"
#"Happy Trumpeter"
#"Tootie Flutie"
Additional music was composed and conducted by Howard Blake.

==Reception== Howard Thompson of The New York Times found the first half hour slow, he thought Travers and McKenna "personable", and approved the film for childrens writing, "Its ideal for the 4- to 11-year age bracket." The critic also noted, "the picture is a tame, dramatically uneventful affair, nowhere near as finished or forceful as Born Free. Frail and even rambling in structure, the picture edges purposefully into the wilds for some magnificently authentic animal footage...The only jolt, handled discreetly, is the sight of some jungle wild dogs finishing off a gazelle." 

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 