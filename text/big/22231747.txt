The Lemon Sisters
{{Infobox film
| name           = The Lemon Sisters
| image          = The Lemon sisters.jpg
| alt            = 
| caption        = Film poster
| director       = Joyce Chopra 
| producer       = Harvey Weinstein Bob Weinstein
| writer         = Jeremy Pikser
| starring       = Diane Keaton Carol Kane Kathryn Grody
| music          = Dick Hyman Howard Shore Bobby Byrne
| editing        = Michael R. Miller Joe Weintraub
| studio         = 
| distributor    = Miramax Films
| released       = August 31, 1990 (USA)
| runtime        = 93 min.
| country        = United States English
| budget         = $15,000,000  
| gross          = $3,473,905 (USA) 
}} American film commercial and critical failure after being shelved for more than a year with extensive revisions.   

==Plot==
Three lifelong friends work the bars in 1980s Atlantic City performing the songs of the Girl groups#1950s and 1960s|60s girl groups.

==Principal cast==
{| class="wikitable" width="50%"
|- bgcolor="#CCCCCC"
! Actor !! Role
|-
| Diane Keaton || Eloise Hamer
|-
| Carol Kane || Franki DAngelo
|-
| Kathryn Grody || Nola Frank
|-
| Elliott Gould  || Fred Frank
|-
| Rubén Blades ||  C.W.
|-
| Aidan Quinn || Frankie McGuinness
|-
| Estelle Parsons  || Mrs. Kupchak
|-
| Nathan Lane || Charlie Sorrell
|}

==Critical reception==
Caryn James of The New York Times hated the film:
 

==References==
 

== External links ==
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 


 