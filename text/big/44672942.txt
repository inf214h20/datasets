Sneha (film)
{{Infobox film
| name = Sneha
| image = 
| caption =
| director = V. S. Reddy
| writer = Posani Krishna Murali Raasi  Karan 
| producer = Mohan Natarajan
| music = V. Ravichandran
| cinematography = G. S. V. Seetharam
| editing = Shyam
| studio = Chintamani Cine Arts
| released =  
| runtime = 145 minutes
| language = Kannada
| country = India
| budget =
}} romantic drama drama film Raasi and Karan in Telugu film Snehithulu (1998) directed by Muthyala Subbaiah and written by Posani Krishna Murali. The film was also remade in Tamil as Aasaiyil Oru Kaditham (1999). The music was composed by Ravichandran to the lyrics of K. Kalyan. The film spoke about the friendship between a man and woman and their troubled marital relationships.

== Cast ==
* V. Ravichandran 
* Ramya Krishna Raasi  Karan
* Srinivasa Murthy
* Doddanna
* Kashi
* Chitra Shenoy
* Mandya Ramesh
* Thalapathi Dinesh

== Soundtrack ==
The music was composed by V. Ravichandran and lyrics were written by K. Kalyan.  A total of 6 tracks have been composed for the film and the audio rights brought by Lahari Music.

{{track listing
| headline = Track listing
| extra_column = Singer(s)
| lyrics_credits = yes
| collapsed = no
| title1 =  Ellide Balina Jaanapada
| extra1 = K. J. Yesudas
| lyrics1 = K. Kalyan
| length1 = 
| title2 = Hrudayake Jaarida
| extra2 = S. P. Balasubrahmanyam
| lyrics2 = K. Kalyan
| length2 = 
| title3 = Jeevana Ennuva Mano
| lyrics3 = K. Kalyan
| length3 = 
| title4 = Surya Sutthangilla
| extra4 = Sukhwinder Singh, Anuradha Sriram
| lyrics4 = K. Kalyan
| length4 = 
| title5 = Chandagathi Chandada
| extra5 = L. N. Shastry, Suma Shastry
| lyrics5 = K. Kalyan
| length5 = 
| title6 = Yakamma Beku Intha Loka
| extra6 = K. J. Yesudas
| lyrics6 = K. Kalyan
| length6 = 
|}}

== References ==
 

== External links ==
*  
*  

 

 
 
 
 
 
 
 


 

 