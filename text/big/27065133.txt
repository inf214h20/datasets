Dhoop
{{Infobox film
| name           = Dhoop
| image          = dhoop.jpg
| director       = Ashwini Chaudhary
| producer       = Parth Arora, Saket Bahl, Karan Grover, Sanjay Reddy
| writer         = Kumud Chaudhary
| starring       = Om Puri Revathi Gul Panag Sanjay Suri Yashpal Sharma
| music          = Lalit Sen
| lyrics         = Nida Fazli
| editing        = Arvind Tyagi
| distributor    = Dream Boat Entertainment Pvt Ltd and Fundamental Films
| released       = 2003
| runtime        = 126 mins
| country        = India
| language       = Hindi
|}}

Dhoop (   ,  ) is a 2003 bollywood, war drama film directed by  Ashwini Chaudhary, based on Battle of Tiger Hill.
 
  MVC and his family. 
 

==Plot==
The movie is based on the events surrounding the death of Capt Anuj Nayyar, MVC of 17 Jat Regiment of the Indian Army, who was killed in operations against Pakistani Army regular soldiers, in the southwest sector of Tiger Hill on July 5, 1999 as part of the Kargil conflict.
The story of the Kapoor family in the film depicts the real events that happened in the lives of the Nayyar family.   

Capt Rohit Kapoor (Sanjay Suri) is a young officer in the 17 Jat Regiment of the Indian Army.
His father Prof S K Kapoor (Om Puri), is a professor of economics in the Delhi School of Economics. His mother Sarita Kapoor (Revathi), is a librarian at the Delhi University Central Library. He is engaged to be married to Pihu Verma (Gul Panag). 

The storyline of the movie takes place between 1999–2002, beginning with the death of Captain Kapoor while successfully defeating insurgents to capture Pt 4875 on the western side of Tiger Hill, Kargil.

The news is a huge shock to his family and the first half of the film portrays their attempts to cope with their grief. They receive messages of condolence from various high government officials as they attempt to come to terms with their loss.

As compensation for the loss of their son, the government allots them a franchise for running a petrol pump (gas station). Mrs Kapoor is aghast at such an offer, and the family is not  inclined to avail of this compensation.
However after a visit from Major Kaul, Capt Kapoors commanding officer, Prof.Kapoor and Pihu feel it might be a worthy site to commemorate the memory of Rohit and decide to take up the offer.

However, as they attempt to make this dream a reality, they encounter massive corruption and red tape at various levels of Indian bureaucracy. They are threatened and humiliated by various government officials and hooligans. However, the family perseveres in the face of immense odds and continues to struggle. The latter half of the film narrates the story of their mission.

==Reception==
The film was a low budget release and did not make any great records at the box office. However, its basis on a moving true story, and excellent acting by a fine acting cast led to huge critical acclaim. 

Om Puris superb portrayal of the courageous Prof Kapoor and Revathis sensitive depiction of his wife are particularly notable among a slew of other substantial contributions from a stellar supporting cast.  The movie marks the cinematic debut of Gul Panag who essays her role with quiet dignity.

The film also has an evocative sound track with songs sung by Jagjit Singh, Hariharan (singer)|Hariharan, Shreya Ghoshal and the Wadali brothers. Jagjit Singhs ghazals, in particular, lend an added luster to the film.

==Inspiration==
The film is an account of the inspiring struggle of Prof Nayyar and his family against entrenched systemic corruption.  His dream was finally realized with the setting up of the petrol pump named Kargil Heights, in the Vasundhara Enclave area of New Delhi  

Prof Nayyars struggles continued for several years, and the film serves to highlight the appalling treatment meted out to the families of courageous Indian soldiers who gave their lives in protecting the nation. 

==Awards and nominations==
===Zee Cine Awards 2003===
====Nominated====
*Best Actress in a Supporting Role - Revathi
*Best Story - Kumud Chaudhary

===Screen Weekly Awards 2004===
====Nominated==== Sanjay Chauhan
*Best Story - Kumud Chaudhary
*Best Supporting Actress - Revathi

==Cast==
*Om Puri as Prof Suresh Kumar Kapoor
*Revathi as Savita Kapoor
*Sanjay Suri as Captain Rohit Kapoor
*Gul Panag as Pihu Verma Yashpal Sharma as Inspector Ram Singh Malik

==References==
 

==External links==
*  

 
 
 
 
 
 