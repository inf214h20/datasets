Shadow of the Castles
{{Infobox film
| name           = Shadow of the Castles
| image          = 
| caption        = 
| director       = Daniel Duval
| producer       = Michel Seydoux
| writer         = Daniel Duval
| starring       = Philippe Léotard
| music          = 
| cinematography = Pierre Lhomme
| editing        = 
| distributor    = 
| released       =  
| runtime        = 90 minutes
| country        = France
| language       = French
| budget         = 
}}

Shadow of the Castles ( ) is a 1977 French drama film written and directed by Daniel Duval. It was entered into the 10th Moscow International Film Festival where it won the Silver Prize.   

==Cast==
* Philippe Léotard as Luigi
* Albert Dray as Rico
* Zoé Chauveau as Fatoun
* Marcel Dalio as Père Renard (as Dalio)
* Stéphane Bouy as Le jeune avocat
* Yves Beneyton as Le chef motard
* Martine Ferrière as La mère supérieure
* Jean Puyberneau as Lavocat
* Louise Chevalier as La réligieuse
* Jean-François Chauvel as Le président du Tribunal
* Philippe Duval as Le père Capello
* Clara Boulet as Le mère Capello

==References==
 

==External links==
*  

 
 
 
 
 
 
 