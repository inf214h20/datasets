The Life Before This
{{Infobox film
| name           = The Life Before This
| image          =
| caption        =
| director       = Jerry Ciccoritti
| producer       =
| writer         = Semi Chellas
| starring       = Catherine OHara Joe Pantoliano Sarah Polley Stephen Rea
| music          =
| cinematography =
| editing        =
| distributor    = Alliance Atlantis
| released       =  
| runtime        = 92 minutes
| country        = Canada
| language       = English
| budget         =
| gross          =
}}

The Life Before This is a 1999 Canadian film directed by Jerry Ciccoritti. It begins with a massacre perpetrated in a coffee shop by two gunmen, and then uses flashbacks to show how each of the people present found themselves in the shop on that day. It shows how small choices can result in life-changing situations.

==Cast==
*  Catherine OHara  ...  Sheena
*  Joe Pantoliano  ...  Jake Maclean
*  Sarah Polley  ...  Connie
*  Stephen Rea  ...  Brian
*  Bernard Behrens  ...  Monsieur Farrin
*  Martha Burns  ...  Gwen Maclean
*  Fab Filippo  ...  Michael
*  Emily Hampshire  ...  Margaret
*  David Hewlett  ...  Nick
*  Leslie Hope  ...  Alice
*  Joel S. Keller  ...  Kevin
*  Dan Lett  ...  Sam

==External links==
* 

 
 
 

 