L'homme orchestre
{{Infobox film
| name           = Lhomme orchestre
| image          = Lhomme orchestre.jpg
| caption        = 
| director       = Serge Korber
| producer       = Gaumont Film Company
| writer         = Jean Halain Serge Korber
| starring       = Louis de Funès
| music          = François de Roubaix
| cinematography = 
| editing        = 
| distributor    = Gaumont distribution
| released       = 1970
| runtime        = 85 minutes
| country        = France
| language       = French
| budget         = 
}} comedy film from 1969 directed by Serge Korber, written by Jean Halain, and starring by Louis de Funès. 

== Cast ==

* Louis de Funès : Mr Edouard
* Noëlle Adam : Françoise
* Olivier de Funès : Philippe Evans
* Daniel Bellus : Young motorist
* Paul Préboist : Manager of the Roman hotel
* Franco Fabrizi : Franco Buzzini
* Max Desrau : A motorist
* Micheline Luccioni : The passenger who dredges on the yacht
* Martine Kelly : dancer who gets married
* Henri Bellus : The young motorist in the red light
* Jacqueline Doyen : A motorist in the red light
* Christor Georgiadis : Chris, taker of sound and cook of the troops
* Vittoria Di Silverio: Romes woman
* Paola Tedesco : The Sicilian girl
* Tiberio Murgia : The Sicilian father
* Marco Tulli : The commissioner

== References ==
 

==External links==
*  
*   at the AlloCiné

 
 
 
 
 
 
 
 


 
 