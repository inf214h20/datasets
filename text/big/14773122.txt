Amy (1981 film)
{{Infobox film
| name = Amy
| image = Amy 1981 Film.jpg
| director = Vincent McEveety
| producer = Jerome Courtland
| writer = Noreen Stone Chris Robinson
| music = Robert F. Brunner
| cinematography = Leonard J. South Walt Disney Productions Buena Vista Distribution
| released =  
| runtime = 100 minutes
| country = United States
| language = English
}} Walt Disney Productions, distributed by Buena Vista Distribution, written by Noreen Stone and directed by Vincent McEveety, and starring Jenny Agutter.  

== Plot == Chris Robinson) objects to a wife with a career, Amy leaves her husband and comfortable lifestyle. She goes on to devote her life to teaching sight-and-hearing-impaired students at a tradition-bound special school. Amy teaches the impaired students how to speak, most of the students have never heard their own name. Amys students take on a team of "normal" kids at a football game.

== Cast ==
*Jenny Agutter as Amy Medford
*Barry Newman as Dr. Ben Corcoran
*Kathleen Nolan as Helen Gibbs Chris Robinson as Elliot Medford
*Lou Fant as Lyle Ferguson
*Margaret OBrien as Hazel Johnson
*Nanette Fabray as Malvina
*Otto Rechenberg as Henry Watkins
*David Hollander as Just George
*Cory Bumper Yothers as Wisley Moods
*Alban Branton as Eugene
*Ronnie Scribner as Walter Ray

== Educational film ==
In 1982, Disney Educational Services excerpted a sequence out of the  film for educational use, entitled Amy-on-the-Lips.

==DVD release==
Disney released a DVD-on-Demand version of this film as part of their "Disney Generations Collection" line of DVDs on June 26, 2011.

== References ==

 

== External links ==
*  
* 
* 
* 

 

 
 
 
 
 
 
 
 

 