Come What May (film)
 
{{Infobox film
| name           = Come What May
| image          = Come What May (film).jpg
| caption        = 
| director       = George D. Escobar Manny Edwards
| producer       = George D. Escobar Manny Edwards
| writer         = Manny Edwards George D. Escobar David Halbrook
| starring       = Austin Kearney Victoria Emmons Karen Jezek Kenneth Jezek Michael Farris
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 
| country        = United States
| language       = English
| budget         = $300,000
| gross          = 
}}
Come What May is an American direct-to-video drama film released on March 17, 2009 by Advent Film Group.  The filmmakers intentionally released it on DVD and not to theaters. George Escobar said, "Going straight to DVD will allow church groups to become the Film distributor|distributor."  The film tells the story of Caleb Hogan who tries to overturn Roe v. Wade in a moot court championship.

==Plot== socially conservative perspective. Since Caleb insists that the PHC Moot Court team will provide him with the best training for working in her law firm, Judith agrees to pay for education on the condition that he wins the Moot Court National Championship.

The Moot Court topic is soon announced as a parental notification case, weighing the daughters right to have an abortion against her parents right to know about it. Caleb believes that the team will be most successful by arguing for a small exception to the apparent "right to abortion" by showing substantial parental interests, not because he himself supports abortion but because he insists that attempting to overturn Roe v. Wade will provoke a negative emotional response from the liberal judges. His partner, Rachel (Victoria Emmons), however, argues that they should make the case to overturn it and completely deny the right to an abortion.
 Supreme Court Kenny Jezek), and her son, Caleb, Judith takes the case and asks Rachel to work as an intern at her law firm to help her prepare. As the Moot Court championship tournament draws near, Caleb is faced with a growing rift forming between his parents, an escalating argument with his debate partner and the future of his education and his family at stake.

== Cast ==
 
*Austin Kearney as Caleb Hogan
*Victoria Emmons as Rachel Morton Kenny Jezek as Don Hogan
*Karen Jezek as Judith Hogan
*Michael Farris as himself
 

== Production == homeschooled students, digital high-definition video in order to keep the production on a small budget. Filming locations included Purcellville, Virginia|Purcellville, Virginia, at Patrick Henry College and Lynchburg, Virginia at Liberty University. Pickup filming and re-shoots took place in January 2008 in Purcellville and Leesburg, Virginia|Leesburg, Virginia. Post-production editing was performed in Tennessee, sound design was completed in Colorado Springs, Colorado, and the films marketing and distribution activities were organized in Northern Virginia. In total, over 40 students from 16 different U.S. states participated in the making of Come What May.  

== Accolades== Redemptive Film Festival (held at Regent University) 

==References==
 
 

==External links==
*  
*  
 
 
 
 
 
 
 