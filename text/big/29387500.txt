Reel Horror
 
 
Reel Horror is a 1985 horror film/|horror/comedy film directed by Ross Hagen from a screenplay by Jeanne Lucas, composed primarily of footage from other films, in two of which Hagen acted.  The films included are The Butchers (1970), Up Your Teddy Bear (1970), Daddys Deadly Darling (1972), Cycle Psycho (1973), Bad Charleston Charlie (1973), Nicole (film)|Nicole (1978), and Night Creature (1978).  

The premise of the film is that ghosts are escaping from these films and need to be recaptured.

The film received generally negative reviews and currently has a 2.2 rating on The Internet Movie Database.

==Cast==
(in credits order)
*Alexandra ...  Hecate
*John Hayden ...  Irving
*Howard Honig ...  Murray Mogul Robert ONeil ...  The Projectionist
*Tony Lorea ...  The Make-up Artist
*Jeanne Lucas ...  Zena Zoft
*Meredith Dee ...  The Ghost

The remaining cast is all in archive footage from the other films, although they are the ones promoted on the DVD packaging.

==External links==
*  on IMDb
*  on Oh, the Horror!

 
 
 