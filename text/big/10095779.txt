Marie (film)
{{Infobox film
| name = Marie
| image = Marie film.jpg
| caption = Theatrical release poster
| director = Roger Donaldson
| producer =
| story = Peter Maas
| screenplay = John Briley
| starring = {{Plainlist|
* Sissy Spacek
* Jeff Daniels
* Keith Szarabajka
* Morgan Freeman
* Fred Thompson
}}
| editing = Neil Travis
| cinematography = Chris Menges
| music = Francis Lai
| distributor = Metro-Goldwyn-Mayer
| released =  
| runtime = 112 minutes
| country = United States
| language = English
| budget = $9 million
| gross = $2,507,995
}}
Marie (also known as Marie: A True Story) is a 1985 film starring  .
 Fred Thompson, Lisa Banes, John Cullum, Graham Beckel, and Macon McCalman.

==Cast==
* Sissy Spacek as Marie
* Jeff Daniels as Eddie Sisk
* Morgan Freeman as Charles Traughber
* Keith Szarabajka as Kevin McCormack
* Fred Thompson as Himself

==Critical reception==
 Fred Thompson, Senate probe of the Watergate scandal. Thompson was Ragghiantis attorney during the actual trial, and played himself. This was his first film role.

==References==
 

==External links==
* 
* 
*   by Roger Ebert
*   in The New York Times
*   at  

 

 
 
 
 
 
 
 
 
 
 


 
 