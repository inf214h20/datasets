Danube Waltz (film)
 
{{Infobox film
| name           = Danube Waltz
| image          = 
| image_size     = 
| caption        = 
| director       = Victor Janson
| producer       = Gabriel Levy   Rudolf Walther-Fein
| writer         = Walter Reisch 
| narrator       = 
| starring       = Harry Liedtke   Harry Hardt   Adele Sandrock   Ferdinand Bonn
| music          = 
| editing        = 
| cinematography = Guido Seeber 
| studio         = Aafa-Film 
| distributor    = Aafa-Film 
| released       = 16 January 1930
| runtime        = 
| country        = Germany
| language       = Silent   German intertitles
| budget         =  
| gross          =
| preceded_by    = 
| followed_by    = 
}} German silent film directed by Victor Janson and starring Harry Liedtke, Harry Hardt and Adele Sandrock. It was part of a group of nostalgic screenplays by Walter Reisch set in his native Austria. 

==Cast==
* Harry Liedtke
* Harry Hardt  
* Adele Sandrock
* Paul Biensfeldt   
* Ferdinand Bonn   
* Peggy Norman 
* Hermann Picha   
* Ernő Verebes 

==References==
 

==Bibliography==
*Prawer, S.S. Between Two Worlds: The Jewish Presence in German and Austrian Film, 1910–1933. Berghahn Books, 2005.

==External links==
* 

 
 
 
 
 
 

 