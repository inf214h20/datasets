Malatesta (film)
{{Infobox film
| name           = Malatesta
| image          =
| caption        =
| director       = Peter Lilienthal
| producer       = Manfred Durniok
| writer         = Michael Koser Peter Lilienthal Heathcote Williams
| starring       = Eddie Constantine
| music          =
| cinematography = Willy Pankau
| editing        = Brigitta Kliemkiewicz Annemarie Weigand
| distributor    =
| released       =  
| runtime        = 80 minutes
| country        = West Germany
| language       = German
| budget         =
}}

Malatesta is a 1970 German drama film directed by Peter Lilienthal. It was entered into the 1970 Cannes Film Festival.    It contains some biographical aspects of the life and thoughts of the Italian anarchist Errico Malatesta.

==Cast==
* Eddie Constantine – Malatesta
* Christine Noonan – Nina Vassileva
* Vladimír Pucholt – Gardstein
* Diana Senior – Ljuba Milstein
* Heathcote Williams – Josef Solokow
* Wallas Eaton – The Priest (as Wallace Eaton)
* Sheila Gill – Mrs. Gershon
* Sigi Graue – Fritz Svaars (as Siegfried Graue)
* Peter Hirsche – Cafiero

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 
 