Bright Lights (1916 film)
 
{{Infobox film
| name           = Bright Lights
| image          = Bright Lights cph.3b30244.jpg
| caption        = Al St. John, Mabel Normand and Roscoe Arbuckle
| director       = Roscoe Arbuckle
| producer       = Mack Sennett
| writer         =
| starring       = Roscoe Arbuckle Mabel Normand Al St. John
| music          =
| cinematography =
| editing        =
| distributor    = Triangle Film Corporation
| released       =  
| runtime        =
| country        = United States
| language       = Silent with English intertitles
| budget         =
}}
 short comedy film directed by Roscoe Arbuckle    and starring Arbuckle, Mabel Normand and Al St. John.

==Cast==
* Roscoe Arbuckle
* Mabel Normand
* Al St. John
* Joe Bordeaux
* Jimmy Bryant
* Minta Durfee
* Gilbert Ely
* William Jefferson

==See also==
* List of American films of 1916
* Fatty Arbuckle filmography

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 

 