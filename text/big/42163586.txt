Han Gong-ju
{{Infobox film name           = Han Gong-ju  image          = Han Gong-ju (poster).png director       = Lee Su-jin producer       = Lee Su-jin writer         = Lee Su-jin  starring       = Chun Woo-hee cinematography = Hong Jae-sik    editing        = Choi Hyun-sook music          = Kim Tae-seong distributor    = CGV Movie Collage released       =   runtime        = 112 minutes country        = South Korea language       = Korean
}}
Han Gong-ju ( ) is a  2013 South Korean film written and directed by Lee Su-jin, starring Chun Woo-hee in the title role.     It was inspired by the infamous Miryang gang rape case of 2004. 
 2013 Busan CGV Movie Collage Award and the Citizen Reviewers Award. 

As it traveled the international film festival circuit, Han Gong-ju won several top prizes, including the Golden Star at the 2013 Marrakech International Film Festival,   the Tiger Award (given to films that "give young filmmakers a voice" and "push boundaries") at the 2014 International Film Festival Rotterdam,  and the Jury Prize, the Critics Prize, and the Audience Award at the 2014 Deauville Asian Film Festival.   
 classicist structure, this film lures the spectator to participate in the pleasures of storytelling through an extraordinary and intricate narrative puzzle."  

Han Gong-ju was released in theaters on April 17, 2014. 

==Plot==
The film is a coming-of-age film|coming-of-age story about a high school student, the titular Han Gong-ju, who loses her friend in a terrible gang rape incident and is pressured to leave her family and move to another school by the perpetrators parents. To escape scandal, she finds herself living in the home of her former teachers mother in a different neighborhood. After transferring to a new school, the withdrawn and traumatized Gong-ju keeps to herself and tries to move on from what happened. But she is befriended by Eun-hee, who convinces her to join an a cappella club. When news gets out of Gong-jus new hobby, a group of parents of her former classmates causes a stir. Living with a stranger and cold to her new classmates, it takes a long time for Gong-jus troubled past to catch up with her, but when it does the revelation is devastating.  

==Cast==
*Chun Woo-hee as Han Gong-ju
*Jung In-sun as Eun-hee
*Lee Young-ran as Ms. Lee
*Kim So-young as Hwa-ok

==Box office==
Han Gong-ju was released on just over 200 screens (a sizeable exposure for a Korean independent film), and through strong word of mouth, it was a hit with critics and audiences. On its opening day on April 17, 2014, approximately 10,000 people watched the film, but this increased at an unprecedented pace, crossing the 100,000 admissions mark (an enormous benchmark for a Korean independent production) in just nine days, which was quicker than the pace of recent indie favorites, such as Breathless (2009 film)|Breathless (2009) in 19 days and Bedevilled (2010 film)|Bedevilled (2010) in ten. As of April 29, it reached the 150,000 audience mark, breaking the record of Jiseul (2013), which sold 140,490 tickets in 12 days. 
 The Target and The Amazing Spider-Man 2, it continued to attract average audiences of 2,000 a day.  At the end of its run, the film had a total of 223,297 admissions.

==Awards and nominations==
{| class="wikitable"
|-
! Year
! Award
! Category
! Recipient
! Result
|-
| rowspan=3| 2013
| rowspan=2|   18th Busan International Film Festival  CGV Movie Collage Award 
| rowspan=8|   Han Gong-ju
|  
|-
| Citizen Reviewers Award
|  
|-
|   13th Marrakech International Film Festival
| Golden Star
|  
|-
| rowspan=24| 2014
|   43rd International Film Festival Rotterdam
| Tiger Award
|  
|-
| rowspan=3|   16th Deauville Asian Film Festival
| Jury Prize
|  
|-
| Critics Prize
|  
|-
| Audience Award
|  
|-
|   18th Fantasia International Film Festival
| Audience Award, Best Asian Film - Silver
|  
|-
| rowspan=2|   14th Directors Cut Awards
| Best New Actress
| Chun Woo-hee
|  
|-
| Best Independent Film Director
| Lee Su-jin
|  
|-
| rowspan=6|   23rd Buil Film Awards 
| Best Film
| Han Gong-ju
|  
|-
| Best Director
| Lee Su-jin
|  
|-
| Best Actress
| Chun Woo-hee
|  
|-
| Best New Director
| Lee Su-jin
|  
|-
| Best New Actress
| Chun Woo-hee
|  
|-
| Best Screenplay
| Lee Su-jin
|  
|-
| rowspan=3|   34th Korean Association of Film Critics Awards 
| Best Actress
| Chun Woo-hee
|  
|-
| Best Screenplay
| Lee Su-jin
|  
|-
| Critics Top 10
| Han Gong-ju
|  
|-
| rowspan=3|   51st Grand Bell Awards 
| Best Actress
| Chun Woo-hee
|  
|-
| Best New Director
| Lee Su-jin
|  
|-
| Best Screenplay
| Lee Su-jin
|  
|-
|   15th Women in Film Korea Awards
| Best Actress
| Chun Woo-hee
|  
|-
| rowspan=4|   35th Blue Dragon Film Awards 
| Best Actress
| Chun Woo-hee
|  
|-
| Best New Director
| Lee Su-jin
|  
|-
| Best Screenplay
| Lee Su-jin
|  
|-
| Best Editing
| Choi Hyun-sook
|  
|-
| rowspan=17| 2015
| rowspan=3|   6th KOFRA Film Awards  
| Best Film
| Han Gong-ju
|  
|-
| Best Actress
| Chun Woo-hee
|  
|-
| Discovery Award
| Chun Woo-hee
|  
|-
| rowspan=2|   10th Max Movie Awards
| Best Actress
| Chun Woo-hee
|  
|-
| Best Independent Film
| Han Gong-ju
|  
|-
| rowspan=2|   20th Chunsa Film Art Awards 
| Best Actress
| Chun Woo-hee
|  
|-
| Best New Director
| Lee Su-jin
|  
|- 2nd Wildflower Film Awards  
| Grand Prize
| Han Gong-ju
|  
|-
| Best Director (Narrative Film)
| Lee Su-jin
|  
|-
| Best Actress
| Chun Woo-hee
|  
|-
| Best Screenplay
| Lee Su-jin
|  
|-
| Best Cinematography
| Hong Jae-sik
|  
|-
| Best New Director
| Lee Su-jin
|  
|- 51st Baeksang Arts Awards
| Best Film
| Han Gong-ju
|  
|-
| Best New Actress
| Chun Woo-hee
|  
|-
| Best New Director
| Lee Su-jin
|  
|-
| Best Screenplay
| Lee Su-jin
|  
|}

==References==
 

==External links==
* 
* 
* 

  
 
 