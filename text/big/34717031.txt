Sangaree (film)
{{Infobox film
| name           = Sangaree
| image          = Sangaree poster 1953.jpg
| image_size     =
| alt            =
| caption        =
| director       = Edward Ludwig
| producer       = William H. Pine, William C. Thomas David Duncan, Frank Moss
| narrator       =
| starring       = Fernando Lamas, Arlene Dahl
| music          = Lucien Cailliet 
| cinematography = Lionel Lindon Howard Smith
| studio         = Pine-Thomas Productions
| distributor    = Paramount Studios 
| released       =  
| runtime        = 94 minutes
| country        = United States English
| budget         =
| gross          = $1.8 million
}}

 1953 3D period costume drama film by director Edward Ludwig.  It was adapted from the novel Sangaree by Frank G. Slaughter.   The film was released by Paramount Studios. It stars Fernando Lamas and Arlene Dahl.
The film is set during the American Revolution and focuses on an indentured servant, Dr. Carlos Morales, who rises to power in the state of Georgia. 

The film earned an estimated $1.8 million at the North American box office during its first year of release. 

Cinematography was by Lionel Lindon.  It was Paramounts first 3-Dimensional film release. 

==Plot==
On his death bed, old Revolutionary War general Darby bequeaths his Georgia estate, Sangaree, to a man he trusts, Dr. Carlos Morales, requesting he seek freedom for servants and slaves.

The generals son Roy has no objection, but knows his sister Nancy is determined to keep Sangaree in the family. She and her fiance, attorney Harvey Bristol, make it clear to Morales that he will have a fight on his hands.

Roys wife, Martha, is secretly still in love with Morales, an old beau. Martha spreads word that Nancy is in league with a French pirate who is making raids off the Savannah coast. A plague outbreak also concerns Morales, who is running a free clinic.

Morales learns that Bristol has a business partnership with the pirate and that Martha, not Nancy, is involved. Bristols warehouse is the source of the plague so Morales burns it to the ground. Martha contracts the plague and dies, and Nancy joins forces with Morales to live together at Sangaree.

==Cast==
* Fernando Lamas as Dr. Carlos Morales
* Arlene Dahl as Nancy Darby
* Patricia Medina as Martha Darby
* Tom Drake as Dr. Roy Darby
* Lewis Russell as Capt. Bronson
* Francis L. Sullivanas Dr. Bristol 
* Charles Korvin as Felix Pagnol
* Lester Matthews as Gen. Victor Darby 
* Roy Gordon as Dr. Tyrus 
 
==References==
 

==External links==
* 
*    at American Film Institute
*  

 
 
 
 
 
 
 
 
 
 

 