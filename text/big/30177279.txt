Bayen Hath Ka Khel
 
{{Infobox film
| name           = Bayen Hath Ka Khel
| image          = 
| caption        = 
| director       = Vijay Kaul
| producer       = Khaliq Paria
| writer         = Vijay Kaul Pran Waheeda Rehman Danny Denzongpa  Girish Karnad Kulbhushan Kharbanda
| music          = Laxmikant Pyarelal
| cinematography =
| editor         =
| distributor    = 
| released       = 
| runtime        = 190 minutes
| country        = India
| language       = Hindi
| budget         = 
}}
Bayen Hath Ka Khel is a 1985 Hindi film directed by Vijay Kaul and produced by Khaliq Paria. Vijay Kaul debuted as a director with this film. This film has Rajesh Khanna in the lead opposite Tina Munim. The songs are sung by Kishore Kumar for Rajesh Khanna.

==Cast==
*Rajesh Khanna 
*Tina Munim Pran
*Waheeda Rehman
*Danny Denzongpa 
*Girish Karnad  
*Kulbhushan Kharbanda  

==Music==
* "Ye Jhooth Bolti Hai" - Kishore Kumar, Asha Bhosle 
* "Ek Bosa Ham Ne Manga Wahre Mowla Shahji" - Kishore Kumar, Asha Bhosle
* "Kisi Ko Harana Kisi Ko Jitana" - Kishore Kumar, Asha Bhosle 
* "Mera Naam Dil Pe Likh Lijiye Ga Huzoor" - Amit Kumar

==References==
 

==External links==
*  

 
 
 

 