The Painter (film)
 
{{Infobox film
| name           = The Painter
| image          = 
| caption        = 
| director       = Göran du Rées Christina Olofson
| producer       = Göran du Rées
| writer         = Göran du Rées Kent Andersson
| music          = 
| cinematography = Göran du Rées
| editing        = 
| distributor    = 
| released       =  
| runtime        = 89 minutes
| country        = Sweden
| language       = Swedish
| budget         = 
}}

The Painter ( ) is a 1982 Swedish documentary film directed by Göran du Rées and Christina Olofson. It was entered into the 13th Moscow International Film Festival.   

==Cast==
* Hans Mosesson – Stig Dahlman Kent Andersson as Eskil
* Anneli Martini as Mona
* Tomas Forssell as Factory worker
* Weiron Holmberg as Sune
* Hans Johansson as Tjocken
* Mats Johansson as Factory worker
* Stellan Johansson as Bertil
* Jussi Larnö as Finnen
* Sten Ljunggren as Supervisor
* Ingmar Nilsson as Factory worker
* Hans Wiktorsson as Åke

==References==
 

==External links==
*  
*  

 
 
 
 
 
 


 
 