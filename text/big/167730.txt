Lady Caroline Lamb (film)
{{Infobox film
| name           = Lady Caroline Lamb
| image          = ladycarolinelamb.jpg
| caption        = Theatrical release poster
| director       = Robert Bolt
| writer         = Robert Bolt Richard Chamberlain Laurence Olivier
| producer       = Fernando Ghia
| cinematography = Oswald Morris
| music          = Richard Rodney Bennett
| distributor    = EMI Films|MGM-EMI (UK)   United Artists (US)
| released       = 1972
| runtime        =
| language       = English
| budget         =
}} Prime Minister). The film was written and directed by Robert Bolt and starred his wife, Sarah Miles, as Lady Caroline.
 Duke of Richard Chamberlain George IV. Michael Wilding appeared, in an uncredited, non-speaking cameo with his last wife, Margaret Leighton, who played Lady Melbourne.

==Cast==
*Sarah Miles as Lady Caroline Lamb
*Jon Finch as William Lamb
*Richard Chamberlain as Lord Byron
*John Mills as Canning
*Margaret Leighton as Lady Melbourne Pamela Brown as Lady Bessborough
*Silvia Monti as Miss Millbanke
*Ralph Richardson as King George IV
*Laurence Olivier as Duke of Wellington Michael Wilding as Lord Holland		
*Peter Bull as Minister Charles Carson as Potter
*Sonia Dresdel as Lady Pont
*Nicholas Field as St. John
*Caterina Boratto 
*Felicity Gibson as Girl in Blue

==Reception==
The film was given a major international release based upon its stellar cast, but received mixed critical reviews, and was a box-office failure; it was criticized both for its historical inaccuracies (the timing of William Lambs political career, the portrayal of Byron as a tall, handsome man who lacked his characteristic limp) and for its histrionic qualities and interpolations, such as an incoherent episode at night in some Italian ruins where beggars seem to attack Lady Caroline and her husband. Consistent with feminist developments of the early 1970s, the film characterizes Byron overly negatively and elevates Lady Caroline to a quasi-heroic status.
 Michael Wildings final film before his death in 1979.

The films failure dissuaded Bolt from further directorial work, and may have contributed to his breakup with Sarah Miles in 1976, although he remarried Miles in 1988, a marriage which lasted until his death in 1995.

The film had an atmospheric music score composed by Richard Rodney Bennett, who later based a concert work, Elegy for Lady Caroline Lamb for viola and orchestra, on some of the material. Oswald Morriss cinematography was praised for its beauty and clarity.

== External links ==
*  

== References ==
 

 
 
 
 
 
 
 
 
 
 
 