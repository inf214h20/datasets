In Prison Awaiting Trial
 
{{Infobox film
| name           = In Prison Awaiting Trial (Detenuto in attesa di giudizio)
| image          = In Prison Awaiting Trial.jpg
| caption        = Film poster
| director       = Nanni Loy
| producer       = Gianni Hecht Lucari Fausto Saraceni
| writer         = Sergio Amidei Emilio Sanna
| starring       = Alberto Sordi, Lino Banfi
| music          = Carlo Rustichelli
| cinematography = Sergio DOffizi
| editing        = Franco Fraticelli
| distributor    = 
| released       =  
| runtime        = 102 minutes
| country        = Italy
| language       = Italian
| budget         = 
}}

In Prison Awaiting Trial ( ) is a 1971 Italian drama film directed by Nanni Loy. It was entered into the 22nd Berlin International Film Festival where Alberto Sordi won the Silver Bear for Best Actor award.   

==Plot==
Roman surveyor Giuseppe Di Noi, who moved to Sweden for years, married to a Swedish woman and respected professional, decides to take on holiday in Italy his family. At the Italian border man was stopped and arrested without being given any explanation. After three days in jail in Milan, learns - through the efforts of a guard - to be charged with "manslaughter manslaughter" of a German citizen. Convinced that this is a misunderstanding, the victim is translated in prison in prison until the imaginary town of Sagunto (near Salerno) and interned in solitary confinement, because, being considered "fugitive", he is prevented from use of house arrest.

Di Noi undergoes a genuine judicial ordeal, full of humiliating treatment and impersonal. The nightmare continues far beyond the expected. The investigating magistrate scolds him that he could not hear because without a lawyer. The man is, against his will, involved in a riot and transferred first to a prison for inmates serving life sentences, and then in a psychiatric facility. It takes the obstinacy of his wife, the passionate interest of his lawyer and the benevolence of the investigating magistrate otherwise on vacation, to arrive at a logical explanation.

Received at the hospital, Di Noi legal learns from a highway viaduct Battipaglia-Matera - built years before by an Italian firm where he worked as a surveyor - it was then considered natural causes collapsed, causing the death of a German driver in transit. Us, in the meantime moved to Sweden and without international communication, not responding to the subpoena, was technically considered a fugitive and hence the arrest. Now clarified its position, the poor surveyor regains his freedom, but hes now irrevocably marked, physically and psychologically.

==Cast==
* Alberto Sordi as Giuseppe Di Noi
* Elga Andersen as Ingrid
* Andrea Aureli as Guardia
* Lino Banfi as Direttore del carcere di Sagunto
* Antonio Casagrande as Giudice
* Mario Pisu as Psichiatra
* Michele Gammino as Don Paolo
* Tano Cimarosa as Un secondino
* Gianni Bonagura as Avv. Sallustio Giordana (as Gianfelice Bonagura)
* Nino Formicola
* Silvio Spaccesi as Maresciallo
* Nazzareno Natale as Saverio
* Giovanni Pallavicino

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 


 