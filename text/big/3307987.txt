It Had to Be You!
 
 
{{Infobox film
| name           = It Had to Be You!
| image          = It Had to Be You poster.jpg
| image_size     = 
| caption        = 
| director       = Maurice Li   Andrew Loo
| producer       = 
| writer         = 
| narrator       = 
| starring       = Ekin Cheng  Karena Lam Eric Tsang
| music          = 
| cinematography = 
| editing        = 
| distributor    = Hong Kong:   Mei Ah Entertainment
| released       = 2005
| runtime        = 93 min
| country        = Hong Kong Cantonese
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
 2005 romantic comedy, filmed and set in Hong Kong, starring Ekin Cheng, Karena Lam, and Eric Tsang.

==Plot==
It Had to Be You is a light romantic comedy, sweet and touching. Ekin and Karena appear as lovers on the silver screen for the third time after Heroic Duo and The Floating Landscape. Previous collaborations allow them to perform naturally in front of the camera as a perfect match. The film also features Wu Bing, who stars in a Hong Kong movie for the first time. If you want to be amused by gorgeous stars in a light romance, it has to be Ekin and Karena in It Had to Be You!

Restaurant supervisor Jill (Karena Lam) has a handsome boyfriend Chi On (Wu Bing), but she is just his backup girlfriend. She knows she is the other girl, but her hope for being his one and only has never ceased until he changes his formal girlfriend once again. All her anger goes to her co-worker Jack (Ekin Cheng), who appears to be a womanizer but indeed shares a similar unfortunate romantic situation of being the backup boyfriend of an airhostess. Knowing that both are victims in romantic relationships, Jack and Jill no longer spar with each other and a liking between them start to develop.

==Cast==
* Ekin Cheng
* Karena Lam
* Eric Tsang
* Harvey Hu
* Benz Hui
* Yan Ng
* Derek Tsang
* Chin Kar Lok
* Bobo Chan
* Kristal Tin
* Hayama Go
* Benjamin Yuen
* Kitty Ho

==See also==
* List of Hong Kong films

== External links ==
*  

 
 
 


 
 