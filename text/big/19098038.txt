The Fiancee (film)
{{Infobox film
| name           = The Fiancee
| image          = 
| caption        = 
| director       = Günter Reisch Günther Rücker
| producer       = 
| writer         = Eva Lippold Günter Reisch Günther Rücker
| starring       = Jutta Wachowiak
| music          = 
| cinematography = Jürgen Brauer
| editing        = 
| distributor    = 
| released       =  
| runtime        = 83 minutes
| country        = East Germany
| language       = German
| budget         = 
}}
The Fiancee ( ) is a 1980 East German drama film directed by Günter Reisch and Günther Rücker and based on a novel by Eva Lippold. The film is about the resistance of the communist Hella Lindau (Jutta Wachowiak) and her fiancee Hermann Reimers (Regimantas Adomajtis) against the Nazis.
 official submission Best Foreign Language Film, but did not manage to receive a nomination. 

==Cast==
* Jutta Wachowiak as Hella Lindau
* Regimantas Adomaitis as Reimers
* Slávka Budínová as Lola
* Christine Gloger as Frenzel
* Inge Keller as Irene
* Käthe Reichel as Olser
* Hans-Joachim Hegewald as Hensch
* Barbara Zinn as Elsie
* Katrin Saß as Barbara
* Ewa Zietek as Hilde
* Ursula Braun as Naudorf

==See also==
* List of submissions to the 53rd Academy Awards for Best Foreign Language Film
* List of German submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
* 
*   at Filmportal.de
*   at the Deutsche Film-Aktiengesellschaft|DEFA-Stiftung  

 

 
 
 
 
 
 
 
 
 
 