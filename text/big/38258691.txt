Ideachi Kalpana
{{Infobox film
| name           = Ideachi Kalpana
| image          = Ideachi Kalpana.jpg
| border         = yes
| alt            =  
| caption        = Theatrical release poster
| director       = Sachin Pilgaonkar
| producer       = Sachin Pilgaonkar
| screenplay     = 
| story          = 
| writer         = Kshitij Zarapkar
| starring       = Sachin Pilgaonkar  Mahesh Kothare  Ashok Saraf
| music          = Avdhoot Gupte  Sachin Pilgaonkar
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 175 minutes
| country        = India
| language       = Marathi
| budget         = 
| gross          = 
}}
Ideachi Kalpana is a Marathi film released on 31 December 2010.  Produced and directed by Sachin Pilgaonkar. The movie is a laughter riot revolving around a lie, a lawyer who begins the lie and a whole bunch of people who wants answers.

== Synopsis ==
Jayram is a struggling actor who is slated for greatness or at least his sister Jaywanti thinks so. Her husband Manohar is a lawyer and also a fraud. He hasn’t fought a single case in court as he prefers out of court settlements. Jayram is hit by a car driven by Priti whose brother Mahesh Thakur is the commissioner of Police. Since she had taken her brothers car the hit and run case becomes that of the Commissioner. To solve the issue the commissioner comes to the hospital but by then Manohar has decided to sue the owner of the car for 50 lakh rupees.

Jayram is actually fine but Manohar convinces him that he should play along with his plan and remain in the hospital till the case ends leading to Thakur and Manohar in a face off in a non-existent case. Meanwhile Priti decides to tell Jayram that her brother is not involved in any of this but falls in love with him instead. Manohar leaks the news to the press and Jayram reads about it in the newspaper. Guilt ridden he now wants to tell Priti the truth. Somehow he escapes from the hospital and goes to the Commissioners bungalow but doesn’t meet Priti. He ends up face to face with the Commissioner. 

== Cast ==
*Sachin Pilgaonkar as Jayram
*Ashok Saraf as Manohar
*Mahesh Kothare as Mahesh Thakur
*Nirmiti Sawant as Jaywanti
*Bhargavi Chirmule as Priti

==Soundtrack==
The music is provided by Avdhut Gupte and Sachin Pilgaonkar.

===Track listing===
{{Track listing
| extra_column = Performer(s)
| title1 = Amhi Nahi Jaa | length1 =
| title2 = Ideachi Kalpana | length2 =
| title2 = Khulya Jagachi Reet | length3 =
| title2 = Laga Motariya Ka Dhakka | length4 =
}}

== References ==
 
 

==External links==
*  
*  
*  
*  

 
 
 


 