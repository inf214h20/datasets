The Devil's Trail
{{infobox film
| name           = The Devils Trail
| image          = Thedevilstrail-lanternslide-1919.jpg
| imagesize      =
| caption        = Lantern slide for the film.
| director       = Stuart Paton
| producer       = World Film Company
| writer         = Frank S. Beresford (story)
| starring       = Betty Compson
| music          =
| cinematography = William Thornley
| editing        =
| distributor    = World Film
| released       =  
| runtime        = 5 reel#Motion picture terminology|reels; 4,693 feet)
| country        = United States
| language       = Silent (English intertitles)
}}
The Devils Trail is a 1919 American silent drama film that is set in the woods of the Pacific Northwest. It was directed by  .

==Plot==
As described in a film magazine,    Dibec (Malatesta), a whiskey smuggler who trades liquor to the Indians for blankets and furs, is being pursued by the Royal Northwest Mounted Police. Dubec stops at the post where Mrs. Delisle, wife of Sergeant Delisle, is alone with her daughter Nonette and her baby sister Julie. Dubec kills Mrs. Delisle and abducts Nonette. Twelve years later, Julie (Compson) is celebrating her 16th birthday. The men of the Royal Mounted attend this function, and while the dinner is in progress, Sergeant MacNair arrives. He at once falls in love with Julie, and she with him, and this love persists even though MacNair is to succeed Julies father as commander of the post. The post is located at Chino Landing, and Sergeant Delisle has been unable to curb the lawless element brought by the gold rush. After twelve years Dubec has returned from the gold camp and is accompanied by Nonette. She is now a woman of the dance halls, and at first she succeeds in keeping her identity secret from her father. But when Delisles life is endangered by the lawless element and Julie is kidnapped by Dubec, Nonette reveals her secret. While MacNair, who has also been taken prisoner, fights to save Julie, Nonette brings her father and others to the scene. Dubec is captured and there is a happy reunion.

==Cast==
*Betty Compson - Julie Delisle 
*George Larkin - Sergeant MacNair
*William Quinn - Dutch Vogel
*Fred Malatesta - Dubec
*Claire Du Brey - Dubecs Wife
*H. C. Carpenter
*Joseph Franz
*Howard Crampton -
*Robert F. McGowan

unbilled
*Alberta Franklin

==Production==
The Devils Trail during production had the working title of Rose of the Border,  which would have reflected the name of Compsons role, then named Rose.

==References==
 

==External links==
 
* 
* 
* 

 
 
 
 
 
 
 
 


 