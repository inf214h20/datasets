Deva (film)
 
{{Infobox film name       = Devaa image      = caption    = director   = S. A. Chandrasekhar producer   = B. Vimal screenplay = S. A. Chandrasekhar story      = Shoba Chandrasekhar starring   =   music  Deva
|cinematography = R. Selva editing    = Gautham Raju studio     = B. V. Combines distributor = B. V. Combines released   =   runtime    = 160 mins country    = India language   = Tamil
| budget    =  1.7 crore
}}
 Tamil movie Swathi in the lead roles. It was directed by S. A. Chandrasekhar. The music is scored by Deva (music director)|Deva. 

==Plot==
The film is a story of  ) and Bharadhi (Swathi (actress)|Swathi). The two are deep in love but encounter opposition from Bharadhis father (Sivakumar), the head of the village. Sivakumar believes that his daughters suitor is not a suitable one, due to his perceived reputation throughout the village. Although the son of the well to do Manorama, it comes to light that Sivakumars disapproval stems from his relationship with his brother, who is seemingly apathetic and lackadaisical. The film ends happily with Sivakumar recognizing that his now son-in-law is in actuality a really upstanding citizen in the village. The two young lovers overcome all problems and live their lives happily married.

==Cast== Vijay as Deva
*Sivakumar as the village leader Swathi as Bharathi, daughter of the village leader Manorama as Raasathi, Devas mother Mansoor Ali Khan as Periyavar, Devas brother
*Manivannan
*Vinu Chakravarthy
*S A Chandrasekhar

==Crew==
* Screenplay, Dialogue and Directed by: S. A. Chandrasekhar
* Story: Shoba Chandrasekhar
* Produced by: B. Vimal Deva
* Cinematography: Selva. R 
* Editing: Gautham Raju
* Art: P. L. Shanmugam 
* Fights: Kanal Kannan
* Lyrics: Vaali (poet)|Vaali, Pulamaipithan and Kalidasan
* Choreography: D. K. S. Babu, Kala and Naseer Babu
* Costumes: Seiyyath Bava and S. Selvam
* Banner: B. V. Combines

==Soundtrack==
The music composed by Deva (music director)|Deva, while lyrics written by Vaali (poet)|Vaali, Pulamaipithan and Kalidasan. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers || Length (m:ss)
|- Vijay || 
|-
| 2 || Chinna Paiyyan || S. N. Surender, K. S. Chithra || 
|-
| 3 || Innoru Gandhi || Mano (singer)|Mano, S. N. Surender ||
|- Manorama ||
|- Vijay || 
|-
| 6 || Oru Kaditham (Female) || K. S. Chithra || 
|-
| 7 || Sollungo || Deva (music director)|Deva, Krishnaraj ||
|}

==References==
 

==External links==
*  

 

 
 
 
 
 
 


 