The Draughtsman's Contract
{{Infobox film
| name           = The Draughtsmans Contract
| image          = TheDraughtsmansContractDVD.jpg
| caption        = Cover of the 1999 Fox Lorber DVD release of The Draughtsmans Contract
| director       = Peter Greenaway
| producer       = David Payne
| writer         = Peter Greenaway
| narrator       = Anthony Higgins Hugh Fraser
| music          = Michael Nyman
| cinematography = Curtis Clark
| editing        = John Wilson
| studio         = British Film Institute Channel 4 United Artists Classics (USA)
| released       = 1982
| runtime        = 103 minutes
| country        = United Kingdom English
| budget         = £320,000 Alexander Walker, National Heroes: British Cinema in the Seventies and Eighties, Harrap, 1985 p 261 
| preceded_by    =
| followed_by    =
}} 1982 United British film Grand Prix of the Belgian Film Critics Association.

==Plot== Anthony Higgins), a young and arrogant artist and something of a Byronic hero, is contracted to produce a series of 12 landscape drawings of an estate, by Mrs. Virginia Herbert (Janet Suzman) for her absent and estranged husband.  Part of the contract is that Mrs. Herbert agrees "to meet Mr. Neville in private and to comply with his requests concerning his pleasure with me." Several sexual encounters between them follow, each of them acted in such a way as to emphasise reluctance or distress on the part of Mrs Herbert and sexual aggression or insensitivity on the part of Mr Neville. Whilst living on the estate, Mr. Neville gains quite a reputation with its dwellers, especially with Mrs. Herberts son-in-law, Mr. Talmann.

Mrs. Herbert, wearied of meeting Mr. Neville for his pleasure, tries to terminate the contract before all of the drawings are completed and orders Mr. Neville to stop. Neville refuses to void the contract and continues as before. Then Mrs. Herberts married but as yet childless, daughter Mrs. Talmann, who has apparently become attracted to Mr. Neville, seems to blackmail him into making a second contract in which he agrees to comply with what is described as her pleasure, rather than his — a reversal of the position in regard to her mother.

A number of curious objects appear in Nevilles drawings, which point ultimately to the murder of Mr. Herbert, whose body is discovered in the moat of the house. Mr. Neville completes his twelve drawings and leaves the house but returns to make an unlucky thirteenth drawing. In the evening, while Mr. Neville is apparently finishing the final sketch, he is approached by a masked stranger, who is obviously Mr. Talmann in disguise, who is then joined by Mr. Noyes, Mr. Seymore and the Poulencs, a pair of eccentric local landowner twins. The party accuses Mr. Neville of the murder of Mr. Herbert, for the drawings can be interpreted to suggest more than one illegal act and to implicate more than one person. After he defensively denies such accusations, the group ask Mr. Neville to remove his hat. He agrees mockingly, at which point they hit him on the head, burn out his eyes, club him to death and then throw him into the moat, at the place where Mr. Herberts body was found.

==Themes==
Although there is a murder mystery, its resolution is not explicit, it is implied is that the mother (Mrs Herbert) and daughter (Mrs Talmann) planned the murder of Mr Herbert.  Mrs Herbert and Mrs Talmann were aided by Mr Clarke, the gardener and his assistant. In order to keep the estate in their hands, they needed an heir. Because Mr Talmann was impotent, they used Mr Neville as a stud. Mr Herbert was murdered at the site where Mr Neville is murdered. (In the original treatment, Mr Herbert is murdered on his return, on the 12th day and the site was vetoed as a painting site because it was instead to be used as a murder site.)

== Background ==
 
The original cut of the film was about three hours long. The opening scene was about 30 minutes long and showed each character talking, at least once, with every other character. Possibly to make the film easier to watch, Greenaway edited it to 103 minutes. The opening scene is now about 10 minutes long and no longer shows all the interactions among all of the characters.  Some anomalies in the longer version film are deliberate   in the 17th century and the inclusion on the walls of the house of paintings by Greenaway in emulation of Roy Lichtenstein which are partly visible in the released version of the film. 

The released final version provides fewer explanations to the plots numerous oddities and mysteries. The main murder mystery is never solved, though little doubt remains as to who did it. The reasons for the living statue in the garden and why Mr. Neville attached so many conditions to his contract were also more developed in the first version.

==Locations==
Groombridge Place was the main location in this tale of 17th Century intrigue and murder.
 

==Music==
{{Infobox album  
| Name        = The Draughtsmans Contract
| Type        = soundtrack
| Longtype    =
| Artist      = Michael Nyman
| Cover       = Dcsoundtrack.jpg
| Cover size  = 200
| Caption     =
| Released    = 1982
| Recorded    = minimalism
| Length      = 40:42
| Language    = Piano DRG DRG (Italy) Caroline (CD)
| Director    = David Cunningham
| Compiler    =
| Chronology  = Michael Nyman  1981
| This album  = The Draughtsmans Contract  1982
| Next album  = The Cold Room (1984)
| Misc        =
}}
{{Album ratings
| rev1      = Allmusic
| rev1Score =   
| rev2      =
| rev2Score =
}} grounds by Henry Purcell overlain by new melodies.  The original plan was to use one ground for every two of the twelve drawings but Nyman states in the liner notes that this was unworkable.  Ironically, the ground for one of the most popular pieces, "An Eye for Optical Theory", is considered to be probably composed by William Croft, a contemporary of Purcell.   The goal was to create a generalized memory of Purcell, rather than specific memories, so a piece as recognized as "Didos Lament" was not considered an acceptable source of a ground.  Purcell is credited as a "music consultant".
 woodwind and The Divine Comedy. "Somehow they just manage to… rock. With a vengeance." 

The following ground sources are taken from the chart in Pwyll ap Siôns The Music of Michael Nyman:  Text, Context and Intertext, reordered to match their sequence on the album. 

===Track listing===
#  , Act III, Scene 2, Prelude (as Cupid descends))
# The Disposition of the Linen 4:47 ("She Loves and She Confesses Too" (Secular Song, Z.413))
# A Watery Death 3:31 ("Pavan in B flat," Z. 750; "Chaconne" from Suite No. 2 in G Minor)
# The Garden Is Becoming a Robe Room 6:05 ("Here the deities approve" from Welcome to all the Pleasures (Ode); E minor ground in Henry Playfords collection, Musicks Hand-Maid (Second Part))
# Queen of the Night 6:09 ("So when the glittring Queen of the Night" from The Yorkshire Feast Song)
# An Eye for Optical Theory 5:09 (Ground in C minor (D221)  )
# Bravura in the Face of Grief 12:16 ("The Plaint" from The Fairy-Queen, Act V)

The first music heard in the film is, in fact, a bit of Purcells song, "Queen of the Night".  "The Disposition of the Linen", in its Nyman formulation, is a waltz, a form that postdates Purcell by  

The album was issued on compact disc in 1989 by Virgin Records, marketed in the United States by Caroline Records under their Blue Plate imprint (trade name)|imprint.  Initially this was indicated with a sticker; it was later incorporated into the back cover design in a much smaller size.

The entire album has been rerecorded by the current lineup of the Michael Nyman Band.  See  .

== Art references ==
The visual references for the film are paintings by   and other French and Italian artists". 

== Restoration == BFI restored the film digitally and this restoration was released on DVD. Umbrella Entertainment released the digitally restored film on DVD in Australia, with special features including an introduction and commentary by Peter Greenaway, an interview With Composer Michael Nyman, behind the scenes footage and on set interviews, deleted scenes, trailers and a featurette on the films digital restoration.   

==Cast== Anthony Higgins – Neville Hugh Fraser – Talmann
* Anne-Louise Lambert – Mrs Talmann Dave Hill – Herbert
* Janet Suzman – Virginia Herbert
* David Meyer – Poulenc brother Tony Meyer – Poulenc brother
* Nicholas Amer – Parkes
* Suzan Crowley – Mrs Pierpont
* Lynda La Plante – Mrs Clement
* Michael Feast – The Statue
* Neil Cunningham – Thomas Noyes
* David Gant – Seymour
* Alastair Cumming – Philip
* Steve Ubels – Hoyten

== References ==
 

==External links==

===Sites===
*  
*  
*  
*  
*  
**   –  
 

===Reviews===
*  , film reference, Sylvia Paskin – detailed bibliography
*  , Shooting Down Pictures – links and excerpts to many reviews
*  , James Mackenzie, January 2001
*  , not coming to a theater near you, David Carter, 12 January 2010
*  , DVD Beaver – comparison of DVD releases

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 