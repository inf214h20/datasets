Do Dooni Chaar
 
 
{{Infobox film
| name           = Do Dooni Chaar
| image          = Dodoonichaar.jpg
| caption        = Theatrical release poster
| director       = Habib Faisal
| producer       = Arindam Chaudhuri
| story          = Habib Faisal
| screenplay     = Habib Faisal Rahil Qazi
| starring       = Rishi Kapoor Neetu Singh
| music          = Meet Bros Anjan Ankit
| cinematography = Anshuman Mahaley
| editing        = Aarti Bajaj
| lyrics         = Manoj Muntashir Disney World Cinema Planman Motion Pictures Walt Disney Pictures Walt Disney Studios Motion Pictures
| released       =  
| runtime        = 110 minutes
| country        = India
| language       = Hindi
| budget         = Rs.20&nbsp;million
| gross          = Rs.45&nbsp;million
}} Disney World Cinema.      

==Plot==
 
Life can be tough for a man (Rishi Kapoor) who teaches at a school for a living, lives in a DDA flat in Lajpat Nagar, Delhi and is coping with the double digit inflation rates and single digit increments in his salary. Add to that, A teenage daughter, a fast-track son and a wife (Neetu Kapoor) who loves the good life. The life of the Duggals is passing by in simply taking care of the basics. Until one day, they decide to dream. Their ticket to dreaming comes in the form of a wedding invitation. An innocent little wedding invitation. What follows is a journey of chaos, realizations, calculations, confrontations and bonding. This crazy Duggals will fight it out at home (and the neighborhood) for what they think is a ‘good buy’ for the family! This family bonds in the most unexpected and hilarious situations. And just when they think they are sorted out, comes one big, lurking temptation. Much like the apple in the Garden of Eden. And then, begins another journey that will drive them, and you, pretty much nuts.
===Theme===
The film deals with the issue of underpaid teachers, and their issue with their self-worth in the face of growing inflation and demands of their family. The middle-class school teacher who works overtime to support his income and to send his children to good schools, ends up in a moral dilemma, when it comes to fulfilling the needs of his ever unsatisfied teenage children. As per producer, Arindam Chaudhuri, "The common question the teacher asks when he looks at his student, who is just 25 and owns a car, is why at 55 he is still riding a scooter?"  In the end, it is not just the teacher who redeems himself by not succumbing to taking bribes, but also his children who begin to see him in the true light and importance of being a teacher and an honest citizen.  

==Cast==

* Rishi Kapoor as Santosh Duggal
* Neetu Singh as Kusum Duggal
* Archit Krishna as Sandeep Sandy Duggal
* Aditi Vasudev as Payal Duggal
* Akhilendra Mishra as Farooqui
* Supriya Shukla as Urmi (Fuppu)
* Natasha Rastogi as Salma Farooqui

==Production==

===Development===
Even at the script development stage, Rishi Kapoor had been in the mind of the makers, for the female lead opposite Rishi Kapoor, initially Juhi Chawla was approached, she however refused the role.      Although Neetu-Rishi pair had made a brief appearance in the Saif Ali Khan and Deepika Padukone starrer 2009 film Love Aaj Kal, Do Dooni Chaar was their first film as a lead pair, after a gap of 30 years as Neetu Singh had retired from film after her marriage to Rishi Kapoor in 1979.  Later in an interview Neetu, revealed she had no intentions of signing on the film and agreed to listen to the script at the insistence of her husband, who has already been signed on. Upon hearing the script from the director Habib, she started imaging herself as "Kusum Duggal" and immediately agreed to the part. 

===Filming===
Film was shot on locations across Delhi,  in places like Kirori Mal College, Vinobapuri, Shalimar Bagh (Delhi), Khan Market, C.R. Park and Noida. The wedding sequence in the film set in Meerut, was shot in a Chhattarpur farmhouse and later in Nizamuddin area, in early 2009. 

==Release==
  at the premiere of the film, in Mumbai]]
In August 2010, the film became the first live action Hindi feature film to be distributed by Walt Disney India.  The movie premiered at a suburban theater in Mumbai on 6 October, attended by the cast and Bollywood stars, followed by a nationwide release on 8 October 2010.  The film had its North American premiere as the opening night film of the 2011 New York Indian Film Festival on 4 May 2011.   

===Home media===
The film will be released on Disney DVD, Movie Download, and On Demand on 26 July 2011.              The release will be produced in DVD widescreen and include a Hindi language track plus English subtitles.

==Promotion==
Lead actors, Rishi Kapoor and Neetu Kapoor appeared on several television shows, to promote the film, including, Taarak Mehta Ka Ooltah Chashmah.

==Reception==

===Critical reception=== The Telegraph gave two thumbs up to the film calling it "an irresistible trip to the movies" that "makes you feel good about yourself".  Mayank Shekhar of Hindustan Times giving 4/5 stars said, "Full on paisa vasool! dig into the duggals"  while Sudhish Kamath of The Hindu called it “One of the most important films of our times. A celebration of the great Indian middle class. A landmark in indian film making!”    Rajeev Masand of CNN-IBN also liked the movie saying "Do Dooni... is simple but lovable."  Similarly veteran critic Taran Adarsh of Bollywood Hungama called it a “A little gem that should not be missed!”  Vinayak Chakravorty of Mail Today gave it three stars, writing: "Debutant Habib Faisal’s direction works for the way he underplays routine ironies of life."

===Box office===
The film collected   in its theatrical run and was declared a below average grosser.   

==Soundtracks==
# Do Dooni Chaar - Shankar Mahadevan, Vishal Dadlani
# Baaja Bajya - Sunidhi Chauhan, Meet Bros Anjan Ankit
# Ek Haath De - Meet Bros Anjan Ankit
# Maange Ki Ghodi - Rakesh Pandit, Krishna
# Do Dooni Chaar Jam - Shankar Mahadevan, Vishal Dadlani
# Do Dooni Chaar Club Mix - Shankar Mahadevan, Vishal Dadlani, Meet Bros Anjjan, Ankit

==Awards==

;58th National Film Awards
*Best Hindi Feature film - Do Dooni Chaar 
 2011 Star Screen Awards
* Star Screen Award for Best Art Direction - Mukund Gupta 
 2011 Filmfare Awards  Critics Award for Best Performance - Rishi Kapoor Best Dialogue - Habib Faisal Best Costume Design - Varsha and Shilpa Best Production Design - Mukund Gupta

==References==
 

== External links ==
*  
* 

 

 
 
 
 
 
 
 
 
 