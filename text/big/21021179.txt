Foreign Harbour
{{Infobox film
| name           = Främmande hamn
| image          = 
| image_size     = 
| caption        = 
| director       = Hampe Faustman
| producer       = 
| writer         = Herbert Grevenius
| narrator       = 
| starring       = Adolf Jahr
| music          = 
| cinematography = Carl-Erik Edlund
| editing        = Lennart Wallén
| distributor    = 
| released       = 1948
| runtime        = 
| country        = Sweden
| language       = Swedish
| budget         = 
| preceded_by    = 
| followed_by    = 
}}

Foreign Harbour ( ) is a 1948 Swedish drama film directed by Hampe Faustman. It was entered into the 1949 Cannes Film Festival.     

==Cast==
* Adolf Jahr - Captain Greger
* George Fant - Håkan Eriksson
* Illona Wieselmann - Mimi
* Fritiof Billquist - First Mate
* Åke Fridell - Steward
* Stig Järrel - Man wearing fur
* Gita Gordeladze
* Gösta Holmström - Second Mate
* Carl Ström - Engine-man

==References==
 

==External links==
* 

 
 
 
 
 
 
 