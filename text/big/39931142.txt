City Kids 1989
 
 
{{Infobox film
| name           = City Kids 1989
| image          = CityKids1989.jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = DVD cover
| film name      = {{Film name
| traditional    = 人海孤鴻
| simplified     = 人海孤鸿
| pinyin         = Rén Hǎi Gū Hóng
| jyutping       = Jan4 Hoi2 Gu1 Hung4 }}
| director       = Poon Man Kit
| producer       = Clarence Yip
| writer         = 
| screenplay     = Jubic Chui James Yuen Clarence Yip
| story          = 
| based on       = 
| narrator       = 
| starring       = Andy Lau Max Mok Wong Chung May Lo Paw Hee-ching
| music          = Richard Lo
| cinematography = Mark Lee
| editing        = Robert Choi
| studio         = Movie Impact
| distributor    = Movie Impact
| released       =  
| runtime        = 94 minutes Hong Kong
| language       = Cantonese
| budget         = 
| gross          = HK$10,627,142
}}
 1989 Cinema Hong Kong action drama The Orphan which stars Bruce Lee.

==Plot==
Sas and Chow Cho Sam are sworn brothers who grew up in a triad gang and lacked family warmth and proper education. Sam falls in love with May and they have shotgun marriage. In order to pay for their marriage, Sas robs where Sam injures three policemen and the two are imprisoned for six years. After they were released, May is already somebodys fiance but Sam is still in love with her but since punks and love have no fate, they can only be friends. Sas and Sam borrow money from a loan shark for their private business, however, their business fails and debt collector Fu forces them to kidnap to pay off their debts. They are ordered to kidnap tutoring student Ka Po, daughter of a wealthy man. Sas was dissatisfied with Fu for deciding to murder the girl after collecting ransom and personally hands Kao Po back to May which angers Fu and hires people to chop Sas. After Sas dies from his injuries, the sadden and angered Sam seeks vengeance on Fu. After some fighting when Sam was about to kill Fu, his long lost father, Inspector Chow Wing, persuades him to reform and not to repeat the same mistakes. Wing hands Fu to the law and helps Sam turn over a new leaf.

==Cast==
*Andy Lau as Sas / Puppy
*Max Mok as Chow Cho Sam
*Wong Chung as Inspector Chow Wing
*May Lo as May
*Paw Hee-ching as Chow Man Sau
*Newton Lai as Brother Fu
*Shing Fui-On as Big Skin Chuen
*Blackie Ko as Killer
*Stephen Chang as Yellow
*Chun Kwai Po as Fus thug
*Ho Kit
*Tam Siu Ying
*Hon San as Big Brother
*Fong Ka Wai
*Mok Hon Kei
*Chan King Chuen as Mays dad
*Ngai Ming Yiu
*Thomas Sin as Yiu
*Kan Sek Ming as Mr. Kan
*Shing Fuk On as Mad Dog
*John Cheung as Police Officer
*Fei Pak as Poilceman
*Lui Siu Ming as prisoner
*Fung Kam Hung as prisoner

==Box office==
The film grossed HK$10,627,142 at the Hong Kong box office during its theatrical run 5 to 24 August 1989 in Hong Kong.

==See also==
*Andy Lau filmography

==External links==
* 
*  at Hong Kong Cinemagic
* 

 
 
 
 
 
 
 
 
 