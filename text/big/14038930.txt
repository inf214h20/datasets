The American Gangster
 
{{Infobox Film
| name           = The American Gangster
| image          = The American Gangster VHS cover.jpg
| caption        = VHS cover
| director       = Ben Burtt
| producer       = Ray Herbeck Jr.
| writer         = Ray Herbeck Jr.
| narrator       = Dennis Farina
| starring       = 
| music          = Marco DAmbrosio
| cinematography = 
| editing        = 
| distributor    = Sony Pictures Home Entertainment
| released       =  
| runtime        = 45 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
The American Gangster is a 1992 American crime documentary film directed by Ben Burtt and written and produced by Ray Herbeck Jr.  The documentary is narrated by Dennis Farina and explores the lives of Americas gangsters like Pretty Boy Floyd, John Dillinger, Al Capone, and Bugsy Siegel.  It was directly released on VHS in 1992 and later released as part of a DVD box set in 2006.

==Background==
The American Gangster is a documentary that chronicles the formation of the first generation of American gangsters.  The documentary explores the illegal businesses involving gambling, prostitution, and defiance of prohibition of alcohol that empowered the gangsters.   The documentary shows actual film footage and photographs of gangsters like Pretty Boy Floyd, John Dillinger, Al Capone, and Bugsy Siegel. 

==Release== Donnie Brasco (1997), and Snatch (film)|Snatch (2000). 

==References==
 

==External links==
* 
*  
 

 
 
 
 
 
 
 
 
 


 