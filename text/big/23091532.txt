Sex and the City 2
{{Infobox film
| name           = Sex and the City 2
| image          = Sex and the City 2 poster.jpg
| image_size     = 215px
| alt            = 
| caption        = Theatrical release poster
| director       = Michael Patrick King
| producer       = Michael Patrick King Sarah Jessica Parker Darren Star John Melfi
| screenplay     = Michael Patrick King
| story          = Candace Bushnell   Darren Star
| starring       = Sarah Jessica Parker Kim Cattrall Kristin Davis Cynthia Nixon
| music          = Aaron Zigman
| cinematography = John Thomas
| editing        = Michael Berenbaum
| studio         = New Line Cinema HBO Films Village Roadshow Pictures Warner Bros. Pictures
| released       =  
| runtime        = 146 minutes
| country        = United States
| language       = English
| budget         = $95 million    
| gross          = $305,153,249 
}} Sex and TV series of the same name.
 Broadway actors, Norm Lewis, Kelli OHara, and Ryan Silverman. It received negative reviews from critics, but was a commercial success.

==Plot==
  Miranda meeting flashback to how Carrie arrived in New York City in 1986, then met Charlotte one year later, Miranda in 1989. She also met Samantha, when she was bartender at a bar but she doesnt tell a year.
 and her team of doctors" to keep her menopause at bay.  The four of them attend Anthony and Stanfords wedding, where Carrie serves as "best man." Miranda quits her job after the new managing partner disrespects her once too often. Charlottes two children are a handful and shes worried that Harry is attracted to their buxom Irish nanny, Erin. Carries marriage to Mr. Big (Sex and the City)|Mr. Big has settled down, though they differ on how to spend their spare time. For their anniversary, Carrie gives Mr. Big a vintage Rolex watch engraved with a romantic message, while he, much to her dismay, shows her a new TV in their bedroom as his gift, which Big says they can use to watch old movies together, something they did at the hotel at Anthony and Stanfords wedding and seemed to enjoy. Carrie, however, is disappointed, as she had hoped for jewelry as a gift.
 PR campaign for his business. He offers to fly her and her friends on an all-expenses-paid luxury vacation to Abu Dhabi. The girls happily accept, although Carrie is worried about the separation from Big and Charlotte is worried about leaving her husband alone with the nanny. Only Miranda, unfettered by a job for the first time in her life, is enthusiastic. Upon entering Abu Dhabi, Samanthas hormone-enhancing drugs are confiscated under UAE law. This renders her devoid of estrogen; her famous libido goes dead. Charlotte tries to call Harry every few minutes; Miranda revels in the luxury surrounding her, while Carrie befriends her manservant, Gaurau, who is an underpaid temporary worker from India.

Carrie runs into her former lover, Sex and the City characters#Aidan Shaw|Aidan. He proposes dinner à deux at his hotel and she decides to meet Aidan for dinner. The dinner is very enjoyable, with the two discussing old times. Aidan remarks on the ways Carrie is "not like other women". In a moment of remembered passion, they kiss. Carrie runs away in panic and returns to the hotel. Back at the hotel, Miranda and Charlotte have drinks together and discuss the difficulties of motherhood. Carrie arrives, tells her friends about the kiss, and asks them whether she should tell Big, as they have no secrets between them. Miranda reflects on the events of the previous film, when her husband, Steve Brady|Steve, told her about his affair. Samantha counsels Carrie to wait before deciding anything. Carrie opts to call Big to tell him. Big is silent upon hearing the news, and after saying a few words, hangs up.
 Western attitudes black robes.

When Carrie returns home, she finds the bedroom television removed and Big gone. She passes an anxious day, at the end of which he returns. Big tells her that although he was "pretty torn up", he realizes that what she needs is something to remind her at all times that she is married. He hands her a jewelry box, which reveals an engagement ring set with a black diamond. When Carrie asks him why a black diamond, he says, "Because youre not like anyone else", echoing Aidans earlier comment.

The problems the women faced at the beginning of the film are resolved. Big and Carrie combine their interests; Charlottes nanny, Erin, turns out to be a lesbian and is no threat to her marriage; Miranda finds a new job where she is appreciated, and Samantha stays the same, even meeting up with the Danish architect she met in Abu Dhabi for sex on the beach, this time a beach on The Hamptons.

==Cast==
* Sarah Jessica Parker as Carrie Bradshaw Samantha Jones
* Kristin Davis as Charlotte York Goldenblatt
* Cynthia Nixon as Miranda Hobbes John James Preston / Mr. Big John Corbett Aidan Shaw
* David Eigenberg as Steve Brady Harry Goldenblatt Jason Lewis Jerry "Smith" Jerrod Stanford Blatch Anthony Marentino Magda
* Alice Eve as Erin (Charlottes Irish nanny)
* Noah Mills as Nicky Marentino
* Megan Boone as Allie
* Max Ryan as Dr. Rikard Spirt
* Raza Jaffrey as Gaurau
* Viola Harris as Gloria Blatch
* Omid Djalili as Mr. Safir, Hotel Manager
* Dhaffer LAbidine as Mahmud
* Art Malik as Sheikh Khaled
* Penélope Cruz as Carmen García Carrión
* Liza Minnelli as herself
* Miley Cyrus as herself
* Tuesday Knight as herself
* Tim Gunn as himself

==Production==
  at the movie set.]]

===Development===
After months of speculation, the cast confirmed in February 2009 that a sequel was in the works. Filming began in August 2009.

The sequel is noticeably different from its predecessor, and includes more exotic locales than the original. King credits this to the experience he had promoting the original film in such locales. He was also inspired by the recession to write something bigger more akin to the extravagant adventures and escapist comedies of the 1930s.    The location of Abu Dhabi was chosen because of its high fashion culture (although the authorities later revoked filming clearance) and also that it was a location relatively free from the recession. 
 John Corbett as Aiden Shaw, David Eigenberg played Steve Brady once more, Willie Garson returned as Stanford Blatch, and Mario Cantone again played Anthony Marentino, making the original cast almost complete.  In addition, Michael Patrick King wrote and directed again, and Patricia Field once again took charge of the costumes and wardrobe.  Hats were once again created by Prudence Millinery for Vivienne Westwood.

Entertainment Weekly confirmed that the budget for the film was US$95 million,  exactly $30 million greater than the budget for the first film. Sarah Jessica Parker was paid $15 million for her dual role as a producer and starring as Carrie Bradshaw. 

===Filming===
Filming in New York was postponed to the end of July as Emirati authorities refused clearance for filming in the United Arab Emirates|emirate. As a result, the Abu Dhabi segment of the film was filmed in Morocco.   All four leading ladies and other cast and crew were photographed  filming scenes in Morocco in November 2009, where they had originally planned to shoot for 13 days, which had to be extended to almost six weeks.  The Some of the scenes were filmed at Amanjena, outside of Marrakesh.   

The sequel officially began filming on September 1, 2009 and continued until the end of the year. Photos of all four leading women filming scenes around New York together and separately have emerged, featuring present-day scenes as well as a range of looks believed to be flashbacks from the earlier years of Carrie, Samantha, Miranda, and Charlottes long-standing friendships. Images of Samantha in a wedding dress have also been released.  Additionally, scenes featuring prominent characters such as Mr. Big, Magda, Smith, Steve, and the children of Miranda and Charlotte have been filmed and photographed.

===Casting===
In September 2009, American singer/actress Liza Minnelli confirmed to several media outlets that she appeared in a cameo role. Singer/actress Bette Midler had been photographed on set, but does not appear in the film. Penélope Cruz appears briefly as Carmen, a banker.  Miley Cyrus appeared in one scene where she appears at the premiere of Smith Jerrods new film, wearing the same dress as Samantha. On October 17, Oceanup.com posted several pictures of Miley filming the scene. 
 John Corbett was seen on location in Morocco, confirming his speculated involvement in the film as Sex and the City characters#Aidan Shaw|Aidan. 

==Release== teaser poster was released online, featuring Carrie in a white dress and gold sunglasses which reflect a Moroccan backdrop, and the tagline "Carrie On", a similar pun of the lead characters name as "Get Carried Away" from the first film.  The same image and tagline was used for the launch of the official Sex and the City 2 website, also launched in December 2009.

The teaser trailer premiered online on December 22, 2009.  In March 2010, new promotional stills were released, predominantly featuring scenes from the Moroccan portion of the film.  Also in March, Sarah Jessica Parker, Kristin Davis and Cynthia Nixon attended ShoWest 2010 in Las Vegas to premiere the full length trailer and discuss the film (Kim Cattrall was in London performing on stage in the West End, and joined the rest of the cast for promotion when her stage run ended on May 3).
 full theatrical trailer premiered on Entertainment Tonight and online on April 8, 2010,  featuring current New York City-themed hit "Empire State of Mind" by Jay-Z and Alicia Keys as well as Australian R&B/Pop singer, Ricki-Lee Coulters platinum selling hit, "Cant Touch It".

A full scale promotional tour with all key cast members – including television, press conference and print – commenced in early May 2010, and continued throughout the films release, encompassing many different countries and cities. The New York City premiere of the film was held on 24 May 2010.

==Reception==

===Critical response===
 
The film has received largely negative reviews; review aggregate   gave the film a score of 27/100 based on a normalized average of 39 reviews indicating generally unfavorable reviews.   

The film was also criticized for its portrayal of the Middle East. Stephen Farber of The Hollywood Reporter called it "blatantly Islamophobia|anti-Muslim"    and Hadley Freeman of the UK broadsheet The Guardian described the trailers as "borderline racist".   

Andrew OHagan of the London Evening Standard described the movie as ugly on the inside, and ended by saying "This could be the most stupid, the most racist, the most polluting and women-hating film of the year".   

Roger Ebert gave the film one star out of four; he wrote, "I am obliged to report that this film will no doubt be deliriously enjoyed by its fans, for the reasons described above. Male couch potatoes dragged to the film against their will may find some consolation. Reader, I must confess that while attending the sneak preview with its overwhelmingly female audience, I was gob-smacked by the delightful cleavage on display." 

Lindy West wrote a noted   review of the film, saying that "SATC2 takes everything that I hold dear as a woman and as a human—working hard, contributing to society, not being an entitled cunt like its my job—and rapes it to death with a stiletto that costs more than my car. It is 146 minutes long, which means that I entered the theater in the bloom of youth and emerged with a family of field mice living in my long, white mustache. This is an entirely inappropriate length for what is essentially a home video of gay men playing with giant Barbie dolls." 

Mitu Sengupta, a Toronto-based   or crass consumerism|materialism, but how easily this seemingly benign bubble-gum flick ends up fighting a very macho war of global one-upmanship on the bodies of women and gay men." 

British film critic Mark Kermode declared it the worst film of 2010, saying he couldnt think of anything civilization had made that was "more poisonous, more repugnant, more repulsive, more retrograde, more depressing than Sex and the City 2," and jokingly said its unintentionally "ghastly" portrayal of the "disgustingly wealthy" almost made him want to become a communist.     Time (magazine)|Time named it one of the top 10 worst movies based on TV shows. 
 Worst Couple/Screen Ensemble Razzie, which was awarded to the entire cast. According to Razzies founder John J. B. Wilson, "  said that he had never won an award of any kind and if this was what he won, he would accept it." Eigenberg then collaborated with Wilson to make a humorous acceptance video which was posted on the official YouTube channel of the Golden Raspberry Awards. 

===Box office===
The film opened in 3,445 theaters on May 27, 2010, setting a record for one of the widest release for a R rated romantic comedy film. Playing in 2,000 theaters, the film grossed $3 million from its midnight premiere.  On its opening day, the film topped the box office grossing $14.2 million,  for a projected $60 million for a 4-day opening weekend, plus $75 million for a 5-day Memorial day weekend.  But it debuted in second place behind Shrek Forever After with $32.1 million, and its total to $45.3 million, for its 4-day opening weekend, plus $53 million on its 5-day opening weekend (Memorial Day). The film added $27 million overseas giving it a worldwide total to $73 million. The first film debuted in first place at the box office taking in $57 million in 2008 in the US.

However, overseas, the sequel was a bigger success topping the charts in Germany for 5 weeks, Britain for 3 weeks, Australia for 2 weeks and exceeding the original in Japan and Greece. In fact, Sex and the City 2 sold more tickets than the first part in many of the foreign markets. As of August 19, 2010, the films total domestic gross stands at $95,347,692 and as of August 1, 2010, overseas, it has grossed $193,000,000 — giving it a worldwide total of $288,347,692, which is 30% lower than part 1. However, it was 2010s highest-grossing romantic comedy.   

The release of the home video saw the film gross a further $16,805,557 in its first week, with a total gross of $305,153,249.

==Home release==
Sex and the City 2 was released on DVD, Blu-ray Disc|Blu-ray, and iTunes on October 26, 2010, in the US  and November 29, 2010, in the UK.

;DVD charts
{|class="wikitable"
|-
!Chart (2010)
!Peak position
!Sales Units
!DVD Gross
|- US DVD Sales Chart  1
|989,144
|$18,422,358
|- UK DVD Chart  1
|Unknown Unknown
|}

==Soundtrack==
{{Infobox album |  
| Name        = Sex and the City 2
| Type        = Soundtrack
| Artist      = Various Artists
| Cover       = 
| Released    = May 25, 2010
| Recorded    =
| Genre       = Soundtrack
| Length      =
| Label       = Sony Music Australia
| Producer    =
| Chronology  = Sex and the City soundtrack
| Last album  =   (2008)
| This album  = Sex and the City 2 (2010)
| Next album  =
}}
Sex and the City 2 original motion picture soundtrack was released on May 25, 2010.

# "Rapture (song)|Rapture" – Alicia Keys (Blondie (band)|Blondie) Dido
# Cee Lo Window Seat" – Erykah Badu
# "Kidda" – Natacha Atlas
# "Euphrates Dream" – Michael McGregor
# "Single Ladies (Put a Ring on It)" – Liza Minnelli (Beyoncé Knowles|Beyoncé)
# "Cant Touch It" – Ricki-Lee Coulter|Ricki-Lee
# "Empire State of Mind (Part II) Broken Down" – Alicia Keys
# "Love Is Your Color" – Jennifer Hudson & Leona Lewis
# "I Am Woman" – Sarah Jessica Parker, Kim Cattrall, Kristin Davis, Cynthia Nixon
# "If Ever I Would Leave You" – Sex and the City Mens Choir
# "Fiddler on the Roof|Sunrise, Sunset" – Sex and the City Mens Choir
# "Till There Was You" – Sex and the City Mens Choir
# "Bewitched, Bothered and Bewildered" – Shayna Steele, Jordan Ballard, Kamilah Marshall
# "Evry Time We Say Goodbye" – Liza Minnelli with Billy Stritch (Cole Porter) True Colors" – Cyndi Lauper
# "Divas and Dunes" – Aaron Zigman

;Not included on the soundtrack. Annie appears in the background of the after party scene, but is not included on the soundtrack.
* "Empire State of Mind" by Jay-Z featuring Alicia Keys appears in the background of the Smiths movie premiere scene, and official trailer, but is not included on the soundtrack.

The score was recorded and mixed by Dennis S. Sands and Steve Kempster and performed by a large ensemble of the Hollywood Studio Symphony conducted by Stephen Coleman who orchestrated Zigmans score. Patrick Kirst also orchestrated.

;Charts
{|class="wikitable"
|-
!Chart (2010)
!Peak position
|- UK Albums UK Compilation Chart  7
|- Irish Albums Irish Top 30 Compilation Chart  2
|- ARIA Charts|Australian Albums Chart  7
|- Billboard 200|US Billboard 200  13
|- Federation of Italian Top 30 Compilation Chart  15
|}

==Potential sequel==

Sarah Jessica Parker has expressed interest in a third film to end the series, saying "A part of me thinks there is one last chapter to tell". 

==Awards and nominations==
{| class="wikitable"
|-
! Year !! Award !! Category !! Work !! Result
|-
| 2010
| National Movie Awards
| Anticipated Movie of the Summer
| rowspan="2"| Sex and the City 2
| 
|- 2011
| Peoples Choice Awards
| Favorite Comedy Movie
| 
|- Golden Raspberry Awards Worst Picture
|
|   - lost to The Last Airbender
|- Worst Actress
| Kim Cattrall, Kristin Davis, Cynthia Nixon, and Sarah Jessica Parker
|  
|- Worst Supporting Actress
| Liza Minnelli The Killer Inside Me, Little Fockers, Machete (film)|Machete and Valentines Day (2010 film)|Valentines Day
|- Worst Director
| Michael Patrick King
|   - lost to M. Night Shyamalan for The Last Airbender
|- Worst Screenplay
| Michael Patrick King
|   - lost to The Last Airbender
|- Worst Prequel, Remake, Rip-off or Sequel
|
|  
|-
| Worst Screen Ensemble
|
|  
|}

==References==
 

==External links==
*  
*  
*  
*  
*  
*  
*  
*   at The Numbers

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 