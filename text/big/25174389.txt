Kush (film)
 
 
 independent action-thriller film directed by York Shackleton, starring Nick Annunziata (HBO’s “The Sopranos”), William Atherton, Michael Bellisario, Matthew Carey, James DeBello, Lin Shaye, and James Duval. Kush is distributed by Maverick Entertainment Group.

Kush was nominated for Best Drama at the 2008 High Times Stony Awards. 

==Plot==
A group of young drug dealers lose $30,000 to an addict and plot revenge by scheming to kidnap the addicts teenage brother.
  The plot drew comparisons to the 2006 film Alpha Dog, which based on the true story of the 2000 kidnapping and murder of Nicholas Markowitz by Jesse James Hollywood. Kush takes place in a similar location (Calabasas, California), but focuses on the drug dealer who makes a terrible mistake; the kidnapped teen is more a background character. In Kush, the sympathetic characters are Dusty, the young but polished leader of the crew who avoids trouble; and Christian, the drug dealers son who is trying to grow up too fast.

==Cast==
* Bret Roberts as Dusty
* Nicole Marie Lenz as Blaise
* Mike Erwin as Christian
* Marne Patterson as Taylor
* Matthew Carey as Ryder
* James DeBello as Animal
* James Duval as Cyrus
* Lin Shaye as Susan
* Michael Bellisario as Jared
* William Atherton as King
* Nick Annunziata as Johnny 

==References==
 

==External links==
*  
*   on Mavericks website
*  

 
 
 
 


 