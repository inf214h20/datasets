Down Home (film)
{{infobox film
| name           = Down Home
| image          = Downhome-1920-newspaperad.jpg
| imagesize      =
| caption        = Newspaper advertisement.
| director       = Irvin Willat
| producer       = Irvin Willat
| writer         = Irvin Willat (scenario)
| based on       =  
| starring       = Leatrice Joy
| music          = 
| cinematography = Frank Blount Andrew Webber
| editing        =
| distributor    = W. W. Hodkinson
| released       =  
| runtime        = 70 mins.
| country        = United States
| language       = Silent (English intertitles)
}}
 silent drama film written, directed, and produced by Irvin Willat and starring Leatrice Joy and James Barrows. The film is based on the novel Dabney Todd, by F. N. Westcott. It was distributed by the independent film distributor W. W. Hodkinson. 

An early surviving Leatrice Joy feature at the Library of Congress.  The film is available on DVD from Ioffer.com 

==Cast==
* Leatrice Joy - Nance Pelot
* James Barrows - Dabney Todd (*aka James O. Barrows) Edward Hearn - Chet Todd
* Aggie Herring - Mrs. Todd
* Edward Nolan - Martin Doover
* Robert Daly - Joe Pelot  Sidney A. Franklin - Cash Bailey 
* Bert Hadley - Reverence Mr. Blake
* Frank Braidwood - Larry Shayne
* Robert Chandler - Deacon Howe 
* Nelson McDowell - Lige Conklin
* Florence Gilbert - Clerk
* J. P. Lockney - Barney Shayne, Larrys Father
* William Sloane - Townsman
* Helen Gilmore - Townswoman

==References==
 

==External links==
 
*  
*  

 
 
 
 
 
 
 
 
 
 


 