Little Chenier
{{Infobox film
| name           = Little Chenier: A Cajun Story
| image          = Littlechenier.jpg
| caption        = 
| director       = Bethany Ashton
| producer       = William Dore (executive producer), Philip S. Bligh (executive producer), Bruce Randolph Tizes (executive producer), Clifton Collins Jr., Jane Dennison, Jessica Schatz, Teddy Valenti (co-producers), Kira Bosak, Amy N. Johnson, Scott Trahan (associate producers), Bethany Ashton, Gavin Boyd, Jace Johnson, & Ron West (producers)
| writer         = Bethany Ashton & Jace Johnson Jeremy Davidson, Clifton Collins Jr., & Chris Mulkey
| music          = Michael Picton
| editing        = Brian Anton
| distributor    = Radio London Films
| released       = October 20, 2006 (Austin Film Festival)
| runtime        = 120 min
| country        = USA
| language       = English French
| budget         = $6,000,000 (estimated)
}}
 Jeremy Davidson, Clifton Collins Jr., & Chris Mulkey. The film completed production in 2006 but had difficulty finding a distributor despite glowing reviews from a number of film festivals.   It premiered at the Austin Film Festival on October 20, 2006 and was released January 18, 2008. Radio London Films released it on DVD on July 8, 2008.

==Plot==

Two brothers are victimized by a weak and jealous man in this drama shot on location in Louisiana. Beauxregard Beaux Dupuis (Jonathan Schaech) lives  in the swamps of Cajun country on a small stretch of land called Little Chenier with his younger mentally-handicapped brother, Pemon (Fred Koehler). Beaux supports them both by running a bait shop. Beaux is in love with Mary-Louise (Tamara Braun), who has left him to elope with the son of the sheriff, Carl. Neither Carl nor Beaux are fond of each other. Carl also enjoys tormenting Pemon. It is revealed that Mary-Louise left Beaux for Carl when Carl threatened to take her familys property away. When Carls father is killed in the line of duty, his son takes his place; it isnt long before Carl learns Mary-Louise has been having an affair with Beaux, and he uses his new authority to put Pemon behind bars on false change as a way of punishing Beaux.

==Main cast==
*Johnathon Schaech as Beauxregard Beaux Dupuis
*Frederick Koehler as Pemon Dupuis
*Tamara Braun as Marie-Louise LeBauve
*Jeremy Davidson as Carl LeBauve
*Clifton Collins Jr. as T-Boy Trahan
*Chris Mulkey as Sheriff Kline LeBauve

==Hurricane Rita== Lake Charles parishes used for filming. Virtually all of the sets and locations used in the film were destroyed, leaving many locals homeless. The film is believed to be the only known footage of the area. Bethany Ashton and her family set up a non-profit organization called   to rebuild the communities and help those in need, who were largely left out of the eventual help offered to victims of Katrina. The charity is unique in that Ashton and the fellow creators invite people to correspond directly with them to find out exactly how their contributions were used.

==Reviews==
*  
*  
*  

==References==
 

== External links ==
*  

 