Nachtrit
  thriller film about the tribulations of an Amsterdam taxi-driver, played by Frank Lammers. The context is a vicious taxi war that was waged between two competing taxi companies in the city around 2000. We follow Denniss daily life as he struggles to keep himself and his family afloat in an environment of treachery and ruthless savagery. The harder he fights, the deeper he gets into trouble, and the fewer options remain to him.

== Cast == 
* Frank Lammers as Dennis van der Horst 
* Fedja van Huêt as Marco van der Horst 
* Peggy Jane de Schepper as Elize van der Horst 
* Zita the Quay  as Maxje 
* Muhammad Chaara as Mahmoud 
* Henk Poort as Uncle Jan Bremer 
* Hans Kesting as Joris 
* Fred Schrijber as Ruud 
* Bob Schrijber as Cees 
* Jeroen Willems as Joop Schroeder 
* Jaap Spijkers as Grimbergen 
* Yorick van Wageningen as Taxi-driver 
* Theo Maassen as Passenger 
* Caro Lenssen as Passenger 

==Reception== Golden Calves—for best actor (won by Lammers), best supporting actor (won by van Huêt), best film, and best supporting actress. Dutch film journalists voted it the best Dutch film of 2006. In 2007, Lammers was nominated for a Rembrandt Award (best Dutch actor). 

==References==
 

==External links==
*  
*   at SBS Film

 
 
 

 
 