Bertoldo, Bertoldino e Cacasenno
{{Infobox film
 | name = Bertoldo, Bertoldino e Cacasenno
 | image =  Bertoldo, Bertoldino e Cacasenno.jpg
 | caption =
 | director = Mario Monicelli
 | writer = Leo Benvenuti, Piero De Bernardi, Suso Cecchi dAmico, Mario Monicelli (from the tales written by Giulio Cesare Croce)
 | starring = Ugo Tognazzi 
 | music =  Nicola Piovani
 | cinematography = Camillo Bazzoni
 | editing = Ruggero Mastroianni
 | producer = Luigi De Laurentiis  Aurelio De Laurentiis
 | distributor = Filmauro
 | starring = Ugo Tognazzi, Lello Arena, Maurizio Nichetti, Alberto Sordi, Annabella Schiavone, Carlo Bagno
 | released = 1984
 | runtime = 120 min
 | awards =
 | country = Italy
 | language =  Italian
 | budget =
 }} 1984 Cinema Italian comedy film directed by Mario Monicelli.   It was filmed in Rome, Cappadocia, Marano Lagunare and Exilles. 

== Plot summary ==
Vicenza, 800 AD. The crude peasant Bertoldo is invited to the court of King Alboin and manages to ingratiate himself with all the nobility of the city for his pranks and his jokes. Back in his small village from the silly wife Marcolfa and the brain-damaged son Bertoldino, the farmer Bertoldo discovers that he was robbed. The fault is of some Fra Cipolla from Frosolone, a swindler monk who sells fake relics impersonating sacred objects belonging to the saints. Among various adventures, intrigues at court because of the princess who does not want to marry a nobleman, Bertoldo between bickering between the Conqueror and various troubles because of a royal ring, Bertoldo and his entire family is admitted to the court of Alboin. But to no avail Bertoldo the life of the rich and soon dies of a broken heart, while Bertoldino that he married his wife generates a dwarf and foolish child, that he will call Cacasenno.

== Cast ==
*Ugo Tognazzi: Bertoldo 
*Maurizio Nichetti: Bertoldino 
*Alberto Sordi: fra Cipolla 
*Lello Arena: King Alboino  
*Pamela Denise Roberts: Queen Magonia

==References==
 

==External links==
* 
 

 
 
 
 
 
 
 


 
 