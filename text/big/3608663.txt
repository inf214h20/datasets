Black Rain (1989 Japanese film)
 
 
{{Infobox film
  | name           = Black Rain
  | image          = Black Rain 1989.jpg
  | caption        = DVD cover
  | director       = Shohei Imamura
  | producer       = Hisa Iino
  | writer         = Ibuse Masuji (story)  Toshiro Ishido
  | starring       = Yoshiko Tanaka Kazuo Kitamura
  | music          = Toru Takemitsu
  | cinematography = Takashi Kawamata
  | editing        = Hajime Okayasu Hayashibara Group Imamura Productions Tohokushinsha Film Co
  | released       =  
  | runtime        = 123 minutes
  | country        = Japan
  | language       = Japanese
  | budget         =
  }}
 Japanese film novel of atomic bombing of Hiroshima.

==Plot==
  jizo and suffers a form of posttraumatic stress disorder|post-traumatic stress disorder where he attacks passing motor vehicles as "tanks."

==Cast==
* Yoshiko Tanaka as Yasuko
* Kazuo Kitamura as Shigematsu Shizuma
* Etsuko Ichihara as Shigeko Shizuma
* Shoichi Ozawa as Shokichi
* Norihei Miki as Kotaro
* Keisuke Ishida as Yuichi
* Hisako Hara as Kin
* Masato Yamada as Tatsu
* Taiji Tonoyama

==Analysis==
  transience and the uncertainty of the time of ones death. 

Aside from its compassionate treatment of the hibakusha, who were often shunned by their fellow Japanese, this film is remarkable for its terrifying re-creation of the Hiroshima atomic bombing and its immediate aftermath. Its scenes of horribly burned survivors of the explosion, their flesh seared or flayed by the heat, struggling to escape from under collapsed buildings or find cooling water in which to immerse themselves, often dying in the streams once they find them, are unforgettable.

==Awards==
;Wins
*Japanese Academy Awards 1990: Best Actress, Best Cinematography, Best Director, Best Editing, Best Film, Best Lighting, Best Music Score, Best Screenplay, Best Supporting Actress Blue Ribbon Award 1990: Best Actress Cannes Film Festival 1989: Prize of the Ecumenical Jury - Special Mention, Technical Grand Prize   
*Flanders International Film Festival Ghent 1989: Georges Delerue Prize, Golden Spur
*Hochi Film Awards 1989: Best Actress
*Kinema Junpo Award 1990: Best Actress, Best Director, Best Film
*Mainichi Film Concours 1990: Best Actress, Best Art Direction, Best Film
*Sant Jordi Awards 1991: Best Foreign Film

;Nominations
*Japanese Academy Awards 1990: Best Art Direction, Best Sound
* 
*  
*Independent Spirit Awards 1991: Best Foreign Film

==References==
 

==Further reading==
*An academic comparative study of Black Rain (American film) and Black Rain (Japanese film), entitled "Nuclear Bomb Films in Japan and America: Two Black Rain Films" by Yoko Ima-Izumi included in Essays on British and American Literature and Culture: From Perspectives of Transpacific American Studies edited by Tatsushi Narita (Nagoya: Kougaku Shuppan, 2007).

==External links==
* 
* 

 
{{Navboxes title = Awards list =
 
 
 
}}

 
 
 
 
 
 
 
 
 
 
 
 
 