The Last Shepherd
{{Infobox film
| name           = The Last Shepherd
| image          = 
| caption        = 
| director       = Marco Bonfanti
| producer       = Anna Godano Marco Bonfanti Franco Bocca Gelsi
| writer         = Marco Bonfanti
| starring       = Renato Zucchelli Piero Lombardi Lucia Zucchelli
| music          = Danilo Caposeno
| cinematography = Michele DAttanasio
| editing        = Valentina Andreoli
| studio         = 
| distributor    = Cinecittà Luce
| released       =  
| runtime        = 76 minutes
| country        = Italy
| language       = Italian
| budget         = 
| gross          = 
}}
The Last Shepherd (original title: Lultimo pastore) is a 2012 biographical drama/documentary film written and directed by Marco Bonfanti. It premièred at the 2012 Tokyo International Film Festival  and in Italy at the 2013 Turin Film Festival. In 2013 it was shown at the Dubai International Film Festival, at the Slamdance Film Festival and several other film festivals. 

==Synopsis==
Renato Zucchelli is the last travelling shepherd left in a metropolis, and he has a dream: to lead his flock of sheep to the inaccessible city center to meet the children who have never seen him, showing them that dreams and freedom will always exist as long as there is still space to believe in a last shepherd...who conquered back his city together with only his sheep and the power of imagination.

==Production== Piazza del Duomo in Milan. The news of this story traveled through national and international press. 

==References==
 

==External links==
* 
* 
*  

 
 
 
 
 

 
 