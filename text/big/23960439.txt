Roman Holiday (1987 film)
{{Infobox Film
| name = Roman Holiday
| image = RomanHolidayVHS.jpg
| image_size =
| caption = VHS cover
| director = Noel Nosseck
| producer = Mel Efros
| writer = Dalton Trumbo (story) Ian McLellan Hunter (front for Dalton Trumbo) Jerry Ludwig (teleplay)
| narrator =
| starring = Catherine Oxenberg Tom Conti Ed Begley, Jr.
| music =
| cinematography = Romano Albani
| editing = NBC CBS Television Distribution
| released = December 28, 1987 (USA)
| runtime = 100 minutes
| country = United States English
| budget =
| gross =
| preceded_by =
| followed_by =
}} same name. The plot features Princess Elysa (Catherine Oxenberg), who is touring Rome, and decides to get out and about away from her normal life. She meets with an American reporter and his photographer, who show her the sights. The reporter is initially more interested in a story than the Princess, but begins to fall for Her Highness... 

==Cast==
* Catherine Oxenberg - Princess Elysa
* Tom Conti - Joe Bradley
* Ed Begley, Jr. - Leonard Lupo
* Paul Daneman - King
* Eileen Atkins - Countess
* Patrick Allen - General Francis Matthews - Ambassador
* Shane Rimmer - Hogan
* Christopher Muncke - Phil
* Tessa Hood - Secretary
* Andrew Bicknell - Elite Guard (Squad Leader) David Rolfe - Major Domo

==References==
 

==External links==
*  

 
 
 
 


 