Afonya
 

{{Infobox film name            = Afonya image           = Afonyaposter.jpg caption         = Film poster director        = Georgiy Daneliya producer        = Mosfilm writer          = Alexander Borodyanski cinematography  = Sergei Vronsky starring        = Leonid Kuravlyov Yevgeny Leonov Yevgeniya Simonova music           = Mieczysław Weinberg studio          = Mosfilm released        =   country         = Soviet Union runtime         = 92 min. language        = Russian
}} Soviet film Soviet box office leader of 1975 with a total of 62.2 million ticket sales. The film was shot on location in Yaroslavl.

==Plot summary== clients and is often in trouble with the local committee for his behaviour.

Afonya meets plasterer Kolya (Yevgeny Leonov) in a pub, gets drunk and comes home. When his girlfriend sees the state hes in, she leaves him. The next morning he cant even remember yesterdays drinking companion.

Things continue to go downhill for Afonya. When student interns from the vocational school are allocated to the plumbers, master ZHEK Vostryakovo (Valentine Talyzina) doesnt allocate any to him, fearing that he will not teach them well. Afonya begs for trainees and gets two. Having worked with him for one day and having seen his attitude and working methods, the trainees refuse to work with him anymore. The next day, Kolya arrives to live at Afonyas, having been thrown out of his house by his wife.

At a dance Afonya meets young nurse Katya Snegireva (Yevgeniya Simonova), who knows about him through her brother, who used to play in Afonyas volleyball team. Afonya doesnt pay her much attention, because hes more interested in older women and already has his eye on one at the dance. However, a romantic walk with the older woman after the dance is over before it begins – Afonya is challenged to a fight by a hooligan, who hed quarrelled with at the dance. The hooligans friends join in, and finish the unequal fight. Katya worries for Afonyas safety, and calls the militia - which will only bring Anfonya to the committees attention again.

At a regular work call Borshchov meets Helen (Nina Maslova) and falls in love at first sight. He starts finding any excuse to work in her flat - even fooling a tenant, astronomer (Gotlib Roninson), by swapping his new Finnish sink for an old one, so he can install the Finnish sink in Helens home as a gift. In his dreams Athanasius sees a family idyll with his wife Helen and their perfect children.

Katya Snegireva, head over heels for Afonya,  keeps engineering new meetings with him, and stops at nothing to attract his attention: "Athanasius!  someone called, I thought it was – you ... ". Afonya is completely oblivious to her feelings.

Meanwhile Afonyas run in with the militia catches up with him and, for persistent drunkenness, truancy and fighting Afonya is threatened with being sacked at a meeting of the local committee. In addition, if he doesnt restore the Finnish sink hes definitely going to be fired. Afonya takes a porcelain sink with flowers to Helen to swap with the Finnish sink, but meets Helen coming home with company. She makes her feelings quite clear: she has her own life among fashionable and wealthy men, and Afonya is just a plumber.

Afonya, depressed, goes to a restaurant with Fedulov and tries to escape into drunkenness, but it doesnt help. In his drunken state he goes to Katya Snegirevas home and proposes marriage, and wakes up next to her in the morning. Katya tells him shes due to move to Africa with work and wonders if she should cancel for Afonya.

Afonya then decides to go back to his village, to his aunt Frosya (Raisa Kurkina), who brought him up. In the village, he meets his childhood friend Fidget (Savely Kramarov) and, in a joyous moment sends the city a telegram resigning from his job and giving up his apartment. Only then does he learn that Aunt Frosya died two years ago.

Afonyas depression increases – he has lost everything and has nowhere to go. Neighbour Uncle Yegor (Nikolai Grinko) gives Afonya his Aunt Frosyas legacy – savings account in his name, the deeds to her house, and the letters shed sent to herself, pretending they were from Afonya to the neighbours (who all knew). The guilt of having ignored her only increases his gloom.

Athanasius goes to the post office and tries to call Katya Snegireva on her memorable phone number 50-50-2, or as he says himself, "rug-rug or two." The answer comes back - Katya has left. Finally frustrated, he goes to the airport. He does not care where hes going or what will happen to him. Things have gotten so bad that a local militia man has to be convinced Afonya is the man in his passport photograph, so grim has he gotten since it was taken. Finally, just when Athanasius is heading to the AN-2 aircraft, a familiar girlish voice calls out. Its Katya, suitcase in hand: "Athanasius!, someone called, I thought it was – you ... "

== Cast ==
* Leonid Kuravlev -Borshchov Afanasy N. (Afonya)
* Yevgeniya Simonova -Katya Snegireva
* Yevgeny Leonov -Kolya
* Savely Kramarov -Razor
* Nina Maslova -Elena (Elena, Helene), Beauty of the 139th flats
* Borislav Brondukov -Fedulov
* Igor Bogolyubov -palFedulov
* Valentine Talyzina -Vostryakovo
* Vladimir Basov -Vladimir Belikov
* Nikolai Parfenov -Fomin, Boris Petrovich
* Gotlib Roninson -astronomer ("Archimedes")
* Raisa Kurkina -AuntFrosya
* Nina Ruslanova -Tamara
* Nikolai Grabbe -the chief of housing offices
* Nikolai Grinko -Uncle Yegor
* Gennady Yalovich -the director in the theater
* Radner Muratov -Murat Rakhimov
* Tamara Sovchi -cashierdining
* Yusup Daniyal – cashier dining companion
* Mikhail Vaskov -the policeman at the airport
* Peter Lyubeshkin -Uncle Pavel Shevchenko
* Renee Hobua – not featured in the film, although there is in the credits. Alexei Vanin -Ivan Orlov, Elenas husband
* Capitolina Ilienko -a participant in the meeting of housing the office
* Alexander Potapov -employee of housing offices
* Michael Svetin -the driverVorontsov
* Tatiana Rasputin -a dance partner Afonyia
* Alexander Novikov -a bully with a beard

==External links==
* 
* 

 

 
 
 
 
 
 
 