The Romance of Betty Boop
{{Infobox Film
| name = The Romance of Betty Boop (1985)
| image = 
| caption = 
| director = Bill Melendez
| producer = 
| writer = Ron Friedman
| starring = Desirée Goyette Frank Buxton
| music = Harry Ruby Harry Warren
| distributor = King Features
| released =  
| runtime = 25 minutes
| country = United States
| language = English
}}
 animated television special featuring Betty Boop. 

==Plot summary==
The story is set in New York City in 1939, during the Great Depression. The film opens with a New York Montage (filmmaking)|montage, as a voiceover in mock period style introduces the story of "a romantic hardworking New Yorker, an independent girl who wants nothing more than to put her feet up and marry a handsome millionaire." Desirée Goyette sings "Just Give Em Your Boop-Oop-a-Doop" over the opening credits. Scraps of popular songs, especially "I Only Have Eyes for You", appear throughout the film.

Betty Boop is a girl who is adored by her neighborhood and courted by Freddie the Iceman (occupation)|iceman, but who dreams of moving in high society and achieving "personal greatness". Betty sells shoes by day and performs at Club Bubbles by night. Uncle Mischa Bubble, the Russian owner of the club, is threatened by Johnny Throat and his two henchmen because he cannot pay his debts. Seeing Betty perform on the stage, Throat decides to take over the club to force Betty to go out with him.

Betty sings "I Wanna Be Loved By You" to attract the attention of Waldo Van Lavish, a millionaire playboy. Betty goes on a date with Waldo, during which he says he wants Betty to meet his parents, so she believes he wants to marry her. Later, on her way to meet him, Betty is kidnapped by Throats henchmen and taken to his apartment. Uncle Mischa calls Freddie to rescue Betty, but Betty escapes on her own. She meets Waldos aged parents and discovers that Waldo merely wants her to be a maid in his parents house. At the end, Betty apologizes to Freddie for ignoring him and gives him a kiss. To Freddies dismay, however, she then gets a call from Hollywood to do a screen test.

==Cast==
*Desirée Goyette as Betty Boop
*Sean Allan as Freddie
*Frank Buxton
*Ron Friedman
*Sandy Kenyon
*Derek McGrath
*Marsha Meyers John Stephenson
*Robert Towers
*Marsha Meyers
*George Wendt

== Soundtrack ==
*"I Wanna Be Loved by You"
:Sung by Desirée Goyette
*"I Only Have Eyes for You"
:Sung by Desirée Goyette

==External links==
* 
*  at Bcdb

 
 
 
 
 
 
 
 
 
 
 


 