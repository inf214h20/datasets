Lost Lagoon (film)
{{Infobox film
| name           = Lost Lagoon
| image          = Lost Lagoon poster.jpg
| alt            = 
| caption        = Theatrical release poster John Rawlins
| producer       = John Rawlins
| screenplay     = Milton Subotsky John Rawlins Jeffrey Lynn
| starring       = Jeffrey Lynn Leila Barry Peter Donat Don Gibson Roger Clark Jane Hartley
| music          = Terry Brannon Hubert Smith
| cinematography = Harry W. Smith 
| editing        = David Rawlins 
| studio         = Bermuda Studio Productions
| distributor    = United Artists
| released       =  
| runtime        = 79 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 John Rawlins and written by Milton Subotsky, John Rawlins and Jeffrey Lynn. The film stars Jeffrey Lynn, Leila Barry, Peter Donat, Don Gibson, Roger Clark and Jane Hartley. The film was released on February 1, 1958, by United Artists.  

==Plot==
 

== Cast ==	
*Jeffrey Lynn as Charlie Walker
*Leila Barry as Elizabeth Moore 
*Peter Donat as David Burnham
*Don Gibson as Mr. Beakins
*Roger Clark as Millard Cauley
*Jane Hartley as Bernadine Walker
*Celeste Robinson as Colima
*Stanley Seymour as Native
*Isabelle Jones as Native
*Hubert Smith as Himself 

== References ==
 

== External links ==
*  

 

 
 
 
 
 
 
 
 