The Penguin Pool Murder
{{Infobox film
| name           = The Penguin Pool Murder
| image          = Penguin Pool Murder poster.jpg
| image_size     = 225px
| caption        = theatrical poster
| director       = George Archainbaud Ray Lissner (assistant)
| producer       = Kenneth Macgowan
| based on       =  
| screenplay     = Lowell Brentano (story) Willis Goldbeck (screenplay)
| starring       = Edna May Oliver
| music          = Max Steiner
| cinematography = Henry W. Gerrard
| editing        = Jack Kitchin
| distributor    = RKO Radio Pictures
| released       =  
| runtime        = 70 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}

The Penguin Pool Murder (1932) is a   

==Plot== Donald Cook) at the local aquarium and asks him for some money so she can leave her husband, stockbroker Gerald Parker. However, Mr. Parker receives an anonymous telephone call tipping him off to the rendezvous. When he confronts the pair, Seymour knocks him out with a punch. As there are no witnesses to the altercation, he hides the unconscious man in the room behind an exhibit.
 Robert Armstrong) catches Gwen Parker when she faints, and acquires a client when she is taken in for questioning.

Seymour confesses to protect Mrs. Parker, but Miss Withers does not believe him. She convinces Piper to notify the press that the murder was committed with a thrust through the left ear.

Later, Costello passes along a message from Chicago Lew, in which he claims to know the identity of the killer. However, when Piper and Miss Withers go to see him at the jail, they find him dead from hanging. Costello concocts a way in which Seymour could have escaped from his nearby cell using a duplicate key (which is found), strangled Lew, and hanged him with wire without entering Lews cell.

At the murder trial of Philip Seymour and Gwen Parker, while questioning Miss Withers, Costello slips up, showing that he knew that Gerald Parker was killed via the right ear. The motive is that he is Gwen Parkers current lover.

When Gwen Parker is released, the waiting Seymour slaps her in the face, to the amusement of Piper and Miss Withers. Piper then unexpectedly asks Miss Withers to marry him. She accepts. (However, in the sequel, Murder on the Blackboard, they are still single.)

==Cast==
*Edna May Oliver as Miss Hildegarde Martha Withers. Oliver reprised her role in two sequels, Murder on the Blackboard (1934) and Murder on a Honeymoon (1935). Robert Armstrong as Barry Costello
*James Gleason as Inspector Oscar Piper. Gleason played Piper in all six films in the series. 
*Mae Clarke as Gwen Parker Donald Cook as Philip Seymour
*Edgar Kennedy as Policeman Donovan Clarence Wilson as Bertrand B. Hemingway
*James Donlan as Security Guard Fink
*Gustav von Seyffertitz as Von Donnen / Dr Max Bloom
*William Le Maire as MacDonald, Aquarium Guard At Front Door
*Joe Hermano as Chicago Lew
*Guy Usher as Gerald Parker
*Rochelle Hudson as Parkers Telephone Operator
*Wilfrid North as The Judge

==References==
Notes
 
 

==External links==
*  
*  
*  
*  

 

 


 
 
 
 
 
 
 
 
 
 
 