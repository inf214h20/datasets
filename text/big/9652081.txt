Mickey's Gala Premier
{{Infobox film
| name           = Mickeys Gala Premier
| image          = 
| image_size     = 
| alt            = 
| caption        = 
| director       = Burt Gillett
| producer       = Walt Disney
| writer         = 
| based on       = 
| starring       = 
| studio         = Walt Disney Films
| distributor    = United Artists
| released       = July 1, 1933
| runtime        = 7 minutes
| country        = United States
| language       = English
}} cartoon produced in 1933, directed by Burt Gillett, and featuring parodies of several famous Hollywood film actors from the 1930s.

Some sources claim this cartoon is called "Mickeys Gala Premiere". However "Premier" can be clearly read from the title card.

==Synopsis==
A new Mickey Mouse cartoon will have its premiere in the Graumans Chinese Theatre in Hollywood. Several Hollywood celebrities all arrive in limousines to attend this special event. Outside The Keystone Cops (Ben Turpin, Ford Sterling, Mack Swain, Harry Langdon and Chester Conklin) are guarding the traffic.

Wallace Beery, Marie Dressler, Lionel Barrymore, John Barrymore and Ethel Barrymore step out of the first limousine, all costumed as in the film Rasputin and the Empress (1932). Then Laurel and Hardy leave the car and close the door behind them. Inside The Marx Brothers all stick their heads out of the car window.

In the next scene Maurice Chevalier, Eddie Cantor costumed as in the film The Kid from Spain, and Jimmy Durante take turns singing in front of a microphone. They are followed by Jean Harlow, Joan Crawford costumed as in the film Rain (1932 film)|Rain and Constance Bennett all singing new lyrics to the chant. Finally Harold Lloyd, Clark Gable, Edward G. Robinson, and Adolphe Menjou join in to conclude the song.
 Joe E. Brown simply enter, but Charlie Chaplin sneaks inside. Then Buster Keaton enters the building, followed by The Marx Brothers all hidden under Groucho Marx coat. Mae West enters, costumed as in the film She Done Him Wrong, and utters her famous line, "Why dont you come up sometime and see me?", which shocks and embarrasses Grauman.

Then Mickey Mouse, Minnie Mouse, Pluto (Disney)|Pluto, Horace Horsecollar, and Clarabelle Cow arrive in a limousine and are cheered by the audience. Once inside the theatre Mickey’s new cartoon, Gallopin Romance, premieres. The plot revolves around Mickey and Minnie playing music together, when suddenly Pegleg Pete kidnaps Minnie and drives off on a horse (Horace Horsecollar). Mickey chases him and beats Pete in the end, bringing Minnie to safety.
 Joe E. poker face. Jimmy Durante and Douglas Fairbanks laugh so loud that they literally "roll in the aisles". They are joined in by Groucho Marx, Joe E. Brown, Charlie Chaplin, Harold Lloyd and Oliver Hardy. As the cartoon ends the whole audience applauds and congratulates Mickey with his success. But Mickey is so shy that he has to be pulled on the stage by Will Rogers with a rope. All the Hollywood actors now shake Mickey’s hands (and feet!) to congratulate him with his success. Then Greta Garbo walks onto the stage and starts covering Mickey’s face with kisses. Mickey wakes up in his bed, while Pluto is licking his face. Mickey wonders if he was dreaming.

Other Hollywood celebrities that can be spotted in the crowd scenes: Constance Bennett, Warner Baxter, and Walt Disney (as the fourth person on the right, in the scene where the other actors shy away because Garbo enters the stage).

==Production notes==
* The cartoon seen in the film, Gallopin’ Romance, was made exclusively for Mickey’s Gala Premiere. No separate Mickey Mouse cartoon features this title.
 Alice in Mary Poppins (1964) and Maurice Chevalier would later sing the theme song of the Disney film The Aristocats (1970), both would go on to be named a Disney Legend, an award given to those who have made an outstanding contribution to The Walt Disney Company.

* Helen Hayes would later play Grandma Steinmetz in the Disney film Herbie Rides Again (1974).

* This the first time Mickey interacts with humans.

*At this point in time, the Mickey shorts were released through United Artists, but the only currently existing prints of this short are reissue prints that omit any mention of UA.  However, the "titles" for the short-within-a-short Gallopin’ Romance still exist in their original form, with the UA credits.  This gives todays viewer an idea on what an opening of a Mickey Mouse cartoon might have looked like in the UA era.

== Temporary shutdown of BBC Television Service ==
On 1 September 1939, Mickey’s Gala Premiere was the final programme broadcast by the BBC Television Service (todays BBC One) before it ceased broadcasting during World War II.  An urban legend about this final broadcast claims that due to the sudden outbreak of the war the BBC cut the cartoon short, and when BBC-TV resumed after the conflict, it was picked up at the very point it had been interrupted. (Some versions of the tale have the same presenter from 1939 say, "Now, as I was saying before I was so rudely interrupted...") 

Despite this widespread belief, the cartoon was shown in its entirety and then followed by an announcement of later scheduled programming (which wasnt  shown) and tuning signals. On 7 June 1946, the day BBC television broadcasts resumed after the war, Mickey’s Gala Premiere was shown again. 

==See also==
* Mickeys Polo Team
* Mother Goose Goes Hollywood
* Hollywood Steps Out
* Hollywood Daffy
* The Autograph Hound
* Slick Hare
* Whats Cookin Doc?
* Felix in Hollywood

==References==
 

==External links==
* 
* 
* 

 

 
 
 
 
 
 
 