Jayam Manadera
{{Infobox film
| name           = Jayam Manade Raa
| image          = jayammanaderaa.jpg
| imdb_id        = 
| writer         = Paruchuri Brothers  (dialogues ) 
| story          = N.Shankar Venkatesh Soundarya Bhanupriya 
| director       = N.Shankar
| producer       = Daggubati Suresh Babu|D.Suresh Babu
| studio         = Suresh Productions
| cinematography = K Ravindra Babu
| editing        = Marthand K. Venkatesh K.Madhav 
| released       = 7th October 2000
| runtime        = 2:32:07
| country        = India
| language       = Telugu
| music          = Vandemataram Srinivas
| awards         = 
| budget         = 
}} Tollywood film produced by Daggubati Suresh Babu|D.Suresh Babu on Suresh Productions banner, directed by N. Shankar. Starring Daggubati Venkatesh|Venkatesh, Soundarya, Bhanupriya in the lead roles and music composed by Vandemataram Srinivas. The film is recorded as Super Hit at box-office. This film was dubbed in Hindi as Dum Man Of Power.

== Plot ==

Abhiram (Daggubati Venkatesh|Venkatesh) is a fun loving guy staying with his parents in London. A bunch of 8 people won the contest of Thumps up, which gave them an opportunity to tour Europe. Uma (Soundarya) is one of the contest winners. Abhiram likes Uma and acts as a tour companion for them during their stay in Europe. A shy Uma could not express her love towards him so she leaves a message in Abhirams answering machine. But Abhiram could not listen to the message at the right time. Hence, Uma presumes that Abhiram does not love her and leaves back to India with her companions. Abhiram, who happened to listen the message after Uma left, calls up Uma and make a contact with her. But its too late for him, as the father of Soundarya commits a marriage for her daughter with Jasjit, the brother of main villain. On the other track, a parallel story is running in which Jasjit and his gang of goons is searching for Mahadeva Naidu. When the father of Prasad Babu sees the photograph of Abhiram, he gets frieghtened and approach Jaya Prakash with the photograph. Jaya Prakash wants to eliminate Mahadeva Naidu. When Abhiram comes to know that Uma may be forcibly getting married another guys, he decides to come to Umas village to marry his sweetheart. Just when he comes of the Railway Station, a gang that is after his life chases him and Uma. They (Abhiram & Uma) could manage to slip off the goons and get ready to flee. During that time, goons again chased him and he is saved by Jhansi in a nick of time. She takes him to a hideout where a group of people hide. They explain his about his flash back. He is the son of Mahadeva Naidu (Daggubati Venkatesh), who was the savior or Dalitulu and downtrodden people of Karamchedu. He libarated the Dalitulu in that area and he was killed by the villains, who are none but his relatives (Jaya Prakash). At the time of killing Rudrama Naidu (son) and Jhansi (daughter) are taken away by the lieutenants of Mahadeva Naidu so that they can be saved. At the time of killing Mahadeva Naidu swears that Rudrama Naidu would come back and destroy the villains and save the poor people. Rudrama Naidu, who is raised as Abhiraam, comes to know about the flash back and rest of the film is about how he takes the revenge.

==Cast==
{{columns-list|3| Venkatesh as Mahadeva Naidu & Rudraman Naidu / Abhiram (duelrole)
* Soundarya as Uma
* Bhanupriya as Bhuvaneswari
* Jaya Prakash Reddy as Narasimha Naidu
* Atul Kulkarni as Basavayah
* Brahmanandam as Krupakaram Ali 
* Tanikella Bharani as Malesh Yadav
* M. S. Narayana as Lingam AVS as Sonthi Paramahamsa 
* L. B. Sriram  as Mathayah
* Satya Prakash as Janardhan Naidu
* Ahuti Prasad as Gangadharam Naidu
* Prasad Babu as Subramanyam
* K.Ashok Kumar as Vyakuntha Naidu
* Benarjee as Kesava Naidu
* Ravi Babu as Ravindra Naidu
* Surya as Purushothama Naidu
* Raja Ravindra as Damodara Naidu
* Navabharat Balaji as Suridu  Ashok Kumar as Priest
* Gadiraju Subba Rao as Villager
* Narsingh Yadav as Police Officer
* Rama Prabha as Umas Bamma Venniradi Nirmala as Parvathi Hema as Chandramma
* Jhansi as Bhavani
* Siva Parvathi as Narasimha Naidus mother
* Bangalore Padma  Priya as Umas friend
* Ramyasri as Kesava Naidus wife
* Meena Kumari as Maheswari
* Sumalata 
* Shobha 
* Sunitha 
* Harika as Rangamma
* Indu Anand as Narasima Naidus wife
* Srija 
* Sweety
}}

==Soundtrack==
{{Infobox album
| Name        = Jayam Manadera
| Tagline     = 
| Type        = film
| Artist      = Vandemataram Srinivas
| Cover       = 
| Released    = 2000
| Recorded    = 
| Genre       = Soundtrack
| Length      = 34:01
| Label       = Aditya Music
| Producer    = Vandemataram Srinivas
| Reviews     =
| Last album  = Swayamvaram (1999 film)|Swayamvaram   (1999)
| This album  = Jayam Manade Raa   (2000) 
| Next album  = Kante Koothurne Kanu   (2000)
}}

Music composed by Vandemataram Srinivas. All songs are hit tracks. Music released on ADITYA Music Company.
{{Track listing
| collapsed =
| headline =
| extra_column = Singer(s)
| total_length = 34:01
| all_writing =
| all_lyrics =
| all_music =
| writing_credits =
| lyrics_credits = yes
| music_credits =

| title1  = Dont Miss Chandrabose
| extra1  = Shankar Mahadevan
| length1 = 4:59

| title2  = Meriseti Jaabili Sirivennela Sitarama Sastry
| extra2  = Kumar Sanu,Swarnalatha
| length2 = 5:32

| title3  = Happy Ga Jolly Ga
| lyrics3 = Sonu Nigam,Gopika Poornima
| extra3  = Veturi Sundararama Murthy
| length3 = 4:32

| title4  = Hindustan Lo Chandrabose
| extra4  = Udit Narayan,Jaspinder Narula
| length4 = 4:59

| title5  = Pelliki Baja Chandrabose
| SP Balu,Kavita Krishnamurthy
| length5 = 4:44

| title6  = O Chupuke  Chandrabose   SP Balu,Anuradha Sriram
| length6 = 4:58

| title7  = Chinni Chinni 
| lyrics7 = Lele Suresh  
| extra7  = Vandemataram Srinivas
| length7 = 4:45
}}

==Awards== Venkatesh won Filmfare Award for Best Actor – Telugu (2000).

==Release==
*The film released with 114 prints in 149 centres.   

==Box office performance==
*The film collected a distributors share of Rs. 30.4 million in its opening week. 

== References ==
 

 
 