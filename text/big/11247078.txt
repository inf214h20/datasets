Lucid (film)
{{Infobox film
| name           = Lucid
| image          = lucid_filmposter.jpg
| caption        = 
| tagline        = Joel Rothman cant sleep Sean Garrity Sean Garrity
| writer         = Sean Garrity Jonas Chernick
| starring       = Jonas Chernick   Lindy Booth  Callum Keith Rennie  Kristen Harris Michelle Nolden
| music          = Richard Moody
| cinematography = Michael Marshall
| editing        = John Gurdebeke
| distributor    = Mongrel Media
| released       =  
| runtime        = 113 minutes
| country        = Canada
| language       = English
| budget         = 
}} directed by Sean Garrity. It won the award for Best Western Canadian Feature Film at the Vancouver International Film Festival in 2005 and it was nominated for Best Film at the International Filmfestival Mannheim-Heidelberg|Mannheim-Heidelberg International Film Festival in 2006.

==Plot==
Joel Rothman (Jonas Chernick) is suffering from insomnia after having massive problems in his personal life including a separation and being targeted by his boss. As a psychotherapist he is assigned three patients suffering from Post Traumatic Stress Disorder. He must treat them to figure out his own life.

==External links==
*  
*  

 
 
 
 


 
 