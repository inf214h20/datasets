Happy Husbands (2011 film)
{{Infobox film
| name           = Happy Husbands
| image          = HappyHusbands2011Film.jpg
| caption        = Promotional poster Anay Sharma
| producer       = Suresh Sharma Anay Sharma Kurush Deboo Gaurav Ghai Bakul Thakkar Farida Archana Anay Sharma Anay Sharma
| cinematography = Srikant Naroj
| editing        = Vilas Ranade   Deepak Wirkud  Anay
| studio         = Phenomenal Craft
| distributor    = 
| released       =  
| country        = India
| language       = Hindi
}} Anay Sharma and produced by Suresh Sharma. The film was released under the Phenomenal Craft banner.  


==Plot==
Arjun, Champoo, and Mohit are married to wives who love them, however, they would prefer to meet other women and even date them. For this purpose, Arjun introduces them to a successful executive, Jaiveer, who is married to Priya, and has a young son. Through him they meet and get involved with a variety of women. Their respective wives are not aware of these turn of events, however, things get complicated for Jaiveer himself when his wife gets evidence of his infidelity.


==Cast== Anay Sharma as Jaiveer
* Ahwaan Kumar as Arjun
* Kurush Deboo as Champoo Patel
* Mohit Ghai as Mohit
* Archana Sharma as Priya
* Danica Moadi as Diya, Arjuns wife
* Shruti Sharma as Naina, Mohits wife
* Akanksha as Jimmys wife 
* Jimmy Cooper as Jimmy
* Sunil Bhalla as Bhalla
* Aray Bhalla as Happy Singh
* Gaurav Ghai as Dave
* Lisa as Daves girlfriend 
* Tonikram Mattoo as Chadda
* Harshaali as Chaddas wife
* Heena Rajput as Heena

==Soundtracks== Anay Sharma and lyrics written by Sarim Momin.
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)

|-
| 1
| Aai Aai Aai Hai  Anay Sharma
|-
| 2
| Love You
| Javed Ali
|-
| 3
| Tum Se
| Naresh Iyer
|-
| 4
| Tu Hi Meri
| Hamza Faruqui
|-
| 5
| Aish Kar Aish Kar
| Sunidhi Chauhan
|}

==Notes and references==
 
 

==External links==
*  

 
 
 
 
 


 
 