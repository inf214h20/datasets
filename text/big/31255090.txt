A Warm Corner
{{Infobox film
| name = A Warm Corner
| director = Victor Saville
| starring = Leslie Henson Heather Thatcher Austin Melford
| released =  
| runtime = 104 minutes
| country = United Kingdom
| language = English
}}
A Warm Corner is a 1930 British comedy film directed by Victor Saville and starring Leslie Henson, Heather Thatcher and Austin Melford.  The films sets were designed by Walter Murton. It was based on a successful play by Franz Arnold. If featured an early screen appearance by Merle Oberon.

==Cast==
* Leslie Henson - Mr Corner
* Heather Thatcher - Mimi
* Austin Melford - Peter Price
* Connie Ediss - Mrs Corner
* Toni Edgar-Bruce - Lady Bayswater
* Alfred Wellesley - Mr Turner
* Kim Peacock - Count Toscani
* Belle Chrystall - Peggy
* George DeWarfaz - Count Pasetti
* Harry Crocker - Joseph
* Merle Oberon - Minor role

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 


 