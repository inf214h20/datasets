The Anniversary Party
{{Infobox film
| name           = The Anniversary Party
| image          = The Anniversary Party Poster.jpg
| image_size     = 215px
| alt            = 
| caption        = Theatrical release poster
| director       = Jennifer Jason Leigh Alan Cumming
| producer       = Jennifer Jason Leigh Alan Cumming
| writer         = Jennifer Jason Leigh Alan Cumming Jane Adams Parker Posey Phoebe Cates Gwyneth Paltrow
| music          = Michael Penn John Bailey
| editing        = Carol Littleton Suzanne Spangler
| studio         = Ghoulardi Film Company Pas de Quoi
| distributor    = Fine Line Features
| released       =  
| runtime        = 115 minutes  
| country        = United States
| language       = English
| budget         = 
| gross          = $4,931,888 
}}
The Anniversary Party is a 2001 American comedy-drama film written, directed, produced by, and starring Jennifer Jason Leigh and Alan Cumming.

==Plot==
Sally Nash and Joe Therrian are a Hollywood couple celebrating their sixth wedding anniversary shortly after reconciling following a period of separation. He is a novelist who is about to direct the screen adaptation of his most recent bestseller; she is an actress he has opted not to cast in the lead role, despite the fact its partly based on her, because he feels shes too old for the part. This decision, coupled with an ongoing dispute about their barking dog Otis with their strait-laced, non-industry neighbors, clean-and-sober writer Ryan and interior decorator Monica Rose, has resulted in an undercurrent of tension between the two as they prepare for the arrival of their guests.
 neurotic wife Clair; photographer Gina Taylor, whose relationship with Joe prior to his marriage and ongoing close friendship since troubles Sally; business manager Jerry Adams and his wife Judy; eccentric violinist Levi Panes; Jeffrey, Joes roommate - and lover - at University of Oxford|Oxford; and up-and-coming actress Skye Davidson, whom Joe has cast in the role Sally believes deservedly is hers. In an effort to dispel the simmering animosity between them and their neighbors, Sally and Joe have invited the Roses as well. 
 ecstasy Skye brought them as a gift. As it begins to take effect, the night deteriorates, accusations are made, secrets are revealed, and relationships slowly unravel. Complicating emotions triggered by the drug are the disappearance of Otis and a phone call from Joes father bringing tragic news about his beloved sister Lucy.

==Cast==
* Jennifer Jason Leigh as Sally Nash
* Alan Cumming as Joe Therrian
* Kevin Kline as Cal Gold
* John C. Reilly as Mac Forsyth Jane Adams as Clair Forsyth
* Parker Posey as Judy Adams
* Phoebe Cates as Sophia Gold
* Gwyneth Paltrow as Skye Davidson
* Denis OHare as Ryan Rose
* Mina Badie as Monica Rose
* John Benjamin Hickey as Jerry Adams
* Michael Panes as Levi Panes
* Jennifer Beals as Gina Taylor

==Production notes== Sundance Channel series Anatomy of a Scene, the filmmakers discussed the project at length. Because of conflicting schedules, there was a period of only 19 days in which the entire cast—consisting of friends and actors with whom Leigh and Cumming previously had worked—would be available for filming. This prompted the decision to film using digital video, which Leigh felt also added a sense of immediacy and intimacy that would draw the audience into the action as party guests observing everything from the sidelines.

The order in which the toasts were made was determined before the scene was filmed, although with the exception of that offered by Skye, they were improvised rather than scripted. 

Retired actress Phoebe Cates returned to acting for this one film, as a favor to director Leigh, her best friend.

==Soundtrack==
The films soundtrack includes "I Know a Place" by Petula Clark, "I May Never Go Home Anymore" by Marlene Dietrich, "Comin Home Baby" by Mel Tormé, "There Is No Greater Love" and "A Lot of Livin to Do" by Sammy Davis Jr., "Stealing My Love from Me" by Lulu (singer)|Lulu, "Troubles" by Blair Tefkin and the Adagio from the Sonata No. 1 in G minor by Johann Sebastian Bach.

==Release==
The film premiered at the 2001 Cannes Film Festival in May    prior to its limited release in the US the following month. It grossed $4,047,329 in the US and $884,559 in foreign markets for a total worldwide box office of $4,931,888.   

==Critical reception==
In his review in The New York Times, Stephen Holden called it "an articulate, acutely observant film   makes you realize how starved Hollywood movies are for great ensemble acting . . . the movie has such finely woven performances that the best scenes project a convincing illusion of spontaneity . . . Ms. Leigh and Mr. Cummings screenplay does an amazing job of creating about a dozen fully rounded, nuanced characters with a minimum of words. The dialogue, though it comes quickly and in scraps, is so juicy that the zest with which the actors bite into it suggests they invented it themselves . . . This isnt Anton Chekhov|Chekhov, by any stretch of the imagination. The empathy the film extends to its characters may be evenly distributed, but it isnt all-embracing. Yet despite its shortcomings, this smart, caustic movie is easily the most incisive and realistic comedy of manners to emerge from Hollywood in quite a while, and thats saying a lot." 

Roger Ebert of the Chicago Sun-Times observed, "The appeal of the film is largely voyeuristic. We learn nothing we dont already more or less know, but the material is covered with such authenticity and unforced natural conviction that it plays like a privileged glimpse into the sad lives of the rich and famous. Were like the neighbors who are invited. Leigh and Cumming . . . are confident professionals who dont indulge their material or themselves. This isnt a confessional home movie, but a cool and intelligent look at a lifestyle where smart people are required to lead their lives according to dumb rules." 

In the San Francisco Chronicle, Mick LaSalle said, "Leigh and Cumming save the best roles for themselves, and both their roles reach major emotional crescendos. Yet to their credit, with all the video in the world at their disposal and nobody to rein them in, they dont indulge themselves. Theyre both brilliant, spot-on and wonderfully true. The Anniversary Party is probably one of those miracles that can happen only once. Still one cant help hoping that Leigh and Cumming collaborate on another film." 

Peter Travers of Rolling Stone stated, "The final result should be a self-indulgent mess - and, in truth, the final third of the film comes close. But until Leigh and Cumming let their actorly urge for high drama blunt their flair for bracing wit and subtle feeling, they turn what could have been an acting stunt into an intimate and compelling study of bruised emotions . . .   were fortunate to secure the services of master cinematographer John Bailey, who brings textured marvels of light and shadow to digital camerawork that is often crude in lesser hands. Its only when the guests head for the pool to play truth games on Ecstasy while Leigh and Cumming head for the hills for a Whos Afraid of Virginia Woolf? sparring match that the movie collapses under the weight of its artsy-fartsy ambitions. My advice on how to get the most from this Party is: Leave early." 

In Variety (magazine)|Variety, Todd McCarthy said the film "is well observed in many particulars but is too familiar in its basic trajectory to be fresh or compelling . . . All the roles are in good hands, and its mildly amusing in a voyeuristic way to watch the likes of Paltrow behave as we might imagine stars do at a party . . . Although the digital video imprint is still evident, ace vet lenser John Bailey has gone a long way toward making this look like a celluloid-shot picture, most successfully in the bright, daytime scenes, less so at night or under low lighting conditions, where the images sometimes appear washed out." 

==Awards and nominations==
* Independent Spirit Award for Best First Film (nominee - lost to In the Bedroom) Ghost World)
* Independent Spirit Award for Best Supporting Actor - John C. Reilly (nominee - lost to Steve Buscemi for Ghost World) National Board of Review Award for Excellence In Filmmaking (winner)

==See also==
* List of American films of 2001

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 