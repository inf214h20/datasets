Day of Independence
{{Infobox film
| name = Day of Independence
| image = DAYposter.jpg
| caption = poster design: Dennis Mukai
| runtime = 27 minutes
| creator = Cedar Grove Productions
| director = Chris Tashima
| producer = Lisa Onodera
| writer = Chris Tashima Tim Toyama
| based on =  
| starring = Derek Mio Marcus Toji Alan Muraoka Keiko Kawashima Gina Hiraizumi Chris Tashima
| music = Scott Nagatani
| country = United States
| language = English
| released =  
}} Visual Communications as fiscal sponsor.

== History ==
The story of the film is based on playwright and executive producer Tim Toyamas own fathers World War II experience.  During the war, Toyamas father, whose nickname was Zip, was sent along with his entire family to a Japanese American internment|U.S. internment camp for Japanese Americans. Zips Issei Japanese American|Issei (Japanese immigrant) father fell ill and elected to return to Japan, along with Zips mother, on a prisoner exchange ship, called the MS Gripsholm|Gripsholm.   However, the parents told Zip that as an American, he should remain in the U.S.

== Background ==
Toyama wrote a play based on his family history, entitled "Independence Day." He and director Chris Tashima then adapted the play into a short film, which was produced by Lisa Onodera.  The film was shot in 6 days, in Stockton, California and in Los Angeles.   It was completed in 2003 and played in over 70 film and video festivals and competitions, winning 25 awards.  Following its broadcast premiere on KHET|KHET/PBS Hawaii on May 12, 2005,  the film received a Regional Emmy nomination, from the NATAS San Francisco/Northern California Chapter (which includes Hawaii), in the category of Historical/Cultural – Program/Special. 

==Synopsis==
A young baseball player faces the tragic circumstances of the internment of 110,000 Americans of Japanese ancestry during World War II.  Set in a relocation camp in 1943, Day of Independence chronicles a family torn apart by a forced, unjust incarceration, a fathers decision that challenges his son, and ultimately his sons triumph through courage, sacrifice and the All-American game of baseball.

==Cast==
* Derek Mio as Zip
* Marcus Toji as Hog
* Alan Muraoka as Father
* Keiko Kawashima as Mother
* Chris Tashima as The Umpire
(In order of appearance)
* Dean Komure as Spectator National Anthem" singer)
* Diana Toshiko as Betty
* Sarah Chang as Sadie
* Julie Tofukuji as Mimi
* Ulysses Lee as Tad
* Jonathan Okui as Satch
* Gina Hiraizumi as Rose

==Awards==
(partial list) Emmy Awards
*CINE Golden Eagle 
*Platinum Best of Show - Aurora Awards
*Accolade Award of Excellence, Short Film 
*Best Short - Stony Brook Film Festival 
*Best Narrative Short - Tambay Film & Video Festival 
*"Slate" Award, Best Short - California Independent Film Festival 
*Best Short Film - Houston Multicultural Independent Film Festival
*Gold / 1st Prize - Crested Butte Reel Fest 
*Audience Award, Best Drama - Marco Island Film Festival

== Trivia ==
* Performance artist Dan Kwong, who is also a baseball player, coached lead actor Derek Mio in old-style pitching form. Nisei singer Minidoka Japanese internment camp as a teenager, where she was known for her singing.  Joe provided the singing voice on the soundtrack as well, which was recorded on Mothers Day just after her mother died from cancer.
* During one of the montage sequences of "camp life," there is a painter standing by an easel, who is portrayed by playwright Tim Toyama, who stepped in at the last moment when the person cast to play the part didnt show up for filming.
* The scene depicting the choir rehearsal has un-credited cameo appearances by noted actors Tamlyn Tomita, Emily Kuroda, Sab Shimono and Greg Watanabe who are friends of the director. 

==References==
 

== External links ==
*  
*   by Philip W. Chung in AsianWeek - 11/28/03
*   in Honolulu Star-Bulletin

 
 
 
 
 
 