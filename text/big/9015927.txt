Kannum Kannum
{{Infobox film
| name           = Kannum Kannum
| image          = 
| director       = G. Marimuthu
| producer       = M.R. Mohan Ratha  B.S. Radhakrishnan Santhanam
| music          = Dhina
| cinematography = Balasubramaniem
| editing        = G. Sasikumar
| released       =  
| country        = India
| language       = Tamil
}} Tamil film Prasanna plays the leading role as Sathyamoorthy, and Udhayathara pairs with him. The films score and soundtrack is composed by Dhina, with lyrics by "Kavi Perarasu" Vairamuthu. It premièred on Kalaignar TV as part of their Gandhi Jayanthi Special Programmes on  October 2, 2008.

==Plot==
Sathyamoorthy, an orphan, vents his feelings in a poem but doesn’t submit it to a magazine. Surprisingly he reads a poem, penned by a girl, in a magazine that is quite similar to what he has written. He is amused and sets off to find the girl. He finds that she is a college student and living in Kutralam. He writes a letter and gets a reply. The friendship develops and it gradually evolves into love.

One fine day he goes to Kutralam to meet the girl and stays in his friend’s house. The girl is away on a college tour. Ironically, she is the sister of his friend. In the second half, the friend dies in a mishap accidentally caused by Sathyamoorthy. Wishing to atone for his fault, he takes on his friends responsibilities and considers his friend’s sisters as his own sisters. When the girl comes back, she finds a new brother, who is supposed to be her lover. She is opposed to Sathyamoorphy since she thought it was totally his fault until she realises why he has come to Kutralam. This is followed by an emotional resolution of their love.

The Subplot follows Vadivelu as Udumban, whose antics are one of the highlights in the movie, especially the "Kanatha Kaanom" scene. A reference to the scene is made in the 2009 film Kanthaswamy.

==Cast== Prasanna as Sathyamoorthy
* Udhayathara as Anandhi
* Vadivelu as Udumban Vijayakumar
* Harish Musa Rajesh
* Santhanam

==References==
 
==External links==
*  

 
 
 
 


 