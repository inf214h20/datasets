Whispers in the Dark (film)
{{Infobox film
| name           = Whispers in the Dark
| image          = Whispers in the dark poster.jpg
| image size     =
| caption        = Theatrical release poster
| director       = Christopher Crowe
| producer       = Martin Bregman Michael Bregman
| writer         = Christopher Crowe
| narrator       = Deborah Unger Alan Alda
| music          = Thomas Newman Michael Chapman
| editing        = Ray Hubley Bill Pankow
| distributor    = Paramount Pictures
| released       = August 7, 1992 
| runtime        = 103 minutes
| country        = United States
| language       = English
| budget         =
| gross          = $11,124,511 IMDB,  .  Retrieved 2014-04-01.  
| preceded by    =
| followed by    =
}}

Whispers in the Dark is a 1992 American thriller about a psychiatrist whose patients lover may or may not be a serial killer. The film starred Annabella Sciorra, Jamey Sheridan, Alan Alda, Jill Clayburgh, John Leguizamo and Anthony LaPaglia. The film was released by Paramount Pictures on August 7, 1992. It was nominated for a Razzie Award for Alan Alda as Worst Supporting Actor.

==Plot==
The confessions of a sadomasochistic sexually obsessed patient disclosed as fantasies during Manhattan psychiatrist sessions begin to permeate the troubled doctors subconscious as erotic dreams. This leads to a heated love affair. At the same time a series of shocking murders occur with evidence suggesting it is her new lover / patient.

==Cast==
*Annabella Sciorra  - Ann Hecker
*Jamey Sheridan 	- Doug McDowell
*Anthony LaPaglia  	- Det. Morgenstern
*Jill Clayburgh  	- Sarah Green
*John Leguizamo  	- Fast Johnny C. Deborah Unger - Eve Abergray
*Alan Alda  	- Leo Green
*Anthony Heald  	- Paul
*Jacqueline Brookes - Mrs. McDowell
*Gene Canfield  	- Billy OMeara
*Joe Badalucco  	- Undercover Cop
*Mary Colquhoun  	
*Bo Dietl  	- Detective Ditali
*Allison Field  	-  
*Nicholas J. Giangiulio-  
*Sondra James  	-  
*David Kramer  	-  
*Philip Levy  	-  
*Karen Longwell  	-  
*Art Malik  	- Earring Vendor
*Dominic Marcus  	-  
*Albert Pisarenkov 	- Cab Driver
*William Timoney  	-  
*Lisa Vidal  	-  

==Reception==

The movie gained a negative reception. The New York Times said "in its worst moments,   is exploitative, with the detective flashing gruesome photos of tortured women at Ann. More often, it is so loopy it should have been played for laughs."  Owen Gleiberman of Entertainment Weekly gave the film a C- grade.  Los Angeles Times staff writer Peter Rainer called it "a textbook thriller" and stated "Doug is so Too Good to Be True that, when the inevitable murder makes its scheduled stop,   can sniff red herring a mile away. But then this movie has so many of them--including a hot-footed cop played by Anthony LaPaglia and a psychiatrist friend of Anns played by Alan Alda--that   practically need a trawler to get through it."  It has a total of 25% on Rotten Tomatoes.

===Box Office===

The film, while grossing over $11 million,  was not considered to be financially successful.  

==References==
 

== External links ==
*  

 
 
 
 
 
 
 
 