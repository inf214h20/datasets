Off Beat (1986 film)
 
{{Infobox Film
| name           = Off Beat
| image          = Offbeatposter.jpg
| image_size     = 
| caption        = Theatrical release poster
| director       = Michael Dinner
| producer       = Joe Roth Harry J. Ufland
| screenplay     = Mark Medoff
| story          = Dezsö Magyar
| starring = {{Plainlist|
* Judge Reinhold
* Meg Tilly Cleavant Derricks Jacques dAmboise
* Harvey Keitel
}}
| music          = James Horner
| cinematography = Carlo Di Palma
| editing        = Dede Allen Angelo Corrao Touchstone Films Silver Screen Partners II Buena Vista Distribution
| released       = April 11, 1986
| runtime        = 92 min
| country        =  
| language       = English
| budget         = 
| gross          = $4,117,061 (USA)
}}
 1986 comedy Cleavant Derricks.

==Plot==
Joe Gower is a likable librarian who glides around his job on roller skates. He has a strict boss, Mr. Pepper, and a good friend whos a cop, Abe Washington.

A mistake he makes inadvertently messes up Washingtons undercover work. Joe now owes him a favor, but is unprepared for what Washington wants. A police charity event needs officers to dress in drag, but because Washington wants no part of that, he asks Joe to take his place.

A reluctant Joe decides to go through with the audition, expecting to be so bad that he wont be cast in the show. When he goes there and meets an attractive policewoman, Rachel Wareham, it changes everything. Joe not only does the show, he continues to keep from Rachel the fact that hes not a real cop.
 impersonating a police officer or being shot by a crook.

==Cast==
* Judge Reinhold as Joe
* Meg Tilly as Rachel
* John Turturro as Pepper Cleavant Derricks as Washington
* Harvey Keitel as Mickey
* Joe Mantegna as Peterson
* Anthony Zerbe as Wareham
* Amy Wright as Mary Ellen
* Penn Jillette as Norman
* Mel Winkler as Earl
* Irving Metzman as Deluca Mike Starr as James Bonnell
* Shawn Elliott as Hector
* Stanley Simmonds as Pud
* Nancy Giles as Celestine Paul Butler as Jordan
* John Kapelos as Lou Wareham William Sadler as Dickson (as Bill Sadler)
* Chris Noth as Ely Wareham Jr. (as Christopher Noth)
* Austin Pendleton as Gun Shop Salesman

==Critical reaction==
In his review of April 11, 1986, Roger Ebert of the Chicago Sun-Times gave this film three-and-a-half stars out of a possible four, describing it as one of the years best comedies. 

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 