I Can't Escape
{{Infobox film
| name           = I Cant Escape
| image          =
| caption        =
| director       = Otto Brower Max Alexander (producer) Peter E. Kassler (associate producer) Sam Wiesenthal (associate producer)
| writer         = Nathan Ash (story) Jerry Sackheim (story) Faith Thomas (writer)
| narrator       =
| starring       = See below
| music          =
| cinematography = Jerome Ash
| editing        = Frederic Knudtson Louis Sackin
| distributor    =
| released       =  
| runtime        = 60 minutes 26 minutes (American edited TV version)
| country        = United States
| language       =
| budget         =
| gross          =
}}

I Cant Escape is a 1934 American film directed by Otto Brower. The film is also known as The Magic Vault (American alternative title).

== Cast ==
*Onslow Stevens as Steve Nichols, alias Steve Cummings
*Lila Lee as Mae Nichols
*Russell Gleason as Tom Martin William Desmond as Parole Officer Donovan
*Hooper Atchley as Harley
*Otis Harlan as Jim Bonn
*Kane Richmond as Bob, college boy at club
*Clara Kimball Young as Mrs. Wilson
*Eddie Gribbon as Regan - Beat Cop
*Nat Carr as Mr. Watson, clothier
*Richard Cramer as Joe, bartender-pimp

== External links ==
* 
* 

 
 
 
 
 
 
 
 

 