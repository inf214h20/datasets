Sarah Brightman: In Concert
 
 
{{Infobox film
| name           = Sarah Brightman: In Concert
| image          = SarahBrightmanInConcert.png
| image_size     =
| caption        = David Mallet
| producer       =
| writer         =
| narrator       =
| starring       = Sarah Brightman
| music          =
| cinematography =
| editing        =
| distributor    = Sony Pictures
| released       =  
| runtime        = 75 min.
| country        =
| language       =
| budget         =
| gross          =
}}
Sarah Brightman: In Concert is a live concert recording by English soprano Sarah Brightman. Recorded at the Royal Albert Hall, it was released on DVD and VHS in 1998 and reissued in 2008 in DVD and CD. Guests in the concert include Adam Clarke, Andrew Lloyd Webber and Andrea Bocelli. The conductor is Paul Bateman who leads the English National Orchestra.

==Track listing==

# "Overture: Capriccio Espagnol: Scena e canto gitano/Fandango asturiano"
# "Chants dAuvergne|Baïlèro"
# "Les Filles de Cadiz" (by Léo Delibes)
# "O mio babbino caro"
# "Peer Gynt (Grieg)|Solveigs Song"
# "Summertime (song)|Summertime" Pie Jesu" (duet with Adam Clarke)
# "Medley: Somewhere (song)|Somewhere/I Feel Pretty/Tonight (1956 song)|Tonight"
# "Tu Quieres Volver" Who Wants To Live Forever" Whistle Down The Wind" (with Andrew Lloyd Webber on piano)
# "Overture/Wishing You Were Somehow Here Again"
# "The Music of the Night" Time to Say Goodbye" (duet with Andrea Bocelli)
# "Dont Cry for Me Argentina"

==Certifications==
 
 
 
 

==References==
 

== External links ==
*  

 

 
 
 
 
 