Blood Lake
 

{{Infobox film
| name           = Blood Lake
| image          = BloodLakeXXX.jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = Cover art of Blood Lake
| film name      = 
| director       = C.L. Gregory
| producer       = David Lash   C.L. Gregory
| writer         = Erin Gilmer
| screenplay     = 
| story          = 
| based on       = 
| narrator       = 
| starring       = Dan Holmes   Dick Chibbles   Tyler Houston   India Summer   C.J. Summers   Bobby Banger   Sandy Simmers Kevin MacLeod
| cinematography = Bryan McPherson
| editing        = Roderick D. Escue
| studio         = Midnite Films
| distributor    = VCA Pictures
| released       =  
| runtime        = 78 minutes
| country        = United States English
| budget         = 
| gross          = 
}}
 pornographic horror directed by C.L. Gregory.

== Plot ==
A group of friends (Andy, Roger, Tricia, Max, Nell, Charlotte and Nicole) head to some lakeside cabins for the weekend. Arriving at the cabins, the septet find a "blood book" that tells the story of Preacher Jacob, a priest who took over the nearby towns congregation in 1896. Jacob had his followers commit mass suicide, under the belief that he could resurrect them as a new, perfect race enlightened by their deaths. Max and Nell go off to have sex, but before they do so Max skims the blood book and reads an incantation aloud, summoning the homicidal spirit of Preacher Jacob.

The first of the group to be killed is Roger, who Preacher Jacob axes in the back. The preacher then captures Nicole, ties her to a tree, and hacks her to death with the axe. Next, Sam is garroted with a rope, and Nell has her throat slit in the shower. Tricia realizes what is going on, and tries to get help from Andy and Charlotte, but the two are kept sealed in their cabin by a supernatural force until Tricia is scared off by Preacher Jacob, who Andy and Charlotte spot, and conclude is connected to the blood book. Andy and Charlotte decide to barricade themselves in the cabin until sunrise, and pass the time by having sex.

Later, Charlotte goes to wake Andy up, but finds Preacher Jacob in his bed. The preacher kills her, and goes after the wounded Andy (who has the blood book) and Tricia. Preacher Jacob chases Andy and Tricia to a dock, strangles Andy, and is seemingly destroyed when Tricia sets the blood book on fire. Tricia gets in one of the cars and drives away, but as she leaves the campgrounds, Preacher Jacob appears in the vehicle, and attacks her.

== Cast ==
* C.J. Summers as Charlotte
* Tyler Houston as Tricia
* Sandy Simmers as Nicole
* India Summer as Nell
* Johnny Depth as Roger
* Dick Chibbles as Max
* Bobby Banger as Andy
* Dan Holmes as Preacher Jacob

== Reception ==
AVN (magazine)|AVN said the films sex scenes were passable but that it suffered from an incoherent script, sloppy editing, painfully bad acting, and dull dialogue. 

Adult DVD Talk gave Blood Lake an overall grade of a half star out of five, and stated the photography was terrible, the editing laughably inept, and the acting and gore non-existent.  X Critic awarded the film one star out of five, and criticized every aspect of it, writing that it is "one of the worst hardcore releases in recent years" and that it felt "half completed, over edited and devoid of anything that makes terror and twat the least bit interesting". 

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 