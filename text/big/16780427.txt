The Unholy Three (1930 film)
{{Infobox film
| name           = The Unholy Three
| image          = The Unholy Three (1930 film).jpg
| caption        = Film poster Jack Conway
| producer       = Irving Thalberg (uncredited)
| based on       =   
| writer         = J.C. Nugent   Elliott Nugent Lon Chaney   Lila Lee   Elliott Nugent Harry Earles   John Miljan   Ivan Linow
| music          = William Axt (uncredited)
| cinematography = Percy Hilburn Frank Sullivan
| studio         = Metro-Goldwyn-Mayer
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 72 minutes
| country        = United States
| language       = English
| gross          = $988,000   
}}
 Jack Conway Lon Chaney. 1925 film of the same name, with both movies based on the novel The Unholy Three, by Clarence Aaron "Tod" Robbins.
 throat cancer one month after the films release.  

==Plot== strongman Hercules (Ivan Linow), and he leave and, as "The Unholy Three", use their talents to commit crimes. Echo also takes along his pickpocket girlfriend Rosie (Lila Lee) and his gorilla, whom Hercules fears. 

Echo disguises himself as Mrs. OGrady, a kindly old grandmother who runs a pet shop. Tweedledee pretends to be her baby grandson, and Hercules her son-in-law. They use the information they gain from their wealthier patrons to rob them. Echo is the leader and brains behind the outfit, but his bossy ways leave the other two resentful. Meanwhile, the shops clerk, Hector McDonald (Elliott Nugent), falls in love with Rosie.

The gang is ready to pull off a theft on Christmas Eve. When Echo decides to postpone it, Tweedledee and Hercules go ahead without him. Afterwards, Tweedledee gleefully recounts how they not only robbed but also killed the wealthy Mr. Arlington, despite his pleas for mercy. Worried about the police, they decide to frame Hector by planting a stolen necklace in his closet. 

That same night, Hector asks Rosie to marry him. Ashamed of her past, she pretends she was only leading him on for a laugh. After he leaves, she starts crying; he returns, sees that she really does love him, and they become engaged.

However, Hector is arrested for murder. Still frightened, the Unholy Trio hide out in an isolated cabin in the country, forcibly taking Rosie with them. Rosie pleads with Echo to exonerate Hector somehow in exchange for her returning to him. Tweedledee tries to persuade Hercules to shoot them both, but the strongman refuses.

Echo, as "Grandma" OGrady, shows up at the trial and tries to provide an alibi, but slips up and his disguise is discovered. He makes a full confession and receives a sentence of one to five years. Back at the cabin, Tweedledee overhears Hercules offering Rosie a chance to run away with him (and the loot), so he lets loose the gorilla; Hercules murders the midget before he himself is killed by the ape. Rosie escapes.

As Echo is being taken to prison, Rosie promises to wait for him, honoring their agreement. Realizing she loves Hector, he generously tells her not to.

==Cast== Lon Chaney as Professor Echo / Mrs OGrady
*Lila Lee as Rosie OGrady
*Elliott Nugent as Hector McDonald
*Harry Earles as Midget / Tweedle Dee
*John Miljan as Prosecuting Attorney
*Ivan Linow as Hercules
*Clarence Burton as Detective Regan
*Crauford Kent as Defense Attorney
*Trixie Friganza as woman in birdshop (uncredited)
*Charles Gemora as the gorilla (uncredited)

==References==
 

== External links ==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 