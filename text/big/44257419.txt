Ragasiya Police
{{Infobox film
| name           = Ragasiya Police
| image          = 
| image_size     =
| caption        = 
| director       = R. S. Elavarasan
| producer       = A. N. Sundaresan
| writer         = R. S. Elavarasan Balakumaran  (dialogues) 
| starring       =  
| music          = Laxmikant–Pyarelal
| cinematography = Ashok Kumar
| editing        = G. Jayachandran
| distributor    =
| studio         = A. N. S. Film International
| released       =  
| runtime        = 155 minutes
| country        = India
| language       = Tamil
}}
 1995 Tamil Tamil crime film directed by R. S. Elavarasan. The film features R. Sarathkumar and Nagma in lead roles. The film, produced by A. N. Sundaresan, had musical score by Laxmikant–Pyarelal and was released on 23 October 1995. The film bombed at the box-office.   

==Plot==
 Anti Terrorist Squads Assistant Commissioner Suriya (R. Sarathkumar) to track down the terrorist group. Meanwhile, Suriya and Raji fall in love with each other. What transpires next forms the rest of the story.

==Cast==
 
*R. Sarathkumar as Suriya
*Nagma as Raji
*Raadhika Sarathkumar as Central Minister
*Anandaraj as Azhagam Perumal
*Goundamani as Ponnurangam Devan as Dinesh Senthil as Harichandran
*Vinu Chakravarthy as Dharmaraj Sangeeta as Suriyas mother
*Vichithra as Ezhilarasi
*S. N. Lakshmi
*Jyothi Meena
*Kavithasri
*Bindiya
*Sudharshan
*Muthukumar
*Krishnamoorthy
*Raviraj
*O. A. K. Sundar
*Mahanadi Shankar
 

==Soundtrack==

{{Infobox Album |  
| Name        = Ragasiya Police
| Type        = soundtrack
| Artist      = Laxmikant–Pyarelal
| Cover       = 
| Released    = 1995
| Recorded    = 1995 Feature film soundtrack |
| Length      = 30:57
| Label       = 
| Producer    = Laxmikant–Pyarelal
| Reviews     = 
}}

The film score and the soundtrack were composed by film composer Laxmikant–Pyarelal. The soundtrack, released in 1995, features 6 tracks with lyrics written by Vaali (poet)|Vaali.  

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Track !! Song !! Singer(s) !! Duration
|- 1 || Yen Yen || S. P. Balasubrahmanyam, Kavita Krishnamurthy || 6:03
|- 2 || Maiyil Thogai || S. P. Balasubrahmanyam || 5:17
|- 3 || Manmathan || Kavita Krishnamurthy || 5:22
|- 4 || Kann Imaikkamal || Mano (singer)|Mano, Swarnalatha || 3:59
|- 5 || Kann Imaikkamal (repeat) || Mano, Swarnalatha || 3:59
|- 6 || Echangatru || Kavita Krishnamurthy || 6:17
|}

==References==
 

 
 
 
 
 

 