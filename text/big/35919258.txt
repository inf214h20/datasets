East Meets West (1995 film)
{{multiple issues|
 
 
}}

East Meets West is a Japanese film directed by Okamoto Kihachi, and was released in 1995. 

==Cast== Christopher Mayer as Gus Taylor
* Hiroyuki Sanada as Kamijo Kenkichi
* Scott Bachicha as Sam
* Tatsuya Nakadai as Rentaro Katsu
* Naoto Takenaka as Tommy, Tamejiro
* Ittoku Kishibe as John Manjiro
* Richard Nason as Hutch
* Angelique Midthunder as Nantai (as Angelique Roehm)
* Etsushi Takahashi as Kimura
* Jay Kerr as Hardy
* David Midthunder as Red Hair

==Plot==
The film starts off with an old man in the desert and two signs are shown; one that says East, the other says west.

Japan has sent a mission to San Francisco. In San Francisco, the Japanese are surprised by the American culture.

One American, Gus Taylor, and his gang steal all the gold from the mission and make their way in to the desert.  One of the samurai of the mission chases after the gang into the desert. He is joined by a young American boy, Sam, whose father was killed by the gang leader. The group picks up a variety of people along the way to New Mexico.

The Japanese and the Americans on the trip share parts of their own cultures with each other. The group of vigilantes eventually makes it to New Mexico and finds the gang. They take back their stolen gold and return to San Francisco.

==References==
 

==External links==
*  
* 

 

 
 
 
 
 
 
 
 


 
 