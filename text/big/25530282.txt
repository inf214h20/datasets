Eternal Theater
{{Infobox film
| name           = Eternal Theater
| image          = Ultimatum poster.jpg
| image size     = 
| caption        = 
| director       = Daniel Knudsen
| producer       = Mark A. Knudsen Michelle Knudsen
| writer         = 
| narrator       = George W. Sarris
| starring       = George W. Sarris Daniel Knudsen
| music          = Samuel Joshua
| cinematography = 
| editing        = 
| studio         = Crystal Creek Media
| distributor    = 
| released       =  
| runtime        = 38 minutes
| country        = United States English
| budget         = 
| gross          =
}} Christian documentary film directed by Daniel Knudsen. It was released to DVD on April 2, 2010. The film, an overview of the events of the Bible, was produced by Crystal Creek Media.  The film is partially computer-animated and it was formerly titled Ultimatum.

==Plot== Genesis to the Gospel is presented.

==Cast==
Because Eternal Theater is presented in a narrative style, the cast has only two members.

* George Sarris as the Narrator
* Daniel Knudsen as the Timekeeper

==See also==
* Christian film
* Crystal Creek Media

==References==
 

==External links==
*  
*  

 
 
 
 
 
 


 