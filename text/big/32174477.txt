Heart of a Siren
 
{{infobox film
| name           = Heart of a Siren
| image          =
| imagesize      =
| caption        =
| director       = Phil Rosen
| producer       = Associated Pictures Corporation
| based on       =  
| writer         = Frederick and Fanny Hatton (adaptation)  Arthur Soerl (scenario)
| starring       = Barbara La Marr
| music          =
| cinematography = Rudolph Berquist
| editing        = Elmer J. McGovern
| distributor    = First National Pictures
| released       = March 15, 1925
| runtime        = 7 reel#Motion picture terminology|reels; 6,700 feet
| country        = United States
| language       = Silent film English intertitles
}} silent film drama/romance directed by Phil Rosen and distributed by First National Pictures. Barbara La Marr is the star of the picture which is a surviving film and one of La Marrs final movies. The copy that survives belongs to the UCLA Film and Television Archive.  

==Cast==
* Barbara La Marr - Isabella Echevaria
* Conway Tearle - Gerald Rexford
* Harry T. Morey - John Strong
* Paul Doucet - Mario Gonzalez
* Benjamin F. Finney, Jr. - George Drew
* Florence Auer - Lisette - Isabellas Maid
* Ida Darling - Mrs. Rexford, Duchess of Chatham
* William Ricciardi - Emilio, Isabellas Uncle
* Clifton Webb - Maxim
* Florence Billings - Lady Louise Calvert
* Michael Rayle - Pierre (billed Mike Rayle)
* Katherine Sullivan  - Marie

==References==
 

==External links==
* 
* 

 
 
 
 
 
 

 
 