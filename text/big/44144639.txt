O Kadhal Kanmani
 
{{Infobox film
| name           = O Kadhal Kanmani
| image          = Okay Kanmani film poster.jpg
| alt            =  
| caption        = Official poster
| director       = Mani Ratnam
| producer       = Mani Ratnam
| writer         = Mani Ratnam
| starring       = Dulquer Salmaan Nithya Menen 
| music          = A. R. Rahman
| cinematography = P. C. Sreeram
| editing        = A. Sreekar Prasad
| studio         = Madras Talkies
| distributor    = Studio Green   Sri Venkateswara Creations  
| released       =  
| runtime        = 138 minutes
| country        = India Tamil
| budget         = 
| gross          = 
}} Tamil romantic comedy film written, directed and produced by Mani Ratnam.  The film stars Dulquer Salmaan and Nithya Menen     as a young couple in a Live-in relationships in India|live-in relationship     and was said to be a "reflection of the modern mindset of urban India", dealing with issues such as marriage and traditional values.    The music was composed by A. R. Rahman. The film released on 17 April 2015.   

==Cast==
* Dulquer Salmaan as Adithya Varadarajan
* Nithya Menen  as Tara
* Prakash Raj as Ganapathy
* Leela Samson as Bhavani
* Baby Rakshana
* Prabhu Lakshman
* Ramya Subramaniam as Ananya Kanika (cameo appearance)
* B.V. Doshi (cameo appearance)   
* Padam Bhola (cameo appearance)

==Production==

===Development=== Tamil and traditional culture live sound.  Anand Krishnamoorthi was the post production Sound Designer on the film.  

===Casting=== Mohenjo Daro Parvathy was Kanika shot for a day with the team and confirmed her part a cameo appearance. P. C. Sreeram was signed on to be the films cinematographer, collaborating with after fifteen years.  Eka Lakhani had designed the costumes for the main cast. 

The character Aadi (played by Dulquer Salmaan) is about a young urban man. For this role, Ratnam was looking for someone real and belonging to an upper class family, but not a hero. Ratnam first met Salmaan at an audio release function and felt that he was the right choice to play the character Aadi, although he hadnt seen any of the latters films. However, Ratnam had watched Nithya Menens previous films where she played small roles.  Ratnam was quoted saying, "I found something alive about her, something real and perfectly beautiful about her. Nithya carries an individuality and is not just a glamorous girl. Like her character in the film, Tara, she has a mind of her own."  The supporting cast includes Prakash Raj and Leela Samson as onscreen husband and wife.  The latter makes her cinematic acting debut through this film. 

===Filming=== Indian Institute of Management and CEPT University both in Ahmedabad, the following month.     Filming was completed by December 2014.  

==Music==
 
 Carnatic themes.  Since Leela Samson plays the role of an old Carnatic singer in the film, there is a tinge of Carnatic music in the film.  Ratnam wanted an original soundtrack from Rahman that is contemporary as well as trendy.  Rahmans son, A. R. Ameen sang the track "Maula Wa Sallim" in Arabic. A first preview of the song "Mental Manadhil (Male version)" was released on March 17,  while the full song was released as a single on March 17 to a very positive critical response.  The soundtrack album was released by Sony Music India on April 4, 2015. 

==Promotion and release==

On April 8, 2015, the film was awarded a "U/A" certificate by the Central Board of Film Certification because of adult situations depicted in the film. The makers then sent the film to the revising committee to acquire a "U" (Universal) certificate but the status remained unchanged. 

The film was distributed by Studio Green  in 350 theatres in Tamil Nadu   whereas producer Dil Rajus Sri Venkateswara Creations distributed the Telugu version.  It was Dil Raju who suggested Mani to name the Telugu version as Ok Bangaram. 

The first poster of the Tamil and the dubbed Telugu version of the film were released on February 14, 2015.  The teaser titled "OK Kanmani - A Glimpse" was released on February 27.  The first trailer was released on March 1. 

== Reception ==

===Critical reception=== International Business Times India who called the movie as "the directors best in recent years" in their review roundup.   
  gave the film 3.25 stars out of 5 stating, "Okay, moral of the story - One fine morning, Mani Ratnam got up and decided to make something special for all young people out there. He then makes OK Kanmani, and in style".  Critics based at Sify stated, "The premise is simple, the story is focused. Technically OK Kanmani is superior as both AR Rahman and PC Sreeram have given their career best work for Mani Ratnam as the master craftsman himself is in tremendous form. O Kadhal Kanmani is definitely above a notch compared to the mediocre cinema we have been subjected to lately."  Sudhish Kamath wrote, "OK Kanmani is unfortunately that Uncle who makes you believe that marriage is the answer to your conflict of living in without any expectations from each other."  Haricharan Pudipeddi for Hindustan Times said that the film is a refreshing take on romance and relationship. This is his best work in years and this magical spell wont be forgotten easily. By throwing the spotlight on modern Indias idea of romance, Ratnam has also succeeded in making us root for an older couple madly in love in O Kadhal Kanmani."  Karthik Keramalu of IBN Live stated that the film is a love letter to the audience. He went on to call the film as, Mani Ratnams timely reflection of our society which mustnt be missed.    He rated the film 5 out of 5.  R. S. Prakash of Bangalore Mirror wrote, "Mani delves into the plot of live-in relationship, but he has chosen not to go overboard, given the sensitivity of the subject, especially in his part of the world. The screenplay, though a bit implausible at places, flows steadily, carrying the touches of the master filmmaker."  Writing for The Times of India M. Suganth said, "The filmmaker shows that his touch is in tact and he can still make a romance come alive on screen." The film was rated 3.5 out of 5 by him.  Indo-Asian News Service stated, "By throwing the spotlight on modern India’s idea of romance, Ratnam has also succeeded in making us root for an older couple madly in love in “O Kadhal Kanmani”."  S. Saraswathi of Rediff.com|Rediff gave the film 3.5 out of 5 stars claiming that it has, "directors trademark, unique narrative style, realistic characters, and excellent performances coupled with great music and stunning visuals". 

==References==
 

==External links==
*  
 
 

 
 
 
 
 
 
 
 