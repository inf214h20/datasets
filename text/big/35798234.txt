Tannenberg (film)
{{Infobox film
| name           = Tannenberg 
| image          = 
| image_size     = 
| caption        = 
| director       = Heinz Paul
| producer       = Lazar Wechsler
| writer         = Paul Oskar Höcker Georg von Viebahn Heinz Paul
| narrator       = 
| starring       = Hans Stüwe Käthe Haack Jutta Sauer Hertha von Walther
| music          = Ernst Erich Buder
| editing        = 
| cinematography = Georg Bruckbauer Viktor Gluck
| studio         = Paul-Filmproduktion Praesens-Film UFA
| released       = 8 September 1932
| runtime        = 105 min.
| country        = Germany Switzerland
| language       = German
| budget         =  
| gross          =
}} German war film directed by Heinz Paul and starring Hans Stüwe, Käthe Haack and Jutta Sauer. The film is based on the 1914 Battle of Tannenberg during the First World War.  It focuses on a German landowner Captan von Arndt and his family.

==Production== shot on Russian commanders respectfully.  It was due to be released on 26 August 1932, the eighteenth anniversary of the battle, but was delayed by the censors acting on a request from the German President Paul von Hindenburg who was unhappy with his portrayal in the film and the première was pushed back until certain scenes had been cut. 

==Cast==
* Hans Stüwe - Gutsbesitzer Rittmeister von Arndt
* Käthe Haack - Grete von Arndt
* Jutta Sauer - Inge von Arndt
* Hertha von Walther -  Schwägerin Sonja
* Erika Dannhoff - Schwägerin Lita
* Hannelore Benzinger - Lisbeth, Arndt, Magd
* Karl Klöckner - Gutsverwalter Puchheiten
* Franziska Kinz - Gutsfrau Frau Puchheiten
* Rudolf Klicks - Fritz Puchheiten
* Alfred Döderlein - Leutnant Schmidz
* Wolfgang Staudte - Husar Franke Karl Körner - Paul von Hindenburg
* Henry Pleß - Generalmajor Ludendorff
* Hans Mühlhofer - Oberstleutnant Hoffmann
* Friedrich Franz Stampe - Generalmajor Grünert
* Alfred Gerasch - Generalmajor Graf von Waldensee
*  Graf Schönborn - Hauptmann Fleischmann von Theißruck
* Edgar Boltz - General von Scholz
* Georg H. Schnell - General der Kavallerie Shilinski General der Kavallerie Samsonow
* Carl Auen - Generalmajor Postowski
* Georg Schmieter - Oberst Gerwe
* Aruth Wartan - Stabstrompeter Kupschik
* Fritz Arno Wagner - Militärattaché Oberst Knox
* Ernst Pröckl - General der Artillerie Martos
* Erwin Suttner - General der Infanterie Kljujew
* Fritz Alberti - Generalleutnant Mingin
* Valy Arnheim - Oberstleutnant Fjedorow
* Viktor de Kowa - Rittmeister Fürst Wolgoff
* Friedrich Ettel - Kosakenwachtmeister

==References==
 

==Bibliography==
* Kester, Bernadette. Film Front Weimar: Representations of the First World War in German films of the Weimar Period (1919-1933). Amsterdam University Press, 2003.
* Von der Goltz, Anna. Hindenburg: Power, Myth, and the Rise of the Nazis. Oxford University Press, 2009.

==External links==
* 

 

 
 
 
 
 
 
 