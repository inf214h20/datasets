King of Devil's Island
 
{{Infobox film
| name           = King of Devils Island
| image          = King of Devils Island.jpg
| director       = Marius Holst
| producer       = Karin Julsrud
| writer         = Dennis Magnusson
| starring       =  
| music          = Johan Söderqvist
| cinematography = John Andreas Andersen
| editing        = Michal Leszczylowski
| distributor    = 
| released       =  
| runtime        = 115 min.
| country        = Norway
| language       = Norwegian, Swedish
| budget         = 54 millioner kroner
}}
King of Devils Island ( ) is a 2010 Norwegian film directed by   2011. The story is based on true events that occurred at Bastøy Prison in Norway.   Retrieved 2 February 2011 

Shooting for King of Devils Island took place in Estonia.

==Plot== 
At the Bastøy prison for youths, the newest arrival, Erling "C19," becomes fast friends with Olav ("C1"). Under the rule of Håkon, Erling comes to terms with the harsh winter, the mistreatment by the staff, and the hated Housefather Bråthen. Later, Olav sees Bråthen molesting "C5", Ivar, a timid, shy boy. When he raises this to the Governor, the boys are severely punished and C5 is reassigned far away from Bråthen. However, while the rest of the boys are chopping wood, Ivar drowns himself in the freezing Norwegian waters. Bråthen is apparently fired, to the overwhelming delight of the other boys. Olav is pardoned and is planning to depart, but, on the way to the departing boat, walks past Bråthen, who had been sent away on a shopping trip until Olav, the only witness to Ivars rape, was away from Bastøy. Olav and Erling attack Bråthen and are locked in the freezing solitary area. They are freed by Bjern, one of the few Bastøy boys who has remained at the island as a caretaker. Olav again pursues Bråthen, and the attack inspires a mass uprising. The boys ransack the prison, and drive away the staff. Bråthen is repeatedly hung and beaten, and the barn he is in is set ablaze, but Erling drags him to safety. The army is brought in and violently puts down the uprising. Olav and Erling manage to escape across a frozen fjord back to the mainland, but Erling falls through a gap in the ice close to shore and quickly freezes to death. 

Olav is shown later, older, as a member of a ships crew, watching Bastøy pass by the side of his ship.

==Cast==
* Stellan Skarsgård as Håkon
* Benjamin Helstad as Erling "C19"
* Trond Nilssen as Olav "C1"
* Kristoffer Joner as Bråthen
* Ellen Dorrit Petersen as Astrid
* Magnus Langlete as Ivar "C5"

==Historical background== Oslo fjord south of Horten municipality in the county of Vestfold Norway. The Norwegian government purchased the island in 1898 for 95,000 kroner, and the reformatory opened in 1900.

See Bastøy Prison for the real events which the film fictionalizes.

==References==
 

==External links==
*  
*   at BeyondHollywood.com

 

 
 
 
 
 
 
 
 