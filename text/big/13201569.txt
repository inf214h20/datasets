The Great Adventures of Wild Bill Hickok
{{Infobox film
| name           = The Great Adventures of Wild Bill Hickok
| image          =
| image_size     =
| caption        =
| director       = Sam Nelson Mack V. Wright
| producer       = Jack Fier Harry S. Webb (associate producer)
| writer         = John Peere Miles George Rosener George Arthur Durlam Dallas M. Fitzgerald Tom Gibson Charles Arthur Powell
| narrator       = Bill Elliott (as Gordon Elliott) Monte Blue Carole Wayne Frankie Darro Dickie Jones
| music          =
| cinematography = Benjamin H. Kline George Meehan
| editing        = Richard Fantl
| distributor    = Columbia Pictures
| released       =  
| runtime        = 15 chapters
| country        = United States
| language       = English
| budget         =
| gross          =
}}
 Columbia Serial movie serial. western serial.  The serial was the first from a new production company, the previous three serials had been produced by Weiss Brothers.

==Plot==
Wild Bill Hickok, United States Marshals Service|U.S. Marshal in Abilene, Kansas, is sent to stop the mysterious "Phantom Riders" from disrupting the cattle drives across the Chisholm Trail and construction of a new railroad.
 

==Cast== Bill Elliott as Wild Bill Hickok, United States Marshals Service|U.S. Marshal.  This serial was the source of actor Gordon Nances subsequent screenname "Wild Bill" Elliot.  He had previous worked under the name "Gordon Elliott".   The Great Adventures of Wild Bill Hickok was Wild Bill Elliots first starring role.  In addition to his screenname, he gained such trademarks as buckskins, reversed holsters and the catchphrase "Im a peaceable man," from this serial. {{cite book
 | last = Stedman
 | first = Raymond William
 | title = Serials: Suspense and Drama By Installment
 | origyear = 1971
 | publisher = University of Oklahoma Press
 | isbn = 978-0-8061-0927-5
 | pages = 121
 | chapter = 5. Shazam and Good-by
 }}  {{cite book
 | last = Harmon
 | first = Jim
 |author2=Donald F. Glut 
 | authorlink = Jim Harmon
 | title = The Great Movie Serials: Their Sound and Fury
 | origyear = 1973
 | publisher = Routledge
 | isbn = 978-0-7130-0097-9
 | pages = 67
 | chapter = Science Fiction/Westerns "Drop That Zap Gun, Hombre"
 }} 
*Monte Blue as Mr Cameron
*Carole Wayne as Ruth Cameron
*Frankie Darro as Jerry/Little Brave Heart
*Dickie Jones as Buddy
*Sammy McKim as Boots
*Kermit Maynard as Kit Lawson, Army scout
*Roscoe Ates as Oscar Snake-Eyes Smith
*Monte Collins as Danny, printer
*Reed Hadley as Jim Blakely
*Chief Thundercloud as Chief Gray Eagle
*Ray Mala as Little Elk Robert Fiske as Morrell, villain and leader of the Phantom Raiders
*Walter Wills as Joshua Bruce
*J.P. McGowan as Scudder, trail Leader Eddie Waller as Stone

==Production==
===Stunts===
*Gene Alsace
*Chuck Hamilton
*Ted Mapes
*Carl Mathews
*Kermit Maynard Tom Steele Francis Walker

==Critical reception==
The Motion Picture Herald called this serial "a compliment to its title." 

==Influence==
The name of Wild Bill Hickok stuck with Bill Elliot so much that Columbia made a series of Hickok westerns with him. {{cite book
 | last = Cline
 | first = William C.
 | title = In the Nick of Time
 | origyear = 1984
 | publisher = McFarland & Company, Inc.
 | isbn = 0-7864-0471-X
 | pages = 45
 | chapter = 3. The Six Faces of Adventure
 }} 

==Chapter titles==
# The Law of the Gun
# Stampede
# Blazing Terror
# Mystery Canyon
# Flaming Brands
# The Apache Killer
# Prowling Wolves
# The Pit
# Ambush
# Savage Vengeance
# Burning Waters
# Desperation
# Phantom Bullets
# The Lure
# Trails End
 Source:  {{cite book
 | last = Cline
 | first = William C.
 | title = In the Nick of Time
 | origyear = 1984
 | publisher = McFarland & Company, Inc.
 | isbn = 0-7864-0471-X
 | pages = 222
 | chapter = Filmography
 }} 

==References==
 

==External links==
* 
* 

 

 

 
 
 
 
 
 
 
 