Dames Ahoy!
{{infobox film
| title          = Dames Ahoy!
| image          =
| imagesize      =
| caption        =
| director       = William James Craft
| producer       = Carl Laemmle
| writer         = Sherman L. Lowe (story) Albert DeMond (script, titles) Matt Taylor (script, titles)
| starring       = Glenn Tryon Otis Harlan
| music          =
| cinematography = Alan Jones
| editing        = Harry W. Lieb
| distributor    = Universal Pictures
| released       = February 9, 1930
| runtime        = 64 minutes
| country        = USA
| language       = English (silent version: English intertitles)
}}
Dames Ahoy! is a 1930 early talkie film produced and distributed by Universal Pictures, and released in a silent version as well. The film was directed by William James Craft and starred Glenn Tryon and Otis Harlan. 

==Cast==
*Glenn Tryon - Jimmy Chase
*Helen Wright - Mabel McGuire
*Otis Harlan - Bill Jones
*Eddie Gribbon - Mac Dougal
*Gertrude Astor - The Blonde
*Alice Belcher - uncredited

==References==
 

==External links==
* 
* 
* 

 
 
 
 
 


 