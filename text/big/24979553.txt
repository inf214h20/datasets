I Loved an Armchair
{{Infobox film
| name           = Agapitsa mia polythrona
| image          =
| image size     =
| caption        =
| director       = Dinos Dimopoulos
| producer       =
| writer         = Lakis Mihailides
| starring       = Kostas Voutsas Eleni Erimou Giorgos Papazisis Stavros Xenidis Athinodoros Proussalis Babis Anthopoulos Giorgos Tzifos Katerina Gioulaki Giorgos Moschidis Maria Foka Nikitas Platis Kostas Palios
| cinematography =
| photographer   =
| music          = Giorgos Hadjinassiou
| editing        =
| distributor    = Karagiannis-Karatzopoulos
| released       =  
| runtime        = 89 minutes
| country        = Greece
| language       = Greek
}} The Twelve Chairs directed by Mel Brooks.  The music director in the movie was Giorgos Hadjinassiou.

The film is 89 minutes long and tells the story of a poor young man who was forced to sell four chairs he had inherited from his aunt and then learned that one of them contained hidden jewelry.

==Cast==
*Kostas Voutsas as Grigoris Karouzos
*Eleni Erimou as Kaiti
*Giorgos Papazisis as Triandafilos
*Stavros Xenidis as a psychiatrist
*Athinodoros Prousalis
*Babis Anthopoulos as a director
*Giorgos Tzifos as an assistant director
*Katerina Gioulaki as Zeta
*Giorgos Moschidis as Miltos Karnezis
*Maria Foka as aunt Vangelitsa
*Nikitas Platis as Dimitrios Nikolaou
*Kostas Palios as a judge president
*Stelios Christoforidis
*Grigoris Dekakis
*Kostas Fatouros
*Kostas Fyssoun
*Thanos Grammenos
*Ilias Kapetanidis
*Giorgos Messalas
*Panos Nikolakopoulos
*Nikos Paschalidis
*Nick Spyridonakos
*Alekos Zartaloudis

==External links==
*  

 
 
 
 

 