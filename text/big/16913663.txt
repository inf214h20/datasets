Constantine's Sword (film)
{{Infobox film
| name           = Constantines Sword
| image          = 
| image size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Oren Jacoby
| producer       = Mick Garris Robb Idels John Landis
| writer         = Oren Jacoby James Carroll
| screenplay     = 
| story          = 
| narrator       = Amanda Pays
| starring       = 
| music          = 
| cinematography = Robert Richman
| editing        = Kate Hirson
| studio         = 
| distributor    = 
| released       =  
| runtime        = 93 minutes
| country        = United States
| language       = 
| budget         = 
| gross          = 
}}
 
James Carrolls Constantines Sword, or Constantines Sword, is a 2007 historical documentary film on the relationship between the Catholic Church and Jewish people|Jews. Directed and produced by Academy Award|Oscar-nominated filmmaker Oren Jacoby, the film is inspired by former priest James P. Carrolls 2001 book Constantines Sword.  

==Synopsis== Joseph Carroll) was a famous Air Force general, implies that there has been a relationship between religiously inspired violence and war, beginning with the adoption of Christianity by the Roman Emperor Constantine I in 312 AD. Constantine was convinced that he had won a battle because he had followed the instructions of a vision, to inscribe a sign of the cross (the Labarum) on the shields of his soldiers. In Carrolls view, this event marked the beginning of an unholy alliance between the military and the Church.
 evangelical anti-Judaism, and invokes the cross as a symbol of the long history of Christian xenophobic violence against Jews and non-Christians,   from the Crusades, through the Roman Inquisition and the creation of the Jewish Roman ghetto|ghetto, to the Holocaust. Carroll also charges that there is an ongoing evangelical infiltration of the U.S. military, and that this has had negative consequences for U.S. foreign policy. The films final chapter, "No war is holy", concludes with views of military cemeteries as Aaron Neville sings "With God On Our Side".

==Technical details==
*95 minutes Italian and Yiddish
*Cast/Featuring: Liev Schreiber, pastor Ted Haggard, Philip Bosco, Natasha Richardson, Eli Wallach
*Director: Oren Jacoby 
*Producers: Oren Jacoby, James Carroll, Michael Solomon, Betsy West.
*Supervising producer: Elgin Smith
*Screenwriters: James Carroll, Oren Jacoby 
*Production company: Storyville Films
*Studio: First Run Features

==See also==
*Constantines Sword (2001) book by James P. Carroll

== References ==

 

==External links==
* 
* 
* 
* 
* 
*  from First Things
*   from the  

 
 
 
 
 
 
 