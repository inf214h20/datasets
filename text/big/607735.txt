Cocktail (1988 film)
{{Infobox film
| name = Cocktail
| image = Cocktail 1988.jpg
| caption = Theatrical release poster
| director = Roger Donaldson
| producer = Ted Field Robert W. Cort
| screenplay = Heywood Gould
| based on = Cocktail (book)|Cocktail by Heywood Gould
| starring = Tom Cruise Bryan Brown Elisabeth Shue
| music = J. Peter Robinson
| cinematography = Dean Semler
| editing = Neil Travis
| studio = Touchstone Pictures Silver Screen Partners III Interscope Communications Buena Vista Pictures
| released =  
| runtime = 103 minutes
| country = United States English
| budget = $20 million 
| gross = $171,504,781 
}} romantic comedy-drama film directed by Roger Donaldson and written by Heywood Gould, whose screenplay was based on his book of the same name. The film tells the story of a young New York City business student, Brian Flanagan, who takes up bartending in order to make ends meet.

The film stars Tom Cruise as Brian Flanagan, Bryan Brown as Doug Coughlin, and Elisabeth Shue as Jordan Mooney. Released by Touchstone Pictures, the film features an original music score composed by J. Peter Robinson.

==Plot==
After leaving the United States Army and moving to New York City, Brian Flanagan (Tom Cruise) gets a part-time job as a bartender at night while studying for a business degree. Over time, he learns the tricks of the trade, including Flair bartending|flairing, from his boss/mentor Doug Coughlin (Bryan Brown). Brian and Doug soon become very close; Doug readily assumes a mentor role over the young and naive Brian, and rains advice and opinions down upon him. His advice takes a familiar structure, as he usually begins most of them with "Coughlins Law".

While Brian has high personal aspirations, Doug is leery of the notion of starting their own bar together. Doug intends to call his bar "Cocktails & Dreams".

Eventually, Brian and Dougs bar-tending act becomes popular and they end up working at a trendy nightclub catering to New Yorks wealthy and elite. As their popularity rises, Brian becomes the focus of attention from a sultry brunette named Coral (Gina Gershon) quickly, their friendship becomes sexual. Doug is alarmed that Coral is coming between their work partnership and bets him Coral will leave him by weeks end, essentially doubting there is anything special about the seemingly perfect relationship shared between the two. Unknown to Brian, Doug lies to Coral about secrets being shared by Brian about her, and secures his bet having a passionate with Coral while at work in the bar in front of Brian. Coral then tells Brian he should never have discussed their love life with Doug and so he is dumped. Brian is very upset and fights with Doug at work, essentially ending his informal partnership with Doug.

The film fast-forwards three years. Brian, taking advice from his former girlfriend, takes a job in Jamaica as a bartender to raise money for his own place. He finds a romantic partner in Jordan Mooney (Elisabeth Shue). Jordan is an aspiring artist and waitress in New York. She and Brian spend romantic times together, playing in the water, before making passionate love on the beach by a campfire. Prior to this, however, Doug has shown up in Jamaica, now married to Kerry (Kelly Lynch), a wealthy woman who openly flirts with other men and wears Tarzan-like bikinis.

Doug quickly asserts himself and bets Brian to show how he can pick up a new customer named Bonnie (Lisa Banes), a wealthy older woman. Jordan catches Brian without being seen and devastated, takes an overnight plane back home.

With Jordan flying back to New York, Brian decides to upstage Doug and return to New York with the Cougar (slang)|cougar-esque Bonnie under the auspices that he will be placed high up in her company due to their romantic attachment. Brian becomes impatient, as the pay-off is too slow. They have a blow-up during an art exhibition where Brian gets into a fight with the artist. As they cut ties, Brian, displaying wisdom one can only assume has been gleaned from his former mentor, states: "All things end badly. Otherwise they wouldnt end!"

Brian then seeks out Jordan. Much to his surprise, Brian learns that she is pregnant with his child. He embarks on a journey to win over the independent Jordan and prove to her that, despite being just a lowly bartender, he would make a worthy father.
 Park Avenue penthouse to speak with Jordan. Unhappy with the situation, Jordans father, Richard (Laurence Luckinbill), attempts to buy Brian off. Brian is forced to decide between the money offered which would surely help him get his bar started versus a life with Jordan and his child; Brian refuses the money. Jordan keeps her distance, not wanting to be hurt again.

Brian meets up with Doug. Despite the outward appearance of wealth, Doug confides that his wifes money is nearly gone, lost in the commodities market. Doug is despondent, unwilling to confess to his bride the precarious position they are in; Brian is completely shocked. Later on, Kerry makes Brian take her home when Doug is too drunk to do so, and forces him to walk her to her apartment. She tells him that he is the only person Doug respects and wants to discuss with him Dougs problems. However once inside, she attempts to seduce him by kissing him but Brian refuses out of respect for his friendship with Doug. Kerry gets angry at being rejected and reveals that she cannot endure sexual monogamy for the rest of her life. Brian then leaves with Kerry calling him a coward.
 Baccarat crystal glass. After the funeral, Kerry sends Brian a letter left for him by Doug, which is revealed to be Dougs suicide note in which he explains why he did what he did. Brian cries after reading the letter, realizing that Doug killed himself because he realized that his life was a sham.

Now reeling from the misfortune of the stiff-arm from Jordan and losing his best friend to suicide, he goes to Richards home (where Jordan is staying) and begs her forgiveness. He further promises to take good care of her and their unborn child. Brian has a brief scuffle with Richards staff, and then takes the willing Jordan by the hand and heads for the door. They leave together, finally, as a couple and future parents. Richard pledges not to lend a dime to the fledgling couple.

Using the advice of the other mentor in his life, his Uncle Pat (Ron Dean), Brian is able to finally achieve his lifelong goal; he opens a neighborhood bar called "Flanagans Cocktails & Dreams". Brian and Jordan have their wedding reception at the new bar while Jordan is visibly pregnant. Just before the credits roll, Jordan reveals she is pregnant with twins. Brian offers free drinks to celebrate, much to his Uncle Pats chagrin.

==Reception==
Cocktail was a financial success, earning $78.2 million at the box office, and $93.3 million globally to a total of $171.5 million worldwide.    
 Worst Picture Worst Actor Worst Director. John Wilsons book The Official Razzie Movie Guide as one of The 100 Most Enjoyably Bad Movies Ever Made. 

Vincent Canby of The New York Times gave Cocktail a negative review, calling it "an upscale, utterly brainless variation on those efficient old B-movies of the 1930s and 40s about the lives, loves and skills of coal miners, sand hogs and telephone linemen, among others."  Roger Ebert of the Chicago Sun-Times was also critical of the film, explaining that "the more you think about what really happens in Cocktail, the more you realize how empty and fabricated it really is." 

==Soundtrack==
{{Infobox album  
| Name   = Cocktail (Original Motion Picture Soundtrack)
| Type   = Soundtrack
| Artist = Various Artists
| Cover  = 
| Released = August 2, 1988 (US) rock
| Length = 35:27
| Label  = Elektra Records
}}
{{Album ratings
|rev1= 
|rev1score= 
}}
{{tracklist
| music_credits = yes Wild Again Starship
| length1 = 4:43
| title2  = Powerful Stuff
| music2  = The Fabulous Thunderbirds             
| length2 = 4:48
| title3  = Since When
| music3  = Robbie Nevil
| length3 = 4:02
| title4  = Dont Worry, Be Happy
| music4  = Bobby McFerrin
| length4 = 4:48
| title5  = Hippy Hippy Shake
| music5  = The Georgia Satellites
| length5 = 1:45 Kokomo
| music6  = The Beach Boys
| length6 = 3:34
| title7  = Rave On! John Cougar Mellencamp
| length7 = 3:13
| title8  = All Shook Up
| music8  = Ry Cooder
| length8 = 3:29
| title9  = Oh, I Love You So Preston Smith
| length9 = 2:42 Tutti Frutti
| music10 = Little Richard
| length10 = 2:23
}}

Additional tracks featured in the film include: Addicted to Robert Palmer
*"Shelter of Your Love" – Jimmy Cliff
*"This Magic Moment" – Leroy Gibbons When Will I Be Loved" – The Everly Brothers (uncredited)
*"That Hypnotizin Boogie" - David Wilcox

===Chart positions===

====Weekly====
{| class="wikitable plainrowheaders sortable"
|-
!Chart (1988–89)
!Peak position
|- Australian Recording Australian Albums Chart  1
|- Austrian Albums Chart  3
|- RPM (magazine)|Canadian Albums Chart  1
|- Dutch Albums Chart  22
|- German Albums Chart  4
|- Official New New Zealand Album Charts  2
|- Swedish Albums Chart  3
|- Swiss Music Swiss Album Charts  3
|- US Billboard 200|Billboard 200  2
|}

====Year-end====
{| class="wikitable plainrowheaders sortable"
|-
!Chart (1989)
!Peak position
|- Australian Albums Chart  19
|- Austrian Albums Chart  26
|- Canadian Albums Chart  31
|- Swiss Albums Chart  27
|}

==In other media==
* In The King of Queens episode titled "Pour Judgment", Doug talks about how the movie took the world by storm in 1988 and inspired him to try and bartend.
* The Family Guy episode, "Excellence in Broadcasting", makes a reference to the negative reaction of the film.
* In Season 4, episode 13 of How I Met Your Mother, titled “Three Days of Snow”, Ted and Barney celebrate cocktail mixture while the song "Kokomo" is played—a reference to the movie.
* In the film Grandmas Boy (2006 film)|Grandmas Boy, Dante makes a reference about Tom Cruise and this movie while serving shots.
* In the novel American Psycho by Bret Easton Ellis, protagonist Patrick Bateman shares an elevator with Cruise and compliments his performance in the movie Bartender. Cruise corrects him by saying the movies title is Cocktail. 
* There is a nightclub in Surfers Paradise, Australia named "Cocktails and Dreams" after the movie. 

==References==
 

==External links==
 
* 
* 
* 
* 

 
 
{{Succession box
| title=Golden Raspberry Award for Worst Picture 
| years=9th Golden Raspberry Awards
| before=Leonard Part 6
| after= 
}}
{{Succession box
| before = Summer 89 by Various artists ARIA Albums Chart Number-one albums of 1989 (Australia)|number-one album
| years = January 9 – February 12, 1989
| after = Traveling Wilburys Vol. 1  by Traveling Wilburys
}}
 

 
 
 
 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 