Casino Jack and the United States of Money
 
{{Infobox film
| name = Casino Jack and the United States of Money
| image = Casino Jack and the United States of Money.jpg
| caption =
| director = Alex Gibney
| producer = Bill Banowsky Mark Cuban Benjamin Goldhirsh Jeff Skoll Todd Wagner Diane Weyermann
| writer = Alex Gibney
| music = David Robbins
| cinematography = Maryse Alberti
| editing = Alison Ellwood
| studio = Jigsaw Productions Participant Media
| distributor = Magnolia Pictures
| released =  
| runtime = 118 minutes
| country = United States
| language = English
| budget =
| gross = $176,865
}}
Casino Jack and the United States of Money is a 2010 documentary film directed by Alex Gibney.

==Synopsis== con man corruption scandal Bush White House officials, United States House of Representatives|Rep. Bob Ney, and nine other lobbyists and congressional staffers. Abramoff was convicted of fraud, Conspiracy (crime)|conspiracy, and tax evasion in 2006  and of trading expensive gifts, meals and sports trips in exchange for political favors.  As of December 2010 Abramoff has completed his prison sentence. 

==Contributors==
*Juan Babauta – Governor, CNMI (2002–2006)
*Jim Benedetto – Federal Labor Ombudsman, CNMI  
*Pamela Brown – Former Attorney General, CNMI
*Tom DeLay – U.S. Congressman (R-TX) (1984–2006)
*Nina Easton – Author, Gang of Five Peter Fitzgerald – U.S. Senator (R-IL) (1999–2005) The Wrecking Crew
*David Grosh – Former Rehoboth Beach lifeguard
*Carlos Hisa – Lt. Governor, Tigua Tribe of El Paso, Texas
*Robert G. Kaiser – Author, So Damn Much Money
*Adam Kidan – Former owner, SunCruz Casinos Shawn Martin – Reporter, American Press of Lake Charles, Louisiana George Miller (D-CA)
*Bob Ney – U.S. Congressman, (R-OH) (1995–2006)
*Ron Platt – Former Greenberg Traurig lobbyist
*Tom Rodgers – Lobbyist, Carlyle Consulting
*Rep. Dana Rohrabacher (R-CA)
*Khaled Saffuri – Public affairs consultant, Meridian Strategies
*Susan Schmidt – Former reporter, The Washington Post
*David Sickey – Vice Chairman, Coushatta Tribe of Louisiana
*Melanie Sloan – Citizens for Responsibility and Ethics in Washington (CREW)
* 
*Froilan Tenorio – Governor, CNMI (1994–1998)
*Neil Volz – Former Chief of Staff to Rep. Bob Ney (R-OH)
*J. Michael Waller – Director, Institute of World Politics

==References==
 

==External links==
* 
*  
* 

 
 

 
 
 
 
 
 
 
 
 
 
 