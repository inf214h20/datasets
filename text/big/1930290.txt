Barbarella (film)
 Barbarella}}
 
{{Infobox film
| name           = Barbarella
| image          = Barbarella-french-film-poster.jpg
| image_size     = 225px
| alt            = 
| caption        = French theatrical poster
| director       = Roger Vadim
| producer       = Dino De Laurentiis
| writer         = {{Plainlist|
*Vittorio Bonicelli
*Clement Biddle Wood
* Brian Degas
* Tudor Gates
}}
| screenplay     = {{Plainlist|
* Terry Southern
*Roger Vadim
}}
| based on       =  
| starring       = {{Plainlist|
* Jane Fonda
* Ugo Tognazzi
* Anita Pallenberg
* Milo OShea
* Marcel Marceau Claude Dauphin
 
}}
| music          = Maurice Jarre  
| cinematography = Claude Renoir
| editing        = Victoria Mercanton
| studio         = Dino de Laurentiis Cinematografica Marianne Productions
| distributor    = Paramount Pictures
| released       =   }}
| runtime        = 98 minutes
| country        = France Italy
| language       = French English
| budget         = $9 million  or $4 million Vadims Barbarella, a challenging film: A free hand Employs improvisation
By Kimmis Hendrick. The Christian Science Monitor (1908-Current file)   14 Oct 1967: 6.  
| gross          = $2.5 million (US) "Big Rental Films of 1968", Variety, 8 January 1969 p 15. Please note this figure is a rental accruing to distributors.  878,015 admissions (France) }} French Barbarella (comics)|Barbarella comics. The film stars Jane Fonda in the title role and was directed by Roger Vadim, who was Fondas husband at the time. The film was not popular at its release, but received greater attention afterward with a 1977 re-release. It has since become a cult film.

==Plot==
In an unspecified future, Barbarella is assigned by the President of Earth to retrieve Doctor Durand Durand from the Tau Ceti region. Durand Durand is the inventor of the Positronic Ray, a weapon that Earth leaders fear will fall into the wrong hands. Barbarella crashes on the 16th planet of Tau Ceti and is soon knocked unconscious by two mysterious girls, who take Barbarella to the wreckage of a spaceship. Inside the wreckage, she is tied up and several children emerge from within the ship. They set out several dolls which have razor sharp teeth. As the dolls begin to bite her, Barbarella faints but is rescued by Mark Hand, the Catchman, who patrols the ice looking for errant children. While Hand takes her back to her ship, Barbarella offers to reward Mark and he suggests sex. She says that people on Earth no longer have penetrative intercourse but consume exaltation transference pills and press their palms together when their "psychocardiograms are in perfect harmony". Hand prefers the bed, and Barbarella agrees. Hands vessel makes long loops around Barbarellas crashed vessel while the two have sex, and when it finally comes to a stop, Barbarella is blissfully humming. After Hand repairs her ship, Barbarella departs and promises to return, agreeing that doing things the old-fashioned way is occasionally best.

Barbarellas ship burrows through the planet and comes out next to a vast labyrinth. Upon emerging from her ship, she is knocked unconscious by a rockslide. She is found by a blind angel named Pygar, who states he is the last of the ornithanthropes and has lost the will to fly. Barbarella discovers the labyrinth is a prison for people cast out of Sogo, the City of Night. Pygar introduces her to Professor Ping, who offers to repair her ship. Ping also notes that Pygar is capable of flight but merely lacks the will. After Pygar rescues her from the Black Guards, Barbarella has sex with him, and he regains his will to fly. Pygar flies Barbarella to Sogo and uses the weaponry she brought with her to destroy the citys guards. Sogo is a decadent city ruled over by the Great Tyrant and powered by a liquid essence of evil called the Mathmos. Barbarella is briefly separated from Pygar, and meets a one-eyed wench who saves her from being assaulted by two of Sogos residents. Barbarella soon reunites with Pygar and the two are taken by the Concierge to meet the Great Tyrant. Pygar is left to become the Great Tyrants plaything, while Barbarella is placed in a cage, to be pecked to death by birds.

Barbarella is rescued by Dildano, leader of the resistance. Barbarella eagerly offers to reward him, but he says he wants to experience sex the Earthling way. Dildano offers to help Barbarella find Durand Durand in exchange for her help in deposing the Great Tyrant. Barbarella is captured by the Concierge and placed inside the Excessive Machine, which the Concierge says will cause her to die of pleasure. Barbarella writhes in ecstasy, and the machine overloads, unable to keep up with her. Barbarella then discovers the Concierge is none other than Durand Durand, aged thirty years due to the Mathmos.

Durand Durand traps Barbarella, and, as he prepares to crown himself lord of Sogo, Dildano launches his revolution. Durand Durand uses his Positronic Ray to decimate the rebels.  The Great Tyrant then releases the Mathmos, which consumes all of Sogo and Durand Durand with it. Barbarella and the Great Tyrant are protected from the Mathmos by Barbarellas innate goodness. They emerge from the Mathmos to find Pygar. Pygar flies Barbarella and the Tyrant away from the Mathmos. When asked by Barbarella why he saved the Tyrant after everything she had done to him, Pygar responds, "An angel has no memory."

==Cast==
 
* Jane Fonda as Barbarella
* Ugo Tognazzi as Mark Hand
* Anita Pallenberg (dubbed by Joan Greenwood) as The Great Tyrant, Black Queen of Sogo
* Milo OShea as Durand Durand / Concierge
* Marcel Marceau as Professor Ping Claude Dauphin as President Dianthus of Earth
* David Hemmings as Dildano
* John Phillip Law as Pygar, the angel
* Fabienne Fabre as the tree woman

==Production==
===Development===
Producer Dino De Laurentiis invited Fonda to the project after Sophia Loren and Brigitte Bardot turned down the starring role.  Though Fonda also declined, Vadim convinced her to change her mind.  

Vadim was a fan of American comic strips such as Popeye and Peanuts. "I like the wild humour and impossible exaggeration of the comic strips," said Vadim. "I want to do something in that myself in my next film." And Vadim Created Jane Fonda
By THOMAS QUINN CURTISS. New York Times (1923-Current file)   16 Jan 1966: X15.  

"It is absolutely camp, sophisticated camp, the wildest of them all," said Fonda. Nice Work, If You Can Get It
Los Angeles Times (1923-Current File)   06 May 1966: c14.  

"In science fiction, technology is everything," said Vadim. "The characters are so boring - they have no psychology. I want to do this film as though I had arrived on a strange planet with my camera directly on my shoulder - as though I was a reporter doing a newsreel." 

"What interests me is the chance to escape totally from the morals of the 20th century and depict a new, futuristic morality," added Vadim. "Its a very romantic story, really. Barbarella has no sense of guilt about her body. I want to make something beautiful out of eroticism." Heres What Happened to Baby Jane
By GERALD JONAS. New York Times (1923-Current file)   22 Jan 1967: 91. 

"Vadim loves science fiction and hes gotten me interested," said Fonda. "In a way, cinema is the natural medium for it, but up to now the technical gimmicks have been treated as the raison detre of the science fiction film. As an actress, Im more concerned about the story, and the character." 

Vadim later elaborated:
 I can tell you all the things she wont be. She wont be a science fiction character, nor will she play Barbarella tongue in cheek. She is just a lovely, average girl with a terrific space record and a lovely body. I am not going to intellectualise her. Although there is going to be a bit of satire about our morals and our ethics, the picture is going to be more of a spectacle than a cerebral exercise for a few way out intellectuals. She is going to be an uninhibited girl, not being weight down by thousands and thousands of years of Puritan education. What Kind of Supergirl Will Jane Fonda Be as Barbarella?
ABA, MARIKA. Los Angeles Times (1923-Current File)   10 Sep 1967: c12.  
Fonda explained how she saw the character:
 The main thing about this role is to keep her innocent. You see, Barbarella is not a vamp and her sexuality is not measured by the rules of our society. She is not being promiscuous, but she follows the natural reaction of another type of upbringing.  She is not a so-called sexually liberated woman either. That would mean rebellion against something. She is different. She was born free.   

Her father, Henry Fonda, was the original choice for the President of Earth.

===Writing===
A large number of writers worked on the script, including Terry Southern. Southern later claimed "Vadim wasnt particularly interested in the script, but he was a lot of fun, with a discerning eye for the erotic, grotesque, and the absurd. And Jane Fonda was super in all regards." Lee Hill, "Terry Southern: The Ultra Hip", McGilligan, Patrick. Ed Backstory 3: Interviews with Screenwriters of the 60s Berkeley: University of California Press,  c1997 p 385 

Charles B. Griffith worked on the script uncredited; he said "they hired fourteen other writers" after Terry Southern "before they got to me. I didnt get credit because I was the last one."  He says he became involved because he was a friend of John Phillip Laws:
 I guess I rewrote about a quarter of the film that was shot, then reshot, and I added the concept that there had been thousands of years since violence existed, so that Barbarella was very clumsy all through the picture. She shoots herself in the foot and everything. It was pretty ludicrous. The stuff with Claude Dauphin and the suicide room were also part of my contribution to the film.   retrieved 22 June 2012  

===Shooting===
Production began in 1967 in Rome on 15 April 1967.  Poe Segment for Vadim
Martin, Betty. Los Angeles Times (1923-Current File)   06 Mar 1967: c28.    

Fashion designer Paco Rabanne was responsible for Fondas costumes.   Rabanne was influenced by the womens liberation movement and designed outfits in the style of metal armor, drawing influencing from an Indian philosophy that posited an age of iron. 

Marcel Marceau had his first speaking role in the film, as Professor Ping. Marceau: Bip and some Harpo Marx, some Einstein, some Chagall the French painter and some Marceau. The voice part is the most interesting. I dont forget the lines, but I have trouble organising them. Its a different way of making whats inside come out. It goes from the brain to the vocal chords, and not directly to the body. First Speaking Role for Marcel Marceau
Redmont, Dennis F. Los Angeles Times (1923-Current File)   25 Oct 1967: d20.  
Vadim said during filming that "Paramount has left me completely free, and so has De Laurentiis, who is known as a tyrant." Vadims Barbarella, a challenging film: A free hand Employs improvisation
By Kimmis Hendrick. The Christian Science Monitor (1908-Current file)   14 Oct 1967: 6.  

Filming was postponed when Fonda fell ill with a fever and had to be subjected to some tests. Miss Fonda and the Hummingbirds
Ebert, Roger. Los Angeles Times (1923-Current File)   25 Aug 1967: d12.  

==Release==
Barbarella was released 10 October 1968,  and it earned $2.5 million in North American theatre rentals in 1968.   The film was the second most popular film in general release in the UK in 1968. 

In 1977, the film was re-released in theaters as Barbarella: Queen of the Galaxy in an edited version that removed some nudity.   This edited version was released for the home video market on VHS, Betamax, CED disc, and Laserdisc.  Later video release versions on VHS, Laserdisc (Widescreen version), DVD, and Blu-ray disc use the 1977 artwork, the Queen of the Galaxy name, and the PG rating on the packaging, but the film in those releases is the original 1968 version. 

The Blu-ray was released in July 2012 and features the 1968 theatrical trailer as the discs only bonus feature. 

==Reception==
The film was both a box office and critical failure on its release.    Variety (magazine)|Variety s review stated that "Despite a certain amount of production dash and polish and a few silly-funny lines of dialogue, Barbarella isnt very much of a film. Based on what has been called an adult comic strip, the Dino De Laurentiis production is flawed with a cast that is not particularly adept at comedy, a flat script, and direction which cant get this beached whale afloat."   Rotten Tomatoes, a review aggregator, reports that 73% of 44 surveyed critics gave the film a positive review; the average rating is 6.3/10.  The sites consensus reads: "Unevenly paced and thoroughly cheesy, Barbarella is nonetheless full of humor, entertaining visuals, and Jane Fondas sex appeal." 

==Legacy== Put Yourself Jem in Break Free". Prince has cited the film as an inspiration for "Endorphin Machine". 

Despite an initially negative reception, in the years since its initial release, Barbarella has become a cult film.  Mysteries of the cult
French, Sean. The Observer (1901- 2003)   11 Dec 1988: A11.  

===Sequel=== Robert Evans head of Paramount saying the title would be Barbarella Goes Down. Film Pair Gets Bums Rush in Bistros
Joyce Haber:. The Washington Post, Times Herald (1959-1973)   28 Nov 1968: D15.  

Plans for a sequel with Fonda fell through in the mid to late 1970s. 

Terry Southern says he was contacted by Dino De Laurentiis in 1990 to write a sequel "on the cheap... but with plenty of action and plenty of sex" which could possibly star Fondas daughter.  The sequel was never written.

Roger Vadim said that he would be open to making a sequel with actresses Sherilyn Fenn or Drew Barrymore in the title role, but nothing came of it. 
===Remake===
There were also discussions of a possible remake. 
 James Bond Dino and Martha De Laurentiis heading as producers.   
 Sin City director Robert Rodriguez was soon after announced as the slated director for the remake.  Early candidates for the role of Barbarella were actresses Erica Durance of the WBs Smallville (TV series)|Smallville, Sienna Miller, and Rodriguezs Grindhouse star Rose McGowan. Later news articles confirmed that McGowan had been cast as the title role.  Universal Studios eventually backed out of the movie with some news sources speculating that it was due to studio executives doubting McGowans ability to carry a big budget movie and that the studio had slashed the budget after learning of McGowan winning the role. Rodriguez denied this, stating, "Universal had initially signed on for $60 million, but then when we were done with the script it wound up at closer to $82 million."  

Due to Universal insisting on lowering the cost of the film and on recasting the role of Barbarella, Rodriguez shopped the remake to other studios in the hopes of gaining a larger budget allowance and retaining McGowan as Barbarella. Rodriguez has stated that the large budget needs stem from the fact that the majority of the movie takes place in outer space, and that "we don’t want the movie to look like the original."    Rodriguez said he abandoned the project in May 2009 after he turned down a $70 million budget that required shooting in Germany. Expressing regret for the undone film, he thought he could not be away from his five children for as long as it would take if shot in Germany. 

Although later news articles would attach director Robert Luketic to the project,  the film had not met its projected release date of summer 2010 and there are no active plans to produce the film.

==See also==
* Barbarella (comics) which includes adaptations for other media including TV
* List of films based on French-language comics

==References==
 

==External links==
 
 
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 