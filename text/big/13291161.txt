Life Begins at Midnight
{{Infobox Film
| name           = Life Begins at Midnight
| image          = 
| image_size     = 
| caption        = 
| director       = Juan de Orduña
| producer       = Joaquín Cuquerella 
| writer         = Luisa-Maria Linares (novel)   Antonio Mas Guindal   Juan de Orduña
| narrator       = 
| starring       = Marta Santaolalla Armando Calvo Julia Lajos José María Seoane Juan Quintero
| cinematography = Willy Goldberger Tomás Duch
| editing        = Juan J. Doria
| distributor    = CIFESA
| released       = 9 November 1944
| runtime        = 88 minutes
| country        = Spain
| language       = Spanish
| budget         = 
}} 1944 Spain|Spanish comedy film written and directed by Juan de Orduña and starring Marta Santaolalla, Armando Calvo and Julia Lajos. 

The movie is based on the novel written by Luisa-Maria Linares.

==Cast==
* Marta Santaolalla as Silvia
* Armando Calvo as Ricardo
* Julia Lajos as María Linz
* José María Seoane as Álvaro
* José Isbert as El abuelo
* María Isbert as Clarita
* Consuelo de Nieva as Marcela
* José Prada as Juan Manuel Requena as Gorito
* Luis Sanz as Guillermito Manuel Soto as Director del hotel
* Antonio Prada
* Xan das Bolas
* Dolores Castillejo

==References==
 

==Bibliography==
* Bentley, Bernard. A Companion to Spanish Cinema. Boydell & Brewer, 2008.

==External links==
*  

 

 
 
 
 
 
 
 
 
 

 
 