Absolute Evil
{{Infobox Film
| name = Absolute Evil – Final Exit
| image          = Ulli Lommels Absolute Evil.jpg
| image_size     =
| caption        = DVD Release Cover (Germany)
| director = Ulli Lommel
| producer = Ulli Lommel Nola Roeper
| writer = Ulli Lommel
| starring = Carolyn Neff Rusty Joiner David Carradine Ulli Lommel Christopher Kriesa Elissa Dowling
| music = Robert J. Walsh
| editing = Christian Behm Brian Lopiano Frank Schönfelder
| released =  
| runtime = 90 minutes
| country = Germany United States
| language = English
| budget = $1,2 million
}}
Absolute Evil – Final Exit is a drama film written and directed by Ulli Lommel. The film stars Carolyn Neff, Rusty Joiner and David Carradine. The film premiered at the Berlin International Film Festival on February 8, 2009.

== Plot==
Lovers Savannah (Carolyn Neff) and Cooper (Rusty Joiner) share a childhood psychological trauma|trauma. But just as they seem to have conquered their inner demons, the past catches up on them, and they are soon pursued by enigmatic gang leader Raf (David Carradine) and his plans of retaliation and murder.

== Cast ==
{| class="wikitable"
|-
! Actor
! Role
|-
| Carolyn Neff || Savannah Miller
|-
| Rusty Joiner || Cooper Lee Baines
|-
| David Carradine || Raf McCane
|-
| Ulli Lommel  || Rick
|-
| Christopher Kriesa || Beauregard
|-
| Elissa Dowling || Lillie McCane
|-
| Mark Irvingsen || Ringo
|-
| Jamie Bernadette || Maggie
|}

== Premiere ==
The film was accepted to Berlin International Film Festival and saw its premiere in February 2009. The film was shown in the Panorama section.

  wrote: "At least once every festival, critics collectively scratch their heads and say "How did THAT get selected?" Absolute Evil is the tentative awardee for worst film at this years Berlinale. Shot in an ugly digital format (not High-definition video|HD) that is often out of focus, the stock thriller structure also sports horribly cliched, repetitive dialogue, dramatic "gestures" that weve seen a thousand times, and very bad performances (with the exception of David Carradine, who seems to be having the time of his life). Commercial prospects, aside from a few DVD buyers who might be seeking campy entertainment, seem quite remote."

== Trilogy ==
Absolute Evil – Final Exit is the first part of the trilogy that will continue with Absolute Evil – Life After Death and Absolute Evil – Back from the Dead. 

== References ==
 

== External links ==
*  
*  
*  

 
 
 
 
 
 
 
 
 
 


 
 