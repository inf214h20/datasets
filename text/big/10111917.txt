Joymoti (1935 film)
 Joymoti}}
{{Infobox Film
| name           = Joymoti
| image          = Joymoti film screenshot.jpg
| image_size     =
| caption        = A screenshot of the film   (actress Aideu Handique as Joymoti)
| director       = Jyoti Prasad Agarwala
| producer       = Jyoti Prasad Agarwala
| writer         = Lakshminath Bezbaroa (play)
| narrator       = 
| starring       = Aideu Handique Phunu Barooah 
| music          = Jyoti Prasad Agarwala
| cinematography = Bhopal Shankar Mehta
| Budget         =  
| distributor    = Chitralekha Movietone
| released       = 10 March 1935
| runtime        = 
| country        =   India Assamese
| Box Office     =  
}} Assamese film made. Based on Lakshminath Bezbaroas play about the 17th-century Ahom princess Soti Joymoti, the film was produced and directed by the noted Assamese poet, author, and film-maker Jyoti Prasad Agarwala, and starred  Aideu Handique and acclaimed stage actor and playwright Phani Sarma. The film, shot between 1933 and 1935,  was released by Chitralekha Movietone in 1935 and marked the beginning of Assamese cinema.

Joymoti was screened at the 50th International Conference of the Society For Cinema and Media Studies (SCMC) of Northwestern University in Evanston, Illinois, in March 2011. 

Other screenings include:
* India-Bangladesh Joint Celebration of 100 Years of Indian Cinema, Dhaka (2012)
* UCLAs Centre for India and South Asia Studies, Los Angeles (April 2010)
* Osian-Cinefans 10th Film Festival of Asian and Arabic Cinema, New Delhi (2008)
* Filmbüro Baden Württembergs Internationales Indisches Filmfestival, Stuttgart (2006)
* Asiaticafilmidale (Encounters with Asian Cinema), Rome (2006)
* Munich Film Festival (2006).
 
Although never a commercial success, Joymoti was noted for its political views and the use of a female protagonist, something almost unheard of in Indian cinema of the time.

The film was the first Indian talkie to have used Dubbing and Re-recording Technology,  and the first to engage with "realism" and politics in Indian cinema.  The original print containing entire length of the film was thought to be lost after Indias division in 1947. However, in 1995, popular Assamese story-writer, novelist, engineer, actor, screenwriter and documentary film director Arnab Jan Deka managed to recover entire footage of the lost film at a Studio in Bombay in intact condition, and reported back the matter to Assam Government apart from writing about this recovery in Assamese daily Dainik Asam. This films directors younger brother Hridayananda Agarwala and famous Assamese actor Satya Prasad Barua also confirmed and publicly acknowledged Arnab Jan Dekas great recovery through two separate write-ups in Dainik Asam and highly circulated English daily The Assam Tribune respectively in 1996. This matter was also debated at Assam Legislative Assembly, and Secretary, Cultural Affairs Department of Assam Government, convened an official meet to discuss this matter together with other issues pertaining to development of Assamese films. Meanwhile, some reels of another remaining print of the film maintained by Hridayananda Agarwala has been restored in part by Altaf Mazid. 

==Plot summary== Ahom princess tortured and killed by the Ahom king Borphukan for refusing to betray her husband Gadapani by disclosing his whereabouts. The event is interpreted in contemporary patriotic terms, and calls for a greater harmony between the people of the hills and those of the plains. The hills are represented by the leader Dalimi, a Naga tribeswoman who shelters the fugitive Prince Gadapani.

==Background ==
  as Dalimi)|200px]]  UFA Studios in Berlin, learning film-making. Once back in Assam, he decided to make his first film. He established Chitraban Studios at the Bholaguri Tea Estate. Two camps were established: one near the Managers Bungalow for the female artists, and the other near the tea factory for the male artists. Tea was manufactured by day, and by night actors performed at their rehearsals. Members of the cast were encouraged to keep up their physical exercises to stay fit.

A special property room was constructed, in which Jyoti Prasad Agarwala collected traditional costumes, ornaments, props, hats, etc. This grew into a museum. Technicians were brought in from Lahore; ice, transported from Calcutta.

The film was taken to Dhaka for editing, at which stage Agarwala discovered there was no sound for one half of the film. Unable to marshal the actors once again from their native places due to various constraints, he hired a sound studio and dubbed the voices of all male and female characters. On a single day, he recorded six thousand feet of film. This unplanned accomplishment made Jyotiprasad Agarwala the first Indian filmmaker to have introduced Dubbing and Re-recording Technology in Talkies. 

===Plot background===
Joymoti was the wife of the Ahom prince Gadapani. During the Purge of the Princes from 1679 to 1681 under King Sulikphaa (Loraa Roja), instigated by Laluksola Borphukan, Gadapani took flight.  Over the next few years, he sought shelter at Sattras (Vaishnav monasteries) and the adjoining hills outside the Ahom kingdom.   
Failing to trace Prince Gadapani, Sulikphaas soldiers brought his wife Joymoti to Jerenga Pathar where, despite brutal and inhuman torture, the princess refused to reveal the whereabouts of her husband. After continuous physical torture over 14 days, Joymoti breathed her last on 13 Choit of 1601 Saka, or 27 March, AD 1680. 

Joymotis self-sacrifice would bear fruit in time: Laluk was murdered in November 1680 by a disgruntled body of household retainers. The ministers, now roused to a sense of patriotism, sent out search parties for Gadapani who, gathering his strength, returned from his exile in the Garo Hills to oust Sulikphaa from the throne. Joymoti had known that her husband alone was capable of ending Sulikphaa-Laluks reign of terror. For her love and her supreme sacrifice for husband and country, folk accounts refer to her as a Soti.

==Reception==
The film was released on 10 March 1935, at the Raonak Theatre, and was inaugurated by the Assamese writer Lakshminath Bezbarua. In Guwahati, it was screened at the Kumar Bhaskar Natya Mandir, the only cinema in Assam which then had sound. The film was not well received, consequently suffering a debilitating financial loss. It was able to collect only INR 24,000 from its screenings, less than half its budget of INR 50,000 (at the time), which today amounts to INR 75,00,000.

==Overview==
 
Joymoti, a study of the culture and history of Assam, carried with it the bright possibility of a film tradition. The significant similarities with the Russian montage reflect an element of influence. The film is noted for its constantly changing angles, unique sets (built from scratch on a tea plantation, with local materials), and other stylistics tactics employed by the imaginative Jyoti Prasad in this his film debut. By then a published poet and writer, his lyricism is clearly evident in this pioneering film.

==Technical aspects==
The film was shot on a 4267.20 m-length film.

==Filming ==
  buffalo Horn (anatomy)|horns, and Nāga spears. To furnish the royal court of the Ahoms, he used regal utensils such as maihang, hengdang, bhogjara, bata, sariya, and banbata. He imported a Faizi sound-recording system from Lahore, and the first camera into Assam from Mehta of Calcutta.

The unit arrived at Bholaguri Tea Estate in December 1933 to commence filming, and camped in front of the garden factory. Jyoti Prasad arranged all accommodation arrangements personally, and set up a laboratory for developing film by the side of his ‘Chitraban’ studio.
 fade out, zoom lens|zoom, dissolve, back projection, and model shooting. The 17th-century costumes used in the film were designed by Jyoti Prasad, and the make-up was done by the actors themselves, assisted by Sonitkowar Gajen Borua. 
 
Although shooting at the Chitraban Studio started in April 1933, it faced an initial delay as Jyoti Prasad was unable to find a suitable young woman to play "Joymoti", as well as actors for a few other roles. He floated newspaper advertisements for actors and actresses, mentioning brief outlines of the film and descriptions of the characters. His idea was to get ‘types’ for his characters, not seasoned actors, even offering remunerations for successful candidates. One of his preconditions was that potential actors needed to be from ‘respectable’ families, as opposed to red-light areas, as had been the case during the 1930s in  . He then brought together the other chosen actors, of whom some had never seen a film, to acquaint them with his characters. He sought out a film-making trio, Bhupal Shankar Mehta and the Faizi Brothers from Lahore, as cameraman and sound-recordists.

During filming, the rainy season was to prove a challenge to developments in the technical process, with Jyoti Prasad having to suspend shooting for several days at a time, due to insufficient light in the absence of outdoor electricity. Shooting was carried out under sunlight by using reflectors. When the rain stopped, the banana stumps used to build the splendid Ahom court dried out under the sunlight, yet filming continued - japis ingeniously fastened over the dry patches. Filming was eventually completed in August 1934, and Joymoti released in early 1935 after Jyoti Prasad had completed his own editing.

==Cast==
 
*Phanu Barua   
*Aideu Handique   
*Mohini Rajkumari   
*Swargajyoti Datta Barooah 
*Manabhiram Barua  
*Phani Sarma  
*Sneha Chandra Barua 
*Naren Bardoloi  
*Rana Barua  
*Shamshul Haque  
*Rajen Baruah 
*Putal Haque  
*Pratap Barua  
*Rajkumari Gohain
*Subarnarekha Saikia (as Kheuti)   
*Lalit Mohan Choudhury  
*Banamali Das  
*Prafulla Chandra Barua  
*Kamala Prasad Agarwala
*Subha Barowa (in the role of spy)

==Film rediscovered==
Following the Second World War, Joymoti was almost forgotten and lost. In the early 1970s, Jyoti Prasad’s youngest brother Hridayananda Agarwala found seven reels of the lone print of Joymoti while cleaning junk out of his garage. Jyoti Prasad’s venture, with its considerable losses, had cost the family plantation dearly, placing his family in acute difficulties. The condition of the reels, by the early 1970s, was abysmal, but his brother Hridayananda commissioned the well-known Assamese director Bhupen Hazarika to direct the long documentary Rupkonwar Jyotiprasad aru Joymoti in 1976, in which the reels were incorporated. The documentary thereby saved the reels, which have been copied and remastered since. However, in 1995, popular Assamese story-writer, novelist, engineer, actor, screenwriter and documentary film director Arnab Jan Deka managed to recover the original print of the film containing entire footage length at a Studio in Bombay in intact condition. This original print of Joymoti was thought to be lost after Indias division in 1947, as it was left behind in a studio in Lahore, now in Pakistan. Somehow, the print together with other films travelled from Lahore and resurfaced in Indias film capital. After making this great recovery, Arnab Jan Deka reported back the matter to Assam Government apart from writing himself about this recovery in Assamese daily Dainik Asam. This films directors younger brother Hridayananda Agarwala and famous Assamese actor Satya Prasad Barua also confirmed and publicly acknowledged Arnab Jan Dekas great recovery through two separate write-ups in Dainik Asam and highly circulated English daily The Assam Tribune respectively in 1996. This matter was also debated at Assam Legislative Assembly, and Secretary, Cultural Affairs Department of Assam Government, convened an official meet to discuss this matter together with other issues pertaining to development of Assamese films. Arnab Jan Deka again wrote in details about this entire episode in a very prestigious Assamese literary journal Prantik in 2011.

==The fate of Chitraban==
Situated about 10&nbsp;km west of Gohpur, Jyoti Prasads temporary film studio ‘Chitraban’, at Bholaguri Tea Estate, today stands deserted, a nostalgic nod to its glorious past. Once owned by Jyoti Prasad, the tea plantation passed on to the Assam Tea Corporation in 1978. The garden, where Jyoti Prasad single-handedly laid the foundation stone of Assamese cinema, now lies abandoned. The bungalow, where he composed the music for Joymoti on his organ, still stands - albeit in a dilapidated condition.

==See also==
*Indramalati (1939)

==References==
 

==External links==
*  
*  , from  
*   at  

 
 
 
 
 
 
 