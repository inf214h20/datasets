Baazi (1951 film)
{{Infobox film
| name = Baazi
| image = 
| caption = 
| director = Guru Dutt
| producer = Dev Anand (Navketan Films)
| writer = Balraj Sahni (screenplay, story, dialogue)  Sahir Ludhianvi (lyrics)
| starring = Dev Anand Geeta Bali Kalpana Kartik
| music = S. D. Burman
| cinematography = V. Ratra
| editing = Y.G. Chawhan
| distributor = 
| released =  
| runtime = 
| country = India
| language = Hindi, Urdu
| budget = 
}} Hindi film directed by Guru Dutt. This was the second film of Dev Anands production house Navketan Films, and as per a commitment given by Dev Anand to Guru Dutt in their days of struggle, the movie was directed by Dutt.

The movie stars Dev Anand with Geeta Bali and Kalpana Kartik. It is a crime thriller and had very popular music composed by S.D. Burman.                        

The film is a tribute to the Forties Film Noir Hollywood with the morally ambiguous hero, the transgressing siren, and shadow lighting. It was very successful at the box-office.

==Synopsis==
Madan (Dev Anand) comes from a once well-to-do family background who is now out of work and lives in a shanty with a sick younger sister Manju (Roopa Verman). Unable to find employment, he takes to gambling in a big way. He develops a reputation for being a lucky gambler and is one day found and escorted by a stranger named Pedro to the Star Hotel where he meets the seductive dancer Leena(Geeta Bali) and is offered a job by the mysterious Maalik (Boss)of tempting rich gentlemen to come and gamble at the club. Initially reluctant to take up this offer due to moral pangs, he refuses and leaves. He meets with sophisticated and cultured Dr. Rajani (Kalpana Kartik), who has opened a free clinic in his locality to treat the poor and needy and helps him out in the treatment of his sister, who is suffering from tuberculosis. Both are attracted to each other and soon fall in love. Rajanis rich lawyer dad (K.N. Singh) does not approve of Madan, nor of his background, and prefers that Rajani marry her childhood friend Inspector Ramesh (Krishan Dhawan), who is in love with her. Dejected and in dire need of money for his sisters treatment who has been packed off to a sanatorium somewhere in a hill station, Madan accepts the job at star club and meets more frequently with the sexy club dancer Leena. He becomes friends with her and is seen discussing his troubles and thoughts with her. In turn, she is obviously charmed by him and evidently has a soft spot for him. Then Inspector Ramesh arrests Madan and imprisons him for the murder of Leena, who was killed with a revolver with Madans fingerprints on it. The shooter intended to kill Madan, but Leena protected him and was killed instead. Rajanis father (who also turns out to be Maalik, the mysterious Boss of Star Club) had ordered the killing because he didnt consider Madan suitable for his daughter. Rajanis father threatens Madan that he will kill his sister if he says anything. Madan keeps quiet and is sentenced to be hanged at 6 am. Inspector Ramesh finds some evidence that it wasnt Madan who killed Leena. He sets a trap for Rajanis father and leads him to believe that Madan has been hanged, and in his joy, makes him confess to his plan to frame Madan. Then, Rajanis father is arrested and imprisoned and Madan is sent to three months worth probation for indulging in gambling activities. He is freed after serving his small sentence and is united with Rajani.

==Cast==
* Dev Anand ... Madan
* Geeta Bali ... Leena
* Kalpana Kartik ... Rajani
* Roopa Verman		
* K.N. Singh	... 	Rajanis Father Johnny Walker
* Krishan Dhawan	... 	Ramesh Rashid Khan ... Pedro
* Habib
* Srinath
* Rashid Ahmed
* Abu Baker
* Nirmal Kumar

==Music==
{{Infobox Album |  
 Name = Baazi
| Type = Album
| Artist = Sachin Dev Burman
| Cover = 
| Released = 1951
| Recorded = 1951 Feature film soundtrack
| Length = 
| Label = EMI Records
| Producer = Sachin Dev Burman
}}
{| border="2" cellpadding="5" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Track # !! Song !! Singer(s)
|-
| 1
| Suno Gajar Kya Gaaye
| Geeta Dutt
|-
| 2
| Dekh Ke Akeli Mohe Barkha Sataaye
| Geeta Dutt
|-
| 3
| Yeh Kaun Aaya Ki Meri Dil Ki Duniya Mein
| Geeta Dutt
|-
| 4
| Tadbeer Se Bigdi Hui Taqdeer Bana Le
| Geeta Dutt
|-
| 5
| Aaj Ki Raat Piya
| Geeta Dutt
|-
| 6
| Tum Bhi Na Bhoolo Balam
| Geeta Dutt
|- 
| 7
| Sharmaaye Kaahe Ghabraaye Kaahe
| Shamshad Begum
|-
| 8
| Mere Labon Pe Chhipe
| Kishore Kumar
|}

== References ==
 

== External links ==
*  

 
 
 
 
 