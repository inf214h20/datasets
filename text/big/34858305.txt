Madicken på Junibacken
{{Infobox film
| name           = Madicken på Junibacken
| image          = Madicken på Junibacken.jpg
| image size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Göran Graffman
| producer       = Olle Hellbom Olle Nordemar
| screenplay     = Astrid Lindgren
| based on       =  
| narrator       = 
| starring       = Jonna Liljendahl
| music          = Bengt Hallberg
| cinematography = Jörgen Persson
| editing        = 
| studio         = 
| distributor    = AB Svensk Filmindustri
| released       =  
| runtime        = 82 minutes
| country        = Sweden
| language       = Swedish
| budget         = 
| gross          = 
}}
Madicken på Junibacken is a 1980 Swedish film about the Astrid Lindgren character Madicken, directed by Göran Graffman.

==Cast==
* Jonna Liljendahl as Margareta "Madicken" Engström
* Liv Alsterlund as Lisabet
* Monica Nordquist as Kajsa
* Björn Granath as Jonas
* Lis Nilheim as Alva
* Birgitta Andersson as Emma Nilsson
* Allan Edwall as Emil P. Nilsson
* Sebastian Håkansson as Abbe Nilsson
* Kerstin Hansson as Mia

==External links==
*  
*  

 
 
 
 
 


 