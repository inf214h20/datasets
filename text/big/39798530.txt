Roger Rabbit short films
  Walt Disney live action and animation, where the characters interact with human beings, akin to the 1988 film.
 Kathleen Kennedy, Frank Marshall, and Don Hahn. Marshall also directed the live-action segments in the first two shorts, while Industrial Light & Magic was responsible for the live-action visual effects. Produced in association with Spielbergs Amblin Entertainment, the three shorts (Tummy Trouble, Roller Coaster Rabbit and Trail Mix-Up) were originally attached to the theatrical releases of several Disney and Amblin films. A fourth short, Hare in My Soup, was cancelled during pre-production.    

==Tummy Trouble==
{{Infobox Hollywood cartoon italic title=no
| cartoon name      = Tummy Trouble
| series            = Roger Rabbit
| image             = Tummy Trouble poster.jpg
| image size        = 200px
| alt               = 
| caption           = 
| director          = Rob Minkoff
| producer          = Don Hahn  
| basedon           = Characters Created by Gary K. Wolf
| narrator          =  See cast
| musician          = James Horner
| animator          = 
| layout artist     = 
| background artist = 
| studio            = Walt Disney Pictures Amblin Entertainment  Buena Vista Pictures
| release date      =  
| color process     = 
| runtime           = 7 minutes
| country           =  
| language          =  
}}

===Plot===
Roger is placed in charge of watching Baby Herman when his mother needs to step out for an hour; as soon as she leaves, Herman breaks into a heavy crying fit which Roger doesnt seem to be able to break until he pulls out Hermans favorite Rattle (percussion instrument)|rattle, which immediately garners Hermans attention. After a brief second of shaking it, Herman swallows the rattle, prompting Roger to rush the baby to the emergency room. Roger is overcome with guilt when he visits but quickly realizes Herman wants to drink from a milk bottle in the room; after Roger burps Herman, he hiccups the rattle, but in Rogers joyous celebration he accidentally swallows it, causing Baby Herman to become upset he lost his toy. Roger begins to dance, his hips rattling with the toy and giving Baby Herman some amusement, but a doctor bursts in and mistakes Roger for Baby Herman and preps him for emergency surgery.

While hes gone, Herman spies Jessica Rabbit pushing a cart of milk bottles and gives chase, eventually following a runaway milk bottle into the emergency room where Roger is strapped to the table while the surgeons had disappeared for a lunch break. Herman mistakes a large surgical laser for a bottle and climbs up onto it, nearly dissecting Roger in the process. The laser detaches itself from the ceiling and flings a table of scalpels and hypodermic needles at Roger, who avoids them, but is electrocuted in the process. The laser flies around the room and lodges itself under Rogers stretcher and he and Herman both eject from the emergency room, causing Roger to gag up the rattle and Baby Herman to again swallow it, before crashing into a wheelchair, they then fly down the hall and into an open elevator shaft due to wet floors causing the wheelchair they landed on to skid out of control. Baby Hermans diaper parachutes him safely to a floor while Roger ends up getting crushed by an elevator while trying to catch Herman. Eventually they end up in a room with piles of gas pumps which are ignited and the pair are launched miles into the air. As they fall, Herman coughs up the rattle and Roger swallows it again. As they crash back into the hospital, Roger crashes through several floors before landing smack down on the receptionist floor in the hospital. As he recovers, Baby Herman lands on Roger, causing him to cough up the rattle again, finally ending their adventure. But Rogers celebration is short lived when he sees the bill for their rampant destruction and passes out. Herman then crawls over to the rattle and as the screen fades to black there is a gulping sound as he again swallows the rattle.

During the end credits, however, Herman, angrily threatens trouble if he has to swallow the rattle again.
Unlike the next two cartoons, this short was completed.

===Cast===
*Charles Fleischer as Roger Rabbit
*Kathleen Turner as Jessica Rabbit
*Lou Hirsch as Baby Herman
*April Winchell as Mrs. Herman and Baby Hermans baby talk
*Corey Burton as Orderly  Richard Williams as Droopy

===Production===
Tummy Trouble was produced over the course of nine months by a staff of 70 Disney animators.   It was the first animated short Disney had produced in 25 years to accompany a feature film, since Goofys Freeway Trouble in 1965. 

The short was released with  .

==Roller Coaster Rabbit==
{{Infobox Hollywood cartoon italic title=no
| cartoon name      = Roller Coaster Rabbit
| series            = Roger Rabbit
| image             = Roller Coaster Rabbit poster.jpg
| image size        = 200px
| alt               = 
| caption           = 
| director          = Rob Minkoff
| producer          =  
| basedon           = Characters Created by Gary K. Wolf 
| narrator          = 
| voice actor       = See cast
| musician          = Bruce Broughton
| animator          = 
| layout artist     = 
| background artist = 
| studio            = Touchstone Pictures  Amblin Entertainment  Buena Vista Pictures
| release date      =  
| color process     = 
| runtime           =  7 minutes
| country           =  
| language          =  
}}

===Plot===
Roger Rabbit, Baby Herman and Mrs. Herman are at the local county fair. Baby Herman loses his red balloon and Roger goes to get him a new one. Before he returns, however, Baby Herman sees another red balloon at a dart game and goes to try to get it. When Roger comes back to give Baby his balloon, he finds that he is gone, and the chase begins. Firstly Baby Herman finds himself following the balloon into a field homed to a grazing bull. Roger soon follows the youngster, and Roger falls in bull dung Baby Herman walks through directly underneath the bull, he notices a round balloon-like object and grasps it unknown to him that it was in fact the bulls scrotum. The grazing creature snaps, Roger picks up Baby Herman but just happens to be looking the bull in the eyes. The animal  hurls Roger and baby into the air sending him flying out of the field and the two land crashing into a roller coaster carriage which is traveling slowly up.
 Jessica Rabbit appears where she is tied down to the tracks, unable to move. She calls out to be saved before  Roger and Baby Hermans carriage crushes her. As the cart draws near, it topples over and fortunately bounces over Jessica avoiding her completely. The camera moves along and beside her appears Droopy for a quick one-liner. The story then continues. Roger grasping onto Baby Herman, tumbling and losing their carriage leaving Roger sliding along the tracks with his feet, gradually gaining friction causing his feet to catch fire. The tracks run into a dark tunnel and then stumbles across a wrong way sign. Finally Herman and Roger crash through the sign and into a real-life filming studio. A direct reference to the reality/cartoon crossover in the feature film.

===Cast===
*Charles Fleischer as Roger Rabbit
*Kathleen Turner as Jessica Rabbit
*Lou Hirsch as Baby Herman
*April Winchell as Mrs. Herman and Baby Hermans baby talk
*Corey Burton as Droopy
*Frank Welker as the Bull

===Production=== the satellite studio located at Disneys Hollywood Studios|Disney-MGM Studios in Lake Buena Vista, Florida.  Rob Minkoff returned to direct the second short in the series.
 Dick Tracy, in hopes that the short would increase awareness to the film.     Spielberg, who controlled a 50% ownership stake in the character, decided to cancel Hare in My Soup, the third short that had entered production.  

==Trail Mix-Up==
{{Infobox Hollywood cartoon italic title=no
| cartoon name      = Trail Mix-Up
| series            = Roger Rabbit
| image             = Trail Mix-Up poster.jpg
| image size        = 200px
| alt               = 
| caption           = 
| director          = Barry Cook
| producer          = Rob Minkoff 
| basedon           = Characters Created by Gary K. Wolf
| narrator          = 
| voice actor       = See cast
| musician          = Bruce Broughton
| animator          = 
| layout artist     = 
| background artist = 
| studio            = Walt Disney Pictures Amblin Entertainment  Buena Vista Pictures
| release date      =  
| color process     = 
| runtime           = 8 minutes
| country           =  
| language          =  
}}

===Plot===
The short features Roger Rabbit, Baby Herman and Mrs. Herman at the park setting up camp. Mrs. Herman plans to go hunting and leaves Roger in charge of watching Baby Herman. Trouble begins when Baby wanders off in the dangers of the forest and Roger has to go and save him, leading to multiple calamities such as Roger panicking at the sight of a bug and spraying so much insecticide (named Mink-Off) that many trees die. Later, Baby Herman follows a bee up to a beehive, and Roger tries to save him. The beehive falls on Rogers head, causing him to get stung multiple times. The bees proceed to chase him, so Roger runs into a lake, where he panics at the sight of a sharks dorsal fin (which is actually controlled by Droopy).

Later, Baby Herman follows a beaver (mistaking him for a dog), and Roger chases after them. Baby Herman follows the beaver up a pile of logs, and Roger follows, only to have the log that Baby and the beaver are on taken to the sawmill. This ends up with Roger being shredded by a sawmill (and the result is 13 tiny Rogers, which then join again into a regular-sized Roger, who follows Baby Herman (still following the beaver) onto a conveyor belt with logs.) It ends up with the logs being thrown down a log flume, eventually landing in a river. The log Roger, Baby and the beaver are on crashes into a bear, who ends up on the log. Then the four fall off a waterfall. Rogers head gets stuck in a twig sticking out of the waterfall, and he catches Baby Herman (holding on to the beaver), and the bear grabs onto Rogers legs. The combined weight rebounds, sending all four flying, landing on a large boulder.

The boulder proceeds to roll down a hill, knocking over a tree trunk (with the same sound effects as a bowling pin), and then flying off a cliff. Eventually, Roger, the bear, the log, the beaver, the boulder, and Baby Herman all land on top of Old Predictable Geyser in that order. Then, Old Predictable Geyser erupts, sending Roger, the bear, the log, the beaver, the boulder, and Baby Herman flying out of the studio, above Hollywood, before landing on Mount Rushmore, destroying it. Everyone is battered and beaten, and Baby Herman yells at Roger for destroying Mount Rushmore. Roger sticks a flag (made of his pants) in the ground and salutes, but then the Earth deflates.

===Cast===
*Charles Fleischer as Roger Rabbit
*Kathleen Turner as Jessica Rabbit
*Lou Hirsch as Baby Herman
*April Winchell as Mrs. Herman and Baby Hermans baby talk
*Corey Burton as Droopy
*Frank Welker as the Bear and Beaver
*Alice Playten as the Bee (uncredited)

===Production=== Florida studio. CAPS system. 
The short was released theatrically with Disney/Amblins A Far Off Place on March 12, 1993. 

==Home media==
In 1995, a VHS tape of the three shorts was released under the title Its Roger Rabbit, bundled with Who Framed Roger Rabbit. A nearly identical video was released by itself in 1996 under the title Disney and Steven Spielberg present The Best of Roger Rabbit. The three shorts are also included in the 2003 special edition "Vista Series" DVD of Who Framed Roger Rabbit. On March 12, 2013, Walt Disney Studios Home Entertainment remastered and reissued all three shorts as part of the 25th anniversary Blu-ray release of Who Framed Roger Rabbit.    

==Footnote==
 	
#  Roller Coaster Rabbit was reissued under the Walt Disney Pictures label for the 2013 Blu-ray release of Who Framed Roger Rabbit.  
 

==References==
 

==External links==
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 