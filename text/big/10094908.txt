A Tale of Sorrow and Sadness
{{Infobox film
| name           = A Tale of Sorrow and Sadness
| image          = A Tale of Sorrow and Sadness poster.jpg
| caption        =
| director       = Seijun Suzuki
| producer       =
| story          = Ikki Kajiwara
| screenplay     = Atsushi Yamatoya
| starring       = Yoko Shiraki Yoshio Harada Masumi Okada Joe Shishido
| music          = Keitaro Miho Ichiro Tomita
| cinematography = Masaru Mori
| editing        = Akira Suzuki
| distributor    = Shochiku
| released       = May 21, 1977
| runtime        = 93 minutes
| country        = Japan
| language       = Japanese
| budget         =
| gross          =
}}
  is a 1977 Japanese film directed by Seijun Suzuki.

==Plot==
The film is about a professional model Reiko (Shiraki) who is being groomed for the golf circuit by the editor of a golfing fashion magazine.  During her first professional competition she has great success, winning the approval of her mentor, a TV audience and others. Suddenly, everyone wants a piece of Reiko.    The plot turns sinister as one of her devoted followers develops an obsession with Reiko and starts to blackmail and threaten her.   

==Cast==
* Yoko Shiraki
* Yoshio Harada
* Masumi Okada
* Joe Shishido
* Koji Wada
* Shuji Sano
* Asao Koike
* Keisuke Noro

==Reception== What Ever Happened to Baby Jane? (1962), this sinister social satire of Stepford Wives-esque suburban aspiration set against the glamorous world of big budget sports promotion is impossible to pigeonhole as anything other than a Suzuki film." 

David Carter of Film Fanaddict described the film as "a vastly different film from his previous body of work, but one that retained many of the stylistic touches for which he was known and contained more than a few subtle digs at corporations, fame and the entertainment industry." 

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 


 
 