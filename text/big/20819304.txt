The Gunsaulus Mystery
{{Infobox film
| name           = The Gunsaulus Mystery
| image          = GunsaulusMystery1921-film.jpg
| image_size     =
| caption        =
| director       = Oscar Micheaux
| producer       = Oscar Micheaux
| writer         = Oscar Micheaux
| narrator       =
| starring       = Evelyn Preer Lawrence Chenault Edward R. Abrams
| music          =
| release        =
| cinematography = Leonard Galezio
| editing        =
| released       = 1921 (USA) reels
| country        = United States of America
| language       = Silent (English intertitles)
| budget         =
| gross          =
| distributor    = Micheaux Film Corporation
}}
 

The Gunsaulus Mystery is a 1921 American silent race film directed, produced, and written by Oscar Micheaux. The film was inspired by events and figures in the 1913-1915 trial of Leo Frank for the murder of Mary Phagan.

==Plot==
The body of Myrtle Gunsaulus, a young African-American girl, is discovered in the basement of a New York City factory. Arthur Gilpin, the African-American janitor who discovered the body, is arrested and charged with her murder.

Arthur’s sister Ida May (Evelyn Preer) contacts her former boyfriend, the attorney Sidney Wyeth (Lawrence Chenault), to defend Gilpin in court. During the trial, Wyeth redirects attention for the murder away from Gilpin to Anthony Brisbane, a white man with a history of sexual deviancy. Gilpin is exonerated while Brisbane is revealed as Myrtle Gunsaulus killer. 

==Production==
Oscar Micheaux, the most prolific African-American filmmaker of the race film genre, had previously addressed the issue of violence by whites against blacks in his 1920 feature Within Our Gates, which aroused controversy.  , BlackPast, accessed 14 Dec 2010  That film’s storyline, which included a portrayal of racial lynching and the sexual attack by a white man against a black woman, resulted in censorship rulings in Atlanta and other major cities throughout the U.S. 

Micheaux tackled another controversial subject with his 1921 The Gunsaulus Mystery. " , New York Times  The plot was based on the 1913 murder of Mary Phagan and the trial of Leo Frank.  After an African American was first interrogated, police attention turned to Frank, the Jewish-American manager of the factory.  He was prosecuted and convicted of the crime. After appeals had failed, he received commutation of his death sentence, but Frank was kidnapped and lynched on August 17, 1915.

Micheaux shot The Gunsaulus Mystery at the Estee Studios in New York City and distributed the film through his Micheaux Film Corporation. Evelyn Preer, the star of Within Our Gates, also starred in this production.

Micheaux revisited the subject again in 1935 with a sound remake, which was released under the titles Murder in Harlem and Lem Hawkins Confession.  Especially in this version, Micheaux used the conventions of the detective story to introduce differing narratives and rework the binary nature of the trial, in which an African-American man and Jewish-American man had testified against each other. 
 lengthy 1988 four-hour TV miniseries The Murder of Mary Phagan. 

==See also==
*List of lost films

==References==
 

==External links==
 
* 
* 
* 
* 

 

 
 
 
 
 
 
 
 