House of Dust
 
{{Infobox film
| name           = House of Dust
| image          = HOD-Teaser-Poster.jpg
| caption        = Teaser poster
| director       = A.D. Calvo
| producer       = A.D. Calvo, Todd Slater
| story          = A.D. Calvo
| screenplay     = Nevada Grey & Alyssa Alexandria
| starring       =  
| cinematography = Christo Bakalov
| editing        = 
| studio         = Budderfly / Goodnight Film
| music          = 
| distributor    = Anchor Bay Entertainment
| released       =  
| country        = United States
| language       = English
| runtime        = 
}} thriller film directed by A.D. Calvo.  The movie had its world premiere on May 5, 2013 and focuses on a group of college students that become possessed by the ghosts of former mental patients. Filming took place in Willimantic, Connecticut and Mansfield, Connecticut at the Mansfield Training School and Hospital, University of Connecticut, and Eastern Connecticut State University during the summer of 2011.     

==Plot==
College students exploring an abandoned insane asylum accidentally shatter canisters holding the cremains of former mental patients; inhaling the dusty ash filling the air, they’re soon possessed by the souls once held within them. One is a convicted serial killer from 1950.

==Cast==
*Inbar Lavi as Emma
*Steven Grayhm as Kolt
*Eddie Hassell as Dylan
*Holland Roden as Gabby
*John Lee Ames as Levius
*Stephen Spinella as Lobotomy Surgeon
*Justin James Lang as Clyde
*Alesandra Assante as Allyson
*Joy Lauren as Blythe
*Nicole Travolta as Christine

==Reception==
HorrorNews.net panned House of Dust, praising the movies cast while stating that the movie "didn’t bring anything new to the table and was just lackluster and forgettable overall".  DVD Verdict gave a mixed review and commented that while the movie was "no classic", that "A.D. Calvo directs the action well and gets good performances from actors lacking much experience" and that it would appeal best to fans of independent horror or films set in mental institutions. 

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 
 
 

 