Planeat
{{Infobox film name           = Planeat image          = Planeat (film).jpg caption        =  director       = Or Shlomi Shelley Lee Davies producer       = Or Shlomi Shelley Lee Davies Christopher Hird writer         = Or Shlomi Shelley Lee Davies starring       = T. Colin Campbell Caldwell Esselstyn Gidon Eshel Peter Singer music          = Ernie Wood cinematography =  editing        = Or Shlomi Shelley Lee Davies distributor    = Studio At 58 released       =   runtime        = 72 minutes country        = United Kingdom language       = English budget         = gross          = 
}} nutritional and environmental benefits of adopting a whole foods, Veganism|plant-based diet based on the research of T. Colin Campbell, Caldwell Esselstyn and Gidon Eshel.     The film also features the views of Peter Singer. 

According to Shelley Lee Davies, the film purposely does not cover any purported animal welfare arguments for adopting a plant-based (veganism|vegan) diet, but concentrates on the health and environmental reasons instead. 
 House of Commons in May 2011.  

==Critical reception==
Jamie Russell of Total Film called it "Forceful stuff, though we can take or leave the kale sandwiches." 

Cath Clarke of The Guardian gave the movie 2 stars out of 5 and, speaking as a vegetarian convert, speculated on her reasons for disliking the film: "Maybe its the tippy-toe, softly-softly tone, sprinkling inspiration between the science, with visits to boutique organic farms and kooky vegan cupcake bakeries. Possibly it ought to come with a warning: contains traces of smugness. 

Charlotte OSullivan of the London Evening Standard writes, "PlanEat is on a serious mission but its lack of focus is infectious. My big question as the credits rolled: why are so many vegan chefs covered in tattoos?" 

Christopher Long of Movie Metropolis gave the movie a 5 out of 10 saying, "a movie by true believers for true believers, and I am not one." 

==References==
 

==External links==
*  
*  
*  

 

 
 
 