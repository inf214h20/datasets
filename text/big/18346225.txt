The Crowd Roars (1932 film)
{{Infobox film 
| name = The Crowd Roars
| caption = 
| director = Howard Hawks	
| producer = 	 John Bright Niven Busch Kubec Glasmon Seton I. Miller	
| starring =James Cagney Joan Blondell
| music = Bernhard Kaun
| cinematography = Sidney Hickox John Stumar Thomas Pratt
| distributor = Warner Bros.
| released =  
| runtime = 85 minutes
| country = United States
| language = English
| budget = 
}}

The Crowd Roars is a 1932 film directed by Howard Hawks starring James Cagney and featuring Joan Blondell, Ann Dvorak, Eric Linden, Guy Kibbee, and Frank McHugh. 
 board track Pat OBrien in Cagneys role, Ann Sheridan in Blondells role, and McHugh playing the same role he played in the original.

==Plot==
Motor racing champion Joe Greer (James Cagney) returns home to compete in an exhibition race featuring his younger brother Eddie, who has aspirations of becoming a champion. Joes misogynistic obsession with "protecting" Eddie from "women" causes Joe to interfere with Eddies relationship with Anne (Joan Blondell), leading to estrangement between Joe and Eddie, and between Joe and his longtime girlfriend Lee (Ann Dvorak), who is made to feel "not good enough" to be around Eddie.

During the race, a third driver, Spud Connors, wrecks and is burned alive. Driving lap after lap through the flames and the smell of burning flesh (and maybe past the burning body) while blaming himself for the accident, Joe loses his will to race. Eddie goes on to win. Afterward, Joes career plummets as Eddies rises. The power of love eventually triumphs and Joes career and his relationships with Lee and Eddie are rehabilitated.

Sentimentalism is downplayed in this "pre-Code" film. The lingering stench of Spuds burning body is implied strongly by the horrified expression on each drivers face as he passes through the smoke and tongue of burning gasoline that marks the wreck site, sometimes pushing his scarf against his nose.

==Cast (in credits order)==
*James Cagney as Joe Greer
*Joan Blondell as Anne Scott
*Ann Dvorak as Lee Merrick
*Eric Linden as Edward Eddie Greer
*Guy Kibbee as Pop Greer
*Frank McHugh as Spud Connors Billy Arnold as Himself
*Leo Nomis as Jim
*Fred Frame as Himself
*Ralph Hepburn as Himself
*Wilbur Shaw as Himself
*Shorty Cantlon as Himself
*Mel Keneally as Himself
*Stubby Stubblefield as Himself
*Spider Matlock as Himself

== Notes ==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 