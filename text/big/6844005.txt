For All Time
For All Time was a 2000 made-for-TV-movie released in 2000 starring Mark Harmon, Mary McDonnell, and Catherine Hicks. It was based on The Twilight Zone episode, A Stop at Willoughby  written by Rod Serling. The teleplay was by Vivienne Radkoff and it was directed by Steven Schachter. 

== Plot summary ==
Charles Lattimer (Mark Harmon) is an everyday man facing middle age and a marriage to Kristen (Catherine Hicks) coming to an end.  He stumbles across a time slip that occurs on one of his regular train rides, as the train goes through a tunnel. Coming across an antique watch, he learns it allows him to get off the train during the time slip, whereupon he finds himself back in the 1890s. Before long he finds a newfound love, played by Mary McDonnell, and a new purpose there. The watch gets broken and complications occur when the portal back to the past starts to close, leading him to a decision that could leave him stranded out of his own time.

==Awards and nominations==
 
Nominated for the Golden Reel Award in 2001.

Best Sound Editing - Television Movies and Specials (including Mini-Series) - Music 
Chris Ledesma (music editor)  
Bob Beecher (music editor)

Best Sound Editing - Television Movies and Specials - Effects & Foley 
Mark Friedgen (supervising sound editor)  
Kristi Johns (supervising adr editor)  
Anton Holden (sound editor)  
Tim Terusa (sound editor)  
Rusty Tinsley (sound editor)  
Michael Lyle (sound editor)  
Bill Bell (sound editor)  
Mike Dickeson (sound editor)  
Bob Costanza (sound editor)  
Gary Macheel (sound editor)  
Richard S. Steele (sound editor)

==References==
 

 