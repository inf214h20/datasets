Kallazhagar
{{Infobox film
| name = Kallazhagar
| image =
| caption = Bharathi
| writer = Bharathi J. Mahendran Laila
| producer = Hendry Deva
| cinematography = Thangar Bachan
| editing = Peter Pappiah
| studio = Pankaj Productions
| released = 6 February 1999
| runtime = 141 minutes Tamil
|country = India
| budget =
| website =
}}
 Laila in Deva composed the score and soundtrack for the film.  The film released on 6 February 1999 to average reviews. 

==Cast==
 
*Vijayakanth as Kannan and Kamal Kannan Laila as Andaal
*Manivannan
*Nassar
*Major Sundarrajan
*Thilakan
*Sonu Sood
*Ashwini Sumithra
*S. N. Lakshmi
*Melanie
*Riyaz Khan
*Vaiyapuri
*Crane Manohar
*R. N. R. Manohar
*Scissor Manohar
*Sarath
*Napoleon
*Saravanan
*John Babu
*R. R. Sheela
*Bharathi
*Rocky Rajesh
*V. M. Mani
*Mohandass
 

==Production==
Laila Mehdin|Laila, who had appeared in other regional Indian films, opted to make her debut in Tamil films with Kallazhagar after she had famously rejected a string of other Tamil offers including V.I.P (film)|VIP.  The actress also turned down a role in Ajith Kumars Unnaithedi, insistent that Kallazhagar should be her first release. 

An elephant called Appu was brought in from Thrissur in Kerala, where the elephant formed one of a stable maintained by the famed Paaramekaavu temple, which forms the venue of the yearly Thrissur Pooram festival. 

==Release==
The film was initially scheduled to release on January 14, 1999 coinciding with the festival of Thai Pongal though became delayed due to problems at the censor. The film was rejected by Indian censors, because of its potential to spark religious conflicts - with particularly a scene in which some Muslim extremists masquerade themselves as religious Hindus and join in the celebration of a major festival in a temple - being highlighted as a concern. The team subsequently had to adapt the concept partially. 

The success of the film prompted the producer Henry to sign Vijayakanth for his next film. The film also created demand for Laila as a lead heroine and she shortly after signed on to appear in a role in Mudhalvan (2000). 

==References==
 

 
 
 
 
 


 