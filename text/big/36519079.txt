Lost on Journey
{{Infobox film
| name           = Lost on Journey
{{Infobox name module
 | traditional    = 人在囧途
 | simplified     = 
 | chinese        = 
 | pinyin         = Rén zài jiǒng tú
 | jyutping       = 
 | poj            = 
 | translation    =
}}
| image          = Lost on Journey poster.jpg
| caption        = 
| director       = Raymond Yip
| producer       = Manfred Wong Zhang Tianshu
| writer         = Manfred Wong Tian Yusheng Shi Chenfeng Xu Yuanfeng Xu Zheng Wang Baoqiang Li Man Li Xiaolu
| music          = Chan Kwong-wing
| cinematography = Gao Mi
| editing        = Angie Lam
| studio         = 
| distributor    = 
| released       =   (China)
| runtime        = 91 minutes
| country        = China
| language       = Mandarin
| budget         =  &nbsp;million   
| gross          =  &nbsp;million 
}} Xu Zheng Chun Yun. During the Spring Festival, transportation is a serious problem in mainland China as everyone wants to reunite with their family for the Chinese New Year celebrations.
 John Hughess 1987 film Planes, Trains and Automobiles in depicting the journey of two mismatched companions.        A sequel, Lost in Thailand, was released in 2012 to great commercial success in China;  the sequel also stars Xu Zheng and Wang Baoqiang and was directed and written by Xu himself.

==Plot== Xu Zheng) is a wealthy and arrogant businessman who treats his employees poorly and has a secret mistress, Manni (Li Man). Although his mistress wants him to spend Chinese New Year with her in Shijiazhuang, Hebei province, Li feels obligated to return to his hometown Changsha to celebrate Chinese New Year with his wife Meili (Zuo Xiaoqing), daughter, and mother. Arriving at the crowded airport, Li finds he has been mistakenly booked in the economy class and sits next to the first-time flyer Niu Geng (Wang Baoqiang), who is a naïve and gullible migrant worker. Niu is also setting out to Changsha to claim his defaulted wages as a milk extraction technician.

As Li and Geng’s lives crosses paths, the two embark in a chaotic journey together. The duo experiences a seemingly never-ending streak of bad luck as they try to make their way to Changsha.

After the airplane takes off, the Changsha airport experiences a heavy snowstorm so their flight is cancelled and they are forced to turn back. Li then manages to acquire a train ticket from a tout. On board, he realizes the ticket he purchased was a counterfeit, which just happens to be the same seat that Niu bought. As some train tracks collapsed due to a landslide, the two leave the train and attempt to take the bus.

When they are about to get on the bus to Changsha, Niu is touched by a female beggar (Zhang Xinyi) and wants to give her money. Li discourages Niu from doing so and calls him an idiot for believing in the lies of the beggar. Niu still ends up giving her money, but the beggar does not return.

The closing of a bridge spanning over the Yangtze River causes them to stop at Wuhan where they spot the beggar. The two go on a fanatic chase to pin down the scam artist. The chase leads them into a peaceful complex and Li goes into the room full of children quietly drawing. The beggar was a teacher who took care of these children and needed money to pay for an operation for a blind child. The duo is touched and gives her all their money.

Continuing on their farcical adventure, Niu convinces Li to buy a lottery ticket. To their surprise, they win the first prize, which is a car. They decide to drive the car to Changsha. Suffering from a lack of sleep, Li decides to let Niu drive temporarily. However, Niu falls asleep and drives the car into a ditch. The two frantically jumps out of the car in anticipation of an explosion as they often see in movies, but nothing happens. They have a heartfelt conversation as they decide to spend the night in the middle of nowhere.

The next day they are able to hitchhike on a tractor transporting chickens. Finally they reach Changsha covered in chicken feathers. Li is surprised to find out that his mistress is also in the city to give him a surprise.

The trip was a voyage of self-discovery for Li as by the end, he changed. He went from scoffing beggars to believing that truthfulness still exists in the world; he changed from a cold-blooded boss to a real human being. Finally realizing what is important to him, he breaks up with his mistress and returns to his family. Li returns money to Niu and calls him his "creditor". Although Niu was not his real creditor, what he owed Niu were the lessons he taught him throughout the journey and the epiphanies regarding the importance of truthfulness and trust.

==Cast== Xu Zheng as Chenggong Li
* Wang Baoqiang as Geng Niu
* Li Man as Manni (Li Chenggongs mistress)
* Zhang Chao as Mr. Handsome
* Zhang Xinyi as the Fraud women
* Li Xiaolu
* Chen Xiao
* Huang Xiaolei
* Zuo Xiaoqing
* Sun Haiying Ma Jian

==Reception==
 
Reviewers were generally complimentary, citing good chemistry between the two leads,    and the realistic depiction of modern Chinese society,  especially the trials of Spring Festival,  although one thought the inconsistencies in the plot were slightly distracting.  Another review stated the plot was deeper than the superficial farce of traveling woes in rallying human spirit to overcome an indifferent world. 

==References==
 

==External links==
*  

 
 
 
 