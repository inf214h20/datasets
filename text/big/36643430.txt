In Search of La Che
{{Infobox film
| name           = In Search of La Che
| image          = In Search Of La Che Poster 2015 Poster.jpg
| caption        = 
| director       = Mark D. Ferguson
| writer         = Andy S. McEwan Mark D. Ferguson Chris Quick
| starring       = Duncan Airlie James Paul Massie Craig Walker Neil Francis
| music          = Paul Massie
| editing        = Chris Quick
| released       =  
| studio         = Quick Off The Mark Productions
| runtime        = 71 minutes
| country        = United Kingdom
| language       = English
| budget         =
}}
In Search of La Che  is a Scottish spoof documentary film directed by Mark D. Ferguson. The screenplay was written by Andy S. McEwan.

In Search of La Che premiered at the Glasgow Film Theatre  on 9 November 2011.

==Plot==
In Search of La Che follows he journey of hardcore music fan John Tavish (Played by Duncan Airlie James) on his quest to find out the circumstances behind the disappearance of Scotland most famous (fictional) son of rock and roll, Roxy La Che.

Searching the internet, John finds that Roxys life hasnt been very well documented and comes across very little other than an unofficial fan page for the rock star. The owner of the site, Larry, agrees to meet with John but his mental instability becomes visibly clear during the interview and John decides to make a quick exit after obtaining the information he needed. John tracks down Arhcie Murno, a pub landlord who gave Roxy his first taste of music fame when he persuaded him to take part in a karaoke night. Archie gives John a history of Roxys troubled upbringing on Pishi Island where the local economy was decimated by the actions of British Prime Minister Margaret Thatcher. Without adequate means to make a living on the island, Roxy move to Glasgow and managed to transform his life through his love for music. During the interview, John discovers for the first time that Roxy was in fact signed by two record companies instead of one. The first record label was run by Hector Blitzkrieg Wallace who was a Nazi sympathiser and forced Roxy to sing songs about hatred. Roxys time with Blitzkrieg was brief as he later jumped ship to Met Records run by Gary Pringle.

After speaking with both of Roxys managers, John is made aware of Roxys various battles with depression which often lead to long stays at rehabilitation units. Moving from sofa to sofa, Roxys last known appearance was with his old roadie friend Shimmy Quiffer. Shimmy explains to John that he had been staying with him for six months during the early 1990s but upon returning home one day, Roxy was packed up ready to go but couldnt tell Shimmy where he was going. Thinking the trail has gone cold, John receives an unopened letter from Shimmy that arrived for Roxy shortly after his departure. The letter is written by Alex H. Croy, an old friend of Roxy telling him that the pair should meet up as he is now staying in a hospice not far from Shimmys location.

John visits the hospice in the hope of meeting his idol but his hopes are dashed when it is revealed that Roxy La Che had died in 2002, twenty years after the release of his first album.

==Main cast==
{{multiple image
   | direction = horizontal
   | footer = Duncan Airlie James (left) and Steve Nallon (right). James plays the lead role of John Tavish and Nallon supplies the voice of Prime Minister Margaret Thatcher in the film.
   | image1 = Duncan_Airlie_James_at_In_Search_Of_La_Che_Premiere.jpg
   | width1 = 300
   | image2 = Steve_Nallon.jpeg
   | width2 = 200
  }}
*Duncan Airlie James as John Tavish
*Paul Massie as Roxy La Che
*Neil Francis as Archie Munro
*Kyle Calderwood as Larry
*Craig Walker as Hector "Blitzkrieg"" Wallace
*Dave Wills as Gary Pringle
*Allan Thornton as Shimmy Quiffer
*Elizabeth Baillie as The P.A / Debbie (Voice)
*Craig Maclachlan as The Foreign sound man
*James Frame as Kirkwood
*Mark D. Ferguson as Mark
*Andy S. McEwan as Whippy / Nelson Sheffield 
*Steve Nallon as Margaret Thatcher (Voice)
*Lynn Murray as The Nurse
*Andrew ODonnell as Habrbour Master (voice)
*Chris Quick as Wermit (voice)
*Euan Cuthbertson as Neil Kinnock (voice)
*Nichola Hosie as Miss Gormley
*Lisa Massie as The Receptionist
*Ronnie Smith as The Doctor

==Music and soundtrack==
 
The majority of the soundtrack for In Search of La Che was composed and performed by Paul Massie. Amongst the tracks he recorded were, Beuracatic Greenhouse, Biological Memory, Cheerful Bus In A Big Gay City and Free Now which was used over the end credits.

Additional performances came from Glaswegian bands .Scores and Sound Over Silence. Paul Slevin composed the music for Blitzkriegs backstory. 

==Release and reception==
 , Paul Massie (Bottom L-R) Andy S. McEwan, Duncan Airlie James, Mark D. Ferguson]]
The film premiered at the Glasgow Film Theatre  on 9 November 2011. It was the first leading role of former thai kickboxing champion, Duncan Airlie James. 

The film received a positive review from Flicker Magazine  in the United Kingdom but in the United States the film failed to amuse American audiences with many blaming the thick Scottish dialect.   Scott Knopf from Film Threat wrote:   Despite this, the film was selected to appear in the spring showcase of the 2014 American Online Film Awards. 

When the DVD was released the following year, the film was dedicated to Hugh Campbell, the father of Duncan Airlie James. During production, Campbells house was used as the home of the main character John Tavish. He died in December 2011, a month after the films premiere. 

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 