Your Job in Germany
{{Infobox film
| name = Your Job in Germany
| image = | image size = | caption = 
| director =  }}
| producer = 
| writer =  }}
| narrator = | starring = | music = | cinematography = | editing = | distributor = | released = 
| runtime = 12:49
| country = United States English
| budget = | gross = | preceded by = | followed by = 
}}
 Victory in occupation duty in Germany.  The film was made by the military film unit commanded by Frank Capra and was written by Theodor Geisel,    better known by his pen name Dr. Seuss.

== Content ==
 
 US Army Signal Corps. It was criticized by one commentator as a "bitter and angry anti-German propaganda film" that characterized the post-war German mind as "diseased". Robert Niemi, "History in the media: film and television history in the media", p.84 

The film urged against fraternization with the German people, who are portrayed as thoroughly untrustworthy. It reminds its viewers of Germanys history of aggression, under "F&uuml;hrer Number 1" Otto von Bismarck, "F&uuml;hrer Number 2" Kaiser Wilhelm II and "F&uuml;hrer Number 3" Adolf Hitler. It argues that the German youth are especially dangerous because they had spent their entire lives under the Nazi regime.

The policy of Fraternization#Military non-fraternization|non-fraternisation where US soldiers were forbidden to speak even to small children was first announced to the soldiers in this movie:
:"The Nazi party may be gone, but Nazi thinking, Nazi training and Nazi trickery remains. The German lust for conquest is not dead.... You will not argue with them. You will not be friendly.... There will be no fraternization with any of the German people"&nbsp; Judith Morgan, Neil Morgan, "Dr. Seuss & Mr. Geisel: a biography", p. 111–113. 

The basic theme that the German people could not be trusted derived from the peace policy that emerged from the Second Quebec Conference. 

The movie was first screened to the top US generals, including Dwight D. Eisenhower. George Patton reportedly walked out of the screening he attended, saying "Bullshit!". 

== Hitler Lives? ==
 Academy Award (Oscar) for Documentary Short Subject.  

== In popular culture ==
 Cabaret Voltaire from their album Code (album)|Code. 

== See also ==
*Our Job in Japan, a companion film to Your Job In Germany also written by Geisel.
*Here Is Germany
*Death Mills
*List of Allied propaganda films of World War II

== References ==
 

== External links ==
*  
*  
*   at Der Spiegel website.

 

 
 
 
 
 
 
 
 