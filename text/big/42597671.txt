Pride (2014 film)
 
 
{{Infobox film
| name           = Pride
| image          = Pride poster.jpg
| alt            = 
| caption        = UK release poster
| director       = Matthew Warchus
| producer       = David Livingstone
| writer         = Stephen Beresford
| starring       =  
| music          = Christopher Nightingale
| cinematography = Tat Radcliffe
| editing        = Melanie Oliver
| studio         =  
| distributor    = Pathé
| released       =  
| runtime        = 120 minutes  
| country        =  
| language       = English
| budget         = 
| gross          = $14.7 million   
}} historical comedy-drama film written by Stephen Beresford and directed by Matthew Warchus. It was screened as part of the Directors Fortnight section of the 2014 Cannes Film Festival,       where it won the Queer Palm award.  Writer Stephen Beresford said a stage musical adaptation involving director Matthew Warchus was being planned. 
 Best British Best Actress Outstanding Debut by a British Writer, Director or Producer.

==Plot== British miners National Union of Mineworkers was reluctant to accept the groups support due to the unions public relations worries about being openly associated with a gay group, so the activists instead decided to take their donations directly to Onllwyn, a small mining village in Wales, resulting in an alliance between the two communities. The alliance was unlike any seen before but was successful. 

==Cast==
 
* Bill Nighy as Cliff
* Imelda Staunton as Hefina Headon
* Dominic West as Jonathan Blake
* Paddy Considine as Dai Donovan Andrew Scott as Gethin Roberts, who in the film is Jonathans partner.  In real life, Jonathans partner was also a member of LGSM named Nigel Young. His character was combined with Gethins.    George MacKay as Joe "London Borough of Bromley|Bromley" Cooper, one of the few fictional characters created for the film to be an audience surrogate. 
* Ben Schnetzer as Mark Ashton
* Joe Gilgun as Mike Jackson
* Faye Marsay as Stephanie "Steph" Chambers Freddie Fox as Jeff Cole
* Monica Dolan as Marion Cooper Liz White as Margaret Donovan
* Karina Fernandez as Stella
* Jessie Cave as Zoe
* Jessica Gunning as Siân James (politician)|Siân James
* Rhodri Meilir as Martin
* Russell Tovey as Tim
* Lisa Palfrey as Maureen
* Menna Trussler as Gwen
* Jack Baggs as Gary
* Kyle Rees as Carl
* Chris Overton as Reggie Blennerhassett
* Joshua Hill as Ray Aller
* Feargal Quinn as Jimmy Somerville
* Deddie Davies as Old Lady
 

==Release==

===Cinema release===
Pride premiered at the 2014 Cannes Film Festival, where it received a standing ovation and won the Queer Palm award.    The film was also screened at the 2014 Toronto International Film Festival, with the Washington Post reporting that Pride was "hugely popular with preview and festival audiences".    It was released to cinemas throughout the UK on 12 September 2014.    In France the film received its release on 17 September.    The distribution of the film in the UK and France was handled by Pathé.    CBS Films acquired the distribution rights for the film in the United States.   

The film received a limited release in the US on 26 September 2014, being screened in New York, Los Angeles and San Francisco.      

===Controversies===
In the UK the film received a 15 certificate by the British Board of Film Classification for "occasional strong language" and two scenes of a sexual nature;  one scene in a gay club where men are depicted "wearing bondage clothing",  and a comedic scene where some of the characters discover a pornographic magazine in a bedroom. 
 MPAA gave R rating, the nearest US equivalent to the UKs 15 certificate. This reflects common practice; the British Film Institute states that "most" 15 certificate films are R-rated in the US. 

The Independent published an article calling the MPAAs rating "draconian",    alleging that the R ratings higher age restriction ("no unaccompanied under-17s") was specifically applied due to gay content. The Independent s article formed the basis for a The Guardian|Guardian article    which further compounded the issue by mistakenly stating that the MPAA had given the film an Motion Picture Association of America film rating system#X replaced by NC-17|NC-17 rating. This error was corrected a few days later.

In January 2015, it was reported that the cover of the US DVD release of the film makes no mention of the gay content. A standard description of "a London-based group of gay and lesbian activists" was reduced to "a group of London-based activists", and a lesbian and gay banner was removed from a photograph on the back cover. 

==Reception==

===Box office===
In its opening weekend Pride took Pound sterling|£718,778 at the UK box office.    The film was the third highest-grossing release of the weekend, behind Lucy (2014 film)|Lucy in second place and The Boxtrolls, which debuted at the top of the box office.  During its second weekend at the UK box office Pride retained its third place position on the charts, with takings of £578,794.    The Guardian reported that the film had a drop of just 12% in takings during its second weekend at the box office, as well as a strong weekday performance at the box office, commenting: "After a somewhat shaky start, Matthew Warchus film is displaying signs of solid traction with audiences."    In its third weekend at the UK box office, Pride dropped to sixth in the charts with takings of £400,247 over the weekend period.    By its fourth weekend Pride had dropped to tenth place in the box office, with takings of £248,654 and an overall UK gross totalling £3,265,317.   

In the US, Pride grossed $84,800 from six theatres in its opening weekend.    The film expanded slowly, adding theatres in existing markets for its second weekend followed by release in additional cities from 10 October. 

===Critical response===
Pride has been met with critical acclaim. The review aggregator   gave the film an aggregate score of 79/100 based on 36 reviews, indicating "generally favorable reviews." 

Geoffrey Macnab, of The Independent, noted how Pride followed on from other British films such as The Full Monty, Brassed Off and Billy Elliot as "a story set in a Britain whose industrial base is being shattered".    Macnab, who gave the film a five-star review, praised the screenplay for combining "broad comedy with subtle observation" and noted that director Matthew Warchus "relishes visual contrasts and jarring juxtapositions" throughout the film.  Macnabs review stated that Pride retained its humour and accessibility without trivialising the issues addressed in the film.

 ) and hes fantastic here".   

Paul Byrnes in The Sydney Morning Herald described the film as "dry, surprising, compassionate, politically savvy, emotionally rewarding and stacked to the gills with great actors doing solid work"   

Nigel Andrews, writing for the Financial Times, gave the film one star out of five, describing it as "a parade of tricks, tropes and tritenesses, designed to keep its balance for two hours atop a political correctness unicycle". 

Gareth Kingston of essentialmoviereviews.com gave Pride 4.5 stars out of five, and said of the film "As Billy Braggs There Is Power In A Union plays at the end of the movie, you realise that you have seen a special movie about a special coming together of communities."   

===Accolades===
{|class="wikitable plainrowheaders sortable"
|- style="background:#ccc; text-align:center;" Award
! Date of ceremony Category
! Recipients and nominees Result
|- 2014 Cannes Cannes Film Festival  25 May 2014
| Queer Palm
| Pride
|  
|-
| Directors Fortnight
| Pride
|  
|-
| Flanders International Film Festival Ghent 2014  
| 27 October 2014
| Audience award "Port of Ghent"
| Pride
|  
|-
| Leiden International Film Festival 2014 
| 10 November 2014
| Audience Award
| Pride
|  
|- British Independent British Independent Film Awards  7 December 2014 Best British Independent Film
| Pride
|  
|-
| Best Director 
| Matthew Warchus
|  
|-
| Best Screenplay
| Stephen Beresford
|  
|- Best Supporting Actress
| Imelda Staunton
|  
|- Best Supporting Actor Andrew Scott
|  
|- Best Supporting Actor
| Ben Schnetzer
|  
|-
| Most Promising Newcomer
| Ben Schnetzer
|  
|- Golden Globe Awards  
| 11 January 2015 Best Motion Picture – Musical or Comedy
| Pride
|  
|- London Film Critics Circle Awards
| 18 January 2015 British Film of the Year
| Pride
|  
|-
! scope="row" rowspan="3"| Dorian Awards 2014    
! scope="row" rowspan="3"| 20 January 2015
| LGBTQ Film of the Year
| Pride
|  
|-
| Unsung Film of the Year
| Pride
|  
|-
| Film of the Year
| Pride
|  
|- Artios Awards    
| 22 January 2015
| Outstanding Achievement in Casting - Feature Film Studio or Independent Comedy
| Fiona Weir
|  
|- British Academy Film Awards    
! scope="row" rowspan="3"| 8 February 2015 Best British Film
| Pride
|  
|- Best Actress in a Supporting Role
| Imelda Staunton
|  
|- Outstanding Debut by a British Writer, Director or Producer
| Stephen Beresford and David Livingstone
|  
|}

==Soundtrack==
{{Infobox album  
| Name       = Pride  
| Type       = soundtrack
| Artist     = Various artists
| Cover      = 
| Alt        = 
| Released   =  
| Recorded   = 
| Genre      = 
| Length     =   Universal Music
| Producer   = Christopher Nightingale
| Last album = 
| This album = 
| Next album = 
}}
{{Track listing
| collapsed       = yes
| headline        = Side one
| total_length    = 01:17:31
| writing_credits = yes
| title1          = I Want to Break Free Queen
| length1         = 4:23
| title2          = Shame, Shame, Shame (Shirley & Company song)|Shame, Shame, Shame
| writer2         = Shirley & Company
| length2         = 3:47
| title3          = Why? (Bronski Beat song)|Why?
| writer3         = Bronski Beat	
| length3         = 4:04
| title4          = Love & Pride King	
| length4         = 3:21 Relax
| writer5         = Frankie Goes to Hollywood	
| length5         = 3:57
| title6          = Tainted Love
| writer6         = Soft Cell	
| length6         = 2:37
| title7          = West End Girls
| writer7         = Pet Shop Boys	
| length7         = 4:01
| title8          = Karma Chameleon
| writer8         = Culture Club	
| length8         = 4:02
| title9          = Pull Up to the Bumper
| writer9         = Grace Jones	
| length9         = 4:42 You Spin Me Round Dead or Alive	
| length10        = 3:19 Freedom
| writer11        = Wham!	
| length11        = 5:20
| title12         = I Second That Emotion
| writer12        = Smokey Robinson	
| length12        = 2:42 Walls Come Tumbling Down
| writer13        = The Style Council	
| length13        = 3:23 Temptation
| writer14        = Heaven 17	
| length14        = 3:26
| title15         = Love Will Tear Us Apart
| writer15        = Joy Division	
| length15        = 3:21
| title16         = Pale Shelter
| writer16        = Tears for Fears	
| length16        = 4:27 Making Plans For Nigel
| writer17        = XTC
| length17        = 4:12
| title18         = Our Lips Are Sealed
| writer18        = Fun Boy Three	
| length18        = 2:53
| title19         = There Is Power in a Union
| writer19        = Billy Bragg	
| length19        = 2:49
| title20         = Solidarity Forever
| writer20        = Pete Seeger	
| length20        = 2:51
| title21         = Across the Great Divide
| writer21        = Frank Solivan
| length21        = 3:54
}}
{{Track listing
| collapsed       = yes
| headline        = Side two
| total_length    = 01:15:38
| writing_credits = yes
| title1          = Two Tribes
| writer1         = Frankie Goes to Hollywood	
| length1         = 3:24 Blue Monday
| writer2         = New Order	
| length2         = 4:04
| title3          = For a Friend
| writer3         = The Communards	
| length3         = 4:36
| title4          = All of My Heart ABC	
| length4         = 4:49
| title5          = Do Ya Wanna Funk Sylvester	
| length5         = 3:29
| title6          = Red Red Wine
| writer6         = UB40	
| length6         = 3:00
| title7          = Genius of Love
| writer7         = Tom Tom Club	
| length7         = 3:28 Homosapien
| writer8         = Pete Shelley	
| length8         = 4:34
| title9          = Hard Times
| writer9         = The Human League	
| length9         = 4:54
| title10         = I Travel
| writer10        = Simple Minds	
| length10        = 4:03
| title11         = A New England
| writer11        = Kirsty MacColl
| length11        = 3:48
| title12         = Waiting for the Love Boat Associates	
| length12        = 4:26 Ghosts
| Japan	
| length13        = 4:32
| title14         = Living on the Ceiling Blancmange	
| length14        = 4:03
| title15         = Robert De Niros Waiting...
| writer15        = Bananarama	
| length15        = 3:41
| title16         = Keep On Keepin On!
| writer16        = The Redskins	
| length16        = 3:52
| title17         = Are You Ready to Be Heartbroken
| writer17        = Lloyd Cole and the Commotions	
| length17        = 3:05
| title18         = Across the Bridge
| writer18        = Christopher Nightingale	
| length18        = 1:40
| title19         = Autumn Montage
| writer19        = Christopher Nightingale	
| length19        = 1:25
| title20         = Homecoming
| writer20        = Christopher Nightingale	
| length20        = 2:50
| title21         = Bread and Roses
| writer21        = Bronwen Lewis 
| length21        = 1:55
}}

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 