Charlie's Country
 
 
{{Infobox film
| name           = Charlies Country
| image          = CharliesCountry.jpg
| caption        = Film poster
| director       = Rolf de Heer
| producer       = Rolf de Heer Peter Djigirr Nils Erik Nielsen
| writer         = Rolf de Heer David Gulpilil
| starring       = David Gulpilil Luke Ford
| music          = Graham Tardif
| cinematography = Ian Jones 
| editing        = Tania Nehme 
| distributor    = 
| released       =  
| runtime        = 108 minutes
| country        = Australia Yolngu Matha English
| budget         = 
}}
Charlies Country is a 2013 Australian drama film directed by Rolf de Heer. It was selected to compete in the Un Certain Regard section at the 2014 Cannes Film Festival    where David Gulpilil won the award for Best Actor.  It was also screened in the Contemporary World Cinema section at the 2014 Toronto International Film Festival.   
 Best Foreign Language Film at the 87th Academy Awards, but was not nominated.   

==Plot==
Charlie, an Aboriginal man who lives in in  , 15 October 2013  After his spear is confiscated by the police who think it is a weapon, he decides to leave his Aboriginal community and go to the bush.  He falls ill and is rushed to the hospital in Darwin, Northern Territory|Darwin.  Shortly after, he befriends a woman who buys alcohol illegally for other Aboriginals, and he gets arrested.  As a result, he is sent to prison. 

==Cast==
* Peter Djigirr as Black Pete Luke Ford as Luke
* Jennifer Budukpuduk Gaykamangu as Faith
* David Gulpilil as Charlie
* Peter Minygululu as Old Lulu
* Ritchie Singer as Darwin Doctor

==Critical reception==
Charlies Country currently holds an approval rating of 92% on Rotten Tomatoes. 

Jane Howard of  , 17 July 2014  He added, "The level of trust between actor and director here is part of the reason this work will live on."  Similarly, writing for  , 22 May 2014  

In  , 22 May 2014  In  , 19 July 2014  He added that some scenes were likely to provoke racism in some viewers, thus helping them question their own ingrained prejudices. 

However, writing for the  , 17 July 2014  He called it, "another disappointment in that category of ambitious Australian filmmaking that’s about trying to make art, as well as entertain."  He added that the film lacked "ambiguity" adding that there "are thinly drawn racist characters and an inability to render different tones in the one visual idea."  Moreover, he disageed with other reviewers about de Heer and Gulpilis close relationship: for Di Rosso, "de Heer hasn’t shown enough faith in his central performer and the collection of beautiful and ugly landscapes he’s drawn together."  

===Accolades===
{| class="wikitable"
|-
! Award
! Category
! Subject
! Result
|- AACTA Awards  (4th AACTA Awards|4th)  AACTA Award Best Film Nils Erik Nielsen
| 
|- Peter Djigirr
| 
|- Rolf de Heer
| 
|- AACTA Award Best Direction
| 
|- AACTA Award Best Original Screenplay
| 
|- David Gulpilil
| 
|- AACTA Award Best Actor
| 
|- AACTA Award Best Sound James Currie
| 
|- Tom Heuzenroeder
| 
|- Asia Pacific Screen Awards Asia Pacific Achievement in Directing Rolf de Heer
| 
|- Asia Pacific Best Actor David Gulpilil
| 
|- Cannes Film Festival Un Certain Regard - Best Actor
| 
|- Un Certain Regard Award Rolf de Heer
| 
|-
|}

==See also==
* List of submissions to the 87th Academy Awards for Best Foreign Language Film
* List of Australian submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*  

 
 

 
 
 
 
 
 
 
 