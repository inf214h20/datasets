The Baby-Sitters Club (film)
{{Infobox film
| name           = The Baby-Sitters Club
| image          = Babysitters club film.jpg
| caption        = Theatrical release poster
| director       = Melanie Mayron
| starring       = Schuyler Fisk Bre Blair Tricia Joe Rachael Leigh Cook Larisa Oleynik Stacy Linn Ramsower Zelda Harris
| writer         = Dalene Young
| based on       = The Baby-sitters Club by Ann M. Martin
| producer       = Peter O. Almond  Jane Startz Marc Abraham Thomas Bliss
| music          = David Michael Frank
| cinematography = Willy Kurant
| editing        = Christopher Greenbury
| studio         = Beacon Pictures
| distributor    = Columbia Pictures
| runtime        = 85 minutes
| released       = August 18, 1995
| country        = United States
| language       = English
| gross          = $9,685,976 
}}
 family comedy feature film directorial debut. It is based on The Baby-Sitters Club series of novels and is about one summer in the girls lives in the fictional town of Stoneybrook, Connecticut. The film was shot in Guelph, Ontario|Guelph, Ontario.

==Plot==
Kristy Thomas, president of "The Baby-Sitters Club", decides to open a day camp for their child clients. Her best friend, Mary Anne Spier, along with Mary Annes stepsister Dawn Schafer, offer their parents backyard to serve as the camp site. All of the club members (Kristy Thomas, Mary Anne Spier, Dawn Schafer, Claudia Kishi, Stacey McGill, Mallory Pike, and Jessica "Jessi" Ramsey) vow to keep a close eye out for misbehaving kids.

Meanwhile, Kristy faces problems when she meets her estranged father (who abandoned her family seven years ago and started a new family in California), and faces a dilemma about telling her friends and family about this. Mary Anne is the only one she tells of the visit, and she too is under pressure as the curiosity of her friends grows. The girls perform a rap song for Claudia who is stuck in summer school and is forced to retake a test, or faces repeating a grade and being forced to drop out of the club. 

Stacey McGill has a crush on a seventeen-year-old boy named Luca. As their relationship ensues, she faces problems telling him about her diabetes, and later, her age. This is revealed after a trip to a New York City club, in which a bouncer does not allow her into a club because she is underage. Luca is outraged, unable to believe that Stacey is thirteen years old. 

Meanwhile, Dawn must face her neighbor, Mrs. Haberman, who becomes increasingly upset because of the camp activities that are taking place next door. Also, Mallory is in the process of writing her first novel while Jessi continues to dance her way through life and closer to her dream of becoming a professional dancer. 

It is Kristys thirteenth birthday and she has arranged to go to an amusement park with her father. Promising her friends she would make it to her own party, Kristy goes to meet her father, but he does not show up. She begins to walk home until her friends show up in Lucas car after Mary Annes confession about the return of Kristys father. 

Luca drives the girls back to Mallorys parents cabin and present Kristy with a half-melted birthday cake. As Stacey is saying goodbye to Luca, he tells her that he will be coming to Stoneybrook again next year. Delighted, Stacey tells him that she will be fourteen years old when he returns. They share a kiss just before Luca departs. 

At the end of the movie, the girls, in return for making Mrs. Habermans summer so miserable with their summer camp, give the greenhouse to her. Meanwhile, Kristy witnesses a miracle when Jackie Rodowsky hits his first home run, hitting Cokie Mason, who is sitting in a tree nearby, in the process.

==Cast==
* Schuyler Fisk as Kristy Thomas
* Bre Blair as Stacey McGill
* Rachael Leigh Cook as Mary Anne Spier
* Larisa Oleynik as Dawn Schafer
* Tricia Joe as Claudia Kishi
* Stacy Linn Ramsower as Mallory Pike
* Zelda Harris as Jessica Ramsey
* Vanessa Zima as Rosie Wilder
* Christian Oliver as Luca Brooke Adams as Elizabeth Thomas Brewer
* Bruce Davison as Watson Brewer
* Jessica Needham as Karen Brewer
* Ellen Burstyn as Mrs. Haberman
* Asher Metchik as Jackie Rodowsky
* Austin OBrien as Logan Bruno
* Marla Sokoloff as Cokie Mason
* Aaron Michael Metchik as Alan Gray
* Kyla Pratt as Becca Ramsey
* Scarlett Pomers as Suzi Barrett

==Reception==
The movie had mostly positive reviews.     It holds a 64% rating on Rotten Tomatoes based on 14 reviews.

===Box Office=== Mortal Kombat. The total domestic gross was $9.6 million.   

==Soundtrack==
{{Infobox album  
| Name       = The Baby-Sitters Club: Music from the Motion Picture
| Type       = Soundtrack
| Artist     = Various Artists
| Cover      = 
| Alt        = 
| Released   =  
| Recorded   = 
| Genre      = Pop rock http://www.allmusic.com/album/baby-sitters-club-mw0000174966 
| Length     =   
| Label      = Sony Wonder 
| Producer   = Caulfields, Caulfields, Mike Denneen, Jermaine Dupri, David Michael Frank, Richard Gottehrer, Jeffrey Lesser, Paul McKercher, James McVay, Kevin Moloney, Brendan OBrien, G. Marq Roswell, David Russo, Manuel Seal, Jr., Todd Smallwood, Brad Wood 
}}
{{Album ratings
| rev1 = AllMusic
| rev1Score =   
}}

The Baby-Sitters Club: Music from the Motion Picture is a soundtrack that was released on August 8, 1995. 

===Track listing===
:Track listing adapted from AllMusic. 
{{track listing
| writing_credits = no
| extra_column    = Performer(s)
| total_length    =  
| title1          = Summertime
| extra1          = Moonpools & Caterpillars
| length1         = 2:50
| title2          = Say It
| extra2          = Clouds
| length2         = 2:11
| title3          = Hannah, I Locked You Out
| extra3          = The Caulfields
| length3         = 3:13
| title4          = Let Me Know
| extra4          = Xscape
| length4         = 3:42
| title5          = Hold On
| extra5          = Sun 60
| length5         = 4:40
| title6          = Everything Changes
| extra6          = Matthew Sweet
| length6         = 3:50
| title7          = Dont Leave
| extra7          = Ben Lee
| length7         = 1:59
| title8          = Step Back
| extra8          = Letters to Cleo
| length8         = 2:34
| title9          = Daddys Girl
| extra9          = Lisa Harlow Stark
| length9         = 3:54
| title10         = Girl-Girlfriend
| extra10         = BSC
| length10        = 3:56
}}

==References==
 

==External links==
*  
*  
*  , from Merrills Companion to The Baby-Sitters Club

 

 
 
 
 
 
 
 
 
 
 
 
 
 