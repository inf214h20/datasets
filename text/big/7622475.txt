Springtime (2004 film)
{{Infobox film 
| name         = Springtime
| image        = Springtime poster.jpg
| caption      =  
| writer       = Ryoo Jang-ha
| starring     = Choi Min-sik Jang Shin-young Kim Ho-jeong Youn Yuh-jung Kim Kang-woo
| director     = Ryoo Jang-ha 
| producer     = Choi Eun-hwa 
| distributor  = Big Blue Film
| released     =  
| runtime      = 128 minutes
| country      = South Korea
| language     = Korean
| music        = Jo Seong-woo 
| budget       = 
| film name = {{Film name
 | hangul       =      
 | hanja        = 
 | rr           = Ggotpineun bomi omyeon 
 | mr           = Kkot p‘inŭn pomi omyŏn}}
}}
Springtime ( ; lit. When Spring Comes) is a 2004 South Korean film starring Choi Min-sik as a struggling musician who takes a job as a music teacher in a rural mining town outside of Seoul.

==Plot==
To trumpeter Hyun-woo, life seems to remain forever locked in winter. In desperation, Hyunwoo signs up for a position teaching a children’s wind ensemble at a small junior high school in a distant Dogye village. Worn-out instruments, tarnished trophies and frayed certificates testify to the poor conditions of this ragtag group. This leads Hyun-woo, together with his students, to take on a seemingly impossible challenge.

==Awards and nominations==
;2004 Blue Dragon Film Awards 
* Best Music - Jo Seong-woo
* Nomination – Best Actor - Choi Min-sik
* Nomination – Best New Actress - Jang Shin-young

;2004 Korean Film Awards
* Nomination – Best Actor - Choi Min-sik
* Nomination – Best Supporting Actress – Youn Yuh-jung
* Nomination – Best Music - Jo Seong-woo
* Nomination – Best New Director – Ryoo Jang-ha
* Nomination – Best New Actor - Kim Kang-woo

== References ==
 

== External links ==
*  
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 