Moon in Scorpio
This article is about a 1987 film. For the 1996 music album see Moon in the Scorpio
{{Infobox film
| name           = Moon in Scorpio
| image          = Moon in Scorpio.jpg
| caption        =
| director       = Gary Graver
| executive producer = Moshe Diamant
| producer       = Alan Amiel
| writer         = Robert S. Aiken William Smith April Wayne Lewis van Bergen
| music          = Robert O. Ragland
| editing        = Omer Tal
| distributor    = Trans World Entertainment
| released       =  
| runtime        = 90 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
 William Smith.  Law plays the role of Ekland’s husband.  Cast as a Vietnam War veteran, Law takes Ekland on a yachting trip where they are joined by two of his friends, also Vietnam veterans, and their wives.  During their time at sea, each member of the group is killed, one by one, leaving only Ekland and the apparent killer on board.

The film was directed by Gary Graver who specialized in directing low budget movies and was distributed by Trans World Entertainment. 

The term “Moon in Scorpio” is an astrological reference that has many different meanings, but generally refers to a person who seeks out emotional intensity and whose life may be characterized by transformation and rebirth. 

==Plot==
Britt Ekland plays the role of Linda, who is married to Allen, played by John Phillip Law. Allen is a Vietnam War veteran. Together, they plan to take a trip to Acapulco for their honeymoon on a yacht owned by Burt (Smith) and his wife, Claire (Kesner). Joining them are a third couple, Mark (Van Bergen) and his wife, Isabel (April Wayne). Burt and Mark are also Vietnam War veterans and friends of Allen. 
Prior to the yacht leaving the harbor, one of the marina’s employees is stabbed to death by a mysterious figure in black pajamas.

The couples meet on the boat and set sail. During the voyage, war time flashbacks show the three men committing atrocities during their tour of duty in Vietnam to include the murdering of innocent villagers. The men are clearly troubled by this and tensions on the ship, some of which are sexual, begin to rise. Soon, the boat is vandalized, rendering some of the navigation and communications equipment inoperable. Isabel begins acting erratically and mentions several times that when the “moon is in scorpio”, bad things happen. 
In a short period of time, both Mark, Burt, as well as Claire are gruesomely killed, leaving only Linda, Allen, and Isabel as the survivors.  Finally, Allen is killed and then, as Linda looks for him on the yacht, she surprises Isabel who is in possession of the murder weapon, an odd-shaped grappling hook.  Isabel tries to kill Linda and the two women fight each other.  The fight ends when Linda overpowers Isabel and stabs her to death with the grappling hook. 
The entire story is told via flashbacks, from Linda’s point of view, as she is the only survivor.  The movie opens with an unseen person escaping from a mental institution and Linda in a hospital being comforted by a doctor.  The movie closes with Linda being released from the hospital, after she has narrated her story the police.      

==Director’s Perspective==
 Halloween but set on a yacht. The result, according to Graver, was some of both, but mostly confusion.  Worse, Graver explained, after he went on vacation following the conclusion of the editing, Diamant hired another editor to re-arrange the scenes, creating yet more confusion. Graver claimed that Moon in Scorpio was the only project he ever worked on that made him want to “get into a fistfight.” Although the movie was never released theatrically, Graver claimed it made the producers more than a $1,500,000 profit from video sales and TV and cable showings. 

==Reviews==
Critics have ridiculed the movie.

 ”From the jumbled structure, its apparent that the movie was heavily re-edited to the point of incomprehensibility. I got the impression that Graver and screenwriter Robert Aiken originally intended the movie to be a supernatural thriller, with the veterans past literally coming back to haunt them (either through the ghosts of the villagers they massacred, or through possession of one of the boaters). However, whatever ghostly   … zombie elements there may have been, they have been completely removed. The prologue and the hospital sequences were obviously tacked on afterwards, both to provide a "rational" explanation for the murders and to pad out the running time. Im not sure whether Graver himself helmed any of the framing-sequence footage, or whether it was handled by co-producer Fred Olen Ray...in any case, the result is a mess.”   

 “ ... a borderline hokey story of a sailing vacation gone awry, one populated by terrible acting, bad sets, lazy cinematography, ridiculous dialogue, and an unimaginative story.”  

 "Found in the horror section of video stores, this film could just as easily be classified simply as horrible."  

Other comments offered by critics include:

*  Failure to adequately explain any motive for the killings 
* Inexplicable response by the couples who appear to take the murders of their ship mates in stride as opposed to either suspecting one of the survivors as the perpetrator or by seeking nearby help:  the California coastline remains in the background during much of the murder and mayhem. "Moon in Scorpio" Not Coming to a Theater Near You. Retrieved April 14, 2013   

Ratings: Moon in Scorpio has usually garnered one or two stars on a scale of one to five.   

==References==
 

 
 
 