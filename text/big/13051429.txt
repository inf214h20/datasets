One Take Only
{{Infobox film
| name           = Som and Bank: Bangkok for Sale
| image          = Som_and_Bank_poster.jpg
| image size     = 190px
| caption        = Thai theatrical poster.
| director       = Oxide Pang
| producer       = Kittikorn Liasirikun The Pang Brothers Udom Piboonlapudom
| writer         = Oxide Pang
| narrator       = 
| starring       = Pawarith Monkolpisit Wanatchada Siwapornchai
| music          = Orange Music
| cinematography = Krisorn Buramasing Decha Srimantra	 	
| editing        = Oxide Pang Film Bangkok Tartan Films
| released       =  
| runtime        = 88 minutes
| country        = Thailand Thai
| budget         = 
}}
One Take Only ( , also Som and Bank: Bangkok for Sale) is a 2001 crime film|crime-drama film written and directed by Oxide Pang.

==Plot==
Bank (Pawarith Monkolpisit) is a small-time hoodlum in Bangkok. He uses drugs, and sometimes works for some local gangsters, smuggling guns and drugs. One day he meets Som, a teenage girl who works as a Prostitution in Thailand|prostitute. The pair fall in love, and in a bid to better their lives, they get into a drug deal that is too big for either of them.

==Production and release== Bangkok Dangerous Danny Pangs Nothing to Lose).
 The Eye. Film Bangkok, Board of Censors objected to the title of the film, so it was renamed One Take Only. 

The film was also screened at the International Film Festival Rotterdam, the Hong Kong International Film Festival, the San Francisco International Film Festival, the Asiatica Film Mediale and the NatFilm Festival in 2002.

The film was released on DVD by Tartan Films on August 22, 2006. Confusingly, it  has also been released on DVD in Thailand as Som and Bank: Bangkok for Sale.

==References==
 

==External links==
* 
* 
* 

 

 
 
 
 
 


 
 