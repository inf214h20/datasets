The Actress (1928 film)
 
{{infobox film
| name           = The Actress
| image          = The Actress 1928 lobby poster.jpg
| imagesize      =
| caption        = 1928 lobby poster Sidney Franklin
| producer       = Louis B. Mayer Irving Thalberg
| writer         = Albert Lewin Richard Schayer Joseph Farnham (intertitles)
| based on       =  
| starring       = Norma Shearer
| music          =
| cinematography = William H. Daniels
| editing        = Conrad A. Nervig
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 70 minutes
| country        = United States
| language       = Silent English intertitles
}} silent drama Sidney Franklin and starred Norma Shearer.
 Trelawny of the Wells in 1916.
 lost MGM films from the 1920s. 

==Cast==
* Norma Shearer as Rose Trelawny
* Owen Moore as Tom Wrench
* Gwen Lee as Avonia
* Lee Moran as Colpoys
* Roy DArcy as Gadd
* Virginia Pearson as Mrs. Telfer
* William J. Humphrey as Mr. Telfer
* Effie Ellsler as Mrs. Mossop
* Ralph Forbes as Arthur Gower
* O.P. Heggie as Vice-Chancellor Sir William Gower
* Andree Tourneur as Clara de Foenix
* Cyril Chadwick as Captain de Foenix
* Margaret Seddon as Miss Trafalgar Gower

==References==
 

==External links==
*  
*  
*  at the Cleveland Public Library

 

 
 
 
 
 
 
 
 
 
 
 


 