Reversion (film)
{{Infobox film
| name           = Reversion
| image          = REVERSIONVerticalPoster.jpg
| alt            =
| caption        = 
| director       = Giancarlo Ng
| writer         = Giancarlo Ng
| starring       = Daniela Hummel
| music          = Dan Eckert
| studio         = The Magic Movie Machine
| released       =  
| runtime        = 11 minutes 
| country        = Philippines and International
| language       = English
}}
 computer animated short film directed by Giancarlo Ng and produced by The Magic Movie Machine, a self-described collaborative animation team.

The 11-minute short film was created using Blender (software)|Blender. 2012 Suzanne Animahenasyon 2012.  The short film was also named as a Division II winner at Asiagraph 2013 held in Tokyo, Japan.

== Plot ==
The short film opens with an unidentified soldier infiltrating the facilities of a company known as Ethercorp. Though the lobby is badly damaged, a still-functional computer display reveals the facility to be a medical research centre, located in the Congo (area)|Congo. The soldiers helmet also bears Ethercorps logo. As the soldier downloads tracking software from the computer, a reptilian creature lurks in the shadows above. Just before the creature strikes, the tracking software warns the soldier of its presence, and the creature flees under gunfire. The soldier chases the creature though several offices and medical labs, before losing it in a dark corridor.

The soldier emerges from a duct in the floor of another corridor, and enters a security room containing a holographic projector. The soldier removes her helmet, revealing herself to be a young woman. By viewing the holographic records, she discovers footage of a human test subject being administered an injection, which triggers a metamorphosis into the creature she had been pursuing. In shock, the soldier leaves her helmet in the security room to record the footage, and enters an elevator.

The female soldier emerges from the elevator into an indoor garden, and encounters the reptilian creature. She hesitates to shoot it, but is forced to fire when it charges. The creature retreats, then leaps at the soldier from a high ledge. The soldiers pistol runs out of ammunition, and she pulls out a knife as the creature falls down on her.

A series of clips during the credits reveal that the creature has been killed by a knife wound to the neck. A holographic message on her wrist prompts the soldier to confirm extraction, but she instead switches the hologram off.

== Awards and nominations ==
{| class="wikitable"
|-
! Award !! Year !! Category !! Nominee(s) !! Result
|- Suzanne Awards    2012
|Best Designed Short Film Giancarlo Ng and The Magic Movie Machine
| 
|- Animahenasyon    2012
|Best in Technical Quality Giancarlo Ng and The Magic Movie Machine
| 
|- Asiagraph 2013 (Tokyo, Japan)    2013
|Division II Giancarlo Ng and The Magic Movie Machine
| 
|}

== See also ==
* Blender (software)|Blender

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 