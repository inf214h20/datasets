Beauty of the World
 
{{Infobox film
| name           = Beauty of the World
| image          =
| caption        =
| director       = Mario Almirante
| producer       =
| writer         = Pier Angelo Mazzolotti
| starring       = Italia Almirante-Manzini
| cinematography = Antonio Cufaro
| editing        =
| distributor    =
| released       =  
| runtime        =
| country        = Italy
| language       = Silent
| budget         =
}}

Beauty of the World ( ) is a 1927 silent Italian film directed by Mario Almirante. The film features an early onscreen performance from Vittorio De Sica.   

==Cast==
* Italia Almirante-Manzini
* Renato Cialente
* Luigi Almirante
* Nini Dinelli
* Vittorina Benvenuti
* Guglielmo Barnabò
* Vittorio De Sica

==References==
 

==External links==
* 

 
 
 
 
 
 