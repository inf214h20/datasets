Aakhri Ghulam
{{Infobox film
 | name = AakhriGhulam
 | image = AakhriGhulam89.jpg
 | caption = VCD Cover
 | director = Shibbu Mitra
 | producer = Salim Khan  
 | writer = 
 | dialogue = 
 | starring = Mithun Chakraborty Raj Babbar Sonam Shreedhara Shakti Kapoor Moushmi Chatterjee Anupam Kher
 | music = Bappi Lahiri
 | lyrics = 
 | cinematographer = 
 | associate director = 
 | art director = 
 | choreographer = 
 | released =  July 7, 1989 (India)
 | runtime = 135 min.
 | language = Hindi Rs 5 Crores
 | preceded_by = 
 | followed_by = 
 }}
 directed by Shibbu Mitra, starring Mithun Chakraborty, Raj Babbar, Sonam, Shreedhara,  Shakti Kapoor, Moushmi Chatterjee, Anupam Kher and Anu Kapoor.

==Plot==

Aakhri Ghulam is an action thriller, featuring Mithun Chakraborty in lead role, well supported by Raj Babbar, Sonam, Shreedhara,  Shakti Kapoor, Moushmi Chatterjee and Anupam Kher.

==Cast==

*Mithun Chakraborty
*Raj Babbar
*Sonam
*Shreedhara
*Ranjeet
*Shakti Kapoor
*Moushmi Chatterjee
*Anu Kapoor
*Anupam Kher

==References==
 
* http://www.bollywoodhungama.com/movies/cast/5236/index.html

==External links==
*  

 
 
 
 


 