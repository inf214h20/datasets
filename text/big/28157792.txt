They Had to See Paris
{{Infobox film
| name           = They Had to See Paris
| image          = They-had-to-see-paris-1929.jpg
| image_size     =
| caption        = Film poster
| director       = Frank Borzage William Fox
| writer         = Homer Croy (novel) Sonya Levien (story) Owen Davis (writer)
| narrator       =
| starring       = Will Rogers Irene Rich Marguerite Churchill
| music          = George Lipschultz (uncredited)
| cinematography = Chester Lyons
| editing        = Margaret Clancey (uncredited)
| distributor    = Fox Film Corporation
| released       =  
| runtime        = 95 minutes
| country        = United States English
| budget         =
}} American comedy film directed by Frank Borzage and starring Will Rogers, Irene Rich and Marguerite Churchill. A wealthy American oil tycoon travels to Paris with his family at his wifes request, despite the fact he hates the French.   
 So This Down to Earth (1932) which depicts the return of the Peters family to Great Depression|Depression-hit America. 

==Cast==
* Will Rogers - Pike Peters 
* Irene Rich - Mrs. Idy Peters 
* Owen Davis Jr. - Ross Peters 
* Marguerite Churchill - Opal Peters 
* Fifi DOrsay - Fifi 
* Rex Bell - Clark McCurdy 
* Robert P. Kerr - Tupper 
* Ivan Lebedeff - Marquis de Brissac 
* Edgar Kennedy - Ed Eggers 
* Christiane Yves - Fleurie 
* Marcelle Corday - Marquise De Brissac 
* Theodore Lodi - Grand Duke Mikhail 
* Marcia Manon - Miss Mason 
* André Cheron (actor)|André Cheron - Valet 
* Gregory Gaye - Prince Ordinsky

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 

 