Escape to Danger
 The Web Planet}}
{{Infobox film
| name           = Escape to Danger
| image          = "Escape_to_Danger"_(1943).jpg
| caption        = 
| director       = Lance Comfort Victor Hanbury
| producer       = William Sistrom Wolfgang Wilhelm Patrick Kirwan (story)
| starring       = Eric Portman  Ann Dvorak Guy Green
| music          = William Alwyn
| editing        = Edward B. Jarvis	(as E. Jarvis)
| studio         = RKO Radio British Productions
| released by    = RKO
| released       =  
| runtime        = 84 minutes
| country        = United Kingdom
| language       = English
}}
Escape to Danger is a 1943 British thriller film directed by Lance Comfort and Victor Hanbury and starring Eric Portman, Ann Dvorak and Karel Stepanek. 

==Plot==
During the Second World War a British schoolteacher working in Denmark is caught up when the Germans invade.

==Cast==
* Eric Portman ...  Arthur Lawrence 
* Ann Dvorak ...  Joan Grahame 
* Karel Stepanek ...  Franz von Brinkman 
* Ronald Ward ...  Rupert Chessman  Ronald Adam ...  George Merrick 
* Felix Aylmer ...  Sir Alfred Horton 
* Brefni ORorke ...  Security Officer 
* A.E. Matthews ...  Sir Thomas Leighton 
* Ivor Barnard ...  Henry Waud  David Peel ...  Lt. Peter Leighton 
* Charles Victor ...  Petty Officer Flanagan  George Merritt ...  Works Manager 
* Marjorie Rhodes ...  Mrs. Pickles 
* John Ruddock ...  Jim 
* Frederick Cooper ...  Gösta

==Critical reception==
TV Guide wrote, "WW II espionage tale was timely for its day but has dated."  

==References==
 

==External links==
* 

 
 

 
 
 
 
 
 
 
 


 
 