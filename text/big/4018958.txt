Copying Beethoven
 
 
{{Infobox film
| name           = Copying Beethoven
| image          = CopyingbeethovenNEW.jpg
| caption        = Theatrical release poster
| director       = Agnieszka Holland Michael Taylor Christopher Wilkinson
| writer         = Stephen J. Rivele Christopher Wilkinson
| starring       = Ed Harris Diane Kruger
| music          = 
| cinematography = Ashley Rowe
| editing        = Alex Mackie
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 104 minutes
| country        = United States
| language       = English 
| budget         =
| gross          = $6,191,746 
}}
Copying Beethoven is a 2006 dramatic film released by Metro-Goldwyn-Mayer and directed by Agnieszka Holland which gives a fictional take on the triumphs and heartaches of Ludwig van Beethovens last years.

==Plot== Ninth Symphony.  He is plagued by deafness, loneliness and personal trauma.  A fictional character, a new copyist, Anna Holtz (Diane Kruger) is engaged to help the composer finish preparing the score for the first performance.  Anna is a young conservatory student and aspiring composer.  Her understanding of his work is such that she corrects mistakes he has made, while her personality opens a door into his private world.  Beethoven is initially skeptical, but slowly comes to trust Annas assistance and eventually grows fond of her.  By the time the piece is performed, her presence is a necessity and she helps him conduct the premiere from a spot hidden amongst the orchestra.

After the premiere, they collaborate and become closer. His eccentricities become more and more troublesome, but Anna continues to provide companionship.  She eventually transcribes his compositions as he simply talks her through them.

==Cast==
*Ed Harris as Ludwig van Beethoven
*Diane Kruger as Anna Holtz
*Matthew Goode as Martin Bauer
*Phyllida Law as Mother Canisius Joe Anderson Karl van Beethoven
*Ralph Riach as Wenzel Schlemmer

==Artistic license==
The working manuscript of the score is attributed to two copyists,  both of whom were male, not a single female as depicted in the film.

==Critical reception==
The film received a score of 59 and negative reviews of 28% rating on Rotten Tomatoes based on 80 reviews.

==References==
 

==External links==
* 
*  at Metacritic.com

 

 
 
 
 
 
 
 
 
 