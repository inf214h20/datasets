Mundhinam Paartheney
{{Infobox film
| name           = Mundhinam Paartheney
| image          =
| image_size     =
| caption        =
| director       = Magizh Thirumeni
| producer       = Manickam Narayanan
| writer         = Magizh Thirumeni
| starring       =  
| music          = Thaman
| cinematography = Arul Vincent Anthony
| studio         = Seventh Channel Communications
| distributor    =
| released       =  
| runtime        =
| country        = India
| language       = Tamil
| budget         =
| gross          =
}}
Mundhinam Paartheney is a 2010 Indian Tamil-language romantic comedy film written and directed by debutant director Magizh Thirumeni, starring newcomers Sanjay, Ekta Ghosla, Lizna, Pooja and Sai Prashanth in lead roles. The film, produced by Manickam Narayanans Seventh Channel Communications, released on 19 March 2010. Movie is praised for the screenplay, flow of events and comedy.  

==Cast==
* Sanjay as Sanjay
* Ekta Khosla as Aarthi
* Lizna as Anu
* Pooja as Pooja
* Sai Prashanth as Dinesh

==Plot==
The story unfolds in London, where Sanjay (Sanjay), narrates about his kind of girl, and talks about love. Cut to flashback, the hero meets Pooja.

He tries to impress her in all possible ways. But his hopes are shattered when he comes to know that she is getting ready for a marriage with a NRI youth. Then comes Aarthi (Ekta Khosla), a dance tutor. She is a girl with traditional ideas but modern outlook. Unfortunately, the heros attempts to settle down with her gets jinxed. Then enters Anu, Sanjays colleague. Does he finds her apt for him forms the rest.

==Box Office==
The film did not do well in box office. This was a bold attempt to represent the IT generation and the current issues with finding a right partner. 

== References ==
 

==External links==
*  

 
 
 
 
 


 