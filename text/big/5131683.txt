A Primitive Man's Career to Civilization
{{Infobox film
| name           = A Primitive Mans Career to Civilization
| image          =
| image size     =
| caption        =
| director       = Cherry Kearton
| producer       =
| writer         = Cherry Kearton
| narrator       =
| starring       =
| music          =
| cinematography =
| editing        =
| distributor    = WTC
| released       = 1912
| runtime        =
| country        = United Kingdom English intertitles
| budget         =
| preceded by    =
| followed by    =
}}

A Primitive Mans Career to Civilization was a UK film released in 1912 in film|1912, directed and written by Cherry Kearton. Shot on 35mm film in silent black and white, it was distributed by WTC. 

The film was produced in 1911 by the Ethnographic Society of London, who had previously commissioned Kearton to film a traditional Kenyan dance performed to honour Theodore Roosevelt when he visited the country in 1909. 

==References==
{{reflist|refs=
   
}}

==External links==
* 

 
 
 
 
 

 