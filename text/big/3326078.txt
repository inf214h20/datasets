Super 8 (film)
 
 
{{Infobox film
| name           = Super 8
| image          = Super 8 Poster.jpg
| alt            =  
| caption        = Theatrical release poster
| director       = J. J. Abrams
| producer       = {{Plainlist|
* J. J. Abrams
* Bryan Burk
* Steven Spielberg }}
| writer         = J. J. Abrams
| starring       = {{Plainlist|
* Joel Courtney
* Elle Fanning
* Kyle Chandler
* Riley Griffiths Ryan Lee
* Ron Eldard }}
| music          = Michael Giacchino
| cinematography = Larry Fong
| editing        = {{Plainlist|
* Maryann Brandon
* Mary Jo Markey }}
| studio         = {{Plainlist|
* Bad Robot Productions
* Amblin Entertainment }}
| distributor    = Paramount Pictures
| released       =  
| runtime        = 112 minutes
| country        = United States
| language       = English
| budget         = $50 million 
| gross          = $260.1 million   
}} Super 8 movie when a train derails, releasing a dangerous presence into their town. The movie was filmed in Weirton, West Virginia and surrounding areas.

Super 8 was released on June 10, 2011,  in conventional and IMAX theaters in the US. The film was well received, with critics praising the film for its nostalgia, visual effects, musical score, and for the performances of its young actors, particularly those of Fanning and newcomer Courtney. It was also a commercial success, grossing over $260 million against a $50 million budget. The film received several awards and nominations; primarily in technical and special effects categories, as well as for Courtney and Fannings performances as the films two young leads.

==Plot==
In 1979, Deputy Sheriff Jack Lamb (Kyle Chandler) of Lillian, Ohio, and his 14-year-old  son Joe (Joel Courtney), mourn the death of his mother Elizabeth (Caitriona Balfe) in a steel mill accident.  Jack blames her co-worker, Louis Dainard (Ron Eldard), as she was covering his shift while he recovered from a hangover.
 Ryan Lee), as well as Dainards daughter, Alice (Elle Fanning). Though their fathers would be furious, Joe and Alice become smitten with each other.
 Super 8mm film, and assumes the event was captured on camera.

While Joe and Charles wait for their film to be developed, the town experiences strange events: All the dogs run away, several townspeople go missing, and electronics from all over are stolen. Overhearing military communications, Jack approaches Nelec to address the rising panic in town, but Nelec instead orders him arrested. Nelec orders the use of flamethrowers to start wildfires outside of town, as an excuse to evacuate people to the base. Suddenly, soldiers sweep into town to begin the evacuation. Meanwhile, Joe and Charles watch the derailment footage and discover that a large creature had escaped from the train.

At the base, Joe learns from Alices father that she is missing, abducted by the creature. Joe, Charles, Martin, and Cary find a hole in the bases fence and slip back into town to rescue Alice. They break into Dr. Woodwards storage trailer and discover films and documents from his time as a government researcher.
 alien crash-landed in 1958. The Air Force captured the alien and was running experiments on it while keeping it from its ship. Woodward was one of the scientists experimenting on the ship, composed of the white cubes. At one point, the alien grabs Woodward, apparently establishing a psychic connection with him. Now understanding the alien, he was compelled to rescue it and help it escape from Earth. Finding out about the train years later presented him with the opportunity to help the creature. The boys are caught by Nelec, but as they are taken back to base, the alien attacks their bus. The airmen are killed and the boys escape. Meanwhile, Jack escapes from the bases brig and makes his way to the shelter housing the townsfolk. He learns from Preston about Joes plan to rescue Alice. Jack and Dainard then agree to put aside their differences to save their kids.

In town, their hardware malfunctions as the military attempts to kill the alien. Martin is injured in an explosion, so Charles stays behind with him while Joe and Cary head to the cemetery, where Joe had earlier seen something there that made him suspicious. Inside the cemeterys garage, they find a massive tunnel leading to a warren of underground caverns. In a chamber beneath the towns water tower, they find the alien has created a device from the towns stolen electronics, attached to the base of the tower. The alien also has several people, including Alice, hanging from the ceiling and unconscious. Using Carys firecrackers as a distraction, Joe frees Alice and the others, but they end up trapped in a dead end cavern after the alien chases them down. Alice and Cary scream and run, but Joe steps forward and tries to talk to the alien. The alien grabs Joe, who quietly speaks to the alien, telling him over and over that "bad things happen" but that the alien "can still live". After studying Joe for a moment, the alien releases him and departs, allowing the three to return to the surface.

As Joe and Alice reunite with their fathers, everyone watches as metal objects from all over town are magnetically pulled to the top of the water tower. The white cubes are also pulled in to assemble into a spaceship around the water tank. The locket that used to belong to Joes mom is also drawn towards the tower and Joe, after a brief moment, decides to let it go. The alien enters the completed spaceship; the water tower implodes and the ship rockets into space. Joe takes Alices hand as they watch the ship depart into the night sky.

During the credits, the kids completed film, entitled The Case, is shown.

===The Case===
====Synopsis==== Romero Chemical Plant. Hathaway then goes to the President of Romero Chemicals. He confronts him about an incident that occurred in the plant. The President disregards it as an accident, so Hathaway states that he is going to look around the building. After he leaves the President makes an urgent phone call saying, "He knows." Hathaway is attacked by a zombie in the building. He manages to kill it by knocking its head into some exposed nails on the wall. He then calls someone to buy his wife a ticket to Michigan because it is not safe for her. That night at the train depot, she tells him that she loves him and doesnt want him getting into danger. Seconds later, a massive train wreck occurs, keeping his wife there. The next morning as they look at the wreck, they are attacked by a zombie, whom Hathaway shoots.

That night, Hathaway records in his audio journal that the attacks are putting stress on his town and his marriage. He then gets a call from an Air Force officer that served with Hathaway in Vietnam. He tells him that an airman committed suicide because there was a secret he could no longer keep. He then gives him top-secret information about a doctor that the late airman worked for. He goes to meet this doctor who says that he has found a cure for the zombie epidemic and begins to test it on a victim of the virus. However, the victim wakes up and bites the doctor turning him into a zombie. Hathaway shoots them both. He goes home to see that his wife has been turned into a zombie. She suddenly attacks him, but he injects the antidote into her neck. He gets ready to shoot her but she is cured of the virus and they embrace. The film ends with a small announcement from the director, Charles Kaznyk, who tells the audience how much fun they had making it and that he hopes they choose his film for the festival. Alice, in zombie makeup, then interrupts and pranks the viewer into thinking she is a real zombie when she attacks and bites Charles, before she lunges at the camera.

==Cast==
 
*Joel Courtney as Joseph "Joe" Lamb
*Elle Fanning as Alice "Allie" Dainard, Joes love interest
*Kyle Chandler as Jackson "Jack" Lamb, Joes father
*Riley Griffiths as Charles Kaznyk, Joes best friend Ryan Lee as Cary, a hyperactive, firework-obsessed friend of Joe and Charles.
*Ron Eldard as Louis Dainard, Alices alcoholic father
*Gabriel Basso as Martin, a shy, paranoid friend of Joe and Charles.
*Noah Emmerich as Colonel Nelec
*David Gallagher as Donny
*Bruce Greenwood as Cooper, the name used by the cast and crew for the alien
*Zach Mills as Preston Amanda Michalka as Jen Kaznyk, Charless older sister
*Glynn Turman as Dr. Thomas Woodward, a biology teacher
*Michael Hitchcock as Deputy Rosko
*Caitriona Balfe as Elizabeth Lamb
*Joel McKinnon Miller as Sal Kaznyk
*Jessica Tuck as Mrs. Kaznyk
*Dan Castellaneta as Izzy
*Richard T. Jones as Overmyer
*Dale Dickey as Edie
*Colin Mathews as Crazy Betos Customer
*Brett Rice as Sheriff Pruitt
*Katie Lowes as Tina
 

==Production==
J.J. Abrams had the idea to start a film by showing a factorys "Accident-Free" sign long before he came up with the rest of the idea of the film. Super 8 was actually the combination of two ideas; one for a film about kids making their own movie during the 1970s, and another for a blockbuster alien invasion film. Worried that the former idea would not attract enough attendance, Abrams combined the ideas.

Abrams and Spielberg collaborated in a storytelling committee to come up with the story for the film.  The film was initially reported to be either a sequel or prequel to the 2008 film Cloverfield,  but this was quickly denied by Abrams.  Primary photography began in fall (September/October) 2010. The teaser itself was filmed separately in April.  Super 8 is the first original J. J. Abrams film project produced by Amblin Entertainment, Bad Robot Productions, and Paramount Pictures. 

Abrams wanted to find new faces to play the parts in his movie. He conducted a national talent search in order to find the child actors to play each of the leading roles. Courtney (who was hoping to land a part in a commercial) was picked out of many boys because Abrams found something "different" in him. Riley Griffiths sent Abrams a tape of himself in order to land the part of Charles.
 Windows and Mac versions of Portal 2.
 Industrial Light and Magic found it impossible to integrate CGI into the footage due to the formats graininess. For sequences involving CGI, cinematographer Larry Fong used 16 mm film#Super 16 mm|Super-16 instead. 

===Soundtracks===
{{Infobox album
|Name=Super 8
|Type=Film score
|Artist=Michael Giacchino
|Cover=
|Recorded=2011
|Released=August 2, 2011
|Genre=Orchestral
|Length=69:84
|Label=Varèse Sarabande
|Chronology=Michael Giacchino Last album=Cars 2 (2011) This album=Super 8 (2011) Next album=Monte Monte Carlo (2011)}}
{{Album ratings
|rev1=AllMusic
|rev1Score=  
|noprose=yes}}
The score for the film was composed by Michael Giacchino, Abrams long-time collaborator. The soundtrack was released on August 2, 2011, by Varèse Sarabande. It won the 2012 Saturn Award for Best Music.
 Blondie song Heart of Bye Bye Love" are also featured in the film.
{{Track listing
|headline=Track listing
|all_music=Michael Giacchino (although track 33, "The Case", is credited on the liner notes to the film character Charles Kaznyk)
|total_length=77:43
|title1=Super 8
|length1=1:44
|title2=Family Matters
|length2=0:29
|title3=Model Painting
|length3=0:41
|title4=Acting Chops
|length4=0:40
|title5=Aftermath Class
|length5=5:54
|title6=Thoughts of Cubism
|length6=0:48
|title7=Well Fix It in Post-Haste
|length7=0:44
|title8=Productions Woes
|length8=0:34
|title9=Train of Thought
|length9=0:35
|title10=Circle Gets the Cube
|length10=1:06
|title11=Breen There, Ate That
|length11=1:12
|title12=Dead Over Heels
|length12=0:48
|title13=Gas and Go
|length13=1:34
|title14=Looking for Lucy
|length14=0:49
|title15=Radio Haze
|length15=1:08
|title16=Moms Necklace
|length16=1:33
|title17=Shootus Interuptus
|length17=2:35
|title18=Thoughts of Mom
|length18=1:41
|title19=Woodward Bites It
|length19=1:54
|title20=Alice Projects on Joe
|length20=2:29
|title21=Neighborhood Watch&nbsp;— Fail
|length21=4:45
|title22=The Evacuation of Lillian
|length22=3:40
|title23=A Truckload of Trouble
|length23=0:57
|title24=Lambs on the Lam
|length24=2:40
|title25=Woodwards Home Movies
|length25=2:40
|title26=Spotted Lambs
|length26=1:37
|title27=Air Force HQ or Bust
|length27=1:04
|title28=Worlds Worst Field Trip
|length28=3:36
|title29=The Siege of Lillian
|length29=2:57
|title30=Creature Comforts
|length30=10:10
|title31=Letting Go
|length31=5:18
|title32=Super 8 Suite
|length32=5:54
|title33=The Case
|length33=3:28}}

===Viral marketing campaign===
Like   and contained various clues to the films story-line; the computer was eventually revealed to belong to Josh Woodward, the son of Dr. Woodward, who is trying to find out what happened to his father. Another viral website,   was also found, which like Slusho from Cloverfield plays no direct part in the film but is indirectly related. The official Super 8 website also contained an "editing room" section, which asked users to find various clips from around the web and piece them together. When completed, the reel makes up the film found by the kids in Dr. Woodwards trailer, showing the ship disintegrating into individual white cubes, and the alien reaching through the window of its cage and snatching Dr. Woodward. The video game Portal 2 contains an interactive trailer placing the player on board the train before it derails, and showing the carriage being smashed open and the roar of the alien within.

==Release==
The film was released on June 9, 2011, in Australia; June 10, 2011, in the United States; and August 5, 2011, in the United Kingdom.  On June 8, Paramount also launched a “Super 8 Sneak Peek” Twitter promotion, offering fans a chance to purchase tickets for an advance screening, taking place on June 9, 2011, in the United States.  The film opened at #1 in the U.S. Box Office for that weekend, grossing about $35 million.

===Home media===
The film was released on Blu-ray Disc|Blu-ray and DVD on November 22, 2011.  The release was produced as a combo pack with a Digital Copy, including nine bonus features and fourteen deleted scenes. 

==Reception==
===Box office===
Super 8 had a production budget of United States dollar|$50 million. It was commercially released on  , 2011. In the United States and Canada, it opened in   and grossed over $35.4 million on its opening weekend, ranking first at the box office.  The film grossed $127 million in North America with a worldwide total of some $260 million. 

===Critical response=== weighted average score from 1–100 to reviews from critics, assigned the film a Metascore of 72 based on 41 critics, signifying generally favorable reviews. 

Chris Sosa of Gather.com|Gather gave the film an A rating, calling it, "a gripping and exciting tale of finding ones place in the world amidst tragedy". His review concluded, "While the genre-bending occasionally unsettles, the films genuine and emotionally gripping nature make its journey believable." 
 Christopher Orr of the The Atlantic called it a "love letter to a cinematic era", while Claudia Puig of USA Today praised it as "a summer blockbuster firing on all cylinders".
 New York magazine, called it a "flagrant crib," adding that "Abrams has probably been fighting not to reproduce Spielbergs signature moves since the day he picked up a camera. Now, with the blessing of the master, he can plagiarize with alacrity." 

===Accolades===
{| class="wikitable sortable" width="99%"
|- style="background:#ccc; text-align:center;" List of awards and nominations
|- style="background:#ccc; text-align:center;"
! Award
! Category
! Recipient(s) and nominee(s)
! Result
|- 4th Annual Coming of Age Awards  Best Newcomer Joel Courtney
| 
|- Best Cinematography Larry Fong
| 
|- Special Soundtrack
|
| 
|- Central Ohio Film Critics Association Best Picture
|
| 
|- SFX Awards Best Film
|
| 
|- Best Director
|J. J. Abrams
| 
|- 10th Annual TSR Awards  Actress of the Year  (Multiple Roles)  Elle Fanning
| 
|- Best Visuals: Special Effects
|
| 
|- 38th Saturn Awards  Best Science Fiction Film
|
| 
|- Best Performance by a Younger Actor Joel Courtney
| 
|- Best Performance by a Younger Actor Elle Fanning
| 
|- Best Director
|J. J. Abrams
| 
|- Best Writing
|J. J. Abrams
| 
|- Best Music Michael Giacchino
| 
|- Best Editing Maryann Brandon and Mary Jo Markey
| 
|- Best Special Effects
|
| 
|- 48th Annual CAS Awards  Best Sound Mixing
|
| 
|- 2011 BAM Awards  Best Picture
|
| 
|- Best Director
|J. J. Abrams
| 
|- Best Cinematography Larry Fong
| 
|- Best Makeup
|
| 
|- Best Original Screenplay
|J. J. Abrams
| 
|- Best Editing Maryann Brandon and Mary Jo Markey
| 
|- Best Score Michael Giacchino
| 
|- Best Sound Editing/Mixing
|
| 
|- Best Visual Effects
|
| 
|- Best Costumes
|
| 
|- Best Cast
|
| 
|- Best Youth Ensemble
|
| 
|- Best Performance by a Child Actress in a Leading Role Elle Fanning
| 
|- Best Performance by a Child Actor in a Leading Role Joel Courtney
| 
|- Best Performance by a Child Actor in a Supporting Role Ryan Lee Ryan Lee
| 
|- Best Young Actor/Actress Elle Fanning
| 
|- 17th Empire Awards Best Sci-Fi/Fantasy
|
| 
|- Best Female Newcomer Elle Fanning
| 
|- 2011 St. Louis Film Critics Association Awards Best Visual Effects
|
| 
|- 2011 Phoenix Film Critics Society Awards  Best Editing Maryann Brandon and Mary Jo Markey
| 
|- Best Ensemble Acting
|
| 
|- Best Film
|
| 
|- Best Original Score Michael Giacchino
| 
|- Best Youth Performance&nbsp;— Male Joel Courtney
| 
|- Best Youth Performance&nbsp;— Female Elle Fanning
| 
|- Breakthrough Performance&nbsp;— On Camera Elle Fanning
| 
|- 16th Satellite 2011 Satellite Awards  Best Supporting Actress Elle Fanning
| 
|- Best Original Score Michael Giacchino
| 
|- Best Visual Effects Dennis Muren, Paul Kavanagh, Russell Earl
| 
|- Best Sound (Editing & Mixing) Andy Nelson, Anna Behlmer, Ben Burtt, Mark Ulano, Matthew Wood, and Tom Johnson
| 
|- 2011 Scream Awards 
|- The Ultimate Scream
|
| 
|- Best Science Fiction Movie
|
| 
|- Best Director
|J. J. Abrams
| 
|- Best Scream-Play
|J. J. Abrams
| 
|- Breakout Performance&nbsp;— Female Elle Fanning
| 
|- Holy Sh!t Scene Of The Year The Train Crash
| 
|- 2011 Teen Choice Awards 
|- Teen Choice Award for Choice Movie - Sci-Fi/Fantasy|Sci-Fi/Fantasy Movie
|
| 
|- Teen Choice Award for Choice Movie Actress - Sci-Fi/Fantasy|Sci-Fi/Fantasy Actress Elle Fanning
| 
|- Breakout Male Joel Courtney
| 
|- Teen Choice Scene Stealer Male Riley Griffiths
| 
|- Chemistry
|The Super 8 Crew
| 
|- Teen Choice Hissy Fit The Alien
| 
|- 17th Critics 2012 Broadcast Film Critics Association Awards 
|- Best Action Movie
|
| 
|- Best Sound
|
| 
|- Best Visual Effects
|
| 
|- Best Young Actor/Actress Elle Fanning
| 
|- Motion Picture Golden Reel Awards  Music in a Feature Film
|
| 
|- Dialogue and ADR in a Feature Film
|
| 
|- Sound Effects and Foley in a Feature Film
|
| 
|- Hollywood Film Festival Spotlight Award Elle Fanning
| 
|- YouReviewer Awards  Best Supporting Actress Elle Fanning
| 
|- Best Visual Effects
|
| 
|- Breakthrough Actor Joel Courtney
| 
|- 33rd Young Artist Awards  Young Artist Best Performance in a Feature Film - Leading Young Actor Joel Courtney
| 
|- Young Artist Best Performance in a Feature Film - Leading Young Actress Elle Fanning
| 
|- Best Performance in a Feature Film&nbsp;— Supporting Young Actor Zach Mills
| 
|- Best Performance in a Feature Film&nbsp;— Young Ensemble Cast
| 
| 
|- 2012 MTV Movie Awards MTV Movie Breakthrough Performance  Elle Fanning
| 
|} Best Original BAFTA Award for Best Original Screenplay, Best Sound, and Best Special Visual Effects. Paramount submitted it for several considerations for the BAFTAs including Best Film, Best Director (J. J. Abrams), Best Original Screenplay, Leading Actor (Kyle Chandler), Supporting Actress (Elle Fanning), Supporting Actor (Joel Courtney, Gabriel Basso, Noah Emmerich), Cinematography, Production Design, Editing, Costume Design, Original Music, Sound, Makeup and Hair, and Special Visual Effects.

==References==
 

==External links==
* 
* 
* 
* 

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 