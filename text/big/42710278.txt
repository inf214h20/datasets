And the Heavens Above Us
{{Infobox film
| name = And the Heavens Above Us
| image =
| image_size =
| caption =
| director = Josef von Báky
| producer =  Richard König   
| writer =  Gerhard Grindel
| narrator =
| starring = Hans Albers   Paul Edwin Roth   Lotte Koch   Annemarie Hase
| music = Theo Mackeben  
| cinematography = Werner Krien   Wolfgang Becker    
| studio = Objektiv Film 
| distributor = Schorcht Film
| released = 9 December 1947
| runtime = 103 minutes
| country = Germany  German 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}}
And the Heavens Above Us (German:Und über uns der Himmel) is a 1947 German drama film directed by Josef von Báky and starring Hans Albers, Paul Edwin Roth and Lotte Koch. It was part of the post-war series of rubble films. 

==Cast==
* Hans Albers as Hans Richter  
* Paul Edwin Roth as Werner Richter  
* Lotte Koch as Edith Schröder  
* Annemarie Hase as Frau Burghardt  
* Heidi Scharf as Mizzi Burghardt  
* Ralph Lothar as Fritz  
* Otto Gebühr as Studienrat Heise  
* Elsa Wagner as Frau Heise  
* Ursula Barlen as Frau Roland 
* Ludwig Linkmann as Georg  
* Helmuth Helsig as Harry

== References ==
 

== Bibliography ==
* Bergfelder, Tim. International Adventures: German Popular Cinema and European Co-Productions in the 1960s. Berghahn Books, 2005.

== External links ==
*  

 

 

 
 
 
 
 
 
 
 
 