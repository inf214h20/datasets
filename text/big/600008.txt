Steel Magnolias
 
{{Infobox film
| name = Steel Magnolias
| image = Steel magnolias poster.jpg
| caption = Theatrical release poster
| director = Herbert Ross
| producer = Ray Stark Andrew Stone Victoria White Robert Harling Kevin J. OConnor Sam Shepard
| studio = Rastar
| distributor = TriStar Pictures
| released =  
| runtime = 117 minutes 
| country = United States
| gross = $95,904,091 {{cite web
 | title = Steel Magnolias at Box Office Mojo
 | url = http://www.boxofficemojo.com/movies/?id=steelmagnolias.htm
 | accessdate = 2010-09-29}} 
| language = English
| music = Georges Delerue
}} Robert Harlings play of magnolia flower, and as tough as steel. 

==Plot== type one hypoglycemic state but recovers quickly after having some orange juice. MLynn explains Shelby was recently informed by doctors that she should not have children because her body might not withstand childbirth.

Later that afternoon, Ouiser Boudreaux (Shirley MacLaine) arrives in the salon and questions Annelle about her past, forcing Annelle to reveal that her husband Bunkie Dupuy is a dangerous criminal on the run from the police. Moved by Annelles emotional confession, Shelby invites Annelle to the wedding, where Annelle meets Sammy.

Several months pass and Shelby returns to town to celebrate Christmas. During the festivities she announces that she and her husband Jackson Latcherie (Dylan McDermott) are expecting their first child. Shelbys father Drum (Tom Skerritt) is thrilled, but MLynn is too worried to share in the joy. Even when Shelby confesses that she hopes the arrival of a baby might make her marriage a little easier, MLynn is unable to rejoice. Truvy, Annelle, and Clairee had originally thought that Shelby Infertility|couldnt have children, but on the night of the big announcement, MLynn clarified for them that the doctors said Shelby shouldnt have children because of her diabetes, and that there is a very big difference. Ouiser then says that "this baby is not exactly great news," meaning Shelby could actually die in childbirth because of her diabetes. Unable to give her any words of wisdom, Truvy suggests they focus on the joy of the situation: Jackson and Shelbys first child, as well as Drum and MLynns first grandchild, as well as their boys, Jonathan and Tommys first nephew. MLynn agrees, saying that nothing pleases Shelby more than proving her wrong.
 Jonathan Ward) and Tommy (Knowl Johnson), have enough food to last until MLynn returns home after the transplant. Later, on Halloween, Ouiser, Clairee, Truvy, and MLynn throw Annelle a surprise wedding shower. Shelby is unavailable to attend, due to a conflicting schedule with her nursing job, and is later found unconscious on the porch of her house.

Shelby is rushed to the hospital, where its determined that her body rejected the new kidney, sending her into a coma. The doctors inform the family that Shelby is likely to remain comatose indefinitely, and her family and husband jointly decide to take her off life support. At the funeral, after the other mourners have left, MLynn breaks down in hysterics in front of Ouiser, Clairee, Truvy, and Annelle but is comforted by the other women.
 Kevin J. OConnor), met. MLynn agrees, and assures Annelle that Shelby would love it. Months later, on Easter morning, Annelle goes into labor during an Easter egg hunt, is rushed to the hospital by Truvy and her husband Spud (Sam Shepard), and another life begins.

==Cast==
{| class="wikitable sortable"
|-
! ACTOR
! CHARACTER
! RELATIONSHIP
|-
| Sally Field
| Mary Lynn "MLynn" Eatenton
| Jonathan, Tommy, and Shelbys mother; Drums wife, Jacksons mother-in-law and Jack Jrs maternal grandmother
|-
| Dolly Parton
| Truvy Jones
| Beautician; Spuds wife, and Louies mother
|-
| Shirley MacLaine
| Louisa "Ouiser" Boudreaux
| Neighborhood friend/"curmudgeon"
|-
| Daryl Hannah
| Annelle Dupuy-Desoto
| Newcomer; Beautician, wife of Sammy
|-
| Olympia Dukakis
| Clairee Belcher
| Neighbor friend; former first lady, sister of Drew Marmillion, sister-in-law of Belle Marmillion, and aunt of Marshall and Nancy Beth Marmillion.
|-
| Julia Roberts
| Shelby Eatenton-Latcherie
| Drum and MLynns daughter, Jonathan and Tommys sister, Jacksons wife, and Jack Jrs mother.
|-
| Tom Skerritt
| Drum Eatenton
| MLynns husband, Shelby, Jonathan & Tommys father, Jacksons father-in-law, and Jack Jrs maternal grandfather.
|-
| Sam Shepard
| Spud Jones
| Truvys husband and Louies father.
|-
| Dylan McDermott
| Jackson Latcherie
| Shelbys husband, Jack Jr.s father, Drum and MLynns son-in-law, and Jonathan and Tommys brother-in-law.
|- Kevin J. OConnor
| Sammy Desoto
| Annelles Husband
|-
| Bill McCutcheon
| Owen Jenkins
| Ouisers former boyfriend
|-
| Ann Wedgeworth
| Aunt Fern Thornton
| Jacksons aunt
|-
| Knowl Johnson
| Tommy Eatenton
| Drum and MLynns son, Jonathan and Shelbys brother, Jacksons brother-in-law and Jack Jrs maternal uncle.
|- Jonathan Ward
| Jonathan Eatenton
| Drum and MLynns son, Tommy and Shelbys brother, Jacksons brother-in-law and Jack Jrs maternal uncle.
|-
| Ronald Young
| Drew Marmillion
| Clairees brother, husband to Belle, and father of Marshall and Nancy Beth
|-
| Bibi Besch
| Belle Marmillion
| Drews wife, mother of Marshall and Nancy Beth, and Clairees sister-in-law
|-
| Janine Turner
| Nancy-Beth Marmillion
| Drew and Belles daughter; Clairees niece, Marshalls sister
|-
| James Wlcek
| Marshall Marmillion
| Drew and Belles son; Clairees nephew, and Nancy Beths brother
|- Tom Hodges
| Louie Jones
| Truvy and Spuds son
|-
| C. Houser
| Jackson Latcherie, Jr. (1 year old)
| Jackson and Shelbys son, Drum and MLynns maternal grandson, and Jonathan and Tommys maternal nephew.
|-
| Daniel Camp
| Jackson Latcherie, Jr. (3 years old)
| Jackson and Shelbys son, Drum and MLynns maternal grandson, and Jonathan and Tommys maternal nephew.
|-
|}

==Background== original play described the experience of the family and friends of the play author Harling following the 1985 death of his sister from diabetic complications after the birth of his namesake nephew and failure of a family member donated kidney. A writer friend continuously encouraged him to write it down in order to come to terms with the experience. He did but originally as a short story for his nephew the latter to get an understanding of the deceased mother. It eventually evolved in ten days to the play.  

==Production==
Released by   served as both the 1989 film location and scenario location  with historian Robert DeBlieux, a former Natchitoches mayor, as the local advisor. 

==Reception==
 

It received mixed-to-positive reviews and has 65% on Rotten Tomatoes."  An example of a less enthusiastic critic was Hal Hinson of The Washington Post, who said that it felt "more Hollywood than the South."  An example of a more enthusiastic critic was Roger Ebert, who said that the film was "willing to sacrifice its over-all impact for individual moments of humor, and while that leaves us without much to take home, youve got to hand it to them: The moments work."  

The movie received a limited release on November 15, 1989: entered the U.S. box office at #4 with an opening weekend gross of $5,425,440; by the time of wider release two days later it grossed $15,643,935; stayed in the top 10 for 16 weeks, gross $83,759,091 domestically with a further $12,145,000 with foreign markets giving a worldwide gross of $95,904,091. 

==Home media==
The film was released on VHS on June 19, 1990 and on DVD July 25, 2000, allowing the film to gross a further $40 million.   The movies overall gross was $135,904,091. The film was released on Blu-ray through the boutique label Twilight Time, on September 11, 2012.

==Awards and nominations==
{| class="wikitable sortable"
|-
! Year
! Association
! Category
! Nominated work
! Result
|-
| 1990
| Academy Awards Best Supporting Actress
| Julia Roberts
|  
|-
| 1990
| American Comedy Awards Funniest Supporting Actress in a Motion Picture
| Olympia Dukakis
|  
|-
| 1990
| American Comedy Awards Funniest Supporting Actress in a Motion Picture
| Shirley MacLaine
|  
|-
| 1990
| Chicago Film Critics Association Awards Best Supporting Actress
| Shirley MacLaine
|  
|-
| 1990
| Golden Globe Awards Best Actress – Motion Picture Drama
| Sally Field
|  
|-
| 1990
| Golden Globe Awards Best Supporting Actress – Motion Picture
| Julia Roberts
|  
|-
| 1990
| Peoples Choice Awards Favorite Dramatic Motion Picture
| Steel Magnolias
|  
|-
| 1991
| BAFTA Awards Best Actress in a Supporting Role
| Shirley MacLaine
|  
|-
|}
 My Left Foot). 

==Adaptation - television==
  as MLynn, Sally Kirkland as Truvy, Elaine Stritch as Ouiser, Polly Bergen as Clairee and Sheila McCarthy as Annelle. CBS cancelled further broadcast. 

==Remake 2012==
 
  (MLynn), Jill Scott (Truvy), Alfre Woodard (Ouiser), Phylicia Rashād (Clairee), Adepero Oduye (Annelle) and Condola Rashād (Shelby).   The New York Times had mixed reactions: applauded it on some points and on others as either schmaltz or less attentive than the 1989 film. 

==See also==
*List of films featuring diabetes
*Lists of films

==References==
 

==External links==
 
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 