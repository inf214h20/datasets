House (1986 film)
 
{{Infobox film
| name           = House
| image          = Housefilmposter.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Steve Miner
| producer       = Richard F. Brophy Sean S. Cunningham Patrick Markey Roger Corman
| screenplay     = Ethan Wiley
| story          = Fred Dekker
| starring = {{Plainlist|
* William Katt
* George Wendt
* Richard Moll
* Kay Lenz
}}
| music          = Harry Manfredini
| cinematography = Mac Ahlberg
| editing        = Michael N. Knue
| distributor    = New World Pictures
| released       = February 28, 1986
| runtime        = 93 minutes
| country        = United States
| language       = English
| budget         = $3,000,000
| gross          = $22,144,631
}}

House is a 1986  ,   and House IV.

==Plot==
Roger Cobb (William Katt), an author of horror novels, is a troubled man. He has recently separated from his wife (Kay Lenz); their only son has disappeared without a trace; and his favorite aunt (Susan French), has just died, an apparent suicide by hanging. On top of everything else, it has been more than a year since the release of his latest book and he is being pressured by his publisher to write another.

To the chagrin of his fans and publisher, Cobb plans a novel based on his experiences in Vietnam instead of another horror story. It is not so much that he is interested in the subject, it is more a way of purging himself of the horrors he himself experienced while there.

After his aunts funeral, instead of selling her house, as recommended by the estate attorney, Cobb decides to live there for a while to try to write. Having spent a great deal of time in the house as a child, there are a lot of memories still there for him.

After moving in, Cobb begins to have powerful graphic nightmares. Thoughts about his army buddy, Big Ben (Richard Moll), who died in Vietnam, come spilling out. In addition, strange phenomena spring forth from the house itself, haunting him in his waking hours as well. He tries communicating his fears to his nosy next door neighbor, Harold (George Wendt), but Harold thinks he is crazy.

One night while investigating a noise coming from his late aunts bedroom, Cobb is attacked by "something" in the shape of a horrible beast. More strange things happen: garden tools embed themselves in the door near his head; his wife turns up on the doorstep one day, and as he says hello, she transforms into a hideous hag (Sandywitch), which he shoots; then Cobb battles gremlin creatures (Little Critters) that are attempting to kidnap a neighbor’s child Cobb is reluctantly babysitting.

Eventually Cobb finds what appears to be an entry into a sinister otherworld—through the bathroom medicine cabinet. Looking into the void, he is pulled into the darkness by an unseen creature. In the darkness however, he fortuitously locates his lost son, Jimmy.

Cobb manages to escape with Jimmy but, as they are leaving the house, they are confronted by the "living," partially decomposed corpse of Big Ben. Because Cobb had failed to kill him when he was seriously wounded in Vietnam, and had instead allowed him to be taken prisoner and tortured before dying, Ben reveals that he has been out to destroy Cobb.

Cobb himself confronts Ben, aware that his anger over the kidnapping of his Son has overwhelmed his fears. Unable to instill fear in Cobb any longer, Ben is defeated. Cobb destroys him and escapes with his son. He glances back at the house, triumphantly; he has beaten it, and regained control of his life.

==Cast==
* William Katt as Roger Cobb
* George Wendt as Harold Gorton
* Richard Moll as Big Ben
* Kay Lenz as Sandy Sinclair
* Mary Stavin as Tanya
* Michael Ensign as Chet Parker
* Susan French as Aunt Elizabeth Hooper
* Erik and Mark Silver as Jimmy
* Peter Pitofsky as Sandywitch
* Felix Silla as Little Critter
* Elizabeth Barrington as Little Critter
* Jerry Maren as Little Critter
* Dino Andrade as Little Critter (Critter Voices)
* Mindy Sterling as Woman in Bookstore

Kane Hodder was the stunt cordinator on the film.

==Release==
House opened in 1,440 theaters on February 28, 1986 and grossed $5,923,972 in its opening weekend, missing first place to Pretty in Pink.  By the end of its run, House had grossed $19,444,631 in the domestic box office, making it a moderate commercial success due to its $3,000,000 budget.

The film holds a 50% fresh rating on movie review aggregator website Rotten Tomatoes based on ten reviews. 

==Awards==

In 1987, Richard Moll and Kay Lenz were both nominated for Saturn Awards. Director Steve Miner won a Critics Award for his work on the film and was nominated for an International Fantasy Film Award.

==Soundtrack==

The soundtrack for house was released on  . 

; Side one

# "Opening Titles"
# "The Abduction"
# "Hey, Roy!"
# "A Fiery Sandywitch"
# "Ding-Bat Attack"
# "2nd Hand"
# "Viet Memories/The Chimney"
# "Big Ben Chase"
# "Cujo, The Racoon"
# "Viet Rescue"
# "TransparAuntie"
# "Roger Gets a Wand"
# "Close Shave"

; Side two

# "Opening Titles"
# "An Alternate Universe"
# "Avast Ye Azteacs"
# "Theres a Jungle in There!"
# "Skulldiggery"
# "Looking for the Varmit Who Shot My Father"
# "Arnold the Barbarian"
# "Petra, Petra, Petra"
# "I Love You, Gramps"
# "A Rare Commodity"
# "Finale Grande"
# "End Titles"

==Trivia==

The Victorian architecture style house that was used for filming on location is located at 329 Melrose Avenue, Monrovia, east of Pasadena, California. 

==See also==
*List of ghost films

==References==
 

==External links==
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 