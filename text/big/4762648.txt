University of Laughs
{{Infobox Film
| name           = Warai no Daigaku
| image          = 
| image_size     = 
| caption        = 
| director       = Hoshii Mamoru
| producer       = 
| writer         = 
| narrator       = 
| starring       = Kōji Yakusho  Goro Inagaki
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       = 2004 
| runtime        = 
| country        = Japan
| language       = Japanese 
| budget         = 
| preceded_by    = 
| followed_by    = 
}}

Warai no Daigaku: (笑の大学) University of Laughs is a story by Japanese dramatist Kōki Mitani which began as a play in 1996 starring actors Masahiko Nishimura as the Censor and Yoshimasa Kondo as the Playwright. The play won the Best Play Award at the 1996 Yomiuri Theater Awards. 

After success it was then made into a movie in 2004 starring actors Kōji Yakusho and Goro Inagaki by Fuji Television and Toho Studios. The major theme of the piece is the conflict between censorship and an artists freedom of expression, but it is a comedy.

==Plot==

Set in 1940,  a young playwright, Tsubaki Hajime (Inagaki / Kondo) comes up against a government censor, Sakisaka Mutsuo (Yakusho / Nishimura). The censors job is to prevent anything political or taboo from getting into the pre-war media, but this censor has a thing against comedy, too.

Tsubaki comes to have his script checked by the censors before rehearsals begin. But the censor, who is looking for an excuse to shut down the comedy troupe at which Tsubaki works, tells him that his whole play is rubbish and Tsubaki would have to rewrite it completely before Sakisaka would let it be performed.

But what starts as cruel teasing makes the once poor-quality play better and better as Tsubaki returns day after day to have it torn to shreds and criticized over and over again. Finally the play is perfected and Sakisakas dislike of Tsubaki turns into respect for his talent.

==Production==
This is a two-man play that is given dimension through film. 

The play was originally performed at Aoyama Round Theater in Shibuya, Tokyo|Shibuya, Tokyo, Japan in 1996 with high acclaim.　In 1998, encore played with original cast at Parco Theater in Shibuya, Tokyo.

The movie was then released in 2004 by Toho Studios, and directed by Hoshii Mamoru, who was chosen by the screenwriter himself for his previous work in directing one of his other successful stories, Furuhata Ninzaburo.

===Last Laugh===
 Shibuya previewed West End British actors Martin Freeman as the Playwright and Roger Lloyd-Pack as the Censor.

===Differences Between the Play and Movie===

The only major differences in the two versions was the addition of other non-speaking characters in the opening sequence of the movie for placement, and the cut of a running gag about a stranded crow being cared for by Sakisaka (the Censor) in the original play.

==Writers Take==

Mitani Koki, the writer of the scenario, claims that his inspiration for Tsubaki came from Kikuya Sakae, who was the writer for the comedy star of the Shōwa period, Kenichi Enomoto. He claims that the play was not meant to be any sort of political commentary, nor was it inspired by current events. Instead, Mitani is known to often turn to history for jidaigeki such as his popular Shinsengumi serial drama.

Even though the story deals with censorship, the story does not portray a negative view of it. In fact, the charm of this story is the clever way in which Tsubakis play improves as a result of having to work around the constrictions that Sakisaka places upon him. If it were a commentary on the oppression of artists, then Mitani would have presented the censorship in a more negative light.

However, in an interview for The Japan Times Mitani did admit that the play was timely and had potential to be sympathised with even by people from other countries and cultures.

==Notes==
 

== External links ==
*  
*  
*  
*  

 
 