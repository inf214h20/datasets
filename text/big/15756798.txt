Leatherstocking (serial)
 
{{Infobox film
| name           = Leatherstocking
| image          = 
| caption        = 
| director       = George B. Seitz
| producer       = C. W. Patton
| writer         = James Fenimore Cooper (novel)
| starring       = Edna Murphy Harold Miller
| cinematography = 
| editing        = 
| distributor    = Pathé|Pathé Exchange
| released       =  
| runtime        = 10 episodes 
| country        = United States 
| language       = Silent with English intertitles
| budget         = 
}}
 Western film serial directed by George B. Seitz.

==Cast==
* Edna Murphy - Judith Hutter
* Harold Miller - Leatherstocking
* Whitehorse - Tom Hutter
* Frank Lackteen - Briarthorn
* Ray Myers - Rivenoak
* James Pierce - Harry March
* Lillian Hall - Hetty Hutter
* Aline Goodwin - Wah-Ta-Wah David Dunbar - Chingachgook
* Tom Tyler - Indian (as Vincent Markowski)
* Emily Barrye

==See also==
* List of film serials
* List of film serials by studio

==External links==
* 

 
 

 
 
 
 
 
 
 
 
 

 
 