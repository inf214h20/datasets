Beta (film)
 
 
{{Infobox film
| name           = Beta
| image          = Betafilm.jpg
| image_size     =
| caption        =
| director       = Indra Kumar
| producer       = Indra Kumar Ashok Thakeria
| writer         =
| narrator       =
| starring       = Anil Kapoor Madhuri Dixit Aruna Irani Laxmikant Berde Anupam Kher
| music          = Anand-Milind
| cinematography =
| editing        = Hussain Burmawalla
| studio         =
| distributor    = Maruti International
| released       =  
| runtime        = 172 mins
| country        = India
| language       = Hindi
| budget         =
| website        =
}}
 Rajkumar and Ravichandran and Madhubala in the lead roles. The 1992 film is best remembered for the hit track, "Dhak Dhak Karne Laga", a remake of maestro Ilaiyaraajas composition "Abba Nee Teeyani Debba" originally made for the Telugu cinema, Jagadeka Veerudu Atiloka Sundari.
 Best Actress, Best Female Best Supporting Actress, Aruna Irani. It was the highest grossing film of 1992. 

==Plot==
Beta is the story of Raju (Anil Kapoor), the only child of a widowed multi-millionaire father who can provide his son with anything he wants except Rajus only desire—a mothers love. Rajus father believes he can please his son by marrying Laxmi (Aruna Irani), thinking that she will care for Raju as her natural son. Raju becomes devoted to his stepmother, does whatever she asks of him; she convinces her husband that Raju should remain naïve and uneducated otherwise he would work for others rather than be self-employed. Raju grows older his step-mother increasingly isolates his father from the family, is considered mentally incompetent and eventually locked within a room of the family home.

Raju meets Saraswati (Madhuri Dixit) and following she being abducted and assaulted at a fair, he rescues her. The two fall in love and despite the villagers believing she is no longer chaste he marries her. Saraswati discovers that her step-mother-in-laws motherly love for Raju is a rouse in an attempt to discredit the sanity of Rajus father and thus unable to interfere with the plan Laxmi has for the family fortune to be diverted to Rajus step brother, the natural result of the marriage between his father and step-mother. He also shares in the greed for the family fortune. A battle of wills between the step-mother and the step-daughter-in-law ensues.

Laxmi sees that her influence over the family is being challenged by Saraswati who insists that Rajus father leave behind his prison and return to the family circle as there is nothing wrong with him. And she makes Raju aware of the intentions of his step-mother. Laxmi starts to abuse and embarrass Saraswati with all the family members present. Saraswati is ready to leave but then, to protect her husband and her house from Laxmis intentions, she decides to apologise to her mother-in-law. Saraswati then cleverly starts exposing Laxmis every effort and intention in a dignified manner so that her husband will not be offended. The medical school degree of Rajus step-brother was bought rather than earned through studies and Laxmi is set up to slip on the pavement so that the over-protectiveness that she has instilled in Raju will force her remain in bed as he will serve her without pause; Laxmi never has a moment to herself and her scheming.

Saraswati becoming pregnant prompts Laxmi to attempt to kill Ragus wife and unborn child with poison, saffron and milk. Saraswati discovers this and returns to her direct approach by telling Raju. He continues to refuse to believe the treachery although she takes an oath upon her unborn childs life. Raju defends his step-mother and offers to prove that Saraswati is wrong. Raju drinks the milk and then coughs up blood. He comes to the realisation of what Saraswati said was all along his step-mothers intentions. In his usual innocent manner, he asks her why, that all she had to do as his mother was simply ask for the wealth—he would have happily agreed to give her it all. He tells Laxmi that his wish to die in peace would be accomplished if she, to at least once with a clean heart, call him her son. His words so deeply touch Laxmi that she realises her cruelty has been directed at the only son who all along had ever loved her. Rajus step brother physically confronts his mother as he wants to continue with the plan but the dying Raju saves her.

The film concludes with Raju recovering, agreeing to give up to his mother his worldly possessions, and leaving home with his wife and father. At the last moment, Laxmi begs him not to leave, claiming to have learned the error of her ways. She tears-up the legal papers and tells him that all she wants is nothing more than her son.

==Cast==
*Anil Kapoor as Raju
*Madhuri Dixit as Saraswati
*Aruna Irani as Laxmi
*Laxmikant Berde as Pandu
*Anupam Kher as Totaram
*Priya Arun as Champa
*Kunika as Kunika
*Bharati Achrekar as Mainavati

==Soundtrack==
The soundtrack of Beta was the second best selling album of the year.  Anand-Milind were nominated in the Filmfare, Best Music Directors category, but lost out to Nadeem-Shravan for Deewana. Anuradha Paudwal won her 3rd consecutive Filmfare award for Best Female playback singer. Music directors Dilip Sen – Sameer Sen, Amar-Utpal and Naresh Sharmas compositions are included in the album but not used in the film, nor are they credited in the film titles.


{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="centr"
! # !! Title !! Singer(s)!! Length!! Distinction(s)
|-
| 1
| "Dhak Dhak Karne Laga"
| Udit Narayan, Anuradha Paudwal
| 05:20
| The music of the song was a version of Ilayarajas super hit song "Abbanee teeyani debba" in the Telugu cinema Jagadeka Veerudu Athiloka Sundari (1990) starring Sridevi and Chiranjeevi which was dubbed into Hindi as Aadmi Aur Apsara (1991).
|-
| 2
| "Koyal Se Teri Boli"
| Udit Narayan, Anuradha Paudwal
| 05:38 Tamil movie Enga Chinna Rasa (1987) by K. Bhagyaraj.
|- 
| 3
| "Saiyan Ji Se Chupke"
| Udit Narayan, Anuradha Paudwal
| 07:30
|
|- 
| 4
| "Sajna Main Teri"
| Anuradha Paudwal, Vipin Sachdeva
| 07:14
|
|- 
| 5
| "Dhadkane Saansein Jawani"
| Pankaj Udhas, Anuradha Paudwal
| 05:20
| Music by Dilip Sen – Sameer Sen and lyrics by Dilip Tahir.
|- 
| 6
| "Yeh Do Dil Hain Chanchal"
| Babla Mehta, Anuradha Paudwal
| 06:52
| Music by Amar-Utpal and lyrics by Naqsh Lyallpuri.
|- 
| 7
| "Bhool To Maa Se"
| Udit Narayan
| 02:17
|
|- 
| 8
| "Kushiyon Ka Din Aaya Hai"
| Anuradha Paudwal
| 05:57
| 
|- 
| 9
| "Kitna Pyara Yeh Chehra"
| Anuradha Paudwal, Indrajeet
| 04:40
| Music by Naresh Sharma and lyrics by Dev Kohli.
|- 
| 10
| "Nach Mudiya"
| Anuradha Paudwal, Vipin Sachdeva
| 06:48
| Music by Naresh Sharma and lyrics by Dev Kohli.
|}

==Filmfare Awards==

===Won===  Best Actor – Anil Kapoor Best Actress – Madhuri Dixit Best Supporting Actress – Aruna Irani Best Female Singer – Anuradha Paudwal for "Dhak Dhak Karne Laga" Best Choreographer – Saroj Khan

===Nominated===  Best Film – Indra Kumar, Ashok Thakeria Best Director – Indra Kumar Best Performance in a Comic Role – Laxmikant Berde Best Music Director – Anand-Milind

==Other versions==
The story line has been inspiration for movies and remakes in the Indian film industry.
{| class="wikitable"
|- Year !! Title !! Language !! Director !! Cast
|-
!Step-mother !! Son !! Wife
|- Rajkumar || B Sarojadevi 
|- Oriya || Prashanta Nanda || Mahasweta Ray
|-
| 1981 || Jyothi (1981 film)|Jyothi || Hindi language || Pramod Chakravorty || Shashikala || Jeetendra|| Hema Malini
|-
| 1987 || Enga Chinna Rasa || Tamil language || K. Bhagyaraj || C. R. Saraswati Radha
|-
| 1992 || Beta || Hindi language || Indra Kumar || Aruna Irani || Anil Kapoor || Madhuri Dixit
|- Venkatesh ||Meena Meena
|-
| 1993 || Annayya (1993 Kannada film)|Annayya || Kannada language || D._Rajendra_Babu || Aruna Irani || V. Ravichandran || Madhoo 
|}

==References==
 

==External links==
* 

 
 
 
 
 
 