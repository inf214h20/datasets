The Roaring Forties
The French drama film directed by Christian de Chalonge and starring Jacques Perrin, Julie Christie and Michel Serrault.  The film was loosely based on the book The Strange Last Voyage of Donald Crowhurst by Nicholas Tomalin about the death of the British round the world yachtsman Donald Crowhurst in 1968.

==Cast==
* Jacques Perrin ...  Julien Dantec
* Julie Christie ...  Catherine Dantec
* Michel Serrault ...  Sébastien Barral
* Gila von Weitershausen ...  Emilie Dubuisson
* Heinz Weiss ...  Joss
* Jean Leuvrais ...  Dorange
* François Perrot ...  Le présentateur TV
* Christian Ferry ...  Granville
* Bernard Lincot ...  Janvier
* Eric Raphaël ...  Denis Dantec
* Solena Morane ...  Valérie Dantec
* Mohammed Jalloh ...  Carlos
* Guy Parigot ...  Gouarzin
* Sébastien Keran ...  Jaouen
* René Dupré ...  Pietro Corres

==References==
 

==External links==
* 

 
 
 
 
 
 
 

 