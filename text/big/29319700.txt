Bonjour sourire
{{Infobox film
| name           = Bonjour sourire
| image          = 
| caption        = 
| director       = Claude Sautet
| producer       = Vox Films (France)
| writer         = Jean Marsan Yves Robert Darry Cowl Jean Carmet Annie Cordy
| starring       = Henri Salvador Louis de Funès
| music          = Jean Constantin
| cinematography = 
| editing        = 
| distributor    = Sirius
| released       = 29 February 1956 (France)
| runtime        = 90 minutes
| country        = France
| awards         = 
| language       = French
| budget         = 
| gross = $8,169,739 
| preceded_by    = 
| followed_by    = 
}}
 French Comedy comedy film from 1956, directed by Claude Sautet, written by Jean Marsan, starring Henri Salvador and Louis de Funès. The film is known under the titles "Die tolle Residenz" (West Germany), "Sourire aux lèvres" (Belgium French title). 

== Cast ==
* Henri Salvador : himself
* Louis de Funès : M. Bonoeil, the secretary of the prime minister
* Jimmy Gaillard : in his own role
* Olga Thorel : the princess Aline, Daisy, Sophie
* Marcel Lupovici : Le chevalier Anatole dErceny, premier ministre
* Darry Cowl : Le médecin de la principauté
* Jean Carmet : Jean Courtebride
* Lisette Lebon : Rosette, la soubrette
* André Philipp : Le roi de la principauté du Monte-Néro
* Annie Cordy : Herself
* Christian Duvaleix : Himself
* Harry Max : Double Note, professor of music
* Bernard Musson : the butler
* Pierre Duncan : a policeman

== References ==
 

== External links ==
*  
*   at the Films de France

 

 
 
 
 
 
 


 

 