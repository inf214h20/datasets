Double Indemnity (film)
{{Infobox film
| name           = Double Indemnity
| image          = Double indemnity.jpg
| caption        = Theatrical release poster
| director       = Billy Wilder
| producer       = Buddy DeSylva Joseph Sistrom
| screenplay     = Billy Wilder Raymond Chandler
| based on       =  
| narrator       = Fred MacMurray
| starring       = Fred MacMurray Barbara Stanwyck Edward G. Robinson
| music          = Miklós Rózsa
| cinematography = John F. Seitz
| editing        = Doane Harrison (sup)
| distributor    = Paramount Pictures (original) Universal Studios (current)
| released       =  
| runtime        = 107 minutes
| country        = United States
| language       = English
| budget         = $980,000 Ed Sikov|Sikov, Ed (1998). On Sunset Boulevard: The Life and Times of Billy Wilder. New York: Hyperion. ISBN 978-0-7868-6194-1. p. 211 
| gross          = $5,000,000
}} James M. novella of the same name, which originally appeared as an eight-part serial in Liberty (1924–1950)|Liberty magazine.

The film stars Fred MacMurray as an insurance salesman, Barbara Stanwyck as a provocative housewife who wishes her husband were dead, and Edward G. Robinson as a claims adjuster whose job is to find phony claims.  The term "double indemnity" refers to a clause in certain life insurance policies that doubles the payout in cases when death is caused by certain accidental means.

Praised by many critics when first released, Double Indemnity was nominated for seven Academy Awards but did not win any. Widely regarded as a classic, it is often cited as a paradigmatic film noir and as having set the standard for the films that followed in that genre.
 100 best 10th Anniversary list.

==Plot==
 .]]
Walter Neff (Fred MacMurray), a successful insurance salesman, returns to his office building in downtown Los Angeles late one night. Visibly in pain, he begins dictating a confession into a Dictaphone for his friend and colleague, Barton Keyes (Edward G. Robinson), a brilliant claims adjuster. The story, told primarily in Flashback (narrative)|flashback, ensues.

Neff first meets the alluring Phyllis Dietrichson (Barbara Stanwyck) during a routine house call to remind her husband that his automobile insurance policy is up for renewal. They flirt, until Phyllis asks how she could take out an accident policy on her husbands life without his knowledge. Neff deduces she is contemplating murder, and makes it clear he wants no part of it.

However, he cannot get her out of his mind, and when Phyllis shows up at his own home, he cannot resist her any longer. Neff knows all the tricks of his trade and devises a plan to make the murder of her husband appear to be an accidental fall from a train that will trigger the "double indemnity" clause and pay out twice the policys face value.
 Palo Alto for a college reunion. Neff is hiding in the backseat and kills Dietrichson when Phyllis turns onto a deserted side street. Then, Neff boards the train posing as the victim and using his crutches. He makes his way to the last car, the observation car, and steps outside to the open platform to supposedly smoke. A complication ensues when he finds a man named Jackson (Porter Hall) there, but he manages to get Jackson to leave. Neff then jumps off at a prearranged spot, and he and Phyllis place Dietrichsons body on the tracks.

Mr. Norton, the companys chief, believes the death was suicide, but Keyes scoffs at the idea, quoting statistics indicating the improbability of suicide by jumping off a slow-moving train, to Neffs hidden delight. Keyes does not suspect foul play at first, but his instincts &ndash; what Keyes refers to as the "little man" in his stomach &ndash; start nagging at him. He wonders why Dietrichson did not file a claim for his broken leg, and deduces he did not know about the policy. He eventually concludes that Phyllis and some unknown accomplice murdered him.

Keyes, however, is not Neffs only worry. The victims daughter, Lola (Jean Heather), comes to him, convinced that stepmother Phyllis is behind her fathers death: Lolas mother also died under suspicious circumstances &ndash; while Phyllis was her nurse. Neff begins seeing Lola, at first to keep her from going to the police with her suspicions and then because he is plagued by guilt and a sense of responsibility for her.

Keyes brings Jackson to Los Angeles. After examining photographs of Dietrichson, Jackson is sure the man he met was not the 51-year-old, but someone about 15 years younger. Keyes is eager to reject the claim and force Phyllis to sue. Neff warns Phyllis not to go to court and admits he has been talking to Lola about her past. Lola eventually tells him she has discovered her boyfriend, the hotheaded Nino Zachetti (Byron Barr), has been seeing Phyllis behind her (and Neffs) back.

When Keyes informs Neff that he suspects Nino of being Phylliss accomplice (Nino has been spotted repeatedly visiting Phyllis at night), Neff sees a way out of his predicament. He arranges to meet Phyllis at her house. He informs her that he knows about her involvement with Nino, and guesses that she is planning to have the other man kill him. He tells her that he intends to kill her and put the blame on Nino. She is prepared, however, and shoots him in the shoulder. Wounded but still standing, he slowly comes closer and dares her to shoot again. She does not, and he takes the gun from her. She says she never loved him "until a minute ago, when I couldnt fire that second shot."  She hugs him tightly, but then pulls away when she feels the gun pressed against her. Neff says, "Goodbye, baby," and shoots twice, killing her.

Outside, Neff waits for Nino to arrive (something Neff had orchestrated). Neff advises him not to enter the house and instead go to "the woman who truly loves you": Lola. Nino is reluctantly convinced and leaves.

Neff drives to his office and starts speaking into his Dictaphone, as seen at the films opening. Keyes arrives unnoticed and hears enough to know the truth. Keyes sadly tells him, "Walter, youre all washed up." Neff tells Keyes he is going to Mexico rather than face the gas chamber, but sags to the floor from his injury before he can reach the elevator. Keyes lights him a cigarette as they await the police and an ambulance.

==Cast==
 
* Fred MacMurray as Walter Neff
* Barbara Stanwyck as Phyllis Dietrichson
* Edward G. Robinson as Barton Keyes
* Porter Hall as Mr Jackson
* Jean Heather as Lola Dietrichson
* Tom Powers as Mr Dietrichson
* Byron Barr as Nino Zachetti
* Richard Gaines as Edward S. Norton, Jr.
* Fortunio Bonanova as Sam Garlopis
* John Philliber as Joe Peters
* Raymond Chandler as man reading book (cameo)

==Production==

===Background=== photo of Sing Sing has been called the most famous newsphoto of the 1920s. 
 The Postman Columbia were all competing to buy the rights for $25,000. Then a letter went out from Joseph Breen at the Hays Office, and the studios withdrew their bids as one. In it Breen warned:

 
The general low tone and sordid flavor of this story makes it, in our judgment, thoroughly unacceptable for screen presentation before mixed audiences in the theater. I am sure you will agree that it is most important…to avoid what the code calls "the hardening of audiences," especially those who are young and impressionable, to the thought and fact of crime. 
 

Eight years later Double Indemnity was included in a collection of Cains works entitled Three of a Kind. Paramount executive Joseph Sistrom thought the material would be perfect for Wilder and they bought the rights for $15,000.  Paramount resubmitted the script to the Hays Office, but the response was nearly identical to the one eight years earlier. Wilder, Paramount executive William Dozier, and Sistrom decided to move forward anyway. They submitted a film treatment crafted by Wilder and his writing partner Charles Brackett,  and this time the Hays Office approved the project with only a few objections: the portrayal of the disposal of the body, a proposed gas-chamber execution scene, and the skimpiness of the towel worn by the female lead in her first scene. 

Cain forever after maintained that Joseph Breen owed him $10,000 for vetoing the property back in 1935 when he would have received $25,000. 

===Writing===
 
After  , then suggested Raymond Chandler.

Wilder would later recall with disappointment his first meeting with Chandler. Envisioning a former private detective who had worked his own experiences into gritty prose, he instead met a man he would later describe as looking like an accountant. Chandler was new to Hollywood, but saw it as a golden opportunity. Not realizing that he would be collaborating with Wilder, he demanded $1000 and said he would need at least a week to complete the screenplay, to which Wilder and Sistrom simply looked at one another in amazement. After the first weekend, Chandler presented eighty pages that Wilder characterized as "useless camera instruction"; Lally, p. 128  Wilder quickly put it aside and informed Chandler that they would be working together, slowly and meticulously. By all accounts, the pair did not get along during their four months together. At one point Chandler even quit, submitting a long list of grievances to Paramount as to why he could no longer work with Wilder. Wilder, however, stuck it out, admiring Chandlers gift with words and knowing that his dialogue would translate very well to the screen. 

Initially, Wilder and Chandler had intended to retain as much of Cain’s original dialogue as possible. It was Chandler, ironically, who first realized that the dialogue from the novella would not translate well to the screen. Wilder disagreed and was annoyed that Chandler was not putting more of it into the script. To settle it, Wilder hired a couple of contract players from the studio to read passages of Cain’s original dialogue aloud. To Wilders astonishment, Chandler was right and, in the end, the movie’s cynical and provocative dialogue was more Chandler and Wilder than it was Cain.  Chandler also did a lot of fieldwork while working on the script and took large volumes of notes. By visiting various locations that figured into the film, he was able to bring a sense of realism about Los Angeles that seeped into the script. For example, he hung around Jerrys Market on Melrose Avenue in preparation for the scene where Phyllis and Walter would discreetly meet to plan the murder. Phillips, Creatures, p. 170 
 cameo at 16:12 into the film, glancing up from a book as Neff walks past in the hallway. This is notable because, other than a snippet from a home movie, there is no other footage of Chandler known anywhere. 
 The Lost Weekend, about an alcoholic writer. Wilder made the film, in part, "to explain Chandler to himself." 

Cain himself was very pleased with the way his book turned out on the screen. After seeing the picture half a dozen times he was quoted as saying, " ... Its the only picture I ever saw made from my books that had things in it I wish I had thought of. Wilders ending was much better than my ending, and his device for letting the guy tell the story by taking out the office dictating machine – I would have done it if I had thought of it." McGilligan, p. 125  In addition to changing the ending, the film made Robinsons character much more pivotal than in the book.

Wilders and Bracketts estrangement during Double Indemnity was not a permanent one. Years later Wilder would characterize their time apart as just another kind of adultery: "1944 was The Year of Infidelities," he said. "Charlie produced   in 1950, then split for good.

===Casting===
 
Having the two protagonists mortally wound each other was one of the key factors in gaining Hays Office approval for the script: the Production Code demanded that criminals pay, on screen, for their transgressions. In addition, Double Indemnity broke new cinematic ground on several fronts, one of those being the first time a Hollywood film explicitly explored the means, motives, and opportunity of committing a murder.  It would take skillful performers to bring nuance to these treacherous characters, and casting the roles of Walter Neff and Phyllis Dietrichson would be a challenge for Wilder.

Sistrom and Wilders first choice for the role of Phyllis Dietrichson was Barbara Stanwyck. At the time, Stanwyck was not only the highest paid actress in Hollywood, but the highest paid woman in America.  (Her eventual co-star MacMurray matched Stanwycks prominence at the pay window: in 1943, he was the highest paid actor in Hollywood, and the fourth highest-paid American. )  Given the nature of the role, Stanwyck was reluctant to take the part, fearing it would have an adverse effect on her career. According to Stanwyck,

 I said, "I love the script and I love you, but I am a little afraid after all these years of playing heroines to go into an out-and-out killer." And Mr. Wilder – and rightly so – looked at me and he said, "Well, are you a mouse or an actress?" And I said, "Well, I hope Im an actress." He said, "Then do the part". And I did and Im very grateful to him. Lally, p. 135 
 

The character of Walter Neff was not only a heel, he was a weak and malleable heel – many Hollywood actors including Alan Ladd, James Cagney, Spencer Tracy, Gregory Peck, and Fredric March passed on it. Lally, p. 134  Wilder even recalls "scraping the bottom of the barrel" and approaching George Raft. Raft was illiterate, so Wilder had to tell him the plot. About halfway through, Raft interrupted him with, "Lets get to the lapel bit." "What lapel bit?" a bewildered Wilder replied. "The lapel," the actor said, annoyed by such stupidity. "You know, when the guy flashes his lapel, you see his badge, you know hes a detective." This was his vision of the film, and since it wasnt part of the story, Raft turned the part down. Maurice Zolotow|Zolotow, Maurice (1977). Billy Wilder In Hollywood. New York: G.P. Putnams Sons. ISBN 978-0-399-11789-3. p. 117.  Wilder finally realized that the part should be played by someone who could not only be a cynic, but a nice guy as well. 

Fred MacMurray was accustomed to playing "happy-go-lucky good guys" in light comedies, and when Wilder first approached him about the Neff role, MacMurray said, "Youre making the mistake of your life!" Playing a serious role required acting, he said, "and I cant do it." Phillips, Some Like, p. 61  But Wilder pestered him about it every single day – at home, in the studio commissary, in his dressing room, on the sidewalk – until he simply wore the actor down. MacMurray felt safe about his acquiescence since Paramount, who had him under contract and had carefully crafted his good guy image, would never let him play a "wrong" role. Sikov, p. 202  His trust, however, was misplaced: his contract was up for renewal at the time, and ever since his friend and co-star, Carole Lombard, had shrewdly and successfully taught him how to play hardball with the studio bosses, he wasnt the pliable pushover of old. Paramount executives decided to let him play the unsavory role to teach him a lesson. A lesson was indeed taught, but not the one Paramount had in mind. Sikov, p. 203  MacMurray made a great heel and his performance demonstrated new breadths of his acting talent. "I never dreamed it would be the best picture I ever made," he said. Zolotow, p. 118 
 Little Caesar in 1930, this role represented a step downward to the third lead. Robinson would later admit, "At my age, it was time to begin thinking of character roles, to slide into middle and old age with the same grace as that marvelous actor Lewis Stone". It also helped, as he freely admitted, that he would draw the same salary as the two leads, for fewer shooting days.  The notable Broadway actor Tom Powers was invited to Hollywood for the role of Mr. Dietrichson. It was Powers first film role since 1917 and his start to a "second film career" with many supporting roles until his death in 1955. 

===Filming===
 
The original ending to the Cain novella called for the characters to commit double suicide. Suicide, however, was strictly forbidden at the time by the Hays Production Code as a way to resolve a plot, so Wilder wrote and filmed a different ending in which Neff goes to the gas chamber while Keyes watches. This scene was shot before the scenes that eventually became the films familiar ending, and once that final intimate exchange between Neff and Keyes revealed its power to Wilder, he began to wonder if the gas chamber ending was needed at all. "You couldnt have a more meaningful scene between two men", Wilder said. Lally, p. 137  As he would later recount, "The story was between the two guys. I knew it, even though I had already filmed the gas chamber scene... So we just took out the scene in the gas chamber," Phillips, Creatures, p. 180  despite its $150,000 cost to the studio.  Removal of the scene, over Chandlers objection,  also removed the Hays Offices single biggest remaining objection to the picture, since they regarded it as "unduly gruesome" and predicted that it would never be approved by local and regional censor boards. Lally, p. 138  The footage and sound elements are lost, but production stills of the scene still exist.
 rushes were so dark that you couldnt see anything. He went to the limits of what could be done." Sikov, p. 206  They would contrast the bright sunny Southern California exteriors, shot on location, with dark, gloomy, rotten interiors shot on soundstages to give the audience a sense of what lurks just beneath the facade – and just who is capable of murder.  The contrast was heightened, in Wilders words, by "dirtying up" the sets. Once the set was ready for filming, Wilder would go around and overturn a few ashtrays to give the house an appropriately grubby look.  Wilder and Seitz also blew aluminum particles into the air so that, as they floated down, they looked just like dust. Phillips, Some Like, p. 63 

 Another technique Seitz used was "venetian blind" lighting which almost gives the illusion of prison bars trapping the characters. Barbara Stanwyck later reflected, "...and for an actress, let me tell you the way those sets were lit, the house, Walter’s apartment, those dark shadows, those slices of harsh light at strange angles – all that helped my performance. The way Billy staged it and John Seitz lit it, it was all one sensational mood." Muller, p. 58 

For Neffs office at Pacific All Risk, Wilder and set designer  . Sikov, p. 207  

 

Wilder also decked Stanwyck out in the blonde wig "to complement her anklet...and to make her look as sleazy as possible."  This wig has been cited by some as being the pictures biggest flaw claiming that it looks too “fake”.   According to Wilder, this was exactly what he was going for when he chose the wig wanting to project, "the phoniness of the girl – Bad taste, phony wig," with cheap perfume to match. Phillips, Some Like, p. 62  Unconvinced, Paramount production head Buddy DeSylva was overheard to say, "We hired Barbara Stanwyck, and here we get George Washington." 

The production was not without its lucky accidents: The company had just finished shooting the final segment of the sequence where Phyllis and Walter make their getaway after dumping their victims body on the tracks. The crew was breaking for lunch before striking the set. In the script, the pair get in their car and simply drive away. But as Wilder got into his own car to leave, it wouldnt start. Inspired, he ran back and ordered the crew back. Wilder reshot the scene, only this time as Phyllis starts the car, the motor stalls and wont turn over. She tries several more times, but the car wont start and the two look at each other in growing panic. Walter desperately reaches over, turns the key and guns the motor, finally starting the car. Only then do they speed away from the crime scene. The result was one of the most suspenseful scenes in the film, but was not in the original script. Phillips, Creatures, pp. 175-176  MacMurray was surprised when he first saw it onscreen: "...When I... turned the key I remember I was doing it fast and Billy kept saying, Make it longer, make it longer, and finally I yelled, For Chrissake Billy, its not going to hold that long, and he said, Make it longer, and he was right." Zolotow, p. 116 

Wilder managed to bring the whole production in under budget at $927,262 despite $370,000 in salaries for just four people ($100,000 each for MacMurray, Stanwyck, and Robinson, and $70,000 – $44,000 for writing and $26,000 for directing – for himself). Sikov, p. 211 

===Music===
{{Listen filename     = The_Conspiracy.ogg title        = The Conspiracy description  = The nervous running figure for tremolo strings sets off each of Neffs flashbacks to represent the conspirators activities.
}}
The score to Double Indemnity was composed by Miklós Rózsa, whose work on Wilders previous film, Five Graves to Cairo, had been his first real Hollywood engagement for a major studio. Wilder had praised that work and promised to use Rózsa on his next film. Wilder had the idea of using a restless string fugue (like the opening to  , was of a different mind; he and Wilder had previously clashed over some post-production cuts he had made to the Five Graves score which created problems with the musics continuity and logic. Now the two were coming to loggerheads again. Sikov, pp. 210–211  Miklós Rózsa|Rózsa, Miklós (1982). Double Life: The Autobiography of Miklós Rózsa. New York: Hippocrene Books. ISBN 978-0-88254-688-9. p. 119 
 Madame Curie to learn how to write a proper film score. When Rózsa pointed out that Double Indemnity was a love story, Lipstone suggested his music was more appropriate to The Battle of Russia.  Rózsa, pp. 121  Lipstone was convinced that as soon as the studios Artistic Director, Buddy DeSylva, heard the music he would throw it out. At a screening soon after, DeSylva called him over: expecting heads to roll, Lipstone eagerly huddled with his chief – only to have DeSylva praise the music, saying it was exactly the dissonant, hard-hitting score the film needed. The bosss only criticism: there was not enough of it. By this time Lipstone had an arm around DeSylva, asking unctuously, "I always find you the right guy for the job, Buddy, dont I?" Rózsa, pp. 122 

The score would go on to be nominated for an Academy Award, and the success brought Rózsa offers to do as many films as he had time for. 

===Locations===
  Spanish Colonial Beachwood Canyon neighborhood of Los Angeles. The production team copied the interior of the house, including the spiral staircase, almost exactly on a soundstage at Paramount. 
 Mission Revival Style Southern Pacific Railroad Depot in Glendale, California built in 1923. The station can now be seen as part of the Glendale Transportation Center and was added to the National Register of Historic Places on May 2, 1997. 

Other locations around Los Angeles used in the film were  an apartment building at 1825 N. Kingsley Drive in Hollywood where Walter Neff lived and the building on the southwest corner of Hollywood Blvd. and Western. That building still stands, but the Newman Drug Store originally on the ground floor is no longer there. 

==On screens: 1944 and since==
 

===Critical reception===
Double Indemnity opened on September 6, 1944 and was an immediate hit with audiences – despite a campaign by singer Kate Smith imploring the public to stay away on moral grounds. Sikov, p. 213  As James M. Cain recalled, “…there was a little trouble caused by this fat girl, Kate Smith, who carried on a propaganda asking people to stay away from the picture. Her advertisement probably put a million dollars on its gross.” McGilligan, p. 128 

Reviews from the critics were largely positive, though the content of the story made some uncomfortable. While some reviewers found the story implausible and disturbing, others praised it as an original thriller. In his mixed review of the film in The New York Times, film critic Bosley Crowther called the picture "...Steadily diverting, despite its monotonous pace and length." He complained that the two lead characters "...lack the attractiveness to render their fate of emotional consequence," but also felt the movie possessed a "...realism reminiscent of the bite of past French films." Lally, p. 139 

Howard Barnes at the New York Herald Tribune was much more enthusiastic, calling Double Indemnity "...one of the most vital and arresting films of the year," and praising Wilders "...magnificent direction and a whale of a script." The trade paper Variety (magazine)|Variety, meanwhile, said the film "...sets a new standard for screen treatment in its category." 

Influential radio host and Hearst paper columnist Louella Parsons would go even further, saying, "Double Indemnity is the finest picture of its kind ever made, and I make that flat statement without any fear of getting indigestion later from eating my words." Hoopes, p. 347 
 The Human The Maltese Falcon, and Citizen Kane as Hollywood trailblazers, while Alfred Hitchcock himself wrote Wilder that "Since Double Indemnity, the two most important words in motion pictures are Billy and Wilder". 

The films critical reputation has only grown over the years. In 1977, notably terse critic-historian   praised director Wilder and cinematographer Seitz. He wrote, "The photography by John F. Seitz helped develop the noir style of sharp-edged shadows and shots, strange angles and lonely Edward Hopper settings." Roger Ebert  ,
Chicago Sun-Times, December 20, 1998. Last accessed: December 29, 2007 

===Film noir===
Double Indemnity is an important (and some say the first) example of a   casting strong shadows that both conceal and project characters’ feelings.”  Double Indemnity includes all of these traits.
 Sunset Blvd. flashback narrated by their protagonists.  Sklar explains, “ he unusual juxtaposition of temporalities gives the spectator a premonition of what will occur/has occurred in the flashback story.  … Besides Double Indemnity and Detour, voice-over is a key aspect of Mildred Pierce, Gilda, The Lady from Shanghai, and Out of the Past … as well as many others.”  Wendy Lesser notes that the narrator of Sunset Blvd. is dead before he begins narrating, but in Double Indemnity, "the voice-over has a different meaning.  It is not the voice of a dead man … it is … the voice of an already doomed man.” 

===Academy Award nominations===
At the 17th Academy Awards on March 15, 1945, Double Indemnity was nominated for seven Oscars, but did not win any.   
{|class="wikitable"
|-
! Award
! Category
! Nominee
! Result
|- 17th Academy Awards Academy Award Best Motion Picture Paramount Pictures Lost to Going My Way – Leo McCarey Producer
|- Academy Award Best Director Billy Wilder Lost to Leo McCarey for Going My Way
|- Academy Award Best Actress Barbara Stanwyck Lost to Ingrid Bergman for Gaslight (1944 film)|Gaslight
|- Academy Award Best Writing, Screenplay Billy Wilder and Raymond Chandler Lost to Frank Butler and Frank Cavett for Going My Way
|- Academy Award Best Cinematography – Black and White John F. Seitz Lost to Joseph LaShelle for Laura (1944 film)|Laura
|- Academy Award Best Music, Scoring of a Dramatic or Comedy Picture
|Miklós Rózsa Lost to Max Steiner for Since You Went Away
|- Academy Award Best Sound, Recording Loren Ryder Lost to Edmund H. Hansen for Wilson (film)|Wilson
|}

Wilder went to the awards ceremony expecting to win even though the studio had been backing their other big hit of the year, Leo McCareys Going My Way and studio employees were expected to vote for the studio favorite. As the awards show wore on and Double Indemnity lost in category after category, it became evident that it was going to be a Going My Way sweep. McCarey beamed as his picture won award after award and when he was named Best Director, Wilder could no longer take it. When McCarey got up to make his way to the stage to accept the award for best picture, Wilder, sitting on the aisle, stuck out his foot and tripped him. "Mr. McCarey...stumbled perceptibly," he gleefully recalled. Lally, p. 140  After the ceremony while he and his wife Judith were waiting for his limousine to arrive, he yelled out so loudly that everybody could hear him, "What the hell does the Academy Award mean, for Gods sake? After all – Luise Rainer won it two times. Luise Rainer!" Zolotow, p. 123 

===Others awards===
American Film Institute recognition
* 1998 AFIs 100 Years…100 Movies #38
* 2001 AFIs 100 Years…100 Thrills #24
* 2002 AFIs 100 Years…100 Passions #84
* 2003 AFIs 100 Years…100 Heroes and Villains:
** Phyllis Dietrichson, villain #8
* 2007 AFIs 100 Years…100 Movies (10th Anniversary Edition) #29

===Adaptations===
  Robert Taylor.  It was also adapted to the October 15, 1948 broadcast of the Ford Theatre with Burt Lancaster and Joan Bennett  and the October 30, 1950 broadcast of Lux Radio Theater with MacMurray and Stanwyck. 

Other films inspired by the Snyder-Gray murder include   in 1973 starring Richard Crenna (who also starred in Body Heat), Lee J. Cobb, and Samantha Eggar;  it is included on a bonus disc in the American DVD release of the original film. The The Postman Always Rings Twice (1981 film)|Postman Rings remake was a 1981 theatrical release. An Indian film, Jism (2003 film)|Jism (2003), was also inspired by the film.

Double Indemnity is one of the films parodied in the 1993 film Fatal Instinct; the heros wife conspires to have him shot on a moving train and fall into a lake so that she can collect on his insurance, which has a "triple indemnity" rider. Carol Burnett parodied the film as "Double Calamity" on her TV show.

===Imitators, rivals, reflections=== studios of Ann Savage and Hugh Beaumont. Paramount quickly slapped an injunction on the cut-rate potboiler that remains in force to this day. PRC eventually edited its film down to 67 minutes, re-titled it Apology for Murder, and sold it to television in the early 50s as part of a syndicated half-hour mystery show. 
 Screen Writers Red Scare in Hollywood and Guild members rejected the socialist notion and ran from the attempt. Muller, p. 59 

It was not uncommon at the time for studios to take out ads in trade journals promoting the virtues of their own films.  , thus comparing D. W. Griffith|D. W. Griffiths artistic 1919 classic with his own sordid story of iniquitous murder. Selznick was not amused and threatened to stop advertising in any of the trades if they continued to run Wilders ads. 
 Witness for the Prosecution. 

Wilder was not only proud of his film, he was plainly fond of it as well: "I never heard that expression, film noir, when I made Double Indemnity... I just made pictures I would have liked to see. When I was lucky, it coincided with the taste of the audience. With Double Indemnity, I was lucky." 

== See also ==
 

==References==
 

==External links==
 
 
*  
*  
*  
*  
*   film script at the Internet Movie Script Database
*   literature
*   story analysis
*  

; Streaming audio
*   on Screen Guild Theater: March 5, 1945
*   on Lux Radio Theater: October 30, 1950

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 