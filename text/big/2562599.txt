The Bank (1915 film)
{{Infobox film
| name = The Bank
| image = The Bank (1915 film).jpg
| caption = Theatrical poster to The Bank
| director = Charlie Chaplin
| producer = Jess Robbins
| writer = Charlie Chaplin
| starring = Charles Chaplin Charles Inslee Carl Stockdale Edna Purviance Leo White
| music = Robert Israel (Kino video release)
| cinematography = Harry Ensign
| editing = Charlie Chaplin
| distributor = Essanay Studios General Film Company
| released =  
| runtime = 33 minutes English (original intertitles)
| country = United States
| budget =
}}
  Essanay Films.

Released in 1915 in film|1915, it is a departure from the tramp character, as Charlie Chaplin plays a janitor at a bank. Edna Purviance plays the secretary on whom Charlie has a crush and dreams that she has fallen in love with him. Filmed at the Majestic Studio in Los Angeles. Silent.
There doesnt appear to be any evidence that this film was received any differently from the bulk of Chaplins early work, but today this film is often considered one of his most fascinating efforts. 
 

==Cast==
* Charles Chaplin - Charlie, a Janitor
* Edna Purviance - Edna, a Secretary
* Carl Stockdale - Charles, a Cashier
* Charles Inslee - Charles, a Cashier
* Leo White - Clerk Billy Armstrong - Another Janitor
* Fred Goodwins - Bald Cashier/Bank Robber with Derby John Rand - Bank Robber and salesman
* Lloyd Bacon - Bank Robber
* Frank J. Coleman - Bank Robber
* Paddy McGuire  - Cashier in White Coat
* Wesley Ruggles - Bank Customer
* Carrie Clark Ward - Bank Customer
* Lawrence A. Bowes - Bond Salesman

==External links==
 
* 

 

 
 
 
 
 
 
 
 
 


 