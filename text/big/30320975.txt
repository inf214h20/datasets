Skinning (film)
{{Infobox film
| name           = Skinning
| image          =
| caption        = 
| director       = Stevan Filipović
| producer       = Branislav Jević
| writer         = Stevan Filipović Staša Koprivica Dimitrije Vojnov Nataša Vranješ
| starring       = Nikola Rakočević Viktor Savić Bojana Novaković
| cinematography = Mihajlo Savić
| editing        = Stevan Filipović Nataša Vranješ
| studio         = 
| distributor    = Hypnopolis
| released       =  
| runtime        = 
| country        = Serbia
| language       = Serbian
| budget         = €500,000
| gross          = €171,052
}}
Skinning ( ) is a 2010    Novosti AD|Novosti:   (9 October 2010) 

== Plot == Kosovo is 17 February 2008 unilateral declaration of independence by ethnic Albanians inhabiting Serbias southern province of Kosovo. This news footage is intercut with shots of the movies main protagonist, skinhead Novica, protesting by leading chants and lighting flares in front of the embassy while hes also providing narration.

The story than backs up as Novica (Nikola Rakočević), a timid and geeky high school student with frumpy clothing and disheveled thick hair, is introduced. He lives in Belgrade where his life revolves around attending advanced math classes for gifted kids and taking part in math competitions. Due to his awkwardness around people, his social life is nothing to speak of - his only friends are his stoner cousin Mirko (Miloš Tanasković) as well as an even nerdier math colleague Stanislav (Vladimir Tešović). 

At a math competition as the students are working away solving problems, Novica is pressured into cheating by Relja, a confident skinhead full of bravado who is also gifted at math, but doesnt quite possess Novicas math problem-solving skills. Relja is seated relatively close to Novica at the competition and is stuck on a question that he cant solve. Obviously flattered by the attention from a kid placed much higher on the high school social scale, Novica passes Relja the solution written on a piece of paper. The competition supervisor notices something untoward occurred and tells Novica to report the person hed helped without penalty to himself. Wanting to cover for Relja, Novica purposely wrongly points out nerdy Stanislav as the recipient of his help, leading to the supervisor throwing both Novica and Stanislav out thus breaking her promise to spare Novica. 
 Faculty of Philosophy. Novica brings his cousin Mirko along who, dissatisfied with the right-wing, anti-Semitic overtones in professors lecture, leaves early on. Novica, on the other hand, remains and is very much receptive to what hes hearing. 
 diesel boys" - small group of fans on his payroll. Since he doesnt own the club, Puftas motivation for getting involved with football is taking advantage of his vaguely defined "financier role" in order to siphon off the funds from player transfers abroad. As he conducts his business in ruthless manner, his biggest rivals are similarly run FK Kosančić, the club that also has a financier - a mafia widow. At this particular match FK Radnik is playing FK Novi Pazar and theres no shortage of hateful chants and violence as Novica and Relja, among others, get taken to the police station where theyre questioned by corrupt inspector Milutin (Nikola Kojo) and young idealistic policewoman Lidija (Nataša Šolak) who recently joined the force. She immediately notices that Novica doesnt really fit the usual skinhead profile and encourages him to get out, even giving him her personal contact in case he ever needs help.
 racist skinheads. After having passionate sex in a room filled with nazi-symbols, Mina has Novica sitting naked while she(also naked) cuts off his hair. 

Hanging out one night, under the influence of alcohol, the group is walking along the river quay when they run into one of the diesel boys whos all by himself. In addition to belonging to their hated rival group, hes also a Gypsy, which makes him even more of a target in their eyes. The skinheads pin him down to the ground, giving him a beating using fists and boots. Novica is especially up for confrontation as he charges the incapacitated Gypsy with a concrete block, smashing his head with it thus killing him instantly. Shocked by the gruesome crime Novica just committed, other skinheads panic a bit before regrouping and deciding to dump the body into the river and sink with it with rocks.

== Cast ==
* Nikola Rakočević as Novica
* Viktor Savić as Relja
* Bojana Novakovic as Mina
* Nikola Kojo as Milutin
* Nataša Tapušković as Lidija
* Predrag Ejdus as Professor Hadži–Tankosić
* Milan Mihailović as Novicas father

Actor    Popular Serbian journalist and television presenter Ivon Jafali also appeared in the film as the journalist.   at the Internet Movie Database 

== Production ==
The pre–production of Skinning began in 2007.  The director Stevan Filipović got some funds from the SEE Cinema Network in Thessaloniki. Beogradska kulturna mreža:   (29 September 2010)  The film was shot in stages, as it required great transformations of the actors, which included training in the gym and changing haircuts. 

==Motifs==
Though on occasion applying a thin disguise when it comes to actual names, the movie references and alludes to various individuals, organizations, and events from the Serbian public life during the 2000s. 

The fictitious FK Radnik from Belgrade is obviously FK Rad, the club that has a small but violent core of right-wing fans known for clashes with the fans of FK Novi Pazar. Radniks fictitious cross-town rival FK Kosančić run by an unnamed attractive mafia-widow obviously refers to FK Obilić (instead of Miloš Obilić, one famous medieval Serbian knight from the Battle of Kosovo, the club is named after another one, Ivan Kosančić), while its owner/financier bares an uncanny physical resemblance to Obilićs one-time president Ceca Ražnatović, the widow of slain gangster Arkan.

In fact, many details from the football subplot in the movie are lifted straight from B92 television investigative programme Insajders 2008 exposé (journalism)|exposé "Pravila igre"  and the 2009 one called "(Ne)moć države"  both of which uncovered and alleged high levels of organized criminality and systemic corruption within the Serbian league football.

== Reception ==
The film grossed    As of 29 November 2010, it grossed €171,052.  It ended its theatrical run in Serbia in late February 2011, grossing the total of US$288,512. 

One reviewer wrote: "Skinning is an obligatory, which must be seen by almost all parents who raise a teenager in their home... Especially those whose talents are unimaginable. And especially the parents who doubt to have a genius, because the line between genius and madness is thin. Skinning divides nationalism and false patriotism from violence and anarchy."  Press (newspaper)|Press.  Jovanović, Aleksandar, Press:   (2 November 2010) 

== References ==
 

== External links ==
*  

 
 
 
 
 