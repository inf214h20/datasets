Esther (film)

{{Infobox film
| name           = Esther 
| image          =
| caption        =
| director       = Maurice Elvey
| producer       = 
| writer         = Maurice Elvey Fred Groves   Charles Rock
| music          = 
| cinematography = 
| editing        = 
| studio         = London Films
| distributor    = Jury Films
| released       = January 1916
| runtime        = 
| country        = United Kingdom 
| awards         =
| language       = Silent   English intertitles
| budget         =
| preceded_by    =
| followed_by    =
}} silent historical Fred Groves and Charles Rock.  The film portrays the biblical story of Esther.

==Cast==
*  Elisabeth Risdon as Esther  Fred Groves as Haman 
* Charles Rock as Mordecai 
* Ruth Mackay as Vashti 
* Franklin Dyall
* Guy Newall
* Beatrix Templeton

==References==
 

==Bibliography==
* Murphy, Robert. Directors in British and Irish Cinema: A Reference Companion. British Film Institute, 2006.

==External links==
*  

 

 
 
 
 
 
 
 
 
 

 