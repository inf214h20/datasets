Blood Feud (1978 film)
 
{{Infobox film
| name           = Blood Feud
| caption        = Film poster
| image	         = Blood Feud FilmPoster.jpeg
| director       = Lina Wertmüller
| producer       = Arrigo Colombo
| writer         = Lina Wertmüller
| starring       = Sophia Loren Marcello Mastroianni
| music          =
| cinematography = Tonino Delli Colli
| editing        = Franco Fraticelli
| distributor    =
| released       =  
| runtime        = 124 minutes
| country        = Italy
| language       = Italian
| budget         =
}}

Blood Feud ( , and also known as Revenge) is a 1978 Italian thriller film directed by Lina Wertmüller.   

==Cast==
* Sophia Loren as Titina Paterno
* Marcello Mastroianni as Rosario Maria Spallone
* Giancarlo Giannini as Nicola Sanmichele detto Nick
* Turi Ferro as Vito Acicatena
* Mario Scarpetta as Tonino
* Antonella Murgia as Ragazza incinta
* Lucio Amelio as Dr. Crisafulli
* Maria Carrara as Donna Santa
* Isa Danieli as Una emigrante
* Guido Cerniglia as Segretario Communale
* Vittorio Baratti as Il farmacista
* Oreste Radi as Il maestro
* Tomas Arana as Fascist (uncredited)
* Tito Palma as Tutino (uncredited)

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 


 
 