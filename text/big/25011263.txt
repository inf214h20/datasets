The Hook (1963 film)
{{Infobox film
| name           = The Hook
| image          = TheHook-1963-poster.jpg
| image_size     =
| alt            = 
| caption        = Theatrical release poster with artwork by Reynold Brown
| director       = George Seaton
| writer         = Henry Denker  Vahé Katcha (novel)
| narrator       =  Nick Adams Robert Walker Jr. Nehemiah Persoff
| music          = Larry Adler
| cinematography = Joseph Ruttenberg
| editing        = Robert James Kern
| distributor    = Metro-Goldwyn-Mayer
| released       = July 24, 1963
| runtime        = 98 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =
}}
The Hook is a 1963 Korean War war film directed by George Seaton based on the 1957 novel LHamecon by Vahé Katcha.  The films title comes from the translation of the title of the original novel rather than the Battle of the Hook. The film was shot off Santa Catalina Island, California.

==Plot==
During the Korean War, a group of American soldiers aboard a ship capture the enemy pilot of a plane they have shot down.  They are ordered by headquarters to execute the prisoner.

==Cast==
* Kirk Douglas as Sgt. Brisco Nick Adams as Pvt. Hackett Robert Walker as Pvt. Dennison
* Nehemiah Persoff as Capt. Van Ryn
* William Challee as Schmidt

==References==
 

==External links==
*  

 

 
 
 
 
 
 

 