Ronan's Escape
 
 
 
{{Infobox film
|  name     = Ronans Escape
|  image          = Ronans Escape Movie Poster.jpg
|  caption        = Ronans Escape Movie Poster
|  writer         = A.J. Carter
|  starring       = David Lazarus Ashleigh Zinko Ryan Cammiade Matthew Hennighan Louie Van Praag Kirstie Francis
|  director       = A.J. Carter
|  released       = 2010
|  runtime        = 16 min
|  country        = Australia
|  language       = English 
}}
Ronans Escape is a short film directed by Australian born film writer/director A.J. Carter.  The film, set in the rural wheat belt of Western Australia, provides a candid insight into the life of Ronan, a 14 year old boy who has been bullied at school his whole life and decides to make an escape. This accurate and controversial portrayal       of life for someone who has fallen victim to bullying and the repercussions which exist, is told in a unique, exposition intensive format with very little use of dialogue. Ronans Escape presents arbitrary scenes rather than traditional storytelling which provokes audiences into discussion and interpretation of the scenes on a more personal level while reflecting on their own experiences. 

==Plot==
At school during break Ryan (Ryan Cammiande), a bully kicks a ball at a young boy, Ronan (David Lazarus), sitting alone by himself causing him to fall down and everyone laughs at him. While restraining himself afterwards he finds a wounded bird and puts it in his bag. Later during gym class the teacher (Louie Van Praag) organizes a baton race. While preparing, another bully, named Colin (Matthew Henningham), sneakily unties the laces on Ronans shoes so he trips just as he nears the finish line causing him to get jeered by his classmates including Sally (Ashleigh Zinko), a girl who actually trusted that Ronan could win for her team but has lost faith in him after Ryan confessed that he told her he was a loser. On the bus ride home afterwards Ronans bag is stolen by Justin (James Tayler), who tosses it about and Ronan tries to retrieve before he hurts the bird in it. Later when he goes off the bus he goes up to a tree near his home with a tire swing on it instead of going home. Eventually he finds the bird in his bag that is now killed due to the bullies tossing it around on the bus. Ronan buries it and then finds a piece of paper that says "Loser" on it, which is what everyone has been calling Ronan, and he decides that him and the bird were one. So he decides to have the same life as the bird did. Later on his mother (Kristie Chorley) has been talking to another mother but then starts searching for her son after being made aware that Ronan was not on a bus that passed by her house that looked like Ronans school bus. She gets worried as she looks out into the trees where Ronan is. While the sun is setting it is shown that Ronan has hung himself off the tree with the rope from the tire swing finally unable to take any more bullying. Ronans life is now forever peaceful.

==Cast==
*David Lazarus as Ronan, a boy who is bullied by everyone at school leaving him friendless and no choice but to commit suicide.
*Ashleigh Zinko as Sally, a girl who is actually friendly to Ronan at the start but later joins with everyone else.
*Ryan Cammiande as Ryan, a teenager at Ronans school. He is the first person who is seen bullying Ronan.
*Louie Van Praag as Gymn Teacher, Ronans school teacher. He is unaware of Ronans constant bully problem.
*Matthew Hennighan as Colin, a bully about Ronans age who unties Ronans shoe lace making him trip.
*Kristie Chorley as Ronans mother, who is unaware of Ronans bully problem and eventual suicide.
*James Tayler as Justin, a bully on Ronans bus who steals his bag killing the bird that Ronan put in it earlier on.
*Mark Kont as Bus Driver, Ronans school bus driver. He does not stop Justin from bullying Ronan but when Ronan gets off he wishes that he should have.

==Release== Hoyts Cinemas (Southlands, Western Australia).  The film has since been screened around the world in public film festivals and has also been used in schools as an educational tool, primarily in the United States of America, against school bullying.  

==Critical Acclaim==
The continued international success of Ronans Escape has seen the film officially selected into 19 international festivals around the world and has earned a total of 13 award nominations and 10 wins.  Awards include Best Short Film at the prestigious 16th Annual Sedona International Film Festival, 2010 Directors Choice Awards, presented to Carter by filmmaker Michael Moore,  Best International Film at the 6th Annual HollyShorts Film Festival in Hollywood,  and the World Cinema / Van Gogh award for Best Director at the 2011 Amsterdam International Film Festival.  

==Awards and Nominations==

{| class="wikitable sortable plainrowheaders"
|+ Ronans Escape
|-
! scope="col" | Year
! scope="col" | Festival
! scope="col" | Award Nomination
! scope="col" class="unsortable" | Result
|-
| style="text-align:center;"| 2010 6th Annual HollyShorts International Film Festival
| Best International Film
| WON
|-
| style="text-align:center;"| 2010 17th Annual Sedona International Film Festival
| Best Short Film
| WON
|-
| style="text-align:center;"| 2011
|  
| Best Director (Van Gogh World Directing Award)
| WON
|-
| style="text-align:center;"| 2011
|  
| Best upcoming Screenwriter
| WON
|-
| style="text-align:center;"| 2011
|  
| Best Short Narrative Film
| WON
|- 
| style="text-align:center;"| 2011 Australian Cinematographers Society Awards
| Best Cinematography- Short Fictional Drama
| WON
|-
| style="text-align:center;"| 2011
| Los Angeles International Film Festival
| Best Production Design
| WON
|-
| style="text-align:center;"| 2011
| Los Angeles International Film Festival
| Best Film Honorable Mention
| WON
|-
| style="text-align:center;"| 2011
| Peace on Earth Film Festival
| Best Short Film - Student Choice award
| WON
|-
| style="text-align:center;"| 2012
| Focus Film Festival
| Best Film - Best of Festival award
| WON
|}

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 