Nightmare Castle
 
 
{{Infobox film
| name           = Nightmare Castle
| image          = Nightmare-Castle-poster.jpg
| image_size      =
| caption        = Italian theatrical poster
| director       = Mario Caiano (credited as Allen Grünewald)
| producer       = Carlo Caiano
| writer         = Mario Caiano Fabio De Agostini Paul Muller Helga Liné
| music          = Ennio Morricone
| cinematography = Enzo Barboni
| editing        = Renato Cinquini
| distributor    = Allied Artists Pictures Corporation
| released       = 16 July 1965
| runtime        = 101 min, 90 min (English Edited version)
| country        = Italy
| awards         =
| language       = Italian
| budget         =
}} 1965 Cinema Italian Gothic gothic horror film directed by Mario Caiano credited as Allen Grünewald. The film stars Barbara Steele in dual lead roles, and its music was composed by Ennio Morricone.
 dubbed into English in an edited release in 1968. It was released to VHS and DVD in the 1990s with a longer cut of the film in Italian with English subtitles. It was later released to the public domain and became a cult film.   

==Plot==
 
An evil count tortures and murders his unfaithful wife and her lover, then removes their hearts from their bodies. Discovering that his wife has drawn up a new will giving her fortune to her institutionalized sister, the count marries his sister-in-law. The new wife experiences nightmares and hauntings. The ghosts of the slain return to exact their bloody revenge, until their hearts are destroyed.

==Cast==
*Barbara Steele ...  Muriel Arrowsmith / Jenny Arrowsmith Paul Muller ...  Dr. Stephen Arrowsmith (as Paul Miller)
*Helga Liné ...  Solange
*Laurence Clift ...  Dr. Dereck Joyce (as Lawrence Clift)
*Giuseppe Addobbati ...  Jonathan (as John McDouglas)
*Rik Battaglia ...  David
==Production==
 
Director Mario Caiano had originally planned to film the gory scenes highlighted in red color but due to budget restraints the idea was dropped.   
==Release==
 
Nightmare Castle made its debut on DVD by Madacy on March 20, 2001 as a double feature with Track of the Vampire as a part of its Killer Creature features. The film has been released multiple times on DVD over the years  to date the film has not been released on Blu-ray.

==Reception==
 
Critical reception for the film has been mixed to positive.

TV Guide awarded the film a score of 2.5 / 4 stating, "While certainly not up to the level of Steeles work with the masterful Mario Bava, this is a worthwhile effort. Nightmare Castle finds its greatest success in showing the beautiful horror icon in as many extreme situations and personas as possible". 
Tom Becker from DVD Verdict gave the film a positive review stating, "Cheerfully perverse and agreeably grotesque, Nightmare Castle is a good-time relic from the days when horror was more fun than it was stomach churning". 
Paul Mavis  from DVD Talk gave the film a mostly positive review stating, " Nightmare Castle may borrow elements wholesale from better novels and films, but it still delivers the Gothic horror goods, with Barbara Steele always endlessly interesting (even if shes just prowling around darkened black-and-white sets), and director Mario Caiano alternating from full-blown sadistic horror scenes, to languid, dreamy, sullen nightmares". 
Kevin Nickelson from Classic Horror.com gave the film a mostly positive review stating, "As Gothic horror films go, Nightmare Castle is a very good-looking film". 
Brett H. from Oh The Horror! gave the film a mixed review summarizing, "Nightmare Castle is a bit of a bore, but the climax shows off Steele’s creepy demeanor to terrific demonic (although quite silly) proportions and features ample (for the time) amounts of blood and grue". 
Horror Digital.com awarded the film a C, stating "Nightmare Castle is for the most part passable Gothic horror, with violent beginnings and endings and a whole lot of talking in between". 

The film has attained a cult following over the years and is now considered a classic in Gothic horror.   

==Biography==
* 

==See also==
* List of films in the public domain

==References==
 

==External links==
*  
*  
*  
*   at DB Cult Cinema
 

 
 
 
 
 
 
 
 
 
 

 
 