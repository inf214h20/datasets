Kalle Stropp, Grodan Boll och deras vänner
{{multiple issues|
 
 
}}

{{Infobox film
| name = Kalle Stropp, Grodan Boll och deras vänner
| image = 
| caption =
| producer =
| writer = Thomas Funck
| starring = Thomas Funck
| director = Jan Gissberg
| distributor = Europafilm
| released =  
| runtime = 96 minutes
| country = Sweden Swedish
| music =
| editing =
| budget =
| box office =
}}
Kalle Stropp, Grodan Boll och deras vänner is a 1956 Swedish animated feature film directed by after an original script by Thomas Funck, using Funcks already well-established characters. It follows a shorter film made by the same team in 1987, Kalle Stropp och Grodan Boll räddar Hönan. This is the first time since before 1987 where a Kalle Stropp production features voice acting by others than only Funck himself, only with the exception of children that had participated in other productions as well.
== Plot ==
 
== Swedish Voices ==
* Erik Sjögren-Charlie Strapp
* Thor Zackrisson-Froggy Ball
* Sten Ardenstam-Sheet-Niklas
* Thore Segelström-The Fox
* Tyyne Talvo Cramér-The Parrot
* John Starck-The Baker
* Ewert Ellman-Chimney sweep
* Stig Grybe-Chimney sweep
* Elsie Ståhlberg-The Chicken
* Elsie Ståhlberg-The Old Woman Remember
* Rutger Nygren-The Old Man Oblivious
* Sten Mattsson-The Captain
* Thomas Funck-Himself And All The Cast

== External links ==
* 

 
 
 
 
 
 
 


 
 