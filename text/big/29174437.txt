Paper (film)
{{Infobox film
| name = Paper
| image = PaperFilmPoster.jpg
| alt =
| caption = Promotional Poster
| director = Sinan Çetin
| producer = Sinan Çetin
| writer = Sinan Çetin
| starring = Öner Erkan   Asuman Dabak   Ayşen Gruda   Ahmet Mekin   Zeynep Beşerler   Uğur Bilgin
| music = Fırat Yükselir
| cinematography =
| editing =
| studio =
| distributor =
| released =  
| runtime =
| country = Turkey
| language = Turkish
| budget =
| gross = US$103,964
}}
Paper ( ) is a 2010 Turkish comedy-drama film directed by Sinan Çetin, which tells the story of a young director trying to make his first film. The film, which went on nationwide general release across Turkey on  , was premiered in competition at the 47th International Antalya Golden Orange Film Festival (October 9–14, 2010).      

== Plot ==
Emrah is a dreamer who hopes to be a great director, trying to shoot his first feature film. His father Mehdi, a retired customs enforcement officer, believes that Emrah is going to become a pharmacist. Emrah manages to cobble together funding from producers with the help of his friends and his mother Şahane, but is held up by the bureaucracy. The main obstacle between him and his dreams is an endorsement letter he needs from Müzeyyen, the head of the censorship board. But this proves more difficult than he expected...

Standing up to authority in pursuit of his ideals, this young man finds himself entangled in a vehement struggle against this petty official who blindly enforces a senseless law.

==Release==

=== General release ===
The film opened on nationwide general release in 86 screens across Turkey on   at number 8 in the national box office with a first weekend gross of $54,815.   

=== Festival screenings ===
* 47th Antalya "Golden Orange" International Film Festival (October 9–14, 2010)

==Reception==

===Box Office===
The film has been in the Turkish national box office charts for two weeks and has made a total gross of US$103,964. 

==See also== 2011

==References==
 

==External links==
*   (in Turkish)

 
 
 
 
 


 