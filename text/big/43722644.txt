Aaaah
{{Infobox film
| name           = Aaaah
| image          = 
| caption        = 
| director       = Hari Shankar Hareesh Narayan
| producer       = V. Loganathan V. Janarthanan Srinivas Loganathan
| writer         =
| starring       = Gokulnath Meghna Bobby Simha Bala Saravanan
| music          = K. Venkat Prabhu Shankar Sam C. S.
| cinematography = G. Sathish
| editing        = Hari Shankar
| studio         = KTVR Creative Frames Shankar Bros
| distributor    = 
| released       =  
| country        = India
| language       = Tamil
| runtime        =
}}
Aaaah is an Indian Tamil-language horror anthology film written and directed by Hari Shankar and Hareesh Narayan, in their third venture following Orr Eravuu (2010) and Ambuli (2012). The film features an ensemble cast including. It features five different horror stories shot in five different places; Japan, Dubai, the midsea of the Bay of Bengal, a highway in Andhra Pradesh and a remote ATM booth in Tamil Nadu. Produced by KTVR Creative Frames, it was released on 28 November 2014 with highly negative reviews for its sloppy screenplay.<  

==Plot==
Three friends (Gokul, Meghna and  Bala) meeting at their reunion are being drawn into a bet to prove the presence of ghosts by a rich schoolmate, Prosper (Bobby Simha) This leads them to a journey to five different places around the world suspected to be haunted. Namely events which had unfolded in Japan, Dubai, the mid-sea of the Bay of Bengal, a highway in Andhra Pradesh and a remote ATM booth in Tamil Nadu. They fail to capture ghosts in camera in all the cases. Now they were worried that they would lose 60 crore prize amount and Prosper would win. Meanwhile they learn about a video footage in which a ghost is sighted killing the ATM watchmen. The video footage is in a pen drive owned by the owner of the security company (Bosskey) in which the ATM security guard was employed. When he comes to know the huge reward money for a video footage proving existence of ghosts he demands Rs 50 lakhs for the pen drive. Meghna approaches Prosper to get Rs 50 lakhs. Instead Prosper tries to take advantage of the situation and behaves indecently with her. Meghna gets annoyed and returns home and narrates the incident to Gokul and Bala. That night Meghna gets a call from her lover (Ajay) and with shock and fear she utters one word.....  accident!!!!. Three of them visit the place and find Ajay dying. Before dying he narrates the incident and records in his camera. Ajay and his friends while traveling in a car watch a movie from a CD which they had picked up from a vendor who states that it is an unreleased movie. As they watch the movie the same incidents starts happening to them as shown in the movie. Soon after all of them are killed and Ajay was able to burry the CD. The CD was found but before they could get hold of it all three of them are killed by the ghost. Prosper after winning the bet reclaims and rejoices his prize, a Yamaha RX100. After hearing the door bell ring Prosper opens the door to be killed by a ghost.

== Cast ==
 
*Gokulnath as Thamizh
*Meghna as Cherry
*Bala Saravanan as Singaram
*Bobby Simha as Prosper
*M. S. Bhaskar as Guru
*Bosskey 
*P. S. Srijith as Ranga
*Takehiro Shiraga
*Gaana Bala in a special appearance
 

== References ==
 

==External links==
* 
* 

 
 
 
 