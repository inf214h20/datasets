White Mischief (film)
 
 
 
{{Infobox film
| name = White Mischief
| image = White Mischief.jpg
| caption = Theatrical release poster
| director = Michael Radford
| producer = Simon Perry
| screenplay = Michael Radford Jonathan Gems
| based on =  
| starring = Greta Scacchi Charles Dance Joss Ackland Sarah Miles Geraldine Chaplin John Hurt
| music = George Fenton
| cinematography = Roger Deakins
| editing = Tom Priestley Nelson Entertainment Goldcrest Films BBC
| distributor = Columbia Pictures
| released = 22 April 1988
| runtime = 107 minutes
| country = United Kingdom
| language = English
| budget = £8 million Power, James. "Requiem for a shooting party." Sunday Times   12 Apr. 1987: 51. The Sunday Times Digital Archive. Web. 8 Apr. 2014. 
| gross = $3,107,551 (US) £1,532,903 (UK)   24 Sept. 1995: 9 . The Sunday Times Digital Archive.] Web. 29 Mar. 2014. 
}}
 Happy Valley Sir Henry Josslyn Hay, Earl of Erroll.

Based on a book by the  , Vintage Books, 1998, ISBN 0-394-75687-8  it was directed by Michael Radford.

==Plot==
With much of the rest of the world at war, a number of bored British aristocrats live dissolute and hedonistic lives in a region of Kenya known as Happy Valley, drinking, drugging and indulging in decadent sexual affairs to pass the time.

On 24 January 1941, Josslyn Hay, the philandering Earl of Erroll, is found dead in his car in a remote location. The Earl has a royal pedigree but a somewhat sordid past and a well-deserved reputation for carrying on with other mens wives.

Diana Delves Broughton is one such woman. She is the beautiful wife of Sir John Henry Delves Broughton, known to most as "Jock," a man thirty years her senior. Diana has a pre-nuptial understanding with her husband that should either of them fall in love with someone else, the other will do nothing to impede the romance.

Diana has indeed succumbed to the charms of the roguish Earl of Erroll, whose local conquests also include the drug-addicted American heiress Alice de Janze and the somewhat more reserved Nina Soames. The Earl is more serious about this affair than any of his earlier conquests, and wants Diana to marry him. She is reluctant to leave what she thinks is the financial security of her marriage to marry Errol, who has no funds or prospects, unaware that Delves Broughton is deep in debt. Privately humiliated but appearing to honour their agreement, Delves Broughton publicly toasts the couples affair at the club in Nairobi, asking Errol to bring Diana home at a specified time. Delves Broughton appears to be extremely intoxicated for the rest of the evening; once he is alone it is clear he was feigning drunkenness.

After dropping off Diana, Errol is shot to death in his car not far from the home of Delves Broughton who is soon charged with Errols murder.

Diana is distraught over losing her lover, as is Alice, who openly masturbates next to his corpse at the mortuary. A local plantation owner, Gilbert Colvile, whose only friend is Delves Broughton, quietly offers Diana advice and solace and ultimately shocking her by proposing marriage.

Delves Broughton stands trial. There are no witnesses to the crime and the physical evidence that appears incriminating is also circumstantial. He obviously had the motive and means, but is found innocent and the scandal comes to an end. The film ends with de Janze dying of drug overdose, and Diana discovering further evidence that implicates her husband in her lovers death. After menacing her with a shotgun, Broughton shoots himself in front of her. The film ends with a fleeing, bloodstained Diana discovering the remaining Happy Valley set partying around de Janzes grave.

==Cast==
* Greta Scacchi as Diana Broughton Henry "Jock" Delves Broughton, 11th Baronet Josslyn Hay, Earl of Erroll
* Sarah Miles as Alice de Janzé
* Geraldine Chaplin as Nina Soames
* Ray McAnally as Morris
* Murray Head as Lizzie
* John Hurt as Gilbert Colvile
* Trevor Howard as Jack Soames
* Susan Fleetwood as Gwladys Delamere
* Catherine Neilson as June Carbery
* Hugh Grant as Hugh Dickinson
* Alan Dobie as Sir Walter Harragin Idina Soltau

==Production==
Obtaining funding for the film was difficult and only achieved when David Puttnam became head of Columbia and agreed to provide the balance. 
==Historical Accuracy== Adelphi Hotel in Liverpool in December 1942, over a year later. 
==Reception==
The film made a loss during its theatrical release. 
==Legacy==
The case involving the 1996 murder of Ria Wolmerans by  . Tuesday, 30 January 2001. Retrieved on 27 March 2013. 

==See also==
 
*The Happy Valley, a BBC television drama also dealing with the murder, was first aired on 6 September 1987, several months before White Mischief was released.

==References==
 

==External links==
*  
*  
*  
*   article at Daily Mail
*  
*   film trailer at YouTube

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 