Night at the Museum: Battle of the Smithsonian
 
{{Infobox film
| name           = Night at the Museum: Battle of the Smithsonian
| image          = Night at the Museum 2 poster.jpg
| caption        = Promotional poster
| director       = Shawn Levy Chris Columbus Michael Barnathan Thomas Lennon
| based on       = Characters created by   Robert Ben Garant and Thomas Lennon and The Night at the Museum by Milan Trenc
| starring       = Ben Stiller Amy Adams Owen Wilson Hank Azaria Christopher Guest Alain Chabat Robin Williams
| music          = Alan Silvestri
| cinematography = John Schwartzman Don Zimmerman Dean Zimmerman 21 Laps Ingenious Film Partners 1492 Pictures Dune Entertainment
| distributor    = 20th Century Fox
| released       =  
| runtime        = 104 minutes 
| country        = United States
| language       = English
| budget         = $150 million   
| gross          = $413.1 million 
}}
Night at the Museum: Battle of the Smithsonian (also known as Night at the Museum 2 or Night at the Museum 2: Battle of the Smithsonian) is a 2009 American adventure comedy film directed by Shawn Levy, and starring Ben Stiller, Amy Adams, Owen Wilson, Hank Azaria, Christopher Guest and Alain Chabat, and Robin Williams.

It is the second and penultimate installment in the Night at the Museum (film series)|Night at the Museum trilogy and a sequel to the 2006 film Night at the Museum. It is also the last Night at the Museum film to feature Jake Cherry as Nick Daley. The film like its predecessor received mixed critical reception and a box office success by grossing over $413 million.

==Plot==
  the previous film, Larry Daley (Ben Stiller) is now CEO of Daley Devices, a direct response television company that sells inventions inspired by his experiences as a night security guard at the American Museum of Natural History. While wealthy and successful, he has not had the time to see his museum exhibit friends in several months.
 Easter Island Attila the Patrick Gallagher), Capuchin Monkey, and the Neanderthals will no longer come to life since the Golden Tablet of Akhmenrah that animated the exhibits each night will also remain.

After the exhibits leave, Larry receives a call from Jedediah. Dexter stole Akhmenrahs tablet and brought it to the Federal Archives and that Akhmenrahs evil older brother Pharaoh Kahmunrah (Hank Azaria) is attacking Jedediah and the other exhibits. Larry travels to Washington, D.C. and visits the National Air and Space Museum, the National Gallery of Art, and the Smithsonian Institution Building, searching for the Federal Archives with the help of his son Nick (Jake Cherry).

Larry sneaks into the Archives, disguised as a Smithsonian guard, and locates the exhibits, frozen in their shipping container in the middle of a battle with Kahmunrah. As Larry obtains the tablet, the sun sets and the exhibits and others at the Smithsonian come alive. Kahmunrah tells Larry that bringing exhibits to life is just one of the tablets powers, and that he intends to use it to conquer the world by raising an army from the duat|underworld.
 Napoleon Bonaparte (Alain Chabat) captures them. Napoleon takes Larry to Kahmunrah who has also allied with historical leaders Ivan the Terrible (Christopher Guest) and Al Capone (Jon Bernthal) and has rejected Darth Vader and Oscar the Grouch who wanted to join his team. Jedediah is captured by Al Capones men trying to rescue Larry, but Octavius escapes.

 
Kahmunrah attempts to open the Gate of the Underworld by pressing the symbols on the tablet, but the combination has changed. Kahmunrah forces Larry to obtain the new combination before sunrise by trapping Jedediah in a filling hourglass. He and Amelia pass a bust of Teddy Roosevelt (Robin Williams) who reads the hieroglyphics on the tablet revealing that Larry can find the combination by figuring out the secret at the heart of Pharaohs tomb. Amelia takes Larry to a statue of The Thinker for help, but he gets distracted. Amelia says hes "no Einstein" making Larry remember that he saw Einstein bobble heads in the National Air and Space Museum. They end up sharing a kiss. When Larry and Amelia walk over to the Air and Space museum Kahmunrah assumes hes trying to escape and tells his allies to kill Larry and bring him the tablet. Octavius meanwhile goes to the White House to find help but cant make it across the lawn, and is kidnapped by a squirrel. Larry and Amelia hide from Russian troops and visit the statue of Abraham Lincoln at the Lincoln Memorial. When it is safe they go to the National Air and Space Museum where Larry stops the rockets from blasting off. They consult the Albert Einstein bobble heads (Eugene Levy), who tell them that the new combination is the value of pi. When they are found by Napoleon, Ivan, and Capone, Larry and Amelia escape in the Wright Flyer while the exhibit Tuskegee Airmen hold them off, and they return to the Smithsonian Institution Building. The two separate with Amelia searching for help while Larry delays Kahmunrah.

Napoleon, Ivan, and Capone obtain the combination from one of the bobbleheads. Kahmunrah opens the Gate of the Underworld and summons an army of Horus warriors. Octavius returns on the squirrel and brings along The Lincoln statue, forcing the warriors to retreat to the underworld and Amelia frees the New York exhibits from their container and recruits other Smithsonian exhibits. As the exhibits battle, Octavius frees Jedediah from the hourglass and they attack the enemies from below and Larry is able to obtain the tablet. Napoleon Bonaparte, Al Capone and Ivan the Terrible stop Larry, but he distracts them by asking them who is the boss enabling him to escape while the three of them argue. Larry is stopped by Kahmunrah, but overpowers him and banishes him to the underworld. While the gigantic octopus goes into the waters near the Washington Monument, Amelia flies Larry and the New York exhibits back to the Natural History Museum before flying back to Washington, knowing that she will turn to dust at sunrise if she does not return to the Smithsonian.
 always getting lost."

==Cast==
 
* Ben Stiller as Larry Daley, a former security guard turned CEO of Daley Devices.
* Amy Adams as Amelia Earhart,
** Amy Adams also plays Tess, a young woman at the end of the movie who looks like Amelia Earhart.
* Owen Wilson as Jedediah, a cowboy minifigure.
* Hank Azaria as Kahmunrah, a pharaoh who is the evil brother of Ahkmenrah.
* Robin Williams as Theodore Roosevelt
* Christopher Guest as Ivan the Terrible Napoleon Bonaparte
* Jon Bernthal as Al Capone Roman soldier minifigure.
* Mizuo Peck as Sacagawea
* Ricky Gervais as Dr. McPhee, the curator at the Museum of Natural History.
* Bill Hader as George Armstrong Custer
* Rami Malek as Ahkmenrah Patrick Gallagher Attila the Hun
* Jake Cherry as Nicky Daley, the son of Larry Daley.
* Eugene Levy as Albert Einstein Bobbleheads
* Kerry van der Griend as Neanderthal #1
* Matthew Harrison as Neanderthal #2
* Rick Dobran as Neanderthal #3 Hun #1
* Darryl Quon as Hun #2
* Gerald Wong as Hun #3
* Paul Chih-Ping Cheng as Hun #4
* Jay Baruchel as Joey Motorola, a sailor who resides in the V-J Day in Times Square photograph.
* Mindy Kaling as Docent Tuskegee Airman #1 Craig Robinson Tuskegee Airman #2
* Clint Howard as Air and Space Mission Control Tech #1
* Matty Finochio as Air and Space Mission Control Tech #2
* George Foreman as Himself Thomas Morley as Darth Vader
* Shawn Levy as Infomercial Father
* Jonah Hill as Brandon (pronounced "Brundon") (uncredited)
* Alberta Mayne as Kissing Nurse
* Ed Helms as Larry Daleys Assistant (uncredited) Thomas Lennon as Wilbur Wright (uncredited)
* Robert Ben Garant as Orville Wright (uncredited)

===Voices===
* Hank Azaria as The Thinker, Abraham Lincoln Statue Easter Island Head
* Jonas Brothers as Cupids
* Eugene Levy as Albert Einstein Bobbleheads
* Caroll Spinney as Oscar the Grouch
* Robin Williams as Theodore Roosevelt Bust

==Exhibits at the Smithsonian== Gigantic Octopus
* Pteranodon
* Smilodon skeleton
* Hippopotamus
* Giraffe Fairy Penguin
* Kangaroo

===Artwork===
* Little Dancer of Fourteen Years
* Crying Girl
* American Gothic
* LOVE (sculpture)
* V-J Day in Times Square
* Nighthawks
* The Thinker
* Orange Balloon Dog

==Production== Thomas Lennon confirmed to Dark Horizons that they were writing a sequel to Night at the Museum, originally with the tentative title Another Night at the Museum. The writers said that "therell be existing characters and plenty of new ones."
 Patrick Gallagher, Jake Cherry, Rami Malek, Mizuo Peck, Brad Garrett and Robin Williams would return for the sequel, with Shawn Levy returning as director.
 Vancouver with some scenes filmed in the Smithsonian in Washington, D.C..  A scene was shot at the Lincoln Memorial on the night of May 21, 2008. Scenes were also shot at the American Museum of Natural History in New York on the August 18 and 20, 2008.
 Bedtime Stories, Yes Man Marley & Me in December 2008. The trailer accompanied the film Bride Wars in January, The Pink Panther 2 in February, and Dragonball Evolution in April 2009. The film was also promoted as an opening skit on American Idol, where a replica of the Idol judge seats are being held at the real Smithsonian Institution.

An alternate ending included on the DVD and Blu-ray releases featured the return of Dick Van Dyke as Cecil Fredericks, Bill Cobbs as Reginald, and Mickey Rooney as Gus.

  exhibit in the National Air and Space Museum.]] Filmmakers loaned the Smithsonian Institution props used in the movie which were displayed in the Smithsonian Castle including the pile of artifacts featured in the film.  The Smithsonian also made a brochure available online and at museum visitor service desks outlining where to find artifacts.   

As of 2009, numerous artifacts which inspired the movie were on display at Smithsonian Museums along the National Mall. Many of the artifacts are labeled with "Night at the Museum" logos. 
* National Air and Space Museum Able the space monkey
# Lunar rover
# Lunar Module
# 1903 Wright Flyer
# Amelia Earharts Lockheed Vega
# Medal belonging to Tuskegee Airmen
# Supermarine Spitfire
# F-104 Starfighter
* Steven F. Udvar-Hazy Center
# Messerschmitt 262
* National Museum of Natural History
# Gigantic octopus
# Moai
# Tyrannosaurus
* National Museum of American History
# Oscar the Grouch puppet
# George Armstrong Custers fringed jacket
# Muhammad Alis boxing gloves
# Theodore Roosevelts chaps
# Archie Bunkers chair from the television sitcom All in the Family
# Theodore Roosevelts teddy bear The Wizard of Oz

Gift shops at the Smithsonian also sell a replica of the Einstein Bobble-head, created specifically as a tie-in to the movie.

==Music==
Alan Silvestri returned to score the sequel.   
{{Infobox album  
| Name        = Night At the Museum: Battle of the Smithsonian (Original Motion Picture Soundtrack)
| Type        = Film
| Artist      = Alan Silvestri
| Caption     = 
| Alt         = 
| Released    =  
| Recorded    = 2008
| Genre       = Film score
| Length      =  
| Label       = Varese Sarabande
| Producer    = 
}}

===Track listing===
Varese Sarabande issued the score on May 19, 2009. 
{{Track listing
| headline = Night At the Museum: Battle of the Smithsonian (Original Motion Picture Soundtrack)
| total_length = 49:51
| all_writing = Alan Silvestri
| title1 = Night at the Museum: Battle of the Smithsonian 
| length1 = 02:38
| title2 = Daley Devices 
| length2 = 00:36 
| title3 = This Night is Their Last 
| length3 = 04:35 
| title4 = To Washington 
| length4 = 00:37 
| title5 = Getting Past Security 
| length5 = 01:49 
| title6 = Finding Jed and the Others 
| length6 = 03:16
| title7 = I Have Come Back to Life 
| length7 = 01:04
| title8 = The Tablet 
| length8 = 03:25
| title9 = I Smell Adventure 
| length9 = 04:31 
| title10 = He Doesnt Have All Night
| length10 = 01:46
| title11 = The Adventure Continues 
| length11 = 03:25 
| title12 = Octavius Attacks 
| length12 = 01:22 
| title13 = Entering the Air & Space Museum 
| length13 = 01:32
| title14 = Escape in Wright Flyer 
| length14 = 03:29
| title15 = Got the Combination 
| length15 = 02:19 
| title16 = Gate to the Underworld 
| length16 = 01:02 
| title17 = I Ride the Squirrel 
| length17 = 01:25 
| title18 = On Your Toes 
| length18 = 01:54 
| title19 = The Battle 
| length19 = 01:44 
| title20 = Divide the House 
| length20 = 01:28 
| title21 = Victory is Ours 
| length21 = 01:19 
| title22 = Goodbye 
| length22 = 02:43 
| title23 = Museum Open Late 
| length23 = 02:02
}}

==Release== premiered on May 14, 2009 in Washington, D.C.. The film released in UK on May 20, 2009, on May 22, 2009 in United States, and in Japan on August 12, 2009.   

The film was released under the title of "Noche en el museo 2" in Spain, "Ночь в музее 2" in Russia, "Una notte al museo 2 - La Fuga" in Italy. 

===Box office===
At the end of its box office run, Night at the Museum: Battle of the Smithsonian earned a gross of $177,243,721 in North America and $235,862,449	in other territories, for a worldwide total of $413,106,170 against a budget of $150 million. 

On Friday, May 22, 2009, its opening day, the films estimated gross was $15,568,708, for second day the film grossed $20,086,972 and for third day the gross was $18,517,606 coming in ahead of Terminator Salvation (which released on Thursday) in 4,096 theaters at #1, reaching up to $54.1&nbsp;million, with a $13,226 per-theater average over the Memorial Day weekend.  By comparison, Night at the Museum reached up to $30&nbsp;million on its opening weekend in December 2006. For its second weekend, the film grossed $24.35 million, for third weekend $14.6 million. 

For the opening weekend of May 22, 2009 the film grossed $49,036,322 while playing in theaters of 56 territories; the film debuted in UK ($6.6 million), Russia ($5.23 million) and France ($5.05 million).  The largest market in other territories being UK, Japan, Germany, Australia and France where the film grossed $32.8 million, $21.49 million, $18.78 million, $14.03 million and $13.3 million. 

===Critical reaction===
Like its predecessor, the sequel has received mixed reviews from critics. On Rotten Tomatoes, the film has a 44% "rotten" approval rating, based on 163 reviews, with an average score of 5.1/10. The sites critical consensus reads, "Night at the Museum: Battle at the Smithsonian is busy enough to keep the kids interested but the slapstick goes overboard and the special effects (however well executed) throw the production into mania".  Another review aggregator, Metacritic, which assigns a normalized rating out of 100 top reviews from mainstream critics, gave the film an average score of 42% based on 31&nbsp;reviews, indicating "mixed or average reviews". 
 Michael Phillips of the Chicago Tribune awarded the film 3&nbsp;stars stating that " s terrific -- a sparkling screen presence."  Owen Gleiberman of Entertainment Weekly gave the film a B+ stating "Battle of the Smithsonian has plenty of life. But its Adams who gives it zing."  Also, many reviews noted the costume worn by Amy Adams during the movie.  Perry Seibert of TV Guide gave the film 2&nbsp;stars despite honoring that "thanks to Azaria, a master of comic timing. His grandiose, yet slightly fey bad guy is equally funny when hes chewing out minions as he is when deliberating if Oscar the Grouch and Darth Vader are evil enough to join his team.  Michael Rechtshaffen of the Hollywood Reporter and A.O.&nbsp;Scott of The New York Times enjoyed both performances.  

One critic panned the movie on its excessive use of special effects as noted by Scott Tobias of the A.V. Club when he described the film as "a baffling master plot and a crowded pileup of special effects in search of something to do."  Roger Ebert of the Chicago Sun Times awarded the film 1½&nbsp;stars out of 4 claiming "its premise is lame, its plot relentlessly predictable, its characters with personalities that would distinguish picture books." 

In CinemaScore polls conducted during the opening weekend, cinema audiences gave Night at the Museum: Battle of the Smithsonian an average grade of "B+" on an A+ to F scale.   

===Accolades===
{| class="wikitable"
|- style="text-align:center;"
! colspan=6 style="background:#B0C4DE;" | List of awards and nominations
|- style="text-align:center;"
! style="background:#ccc;" width="10%"| Year
! style="background:#ccc;" width="25%"| Award / Film Festival
! style="background:#ccc;" width="30%"| Category
! style="background:#ccc;" width="40%"| Recipient(s)
! style="background:#ccc;" width="15%"| Result
! scope="col" class="unsortable"| Ref.
|- style="border-top:2px solid gray;"
|-
| 2009
| Teen Choice Award
| Choice Movie Comedy
| Night at the Museum: Battle of the Smithsonian
|  
| rowspan=3 style="text-align:center;"|   
|- 2010
| MTV Movie Award
| Best Comedic Performance
| Ben Stiller
|  
|-
| 36th Peoples Choice Awards|Peoples Choice Awards
| Favorite Family Movie
| Night at the Museum: Battle of the Smithsonian
|  
|}

===Home media===
Night at the Museum: Battle of the Smithsonian was made available December 1, 2009 on DVD and Blu-ray as a two-disc Special Edition and a three-disc Digital Copy Edition. 

 , the film has sold 4,083,829 DVDs and 585,023 Blu-ray discs grossing $51,481,903 and $11,674,546 totalling $63,156,449 in North America. 

==In other media==
;Video game
 

The video game based on the film was released on May 5, 2009. It was fairly well received in comparison to the majority of film-based video-games, netting a 7.5 out of 10 from IGN.com.

==Sequel==
  Thomas Lennon said to Access Hollywood, "That after the success of two Night at the Museum films, its no surprise that 20th&nbsp;Century Fox is looking to develop a third and that those suspicions are indeed true and how could you not? I think its a really outstanding idea to do Night at the Museum 3, in fact," he said. "I wonder if someones not even already working on a script for that," he added with a raised eyebrow. "I cannot confirm that for a fact, but I cannot deny it for a fact either... It might be in the works." In an interview, Stiller confirmed the sequel, however, he said that it was only in the "ideas stage". 
 Sir Lancelot.  On November 15, 2013, it was announced Skyler Gisondo would be replacing Jake Cherry for the role of Nicky Daley.   On December 18, 2013 it was announced that Robin Williams, Stiller, and Ricky Gervais would be returning for the sequel.  On January 9, 2014, it was announced that Rebel Wilson would play a security guard in the British Museum.  On January 14, 2014, the films release date was moved up from December 25, 2014, to December 19, 2014.  On January 23, 2014, it was announced Ben Kingsley would play an Egyptian Pharaoh at the British Museum.  Principal photography and production began on January 27, 2014.  In May 2014, principal photography ended. 

==References==
 

==External links==
 
 
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 