Khoon Bhari Maang
{{Infobox film
| name        = Khoon Bhari Maang
| image       = KhoonBhariMaang.jpg
| caption     = Movie Poster
| director    = Rakesh Roshan 
| producer    = Rakesh Roshan
| writer      = Mohan Kaul Ravi Kapoor 
| starring    = Rekha Kabir Bedi Sonu Walia Shatrughan Sinha Kader Khan
| music       = Rajesh Roshan 
| cinematography = 
| editing     = Sanjay Verma 
| distributor = Film Craft 
| released    = 12 August 1988 
| runtime     = 172 mins 
| country     = India
| language    = Hindi 
| awards      = 3
| budget      = 
}} Return to Eden (1983), and is about a wealthy widow who is almost killed by her lover and sets out for revenge. The film was a comeback venture for Rekha and was a critical and commercial success.

==Synopsis==
Aarti Verma (Rekha) is a widow with two children. She is an unattractive woman with a large birthmark on her face. Aartis husband died in a car accident under mysterious circumstances, and her father (Saeed Jaffrey) is one of the richest and most famous businessmen in the city. However, when Aartis father is murdered by his worker Hiralal (Kader Khan), Aartis world is completely destroyed. She does not find any sense for her life, except bringing up her children. Hiralal pretends to be a friend, and takes care of her like a father. He brings his poor nephew Sanjay (Kabir Bedi) from abroad, who is also the lover of Aartis best friend Nandini (Sonu Walia). Although Nandini loves Aarti, she is intensely in love with Sanjay, and after he requests her to help him, she finally agrees to help him rob Aarti of her wealth.

Slowly, Sanjay gets close to Aartis children. Nandini and the rest of the family convince Aarti to marry Sanjay and finally, she marries him. The day after the wedding, Aarti, Sanjay and Nandini go on a short trip, in which Sanjay pushes Aarti from the rowboat into crocodile-infested waters, so that she dies, and he inherits her wealth. The crocodile mauls Aarti and mutilates her body and face. However, Aartis body is not found and Sanjay cannot inherit the legacy until her body is found and her death is established beyond any doubt. As a result, the family is in a serious situation. Sanjay, out of frustrations becomes abusive to the children and Aartis  pets. While all of this is occurring, Aarti is found adrift by an old farmer, who rescues her.

A few months later, the horribly-disfigured Aarti decides to return to her city and avenge herself and her family. She exchanges her expensive diamond earrings for a huge amount of money, using the money to pay for extensive plastic surgery, and becomes a stunningly beautiful woman, very different from her earlier self. Aarti then changes her name to Jyoti and finds a job as a model in the same agency where Nandini works as well. Now a new person with a new identity, her goal is to conquer Sanjay as a stranger, and kill him in the same way as he had tried to kill her. Aarti, now "reincarnated" as Jyoti, goes on a dangerous journey of murder and revenge, and she will not be satisfied until she regains her home, family and dignity.

== Cast ==
* Rekha - Aarti Verma/Jyoti
* Kabir Bedi - Sanjay
* Sonu Walia - Nandini 
* Kader Khan - Hiralal Satyajeet - Baliya
* Shatrughan Sinha - J.D
* Rakesh Roshan - Vikram (Guest Appearance)
* Saeed Jaffrey - Mr. Saxena (Special Appearance)
* Tom Alter - Plastic surgeon (Special Appearance)

== Crew ==
* Producer: Rakesh Roshan
* Director: Rakesh Roshan
* Story: Mohan Kaul and Ravi Kapoor 
* Screenplay: Kader Khan 
* Dialogues: Kader Khan
* Lyrics: Indeevar
* Music: Rajesh Roshan
* Choreography: Kamal
* Editing: Sanjay Verma
* Costume Design: Abu Jani, Sandeep Khosla and Leena Daru

== Music ==
The film has four songs composed by Rajesh Roshan:
* "Hanste Hanste Kat Jaayen Raaste" - Sadhana Sargam
* "Jeene Ke Bahaane Lakhon Hain" - Asha Bhosle
* "Main Haseen Ghazab Ki" - Asha Bhosle and Sadhana Sargam
* "Main Teri Hoon Jaanam" - Sadhana Sargam

Main Teri Hoon Jaanam borrows heavily from Vangelis Chariots of Fire 

==Reception== The Tribune, while documenting the famous Hindi films of 1988, argued, "With Khoon Bhari Maang, Rakesh Roshan destroyed the myth that it was essential to have a hero as the protagonist and that heroines were there just to serve as interludes and mannenquins." Dhawan further noted, "This fast-paced movie was a crowning glory for Rekha, who rose like a phoenix in this remake of Return to Eden, and bedazzled the audience with her daredevilry."  Akshay Shah from Planet Bollywood wrote, "a perfect female oriented...   demands repeat viewing." 

==Filmfare Awards 1988==
===Awards===
* Filmfare Best Actress Award - Rekha
* Filmfare Best Supporting Actress Award - Sonu Walia 
* Filmfare Best Editing Award - Sanjay Verma

===Awards Nominated===
* Filmfare Best Movie Award
* Filmfare Best Director Award - Rakesh Roshan
* Filmfare Best Female Playback Award - Sadhana Sargam for "Main Teri Hoon Jaanam" 
* Filmfare Best Music Director Award - Rajesh Roshan

==References==
 

==External links==
* 

 

 
 
 
 
 