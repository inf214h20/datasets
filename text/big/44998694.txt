Khwab Ki Duniya
{{Infobox film
| name           = Khwab Ki Duniya
| image          = 
| image_size     = 
| caption        = 
| director       = Vijay Bhatt
| producer       = Vijay Bhatt
| writer         = 
| narrator       =  Jayant Sardar Akhtar Umakant Zahur
| music          = Lallubhai Nayak
| cinematography = 
| editing        = 
| distributor    = 
| studio         = Prakash Pictures
| released       = 1937
| runtime        = 155 minutes
| country        = India
| language       = Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}} 1937 Hindi fantasy film produced and directed by Vijay Bhatt for Prakash Pictures.   The music director was Lallubhai Nayak with lyrics written by Pandit Anuj. The film starred Jayant (actor)|Jayant,    with costars Sardar Akhtar, Umakant, Zahur, Lallubhai, Shirin and Ismail. 
 The Invisible Man (1933) based on H. G. Wells novel of the same name. The film marked the "directorial debut" of Vijay Bhatt.   

==Cast== Jayant
* Sardar Akhtar
* Miss Shirin Banu
* Umakant
* Zahur
* Madhav Marathe
* Ismail

==Production== Hatim Tai (1956) for Homi Wadias Basant Pictures.     The use of the black thread (Kaala Dhaaga) for the special effects scenes in Khwab Ki Duniya earned him the moniker of "Kaala Dhaaga".   

==Soundtrack==
The lyrics of the film were written by Pandit Anuj with music composed by Lallubhai Nayak. The singers were Rajkumari, Ranjit Rai, Shirin, Ismail Azad and Lallubhai Nayak. 
===Songlist===
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer
|-
| 1
| Aao Aao Pran Pyare Sansar Ek Naya Basayein
| Rajkumari
|-
| 2
| Chhayi Aayi Sawan Ki Ghata Bagh Mein
| Ranjit Rai, Shirin
|-
| 3 
| Kali Kali Par Hai Bhamar, Man Kaahe Lalchaaye
| Rajkumari
|-
| 4 
| Bhaj Le Bhaj Sundar Naam Murari
|
|-
| 5 
| Duniya Saari Ek Khwab Hai Jhoothi Uski Maya
|
|-
| 6 
| Neend Nahin aave Jiya Na Chain Pave
|
|-
| 7 
| Sanwariya Tori Banki Chaal Mohe Pyari Laage
| Ismail Azad, Lallubhai Nayak
|-
| 8 
| Preet Ki Reet Nahin Pehchanat Kyun Banta Hai
| Shankarrao Khatu
|-
| 9 
| Hajamaat Hai Duniya Mein Yari Dotarfa
| Ismail Azad, Lallubhai Nayak
|}

==References==
 

==External links==
* 

 

 
 
 
 
 
 

 