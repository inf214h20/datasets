Sita Kalyanam (1976 film)
{{Infobox film
| name           = Seeta Kalyanam
| image          = Seetha Kalyanam.jpg
| image_size     =
| caption        = Bapu
| producer       = Pinjala Ananda Rao P.Subba Rao
| writer         = Mullapudi Venkata Ramana
| narrator       = Dhulipala Mikkilineni Mikkilineni Kaikala Jamuna Hemalatha
| music          = K. V. Mahadevan
| cinematography = Ravikant Nagaich K.S. Prasad
| editing        = 
| studio         =
| distributor    =Ananda Lakshmi Art Movies
| released       =  
| runtime        = 125 minutes
| country        = India Telugu
| budget         =
}}
 Telugu epic Filmfare Award for Best Direction (Telugu).  The ensemble cast film deals with the wedding epic Seeta Kalyanam and Lord Ramas ten incarnations of Vishnu. The film was screened at the BFI London Film Festival, Chicago International Film Festival, San Reno and Denver International Film Festivals in 1978, and is part of the course work at the British Film Institute.  

==Awards==
;Filmfare Awards South Filmfare Award for Best Direction (Telugu) – Bapu (director)|Bapu (1976)
 
==Cast==
*Lord Rama / Vishnu - Ravikumar
*Sita / Lakshmi - Jayaprada
*King Dasaratha - Gummadi Venkateswara Rao
*King Janaka - Mikkilineni Radhakrishna Murthy
*Maharshi Vishwamitra - Mukkamala Krishna Murthy
*Ravana - Kaikala Satyanarayana Dhulipala
*Parasurama - Tyagaraaju	
*Kousalya - Hemalatha	 Jamuna
*Sumitra - P. R. Varalakshmi 	
*Mandodari - Mamata

==References==
 

 
 
 
 
 
 


 