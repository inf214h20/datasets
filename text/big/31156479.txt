Azhakulla Saleena
{{Infobox film 
| name           = Azhakulla Saleena
| image          = Azhakulla Saleena.jpg
| image_size     =
| caption        =
| director       = KS Sethumadhavan
| producer       = KSR Moorthy
| writer         = Muttathu Varkey Thoppil Bhasi (dialogues)
| screenplay     = Thoppil Bhasi Kanchana Vincent Vincent
| music          = K. J. Yesudas
| cinematography = Masthan
| editing        = TR Sreenivasalu
| studio         = Chithrakalakendram
| distributor    = Chithrakalakendram
| released       =  
| country        = India Malayalam
}}
 1973 Cinema Indian Malayalam Malayalam film Kanchana and Vincent in lead roles. The film had musical score by K. J. Yesudas.    The film is best known for the performance by Prem Nazir.  

==Cast==
 
*Prem Nazir as Kunjachan
*Jayabharathi as Saleena Kanchana as Lucyamma Vincent as Johny
*KPAC Lalitha as Mary
*Sankaradi as Agausthi
*Sreelatha Namboothiri as Tribal Girl
*T. R. Omana as Chinnamma
*Abbas Baby Sumathi as Sajan
*Bahadoor as Daiman Mathai
*Changanacherry Thankam
*Kavitha
*Pala Thankam
*Ramadas as Avarachan 
*S. P. Pillai as Paappi 
*Vanchiyoor Radha
 

==Soundtrack==
The music was composed by K. J. Yesudas and lyrics was written by Vayalar Ramavarma and Father Nagel. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Darling Darling || K. J. Yesudas || Vayalar Ramavarma || 
|-
| 2 || Ividathe Chechikku || Latha Raju || Vayalar Ramavarma || 
|-
| 3 || Kaalameghathoppi Vecha || S Janaki || Vayalar Ramavarma || 
|-
| 4 || Maraalike Maraalike || K. J. Yesudas || Vayalar Ramavarma || 
|-
| 5 || Pushpagandhi || K. J. Yesudas, B Vasantha || Vayalar Ramavarma || 
|-
| 6 || Snehathin Idayanaam || P. Leela || Father Nagel || 
|-
| 7 || Tajmahal Nirmicha || P Susheela || Vayalar Ramavarma || 
|}

==References==
 

==External links==
*  

 
 
 
 
 