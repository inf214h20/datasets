X the Unknown
 
 
{{Infobox film
| name           = X the Unknown
| image          = X The Unknown Poster.jpg
| image_size     = 275px
| caption        = theatrical release poster Leslie Norman Joseph Losey 
| producer       = Anthony Hinds
| writer         = Jimmy Sangster
| starring       = Dean Jagger Edward Chapman James Bernard Gerald Gibbs
| editing        = James Needs
| studio         = Hammer Film Productions
| distributor    = Warner Bros. (US)
| released       = 5 November 1956 (UK)
| runtime        = 81 minutes
| language       = English
| budget         = $60,000 (US) Bruce G. Hallenbeck, British Cult Cinema: Hammer Fantasy and Sci-Fi, Hemlock Books 2011 p. 78 
}} science fiction Edward Chapman Leslie Norman. The film is significant in that "it firmly established Hammers transition from B-movie thrillers to out-and-out horror/science fiction" and, with The Quatermass Xperiment (1955) and Quatermass 2 (1957), completes "an important trilogy containing relevant allegorical threads revealing Cold War anxieties and a diminishing national identity resulting from Britains decrease in status as a world power".  

==Plot==
The film takes place in the Lochmouth region of Scotland, near Glasgow. A group of soldiers are taking turns using a Geiger counter to find a small and harmless hidden source of radioactivity in a wide pit area. Private Lansing finds another mysterious source of radiation where ground water starts to boil. As the other soldiers begin to run, there is an explosion. Lansing, who was closest to the explosion, dies of radiation burns while another soldier has bad radiation burns to his back. At the site of the explosion, there is a Y-shaped crack in the ground with no apparent bottom. Dr Royston, from a nearby Atomic Energy Laboratory at Lochmouth, is called in to investigate, along with Inspector McGill, who runs security at the UK Atomic Energy Commission.

That night, a local boy, on a dare from his friend, goes to a tower on the marshes, where he sees a horrific off-camera sight. He refuses to tell his friend what has happened but continues running. The friend follows. Royston investigates the tower and finds an old man inside who had a canister of a formerly radioactive material, now drained of radioactivity. The boy dies next day from radiation burns.

Shortly afterwards, a young doctor named Unwin is having an intimate encounter with a nurse in a radiation lab at the hospital when something off-camera reduces him to a charred corpse and leaves the nurse out of her mind and screaming.

Royston hypothesizes that a form of life that existed in distant prehistory when the Earths surface was largely molten had been trapped by the crust of the Earth as it cooled; every 50 years there is a tidal surge that these creatures feel, which causes them to try to reach the surface in order to find food from radioactive sources.

Two soldiers have been left to guard the pit.  One goes to investigate a mysterious glow in the pit. The other one hears his screams and goes to investigate. He shoots at something off-camera, but is killed. The next day, Roystons colleague Elliott volunteers to be lowered into the crack, and on his way down sees the remains of one of the soldiers. Farther down, he sees the monster, still off camera, and his compatriots race to get him back to the surface again before the monster can reach him.

The army uses flamethrowers and explosives in an attempt to kill the creature, then seals the crack with concrete. Royston points out that the monster broke through miles of earth to get to the surface, so a few feet of concrete will be nowhere near enough to stop it. Meanwhile he continues with his pet experiment, looking for a way to neutralize radiation using radio waves tuned to a certain frequency. The monster comes out again that night; it is shown to be an amorphous glowing mass. Some distance away, a car with four people in it is badly burned and all four people are melted.

The thing travels to Lochmouth Atomic Energy Laboratory to get the cobalt being used there. The Lochmouth inhabitants hide in a chapel as the monster approaches them. The creature raids the nuclear facility before the authorities can remove the radioactive cobalt to a safe distance. As a result, the creature grows even larger.  As it returns through the village of Lochmouth, it narrowly misses the chapel and a little girl who has accidentally been left outside.

Royston and McGill hypothesize that the creature will move through the centre of the nearby city of Inverness, to reach another source of radioactive material. Royston has some success with his anti-radiation device, which neutralises a small container of radioactive material, but causes it to explode violently in the process.  With no time left for further experiment or consideration of safety, they set up two large "scanners" on lorries, and use a canister of cobalt as bait to lure the monster from the crack where it is hidden. The idea works, but Elliott in the jeep carrying the bait barely escapes with his life when the vehicle becomes stuck in mud while leading the monster into scanner range.  However, the jeep makes it to a safe distance, enough for the scanners to do their job, and the creature is neutralised and explodes a sufficient distance from the observers to avoid further injury or death.  As the team approaches the crack from which the monster had emerged, however, a second, more powerful explosion occurs unexpectedly, knocking several of the team off their feet, but otherwise leaving them uninjured.  Puzzled, the team continues approaching the crack, presumably to make further tests, as the film comes to an end.

==Cast==
  
* Dean Jagger as Dr. Adam Royston Edward Chapman as John Elliott
* Leo McKern as Inspector "Mac" McGill
* Anthony Newley as Lance Corporal "Spider" Webb
* Jameson Clark as Jack Harding William Lucas as Peter Elliott Peter Hammond as Lieutenant Bannerman
* Marianne Brauns as Zena, the Nurse
* Ian MacNaughton as Haggis
 
* Michael Ripper as Sergeant Harry Grimsdyke John Harvey as Major Cartwright
* Edwin Richfield as Soldier Burned on Back
* Jane Aird as Vi Harding
* Norman Macowan as Old Tom
* Neil Hallett as Unwin
* Kenneth Cope as Private Lansing
* Michael Brooke as Willie Harding
* Frazer Hines as Ian Osborne
 

==Production==
The film was originally intended by Hammer to be a sequel to the previous years successful The Quatermass Xperiment, but writer Nigel Kneale refused permission for the character of Bernard Quatermass to be used.   

The original director of the film was   

Half the films budget was provided by  . 

==Critical reception==
Variety (magazine)|Variety wrote that the film was "a highly imaginative and fanciful  ....Theres little letup in the action, and suspense angles are kept constantly to the forefront". In the UK the Daily Telegraph said it was "good, grisly fun" and "a welcomed change from interplanetary yarns" was the verdict of Films and Filming. 

==See also==
*The Blob

==References==
 

==External links==
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 