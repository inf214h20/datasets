Guru Sishyan (1997 film)
{{Infobox film
| name           = Guru Sishyan
| image          = Guru Sishyan (1997 film).jpg
| image_size     =
| caption        =
| director       = Sasi Shankar
| producer       = Saraswati Arunachalam Pillai
| writer         = Kaloor Dennis
| narrator       =
| starring       =   Johnson
| cinematography = Prathapan
| editing        = Sukumaran
| distributor    = Ajantha Production Release
| released       = 1997
| runtime        =
| country        = India
| language       = Malayalam
| budget         =
| gross          =
}} Kaveri in the lead roles. The movie was produced by Swathi Arunachalam Pillai under the banner of Ajantha Productions and was distributed by Ajantha Production Release. Kaloor Dennis wrote the screenplay and dialogues for the movie for which the story was by the director himself.

== Cast ==
{| class="wikitable"
|-
! Actor !! Character
|-
| Jagadish || Madhavan
|- Sugunan
|-
| N. F. Varghese ||
|-
| Meghanathan ||
|-
| Jagathy Sreekumar || Raghavan
|-
| Rajan P. Dev || Kunjikrishnan
|-
| Kuthiravattam Pappu ||
|-
| Mamukkoya ||
|-
| Indrans ||
|-
| Salim Kumar ||
|- Kaveri ||
|-
| Usha ||
|-
| Manju Pillai || Sarasu
|-
| Manka Mahesh ||
|}

==Songs==
Girish Puthenchery wrote the lyrics for the songs composed by Johnson (composer)|Johnson.
* Anthimukilkavin - K. J. Yesudas
* Thira nurayum sagaram - Shubha
* Kochu veluppinu - Kalabhavan Mani
* Kashmeri penne vaa - M. G. Sreekumar,  Swarnalatha
* Anthimukilkavin - K. S. Chithra
* Enganen amme - K. S. Chithra

==External links==
*  

 
 
 

 