Drums of Fate
{{Infobox film
| name           = Drums of Fate
| image          = Drums of Fate (1923) - 1.jpg
| alt            = 
| caption        = Still with George Fawcett and Mary Miles Minter
| director       = Charles Maigne
| producer       = Adolph Zukor
| screenplay     = Will M. Ritchey Stephen French Whitman 
| starring       = Mary Miles Minter Maurice Lefty Flynn George Fawcett Robert Cain Casson Ferguson Bertram Grassby Noble Johnson
| music          = 
| cinematography = James Wong Howe 	
| editing        = 
| studio         = Famous Players-Lasky Corporation
| distributor    = Paramount Pictures
| released       =  
| runtime        = 60 minutes
| country        = United States
| language       = Silent (English intertitles)
| budget         = 
| gross          =
}}
 silent drama film directed by Charles Maigne and written by Will M. Ritchey and Stephen French Whitman. The film stars Mary Miles Minter, Maurice Lefty Flynn, George Fawcett, Robert Cain, Casson Ferguson, Bertram Grassby, and Noble Johnson. The film was released on January 14, 1923, by Paramount Pictures.   Its survival status is classified as unknown,  which suggests that it is a lost film.

==Plot==
 

== Cast ==
*Mary Miles Minter as Carol Dolliver
*Maurice Lefty Flynn as Laurence Teck
*George Fawcett as Felix Brantome
*Robert Cain as Cornelius Rysbroek
*Casson Ferguson as David Verne
*Bertram Grassby as Hamoud Bin-Said
*Noble Johnson as Native King 

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 
 