Big Boy (film)
{{Infobox film name = Big Boy image = Big_Boy_1930_Poster.jpg producer = director = Alan Crosland writer = William K. Wells Rex Taylor based on a musical comedy by Harold Atteridge starring = Al Jolson Claudia Dell Louise Closser Hale Lloyd Hughes Noah Beery music = Rex Dunn Alois Reiser Sam H. Stept Bud Green cinematography = Hal Mohr editing = Ralph Dawson distributor = Warner Bros. released =   runtime = 68 minutes language = English
|country = United States
}}
Big Boy (1930) is an Sound film|all-talking musical comedy film produced by Warner Bros. in 1930. The film was directed by Alan Crosland and stars Al Jolson. The film is based on the 1924 Broadway hit show of the same name in which Al Jolson also starred.

==Synopsis==
Al Jolson plays the part of a loyal stable boy and jockey to a rich family in the South that has been interested in horse racing and breeding horses for generations. (In a flashback we see Jolsons grandfather, who also worked for the same family back in 1870.) The young heir of the family, played by Lloyd Hughes, loses a lot of money by gambling and is blackmailed by the crooks he lost to for forging a check. They convinced Hughes to ask his mother to replace Jolson with another jockey for the familys racehorse who is named Big Boy but she refuses. Then the crooks frame Jolson and he is discharged for tampering with Big Boy. Jolson is replaced by a jockey who has been bought off to lose on purpose. Jolson then find works as a waiter in a fancy restaurant. While working there he uncovers the details about the race throwing plot and he reveals this to Hughes and then, with his help, outsmarts the crooks just in time to then ride Big Boy to victory.

==Cast==
*Al Jolson as Gus 
*Claudia Dell as Annabel
*Louise Closser Hale as Mother
*Lloyd Hughes as Jack Eddie Phillips as Coley Reed 
*Noah Beery as Bagby

==Songs==
*Little Sunshine
*Tomorrow Is Another Day
*Liza Lee
*Hooray For Baby And Me

==Preservation== Warner Archive on DVD. The film seems to have been released in a longer version outside the United States where there was never any backlash against musicals. It is unknown whether a copy of this full musical version still exists.

==References==
* Richard Barrios, A Song in the Dark (Oxford University Press, 1995)

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 