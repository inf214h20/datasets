Anguish (film)
{{Infobox film
| name =Anguish
| image =Anguish (film).jpg
| caption =Original theatrical poster (U.S.)
| director = Bigas Luna
| producer = Pepón Coromina
| writer = Bigas Luna
| starring = {{Plainlist| 
* Zelda Rubinstein Michael Lerner
* Talia Paul
* Angel Jove
* Clara Pastor
}}
| cinematography = Josep M. Civit
| music = José Manuel Pagán
| editing = Tom Sabin
| distributor = Luna Films
| released =  
| runtime = 86 minutes
| country = Spain
| language = English, Spanish
| budget =
}}
 Michael Lerner, Talia Paul, Angel Jove and Clara Pastor.

==Plot==

The film begins with a written disclaimer:

 During the film you are about to see, you will be subject to subliminal messages and mild hypnosis. 
This will cause you no physical harm or lasting effect, but if for any reason you lose control or feel 
that your mind is leaving your body -- leave the auditorium immediately.
 

The disclaimer is accompanied by a narrator, who advises viewers to take caution regarding their surroundings once the film has begun, and not to engage in conversation with any unknown individuals for the duration of the running time.

In the Los Angeles theater The Rex, moviegoers watch the film within a film, The Mommy.

The Mommy tells the story of John Pressman, an extremely myopic, uncontrolled diabetic who works as an ophthalmologists assistant and is progressively growing blind. For unstated reasons, his overbearing mother Alice hypnotizes him and induces him to murder people so that he can remove their eyes and bring them back to her. One evening, John—against his mothers wishes—barricades himself up inside of a movie theater playing The Lost World (1925 film)|The Lost World, where he sets about killing the patrons one by one with a scalpel. Once Johns rampage becomes apparent, the surviving moviegoers attempt to flee the now sealed-off theater. The police bring Alice to the theater in an attempt to end the siege; in the course of trying to talk John down, Alice is accidentally shot to death by the police.

As The Mommy wears on, patrons of The Rex begin to experience anxiety attacks and disorientation in response to the events onscreen. In particular, one man grows progressively agitated, constantly checking his watch; and a teenage girl, Patty, begins to break down in tears, though she cannot entirely articulate her fear. At a key point in the film, the man exits the theater and approaches the concession stand, where hes recognized by an employee as a frequent patron of The Mommy. Pattys friend, Linda, goes to use the bathroom moments later, and witnesses The Man removing a gun from his jacket and killing the concession worker and another theater employee. The Man drags their bodies to the bathroom, an act synchronized with John killing a woman in the bathroom of a movie theater in The Mommy. Linda escapes the theater and stops a man passing on the street, whom she asks to call the police.

In The Rex, the man barricades the projectionist in the projector booth and then slips back into the theater, where he holds Patty at gunpoint and begins reciting dialogue from The Mommy. When John Pressman begins his theater rampage onscreen, the man begins indiscriminately shooting patrons of The Rex, using Patty as a human shield.

Outside The Rex, a SWAT team arrives, in synchronicity with the polices arrival at the theater in The Mommy. The police obtain access to the projector booth via the roof and send in a sniper. The man holds Patty hostage in front of the theater, addressing Alice onscreen and asking her to come save him. Attempts by the police to engage the man fail; when the police in The Mommy kill alice, the man becomes enraged, throws Patty to the ground, and resumes firing into the audience; the police sniper then shoots and kills him. Patty, looking up at the screen, has a vision of John gouging out her eye with a scalpel.
 film within a film within a film. The credits for Anguish roll as the theater patrons leave one-by one.

==Cast==

* Zelda Rubinstein — Alice Pressman, the Mother   Michael Lerner — John Pressman  
* Talia Paul — Patty  
* Ángel Jovè — The Killer
* Clara Pastor — Linda
* Isabel García Lorca — Caroline  
* Nat Baker — Teaching Doctor  
* Edward Ledden — Doctor  
* Gustavo Gili — Student #1  
* Antonio Regueiro — Student #2  
* Joaquín Ribas — Student #3  
* Janet Porter — Laboratory Nurse  
* Patricia Manget — Nurse at Clinic #1  
* Merche Gascón — Nurse at Clinic #2  
* Jose M. Chucarro — Carolines Boyfriend  
* Antonella Murgia — Ticket Girl  
* Josephine Borchaca — Concession Girl  
* Georgie Pinkley — Laura  
* Francesco Rabella — Don  
* Diane Pinkley — Popcorn Woman  
* Benito Pocino — Popcorn Man  
* Víctor Guillén — Sleepy  
* Evelyn Rosemika — Bathroom Woman  
* Michael Chandler — Projectionist  
* Vicente Gil — Taxi Driver  
* Michael Heat — Inspector  
* Pedro Vidal — Police Officer
* Robert Long — Police Officer  
* Jaime Ros — Police Officer  (as Juame Ros) 
* Miguel Montfort — Police Officer 
* Jordi Estivill — Police Officer  
* Alberto Merelles — Police Officer  
* Javier Moya — Police Officer  
* John Garcia — Police Officer  
* Kit Kincannon — Salesman
* Tatiana Thauven — Ticket Girl
* Joy Blackburn — Concession Girl
* Marc Maloney — Elderly Man
* Jasmine Parker — Elderly Woman
* Jean Paul Soto — Manny
* Javier Durán — Moe
* Marc Auba — Jack
* Randall Stewart — First Murder
* Eva Heald — Granny
* Rose Sherpac — Grannys Friend
* Emy Matías — Hairy Woman
* Elisa Crehvet — Ann
* Mingo Ràfols — Chicano
* Maribel Martínez — Hysterical Woman
* Gustavo Guarino — Hysterical Husband
* Frank Craven — Sleepy
* Mário Fernández — Black Boy
* May Vives — Blond Girl
* Craig Hill — Doctor at Hospital
* Anita Shemanski — Nurse at Hospital
* Fiacre ORafferty — Projectionist
* Maria Ricard — Pattys Mother
* John Shelly — Pattys Father
* Ricardo Azulay — Police Commander
* Joe Wolberg — SWAT Commander
* Steven Brown — SWAT Man
* Fabia Matas — SWAT Man
* Mark Parker — SWAT Man
* Philip Rodgers — Medic
* Joan Lloveras — Medic
* Ignacio García — Medic
* Jose Luise Amposte — Police Officer
* Eric Pier — Police Officer
* Clause Braum — Police Officer
* Jorge Ferrer — Police Officer
* Jorge Torras — Police Officer
* Pep Cukart — Police Officer
* Tito Álvarez — Police Officer
* John Heald — Police Officer
* Angelika Thiblant — Nurse in Street
* Elvira Salles — Nurse in Street
* Tatiana Gari — Nurse in Street
* Margarita Borchaca — Nurse in Street
* Julia Carrasco — Nurse in Street
* María Guerín — Nurse in Street

==Awards and nominations==

===Won===
Goya Awards
* Best Special Effects (Francisco Teres)

Sant Jordi Awards
* Best Film (Bigas Luna)

===Nominated===
Goya Awards
* Best Director (Bigas Luna)

==References==
 

==External links==
*   
*  

 

 
 
 
 
 
 
 
 
 
 
 