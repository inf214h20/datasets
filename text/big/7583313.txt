Genius (2003 film)
 
{{Infobox film
| name           = Genius
| image          = 
| caption        =  Babar Ahmed
| producer       = 
| writer         = Babar Ahmed
| starring       = Diogerlin Linares Kelly Walters
| music          = Kathy Haggerty
| cinematography = Jonathan Belinski
| editing        = 
| distributor    = MTI Home Video
| released       =  
| runtime        = 95 minutes
| country        = United States
| awards         =
| language       = English
| budget         =
| gross          = 
| preceded_by    = 
| followed_by    =
}}
 Babar Ahmed. The film was Ahmeds debut feature film. It was screened at several  film festivals receiving multiple awards and then released straight to video.

==Plot==
A student with a learning disability hires a self-centered teacher to make him smart enough to win over the heart of the prettiest girl in school. Whatever Mike lacks in brains, he more than makes up for in heart. But in the eyes of his beautiful classmate he may as well not exist. Desperate to capture the attention of his comely but somewhat shallow dream girl Mike enlists the help of universally disliked teacher Ms Goldwyn to increase his IQ so that he may finally form a blip on Hannahs romantic radar.

==Awards and nominations== New York Indo-American Arts Council
*2003: Won, "Outstanding Film Creativity" - Babar Ahmed

Park City Film Music Festival
*2003: Won, "Gold Medal For Excellence/Audience Favorite" - Kathy Haggerty & Babar Ahmed

Valleyfest Film Festival
*2003: Won, "Best Feature Film/Jury Award" - Babar Ahmed

==External links==
* 
*  
* 

 