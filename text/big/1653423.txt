Sliding Doors
 
{{Infobox film
| name           = Sliding Doors
| image          = Slidingdoors.jpg
| image_size     = 215px
| alt            = A vertical mirror image of a woman, above she has short blonde hair, below she has longer brown hair. 
| caption        = Theatrical release poster
| director       = Peter Howitt
| producer       = Sydney Pollack Philippa Braithwaite
| writer         = Peter Howitt John Hannah
| music          = David Hirschfelder
| cinematography = Remi Adefarasin
| editing        = John Smith Mirage Enterprises
| distributor    = Miramax Films (USA theatrical) Paramount Pictures (International, USA home video)
| released       =  
| runtime        = 99 minutes
| country        = United Kingdom United States
| language       = English
| budget         = $6 million   
| gross          = $58 million  
}} romantic Comedy-drama|comedy-drama John Hannah, John Lynch, parallel universes, based on the two paths the central characters life could take depending on whether or not she catches a train and causing different outcomes in her life. 

==Plot== parallel universes, the other detailing the separate path her life would have taken had she caught that train.
 John Hannah) John Lynch), serendipitously pop into Helens life, cheering her up and encouraging her to start her own public relations firm. She and James fall in love despite Helens reservations about beginning another relationship so soon after her ugly breakup with Gerry. Eventually, Helen discovers that she is pregnant, believing it is James child, and goes to see him at his office. She is stunned to learn from James secretary that he is married. Upset, she disappears. James finds her on a bridge and explains that he was married but is now separated and planning a divorce. He and his soon-to-be-ex-wife maintain the appearance of a happy marriage for the sake of his sick mother. After she and James declare their love, Helen walks out into the road and is hit by a car.

In the timeline in which she misses the train, she hails a taxi instead but a man tries to snatch her handbag. Helen is injured in the scuffle and goes to hospital. She arrives home after Lydia has left and carries on, oblivious of Gerrys infidelity, and takes two part-time jobs to pay the bills. Gerry conceals his infidelity and juggles the two women in his life; Lydia even interacts with Helen on several occasions. Helen has a number of conflicts with Gerry but discovers that she is pregnant. She never manages to tell him, but does tell him that she has a job interview with an international PR firm. Gerry, thinking Helen is at her interview, goes to see Lydia, who is also pregnant with his child, at her apartment. While at Lydias, Gerry answers the doorbell and sees Helen standing at the door; she is stunned to see Gerry, while Lydia tells her she cant do the interview because shes "deciding whether or not to keep your boyfriends baby." Distraught, Helen runs off and falls down Lydias staircase while trying to flee Gerry.

In both timelines, Helen goes to the hospital and loses her baby. In the timeline in which she boards the train, she dies in the arms of her new-found love, James; in the timeline in which she misses the train, she recovers and tells Gerry to leave for good. Before waking, she sees brief visions of the alternate Helens life in a dream.
 always look Nobody expects the Spanish Inquisition". She and James stare at one another, each surprised by her response. The lift doors close, leaving the audience to speculate whether it was fate or coincidence that brought Helen and James together under these circumstances.

==Cast==
* Gwyneth Paltrow as Helen Quilley John Hannah as James Hammerton John Lynch as Gerry
* Jeanne Tripplehorn as Lydia
* Zara Turner as Anna
* Douglas McFerran as Russell
* Paul Brightwell as Clive
* Nina Young as Claudia
* Virginia McKenna as Mrs. Hammerton
* Kevin McNally as Paul
* Christopher Villiers as Steve

==Production== Albert Bridge between Battersea and Chelsea, London|Chelsea. The late-night scene when Paltrow and Hannah walk down the street was filmed in Primrose Gardens (formerly Stanley Gardens) in Belsize Park. The final hospital scene where Helen and James meet in the lift was filmed at Chelsea and Westminster Hospital on Fulham Road.

==Soundtrack==
 
# Aimee Mann - "Amateur"
# Elton John - "Bennie and the Jets" Dido - Thank You" Aqua - Turn Back Time"
# Jamiroquai - "Use the Force"
# Abra Moore - "Dont Feel Like Cryin" Peach - "On My Own" Olive - "Miracle (Olive song)|Miracle" Good Enough" Blair - "Have Fun, Go Mad"
# Andre Barreau - "Got a Thing About You"
# Andre Barreau - "Call Me a Fool"
 Thank You" made its appearance on the soundtrack, becoming a hit three years later. It was a commercial for this film featuring "Thank You" as background music that inspired rapper Eminem to use Didos voice for his song, "Stan (song)|Stan". 
 the former Interscope and A&M Records|A&M.

An important omission from the soundtrack is the Patty Larkin cover of "Tenderness on the Block" that plays during the final scene. Due to copyright and recording issues, this track was never released and is only available in the movie.

==Reception==

===Box office===
The film opened at number 17 at the box office with $834,817 during its first weekend but increased by 96.5% to $1,640,438 on its second weekend. It ended up with a total gross of $11,841,544 in the United States.  It also saw success in the United Kingdom with a total box office gross in excess of £12 million. http://www.imdb.com/title/tt0120148/business  The films total world takings totaled over $58 million.   

===Critical response===
Rotten Tomatoes gives the film the film a score of 63% based on 48 reviews, with the sites consensus saying that " espite the gimmicky feel of the split narratives, the movie is watch-able due to the winning performances by the cast".  Metacritic gives the film a score of 59 out of 100 based on 23 reviews, indicating the reaction as "mixed or average". 

  Time Out described the film as "essentially a romantic comedy with a nifty gimmick". 
Angie Errigo of Empire magazine gives the film 3/5 stars. 
 
Roger Ebert gives the film 2/4 stars, and was critical of the screenplay. 

Film director Agnieszka Holland considers the film to be a botched copy of the 1981 Polish film Blind Chance, directed by Krzysztof Kieślowski, with all the "philosophical depths and stylistic subtleties stripped away". 

==See also==
* Time loop

 
;Media with a similar premise
* 12B (Tamil remake)
* Ek Second... Jo Zindagi Badal De? (Indian remake)
* Blind Chance
* Its a Wonderful Life Me Myself I
* Run Lola Run
* Smoking/No Smoking
* The Odds Against
* The Family Man
* Melinda and Melinda
* Happenstance (film)|Happenstance
* The Frasier episode "Sliding Frasiers" parodied this concept, as well as the name. Turn Left"
* The Malcolm in the Middle episode "Bowling (Malcolm in the Middle)|Bowling" Comedy Bang! Bang! episode "Andy Dick Wears a Black Suit Jacket & Skinny Tie"
* The Psych episode "Right Turn or Left for Dead"
* The Go Girls episode "What a Difference a Frock Makes (July)"
* If/Then

==References==
 

==External links==
*  
*  
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 