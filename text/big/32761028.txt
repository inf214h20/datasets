Jane Doe (2001 film)
 
 
{{Infobox television film name = Jane Doe image =   image_size =  caption =  genre = Action Thriller Thriller
 distributor = creator =  director = Kevin Elders producer = Alan Schechter writer = Kevin Elders screenplay =  story =  based on =   narrator =  starring = Teri Hatcher Rob Lowe music = Brian Tyler cinematography = Adam Kane editing = Alain Jakubowicz studio = USA Films budget =  country = United States language = English network = USA Network released =   first_aired =   last_aired =   runtime = 91 minutes num_episodes =  preceded_by =  followed_by =  website = 
}}

Jane Doe is a 2001 American television film starring Teri Hatcher and Rob Lowe directed and written by Kevin Elders.

==Plot summary==
Jane Doe (Hatcher) is the real name of arms manufacturer Cy-Kors recently fired security password employee with top clearance, whose teenage son Michael (Trevor Blumas) is kidnapped. She obeys the bizarre instructions, including getting and learning to use a gun and downloading a secret file (after which her work post starts totally deleting), dumping both in a dumpster and waiting nearby, only to witness the companys CEO Churnings being shot by a sniper using an identical weapon. Michael is released, but the pair is now wanted for the murder and both are abducted by armed men, who bring them to a ranch. There they are welcomed by Michaels father, David Doe (Lowe), who discloses to be an agent of the Defense Intelligence Agency (DIA), and so is Michael, who used her clearance as the whole thing is a sting for the kidnapping and murders brain, Avery (Mark Caven) so he will sell the enemy false data. Alas Davids DIA-partner Kurt Simmons and their boss Phelps have a dirty agenda...

 
 

 