Shadowboxing (2010 film)
 
Shadowboxing is a 2010 American drama film starring Gerald Bunsen, Kelly Briscoe, Kenny Simmons, Glenn Kalison, and David Zayas. It was written, produced, and directed by Vincent Zambrano and Jose L. Patino.

Shadowboxing premiered on May 8, 2010 at the Swansea Bay Film Festival in Swansea, Wales, UK, where it received a nomination for Best Foreign Feature Film. Its next screening will be its United States premiere at The Indie Gathering Film Festival on August 7, 2010, where it has already been announced as winning the 2nd Place Latino Film award. It is also going to continue to make a series of international screenings at film festivals in the United Kingdom, Ireland, South Africa, Thailand, Egypt, Australia, and other regions.

==Plot==
Gerald is a loner who moves back home in order to take care of his ailing father, Kenny. Knowing that his days are few, Kenny begins to try to bridge the years lost between him and his family. Maria, Geralds sister, must hold the family together while at the same time keeping her failing marriage alive. Kenny holds a secret, and that secret can bring them together, or possibly destroy whatever family they have left.

==Awards and nominations==
* 2010 Best American Feature Film (Nomination)  Heart of England Film Festival
* 2010 2nd Place Best Latino Film               The Indie Gathering
* 2010 Best Foreign Feature Film (Nomination)   Swansea Bay Film Festival

==External links==
*  

 
 
 
 
 
 


 