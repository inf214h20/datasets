Jabilamma Pelli
{{Infobox film
| name           = Jabilamma Pelli
| image          =
| caption        =
| writer         =  
| story          =
| screenplay     =
| producer       = Babu S.S.Burugupalli
| director       = A. Kodandarami Reddy
| music          = M. M. Keeravani
| starring       = Jagapati Babu Maheswari Ruthika Vanisri
| cinematography =
| editing        =
| studio         = Lakshmi Srinivasa Art Films
| released       =  
| runtime        =
| country        = India
| language       = Telugu
| budget         =
}}

Jabilamma Pelli  is a 1996 Telugu cinema|Telugu, romance film produced by Babu S.S.Burugupalli on Lakshmi Srinivasa Art Films banner, directed by A. Kodandarami Reddy. Starring Jagapati Babu, Maheswari, Ruthika, Vanisri in the lead roles and music composed by M. M. Keeravani.      

==Cast==
*Jagapati Babu as Ramudu
*Maheswari 
*Ruthika as Madhura Vani / Lakshmi
*Vanisri
*K. Ashok Kumar
*Giri Babu
*Mannava Balayya|M. Balayya
*Tanikella Bharani
*J. V. Somayajulu 
*Chalapathi Rao
*Gundu Hanumantha Rao
*Manju Bhargavi
*Rajitha
*Saraswatamma

==Soundtrack==
{{Infobox album
| Name        = Jabilamma Pelli
| Tagline     = 
| Type        = film
| Artist      = M. M. Keeravani
| Cover       = 
| Released    = 1996
| Recorded    = 
| Genre       = Soundtrack
| Length      = 30:37
| Label       = T-Series
| Producer    = M. M. Keeravani
| Reviews     =
| Last album  = Bombay Priyudu   (1996) 
| This album  = Jabilamma Pelli  (1996)
| Next album  = Pavitra Bandham  (1996)
}}

The music for the film was composed by M. M. Keeravani and released by the T-Series Music Company.
{|class="wik{{Track listing
| collapsed =
| headline =
| extra_column = Singer(s)
| total_length = 30:37
| all_writing =
| all_lyrics =
| all_music =
| writing_credits =
| lyrics_credits = yes
| music_credits =

| title1  = Oh Chukkaluri Chandamama Sirivennela Sitarama Sastry SP Balu, Chitra
| length1 = 4:35

| title2  = Boddupai Vadanam
| lyrics2 = Bhuvanachandra
| extra2  = Mano (singer)|Mano,Chitra
| length2 = 5:05

| title3  = Ghallu Ghallu
| lyrics3 = Sirivennela Sitarama Sastry
| extra3  = SP Balu,Chitra
| length3 = 5:08

| title4  = Paluku Paluku Chilakala
| lyrics4 = Sirivennela Sitarama Sastry
| extra4  = SP Balu,Chitra
| length4 = 4:40

| title5  = Kokoko Kokoko
| lyrics5 = Veturi Sundararama Murthy
| extra5  = SP Balu,Chitra
| length5 = 5:08

| title6  = Unnachota Evvarini
| lyrics6 = Sirivennela Sitarama Sastry  
| extra6  = MM Keeravani
| length6 = 6:01
}}

==References==
 

 
 
 
 


 