The Big Empty (2005 film)
{{Infobox film
| name           = The Big Empty
| image          = 
| caption        = 
| director       = Lisa Chang   Newton Thomas Sigel 
| producer       = Lisa Chang   Newton Thomas Sigel
| writer         = Lisa Chang   Newton Thomas Sigel
| starring       = Selma Blair   Elias Koteas   Richard Kind   Hugh Laurie
| music          = 
| cinematography = Newton Thomas Sigel
| editing        = 
| distributor    = 
| released       =  
| runtime        = 21:13 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
The Big Empty is a 2005 short film starring Selma Blair about a woman with an unusual condition that baffles scientists and everymen alike. It was written, directed and produced by Lisa Chang and Newton Thomas Sigel. It is based on the short story The Specialist, by Alison Smith. It was executive produced by George Clooney and Steven Soderbergh.

==Cast==
*Selma Blair as Alice
*Elias Koteas as The Specialist
*Richard Kind as TV Show Host
*Hugh Laurie as Doctor
*Gabriel Mann as The Thoughtful Man 

==Plot==
A young woman, Alice (Selma Blair), goes to the gynecologist with a mysterious ache inside her. When one doctor cant figure out just what the problem is, she sees another, and still more, until finally one specialist finds that she has an enormous, empty tundra inside of her, the opening of which is her vagina. She is forced into a media parade, brought onto talk shows so that the specialist can talk about his discovery. Alice is treated coldly, like her emptiness is the only thing important about her, and still she aches. A challenge is put forth to see who would be willing to enter the vast tundra and see where it might lead; several men enter and travel for a long time, but find nothing but ice. One young man enters and does not come back out. At the end, Alice goes to a beach and stands by the water. In a rush, the ice from the emptiness inside her flows out, freezing the ocean, and the young man appears beside her.

The Big Emptys theme song was "Song to the Siren" by This Mortal Coil, which played during the final scene.

== External links ==
*  

 
 

 