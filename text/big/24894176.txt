The Chairman (1964 film)
{{Infobox film
| name           = The Chairman 
| image          = 
| alt            = 
| caption        = 
| director       = Aleksei Saltykov
| producer       = 
| writer         = Yuri Nagibin
| narrator       = 
| starring       = Mikhail Ulyanov Nonna Mordyukova
| music          = Aleksandr Kholminov Vladimir Nikolayev
| editing        = 
| studio         = Mosfilm
| distributor    = Soyuzkino
| released       =  
| runtime        = 166 minutes
| country        = Soviet Union
| language       = Russian
| budget         = 
| gross          = 
}} 1964 Soviet film directed by Aleksei Saltykov and starring Mikhail Ulyanov, Nonna Mordyukova and Ivan Lapikov. This film was honored with a Second Prize at All-Union Film Festival in Kiev (1966). 

==Plot==
After World War II ends, soldier Yegor Trubnikov comes back to his native village to restore the ruined collective farm facilities. Rebuilding the kolkhoz is as hard for him as fighting the war. Becoming chairman, he charges himself with the burden of responsibility not only for the collective farm business, but also for the destiny of the people who are so close to him.

==Cast==
*Mikhail Ulyanov as	Yegor Trubnikov
*Ivan Lapikov as Semyon Trubnikov
*Nonna Mordyukova as Donya Trubnikova
*Vyacheslav Nevinnyy as Pavel Markushev
*Valentina Vladimirova as Polina Korshikova
*Kira Golovko as Nadya
*Arkadi Trusov as Ignat Zakharovich
*Vladimir Gulyayev as Ramenkov - District Party Instructor
*Antonina Bogdanova as Praskovya 
*Aleksandr Kashperov as Shiryaev - Blacksmith
*Aleksei Krychenkov as Alyoshka Trubnikov 
*Larisa Blinova as Liza
*Nikolai Parfyonov as Klyagin - First Secretary of the District Party Committee
*Aleksandr Galchenkov as Boris 
*Vladimir Etush as Colonel Kaloye
*Sergei Kurilov	as Kochetkov
*Sergei Blinnikov as Serdyukov
*Maya Blinova		
*Vera Burlakova		
*Antonina Dmitriyeva as Trubnikovs City Wife
*Vladimir Ferapontov		
*Sergei Golovanov as Newspaper Correspondent
*Aleksandr Kashperov		
*Mikhail Kokshenov as Misha
*A. Kolesova		
*Yelizaveta Kuzyurina as Motya Postnikova
*Aleksandr Lebedev		
*V. Lebedev		
*Vladimir Marenkov		
*Varvara Popova as Samokhina
*Vasili Popov		
*Fyodor Seleznyov		
*Vitali Solomin as Valyozhin - Physician
*Yuri Solovyov		
*A. Stroyev		
*P. Timchenko

==References==
 

==External links==
*  
*    

 
 
 
 
 
 
 
 

 