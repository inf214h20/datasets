Yes, Giorgio
{{Infobox film
| name           = Yes, Giorgio
| image          = YesGiorgio1982.jpg
| caption        = Original movie poster
| director       = Franklin J. Schaffner
| producer       = Peter Fetterman
| writer         = Anne Piper Norman Steinberg
| starring = {{plainlist|
* Luciano Pavarotti
* Kathryn Harrold
* Eddie Albert
}}
| music          = Michael J. Lewis
| editing        = Michael F. Anderson
| cinematography = Fred J. Koenekamp
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 110 min. English Italian Italian
| budget         = $19,000,000
| gross          = $1,368,588 (US) 
}}
 1982 musical/comedy film starring Luciano Pavarotti, his only venture into film acting. Michael J. Lewis provided the original music for the film with cinematography by Fred J. Koenekamp. The film is based on the novel by Anne Piper and is rated PG in the United States.

==Plot== The Met. The call brings up bad memories from his disastrous appearance there seven years earlier to where he cannot sing at rehearsal. Everyone panics thinking he is losing his voice. 

His business manager (Eddie Albert) has a female throat specialist Pamela Taylor, played by Kathryn Harrold, to look Fini over. She immediately detects the problem is psychological not physical. Taylor makes up a serious sounding name for the condition and gives Fini a shot to cure it (which she reveals to Finis business manager is harmless vitamin B12). After reacting to the prick of the needle, Fini instantly gets his voice back. 

Fini is immediately physically attracted to Taylor, and even though he is married with two children, she agrees to go out on a dinner date. After later spending a romantic week in San Francisco and the wine country the two eventually fall in love, but because Fini refuses to leave his wife, Taylor throws him a kiss and leaves The Met while Fini is singing Nessun Dorma to her.

==Main cast==
*Luciano Pavarotti ... Giorgio Fini
*Kathryn Harrold ... Pamela Taylor
*Eddie Albert ... Henry Pollack
*Paola Borboni ... Sister Theresa
*James Hong ... Kwan
*Beulah Quo ... Mei Ling
*Norman Steinberg ... Dr. Barmen
*Rod Colbin ... Ted Mullane
*Kathryn Fuller ... Faye Kennedy
*Joseph Mascolo ... Dominic Giordano
*Karen Kondazian ... Francesca Giordano
*Leona Mitchell ... Herself
*Kurt Adler ... Himself
*Emerson Buckley ... Himself
*Alexander Courage ... Conductor, Turandot

==Awards and nominations==
The song "If We Were in Love" was written by John Williams for the movie, and was nominated by the Academy Awards for Best Music, Original Song and nominated for Best Original Song in a Motion Picture by the Golden Globes. Pavarotti was nominated by the Golden Raspberry Awards for Worst Actor and Worst New Star as well as a nominee for Worst Screenplay for Norman Steinberg.

==Reception==
The movie opened to bad reviews and is considered Schaffners weakest film. Gene Siskel and Roger Ebert selected the film as one of the worst of the year in a 1982 episode of Sneak Previews. 

==References==
 

==External links==
* 
* 
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 