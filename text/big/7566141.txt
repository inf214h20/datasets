Stickmen (film)
 
 
{{Infobox Film
  | name = Stickmen
 
  | caption = Promotional film poster
  | director = Hamish Rothwell
  | producer =  Nick Ward John Leigh
  | music = 
  | cinematography = 
  | editing = 
  | distributor =  
  | released = 2001
  | runtime =  98 min. English
  | budget = 
    | followed_by    = 
}}
 2001 New New Zealand film directed by Hamish Rothwell and starring Robbie Magasiva.

== Synopsis ==
 Greek crime lord, they encounter a weird and wonderful collection of people on their way to compete for the big prize.

==Plot==
 
The film begins with Thomas and Wayne playing a game of pool against two English men. They almost win but Wayne loses the game on the last shot due to overconfidence. Their friend Jack then arrives. His pool playing skills come to the attention of Holden (Kirk Torrance), who remarks to the bartender Dave (John Leigh) that the three friends might be good enough to play in "Daddys game". "Daddys game" turns out to be an underground pool tournament run by local Greek crime lord Daddy. Each team pays $500 to enter and the winner takes away $20,000 tax free. The three of them agree to enter under the name "Stickmen".

The boys leave the bar and go to a cafe, where they meet Sara (Anne Nordhaus), a waitress who takes a liking to Thomas. The next morning the three meet up for breakfast where Jack tells them that their first match in the pool tournament will be against two men called Jimmy and Eric, at the Princess Bar. Jack then leaves for work, where he ends up having sex with Karen (Simone Kessell) in his office. Thomas attends a course on "Coping with Redundancy" but is asked to leave. He runs into Sara, who is doing a management course. The two go for a cup of coffee, which Thomas reluctantly accepts because he has a girlfriend. Later that evening, Thomas accompanies Wayne to a job interview, which turns out to be an escort driver. He will be responsible for driving prostitutes Tess (Emma Nooyen) and Lulu (Luanne Gordon) to their jobs. The madam, Janelle, reveals that Daddy owns the brothel. Wayne gets the job.

Meanwhile, Dave is having trouble coming up with the rent for his bar, which he rents from Daddy. Holden turns up and after some friendly chitchat, tells Dave that the cheque bounced. He breaks Daves thumb and promises him that if he doesnt get the money, the next one will be a beating.

The Stickmen turn up to their match at the Princess Bar, which appears to be a gay bar. They easily win the match and go back to celebrate at Daves bar. Sara turns up and she and Thomas hit it off. Karen arrives and chats with Jack. Thomas and Sara, drunk, leave the bar. Holden tells Jack and Wayne that Daddy wants to meet them, and they agree to meet the next morning.

The next morning, Jack and Wayne turn up to Daddys barber shop, which is in a small alley. Thomas doesnt show and it is revealed that he slept with Sara. Holden arrives and tells Jack and Wayne that their next match will be against the Men in Black, who are rumoured to be very good. They go inside and meet Daddy, who has two metal hooks instead of hands. He has a number of Greek thugs as bodyguards. He tells them that life is nothing without passion, and his passion is getting players together in his pool tournament.

Thomas returns home and finds his girlfriend Marie has packed up his stuff and wants him to leave, having suspected his infidelity. He arrives at the bar and is congratulated by Jack and Wayne for sleeping with Sara. Meanwhile, it is revealed that Sara and Karen know each other and are planning something.

The Men in Black, the Stickmens next opponents, turn out to be two priests. They are humorous and good-natured even when the Stickmen beat them. Thomas goes to visit Sara and the two of them bond. Holden tells Jack that Daddy wants another meeting. Jack accompanies Holden and sees Daddy intimidating Dave while shaving him. Dave, who had previously been beaten up by Daddys thugs, is given two days to get Daddys money. Daddy then gives Jack a haircut while offering him a bet of $15,000 on the tournament. Jack protests that they dont have that kind of money but Daddy says that they can work out a payment plan. Feeling intimidated, Jack agrees to the bet.
 Bucket Fountain Cuba Street, where they agree to take their relationship to the next level (being boyfriend and girlfriend).

The next evening, Wayne rescues Lulu from a fat, drunken customer, who has passed out on top of her. He invites her to come and watch him play pool to cheer her up, and takes her home to get his cue. However, he finds that Tess, who had been living with him temporarily, has stolen most of his stuff. He explodes in a fit of anger, starts drinking and tells Lulu that Caller is too good and they cant beat him. After some encouragement from Lulu, he agrees to play.
 calling his drop his trousers. Caller appears to comply with the rule, but then punches Wayne in the face. In the resulting fight, Jacks hand is cut. Holden, backed up by two of Daddys thugs wielding machetes, forces Caller to drop his trousers.

Due to Jacks injury, he is barely able to play pool, meaning that Thomas and Wayne will have to play in the semi-finals. Knowing that they have lost their star player, they decide to quit. Dave convinces them to keep playing by telling them that he gets to keep the bar if they win. He buys them matching jackets to wear to the finals.

At the semi-finals, Holden announces that their opponent is "Bastinados", who turn out to be Sara and Karen. The other match is the "Farmers" versus the "Bankers" (Hugh and his friend). Karen and Sara are exceptionally good, but Sara throws the game by sinking the cue ball on the same shot as the 8-ball. In the break between the game, Sara and Thomas go to the roof to talk, where she tells him that she threw the game and really cares about him. Jack takes Wayne and gets him drunk in the hopes that he will repeat his performance against Caller. Daddy, not wanting the Stickmen to win, locks Thomas and Sara on the roof. 

Wayne steps up to the break of the final match against the Bankers but passes out on the table. Jack has to play, being the only one left. However, Holden rescues Thomas and Sara, and Thomas proceeds to win the game for the Stickmen. Hugh snaps his cue over his knee in anger and storms out. Daddy tells them to stop by his barber shop next week to collect the money. 

A few weeks later, we see that Dave has done up his bar, which is now a successful establishment, and the film ends with the Stickmen playing a friendly game of pool in the bar.

== Characters ==
{| class="wikitable"
|- bgcolor="#CCCCCC"
! Actor !! Role
|-
| Robbie Magasiva || Jack
|-
| Scott Wills || Wayne
|-
| Paolo Rotondo || Thomas
|-
| Simone Kessell || Karen
|-
| Anne Nordhaus || Sara
|- John Leigh || Dave
|-
| Kirk Torrance || Holden
|}

== Fun Facts ==

Director Hamish Rothwell and Writer Nick Ward appear in the movie a total of 4 times (incl voice overs) - Nick as a "john" with only 20 bucks and a burger coupon, Hamish as the voice of the passed out "john" in the previous scene as well as two other small cameos.

Despite Hamishs pool education as part of this movie he has still to beat his father. Having not faced him since the infamous Rothwell family pool competition in the mid 90s.

== External links ==
*  
*   on NZ On Screen
* 

 
 
 
 
 