The Exquisite Thief
 
{{Infobox film
| name           = The Exquisite Thief
| image          = 
| caption        = 
| director       = Tod Browning
| producer       = 
| writer         = Charles W. Tyler (story: "Raggedy Ann") Harvey Gates
| starring       = Priscilla Dean Thurston Hall
| music          = 
| cinematography = Alfred Gosden
| editing        =  Universal Film Manufacturing Company
| released       =   reels
| country        = United States 
| language       = Silent (English intertitles)
| budget         = 
}}
 silent drama film directed by Tod Browning. Prints and/or fragments of this film survive.    

==Plot==
As described in a film magazine,  Blue Jean Billie (Dean), a prosperous young woman crook who lives apart from the denizens of the underworld, has pulled off many robberies of the high society world with the help of her pal Shaver Michael (De Grasse). Billie gains admission to the Vanderhoof dinner at which the engagement of their daughter to Lord Chesterton (Hall) will be announced. While the dinner is in progress, Billie gags and handcuffs special officer Detective Wood (Ross), and proceeds to make a wholesale robbery of the guests. She flees in an automobile and none succeed in tracking her save Lord Chesterton. She makes a prisoner of him, but a police raid follows and she must flee. Once more Lord Chesterton succeeds in following her and again she makes him her prisoner, but she learns to trust and love him. The special agent and Shaver Michael arrive at the scene with resulting complications, but a happy end results for all.

==Cast==
* Priscilla Dean - Blue Jean Billie
* Thurston Hall - Algernon P. Smythe (Lord Chesterton)
* Milton Ross - Det. Wood
* Sam De Grasse - Shaver Michael
* Jean Calhoun - Muriel Vanderflip

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 
 


 