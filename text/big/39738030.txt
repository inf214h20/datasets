The Great Leap (film)
{{Infobox film
| name           = The Great Leap
| image          = The Great Leap 1927 Poster.jpg
| border         = yes
| alt            = 
| caption        = German theatrical release poster
| director       = Arnold Fanck 
| producer       = Arnold Fanck 
| writer         = Arnold Fanck
| starring       = {{Plainlist|
* Leni Riefenstahl
* Luis Trenker
* Hans Schneeberger
* Paul Graetz
}}
| music          = Werner R. Heymann
| cinematography = {{Plainlist|
* Sepp Allgeier
* Richard Angst
* Albert Benitz
* Charles Métain
* Kurt Neubert
* Hans Schneeberger
}}
| editing        = Arnold Fanck  UFA
| distributor    = UFA
| released       =  
| runtime        = 112 minutes
| country        = Germany
| language       = Silent film, German intertitles
| budget         = 
| gross          = 
}}
 silent comedy film directed by Arnold Fanck and starring Leni Riefenstahl, Luis Trenker and Hans Schneeberger.  A young Italian girl living in the Dolomites falls in love with a member of a tourist party skiing on the nearby mountains.

==Cast==
* Leni Riefenstahl as Gita 
* Luis Trenker as Toni 
* Hans Schneeberger as Michael Treuherz 
* Paul Graetz as Paule

==References==
;Notes
 
;Bibliography
 
* Riefenstahl, Leni (1995)   NY Picador USA ISBN 0-312-11926-7 pg 60-62
* Hinton, David B. (2000)   Scarecrow Press ISBN 9781461635062 pg 7
* Rother, Rainer (2003) Leni Riefenstahl: The Seduction of Genius Continuum ISBN 978-0826470232 
* Salkeld, Audrey (2011)   Random House ISBN 9781446475270 pg 50-51

 

==External links==
*  
* Paul Graetz as Diener Paul http://www.walter-riml.at/welcome/photogallery/1927-gita-the-goat-girl/
*  Luis Trenker http://www.walter-riml.at/welcome/1927-gita-the-goat-girl/
*  Leni Riefenstahl http://www.walter-riml.at/welcome/1927-gita-the-goat-girl/
 

 
 
 
 
 
 
 
 
 
 


 
 