Raat Ke Rahi

{{Infobox film
| name           =Raat Ke Rahi
| image          =
| image_size     =
| caption        = Naseem
| producer       =Puspanjali Productions
| writer         =Naseem
| narrator       = Jabeen Achala Anwar Hussain Iftekhar
| music          =Bipin Babul
| cinematography =R. K. Pandit
| editing        =Y. G. Chowhan
| distributor    =
| released       =1959
| runtime        =92 mins.
| country        =India
| language       =Hindi
| budget         =
| gross          =
}}
 1959 Indian Anwar Hussain and Achala Sachdev.  Naseem (Salamuddin) went on to direct the successful Bhojpuri film Hamar Sansar (1967) which was produced by Nazir Hussain.  
 Jamaica Inn, with inns, smuggling, and a stranger involved.The film is not to be confused with the Pakistani Raat Ke Rahi (1960) which was directed by Iqbal Yousuf and starred Rehana, Darpan and Shamim Ara  

==Story==

Rita’s (Jabeen) mother is dying and the priest tends to her. She asks for her older daughter Mary (Achala Sachdev), who stays in another village with her husband James (Nazir Hussain). Mary has written to say that she cannot come and is unable to meet her mother. Following the death of her mother Rita has to move from her village and she goes to stay with her sister. On the way to the inn run by James called the Kaala Ghoda (Black Horse), she stops at another inn called Johnny’s Inn for some tea. There she meets the audacious Bobby (Shammi Kapoor) who pretends to be the owner of the Inn and makes the real owner Johnny (Anwar Hussain) act as the waiter. Bobby is James brother but they don’t get along and he had last seen Rita when she was very young. But Rita’s not impressed by Bobby. He however accompanies her as it’s dark and dangerous on the road to the Kaala Ghoda. No respectable person visits the inn and there are stories of wrongdoings going on there. 

Bobby is actually in the police and he works undercover as an unemployed gadabout drinker with no work so that he can get nearer to the criminals. His friend Johnny fancies reading detective novels and getting up in disguises presumably to look out for any unlawful activity. He has a girlfriend Daisy (Radhika) who helps him in his quest. Bobby reports to another police officer, Robert, who seems to be the only one to know of Bobby’s secret identity.  Johnny tells Bobby that his brother James is up to no good and carries out smuggling from his inn. Bobby believes Johnny but says that James is his brother and he doesn’t want to take action against him without any proof.

When Rita reaches home she is appalled to see her sister Mary looking thin and gaunt. Rita asks her if James still loves her as much as he did earlier. The sister does not answer but instead shows her around the place, which Rita explores except a locked room. James is not at home and arrives after two days. James has changed into a coarse and callous man and Rita can see that Mary is scared. In the evening the crowd at the bar consists of rough looking men. James asks Rita to help at the bar and also dance. Rita wants to leave but she stays on because of her sister. During the night Rita hears noises and discovers the dead body of a man. She senses someone coming and hides. It’s James with a hooded stranger and they are discussing the body and smuggling activity. During the day as she goes for a walk she meets Mr. Watson (Iftekhar) who is a rich gentleman and does social and charitable work. He invites her in for tea and she accepts. In the course of their conversation she tells him about the unusual activity in which her brother-in-law is involved. Mr. Watson tells her to be alert and careful and to report any further goings-on to him only.  The police officer Robert comes to search the inn but is unable to find anything. He tells Rita to beware of Bobby as he’s not a nice person to know. He thinks by doing this he may gain Rita’s confidence. 

Rita and Bobby soon fall in love but James and his henchmen get Bobby arrested on a false charge. Bobby escapes and tries to find out about his brother’s plans. One evening James has had a lot to drink and he tells Rita about the smuggling and killing of three people.  She tells Mr. Watson what James has disclosed to her. James takes Rita with him for their next activity where Bobby also appears. Rita is in the clutches of the hooded stranger and she pulls off the hood to discover him to be Mr. Watson. There is a chase with Bobby apprehending Mr. Watson who has shot and injured a now repentant James. 

==Cast==

*Shammi Kapoor: Bobby
*Jabeen (actress)|Jabeen: Rita
*Achala Sachdev: Mary
*Nazir Hussain: James Anwar Hussain:Johnny
*Radhika:Daisy
*Iftekhar: Mr. Watson
*Hammad
*Manohar
*Prem
*Prem Sagar
*Shujaat
*Shashibala

==Soundtrack==
The music was composed by Bipin Babul and the lyricist was Vishwamitra Adil. The music of two of the songs, “Pau Barah” and “Tu Kya Samjhe” has been credited to composer Jaidev.  

{| class="wikitable"
|-
!Number !!Song !!Singer
|- Ek Nazar Mohammed Rafi 
|- Tu Kya Asha Bhosle
|- Maine Pee Hai Pee Hai || Mohammed Rafi
|- Daayein Baayein Chup Chupake || Asha Bhosle
|- Aa Bhi Jaa || Asha Bhosle
|- Pau Barah || Asha Bhosle
|}



==References==
 

==External Links==
*   
* 

 
 
 