Polytechnique (film)
 
{{Infobox film
| name           = Polytechnique
| image          = Polytechnique Poster.jpg
| caption        = Theatrical release poster
| director       = Denis Villeneuve
| producer       = Don Carmody and Maxime Rémillard
| writer         = Jacques Davidts Denis Villeneuve
| starring       = Maxim Gaudette Sebastien Huberdeau Karine Vanasse Évelyne Brochu Johanne-Marie Tremblay Pierre Yves Cardinal
| music          = Benoit Charest
| cinematography =Pierre Gill
| editing        = Richard Comeau
| distributor    = Alliance Films Remstar Wild Bunch 
| released       =  
| runtime        = 77 minutes
| country        = Canada
| language       = French English
| budget         = $6 million
| gross          = $1,663,867
}}
Polytechnique is a 2009 Canadian film directed by Denis Villeneuve and written by Villeneuve and Jacques Davidts. Set in Montreal, Quebec and based on the École Polytechnique massacre (also known as the "Montreal Massacre"), the film documents the events of December 6, 1989, through the eyes of two students who witness a gunman murder fourteen young women. The film was released on February 6, 2009, in Quebec and on March 20, 2009, in Toronto, Vancouver and Calgary.   Its release has sparked controversy in Quebec.    {{cite news|url=http://blogs.montrealgazette.com/2009/02/02/some-comments-on-the-upcoming-polytechnique-film/|title=
Some comments on the upcoming Polytechnique film.....
|last=Kelley|first=Brendan|date=2009-02-02|work=Montreal Gazette|publisher=Canwest|accessdate=2012-11-11}} 

The film was screened at the 2009 Cannes Film Festival on May 17, 2009.  

==Plot== young man enters a classroom with a rifle. He orders the men to leave and the women to stay. They comply after he shoots into the ceiling to show that he is serious. He tells the women that he hates feminists. Although the women deny being feminists; he shoots at them killing some and wounding others. He then moves through corridors, the cafeteria, and another classroom, specifically targeting women. Once finished, he shoots himself with his own weapon.

The film jumps back and forth in time several times. It shows male student Jean-François who was ordered to leave the classroom. He does not just flee but he returns to try to stop the killer and/or help the victims. Valérie and Stéphanie, two surviving women, play dead thinking the killer returned, though Stephanie later dies of her injuries. Some time after the massacre Jean-François, feeling guilty for complying with the order to leave the classroom and abandoning the women, commits suicide.

==Cast== The Killer
*Sébastien Huberdeau as Jean-François
*Karine Vanasse as Valérie

The rest of the cast listed alphabetically:
*Marie-Évelyne Baribeau as Student
*Evelyne Brochu as Stéphanie
*Mireille Brullemans as Admission Offices secretary
*Pierre-Yves Cardinal as Éric
*Larissa Corriveau as Killers neighbour
*Sophie Desmarais as Female Student (3rd floor corridor)
*Jonathan Dubsky as Frightened Student
*Marina Eva as Student at the party
*Emmanuelle Girard as Student behind speakers
*Nathalie Girard as Injured student
*Natalie Hamel-Roy as Jean-François mother (voice)
*Adam Kosh as Killers roommate
*Manon Lapointe as Killers mother
*Pierre-Xavier Martel as Security Agent
*Johanne-Marie Tremblay as Jean-François mother
*Anne Trudel as Student behind speakers

===Voice cast===

==== English ====
 
__NOTOC__
{| class="wikitable"
|-
!Role
!Voice actor
|-
|Valérie
|
|-
|Jean-François
|
|- The Killer
|
|-
|Stéphanie
|
|}

==Trailer==
The trailer was released in late November 2008, in both French and English versions. An edited version of Mobys song "Everloving" (from his Play (Moby album)|Play album) is featured extensively in the trailer.

==Production==
 
The film was shot at Cégep de Maisonneuve and Collège Ahuntsic as well as Griffintown and Westmount. http://moncinema.cyberpresse.ca/nouvelles-et-critiques/nouvelles/nouvelle-cinema/3684-iPolytechniquei--film-bilingue.html 
 American one.  Villeneuve shot the film in black and white, so as to avoid the presence of blood on screen. 

The name of the perpetrator is never mentioned in the film. The end credits list Maxim Gaudettes character as "The killer".

==Notes==
Later in her career, Valérie (Karine Vanasse) wears an Iron Ring, the professional ring of Canadian engineers.

==Reception==
The film has received mostly positive reviews from film critics. Review aggregator Rotten Tomatoes reports that 85% of professional critics gave the film a positive review, with an average rating of 7.1/10. 

Peter Howell of the Toronto Star gave the film three and a half stars out of four, stating "Polytechnique makes no judgments, offers no panaceas. It shows the violence, faithfully recreating the historical record, but it doesnt wallow in it. Pierre Gills brilliant monochrome lensing minimizes the effect of the blood.   It stands as a work of art, summoning unspoken thoughts the way Picassos war abstraction Guernica (painting)|Guernica does in a scene of contemplation with Jean-François." 

Denis Seguin of ScreenDaily.com gave the film a favourable review, writing "Like Gus Van Sant’s Elephant (2003 film)|Elephant, Polytechnique is a formalist interpretation of an atrocity, with a cool perspective on the events and much for audiences to read between the frames as the film moves back and forth through time."  

Katherine Monk of Canwest News Services gave the film four stars out of five; "The paradox may sound grotesque, but it must be stated loud and clear: Denis Villeneuve and the cast of Polytechnique have transformed the tragedy of the Montreal Massacre into a work of profound beauty."  

In January 2010, it was named the Best Canadian Film of 2009 by the Toronto Film Critics Association. Brian D. Johnson, TFCA president called it a "film of astonishing courage." 

In April 2010, the film won nine Genie Awards, including Best Picture, Best Director for Villeneuve, Best Actress for Vanasse and Best Supporting Actor for Gaudette.

==Box office==
The film grossed $214,419 in Quebec cinemas during its opening weekend, ranking 13th at the Canadian box office, even with limited release.  As of May 28, 2009, the film has grossed $1.66 million,  making it one of the highest-grossing Canadian films of the year.

==DVD release==
Polytechnique was released on August 25, 2009 in two versions; a special two-disc edition with special features and a single disc edition with just the feature film. Both feature the English and French versions of the film. A Blu-ray Disc|Blu-ray format was also released. 

==See also==
* École Polytechnique Massacre
* Cinema of Quebec
* List of Quebec films

==References==
 

==External links==
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 