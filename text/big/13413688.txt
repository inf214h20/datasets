Everything's Cool (film)
{{Infobox Film
| name = Everythings Cool
| image =Cover of the movie Everythings Cool.jpg
| caption = DVD cover
| writer = 
| starring = Ross Gelbspan, Heidi Cullen, Ted Nordhaus, Michael Shellenberger, Philip Cooney
| director = Daniel B. Gold, Judith Helfand
| producer = 
| editing = 
| cinematography = 
| distributor = 
| released = 
| runtime =  English
| music = 
| awards =
| budget = |}}

Everythings Cool is a 2007 documentary film that examines the divide between scientists and the general populace on the topic of global warming. Director Dan Gold said of the motivation for the film that "Im optimistic that finally the message that this is real, that human beings are the cause of the most recent warming trend, and that its an important issue, that message is actually reaching America. On the other hand ... if that message was fully understood, we would be moving a lot faster to slow this down and to reverse this course." 

The documentary was shown at the Sundance film festival in January 2007 and at the San Francisco International Film Festival in May 2007.  The directors also took Blue Vinyl, a film about plastic pollution, to Sundance in 2002.  It was shown on CBC in Canada as part of the Passionate Eye series. 

The New York Times called it "a breezy polemic about the politics of global warming ...   adopts a cheerful comic tone to avoid scaring audiences."  The LA Times said that "With wit and passion, Gold and Helfand marshal a plethora of data and developments yet never lose their narrative thread.".  The New York Sun was less favourable, calling it "the best movie Ive ever seen about global warming for kids in junior high school, but its the most annoying movie about global warming Ive ever seen for adults." 

==References==
 

==External links==
* 
*  at  
* 
* 
* 
 
* 
*  2 out of 5.

 
 
 
 
 