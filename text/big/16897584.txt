Walking Tall Part 2
 
{{Infobox Film
| name           = Walking Tall Part 2
| image          = File:Film_Poster_for_Walking_Tall_Part_2.jpg
| imagesize      =
| caption        = Theatrical release poster
| director       = Earl Bellamy
| producer       = Charles A. Pratt
| writer         = Howard B. Kreitsek
| starring       = Bo Svenson  Luke Askew  John Davis Chandler  Robert DoQui
| music          = Walter Scharf
| cinematography = Keith C. Smith
| editing        = Art Seid
| distributor    = American International Pictures
| released       =   
| runtime        = 109 min
| country        = United States
| language       = English
| budget         =
| gross          = $11,000,000 FILM VIEW: Why Smokey And the Bandit Is Making A Killing FILM VIEW Smokey and the Bandit
Canby, Vincent. New York Times (1923-Current file)   18 Dec 1977: 109. 
}} Walking Tall. Walking Tall Part 2 was directed by Earl Bellamy, and produced by Charles A. Pratt. the film starred Bo Svenson as Buford Pusser, replacing Joe Don Baker who played Pusser in the first Walking Tall film. The on-screen title of the movie is Part 2 Walking Tall: The Legend of Buford Pusser.

==Plot==
 
Sheriff Buford Pusser is recovering from reconstructive surgery on his face that has been disfigured during an ambush on his life that killed his wife and he is deciding whether he wants to leave office or pursue those that are responsible for her death. Buford is re-elected and his first order of business is to bust up a still operating on the river bank. Buford and his deputies surprise the moonshiners and round them all up, except for Pinky Dobson (Luke Askew) who gets away in a boat. John Witter (Logan Ramsey) is angry when Dobson brings Ray Henry (John Davis Chandler) to a meeting at his mansion. Witter and Dobson talk about what to do about Buford. Witter grumbles about how Buford has cost him a lot of money by shutting down his operations. He puts pressure on Dobson to take Buford out. Dobson promises he will.

Dobson goes to a race to talk to a driver named Stud Pardee (Richard Jaeckel) about helping him to get rid of Buford. Pardee removes three lug nuts from the front wheel of Bufords car, then entices him into a high-speed car chase. Bufords wheel comes off during the chase but he survives unhurt. Meanwhile, the only participant in the ambush that Buford recognized from police photos turns up dead. Witter brings in a beautiful woman named Marganne Stilson (Angel Tompkins) to win Bufords confidence and eventually kill him.

An investigation reveals that the only blue and white Camaro in the county is owned by Stud Pardee. Buford gets a warrant, tracks down Pardee at a livestock auction and proceeds to destroy his prized car while looking for contraband. After completely destroying the car, Buford finds a pint of whiskey under Pardees seat. Buford then informs Stud that his car will be impounded and sold at auction.

Next Buford busts up a still operation that uses a church as a cover. One of the moonshiners takes off running when accosted by Buford. Buford chases him through the woods eventually catching him. The man asks Buford why he has time to bother "us poor blacks" He says Buford looks the other way because A.C. Hand (Archie Grinalds) is a friend of his fathers. Buford tells Obra to swear out a warrant for Hands arrest Buford raids Hands place and tells him hes going to be facing time in jail unless he tells him who hes working for. Bufords father (Noah Beery, Jr.) is very upset. A.C. Hand is his oldest and dearest friend. Buford reminds his father that he is after the man at the top because he "destroyed my family."

While Obra is in the middle of telling Buford that his uncle spotted a new moonshining operation, Marganne Stilson, posing as a graduate student from Ole Miss, tries to get Buford to show her around. Buford declines saying he is busy. Stilson says she will hold Buford to his promise to show her around later on. Buford is informed that the speedboat has been spotted again, coming into McNairy County. Buford sets up a "sting", but as he is leaving his house he discovers a bomb planted in his car planted by Ray Henry. Buford radios Obra and Grady to tell them to proceed without him. During the raid, Grady inadvertently steps on a trip wire alerting the moonshiners, and Pinky Dobson again escapes in his speedboat.

Buford had made a deal with a local car dealer named Floyd Tate to get a new car every 4–6 weeks. Tate calls one day to tell Buford his new car is ready. Buford lets Obra talk him into going to exchange the cars for him. While Obra is driving, the car suddenly begins accelerating. Obra also discovers the brakes are out, he cant steer either. Obra dies in a head-on collision with a truck. At the funeral, Buford tells Obras parents that Obra died in his place because his car had been tampered with, and they tell him not to worry.

Buford talks with Sheriff Tanner (Red West) in Alcorn County, to nail Pardee. Buford offers to show Stilson around. They get in Bufords car. Stilson claims to have left a map with all the places she is interested in seeing in the cottage she has rented by the lake. Buford offers to take her there. Two snipers are waiting for him in a boat. Stilson suggests a picnic. Buford and Stilson go inside her cottage where Buford informs her that there was never anyone registered at Ole Miss named Stilson. There is, however, one with a police record from Baton Rouge. He forces her to go back outside with him and frighten off the snipers before telling Stilson that he was wise to her plans.

Sheriff Tanner pursues Pardee back across the state line. Buford takes up the chase. Pardee manages to go around a roadblock set up by the deputies but later crashes and is trapped in his overturned car. Buford gets Pardee to confess that it was Pinky Dobson who paid him. Buford frees Pardee before his car explodes. Dobson once again flees in his boat when the still is raided. This time, however, they are ready for him. Carl has supervised a blockade of logs strung across the river. Buford tells Dobson to surrender. Dobson ignores him and is up-ended in his boat when he hits the barricade. He has a broken shoulder and ribs.

Ray Henry purchases a machine gun and extra ammo. Buford learns from an FBI agent that Ray Henry was Dobsons cell-mate for 3 1/2 years and that they were both seen in the vicinity the night Buford and his wife were ambushed. While recovering Dobson escapes with the help of his girlfriend, Ruby Ann (Brooke Mills). Buford and an FBI agent pursue Dobson and Ruby Ann in an off-road car chase. Buford shoots Dobson. Ruby Ann tells Buford where Ray Henry is and when Dobson tells her to shut up, she says she is not afraid of Witter.

Buford and his men go to the boarding house where Ray Henry is living. Buford is wounded but he kills Ray Henry. Pinky and Ray Henry are the last two who were actually at the ambush. Buford realizes that John Witter is the one who ordered the ambush on him and his wife.
At the end of the movie it showed an accident report where Buford died in an automobile accident, and was the only one in the car.

==Cast==
*  
* Luke Askew: Pinky Dobson
* John Davis Chandler: Ray Henry
* Robert DoQui: Obra Eaker
* Leif Garrett: Mike Pusser
* Bruce Glover: Grady Coker
* Dawn Lyn: Dwana Pusser
* Brooke Mills: Ruby Ann
* Logan Ramsey: John Witter
* Lurene Tuttle: Grandma Helen Pusser
* Angel Tompkins: Marganne Stilson
* Noah Beery, Jr.: Carl Pusser
* Richard Jaeckel: Stud Pardee
* Archie Grinalds: A.C. Hand
* Allen Mullikin: Floyd Tate

==Behind the Scenes==
The real-life Buford Pusser was signed to appear in this film, but died in an automobile accident before filming began. His death is discussed at the end of the movie. The police report is shown.

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 