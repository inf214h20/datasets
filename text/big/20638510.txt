Murder Party
{{Infobox film  name        = Murder Party image       = Murderpartyposter.jpg caption     =  director    = Jeremy Saulnier producer    = Skei Saulnier  Chris Sharp writer      = Jeremy Saulnier  starring    = Chris Sharp  Sandy Barnett  Macon Blair  Paul Goldblatt  William Lacey  Stacy Rock distributor = Magnolia Pictures (United States|US) released    =   runtime     = 79 minutes budget      = country     = United States language    = English
}}
Murder Party  is an American comedy-horror film directed by Jeremy Saulnier.  It was shot in Brooklyn, New York.  It was given the Audience Award for Best Feature at the 2007 Slamdance Film Festival   and screened within such festivals as Maryland Film Festival.

==Plot==
 
 
Christopher, a lonely and plain man, finds an invitation to a Halloween costume party entitled "Murder Party", on the street. Constructing a knight costume out of cardboard, he makes his way to Brooklyn to attend the party, only to discover it is actually a trap set by a group of deranged art students. The art students are costumed as a goth/vampire, a werewolf, a zombie cheerleader, Daryl Hannahs replicant character from Blade Runner, and a Baseball Fury from The Warriors (film)|The Warriors.  They intend to commit a murder as a piece of artwork to impress their wealthy and sinister patron. Chris brought along a loaf of pumpkin raisin bread, which one of the students starts to eat. She then reveals that she is allergic to non-organic raisins, and didnt know about them. The group asks if shell be OK, and she agrees explaining shes only a little dizzy and needs to sit down, however she falls over and hits her head on an axe and dies. The group hides the body, as they dont want their patron to see it when he arrives.

The patron comes and has arrived late to the Murder Party and is searching for students to award grant money to. Drugs and alcohol fuel the group as the situation spirals out of their control and Christopher tries to make it home from the Murder Party alive.

==The Lab of Madness== Super 8.  Their first film was made in the 6th grade and was titled Megacop (1986).  Later they were joined by Paul Goldblatt, Sandy Barnett, and Bill Lacey. The group would often do school assignments as video projects, so their vast filmography includes interpretations of classical works such as Macbeth and Beowulf. After high school, all went to various film schools and kept in touch.
 feature was a screenplay titled Moustache.  They shot a short film titled Crabwalk in an attempt to get funding for the film.  After failing to find investors, the group decided to greenlight Murder Party with no money in August 2005, and in February 2006 they began shooting. 

==Production== Chris Connelly was brought in to work in post-production on the make up effects to supplement what was done on set. Director Jeremy Saulnier names Alex Barnett as the most difficult actor to work with considering constant pranks and lack of attention span.

==References==
 

==External links==
*  
*  
*   on MonsterFresh.com

 
 
 
 
 