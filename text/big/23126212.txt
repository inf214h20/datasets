Raise the Titanic (film)
{{Infobox film 
| name = Raise the Titanic
| image = Raise The Titanic Movie Poster.jpg
| image_size = 215px
| alt = 
| caption = Theatrical release poster
| director = Jerry Jameson Lord Grade   Adam Kennedy
| story = Eric Hughes  
| based on = Raise the Titanic! by Clive Cussler
| starring = Jason Robards Richard Jordan David Selby Anne Archer Sir Alec Guinness John Barry
| cinematography = Matthew F. Leonetti
| editing = Robert F. Shugrue J. Terry Williams
| studio = ITC Entertainment
| distributor = Associated Film Distribution  
| released =  
| runtime = 114 minutes
| country = United Kingdom United States
| language = English
| budget = $40 million    
| gross = $7,000,000 
}} Lew Grades Adam Kennedy book of RMS Titanic because it is carrying cargo valuable to Cold War hegemony.

Although the film starred Jason Robards, Richard Jordan, David Selby, Anne Archer, and Sir Alec Guinness, it was poorly received by critics and audiences and proved to be a box office bomb. The film only grossed about $13.8 million against an estimated $40 million budget. Lew Grade later remarked "it would have been cheaper to lower the Atlantic Ocean|Atlantic".      

==Plot==
The film opens on the fictional island of Svardlov in the far North Sea above the Soviet Union where an American spy breaks into an old mine where he discovers the frozen body of a US Army sergeant and mining expert Jake Hobart. Next to the frozen corpse is a newspaper from 1912 as well as some mining tools from the early part of the 20th Century. Using a radiation meter, the spy discovers that what he seeks, an extremely rare mineral named byzanium was there but has been mined out leaving only traces. He is then chased and shot by Soviet forces but rescued at the last moment by Dirk Pitt (Richard Jordan) a former U.S. Navy officer and a clandestine operator.
 NUMA The National Underwater and Marine Agency, (a NASA like agency for sea exploration) admiral James Sandecker (Jason Robards) that the mineral their man was trying to find is needed to fuel a powerful new defense system code named "The Sicilian Project" that, using laser technology will be able to destroy any incoming nuclear missiles during an attack and "make nuclear war obsolete".
 North Atlantic the president on it and the operation is on. 

At this time the Soviet KGB station chief in Washington D.C., Andre Prevlov (Bo Brundin) is receiving bits and pieces of information on the project and leaks elements of this to a reporter, Dana Archibald (Ann Archer), who is also Seagrams lover as well as a former girlfriend of Pitts. The story blows the projects secret cover and Sandecker must hold a press conference to explain why the ship is being raised. Questions are raised about byzanium but are not answered. 
   
After a lengthy search in which a Titanic band members cornet is first found, experts and the U.S. Navy then begin the dangerous job of raising the ship from the seabed, in which one of the submersibles, Starfish, experiences a cabin flood and implodes. Another submersible, the Deep Quest, while attempting to clear debris from one of the upper decks suddenly tears free and accidentally crashes through the skylight above the main staircase and becomes jammed. Dirk Pitt who heads the salvage operation then decides to attempt raising the ship before the crew of the Deep Quest suffocates.

Eventually the rusting Titanic is brought to the surface using explosives to break the hull loose from the bottom suction, compressed air tanks and buoyancy aids with the Deep Quest safely breaking away during the ascent. In response, Prevlov who has been aboard a Soviet spy ship nearby arranges for a phoney distress call to draw away the American naval escorts and comes aboard and meets with Sandecker, Pitt and Seagram. He tells them that his government knows all about the mineral and challenges them for salvage of the Titanic and ownership of ore claiming it was illegally taken from Russian soil and that if there is to be a "superior weapon" made from it then "Russia must have it!" Sandecker then tells Prevlov they knew he was coming and what he would threaten them with. Pitt then escorts him to the deck where U.S. fighter jets and a nuclear attack submarine have arrived to protect the Titanic from their attempted piracy. Prevlov then leaves in defeat.  

The ship is then towed to New York harbor - its original destination and moored at the old White Star Line dock - with much fanfare, cheered on by huge crowds, escorting ships and aircraft. On entering the watertight vault, the salvage team discover the mummified remains of the American, but no mineral only boxes of gravel. As they contemplate their probable failure Sandecker tells Pitt and Seagram that they actually were thinking of a way to weaponize the byzanium to create a super bomb, not just to power a defensive system which went against everything the scientist believed in. As Pitt listens he goes through the belongings of the dead American found in the vault and finds the clue was in those final words, "Thank God for Southby". Looking at an un-mailed postcard showing a church and graveyard in the village of Southby on the English coast and where the American had arranged a fake burial for the frozen miner Jake Hobart prior to sailing back to the United States on the Titanic. Pitt and Seagram alone go the small graveyard and find that the byzanium is indeed buried there. They decide in the end to leave the mineral in the grave because they agree its existence would destabilize the status quo that maintains the peace between the West and the Soviet Union.

==Cast== Admiral James Sandecker
* Richard Jordan as Dirk Pitt
* David Selby as Dr. Gene Seagram
* Anne Archer as Dana Archibald
* Sir Alec Guinness as John Bigalow
* Bo Brundin as Captain Andre Prevlov Master Chief Vinnie Walker
* J.D. Cannon as Captain Joe Burke
* Norman Bartold as Admiral Kemper
* Elya Baskin as Marganin

==Production==

===Pre-production===
The film endured an arduous pre-production process.  Lew Grade read the script by Clive Cussler and became interested, thinking there was potential for a series along the lines of James Bond movies. He discovered that Stanley Kramer was attached to direct and Grade said he would buy the rights to the book and let Kramer direct and produce. Lew Grade, Still Dancing: My Story, William Collins & Sons 1987 p 260-261  Pre-production began and models of the ship were built; Grade said that the models were at least two or three times larger than they should be. Eventually Kramer quit due to creative differences.  

Production costs spiralled to   as work was undertaken to find a ship that could be converted to look like the sunken Titanic.  The screenplay also underwent numerous rewrites.   
Novelist Larry McMurtry - who disliked Cusslers novel considering it "less a novel than a manual on how to raise a very large boat from deep beneath the sea" - claims that he was one of approximately 17 writers who worked on the screenplay and the only one not to petition for a credit on the finished film.  Cussler himself was furious with the final result as most of the original plot had been jettisoned leaving a hollow shell of his story as well as feeling that the casting was wrong as well.

Elliott Gould was offered a lead role but turned it down. Mann, R. (1978, Oct 22). MOVIES. Los Angeles Times (1923-Current File). Retrieved from http://search.proquest.com/docview/158662943?accountid=13902 

===Filming=== Greek ocean liner SS Santa Rosa (1932)|SS Athinai was converted into a replica of the Titanic. A scale model was used for close-up underwater scenes.

A 10-tonne   scale model was also built for the scene where the Titanic is raised to the surface. Costing $7 million, the model initially proved too large for any existing water tank.  This problem led to one of the worlds first horizon tanks being constructed at the Mediterranean Film Studios near Kalkara, Malta. The 10 million gallon tank could create the illusion a ship was at sea. The Titanic model was raised more than 50 times until a satisfactory shot was acquired.   

===Soundtrack=== John Barry created the films musical score.
 Out of Africa and Dances With Wolves....Sadly, there was no release of a soundtrack recording at the time and the loss of the original session tapes were probably due to the bankruptcy and selling off of the assets of AFD, the films distributor." 

As of August 2014, Network On Air was releasing Raise The Titanic on Blu-ray in the UK with the only known available original Barry score. 

==Reception==
Raise the Titanic received mostly negative reviews at the time. It currently scores a 50% "rotten" rating on Rotten Tomatoes. 
 cinematic works 1992 book, for failing to consult him on the script when it also made huge financial losses. )

The film, which had a budget of $40 million, grossed $7 million at the US box office, plus $6.8 million in video rentals.    However, it was popular in Japan. 

Lew Grade later wrote that he "thought the movie was quite good" particularly enjoying the actual raising of the Titanic and the scene where Dirk Pitt walks into the wrecked ballroom. He blamed the failure of the film in part on the release of a TV movie on the topic, SOS Titanic. 

Raise The Titanic, along with other contemporary flops, has been credited with prompting Grades withdrawal from continued involvement with the film industry. 

===Nominations=== 1st Golden Raspberry Award Worst Picture Worst Supporting Actor Worst Screenplay

==References==
 

==External links==
*  
*  
*  

 
 

 
 
   
 
 
   
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 