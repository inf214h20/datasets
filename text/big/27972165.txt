Hunter Prey
 
{{Infobox film
| name           = Hunter Prey
| image          = HunterPrey.jpg
| alt            = 
| caption        = 
| director       = Sandy Collora
| producer       = Daren Hicks Simon Tams Sandy Collora
| writer         = Nick Damon Sandy Collora
| starring       = Damion Poitier  Isaac C. Singleton Jr.
| music          = Christopher Hoag
| cinematography = Ed Gutentag
| editing        = Toby Divine
| studio         =  
| distributor    = 
| released       =  
| runtime        = 90 minutes
| country        = United States
| language       = English
| budget         = $425,000 
| gross          = 
}} independent science science fiction directed by Sandy Collora, and written by Nick Damon and Collora.     It stars Damion Poitier, Isaac C. Singleton Jr. and Clark Bartram.  After their ship crash lands on a desert planet, an elite team of commandos attempt to recapture the last survivor of a destroyed planet before he can retaliate against their homeworld.

== Plot ==
A group of elite interstellar commandos crash lands on a planet while transporting an alien prisoner. Amidst shifting loyalties among the group, they must track down and recapture the escaped creature, alive. The escaped prisoner sets traps and picks off the commandos one by one. When only Commander Karza and Lieutenant Centauri 7 are left, Centauri suggests they kill the prisoner, but the commander insists that he be taken alive, regardless of the mounting cost. The two engage each other in a tense standoff, and the commander reveals that the prisoner, the last survivor of a planet destroyed during a war, has threatened to destroy their planet in retaliation.

The standoff is interrupted when the commander is killed by a sniper shot. The prisoner declines a shot on Centauri and ambushes him. After he knocks the Sedonian unconscious, the prisoner reveals himself as a human. The human steals Centauris supplies but leaves him alive. The human attempts to access the Sedonian computer database through a stolen communications device but is denied access. Centauri and the human, now able to communicate with each other, discuss the background of the war: Earth was destroyed because it accepted refugees of a Sedonian enemy. When a bounty hunter lands on the planet and attempts to take the human alive, Centauri intercepts him and captures the human, Lieutenant Orin Jericho.

Centauri calls for extraction and sits down to wait. Jericho criticizes the Sedonian atrocities, but Centauri says that he sees nothing wrong with enslaving other races and destroying those who oppose their way of life. When Jericho reveals that the humans have launched a cloaked ship full of enough explosives to destroy the Sedonian homeworld, Centauri appeals to Jerichos sense of honor. Jericho appears to relent, and Centauri grants him access to the Sedonian database so Jericho can reveal the ships coordinates. However, Jericho later reveals the ship as a decoy and claims to have engineered the entire situation; using his access to the Sedonian database, he relays the coordinates of their homeworld to the true ship, which was hidden on the desert planet.

Disguised in Centauris armor, Jericho eliminates the Sedonian extraction team. As Jericho enters a hatch buried in the sand, Centauri has a clear shot at him that he declines. Centauri watches through a scope as Jericho drops into the hatch and launches the bomb ship. Centauri recovers a communications device from the extraction team, and Jericho monitors a conversation in which Centauri mutinously responds to his superiors. The Sedonians demand Centauri take action against the human, but Centauri responds that Jericho is already on his way toward the homeworld. As Jericho and Centauri agree that they will have something to talk about when they next meet, Centauri begins to look for the bounty hunters ship.

== Cast ==
* Damion Poitier as Centauri 7
* Clark Bartram as Orin Jericho
* Isaac C. Singleton Jr. as Commander Karza
* Erin Gray as "Clea"
* Sandy Collora as Slyak

== Production == RED camera. 

== Release ==
The film debuted at a preview screening at the University of Southern California in February 2010,  and it was released on DVD on July 27, 2010. 

== Reception ==
Nick Hartel of DVD Talk rated it 4/5 stars and wrote, "Hunter Prey is likely to go down as one of the best sci-fi films in a long time that most people will never see."  Paul Pritchard of DVD Verdict recommended it despite pacing issues and a generic story. 

== References ==
 

== External links ==
*  
*  

 
 
 
 
 
 
 
 