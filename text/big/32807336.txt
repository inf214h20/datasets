Two Tickets to Broadway
 
{{Infobox film
| name           = Two Tickets to Broadway
| image 	 = Two Tickets to Broadway FilmPoster.jpeg
| caption        = Original film poster
| director       = James V. Kern Fred Fleck (assistant)
| producer       = Norman Krasna Jerry Wald Howard Hughes (uncredited)
| writer         = Sammy Cahn Tony Martin
| music          = Walter Scharf (score) Jule Styne (songs)
| cinematography = Edward Cronjager Harry J. Wild
| editing        = Harry Marker
| studio         = 
| distributor    = RKO Radio Pictures
| released       =  
| runtime        = 106 minutes
| country        = United States
| language       = English
| budget         = 
| gross = $2 million (US rentals) 
}} Sound Recording choreographed by Busby Berkely.

The roles of the two delicatessen owners were originally offered to Stan Laurel and Oliver Hardy who had to turn the roles down due to Laurel being ill. 

==Cast== Tony Martin as Dan Carter
* Janet Leigh as Nancy Peterson
* Gloria DeHaven as Hannah Holbrook
* Eddie Bracken as Lew Conway
* Ann Miller as Joyce Campbell
* Barbara Lawrence as S. F. Rogers
* Bob Crosby as Himself Charles Dale as Leo, Palace Deli Joe Smith as Harry, Palace Deli
* Taylor Holmes as Willard Glendon
* Buddy Baer as Sailor on Bus

==Reception==
The film recorded an estimated loss of $1,150,000. Richard Jewell & Vernon Harbin, The RKO Story. New Rochelle, New York: Arlington House, 1982. p261. 

==References==
 

==External links==
*  

 
 

 
 
 
 
 
 
 
 
 
 