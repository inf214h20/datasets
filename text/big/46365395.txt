Madurai Meenakshi (film)
{{Infobox film
| name           = Madurai Meenakshi
| image          = Madurai Meenakshi DVD cover.jpg
| image_size     = 
| caption        = DVD cover
| director       = P. Amirtham
| producer       = A. Gunanithi
| writer         = Karunanidhi  (dialogues) 
| screenplay     = Karunanidhi
| story          = P. Harirajan
| starring       =   Deva
| cinematography = P. Amirtham
| editing        = P. Venkateswara Rao
| distributor    =
| studio         = Poomalai Productions
| released       =  
| runtime        = 145 minutes
| country        = India
| language       = Tamil
}}
 1993 Tamil Selva and Vijayakumar and Chandrasekhar playing Deva and was released on 24 February 1993.   

==Plot==

Madurai (Selva (actor)|Selva) is a traffic police who dreams to become a police inspector and he lives with his mother Maragatham (Sujatha (actress)|Sujatha). One day, he clashes with Meenakshi (Ranjitha) the daughter of the corrupt home minister Ulaganathan (Captain Raju). Meenakshi uses her fathers power to change Madurais duty and Madurai becomes her house guard. Meenakshi spends her time to humiliate the poor Madurai. Kumarasamy (Vijayakumar (actor)|Vijayakumar), his brother Kulasekaran (Nassar) and his sister-in-law Gomathi (Sabitha Anand) run a free school for blind children in their property. The prime minister orders Ulaganathan to buy their property. But they refuse to sell it so Ulaganathans henchmen kill Kumarasamy. In the meantime, Madurai and Meenakshi fall in love with each other. Ulaganathan gets to know their love and transfers Madurai at the local police station. Inspector Rudra (Pradeep Shakti) and Ulaganathan decide to buy Kulasekarans property by force and to send Madurai behinds the bars. What transpires next forms the rest of the story.

==Cast==

  Selva as Madurai
*Ranjitha as Meenakshi Sujatha
*Nassar as Kulasekaran
*Captain Raju as Ulaganathan
*S. S. Chandran as Brammayya
*Vijay Krishnaraj as Stephen
*Pradeep Shakti as Inspector Rudra
*Prathapachandran as Judge Vijayakumar as Kumarasamy Chandrasekhar as DGP
*M. R. Krishnamurthy
*Thideer Kannaiah as Madasamy
*Sabitha Anand as Gomathi
*Vijaya Chandrika
 

==Soundtrack==

{{Infobox album |  
| Name        = Madurai Meenakshi
| Type        = soundtrack Deva
| Cover       = 
| Released    = 1993
| Recorded    = 1993 Feature film soundtrack
| Length      = 14:19
| Label       =  Deva
| Reviews     = 
}}

The film score and the soundtrack were composed by film composer Deva (music director)|Deva. The soundtrack, released in 1993, features 4 tracks with lyrics written by Karunanidhi and Vairamuthu.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Track !! Song !! Singer(s)!! Duration
|- 1 || I Love You Meena || Mano (singer)|Mano, K. S. Chithra || 4:38
|- 2 || Maalai || S. P. Balasubrahmanyam, K. S. Chithra || 4:17
|- 3 || Neethi Mandram || Malaysia Vasudevan || 3:11
|- 4 || Thanga Kunam || K. S. Chithra, Chorus || 2:13
|}

==References==
 

 
 
 
 
 