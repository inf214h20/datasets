The Grass Arena
  British film John Healy. The book had been out of print for a number of years, but was re-issued on 31 July 2008.

==Storyline==
Raised in a religious family with an abusive father, young Johnny soon learns that he has to learn to defend himself. He takes up boxing, but soon falls victim to alcoholism. His boxing career over, John takes to the "Grass Arena", parks where he lives with other alcoholics. His life descends into an horrific journey of homeless alcoholism and the associated lifestyle of crime and jail and danger. Amongst other events, he describes being given the drug Antabuse in a clinical trial of some kind.

In jail a fellow inmate teaches him chess and he demonstrates an extraordinary ability at the game. He is soon beating grandmasters and having his games published in newspapers. Thus he is redeemed by the game.  Healy is played by Mark Rylance in the film, which also features Pete Postlethwaite.

==External links==
* 
*James, Erwin    The Guardian, August 5, 2008.
*  A fan website dedicated to the book

 

 
 
 
 
 
 
 
 


 