Possessed (1947 film)
{{Infobox film
| name           = Possessed
| image          = Possessed47Poster.jpg
| alt            =
| caption        = Theatrical release poster
| director       = Curtis Bernhardt
| producer       = Jerry Wald
| screenplay     = Silvia Richards Ranald MacDougall
| story          = Rita Weiman
| narrator       = Geraldine Brooks
| music          = Franz Waxman
| cinematography = Joseph Valentine
| editing        = Rudi Fehr
| distributor    = Warner Bros.
| released       =  
| runtime        = 108 minutes
| country        = United States
| language       = English
| budget         = $2,592,000
| gross          = $3,072,000
}}
Possessed is a 1947 film noir directed by Curtis Bernhardt, starring Joan Crawford, Van Heflin, and Raymond Massey in a tale about an unstable womans obsession with her ex-lover. The screenplay by Ranald MacDougall and Silvia Richards was based upon a story by Rita Weiman.

==Plot==
A woman (Joan Crawford) is found wandering Los Angeles, unable to say anything other than "David". Admitted to a hospital, she is coaxed into recounting her life.

She reveals herself as Louise Howell, an emotionally   or not.                         

Time passes and David re-enters the scene, having taken an engineering job with Graham. He is surprised to find Louise with the family. Louise — still obsessed with David — makes a pass and is rebuffed. Moments later, Graham proposes to Louise and she accepts to salvage her pride. She tells him outright that she is not in love with him, but Graham pledges to make it work in spite of that.

Carol takes a fancy to David, much to the consternation of Louise, who tries to dissuade Carol from establishing a relationship with him. Louises mind begins to decline with her obsession over David; she hears voices, has hallucinations, and believes her husbands first wife is still alive.

When David and Carol consider marriage, Louise tries to end their relationship. Graham is concerned about Louises mental state and tries to persuade her to see a doctor. Believing her husband is trying to put her away, Louise bursts into Davids apartment and kills him in a schizophrenic episode.

The psychiatrist to whom Louise has recounted her story pronounces her insane and not responsible for her actions. He laments that he had not seen her sooner, as he is sure that if he had, the tragedy could have been avoided. He tells Graham that he intends to help Louise back to sanity, though the process will be long and arduous, with much pain and suffering in store for her. Graham pledges his full support and vows that he always be there for her, no matter how difficult it becomes.

==Cast==
* Joan Crawford as Louise Howell
* Van Heflin as David Sutton
* Raymond Massey as Dean Graham Geraldine Brooks as Carol Graham
* Stanley Ridges as Dr. Willard
* John Ridgely as Chief Investigator
* Moroni Olsen as Dr. Ames
* Erskine Sanford as Dr. Sherman

==Production==
Crawford spent time visiting mental wards and talking to psychiatrists to prepare for her role,  and said the part was the most difficult she ever played.
 A Stolen Life with Bette Davis. Crawford tried unsuccessfully to convince Warner Bros. to change the films title to The Secret since she had already starred in a film by the same name (Possessed (1931 film)|Possessed) earlier in her career. 

==Critical reception==
When the film was released, the staff at Variety (magazine)|Variety magazine praised the work of Crawford, yet questioned Bernhardts direction.  They wrote, "Joan Crawford cops all thesping honors in this production with a virtuoso performance as a frustrated woman ridden into madness by a guilt-obsessed mind. Actress has a self-assurance that permits her to completely dominate the screen even vis-a-vis such accomplished players as Van Heflin and Raymond Massey ... Despite its overall superiority, Possessed is somewhat marred by an ambiguous approach in Curtis Bernhardt’s direction. Film vacillates between being a cold clinical analysis of a mental crackup and a highly surcharged melodramatic vehicle for Crawford’s histrionics." 

James Agee in Time (magazine)|Time wrote, "Most of it is filmed with unusual imaginativeness and force. The film is uncommonly well acted. Miss Crawford is generally excellent", while Howard Barnes in the New York Herald Tribune argued, "  has obviously studied the aspects of insanity to recreate a rather terrifying portrait of a woman possessed by devils." 
 German expressionism techniques and many familiar film noir shadowy shots, the b/w film takes on a penetrating psychological tone and makes a case for a not guilty of murder plea due to insanity. Though Joan has a powerful presence in this movie, she played her mad role in a too cold and campy way to be thought of as a sympathetic figure. All the psychological treatment therapy sounded like psycho-babble and Joans acting was overstuffed, though some of her morbid imaginations were gripping and held my attention. Too heavy with German stimmung  it still is fun to watch the melodramatics play out in this tale of overbearing love, painful rejection, paranoia and murder." 

Film historian Bob Porfirio notes, "By developing the plot from the point-of-view of a neurotic and skillfully using flashback and fantasy scenes in a straightforward manner, the distinction between reality and Louises imagination is blurred. That makes Possessed a prime example of Oneiric (film theory)|oneirism, the dreamlike tone that is a seminal characteristic of film noir." 
 Academy Awards== The Farmers Daughter.   

==Box-office and exhibition==
The movie was a hit earning $3,027,000 at the worldwide box office on a budget of $2,592,000. 

The film was entered into the 1947 Cannes Film Festival.   

==References==
 

==External links==
*  
*  
*  
*   informational site and DVD review at DVD Beaver (includes images)
*  

 

 
 
 
 
 
 
 
 
 
 