Mujrim
{{Infobox film
| name     = Mujrim 
| image          = mujrim_1989_big.jpg
| caption        = 
| writer         = P.D. Mehra Vinay Shukla
| starring       = Mithun Chakraborty  Madhuri Dixit  Nutan  Amrish Puri  Pallavi Joshi Shakti Kapoor Sharat Saxena 
| director       = Umesh Mehra
| producer       = Parvesh Mehra
| distributor    = Eagle Films 
| editing        = 
| released   = 28 June 1989
| runtime        = 153
| country        = India
| language       = Hindi  Anjaan (Lyrics)
| awards         = 
| budget         = 
}}
Mujrim ( :  مجرم,  n Bollywood movie released on 28 June 1989, directed by Umesh Mehra. The film stars Mithun Chakraborty, Madhuri Dixit, Nutan. It also stars Amrish Puri, Shakti Kapoor and Gulshan Grover in supporting roles. The film follows the story of Shankar, who is regarded as a criminal by the society after being in jail for ten years since childhood.

==Synopsis==
Shankar (Mithun Chakraborty) was jailed at the age of thirteen, when he killed his uncle, who tried to sell his mother Yashoda (Nutan) to a rich and powerful man called Khan (Amrish Puri).
He was imprisoned for ten years. When he comes back home, he finds his mother and sister in poor conditions. He tries to keep appropriate behavior and make amends, but all his attempts fail, as he is widely recognized as a criminal, and he joins a group of criminals, whose leader is a generous man called Malik (Sharat Saxena). His mother, who is an honest woman, refuses to accept him like this and decides she has nothing to do with him. He meets Maliks daughter Sonia (Madhuri Dixit) and the two fall in love. Malik appreciates Shankars faithfulness and authorizes him as his principal successor. After Maliks death, Shankar takes over and gets into business terms with Khan. Shankar and Sonia get married and move into their new house. Shankars one and only wish is to reunite with his mother, but she refuses, and requires him to leave the crime world.

The matters get complicated and Shankar loses his way. He loses his friends in endless fights with the police, and finally when Sonia finds out that she is pregnant, she leaves him and comes to live with Yashoda. Alone and neglected, he comes back home but then his previous life persecutes him. What will be his fate in life?
The cult tamil film  Nandha  starring suriya has taken its basic plot from this movie.

== Cast ==
*Mithun Chakraborty ... Shankar Bose
*Madhuri Dixit ... Sonia
*Nutan ... Yashoda Bose 
*Amrish Puri ... Khan
*Gulshan Grover ... Nagarajan
*Shakti Kapoor ... Chandan
*Suresh Oberoi ... Inspector Gokhale
*Pallavi Joshi ... Sunanda
*Sharat Saxena ... Malik
*Johnny Lever ... Lakhpati

== Music == playback singing steps.

===Songs===
*Daata Pyar De - Sadhana Sargam 
*Kukudoo Koo I Love You - Tanvir Badana 
*Mujrim Na Kehna Mujhe - Mohammed Aziz 
*Naiyo Jeena Tere Bina - Tanvir Badana, Sadhana Sargam 
*Raat Ke Baarah Baje - Amit Kumar, Alka Yagnik, Anu Malik
*Boom Boom Laka Laka Boom - Mohammed Aziz, Anu Malik

==External links==
* 

 
 
 