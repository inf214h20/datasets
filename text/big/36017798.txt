Mata Hari (1927 film)
{{Infobox film
| name           = Mata Hari
| image          = 
| image_size     = 
| caption        = 
| director       = Friedrich Feher 
| producer       = 
| writer         = Leo Birinsky
| narrator       = 
| starring       = Magda Sonja   Wolfgang Zilzer   Fritz Kortner.
| music          = Willy Schmidt-Gentner
| editing        = 
| cinematography = Leopold Kutzleb
| studio         = National-Film
| distributor    = National-Film
| released       = 2 May 1927
| runtime        = 80 minutes
| country        = Germany Silent  German intertitles
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}} German silent silent drama film directed by Friedrich Feher and starring Magda Sonja, Wolfgang Zilzer and Fritz Kortner. It depicts the life and death of the German First World War spy Mata Hari. It was the first feature film|feature-length portrayal of Hari.

==Cast==
* Magda Sonja - Mata Hari 
* Wolfgang Zilzer - Erzherzog Oskar 
* Fritz Kortner - Graf Bobrykin 
* Mathias Wieman - Grigori 
* Emil Lind - Verteidiger 
* Alexander Murski   
* Hermann Wlach   
* Lewis Brody
* Eduard Rothauser - Militär Auditor 
* Max Maximilian - Kosaken Unteroffizier 
* Leo Connard - Poliziehofrat 
* Elisabeth Bach - Indische Dienerin Mata Haris 
* Eberhard Leithoff   
* Georg Paeschke   
* Zlatan Kasherov   
* Carl Zickner   
* Nico Turoff   
* Dorothea Albu - Dancer 
* Georg Gartz

==See also==
*Mata Hari (1931 film)
*Mata Hari (1985 film)

==Bibliography==
* Kelly, Andrew. Cinema and the Great War. Routledge, 1997.

==External links==
* 

 
 
 
 
 
 
 
 
 
 
 


 
 