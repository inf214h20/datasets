There Was an Old Couple
 
{{Infobox film
| name           = There Was an Old Couple (Жили-были старик со старухой)
| image          = 
| caption        = 
| director       = Grigori Chukhrai
| producer       = 
| writer         = Yuli Dunsky Valeri Frid
| starring       = Ivan Marin
| music          = 
| cinematography = Sergei Poluyanov
| editing        = 
| distributor    = 
| released       =  
| runtime        = 103 minutes
| country        = Soviet Union
| language       = Russian
| budget         = 
}}

There Was an Old Couple ( , Transliteration|translit.&nbsp;Zhili-byli starik so starukhoy) is a 1965 Soviet drama film directed by Grigori Chukhrai. It was entered into the 1965 Cannes Film Festival.     

==Cast==
* Ivan Marin - The Old Man, Gusakov
* Vera Kuznetsova - The Old Woman, Gusakova
* Lyudmila Maksakova - Nina
* Georgi Martynyuk - Valentin
* Galina Polskikh - Galya
* Anatoli Yabbarov - Sectarian
* Viktor Kolpakov - Paramedic
* Nikolai Kryuchkov - Director of the Sovkhoz Nikolai Sergeyev - Accountant
* Giuli Chokhonelidze - Engineer
* Yelena Derzhavina - Irochka (as Lenochka Derzhavina)
* Nikolay Khlibko - episode

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 