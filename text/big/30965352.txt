Happy People: A Year in the Taiga
{{Infobox film
| name           = Happy People: A Year in the Taiga
| image          = Happy People A Year in the Taiga poster.jpg
| caption        =
| director       = Dmitry Vasyukov Werner Herzog
| producer       = Christoph Fisser Nick N. Raslan Yanko Damboulev Carl Woebcken Klaus Badelt Timur Bekmambetov Werner Herzog
| writer         = Rudolph Herzog Werner Herzog
| narrator       = Werner Herzog
| music          = Klaus Badelt
| editing        = Joe Bini
| studio         = Studio Babelsberg
| released       =  
| runtime        = 94 minutes
| country        = Germany
| language       = English Russian
| gross          =  $338,987 
}} trappers who hunt for fur animals like sable. It also briefly detours in to a look at the life of native Ket people. The footage in the documentary was edited from a previous television work by Vasuykov, with original production and voiceovers by Herzog. 

The film premiered in Germany in November 2010, had its United States premiere at the 2010 Telluride Film Festival, and the U.S. West Coast premiere on 6 March 2011 at the San Francisco Green Film Festival. 

==References==
 

==External links==
*  
*  
*   at SF Green Film Fest
*  

 

 
 
 
 
 
 
 
 


 