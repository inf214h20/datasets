For Her Father's Sake
{{Infobox film
| name           = For Her Fathers Sake
| image          =
| caption        =
| director       = Alexander Butler 
| producer       = G.B. Samuelson
| writer         = Alfred Sutro (play) James Lindsay    Wyndham Guise
| music          = 
| cinematography = 
| editing        = 
| studio         = G.B. Samuelson Productions
| distributor    = General Film Distributors 
| released       = June 1921
| runtime        = 4,983 feet  
| country        = United Kingdom 
| awards         =
| language       = Silent   English intertitles
| budget         =
| preceded_by    =
| followed_by    =
}} silent drama James Lindsay. It was based on the play The Perfect Lover by Alfred Sutro.

==Cast==
* Owen Nares as Walter Cardew 
* Isobel Elsom as Lilian Armitage  James Lindsay as William Tremblette 
* Renee Davies as Mary Tremblette  Tom Reynolds    Norman Partridge as Joseph Tremblette 
* Cicely Reid as Martha Tremblette 
* Wyndham Guise as Mr. Armitage

==References==
 

==Bibliography==
* Low, Rachael. History of the British Film, 1918-1929. George Allen & Unwin, 1971.

 

==External links==
* 
 
 
 
 
 
 
 

 