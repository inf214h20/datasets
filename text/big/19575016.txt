Leningrad Cowboys Meet Moses
{{Infobox film
| name           = Leningrad Cowboys Meet Moses
| image          = Leningrad-cowboys-meet-moses-german-poster.jpg
| image_size     = 
| caption        = German theatrical poster
| director       = Aki Kaurismäki
| producer       = Aki Kaurismäki Reinhard Brundig Paula Oinonen Fabienne Vonier
| writer         = Sakke Järvenpää Aki Kaurismäki Mato Valtonen
| narrator       = 
| starring       = Matti Pellonpää Kari Väänänen André Wilms Nicky Tesco The Leningrad Cowboys
| music          = Mauri Sumén
| cinematography = Timo Salminen
| editing        = Aki Kaurismäki
| distributor    = Pandora Filmproduktion
| released       = 16 February 1994
| runtime        = 94 minutes
| country        = Finland/Germany/France
| language       = English
| budget         = 
| gross          = 
}} Russian rock band Leningrad Cowboys which, subsequently, became a notable real life rock band in Finland.

==Plot==
Picking up where Leningrad Cowboys Go America left off, the band has settled in Mexico after charting a top ten hit there. However, many band members become alcoholics spending most of their day drinking tequila. Before long, more than half of the band members have died from excessive drinking. The surviving members have, in the mean time, become naturalized Mexicans, complete with mustaches, Mexican wardrobe but with their pompadours still intact.
 born again, now calls himself Moses. Moses gathers the group at his room, and plans a return trip to the Promised Land (Siberia) so that they can rebuild the band. After they leave for Europe, Moses takes the Statue of Libertys nose as a souvenir.
 CIA agent (André Wilms) learns of the missing nose through an article in the New York Tribune. Moses, meanwhile, falls off the plane and meets up with the band at a beach. Several of their people from Siberia arrive in a rented bus to pick them up.

As Moses inherited Vladimirs business talent, he continues to secure gigs for the band for money. Their first stop is a Bingo parlor in France. Eventually, the CIA agent catches up with them at a hotel in Amiens and, posing as a record producer, provides them with money to play for a wedding at the hotel. The agent goes to the bus to find the nose there, but is knocked unconscious by Igor. The next day, they learn of his real identity from the CIA logo on his cigarette holder, and take him for the rest of the trip.

* In Frankfurt, Moses is recognized by a gas station attendant, and phones the police. Igor is able to get some street toughs to bust them out, and they play a show before they set off again.
* In Leipzig, Moses engages in a Bible recitation duel with a fellow band member, who tells the others what he learned and why they must return.
* In the Czech Republic, the band wash their clothes in the spring water. The CIA Agent goes through the same experience as Vladimir/Moses, and renames himself Elijah. While in the area, the band plays for a local family. Later, Elijah is asked by Moses to sing the next song.
* In Poland, one band member gets sick, so after playing for money, they leave him by a hospital doorstep.

Before they cross the border to Russia, Moses states that, according to The Holy Bible, he never sees the promised land, so he retires and returns to being Vladimir. After ridding the bus of empty bottles, they arrive home the next morning. Everyone is happy to see their return and have a party that evening. The film ends with Elijah being left alone with the nose.

==Cast and characters==
* Leningrad Cowboys – themselves
* Matti Pellonpää – Moses/Vladimir, the bands born again manager
* Kari Väänänen – Igor, the mute road manager
* André Wilms – Lazar/Johnson/Elijah, a CIA agent pursuing Moses for stealing the Statue of Libertys nose
* Nicky Tesco, who played the American cousin in Leningrad Cowboys Go America, was listed in the credits but did not appear

==Music==
There has not been a soundtrack album released for this film.  Music credited in the film include:

*"Rosita" – Written by M.Helminen
*"Nolo Tengo Dinares" – Written by Mauri Sumén
*"Kasatchok" – Traditional, arranged by Leningrad Cowboys
*"Kili Watch" – Written by G. Derse, arranged by Mauri Sumén
*"Wedding March" – Written by Erkki Melartin, arranged by Mauri Sumén
*"Matuschka" – Written by Ben Granfelt, arranged by Mauri Sumén
*"Lonely Moon" – Written by Toivo Kärki (as Pedro de Punta), Reino Helismaa (as Orvokki Itä), English lyrics by H. Tervaharju, arranged by Mauri Sumén
*"I Woke Up This Morning Last Night" – Written by Leningrad Cowboys, M. Helminen, arranged by Leningrad Cowboys
*"Rivers of Babylon" – Written by F. Farian, B. Dove, G. Reyam, J. MacNaugton, arranged by Leningrad Cowboys
*"Uralin pihlaja" – Traditional, arranged by Mauri Sumén
*"The Sunbeam and the Goblin" – Written by Reino Helismaa, English lyrics by Juice Leskinen, arranged by Mauri Sumén
*"U.S. Border" – Written by Tokela, Jouni Marjaranta

==See also==
* 1994 in film
* Cinema of Finland
*  

==External links==
*  
*  
* 

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 