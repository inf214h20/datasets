The Crow: Wicked Prayer
{{Infobox film
| name           = The Crow: Wicked Prayer
| image          = Wicked Prayer Poster.JPG
| caption        = Theatrical release poster
| director       = Lance Mungia
| producer       = Jeff Most Edward R. Pressman
| writer         = Norman Partridge (novel) Lance Mungia
| starring       = Edward Furlong David Boreanaz Tara Reid Emmanuelle Chriqui Tito Ortiz Marcus Chong Danny Trejo Dennis Hopper
| music          = Jamie Christopherson
| cinematography = Kurt Brabbee
| editing        = 
| distributor    = Dimension Films
| released       = June 3, 2005 (US)  
| runtime        = 99 min.
| country        = United States
| awards         = 
| language       = English
| budget         =
}}
 The Crow, it had a poor critical reception.

==Plot==
 rapist in a fight, lives with his dog in a mobile home in Lake Ravasu on the Raven Aztec Indian reservation|reservation. Jimmy plans to start a new life with his girlfriend, Lily (Emmanuelle Chriqui), and leave the town for good. Lilys priest father, Harold (Danny Trejo), and brother, local cop Tanner (Dave L. Ortiz) both despise Jimmy, however.
 Satanic biker precognitive powers upon Lola — and Jimmys heart. They dump the bodies inside an old freezer.

The Crow then appears and returns Jimmy to life. Jimmy discovers his newfound invincibility after attempting to shoot himself. He takes Lilys body and leaves it on her bed so it can be found. Tanner and Harold find the body and assume it was Jimmy who killed Lily.
 telepathically what really happened.
 telekinetically hangs Jimmy from a cross, while Lola kills El Niño. Luc and Lola leave the church and head to a nearby burial ground where they must consummate their ritual before sunrise in order for Lucifer to fully manifest.

Harold, Tanner, and the others free Jimmy, who tells them the crow is dying. In order to heal the bird and restore Jimmys powers, Harold performs the Crow Dance. Weakened, Jimmy heads to the graveyard and stops Luc from having sex with Lola. Luc and Jimmy engage in a fight and the revived crow returns, restoring Jimmys invulnerability. The sun rises and Lucs ritual is destroyed. Jimmy then kills Luc by impaling him on a wooden spike and cutting his throat. Lola loses her sight and tries to repent by praying to the Virgin Mary, but it is too late: Harold apprehends her and takes her to prison. Jimmy and Lilys spirits find each other in the afterlife.

==Characters== Jimmy Cuervo/The Crow. His family name, Cuervo, is the Spanish name for "crow"
* David Boreanaz - Luc "Death" Crash/Lucifer, an escaped convict who is a Satanist.
* Tara Reid - Lola Byrne, a Satanist and Lucs girlfriend.
* Marcus Chong - War, one of Lucs henchmen.
* Tito Ortiz - Famine, one of Lucs henchmen.
* Yuji Okumoto - Pestilence, one of Lucs henchmen.
* Dennis Hopper - El Niño, a Satanic preacher who heads the order of Death.
* Emmanuelle Chriqui - Lilly, Jimmys girlfriend. She along with Jimmy are killed by Luc and Lola. After Jimmys resurrection, his love for Lilly becomes the source of his strength.
* Danny Trejo - Harold, Lilly, and Sheriff Tanners father. A preacher, he is a devout Christian who is at first skeptical of Jimmy.
* Dave Baez (credited as Dave L. Ortiz) as Sheriff Tanner - Lillys older brother.

==Reception==
The film was critically panned, currently holding a 0% approval rating based on 5 reviews at Rotten Tomatoes. 

==Re-release==
In 2011, The Crow: Wicked Prayer was re-issued by  .  The only special feature was Widescreen for both films.  There was also a single feature release under the same company.

The film has been featured in various horror compilation DVDs from Echo Bridge Home Entertainment.

On September 11, 2012,   stores before its official release date had been reached.

On October 7, 2014, the film was released on DVD by  . 

==References==
 

== External links ==
*  
* 
* 

 

 
 
 
 
 
 
 
 