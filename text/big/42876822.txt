Merdeka 17805
{{Infobox film
| name      = Merdeka 17805
| image     =Merdeka-17805.jpg 220px
| image size   = 
| border     = 
| alt      = 
| caption    = Promotional poster
| director    = Yukio Fuji
| producer    = 
| writer     = Yoshihiro Ishimatsu
| starring    = {{plain list|
*Jundai Yamada
*Lola Amaria
*Naoki Hosaka
*Muhammad Iqbal
*Aulia Achsan
*Fajar Umbara
*Koji Tsukamoto
}}
| music     =  Ryouichi Kuniyoshi
| cinematography = Kenji Takama
| editing    = Akimasa Kawashima
| studio     = Toho Rapi Films
| distributor  = Rapi Films
| released    =  
| runtime    = 122 minutes
| country    = Indonesia, Japan
| language    = Indonesian, Japanese
| budget     = 
| gross     = 
}} occupation and stays to fight in the Indonesian National Revolution. The film emphasised the Empire of Japans role in Proclamation of Indonesian Independence|Indonesias independence, leading several writers to describe it as propaganda. It was also controversial in Indonesia owing to concerns of historical revisionism, because Japans occupation of the area is depicted as the Empire protecting it from Western aggression.  The film was a financial success in Japan.

==Plot== King Jayabaya centuries before. Touched by this pronouncement, Shimazaki promises to free the archipelago from colonialism. Later, he is assigned to sneak into the last Dutch stronghold in Bandung and negotiate the unconditional surrender of the Dutch Army, a mission he completes successfully.
 Defenders of the Homeland (PETA) after it is established; Shimazaki holds a leadership role.
 newly proclaimed Republic of Indonesia and fight to defend its sovereignty. At first, he does not abandon his duties to Japan, and refuses to give PETA weapons to the Indonesian guerillas, said weapons having been earmarked for surrender to the Allies. Armed only with bambu runcing (sharpened bamboo spears), these guerillas are easily killed by the returning Dutch forces and their allies. Finally, a group of former PETA soldiers&nbsp;– led by Nurhadi, Parto and Asep&nbsp;– go to the Japanese headquarters to force Shimizaki to give them the weapons. After a lengthy stand-off, he agrees to surrender the weapons and to accompany the guerrillas in fighting, saying goodbye to his friend Lieutenant Miyata (Naoki Hosaka).
 Javanese nurse, Operation Crow.
 tags of the fallen and mourns their loss, he is killed by a sniper. After killing the sniper, Nurhadi, Aryati, and the guerrillas bury Shimazaki while singing the Indonesian national anthem, "Indonesia Raya".

Decades later, at Kalibata Heroes Cemetery in Jakarta, Miyatas daughter Fumiko lays flowers on the graves of Miyata and Shimazaki. She is accompanied by Aryati (Mila Karmila) and Nurhadi (Eman Sulaeman), and has accepted the sacrifice of her father and his comrades, remarking that they had become "stars for freedom".

==Production== Japanese kōki (皇紀) calendar. According to promotional material for the film, that this dating convention was used on the proclamation of Indonesian Independence is evidence that the proclamators, Sukarno and Mohammad Hatta, felt gratitude towards the Japanese. The material further states that Japans role in Indonesian independence continues to be remembered.  Production material further notes that approximately 2,000 Japanese soldiers remained the Indonesian archipelago to help the country fight for its independence.  Similar themes had been dealt with in the 1995 video Dokuritsu Ajia no Hikari (The Light of Independent Asia), completed by the National Committee for the Fiftieth Anniversary of the End of the War. 

Merdeka 17805 was directed by Fuji Yukio. Advisors to the film included former Minister of Justice Yukio Hayashida and Professor Takao Fusayama of the Tokyo Medical and Dental University, the latter of whom had military experience in Sumatra during the occupation. 

This Japanese-Indonesia co-production was produced by Hiroaki Fujii, Satoshi Fukushima, Subajio, and Santono, with editing completed by Akimasa Kawashima. Cinematography was handled by Kenji Takama and music was provided by Ryoichi Kuniyoshi. Merdeka 17805 starred Jundai Yamada, Lola Amaria, Naoki Hosaka, Muhammad Iqbal, Aulia Achsan, Fajar Umbara, and Koji Tsukamoto. This 122-minute film contains Japanese- and Indonesian-language dialog. 

Shooting for Merdeka 17805 was completed mostly in Indonesia, over an extended period of time. The films music, Goto Kenichi opines, was "subtle and moving", and together with the films "powerful visual images" creates a form of propaganda.  He suggests that Merdeka 17805 was intended to restore Japanese pride by depicting the countrys role in Indonesias&nbsp;– and, by association, South East Asias&nbsp;– independence from Western rule.   Jonathan Crow of AllMovie echoed the sentiment, describing Merdeka 17805 as a "two-fisted, Flag of Japan|hinomaru-waving, blood-and-guts ode to the soldiers who died for the glory of the emperor and for Dai Nippon". 

==Release and reception==
Merdeka 17805 was released on 12 May 2001 in Japan and distributed by Toho.  It was financially successful in Japan. 
 national revolution.  Goto writes that, lacking proper background knowledge of the Japanese occupation, audiences were likely to "carry away an unduly favorable impression of Japans contribution to Indonesian independence". 

A Special Edition DVD version of the film was released by Happinet Pictures on 25 January 2002. 

==References==
 

==Works cited==
 
*{{Cite web
 |script-title=ja:ムルデカ17805
 |author=Cinema Topics staff
 |trans_title=Merdeka 17805
 |language=Japanese
 |work=Cinema Topics Online
 |url=http://www.cinematopics.com/cinema/works/output2.php?oid=39680
 |accessdate=28 May 2014
 |ref= 
}}
*{{Cite web
 |script-title=ja:ムルデカ　ＭＥＲＤＥＫＡ　１７８０５
 |trans_title=Merdeka 17805
 |language=Japanese
 |work=Kinema Junpo
 |url=http://www.kinenote.com/main/public/cinema/detail.aspx?cinema_id=32419
 |archiveurl=http://www.webcitation.org/6Psy1e5Xn
 |archivedate=27 May 2014
 |accessdate=27 May 2014
 |ref= 
}}
*{{Cite web
 |title=Merdeka
 |publisher=Rovie
 |work=AllMovie
 |ref= 
 |first=Johnathan
 |last=Crow
 |url=http://www.allmovie.com/movie/merdeka-v249826/
 |archivedate=27 May 2014
 |archiveurl=http://www.webcitation.org/6Psz0Ljph
 |accessdate=27 May 2014
}}
*{{Cite book
 |title=Tensions of Empire: Japan and Southeast Asia in the Colonial and Postcolonial World
 |publisher=Ohio University Press
 |year=2003
 |location=Athens, Ohio
 |ref=harv
 |url=http://books.google.co.id/books?id=MqmnPrgeYLEC
 |last=Goto
 |first=Kenichi
 |isbn=978-0-89680-231-5
}}
*{{Cite book
 |chapter=Multilayered Postcolonial Historical Space: Indonesia, the Netherlands, Japan and East Timor
 |title=A New East Asia: Toward a Regional Community
 |publisher=NUS Press
 |year=2007
 |location=Singapore
 |editor1=Kazuko Mōri
 |editor2=Kenichirō Hirano
 |ref=harv
 |url=http://books.google.co.id/books?id=lKvZVyfnAVMC
 |last=Goto
 |first=Kenichi
 |pages=20–43
 |isbn=978-9971-69-388-6
}}
*{{Cite web
 |title=Through Japanese Eyes: World War II in Japanese Cinema
 |publisher=United States Naval Institute
 |ref= 
 |date=14 April 2014
 |url=http://news.usni.org/2014/04/14/japanese-eyes-world-war-ii-japanese-cinema
 |archivedate=27 May 2014
 |archiveurl=http://www.webcitation.org/6PsnvtW9j
 |accessdate=27 May 2014
}}
 

==External links==
* 

 
 
 
 
 