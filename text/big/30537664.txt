Qualunquemente
{{Infobox film
|name= Qualunquemente
|image= Qualunquemente.png
|language=Italian
|country= Italy
|released=  
|runtime= 96 minutes
|director= Giulio Manfredonia
|screenplay= Antonio Albanese  Piero Guerrera   Giulio Manfredonia
|producer= Domenico Procacci Fandango
|distributor= 01 Distribution
|starring=Antonio Albanese Sergio Rubini
|cinematography= Roberto Forza
|music= Banda Osiris budget         =  gross          = € 15.869.000
}} Cetto La Qualunque, a sleazy Southern Italy politician.  The title means "whichever-ly" (adding ungrammatical adverbial endings is a shtick of the main character). It was released in Italy on 600 copies on January 21, 2011  and was screened in the Main Programme of the Panorama section at the 61st Berlin International Film Festival.      

== Plot ==
Corrupt and sleazy entrepreneur Cetto La Qualunque comes back to Italy and "jumps into politics"  lest his law-abiding opponent, Giovanni De Santis, is elected as mayor.   

Cetto La Qualunque is an entrepreneur from Calabria, a region in the South of Italy; hes very crude and vulgar, an embodiment of the Southern Italian society, also represented by corrupted politicians, administrators and auditors. While being a fugitive from the law in a Latin American country, Cetto met a beautiful girl, whom he calls "cosa" ("thing") (comparing her to an object); he conceived a daughter with her whose name he does not even remember. When Cetto gets back to his hometown of Marina di Sopra, in Calabria, his wife goes on a rampage after seeing his concubine alongside her husband. Cetto also has a son named Melo (Carmelo) who lived with his mother while he was away, and who is awkward and shy.

After Cetto makes his triumphal come back to his house, which looks like a typical mafia mansion, he goes to his club in the nearby town, where he meets his friends who support him in every wickedness he commits. Cetto also owns a campsite, wrongly named "Paradais Village" instead of "Paradise Village", a very poor and shabby place. During the meeting, his friends also tell him about his hated neighbour De Santiss intention to run for Mayor of Marina di Sopra, to improve the economic and cultural plight of the people, since almost every of them is poor peasants. Hearing this news, Cetto decides to run for Mayor of the town. He does everything possible to be voted by the people of the town, but his ignorance and narrow-mindedness does not impress journalists and reporters, so Cetto is forced to call a manager, Jerry Salerno, from Bari but living most recently in Milan. Cetto just sees it as a repentant Mafia since he left the South of Italy to go to the North, because even today in Italy there is still a lot of hatred among the citizens who live in the North and those who are in the South. Cetto begins the campaign with Jerry, using tricks, cheating and deceit against the ignorant people of Marina di Sopra.

Meanwhile Cetto also tries to make his son a man, forcing him to leave his girlfriend because she is not curvy enough; together the two of them go hunting illegally in protected areas, shoot at mannequins that resemble black men, and finally he makes Melo have sex with his favorite prostitute. Whatever the right arm with his friend John "The Stranger" (as Cetto is the Calabria while he was the Apulia) divorces his wife Carmela and plans to marry his new girlfriend from South America, and then sends to jail son Melo. In fact, the policeman Cavallaro is by years trying to send to jail Cetto, because of his abusive homes and its contacts with the Mafia, but he had no proof, and when he discovers that Cetto has a building permit of his pizzeria, this abusive comes into the house to arrest him, but Cetto has changed the document to the local making his son the owner and sending him to prison. This way, he has the opportunity to continue his campaign. Meanwhile his wife Concetta believes her husband to be a real heartless monster and leaves, while Cetto is preparing to hold its election speech in the square, and he promises procurement and abolition of taxes, but also guarantees the arrival of many prostitutes that he commonly nicknamed "pilu" (female pubic hair). Thanks to Jerry and corruption of the reporter Claogero, Cetto knocks the good intentions of De Santis and is elected mayor of Marina di Sopra. While hes partying, Jerry leaves Italy and brings with him the Brazilian girl to give her a dignified life and to provide the little daughter a good education.

==Cast==
*Antonio Albanese: Cetto La Qualunque
*Sergio Rubini: Gerardo "Jerry" Salerno
*Lorenza Indovina: Carmen La Qualunque
*Davide Giordano: Melo La Qualunque
*Antonio Gerardi: Tenent Cavallaro
*Massimo De Lorenzo: Mr. Calogero

==Reception==
Qualunquemente earned $7.4 million at the box office in its opening weekend.   Critics have drawn comparisons between the main character and former Italian Prime Minister Silvio Berlusconi, particularly regarding his contemporary sex scandals. 

==The character of the corrupt Italy of the present==
  plays Cetto La Qualunque]] French term "dames et monsieurs", promising the guy that he would have confirmed his enrollment at that university preparation to become skilled waiters!. His only reference point is the women who loves it and uses it as a simple household objects, demanding at times with the strength that his wife Carmen gets used to his concubine (roughly called "Thing") that he has decided to stay at his residence together with the small family. Carmen obviously can not tolerate the presence of a prostitute with a daughter, since she also has a son named Melo! He is a very shy and insecure eighteen-year-old boy, who was forced to be so precisely because of the lack of a father who has made four years on the run in Brazil. Cetto is also very rude and insensitive towards this boy, who is upset because his girlfriend is not quite shapely and especially when he accidentally kills him with the gun the favorite dog of Melo. Even pets unfortunately for Cetto are simple household items to pick up and throw it in the trash as soon as they start and become useless. Melo will be sorely tried by these cruel treatment, especially when the father, not to end up in jail, sends us the child, telling him unceremoniously that the prison is training for young people, almost better than a university. What is most surprising character in the film of Cetto is the fact that he wants to run for mayor of the small town to avoid ending up in jail. The other candidate is a quiet man and observing the laws of honesty. In short, he is a man exactly so. Cetto will be forced to destroy it so that it can take its place on the seat of mayor. He knows how to engage the gullible and ignorant people like him in his speeches in the square. Cetto promise every time plenty of prostitutes on the streets and total cancellation of taxes, which is quite impossible in a normal country. When asked what he plans to do for the people poor, displaced persons and the handicapped, Cetto answers that will leave them all to rot in the sewers, almost formulating a racial discourse similar to those of Mussolini. In the film there is also a sequence in which Cetto, all-round character now depicted as a pimp, and he begins to converse with isuoi henchmen and replication amazed: "crazy things!" when a colleague told him that in that country the police came to arrest a man only because he fired the gun to his neighbor, without wondering if this man was killed because he was a spy or a thug by half a point. With these facets extremely exaggerated, but also very truthful and realistic, Albanese creates a real monster of Italian society corrupted.

==Sequel==
Tutto tutto niente niente is the sequel. The film, whose shooting began in May 2012, will be released "in spite of the Maya" at Christmas 2012, directed by Giulio Manfredonia. The film, written by Antonio Albanese and Piero Guerrera, is produced by Domenico Procacci for Fandango and Leo with Rai Cinema and will be distributed at 01. The cast, in addition to starring Antonio Albanese, there are among others Paolo Villaggio, Nicola Rignanese, Fabrizio Bentivoglio, Lunetta Savino, Lorenza Indovina, Vito Teco Celio, Bob Messini, Luigi Maria Burruano, Davide Giordano, Maria Rosaria Russo and Alfonso Postiglione.

==References==
 

== External links==
*  
*  

 
 
 
 
 
 