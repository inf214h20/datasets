Manya Mahajanangale
{{Infobox film 
| name           = Manya Mahajanangale
| image          =
| caption        =
| director       = AT Abu
| producer       =
| writer         = NP Muhammed
| screenplay     = NP Muhammed Chithra Prem Nazir Adoor Bhasi Shyam
| cinematography = PS Nivas
| editing        = G Venkittaraman
| studio         = Akshara Films
| distributor    = Akshara Films
| released       =  
| country        = India Malayalam
}}
 1985 Cinema Indian Malayalam Malayalam film,  directed by AT Abu and produced by . The film stars Mammootty, Chithra (actress)|Chithra, Prem Nazir and Adoor Bhasi in lead roles. The film had musical score by Shyam (composer)|Shyam.    

==Cast==
*Mammootty Chithra
*Prem Nazir
*Adoor Bhasi
*Sabitha Anand Seema
*TG Ravi

==Soundtrack== Shyam and lyrics was written by Poovachal Khader. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Arayannakkili || K. J. Yesudas, Unni Menon, Lathika || Poovachal Khader || 
|-
| 2 || Kandille Kandille || K. J. Yesudas, Chorus, CO Anto || Poovachal Khader || 
|-
| 3 || Maanyamahaajanangale || P Jayachandran, Unni Menon, CO Anto || Poovachal Khader || 
|-
| 4 || Pathinezhaam Vayassinte || S Janaki, Chorus || Poovachal Khader || 
|}

==References==
 

==External links==
*  

 
 
 

 