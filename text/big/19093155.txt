New York (film)
 
 
{{Infobox film
| name           = New York
| image          = New-York-movie-poster.jpg
| alt            =  
| caption        = Theatrical release poster Kabir Khan
| producer       = Aditya Chopra
| screenplay     = Sandeep Srivastava
| story          = Aditya Chopra John Abraham Katrina Kaif Neil Mukesh Irrfan Khan
| music          = Pritam Pankaj Awasthi Julius Packiam
| cinematography = Aseem Mishra
| editing        = Rameshwar S Bhagat
| studio         = Yash Raj Films
| distributor    = Yash Raj Films
| narrator = John Abraham
| released       =  
| runtime        = 153 minutes
| country        = India
| language       = Hindi
| budget         =  
| gross          =  
}} Kabir Khan, Visual Computing John Abraham, its aftermath.

==Plot== John Abraham), whom he hasnt seen in seven years and who the FBI believes is a terrorist. In the process, Omar discovers that Sam has married Maya (Katrina Kaif) (whom Omar had a crush on in university and another friend) and finds out that Samir and Maya have a young son, Danyal (Aidan Wagner).

Roshan orders Omar to tell him everything he knows about Samir. The film then flashes back to September 1999, when Omar begins his studies at (the fictional) New York State University. He is befriended by his international student counselor Maya and learns that though she was born and raised in New York, she is fluent in Hindi because of her mothers interest in Bollywood films. Omar also meets Sam, another Indian American who is also Muslim and fluent in Hindi due to the fact that his father is a professor of Indian studies. Over the next two years, all three become inseparable friends and gradually Omar falls in love with Maya. When Omar realises that she loves Sam, however, he distances himself from them both. Their carefree days finally end with the onset of September 11 attacks|9/11.

After finishing his story, Omar agrees to help Roshan (rather reluctantly), if only to prove that both he and Sam are innocent. He reunites with Maya and Sam and stays in their house, all the while spying for the FBI. Omar learns that Maya is a civil rights activist who is helping one of Sams employees, Zilgai (Nawazuddin Siddiqui) overcome his experience as a former Guantanamo Bay detention camp|9/11 detainee. Zilgai was eventually released due to lack of evidence and has been having difficulty adjusting back to "normal" life.

As time progresses, Omar feels satisfied that he can find nothing to warrant the FBIs suspicions and is ready to leave when a series of events forces him to reconsider. In the process, Omar learns from Sam that ten days after 9/11, Sam was arrested and detained for a period of nine months as a suspected terrorist, a charge which everyone, including the FBI and Roshan, now agree was incorrect. Though he was eventually released due to lack of evidence, the impact of being detained and tortured permanently changed Sam in ways which are difficult for those surrounding him to understand, leaving him with feelings of deep resentment towards the FBI. Omar thus finds that Sam ultimately resorted to plans for terrorism as a means of revenge.

In addition, Maya is unable to help Zilgai resolve the trauma of being a detainee. After a routine traffic stop escalates and an NYPD police officer gives Maya a very rough full-body search, Zilgai becomes agitated. He drops Maya at her home and eventually kills the police officer the same night. After being declared a fugitive, Zilgai leads the police on a long chase ultimately ending in his suicide.
 climax of the film rests upon the attempts of Maya, Omar, and Roshan to prevent Sam from committing an act of terrorism by telling him that if he perpetuates towards terrorism, others will suffer as he has. Finally convinced, Sam surrenders and aborts his attempt to bomb the FBI building. However, the moment he drops his cell-phone (which was originally intended as a detonator for the bomb) he is shot and killed by FBI snipers. The cell phone falls benign to the ground without activating anything. Maya, who was running toward Sam, is also killed by stray gunfire and Omar, bereft of speech, breaks down. Six months later, he is later comforted by Roshan who explains to him that; everybody was right in their place, but the timing was wrong. As for Sam, the path he chose killed him. Everybody has moved on after 9/11, as its high time. Omar has adopted Danyal, and Roshan has received commendation for aiding in the anti-terrorism cause. They reconcile each other. The film ends with all three of them going out for pasta and a side note describing the after effects of 9/11.

==Cast== John Abraham as Samir Shaikh
* Katrina Kaif as Maya Shaikh
* Neil Nitin Mukesh as Omar Aijaz
* Irrfan Khan as Roshan
* Nawazuddin Siddiqui as Zilgai
* Samrat Chakrabarti as Yakub
* Aidan Wagner as Danyal
* Jakir Ajmeri

==Themes== Kabir Khan argued that the film "is based on part of the political canvas of 9/11, but it speaks of prejudices after the great human tragedy. It is a definite and very relevant subject about post-9/11 prejudices that have increased after the attacks   We have in fact divided time in a pre- and post-9/11 world in the film to highlight its obvious repercussions in times to come. The repercussions of the attacks are still very strongly felt globally and will continue to do so." {{cite web
|url=http://www.hindustantimes.com/StoryPage/StoryPage.aspx?sectionName=HomePage&id=1de96890-efb4-4591-b106-7bc12836572d&Headline=EMNew+York+/EMis+on+post-9/11+prejudices:+Kabir+Khan
|title=New York is on post-9/11 prejudices: Kabir Khan
|accessdate= 2009-06-29
|author=
|last=Bansal
|first= Robin
|authorlink=
|coauthors=
|date=  13 June 2009
|work=
|publisher= Hindustan Times John Abraham argued that, "in its own strange way, New York begins where Khuda Ke Liye ended. That’s the interesting part of the film   Each director has his own way of interpreting and researching the plight of legal detainees." Abraham continued by suggesting that this is why Khan and Shoaib Mansoor offer different interpretations of these events in their respective films.The plot of this movie is similar to the season 1 and episode 21 of TV series lost. {{cite web
|url=http://www.hindu.com/holnus/009200906241031.htm
|title=New York begins where Khuda Kay Liye ends: John Abraham
|accessdate= 2009-06-29
|author=IANS
|last=
|first=
|authorlink=
|coauthors=
|date=  24 June 2009
|work=
|publisher= The Hindu
}}  New York was the first Indian film which focused on this theme, later followed by Karan Johars My Name is Khan, starring Shahrukh Khan and Kajol, Kurbaan (2009 film)|Kurbaan, starring Saif Ali Khan and Kareena Kapoor and Vishwaroopam, starring Kamal Hassan and Andrea Jeremiah.

==Production==
Production began in September 2008 and filming lasted over a period of 100 days. Most of the shoot took place in New York although some of the scenes which took place in New York were actually shot in Philadelphia. New York is the first Hindi film to have a production schedule there. {{cite web
|url=http://sify.com/news/fullstory.php?a=jgwpkthedgf&title=New_York_on_9_11_repercussions_to_break_dry_spell_IANS_Preview
|title=New York on 9/11 repercussions to break dry spell (IANS Preview)
|accessdate= 2009-06-29
|author=IANS
|last=
|first=
|authorlink=
|coauthors=
|date=  22 June 2009
|work=
|publisher= sify.com
}}  {{cite web
|url=http://www.bollywoodhungama.com/features/2008/10/13/4395/index.html
|title=Yash Raj Films New York is being shot in Philadelphia
|accessdate= 2009-06-29
|author=
|last=
|first=
|authorlink=
|coauthors=
|date=  13 October 2008
|work=
|publisher= bollywoodhungama.com
}}  In preparation for his role as an American Muslim of Indian origin, Abraham studied the Quran (he had likewise studied Sanskrit and learned to play the flute for his role in Water (2005 film)|Water). {{cite web
|url=http://in.movies.yahoo.com/news-detail/56544/John-Abraham-studied-Quran-New-York-role.html
|title=John Abraham studied Quran for New York role
|accessdate= 2009-06-29
|author=IANS
|last=
|first=
|authorlink=
|coauthors=
|date=  18 June 2009
|work=
|publisher=yahoo.com
}}  Khan had to submit his script for approval from U.S. authorities before making the film. He stated: "We had to submit the script to seek permission to shoot in Guantanamo as well as in the States and we got their green signal to go ahead very easily. The film may be about 9/11 and what happens post it, but they did not object to our theme and did not even raise any questions. The US officials were very cooperative and we shot for around three days at this high-profile prison." {{cite web
|url=http://www.dnaindia.com/entertainment/report_kabir-khan-gets-script-searched_1266599
|title=Kabir Khan gets script searched
|accessdate= 2009-06-30
|author=IANS
|last=
|first=
|authorlink=
|coauthors=
|date=  19 June 2009
|work=
|publisher=DNA
}} 

==Release==

===Box-office=== Rs 350 million during its first three days in India, with theatres at their highest occupancy since January at 80–85%. {{cite web
|url=http://www.nasdaq.com/aspx/stock-market-news-story.aspx?storyid=200906300344dowjonesdjonline000089&title=bollywood-boosted-by-blockbuster-weekend-for-"new-york"
|title=Bollywood Boosted By Blockbuster Weekend For "New York"
|accessdate= 2009-06-30
|author=AFP
|last=
|first=
|authorlink=
|coauthors=
|date=29 June 2009
|work=
|publisher= NASDAQ
}}  During its first week, it ranked number one in the box office in India {{cite web
|url=http://in.movies.yahoo.com/boxoffice.html?date=2009-06-26
|title=Weekend Bollywood Box Office: 26 June – 2 July 2009 Week
|accessdate= 2009-07-07
|author=
|last=
|first=
|authorlink=
|coauthors=
|date=7 July 2009
|work=
|publisher= Yahoo! India Movies Rs 618.9&nbsp;million worldwide, and was declared a hit. {{cite web
|url=http://www.indiaglitz.com/channels/hindi/article/48064.html
|title=‘New York’ and grand collections
|accessdate= 2009-10-03
|author=
|last=
|first=
|authorlink=
|coauthors=
|date=  4 July 2009
|work=
|publisher= boxofficeindia.com Rs  . {{cite web
|url=http://www.bollywoodhungama.com/movies/boxoffice/13830/index.html
|title=Bollywood Hungama:Box Office for New York
|accessdate= 2009-10-03
|author=
|last=
|first=
|authorlink=
|coauthors=
|date=
|work=
|publisher= Bollywood Hungama
}} 

New Yorks opening weekend was highly successful in India and the Middle East. It also proved popular in Australia and did well in the UK and U.S. Of its opening in India, Khan commented: "I always believed that New York will be a word of mouth film which would open on a decent note and then show an increase in business with every passing day. When I was told by friends and people from industry that it had opened to a houseful response in the very first show at so many places across the country, even I was taken aback."  {{cite web
|url=http://www.bollywoodhungama.com/features/2009/07/01/5277/index.html
|title=New Yorks super-success has surprised me" – Kabir Khan
|accessdate= 2009-07-01
|author=
|last=Tuteja
|first= Joginder
|authorlink=
|coauthors=
|date=1 July 2009
|work=
|publisher= Bollywood Hungama
}} 
It is the sixth highest grossing film of 2009. It grossed Rs.  .

===Critical reception===
New York was well received by a number of critics. Subhash K. Jha  gave New York a rave review arguing that New York "is what cinema in contemporary times should be, must be, though it seldom is" and designates it  "an important film" which "cares about the prejudices that have taken over the world. Jha also states that, "stereotypical portrayals of the cultural diaspora are fortunately rare in this piece of contemporary art which has plenty of heart, a heart that never overflows in an embarrassing torrent of emotions." {{cite web
|url=http://timesofindia.indiatimes.com/Entertainment/Bollywood/New-York-a-remarkable-effort/articleshow/4709214.cms
|title=New York, a remarkable effort
|accessdate= 2009-06-26
|author=
|last=Jha
|first= Subhash
|authorlink=
|coauthors=
|date=  27 June 2008
|work=
|publisher= Times of India
}}  Devansh Patel, film critic for Londons Hounslow Chronicle, {{cite web
|url=http://www.hounslowchronicle.co.uk/authors/devansh-patel/
|title=Devansh Patel Biography
|accessdate= 2009-10-01
|author=
|last=
|first=
|authorlink=
|coauthors=
|date=
|work=
|publisher=  Hounslow Chronicle
}}  gave New York five out of five stars stating that it is, "the most thought provoking movie Yash Raj Films has ever come up with." {{cite web
|url=http://filmifarmoola.blogspot.com/2009/06/new-york-gives-us-lifetime-movie-of.html
|title=NEW YORK film review – 5 stars. The film carries an emotional impact and seeds a bed of uplifting pride for the people of America
|accessdate= 2009-10-01
|author=
|last=Patel
|first= Devansh
|authorlink=
|coauthors=
|date=
|work=
|publisher=
}}  Nikhat Kazmi of the Times of India gave it four out of four stars, describing New York as, "an extremely taut and highly emotive piece of political drama   topical, meaningful, and entertaining, all at the same time." {{cite web
|url=http://timesofindia.indiatimes.com/New-York/articleshow/4707568.cms
|title=Times of India Review: New York
|accessdate= 2009-06-26
|author=
|last=Kazmi
|first=Nikhat
|authorlink=
|coauthors=
|date=  26 June 2009
|work=
|publisher=Times of India Yash Raj   the screenplay is its biggest star, without a doubt. Given the fact that New York isnt one of those routine masala fares, Kabir has injected songs only when required. Cinematography is striking." {{cite web
|url=http://www.bollywoodhungama.com/movies/review/13830/index.html
|title=Bollywood Hungama Review: New York
|accessdate= 2009-06-26
|author=
|last=Adarsh
|first= Taran
|authorlink=
|coauthors=
|date=  26 June 2009
|work=
|publisher= Bollywood Hungama
}}  Joginder Tuteja of the Indo-Asian News Service (IANS) calls the film "a must watch" and  gave it four out of four stars. He states: "When a hardcore commercial flick gets a standing ovation and a huge round of applause at the end of the show, you know that there is something definitely right that the director has done. In this regard, Kabir Khan can take a bow because he has done exceedingly well in making a film that is not frivolous, has a message and still carries enough commercial ingredients to reach out to masses as well as classes."  {{cite web
|url=http://in.news.yahoo.com/43/20090626/914/ten-new-york-a-brilliant-film-and-a-must.html
|title=‘New York’ a brilliant film and a must watch
|accessdate= 2009-06-26
|author=
|last=Tuteja
|first= Joginder
|authorlink=
|coauthors=
|date=  26 June 2009
|work=
|publisher= IANS
}}    Mayank Shekhar of the Hindustan Times argues that comparing New York "to Mark Pellington’s Arlington Road would be grossly unfair. If anything, this is a much better movie than that 1999, part-spooky conspiracy theory. The central theme itself is closer to Shoaib Mansoor’s Khuda Kay Liye (2007), and you can sense how the superior execution here makes all the filmmaking difference." {{cite web
|url=http://www.hindustantimes.com/StoryPage/StoryPage.aspx?sectionName=HomePage&id=8a634d26-76dc-4e86-a90f-9bd262ebd14e&Headline=Review%3a+EMNew+York%2fEM
|title=For sure, NY see!
|accessdate= 2009-06-28
|author=
|last=Shekhar
|first= Mayank
|authorlink=
|coauthors=
|date=  27 June 2009
|work=
|publisher= Hindustan Times
}}  Additionally, Shekhar describes New York in NDTV as, "an A-list film that gets a straight A." {{cite web
|url=http://movies.ndtv.com/reviews.asp?lang=hindi&id=400&moviename=New+York
|title=NDTV Review: New York
|accessdate= 2009-06-26
|author=
|last=Shekhar
|first= Mayank
|authorlink=
|coauthors=
|date=  26 June 2009
|work=
|publisher= NDTV
}}  Sandhya Iyer of the Sakaal Times gave the film three and a half out of four stars arguing that "New York manages to be gripping, thanks to an excellent screenplay. Most importantly, the story has the right mix of realism and drama – one that Kabir Khan unfolds with quiet confidence." {{cite web
|url=http://www.sakaaltimes.com/2009/06/27170631/A-marriage-between-fact-and-fi.html
|title=A marriage between fact and fiction
|accessdate= 2009-06-27
|author=
|last=Iyer
|first= Sandhya
|authorlink=
|coauthors=
|date=  27 June 2009
|work=
|publisher= Sakaal Times
}}    Mathures Paul of The Statesman states: "Finally, here’s a film that entertains and makes viewers think."  {{cite web
|url=http://www.thestatesman.net/page.news.php?clid=19&theme=&usrsess=1&id=259157
|title=Nurturing Relationships
|accessdate= 2009-06-26
|author=
|last=Paul
|first=Mathures
|authorlink=
|coauthors=
|date=  27 June 2009
|work=
|publisher=The Statesman
}}   

New York received mixed reviews from other critics. Rachel Saltz of The New York Times states: "While Mr. Khan’s depictions of American life occasionally seem silly and the plot has some crater-size holes, New York is continually fascinating. It benefits from the performance of Irrfan Khan, who adds layers of complexity to his character."  {{cite web
|url=http://movies.nytimes.com/2009/06/27/movies/27saltz.html
|title=Friendships in the Big City, Bent by 9/11
|accessdate= 2009-06-26
|author=
|last=Saltz
|first=Rachel
|authorlink=
|coauthors=
|date=  27 June 2009
|work=
|publisher=The New York Times
}}  Lisa Tsering of the Hollywood Reporter argues that "although the film is a routine thriller with few surprises, it deserves attention because its topic, even eight years after Sept. 11, is one that many South Asian Americans still take very seriously." {{cite web
|url=http://www.hollywoodreporter.com/hr/film-reviews/new-york-film-review-1003989212.story
|title=Hollywood Reporter Review: New York
|accessdate= 2009-06-30
|author=
|last=Tsering
|first=Lisa
|authorlink=
|coauthors=
|date=  30 June 2009
|work=
|publisher=Hollywood Reporter
}}    John Anderson of Variety (magazine)|Variety suggests that New York has "a certain amount of silliness early on" but is "a courageous movie in many ways, and a surprising one." {{cite web
|url=http://www.variety.com/review/VE1117940607.html?categoryid=1043&cs=1
|title=Variety review: New York
|accessdate= 2009-07-03
|author=
|last=Anderson
|first= John
|authorlink=
|coauthors=
|date=3 July 2009
|work=
|publisher=Variety
}} Rajeev Masand of CNN-IBN gave the film two out of five stars and states: "New York has its heart in the right place and its intentions are entirely honorable   but because the film is constructed from such a sloppy script, that point is lost under all the creative liberties and convenient short-cuts that the screenplay takes." {{cite web
|url=http://ibnlive.in.com/news/masands-movie-review-new-york-not-convincing/95750-8.html
|title=Masands movie review: New York not convincing
|accessdate= 2009-06-27
|author=
|last=Masand
|first=Rajeev
|authorlink=
|coauthors=
|date=  27 June 2009
|work=
|publisher=CNN-IBN
}} 

New York is rated 35/100 on Metacritic, citing generally unfavorable reviews. 

===High profile screenings=== Kabir Khan, was in attendance and addressed the audience at the end of its screening. {{cite web
|url=http://www.indianexpress.com/news/identity-proof/553010/0
|title=Identity Proof
|accessdate= 2009-12-13
|author=
|last=Khan
|first=Kabir
|authorlink=
|coauthors=
|date=  13 December 2009
|work=
|publisher=Indian Express
}}  {{cite web
|url=http://www.outlookindia.com/article.aspx?262991
|title=Al Hind: A Relook
|accessdate= 2009-12-13
|author=
|last=Joshi
|first=Namrata
|authorlink=
|coauthors=
|date=  7 December 2009
|work=
|publisher=Outlook India Asian Filmmaker of the Year award. {{cite web
|url=http://www.businessofcinema.com/news.php?newsid=14709
|title=Yash Chopra honored at Pusan International Film Festival
|accessdate= 2009-11-05
|author=
|last=
|first=
|authorlink=
|coauthors=
|date=  14 October 2009
|work=
|publisher=Business of Cinema
}}  {{cite web
|url=http://timesofindia.indiatimes.com/entertainment/bollywood/news-interviews/Look-whos-headed-to-Cairo/articleshow/5196464.cms
|title=Look who’s headed to Cairo
|accessdate= 2009-11-05
|author=
|last=
|first=
|authorlink=
|coauthors=
|date=  14 October 2009
|work=
|publisher=Times of India
}} 

==Soundtrack==
{{Infobox album
| Name        = New York
| Type        = soundtrack
| Artist      = Pritam
| Cover       = newyork albumcover.jpg
| Released    =  
| Recorded    = 
| Genre       = Film soundtrack
| Length      =  YRF Music
| Producer    = Aditya Chopra
| Last album  = Billu  (2008)
| This album  = New York  (2009)
| Next album  = Ajab Prem Ki Ghazab Kahani  (2009)
}}
Released on 10 June 2009, the soundtrack for New York has composed by Pritam, Julius Packiam and Pankaj Awasthi. Lyrics are penned by Sandeep Shrivastava and Junaid Wasi.

{{Tracklist
| collapsed       =
| headline        =
| extra_column    = Singers
| total_length    =
| all_writing     =
| all_lyrics      =
| all_music       =
| writing_credits =
| lyrics_credits  =
| music_credits   =
| title1          = Hai Junoon
| note1           = 
| writer1         = 
| lyrics1         =
| music1          = KK
| length1         = 05:31
| title2          = Mere Sang
| note2           =
| writer2         =
| lyrics2         =
| music2          =
| extra2          = Sunidhi Chauhan
| length2         = 06:28
| title3          = Tune Jo Na Kahan
| note3           =
| writer3         =
| lyrics3         =
| music3          =
| extra3          = Mohit Chauhan
| length3         = 05:09
| title4          = Aye Saaye Mere
| note4           =
| writer4         =
| lyrics4         =
| music4          =
| extra4          = Pankaj Awasthi
| length4         = 05:45
| title5          = Hai Junoon – Remix
| note5           =
| writer5         =
| lyrics5         =
| music5          =
| extra5          = KK, Monali Thakur
| length5         = 06:08
| title6          = Mere Sang – Remix
| note6           =
| writer6         =
| lyrics6         =
| music6          =
| extra6          = Sunidhi Chauhan
| length6         = 05:55
| title7          = Sams Theme
| note7           =
| writer7         =
| lyrics7         =
| music7          =
| extra7          = Caralisa Monteiro
| length7         = 04:04
| title8          = New York Theme
| note8           =
| writer8         =
| lyrics8         =
| music8          =
| extra8          = Instrumental
| length8         = 03:09
}}

==Home video==
The DVD for New York was released on 8 August 2009. {{cite web
|url=http://www.amazon.com/New-York-Dvd-John-Abraham/dp/B002HI6LGK/ref=sr_1_6?ie=UTF8&s=dvd&qid=1254416959&sr=1-6
|title=Amazon: New York DVD
|accessdate= 2009-10-01
|author=
|last=
|first=
|authorlink=
|coauthors=
|date=
|work=
|publisher=Amazon
}}  Joginder Tuteja of Bollywood Hungama gave the DVD four out of five stars. The DVD includes (but is not limited to) 2.5 hours of special features such as: "Making of the Film," "Deleted Scenes," "New York Special – Zoom TV,"
"The New Yorkers – CNN-IBN," and a number of music videos. {{cite web
|url=http://www.bollywoodhungama.com/movies/dvdreview/13830/index.html
|title=Bollywood Hungama: DVD review
|accessdate= 2009-10-01
|author=
|last=Tuteja
|first=Joginder
|authorlink=
|coauthors=
|date= 20 August 2009
|work=
|publisher=Bollywood Hungama
}} 

==See also==
*List of cultural references to the September 11 attacks
*The Reluctant Fundamentalist

==References==
 

==Further reading==
 
*Bansal, Robin. " ," Times of India, 21 June 2009.
*Jamkhandikar, Shilpa. " ." Reuters, 20 June 2009.
*Jha, Subhash K. " ," Times of India, 6 July 2009.
* , 13 December 2009.
*Parab, Kanika. " ." Huffington Post, 29 June 2009.
*Patel, Devansh. " ," Bollywood Hungama, 18 June 2009.
* , 20 June 2009.
*Syed, Aijaz Zaka. " ." Khaleej Times, 29 July 2009.
*Zaman, Rana Siddiqui. " ." The Hindu, 29 June 2009.
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 