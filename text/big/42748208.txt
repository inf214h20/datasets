Doctor Antonio (1937 film)
{{Infobox film
| name = Doctor Antonio
| image =
| image_size =
| caption =
| director = Enrico Guazzoni
| producer = 
| writer = Giovanni Ruffini (novel)   Gherardo Gherardi   Enrico Guazzoni   Gino Talamo
| narrator =
| starring = Ennio Cerlesi   Maria Gambarelli   Lamberto Picasso   Tina Zucchi 
| music = Umberto Mancini   Giovanni Fusco 
| cinematography = Massimo Terzano
| editing = Gino Talamo 
| studio = Manderfilm 
| distributor =Manderfilm 
| released = 1937
| runtime = 103 minutes
| country = Italy Italian 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}} historical drama novel of the same title by Giovanni Ruffini. It was made at the Cinecittà Studios in Rome.

==Cast==
* Ennio Cerlesi as Il dottor Antonio 
* Maria Gambarelli as Miss Lucy 
* Lamberto Picasso as Sir John Davenne 
* Tina Zucchi as Speranza 
* Vinicio Sofia as Turi 
* Mino Doro as Prospero 
* Margherita Bagni as Miss Elizabeth 
* Claudio Ermelli as Tom 
* Luigi Pavese as Aubrey 
* Giannina Chiantoni as Rosa 
* Romolo Costa as Hasting 
* Augusto Di Giovanni as Ferdinando II di Napoli 
* Guido Celano as Domenico Morelli 
* Enzo Biliotti as Carlo Poerio 
* Alfredo Menichelli as Luigi Settembrini 
* Massimo Pianforini as Lord Cleverton 
* Rocco DAssunta as Michele Pironti 
* Vittorio Bianchi as Il dottore Stage 
* Alfredo Robert as Il generale Nunziante 
* Enzo De Felice as Romeo 
* Olinto Cristina as Ambasciatore inglese 
* Giuseppe Duse as Ufficiale borbonico 
* Achille Majeroni as Lavvocato dellaccusa 
* Giovanni Onorato as Un oratore in piazza 
* Aristide Garbini as Conspiratore 
* Pietro Tordi as Laltro conspiratore  Luigi Esposito 
* Cesare Fantoni
* Giovanni Ferraguti Giovanni Ferrari
* Alessio Gobbi
* Gilberto Macellari
* Michele Malaspina
* Ermena Malusardi
* Ornella Da Vasto 
* Alessandra Varna

== References ==
 

== Bibliography ==
* Goble, Alan. The Complete Index to Literary Sources in Film. Walter de Gruyter, 1999.
 
== External links ==
*  

 

 
 
 
 
 
 
 
 
 
 
 


 