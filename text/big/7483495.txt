Gemini (2002 Tamil film)
 
 
 
{{Infobox film
| name = Gemini
| image = Gemini DVD Cover.jpg
| alt = 
| caption = DVD cover Saran
| producer = {{Plainlist|
* B. Gurunath
* M. Balasubramanian
* M. Saravanan
* M. S. Guhan}}
| writer = Saran
| starring = {{Plainlist| Vikram
* Kiran Rathod}}
| music = Bharathwaj
| cinematography = A. Venkatesh (cinematographer)|A. Venkatesh
| editing = Suresh Urs
| studio = Royal Film Company
| distributor = AVM Productions
| released =  
| runtime = 157 minutes  
| country = India
| language = Tamil
}} Tamil language Saran and Vikram in don who, Murali stars Manorama and Thennavan portray significant roles. Based on gang wars in Chennai, the film delves into the lives of outlaws and the roles the police and society play in their rehabilitation and acceptance.
 Ajith intended to play the lead role, but the project was shelved after a weeks shoot when Ajith lost interest and left. Saran reworked the script, retitled the production Gemini and cast the new leads. The revived project was announced in August 2001. Production began shortly afterwards in December the same year and was completed by March 2002. The film was shot mainly at the AVM Studios in Chennai, while two song sequences were filmed in Switzerland. The film had cinematography by A. Venkatesh (cinematographer)|A. Venkatesh and editing by Suresh Urs while the soundtrack was scored by Bharathwaj.
 Filmfare Awards, ITFA Awards Telugu as Gemeni (film)|Gemeni.

== Plot ==
Teja (Kalabhavan Mani) is a high-profile gangster in North Madras, who often imitates the behaviour characteristic of different animals for sarcastic effect. Accompanied by his gang, he arrives at a magistrates court for a hearing. His animal antics are mocked at by "Chintai" Jeeva, another accused. Teja and his gang retaliate and kill Jeeva within the court premises. Jeeva was a member of a rival gang headed by Gemini (Vikram (actor)|Vikram), an aspiring goon from Chintadripet who aspires to dethrone Teja and take his place. To avenge Jeevas death, Gemini hunts down the murderer Pandian while Isaac, one of Geminis men, kills him. This incident leads to a feud between Gemini and Teja, and a fight for supremacy ensues. Pandians mother Annamma (Manorama (Tamil actress)|Manorama), a destitute woman, locates the whereabouts of her sons murderers. She approaches Gemini, becomes the gangs cook and awaits a chance to poison them.
 Marwari woman. To pursue her, he joins an evening class at the college that she attends and she falls in love with him, unaware of his true identity. Two businessmen approach Gemini to evict traders from a market so that a shopping complex can be built in its place. As the market is in his control, Gemini refuses the offer and the businessmen hire Teja to execute the job. Feigning an altercation with Gemini, his sidekick Kai (Thennavan (actor)|Thennavan) joins Tejas gang, acts as the inside man and foils the plan. Teja becomes enraged at being outsmarted by Gemini.

Singaperumal (Murali (Malayalam actor)|Murali), an astute police officer, is promoted to the position of Director General of Police (DGP). Keen on eradicating crime, he arrests both Gemini and Teja and the arrests are made "off the record" owing to their political influence. Aware of the rivalry between them, Singaperumal puts them in a private cell so they can beat each other to death. While Teja tries to exact revenge for the market issue, Gemini does not fight back but persuades Teja to trick Singaperumal by pleading guilty and requesting a chance to reform. Geminis trick works and they are released.

Since Gemini was arrested at the college, Manisha discovers his identity and resents him. To regain her attention, Gemini reforms his ways. Though his gang initially disapprove of it, they relent. As Gemini and his gang regret their actions, Annamma reveals her true identity and forgives them. Singaperumal helps Gemini to get back into college and reunite with Manisha. Teja returns to his gang and continues his illegal activities. He pesters Gemini to help him in his business. Gemini informs Singaperumal of Tejas activities; Teja is caught smuggling narcotics, is prosecuted and serves a term in prison.

A few months later, Singaperumal is transferred and a corrupt officer (Vinu Chakravarthy) takes his place. The current DGP releases Teja and together, they urge Gemini to work for them and repay for the losses they incurred but Gemini refuses. To force him to return to his old ways, Teja persuades Isaac to conspire against Gemini. With Isaacs help, Teja plots and kills Kai. Gemini is infuriated and confronts Teja to settle the issue. During the fight, Gemini beats up Teja and swaps their clothes, leaving Teja bound and gagged. The new DGP arrives and shoots Gemini dead; he later realises that he had actually shot Teja who was in Geminis clothes. While the DGP grieves over Tejas death, he receives news that he has been transferred to the Sewage Control Board.

== Cast ==
  Vikram as Gemini, an aspiring don who reforms later
* Kiran Rathod as Manisha Natwarlal, a free-spirited Marwari woman
* Kalabhavan Mani as Teja, a don who mimics animals
* Vinu Chakravarthy as a power-obsessed and corrupt police officer Murali as Singaperumal, a sincere and dignified police officer
* Charle as Chinna Salem, a pimp operating a mobile brothel
* Ramesh Khanna as Gopal Master of Arts|M.A., a professor at the evening college auto workshop from where the gang operates
* Vaiyapuri as Oberoi, Dawoods sidekick Rani as Kamini, a divorcée and Geminis classmate who lusts for him Thennavan as Kai, Geminis loyal deputy
* Isaac Varghese as Isaac, Geminis gang member who betrays him later Thyagu as Sammandham, a police officer
* Madhan Bob as R. Anilwal Indian Police Service|I.P.S, a police officer trying to eradicate prostitution
* Ilavarasu as Commissioner of Police
* Omakuchi Narasimhan as Bombay Dawood (named after Dawood Ibrahim), a butcher
* Gemini Ganesan in a cameo as himself Manorama as Annamma, a woman who seeks to avenge her sons death

== Production ==

=== Development ===
  }} DCP of Saran came across a newspaper article carrying this piece of news and was fascinated.     Shortly afterwards in March 2001, Saran announced his next directorial venture would be inspired by the incident. Titled Erumugam (meaning "upward mobility"), the project was scheduled to enter production after the completion of Alli Arjuna. 
 Ajith in the lead after the success of Kaadhal Mannan (1998) and Amarkalam (1999). Laila Mehdin and Richa Pallod, who played the heroine in Sarans Parthen Rasithen (2000) and Alli Arjuna respectively, were to play the female lead roles. While the recording for the films audio reportedly began on 16 March 2001, the filming was to start in mid-June and continue until August that year, followed by post-production work in September. It was planned to release the film on 14 November 2001 coinciding with Diwali.  However, after finding a more engaging script in Red (2002 film)|Red (2002), Ajith lost interest; he left the project after a weeks shoot and the production was shelved. Following this incident, Saran stated that he would never do another film with Ajith.    The pair would however reconcile their differences later, and collaborate on Attahasam (2004) and Aasal (2010).  

Saran rewrote the script based on gang wars in Chennai and began the project again. The film, then untitled, was announced in August 2001 with Vikram to star in the lead role.     The production was taken over by M. Saravanan (film producer)|M. Saravanan, M. Balasubramaniam, M. S. Guhan and B. Gurunath of AVM Productions.    The film was AVMs 162nd production and their first film after a five-year hiatus, their last production being Minsara Kanavu (1997), the release of which marked fifty years since their debut Naam Iruvar (1947).  }} By producing Gemini, AVM became one of the four film studios that had been producing films for over fifty years.    While titling the film, producer M. Saravanan chose Gemini among the many titles suggested to him, but because Gemini Studios was the name of a major production house, Saravanan wrote to S. Balasubramanian, editor of Ananda Vikatan and son of Gemini Studios founder S. S. Vasan, requesting permission to use the title. In response, Balasubramanian gave his consent. 

=== Cast and crew ===
  Malayalam actors Kalabhavan Mani and Murali were approached to play significant roles. 

Gemini is widely believed to be Manis first Tamil film,   though he had already starred in   (2001) and found him "very dignified". He chose Murali as he wanted that dignity for the role. Though he had planned to make Murali a villain at the end of the film, Saran decided against it because he was "amazed to see awe in everyones eyes when Murali entered the sets and performed". 
 Thyagu and Manorama form Brinda and Ashok Raja (dance). The music was composed by Bharathwaj and the lyrics were written by Vairamuthu. 

=== Filming === Hotel Connemara, mimicry artist, Saran asked him to exhibit his talents; Mani aped the behaviour of a few animals and Saran chose among them, which were added to the film. 
 Sangam (1964). Though there were problems in acquiring permission, executive producer M. S. Guhan persisted.  The overseas shoots were arranged by Travel Masters, a Chennai-based company owned by former actor N. Ramji.  Gemini was completed on schedule and M. Saravanan praised the director, saying, "...&nbsp;we felt like we were working with S. P. Muthuraman himself, such was Sarans efficiency". 

== Inspiration == Bhaktavatsalam colony DCP of Goondas Act during the films pre-production. 

When asked about his fascination for "rowdy themes", Saran said:
 

The characters of DGP Singaperumal and "Chintai" Jeeva were based on Shakeel Akhter and Vijayakumar respectively. Since the criminals were re-arrested after being given a chance, the initial script had Singaperumal turning villainous during the climax. When Saran felt that the audience would not be kind to him and that it would damage the film, he added another corrupt police officer to do the job while maintaining Singaperumal as a "very strong, good police officer". 

== Music ==
The soundtrack album and background score were composed by Bharathwaj. Since making his entry into Tamil films with Sarans directorial debut Kaadhal Mannan,  he has scored the music for most films directed by Saran.   The lyrics were written by poet-lyricist Vairamuthu.
 folk song. 

{{Infobox album 
| Name       = Gemini
| Longtype   = to Gemini
| Type       = Soundtrack
| Artist     = Bharadwaj
| Genre      = Film soundtrack
| Length     = 30:54 Tamil
| Label      = Five Star Audio Ayngaran Music An Ak Audio
| Producer   = Bharadwaj
| Last album = Roja Kootam   (2002)
| This album = Gemini   (2002)
| Next album = Thamizh   (2002)
}}

{{track listing
| headline      = Original Tracklist  
| extra_column  = Singer(s)

| title1      = Thala Keezha
| extra1      = Manikka Vinayagam
| length1     = 04:10

| title2      = Kaadhal Enbatha
| extra2      = Timothy
| length2     = 02:56

| title3      = Penn Oruthi
| extra3      = S. P. Balasubrahmanyam
| length3     = 05:11

| title4      = Deewana
| extra4      = Sadhana Sargam
| length4     = 04:26

| title5      = Kaadhal Enbatha&nbsp;– Sad
| extra5      = Bharathwaj
| length5     = 01:16

| title6      = O Podu
| extra6      = Anuradha Sriram, S. P. Balasubrahmanyam
| length6     = 04:03

| title7      = Naattu Katta
| extra7      = Shankar Mahadevan, Swarnalatha
| length7     = 04:52
}}
{{track listing
| headline       = Bonus Track
| extra_column  = Singer(s)
| total_length  =

| title8      = O Podu Vikram
| length8     = 04:00
}}

The songs were well received by the audience and the track "O Podu", in particular, was a hit.      Following the songs success, Vikram was greeted everywhere with screams of "O Podu".   He was overwhelmed by the response and, having already worked as a voice artist and singer, offered to sing his version of the song to which the producers agreed.  According to Vikram, the song was recorded and filmed the same morning, and was added to the soundtrack album a month after its initial release. The film had been completed by then and the additional track was featured during the closing credits.  Initially, a small footage featuring Vikram and Kiran was sent to the cinemas for screening during the end credits. However, the audience were dissatisfied with the shortened version of the song and forced theatre owners to rewind the song and play it again. After receiving calls from distributors and theatre owners, the makers eventually sent the entire song. 

  Filmfare Award.        The music also attracted some unexpected reactions. The high-energy track "O Podu" drove youngsters insane; some resorted to violence, enraging villagers in Tamil Nadu and damaging public property in Malaysia for fun.     The lyrics by Vairamuthu, which are typically in pure Tamil, contained slang and words from other languages like "Deewana" (Hindi).   This departure was criticised by film journalist Sreedhar Pillai, who derided the lyrics of "O Podu" as "pure gibberish".    

The music received positive reviews from critics. Sify wrote that Bharadwajs music was the films only saving grace.  Writing for Rediff, Pearl stated that the music director was "impressive".  Malathi Rangarajan of The Hindu said that the song by Anuradha Sriram has given the term "O! Podu!", which has been part of the "local lingo" for years, a "new, crazy dimension".  The song enjoyed anthem-like popularity   and according to V. Paramesh, a dealer of film music for 23 years, sold like "hot cakes".  It earned Rani on whom the song was picturised the moniker "O Podu Rani".  However, the songs picturisation attracted criticism. In an article that scrutinised and decried the high level of vulgarity depicted in south Indian films, Sudha G. Tilak of Tehelka wrote that hit tracks like "O Podu" were "obvious in their debauched suggestiveness". 
 south Indian songs that are considered a "national rage" in India.  

== Release and reception == signing autographs.  Since "O Podu" was such a hit among children, AVM invited young children to write reviews,  and gave away prizes.  In 2006, Saran revealed in a conversation with director S. P. Jananathan that his nervousness rendered him sleepless for four days until the film was released. 

=== Critical response ===
{{Multiple image
 | direction = vertical
 | width = 150
 | image1 = Chiyaan Vikram Raavan.jpg
 | image2 = Saran.tif 
 | footer = Vikram (top) received praise for his performance, while the script by Saran (bottom) received criticism.
}}
Gemini received mixed reviews from critics. Malathi Rangarajan of The Hindu wrote that the "Vikram style" action film was more of a stylised fare as realism was a casualty in many sequences. She added that while the credibility level of the storyline was low, Saran had tried to strike a balance in making a formulaic film.    Pearl of Rediff.com lauded Sarans "racy" screenplay but found the plot "hackneyed" and reminiscent of Sarans Amarkalam. The critic declared, "Gemini is your typical masala potboiler. And it works."    In contrast, Sify was critical of the film and wrote, "Neither exciting nor absorbing Gemini is as hackneyed as they get.   Saran should be blamed for this inept movie, which has no storyline and has scant regard for logic or sense."   

The performance of the lead also earned mixed response. Malathi Rangarajan analysed, "Be it action or sensitive enactment, Vikram lends a natural touch   helps Gemini score.   With his comic streak Mani makes himself a likeable villain."  Kalabhavan Manis mimicry and portrayal of a villain with a comic sense received acclaim from the critics and audience alike.   Rediff said, "The highlight in Gemini is undoubtedly Kalabhavan Manis performance.   As the paan-chewing Gemini, Vikram, too, delivers a convincing performance."  However, Sify found the cast to be the films major drawback and scrutinised, "Vikram as Gemini is unimpressive   Kalabhavan Mani an excellent actor hams as he plays a villain   Top character actor Murali is also wasted in the film." 

Following the films success, Vikram was compared with actor Rajinikanth. D. Govardan of The Economic Times wrote, "The films success has catapulted its hero, Vikram as the most sought after hero after Rajinikanth in the Tamil film industry today".  Rajinikanth, who saw the film, met Vikram and praised his performance.    Saran told in an interview that Rajinikanth was so impressed with the songs, he predicted the films success in addition to considering Rathod for a role in his film Baba (2002 film)|Baba (2002).   The films premise of an outlaw reforming his ways was appreciated. D. Ramanaidu of Suresh Productions the co-producer of the Telugu remake said, "The story of a rowdy sheeter turning into a good man is a good theme".  In August 2014, Gemini was featured in a list of "Top 10 Tamil Gangster Films" compiled by Saraswathi of Rediff.com.  

In an article discussing the rise of the gangster-based film becoming a genre in itself, Sreedhar Pillai wrote in a reference to Gemini:  

=== Box office ===
Gemini was a box office success and became the biggest hit of the year in Tamil.  Made on an estimated budget of  , the film grossed more than  .   while D. Govardan, writing for The Economic Times, claimed it to be  .   }}  The films success was largely attributed to the popularity of the song "O Podu".      D. Govardan of The Economic Times stated, "A neatly made  masala  (spice) film, with the song O Podu.. as its USP, it took off from day one and has since then not looked back".  Vikram, who was a struggling actor for almost a decade, credited Gemini as his first real blockbuster.  Sreedhar Pillai said that a good story, presentation and peppy music made it a "winning formula"  and declared "Gemini has been the biggest hit among Tamil films in the last two years".  However, the sceptics in the industry dismissed the films success as a fluke. 

The film ran successfully in theatres for more than 125 days.    Since most Tamil films released on the preceding Diwali and Pongal were not successful, Gemini helped the industry recover.  Outlook (magazine)|Outlook wrote, "Gemini has single-handedly revived the Tamil film industry."  The box office collections revived the fortunes of theatres that were on the verge of closure. AVM received a letter from the owner of New Cinema, a theatre in Cuddalore, who repaid his debts with the revenue the film generated.  Abirami Ramanathan, owner of the multiplex Abhirami Mega Mall, said that Geminis success would slow down the rapid closure of theatres from 2,500 to 2,000.    Following the success of the film, Saran named his production house "Gemini Productions", under which he produced films including Aaru (film)|Aaru (2005), Vattaram (2006) and Muni (film)|Muni (2007). 

=== Accolades ===
{|class="wikitable sortable"
|-
! Award
! Ceremony
! Category
! Nominee(s)
! Outcome
|- Filmfare Awards 50th Filmfare Best Music Director || Bharathwaj ||  
|- Best Female Playback Singer || Anuradha Sriram ||  
|- Best Villain || Kalabhavan Mani ||  
|- International Tamil 1st International Best Actor Vikram ||  
|- Best Villain || Kalabhavan Mani ||  
|- Best Singer Female || Anuradha Sriram ||  
|- Cinema Express 22nd Cinema Express Awards   || Best New Face Actress || Kiran Rathod ||  
|-
| Best Music Director || Bharathwaj ||  
|-
| Best Dialogue Writer || Saran ||  
|-
| Best Singer Female || Anuradha Sriram ||  
|}

== Legacy == Telugu as Venkatesh and Namitha in the lead roles,  while Kalabhavan Mani and Murali reprised their roles from the Tamil version.    Most of the crew members were retained.    Posani Krishna Murali translated the dialogues to Telugu.  The soundtrack was composed by R. P. Patnaik, who reused most of the tunes from the original film.  Released in October 2002,  the film received lukewarm response and failed to repeat the success of the original.   In 2013, a Kannada remake was reported to have been planned with Upendra in the lead, but he dismissed the reports as rumours. 
 Sun TV on Sundays with Raaghav as its anchor.   In September 2003, physical trainer Santosh Kumar played "O Podu" among a range of popular music as part of a dance aerobics session in a fitness camp held for the Indian cricket team at Bangalore. 
 assembly election, election commission to facilitate a separate button on the electronic voting machine. 

During the 2010 Asia Cup, a Sri Lankan band performed "O Podu" at the India vs. Pakistan cricket match held in Dambulla.  In July 2011, Vikram inaugurated "Liver 4 Life", an initiative launched by MIOT Hospitals to create awareness of the Hepatitis B virus. As the campaign was targeted at school and college students, the organisers tweaked the term "O Podu" into "B Podu" and made it the events tagline to capitalise on the songs immense appeal.     In a comical sequence in the film, Dhamus character claims to be an expert of a martial art form named "Maan Karate" (Maan means "Deer"), which is actually the art of running away like a deer when in danger.  The phrase became famous and was used to name the 2014 action comedy film Maan Karate because whenever there is a problem in his life, the hero (played by Sivakarthikeyan) fails to face them and runs for cover instead.  
 
== Explanatory notes ==
 

== Further reading ==
*  

== References ==
 

== External links ==
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 