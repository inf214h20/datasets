Communion (1989 film)
 
{{Infobox film
| name = Communion
| image = CommunionPoster.jpg 
| caption = Special Collectors Edition DVD cover
| director = Philippe Mora
| producer = Philippe Mora
| writer = Whitley Strieber
| starring = Christopher Walken Lindsay Crouse Frances Sternhagen
| music = Eric Clapton Allan Zavod
| cinematography = Louis Irving Lee Smith
| distributor = New Line Cinema Allied Vision
| released =  
| runtime = 105 min.
| gross = $1,920,000
| country = United States English
}} thriller film book of the same name by Whitley Strieber.
 extraterrestrial phenomenon while on vacation at a remote home in the wilderness during which the father is abducted and all of their lives change. According to Strieber, the story is a real-life account of his own encounter with "visitors", with Walken playing the role of the author.

The score was composed by Eric Clapton. It received a mostly negative critical reaction due to Walkens performance and was panned by Strieber himself due to its non-factual portrayal of him. He said that while his experience was a dream and on his own, the film showed it as actually happening and his entire family experiencing it. The film was also a box office failure.

==Cast==
*Christopher Walken as Whitley Strieber
*Lindsay Crouse as Anne Strieber
*Frances Sternhagen as Dr. Janet Duffy
*Andreas Katsulas as Alex

==External links==
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 


 