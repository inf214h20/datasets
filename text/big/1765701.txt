Girl Happy
{{Infobox film
| name           = Girl Happy
| image          = GirlHappyElvisP.JPG
| border         = yes
| alt            = 
| caption        = Theatrical release poster
| director       = Boris Sagal
| producer       = Joe Pasternak
| writer         = {{Plainlist| Harvey Bullock
* R.S. Allen
}}
| starring       = {{Plainlist|
* Elvis Presley
* Shelley Fabares
}}
| music          = George E. Stoll
| cinematography = Philip H. Lathrop
| editing        = Rita Roland
| studio         = {{Plainlist|
* Euterpe
* Metro-Goldwyn-Mayer
}}
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 96 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = $3,250,000   
}} musical romantic comedy and beach party film starring Elvis Presley in his 18th feature. The movie won a fourth place prize Laurel Award in the category Top Musical of 1965. It  featured the song "Puppet on a String", which reached #14 on the Billboard (magazine)|Billboard Hot 100, #3 on the Adult Contemporary chart and in Canada, and was certified Gold by the RIAA.

==Plot==
Nightclub singer Rusty Wells (Elvis) and his band have just closed their engagement at the club where they work in Chicago, Illinois|Chicago, and are just about ready to leave for their annual spring break trip to Fort Lauderdale, Florida—that is, until the clubs owner, Big Frank (Harold Stone), extends their stay at his club, foiling the bands plans for some sun and fun in Florida. 

At the same time, Big Franks daughter, college student Valerie (Shelley Fabares) also takes her spring break in Lauderdale with her friends, which worries her father to no end. So at the suggestion of Rusty (who sees this situation as an opportunity for him and the guys to make Lauderdale after all), Big Frank hires Rusty and his band to make the trip to Lauderdale to look after Valerie to make sure she stays out of trouble. But the task isnt easy, as Rusty and the guys struggle to keep sex-crazed Italian exchange student Romano (Fabrizio Mioni) away from Valerie, while at the same time Rusty has to keep explaining the situation to his date, a good-time girl named Deena (Mary Ann Mobley), who has no patience for guys who stand her up or keep her waiting.
 Puppet on a String").  However, after Big Frank reveals to Valerie that he is paying Rusty to be her chaperone, Valerie becomes angry and devastated to the point of going to a local nightclub, getting drunk, and starting a riot, landing everyone there in jail.

When Big Frank arrives in Lauderdale to spring Valerie from jail, he is angry with Rusty at first. But after he sees that Valerie has fallen in love with Rusty, Big Frank makes amends with Rusty, allowing Rusty and Valerie to once again rekindle their relationship.

==Primary cast==
* Elvis Presley as Rusty Wells
* Shelley Fabares as Valerie Frank
* Harold Stone as  Big Frank Gary Crosby as Andy
* Joby Baker as  Wilbur
* Jimmy Hawkins as  Doc
* Nita Talbot as Sunny Daze
* Mary Ann Mobley as Deena Shepherd
* Fabrizio Mioni as Romano Orlada
* Peter Brooks as Brentwood Von Durgenfeld
* Jackie Coogan as Sgt. Benson
* John Fiedler as Mr. Penchill
* Chris Noel as Betsy
* Beverly Adams as Girl #2 (uncredited)
* Dan Haggerty as Charlie (uncredited)
* Red West as Extra in the Kit Kat Club (uncredited)

==Production notes==
Although MGM presented Girl Happy as a beach party film ("Elvis brings his beat to the beach!" "Elvis jumps with the campus crowd to make the beach ball bounce!") and while Presley had previously appeared shirtless in films prior to this, he never appears without a shirt at anytime throughout his many scenes at the pool and on the beach in Florida, wearing long sleeves for most of the film - even while water-skiing. 

Alternate titles considered were The Only Way to Love and Girl Crazy. Joe Pasternak had produced the similar spring break movie Where the Boys Are, which was also set in Fort Lauderdale, Florida, in 1960.

  and Mobley and Noel both in Get Yourself a College Girl. Shelley Fabares and Nina Talbot sing the song "Read All About It". This would the first of three movies in which she would co-star with Elvis Presley. Dan Haggerty, who plays Charlie, would later appear in the The Life and Times of Grizzly Adams movie and TV series.

==Soundtrack==
 

==Home media and DVD reviews==
The film made its home video debut in 1988, being released on VHS. When it was reissued on VHS in 1997, the song "Startin Tonight" was deleted. It was eventually reinstated back into the film when it made its DVD debut in 2007.

===DVD reviews===
*  Review by Stuart Galbraith IV at  , September 11, 2007.
*  by Mike Noyes at  , 08.07.2007.

==References==
 

==External links==
*   Website decitated to Elvis Presleys Movies.
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 