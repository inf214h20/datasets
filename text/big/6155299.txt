Syndromes and a Century
{{Infobox film
| name           = Syndromes and a Century
| image          = Syndromesandacenturyposter.jpeg
| caption        = The theatrical poster.
| director       = Apichatpong Weerasethakul
| producer       = Wouter Barendrecht Simon Field Michael J. Werner Keith Griffiths Charles de Meaux Pantham Thongsangl Apichatpong Weerasethakul
| writer         = Apichatpong Weerasethakul
| narrator       = Lee Chatametikool
| starring       = Nantarat Sawaddikul Jaruchai Iamaram Sophon Pukanok Jenjira Pongpas
| music          = Kantee Anantagant
| cinematography = Sayombhu Mukdeeprom
| editing        = Lee Chatametikool
| distributor    = Fortissimo Films Strand Releasing
| released       =  
| runtime        = 105 minutes
| country        = Thailand
| language       = Thai
| budget         = 
}} Thai drama film written and directed by Apichatpong Weerasethakul. The film was among the works commissioned for Peter Sellars New Crowned Hope festival in Vienna to celebrate the 250th anniversary of the birth of Wolfgang Amadeus Mozart.  It premiered on August 30, 2006 at the 63rd Venice Film Festival.

The film is a tribute to the directors parents and is divided into two parts, with the characters and dialogue in the second half essentially the same as the first, but the settings and outcome of the stories different. The first part is set in a hospital in rural Thailand, while the second half is set in a Bangkok medical center. "The film is about transformation, about how people transform themselves for the better", Apichatpong said in an interview. 
 Board of Censors demanded that four scenes be cut in order for the film to be shown commercially. The director refused to cut the film and withdrew it from domestic release. Rithdee, Kong.  , Bangkok Post, retrieved 2007-04-12  Since then, the director had agreed to a limited showing in Thailand where the cut scenes were replaced with a black screen to protest and inform the public about the issues of censorship.

==Cast==
* Nantarat Sawaddikul as Dr. Toey
* Jaruchai Iamaram as Dr. Nohng
* Nu Nimsomboon as Toa
* Sophon Pukanok as Noom, the orchid expert
* Jenjira Pongpas as Pa Jane
* Arkanae Cherkam as Dentist Ple
* Sakda Kaewbuadee as Monk Sakda
* Sin Kaewpakpin as Old Monk

==Production==

===Origins===
"Its a film about heart", the director told the Bangkok Post. "Its not necessarily about love, its more about memory. Its about feelings that have been forever etched in the heart." 
 Khon Kaen, Thailand. But the director revised that concept when he cast the actors and began filming. The story still focuses on a male and female doctor, and is dedicated to the directors parents, but is set in two hospitals 40 years apart and explores both the memories and current lives of the protagonists.

"I began with my parents story, but it has sprung to other things", Apitchatpong said in an interview. "When I met the actors, when I found the location, there were other stories combined and added in. I try not to limit it. I allow it to flow whichever way it goes. It is very exciting." 

==Reception==

===Critical reception===
 
The film has an 87% "fresh" rating on Rotten Tomatoes, based on 38 reviews.  Metacritic gives it a score of 71 out of 100, based on seven reviews. 

Screen Daily noted the enigmatic, non-existent narrative while praising the films technical aspects. 

===Top ten lists===
The British Film Institutes Sight & Sound year-end poll for 2007 had Syndromes and a Century tied for seventh best with four other films.  Syndromes also appeared on several critics top ten lists of the best films of 2007. 

*1st – David Ansen, Newsweek
*3rd – Nathan Lee, The Village Voice
*5th – Wesley Morris, The Boston Globe
*10th – Scott Tobias, The A.V. Club

In November 2009, the film was named by Toronto International Film Festivals Cinematheque as the best of the decade, as voted by more than sixty international film historians, archivists, curators, and programmers. 

===Thai reception and censorship=== Motion picture rating system in Thailand Board of Censors demanded the removal of four scenes. Apichatpong refused to cut the film and withdrew it from domestic circulation.

He explained his reasons for doing so in an article in the Bangkok Post:

 
 Buddhist monk playing a guitar and two monks playing with a remote-control flying saucer.  The censors refused to return the print unless the requested cuts were made. 

Later in 2007, the film was shown twice in privately arranged screenings at the Alliance française in Bangkok.
 National Legislative Assembly. A replacement for the 1930 film act, the ratings law contained a restrictive ratings structure and retained the governments powers to censor and ban films it deemed would "undermine or disrupt social order and moral decency, or that might impact national security or the pride of the nation". Rithdee, Kong. December 20, 2007.  , Variety (magazine); retrieved 2008-01-23 

To oppose the draft law, Apichatpong and other directors formed the Free Thai Cinema Movement.

"We disagree with the right of the state to ban films", Apichatpong was quoted as saying. "There already are other laws that cover potential wrongdoings by filmmakers." Rithdee, Kong. November 28, 2007.  , Variety (magazine); retrieved 2008-01-23 

Ladda Tangsupachai, director of the Ministry of Cultures Cultural Surveillance Department, said the ratings law was needed because moviegoers in Thailand are "uneducated". "Theyre not intellectuals, thats why we need ratings", she was quoted as saying. Montlake, Simon. October 11, 2007.  , Time (magazine); retrieved 2008-01-23 

Despite the protest, the law was passed on December 20, 2007. 

The film was released for a limited run in April, 2008 at the Paragon Cineplex in Bangkok, Thailand in its censored form.  In protest of the censoring, the director inserted black, scratched film trailer in place of each of the censored scenes, the same length as the scenes that were cut.  The result is that the audience experiences no sound and no picture for the same time and in the same spots in the film as the censored scenes.

===Festivals and awards=== Toronto International Film Festival, London International Film Festival, Vancouver International Film Festival, Chicago International Film Festival, Tokyo Filmex, Pusan International Film Festival, International Film Festival Rotterdam, Taipei Golden Horse Film Festival, Hong Kong International Film Festival, the Maryland Film Festival, and the Melbourne International Film Festival.
 inaugural Asian Film Awards in 2007 in Hong Kong, the film won the Best Editor award for Lee Chatametikool. It was also nominated for best director and best cinematographer. 

==DVD== Region 1 DVD on January 15, 2008 by Strand Releasing.
 Region 2) by the British Film Institute is scheduled for June 30, 2008.

The French DVD of Syndromes and a Century is released by   (with Luminous People in the special features)

==References==
 

==External links==
*  
*  
*  
*  
* 
*  

 

 
 
 
 
 
 
 
 