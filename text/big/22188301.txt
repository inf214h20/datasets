Down the Ancient Staircase
 
{{Infobox film
| name           = Down the Ancient Staircase
| image          = Down the Ancient Staircase.jpg
| caption        = 
| director       = Mauro Bolognini
| producer       = Fulvio Lucisano
| writer         = Raffaele Andreassi Mario Arosio Sinko Solleville Marie Tullio Pinelli Mario Tobino Bernardino Zapponi
| starring       = Marcello Mastroianni
| music          = Ennio Morricone
| cinematography = Ennio Guarnieri
| editing        = Nino Baragli
| distributor    = 
| released       = 1975
| runtime        = 115 minutes
| country        = Italy
| language       = Italian
| budget         = 
}}

Down the Ancient Staircase ( ) is a 1975 Italian drama film directed by Mauro Bolognini.   

==Cast==
* Marcello Mastroianni - Professor Bonaccorsi
* Françoise Fabian - Anna Bersani
* Marthe Keller - Bianca
* Barbara Bouchet - Carla
* Pierre Blaise - Tonio
* Maria Teresa Albani
* Maria Michi
* Paolo Pacino
* Silvano Tranquilli - Professor Rospigliosi Charles Fawcett - Doctor Sfameni
* Enzo Robutti
* Ferruccio De Ceresa - Fascist Official in train
* Lucia Bosé - Francesca
* Adriana Asti - Gianna

==References==
 

==External links==
* 
 

 
 
 
 
 
 
 
 
 