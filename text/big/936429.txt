Miracle Mile (film)
{{Infobox film
| name           = Miracle Mile
| image          = Miracletheatrical.jpg
| image_size     =
| alt            =
| caption        = Theatrical release poster
| director       = Steve De Jarnatt
| producer       = John Daly Derek Gibson
| screenplay     = Steve De Jarnatt
| starring       = Anthony Edwards Mare Winningham Denise Crosby Mykelti Williamson
| music          = Tangerine Dream
| cinematography = Theo van de Sande
| editing        = Stephen Semel Kathie Weaver
| studio         = Miracle Mile Productions
| distributor    = Hemdale Film Corporation
| released       =  
| runtime        = 87 minutes
| country        = United States
| language       = English
| budget         = $3,700,000
| gross          = $1,145,404
}} apocalyptic  thriller cult Miracle Mile Los Angeles, where most of the action takes place.  The movie was well received by critics, but bombed at the box office. Despite the poor box office performance, the movie has attracted a cult following.

==Plot==
The film takes place in a single day and night. The film opens with the two main characters, Harry (Anthony Edwards) and Julie (Mare Winningham), meeting at the La Brea Tar Pits and immediately falling in love. After spending the afternoon together, they make a date to meet after her shift ends at midnight at a local coffee shop, but a power failure means Harrys alarm fails to wake him and Julie leaves for home.
 nuclear war is about to break out in less than seventy minutes. When Harry finally gets a chance to talk and asks whos calling, the caller realizes he has dialed the wrong area code. Harry then hears him plead with Harry to call his father and apologize for some past wrong before he is being confronted and presumably shot. An unfamiliar voice picks up the phone and tells Harry to forget everything he heard "...and go back to sleep" before disconnecting.
 Washington on LAX to an American base in a region in Antarctica with no rainfall. Most of the customers and staff leave with her in the owners delivery van. Harry, unwilling to leave without Julie, arranges to meet at the airport and jumps from the truck.
 Mutual Benefit Life Building. Julie has also tried to find a pilot on her own, and in the moments it takes to find her, Los Angeles descends into violent chaos. There is still no confirmation any of this is real, and Harry wonders if he has sparked a massive false panic in the example of Chicken Little. However, when he uses a phone booth to contact the father of the man who called him (using the number of the booth and the area code the man was trying to use) he reaches a man who says his son is a soldier. Harry tries to pass on the message he was given, but the phone disconnects before he finishes.
 EMP from the detonations causes the helicopter to crash into the La Brea Tar Pits.

As the helicopter sinks and the cabin fills with natural asphalt tar, Harry tries to comfort Julie by saying someday they will be found and they will probably be put in a museum, or maybe they will take a direct hit and be turned into diamonds. Julie seems to take some hope in this, and the movie fades out as the tar fills the compartment. A final explosion seems to imply a direct hit has taken place.

==Cast==
 
 
* Anthony Edwards as Harry Washello
* Mare Winningham as Julie Peters
* John Agar as Ivan Peters
* Lou Hancock as Lucy Peters
* Mykelti Williamson as Wilson
* Kelly Jo Minter as Charlotta
* Kurt Fuller as Gerstead
* Brian Thompson as Helicopter Pilot
* Denise Crosby as Landa
 
* Robert DoQui as Fred the Cook
* O-Lan Jones as Waitress
* Claude Earl Jones as Harlan
* Alan Rosenberg as Mike
* Danny De La Paz as Transvestite
* Earl Boen as Drunk Man in Diner
* Diane Delano as Stewardess
* Edward Bunker as Nightwatchman
 

==Production==
Before Miracle Mile was made, its production had been legendary in Hollywood for ten years. {{cite news
 | last = Richardson
 | first = John H
 | coauthors =
 | title = Miracle Mile Made with Slowly Measured Steps
 | work = St. Petersburg Times
 | pages =
 | language =
 | publisher = 
 | date = May 28, 1989
 | url = 
 | accessdate = }}  In 1983, it had been chosen by American Film magazine as one of the ten best unmade screenplays.  Steve De Jarnatt wrote it just out of the American Film Institute for Warner Brothers with the hope of directing it as well. The studio wanted to make it on a bigger scale and did not want to entrust the project with a first-time director like De Jarnatt. 
 John Daly of Hemdale Films gave De Jarnatt $3.7 million to make the film.

===Filming locations=== Miracle Mile Fairfax District; Santa Monica Pier, Santa Monica, California.

==Reception==
===Critical response===
Miracle Mile received generally positive reviews among critics.

Roger Ebert praised the film, claiming it had a "diabolical effectiveness" and a sense of "real terror". {{cite news
 | last = Ebert
 | first = Roger
 | coauthors =
 | title = Miracle Mile
 | work = Chicago Sun-Times
 | pages =
 | language =
 | publisher = 
 | date = June 9, 1989
 | url = http://rogerebert.suntimes.com/apps/pbcs.dll/article?AID=/19890609/REVIEWS/906090302/1023
 | accessdate = 2010-03-03 }}  In her review for the Washington Post, Rita Kempley wrote: "It seems hes (De Jarnatt) not committed to his story or his characters, but to the idea that he is saying something profound - which he isnt." {{cite news
 | last = Kempley
 | first = Rita
 | coauthors =
 | title = Miracle Mile to Nowhere
 | work = Washington Post
 | pages =
 | language =
 | publisher = 
 | date = June 14, 1989
 | url = http://www.washingtonpost.com/wp-srv/style/longterm/movies/videos/miraclemilerkempley_a09fc6.htm
 | accessdate = 2010-03-03 }}  Stephen Holden, in The New York Times, wrote: "As Harry and Julie, Mr. Edwards and Ms. Winningham make an unusually refreshing pair." {{cite news
 | last = Holden
 | first = Stephen
 | coauthors =
 | title = Waiting in California for the next Big Bang
 | work = The New York Times
 | pages =
 | language =
 | publisher = 
 | date = May 19, 1989
 | url = http://movies.nytimes.com/movie/review?res=950DE3DC143AF93AA25756C0A96F948260
 | accessdate = 2010-03-03 }}  In his review for the Boston Globe, Jay Carr called it: "...a messy film, but its got energy, urgency, conviction and heat and you wont soon forget it." {{cite news
 | last = Carr
 | first = Jay
 | coauthors =
 | title = Miracle Mile
 | work = Boston Globe
 | pages =
 | language =
 | publisher = 
 | date = June 9, 1989
 | url = 
 | accessdate = }}  British film and television critic Charlie Brooker, in an article for the BAFTA web site written in September 2008, awarded Miracle Mile the honor of having the "Biggest Lurch of Tone" of any film he had ever seen. 

The review aggregator Rotten Tomatoes reported that 86% of critics gave the film a positive review, based on twenty-one reviews. 

===Awards===
Wins
* Sitges - Catalonian International Film Festival: Best Special Effects; 1989.

Nominations
* Sitges - Catalonian International Film Festival: Nominated, Best Film, Steve De Jarnatt; 1989
* Sundance Film Festival: Grand Jury Prize, Dramatic, Steve De Jarnatt; 1989.
* Independent Spirit Awards: Best Screenplay, Steve De Jarnatt; Best Supporting Female, Mare Winningham; 1990

==See also==
* List of apocalyptic films
* List of films about nuclear issues
* List of nuclear holocaust fiction
* Nuclear weapons in popular culture
* Survival film

==References==
 

==External links==
*  
*  
*  
*   soundtrack sample by Tangerine Dream at YouTube
*   film trailer at YouTube

 
 
 
 
 
 
 
 
 
 
 
 