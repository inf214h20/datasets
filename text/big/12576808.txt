Futurama: Into the Wild Green Yonder
{{Infobox film
| name           = Futurama:  Into the Wild Green Yonder
| image          = Futurama WildGreenYonder.jpg
| caption        = 
| director       = Peter Avanzino
| producer       = Lee Supercinski Claudia Katz
| screenplay     = Ken Keeler
| story          = Ken Keeler David X. Cohen (Parts One and Four) Billy West Katey Sagal John DiMaggio Tress MacNeille Maurice LaMarche Phil LaMarr Lauren Tom David Herman
| music          = Christopher Tyng
| cinematography  = 
| editing        = Paul D. Calder
| distributor    = 20th Century Fox Home Entertainment
| studio         = The Curiosity Company
| released       =  
| runtime        = 89 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Futurama: Into the Wild Green Yonder is the last of a series of four straight-to-DVD Futurama movies.     The movie was written by Ken Keeler, based on a story by Keeler and David X. Cohen, and directed by Peter Avanzino.  Guest stars include Phil Hendrie, Penn Jillette (credited with Teller (magician)|Teller), Snoop Dogg and Seth MacFarlane, who sings the theme song.  Leela becomes Fry joins a secret society and attempts to stop a mysterious species known as the "Dark Ones" from destroying all life in the universe. The title itself is a reference to the The U.S. Air Force (song)|U.S. Air Force Song, the main chorus of which describes reaching "Into the wild blue yonder".

The DVD and Blu-ray Disc|Blu-ray were released by 20th Century Fox Home Entertainment on February 23, 2009, while the film itself premiered on February 6, 2009 at New York Comic Con. {{cite web|url=http://tvblog.ugo.com/index.php/tvblog/more/live_from_new_york_comic_con_fans_treated_to_futurama_world_premiere/ season five of Futurama, with each film being separated into four episodes of the broadcast season.     It won the 2009 Annie Award for Best Home Entertainment Production,    and 20th Century Fox and Comedy Central cited sales of Into the Wild Green Yonder and the other Futurama direct-to-DVD movies as one reason Comedy Central decided to renew the Futurama television series in 2009.      

==Plot== Vegas and Professor Farnsworth to rubber stamp the project as environmentally friendly. Leela saves a Martian muck leech, the last of its species, from the site.
 transient who advises Fry to wear a tin foil hat to keep others thoughts out of his head. Hutch warns Fry never to reveal his powers and to beware the "Dark Ones".

While golfing with the crew, Leo reveals plans to build the universes largest miniature golf course, destroying 12% of the Milky Way in the process. Farnsworth and the crew survey the site and discover an asteroid in a violet dwarf star system teeming with primordial life. Despite this, Farnsworth approves Leo’s project. Disgusted, Leela joins the eco-feminists, who begin sabotaging the project.

  psionic powers he alone can save it from Leo Wongs plans to turn it into a golf course, and from the Dark Ones, who have evolved to the point that no one knows what they look like.

To end the sabotage, Leo enlists Zapp Brannigan and Kif Kroker, who in turn hire Bender to track down the eco-feministas. Fry infiltrates Leos empire as a security guard. Amy is angered by her fathers sexist jokes and joins Leela, while Bender bugs Frys phone in case he communicates with Leela. Fry runs into Frida and has her take a message of support to Leela, but an unseen Dark One murders Frida.

Farnsworth prepares to close Planet Express; with their delivery team missing they cannot continue. Leo Wong hires them to put up a fence around the construction site. Farnsworth cancels the closing and goes with Zoidberg and Hermes to do the job. They are captured by the eco-feminists, who commandeer the Planet Express ship. When the eco-feministas suspect Fry of murdering Frida, Fry and Leela arrange a rendezvous. They are ambushed by the Nimbus, which was tipped off by Bender. The eco-feministas are sent to prison.

At a Legion meeting, No. 9 explains that Fry must stop the implosion of the violet dwarf and thwart the Dark One who is sure to be present. Though no one knows the Dark Ones form, its mind cannot be read, allowing Fry to identify it.  No. 9 gives Fry the Omega Device, which can temporarily disable the Dark One at close range.

Bender frees the eco-feministas from prison in order to uphold his record for most crimes committed at once. Hermes, Zoidberg, Scruffy, and a repentant Farnsworth rescue them.

At the ceremony, Fry cannot locate an unreadable mind; he concludes that he himself (having an unreadable mind) must be the Dark One. The eco-feministas disrupt the ceremony, but Fry convinces Leela to let him proceed. Fry activates the Omega Device, which creates a small dome around the two that appears to have no effect. Leelas leech falls to the ground and reveals itself as the final Dark One.  The violet dwarf system forms a giant sperm and flies into the star, creating an Encyclopod embryo which quickly matures, taking the form of a giant manta ray-like creature. The Dark One kills Hutch, whose dying act is to pull Fridas necklace out of Frys forehead, causing Fry to lose his telepathy. The Encyclopod kills the Dark One. No. 9 convinces the Encyclopod to preserve the Dark Ones DNA, but Zoidberg eats the remains before it can. The Encyclopod preserves Hutchs DNA before leaving.

Zapp attempts to apprehend the escaped prisoners, but the crew of the Planet Express ship and the Eco-feminists escape along with Kif.  Fry and Leela profess their love for each other as the Nimbus chases the Planet Express ship toward a wormhole, which the Professor warns could take them trillions of light years away. Everyone agrees to go for it. Fry and Leela kiss as the ship enters the wormhole.

===Ending===
The ending refers to the unsure future of the show. The makers were not sure if the show would return to TV, so if it did not, it is implied that the ship was in fact taken trillions of light years away. The show was picked up by Comedy Central, and it is revealed in the next episode "Rebirth (Futurama)|Rebirth" that the wormhole sent the ship directly back to Earth.

==Cast==
{| class="wikitable"
|-
! Actor
! Character
|- Billy West Professor Farnsworth Leo Wong Additional voices
|- Leela
|- Bender List Joey Mousepad Additional voices
|-
| Tress MacNeille || Mom Fanny Additional voices
|- Donbot List Clamps Calculon Additional voices
|-
| Phil LaMarr || Hermes Conrad Additional voices
|- Inez Wong Trixie
|-
| David Herman || Number 9 Additional voices
|- LaBarbara Conrad Prison Warden
|-
| Snoop Dogg || Himself
|- Frida Waterfall   Hutch Waterfall  The Encyclopod
|-
| Seth MacFarlane || Mars Vegas singer
|-
| Penn & Teller || Themselves
|-
|}

==Continuity== now useless as fuel the Planet Express ship has been modified to run on whale oil, an alternative introduced in "Bendin in the Wind". 
 brought to Nibbler because being his own grandfather) would be required to save the universe.    In Yonder Fry is once again appointed for such a task (though by the Legion of Mad Fellows instead of the Nibblonians), due to his immunity from the Dark Ones psionic attacks. 

The No. 9 man, a recurring background character throughout the series, is given a significant role in the movie, though quite different from the role in the series for which he was originally conceived.   

  from the Futurama series.]]
 Morbo mentions that there are no children present. In the DVD commentary, producer David X. Cohen notes that Rough Draft Korea, Futuramas overseas animation studio, charged a significant premium because of the difficulty of animating this scene. 

In the final scene of the movie, Amy and Kif are reunited  . After years of Fry trying to win her over, Leela finally returns Frys love in full; Cohen notes that there was considerable debate among the Futurama writers about how to end the movie, and that Futurama creator Matt Groening himself pushed for the actual conclusion. 

The movie was initially intended to end the series. After Futurama was renewed, its creators were unsure if the storylines in the film would be continued. Groening stated that he wanted to ignore the films ending and move on with the show. Cohen felt differently, stating that the revelations at the end should be resolved, even if the resolutions were brief, which they were in the premiere of the new season. 

==Production== Screen Actors Guild strike deadline, again forcing the writers to revise the script without completely reviewing the picture. 

Aware that Into the Wild Green Yonder could have been the final Futurama episode at the time of writing, the writers inserted numerous references to that fact.  The title screen displays the message "The Humans Shall Not Defeat Us" in Alien Language 1; according to Cohen, the message is a defiant statement regarding the possible end of the series.     Midway through the movie, a shot of the exterior of the Planet Express building draped with a banner reading "Going Out Of Business Forever! Again!" is shown, a reference to the original series previous cancellation in 2003.   The scene where Professor Farnsworth removes Zoidbergs and Hermess career chips and the countdown scene at the violet dwarf implosion ceremony both reference events from the pilot episode, "Space Pilot 3000" and also "The Cryonic Woman".   The cliffhanger nature of the final scene in the movie was devised so that it could conclude the series on an emotional note but also provide a point of departure for a series renewal, according to Cohen.   
 Keeler Gap galactic coordinates scientific name Cyprinodon martius.  Writer Ken Keeler adapted the name from Cyprinodon salinus, the scientific name of the Death Valley pupfish, which like the Martian muck leech lives in the desert and is nearly extinct. 

The opening musical theme is a Frank Sinatra-style number sung by Family Guy and American Dad! creator Seth MacFarlane as the Planet Express ship flies around the casinos of Mars Vegas. 
<!----
===DVD===

The DVD features an audio commentary track, a storyboard animatic for Part 1 of the movie, and a collection of deleted scenes.  The Blu-ray also features a picture-in-picture version of the commentary.
---->

==Reception==
Overall the film has received mixed reviews.  Alynda Wheat of Entertainment Weekly gave the film a grade of B, saying that it catered to established fans.     Scott Collura of IGN rated the movie itself 5/10, praising the use of both major and minor characters from the series and the science-fiction content, but criticizing the movie for being disjointed and for its "underwhelming climax" and concluding that it "never fully captures the greatness of the original series."  Collura rated the DVD 7/10, noting the high quality of the video transfer, the image detail and depth, and the use of surround sound and low-frequency effects.     Martin Liebman of Blu-ray.com rated the movie 2.5/5 and the Blu-ray release 3.5/5 overall.  Liebman praised the film for its development of the primary characters in a way that would appeal to longtime fans and new viewers, but criticized the messy plot and haphazard pacing of the movie.  Liebman lauded the Blu-ray release for its crisp images, resolution of detail in the animation, lossless soundtrack and use of surround sound.    Movie Critic Bruce Kirkland of Sun Media Corporation wrote that the movie was "just as good as Benders Big Score", praising its send-ups of Las Vegas and science fiction themes and writing that it "nicely handles its environmental message with trenchant wit".       Jeffrey Kauffman of DVD Talk rated the movie four stars out of five, calling it "a fun and frenetic windup to a perhaps undervalued television gem".   

According to the-numbers.com, the DVD sold approximately 83,000 units for a total of $1.6 million during its initial week of release, placing it 20th in sales across the USA.  As of April 19, 2009, estimated DVD sales in the USA stand at approximately 159,000 units for a total of $3.03 million.     Comedy Central cited sales of the DVD as one reason it decided to renew the Futurama television series.   

==References==
 

==External links==
*  at the Infosphere.
*  
*   at the SynopsiTV

 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 