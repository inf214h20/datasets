Have Sword, Will Travel
 
 
{{Infobox film
| name           = Have Sword, Will Travel
| image          = HaveSwordWillTravel.jpg
| alt            = 
| caption        = Film poster
| film name      = {{Film name
| traditional    = 保鏢
| simplified     = 保镖
| pinyin         = Bǎo Biāo
| jyutping       = Bou2 Biu1 }}
| director       = Chang Cheh
| producer       = Runme Shaw
| writer         = 
| screenplay     = Ni Kuang
| story          = 
| based on       = 
| starring       = Ti Lung David Chiang Lee Ching
| narrator       = 
| music          = Wang Fu Ling
| cinematography = Kung Mu To
| editing        = Chiang Hsing Lung
| studio         = Shaw Brothers Studio
| distributor    = Shaw Brothers Studio
| released       =  
| runtime        = 101 minutes Hong Kong Mandarin
| budget         = 
| gross          = HK$1,031,768.90
}}
 1969 Cinema Hong Kong wuxia film directed by Chang Cheh and starring Ti Lung, David Chiang and Lee Ching.

==Plot==
The upright and invincible leader of the Wulin clan Lord Yin Ke Feng (Cheng Miu) helps the Luoyang government to escort two hundred million worth of gold annually and has never made a mistake. However, he accidentally loses his martial arts skills and the gold is stolen by Chao Hong (Ku Feng), leader of the Flying Tigers thieves. Lone swordsman Lo Yi (David Chiang) meets Yun Piao Piao (Lee Ching) and her fiance Siang Ding (Ti Lung), who are bodyguards for the gold. First, they fight each other before joining forces to help Yin to defeat Chao. Originally down and out, Lu is taken in by the gentle Yun and volunteers to be a bodyguard. He becomes acquainted with her through his warm-blooded actions.

==Cast==
*Ti Lung as Siang Ding
*David Chiang as Lu Yi
*Lee Ching as Yun Piao Piao
*Wong Chung as The Mute
*Ku Feng as Chao Hong
*Chan Sing as Mao Biao
*Cheng Miu as Lord Yin Ke Feng
*Cheng Lui as Wong Chuen
*Wang Kuang Yu as one of Lord Yins men
*Hung Lau as Chu Tin Hou, a Flying Tiger
*Lau Gong as a Flying Tiger
*Cliff Lok as a Flying Tiger
*Cheng Kang Yeh as Yang Hang Chiu
*Wong Ching Ho as Government official
*Lo Hung
*Yuen Cheung-yan
*Yuen Shun Yi
*Wong Pau Kei
*Fung Hak On
*Lo Wai
*Chan Chuen
*Tsang Cho Lam as Waiter
*Danny Chow
*Yen Shi Kwan
*Wu Chi Chin
*Ho Pak Kwong
*Wong Mei
*Jason Pai as carries silver to the cart
*Wong Shu Tong
*Wan Ling Kwong
*Tam Bo
*Law Keung
*Chu Gam as clerk
*Ho Bo Sing
*Yuen Shing Chau
*Ng Yuen Fan
*Lee Siu Wah
*Fung Hap So
*Yuen Pak Chan
*Goo Chim Hung as Horse buyer
*Chik Nagi Hung
*Hsu Hsia
*Chui Chung Hok
*Cheung Chi Ping
*Chan Sing Tong
*Chan Siu Gai
*San Kuai
*Hon Kwok Choi
*Lau Jun Fai
*Lai Yan
*Yeung Wai
*Tang Tak Cheung
*Chui Fat

==Box office==
The film grossed HK$1,031,768.90 at the Hong Kong box office during its theatrical from 25 December 1969 to 8 January 1970 in Hong Kong.

==External links==
* 
*  at Hong Kong Cinemagic
* 

 

 
 
 
 
 
 
 
 
 
 