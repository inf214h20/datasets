My Grandparents Had a Hotel
 
{{Infobox film
| name           = My Grandparents Had a Hotel
| image          = Mygrandparentshotelhomepage1.jpg
| image_size     =
| caption        =
| director       = Karen Shopsowitz
| producer       =
| writer         =
| narrator       =
| starring       =
| music          =
| cinematography =
| editing        =
| distributor    =
| released       = 1991
| runtime        = 30 min.
| country        = Canada English
| budget         =
| preceded_by    =
| followed_by    =
| website        =
}}

My Grandparents Had a Hotel is a 1991 documentary film by Karen Shopsowitz  that takes a nostalgic look at the Montieth Inn, a popular Jewish resort operated in the Canadian Catskills from 1935-1949. Although it is Canadian, the inn is similar to Grossingers and the Concord Hotel in the Catskills of New York, which also saw their heyday in the mid-20th century.

With playful 16mm home video footage, interviews with people who once worked and vacationed at the family-run inn, and the filmmakers fathers own memories of the familys prized hotel, the film recreates the magic of this spot while telling the history of how the phenomenon came to be, how it flourished, and, finally, why it declined.

The documentary shows this unique time period through numerous vacationers anecdotes. For example, one old man laughs at his young self, recalling the time he and his teenage friends once stirred up trouble by showing up to a formal dining hall wearing dress shirts, ties, and suit jackets — but no pants.

==History of Jewish Resorts in North America==
In the 1930s, restrictions against Jews were common in the North American hotel industry. When someone with a Jewish-sounding last name called to make a reservation, hotels would often lie and say they had no vacancies.

Recognizing that Jews needed a place where they could get away without having to worry about antisemitism, one Canadian-Jewish couple sold their deli and bought a beautiful 150-room hotel on Lake Rosseau in Muskoka, just north of Toronto. The Monteith Inn became a popular vacation spot, where guests could relax in the countryside and enjoy the steady stream of talented entertainers that passed through.

Eventually, the government put an end to the antisemitic practices that made Jewish resorts necessary. Social reform and a terrible natural disaster spurred the decline of the Monteith Inn, but it and other hotels like it have left their traces in old family videos and memories.

==Awards== Gemini award. Shopsowitz won a Peabody Award for another documentary based on her fathers home movie footage, the 2000 National Film Board of Canada production My Fathers Camera.   

==References==
 
 

==External links==
* 

 
 
 
 
 
 
 
 
 


 
 