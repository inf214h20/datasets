Fort Apache Napoli
{{Infobox film
| name           = Fort Apache Napoli (Fortapàsc)
| image          = Fortapash.png
| alt            =  
| caption        = Giancarlo Siani, as played by Libero De Rienzo
| director       = Marco Risi
| producer       = Angelo Barbagallo Gianluca Curti
| writer         = Jim Carrington Andrea Purgatori Marco Risi Maurizio Cerino
| starring       = Libero De Rienzo Valentina Lodovini Michele Riondino Ennio Fantastichini Ernesto Mahieux Daniele Pecci Gianfranco Gallo Massimiliano Gallo
| music          = Franco Piersanti
| cinematography = Marco Onorato
| editing        = Clelio Benevento
| studio         = 
| distributor    = 01 Distribution
| released       =  
| runtime        = 113 minutes
| country        = Italy
| language       = Italian
| budget         = 
| gross          = 
}}
Fort Apache Napoli   ( ) is a 2009 film directed by Marco Risi about the brief life and death of journalist Giancarlo Siani, who is played by Libero De Rienzo.

==Plot== corruption and collusion between politicians and organized crime.

Despite the somewhat veiled threats of the local political class, Siani continues his inquiries, especially after the "massacre of the circle of fishermen". His articles particularly annoy the local Camorra bosses because they undermine their political and criminal alliances. So, after he was transferred to Naples by his paper, the Camorra meet, and decide to kill, Siani. Siani is shot outside his girlfriends house, in the residential district of Vomero, on 23 September 1985. Siani was only 26.

==Cast==
*Libero De Rienzo as Giancarlo Siani
*Valentina Lodovini as Daniela
*Michele Riondino as Rico
*Massimiliano Gallo as Valentino Gionta
*Ernesto Mahieux as Sasà
*Salvatore Cantalupo as Ferrara
*Ennio Fantastichini as sindaco Cassano
*Duccio Camerini as Angelo Nuvoletta
*Renato Carpentieri as Amato Lamberti
*Gianfelice Imparato as pretore Rosone
*Daniele Pecci as capitano Sensales
*Gianfranco Gallo as Donnarumma
*Antonio Buonomo as Lorenzo Nuvoletta
*Raffaele Vassallo as Ciro
*Kiung Mi Lee as Yu
*Mimmo Mignemi as Cifù

==Awards==
*3 Ciak doro: Best Cinematography, Best Score and Best Poster
*1 Globo dOro: Best Director

==Soundtrack==
The soundtrack includes the following tracks:
*Ogni Volta  - Vasco Rossi
*La torre di Babele  - Edoardo Bennato
*Tu ca nun chiagne  - Ciro Capano
*Pe sempre  - Ciro Capano
*O bene mio  - Ciro Capano
*Napule e - Pino Daniele
*Jesce sole  - R. De Simone
*Centro di gravità permanente  - Franco Battiato
*Pop corn e patatine  - Nino DAngelo
*Casanova 70  - performed by Antonio Buonomo
*Dicitencello vuje  - performed by Mario Abbate
*O ritratto e Nanninella  - performed by Antonio Buonomo
*Nocturne from String Quartet No. 2 in D Major  by A. Borodin - performed by the Pessoa Quartet (I Kyung Lee, Marco Quaranta, Rita Gucci, Achilles Taddeo)
*Quanno chiove  - Pino Daniele
*River runs deep  - JJ Cale
*Scumbinata  - Mammoliti, Mambelli, Di Carlo, Poggiani
*Noi ragazzi di oggi  - performed by Luis Miguel

==Revenue==
The film grossed € 703,000. 

==References==
*  

 
 
 
 
 
 
 