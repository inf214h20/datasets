Hello Stranger (film)
{{Infobox film
| name           = Hello Stranger
| image          = Hello_Stranger_(2010_film).jpg
| border         = yes
| caption        = movie poster
| director       = Banjong Pisanthanakun
| producer       =
| writer         = Chantavit Dhanasevi
| starring       =  
| music          =
| cinematography =
| editing        =
| distributor    = GMM Tai Hub
| released       =  
| runtime        =
| country        = Thailand
| language       = Thai
| budget         =
| gross          = $4,413,745 
}}
Hello Stranger ( ,   produced by Banjong Pisanthanakun, starring Chantavit Dhanasevi ( ) and Nuengthida Sophon ( ). It was Banjongs first romantic film, which was inspired by the book Two Shadows in Korea ( , Song Ngao Nai Kao Li) by Songkalot Bangyikhun ( ). 

Hello Stranger portrays a young Thai man and woman who meet by coincidence while on vacation in South Korea. They decide to tour Korea together while keeping their names secret from each other. The movie was filmed in Korea and includes various locations that have appeared in Korean dramas that were broadcast in Thailand.

The cast, the   director, and the musical group 25 Hours ( ) performed the song Nice Not to Know You ( , Yin Di Thi Mai Ru Chak).

== Cast ==
* Chantavit Dhanasevi ( ) as young man with the alias Dang ( )
* Nuengthida Sophon ( ) as young woman with the alias May ( )
* Warutaya Nilakhuha ( ) as Goi ( )
* Paradee Tuktik Mamuangthaisong as young woman with very white skin
* Pakjira Tukta Mamuangthaisong as young Thai woman with a mafia Thai boyfriend.

== Production ==

=== Theme Songs ===
The song Nice Not to Know You was performed by 25 Hours. The song title is a play on the Thai introductory phrase from "Yin Dee Tee Dai Roo Juk," meaning "Nice to know you," to "Yin Dee Tee Mai Roo Juk", meaning "Nice not to know you." Its music video, directed by Nithiwat Tharathorn, stars the actors Chantavit and Nuengthida.  Nuengthida also sang the theme song Love Doesnt Need Time ( , Ruk Mai Tong Gan We La), originally by Klear ( ).

==Awards==
Hello Stranger had its international premiere at 2011 (6th) Osaka Asian Film Festival in Osaka, Japan and won 2 awards from the festival including "Most Promising Talent Award" for the director, Banjong Pisanthanakun and "ABC Award" or Asahi Broadcasting Corporation Award which is a sponsor award given to the most entertaining film among films in the Competition.  Banjong and Chantavit represented all the casts & crews at the festival.

The film was also screened at 2011 Shanghai International Film Festival in Shanghai, China and the lead actor, Chantavit Dhanasevi, became one of the 10 actors to receive "Star Hunter Award". 

==Reception==
Hello Stranger was commercially successful. After its release on August 19, 2010, the film went on to gross 130 million baht in Thailand and became one of the most successful movies ever by GTH (at that time).  The film was also screened in various Asian countries including Vietnam, Malaysia, Singapore, Indonesia, the Philippines, China, and Taiwan.

== References ==
 

== External links ==
*  
*  
*  

 
 
 
 
 
 
 