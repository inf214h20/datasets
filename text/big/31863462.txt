Still Bill (film)
{{Infobox film
| name           = Still Bill
| image          = Still Bill film.jpg
| alt            = 
| caption        = DVD cover
| director       = Damani Baker Alex Vlack
| producer       = Damani Baker Alex Vlack Jon Fine Andrew Zuckerman
| writer         = 
| starring       = Bill Withers
| music          = Robert Burger David Hoffman
| cinematography = Damani Baker Jon Fine Ed Marritz
| editing        = Jon Fine Sakae Ishikawa
| studio         = Late Night and Weekends
| distributor    = B-Side Entertainment 
| released       =  
| runtime        = 77 minutes
| country        = United States
| language       = English
| budget         =
| gross          = 
}} 1972 album of the same name.

==Plot==
The film follows the life of Bill Withers, from his roots in West Virginia to his career in the United States Navy, to his famed musical career and post retirement family life.

==Cast==
*Bill Withers
*Cornell Dupree 
*James Gadson
*Jim James
*Angélique Kidjo
*Ralph MacDonald
*Raul Midón
*Tavis Smiley Sting
*Cornel West

==Critical reception==
The film received mostly positive reviews. Review aggregator website Metacritic gave the film a 76 out of 100, indicating "generally positive reviews." 

Roger Ebert of the Chicago Sun-Times gave the film 3 1/2 out of 4 stars and wrote positively about the film except for one set up interview with Cornel West and Tavis Smiley in the film:
 

Mike Hale of The New York Times also thought the film was well done and mirrored Eberts position on the interview with West and Smiley:
 

==References==
 

== External links ==
*  
*  

 
 
 
 
 
 
 
 