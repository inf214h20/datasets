Guilty Pleasures (2009 film)
 {{Infobox film
| name           = Guilty Pleasures
| image size     = 
| image	         = Guilty Pleasures 2009 film poster.jpg
| alt            = 
| caption        = Film poster
| director       =  
| producer       =  
| writer         = 
| screenplay     =  
| story          = 
| narrator       = 
| starring       =  
| music          = 
| cinematography = Austine Nwaolie
| editing        = Uche Alex Moore
| studio         = Royal Arts Academy
| distributor    = 
| released       =   
| runtime        = 
| country        = Nigeria
| language       = English
| budget         =
| gross          =
}}
Guilty Pleasures is a 2009 Nigerian drama film directed by Desmond Elliot and Daniel Ademinokan, starring Ramsey Nouah, Majid Michel and Nse Ikpe Etim.  It was nominated for Best Screenplay at the 6th Africa Movie Academy Awards.

==Cast==
* Ramsey Nouah as Terso
* Majid Michel as Bobby
* Nse Ikpe Etim as Liz
* Mercy Johnson as Boma
* Omoni Oboli as Nse
* Desmond Elliot as Mr Okoro
* Rukky Sanda as Chidinma
* Beverly Naya as Bella

==Reception==
Nollywood Reinvented gave it a rating of 3 out of 5 stars, commended the actors in the film and noted the plot had both derivative aspects and some originality.  Joy Isi Bewaji of Nigeria Entertainment Today praised the interpretation of roles by the top cast actors and described the plot as "subtly unravelling". 

==References==
 

 
 
 
 
 

 