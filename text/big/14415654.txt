Waterland (film)
{{Infobox film
| name           = Waterland
| image          = Waterland (film).jpg
| caption        = 
| director       = Stephen Gyllenhaal
| producer       = Patrick Cassavetti Katy Mcguinness
| writer         = Graham Swift (novel) Peter Prince (screenplay)
| narrator       =  John Heard
| music          = Carter Burwell
| cinematography = Robert Elswit
| editing        = Lesley Walker
| distributor    = Fine Line Features (US) Polygram Filmed Entertainment (UK) Pandora Cinema
| studio         = British Screen Productions Channel Four Films Palace Pictures Pandora Cinema
| released       = August 21, 1992 (UK) September 12, 1992 (Toronto International Film Festival|TIFF) October 30, 1992 (USA)
| runtime        = 95 minutes
| country        = United States United Kingdom
| language       = English
| budget         = 
| gross          = $1,100,218 
| preceded_by    = 
| followed_by    = 
}} novel of John Heard.

==Plot==

Waterland follows the story of a mentally anguished Pittsburgh high school history teacher (Irons) in 1974 going through a complete reassessment of his life. His method for reassessing his life is to narrate it to his class and interweave in it three generations of his familys history. The movie portrays the teachers narrative in the form of flashbacks to tell the story of a teenage boy and his mentally challenged older brother living on the fens of England with their widowed father. The entire movie is motivated by an opening scene in which the history teachers barren wife (Cusack) steals a child from a supermarket and believes it to be hers. The teacher explains to his class how he and his wife carried out a teenage romance which led to a disastrous abortion that mutilated the girls womb and left her infertile. The teacher is tortured by the guilt of this act as well as the jealousy he demonstrated to his older brother when he suspected his girlfriends child was that of his brother. The girlfriends flirtation with the older brother sets off events that lead to the older brothers suicide by drowning. A side-theme throughout the narration is of the teachers grandfather, who was a successful brewer and who fathered with his daughter the narrators older brother, thus accounting for the older brothers genetic difficulties. The movie ends with the teachers dismissal from his school and a possible renewal of his relationship with his wife.

== Cast ==
* Jeremy Irons – Tom Crick 
* Sinéad Cusack – Mary Crick
* Ethan Hawke – Matthew Price John Heard - Lewis Scott
* Grant Warnock - Young Tom
* Lena Headey – Young Mary
* Pete Postlethwaite – Henry Crick 
* Cara Buono – Judy Dobson
* David Morrissey – Dick Crick
* Maggie Gyllenhaal - Maggie Ruth
Ethan Hawke & Lena Headey starred together in the 2013 film The Purge

==Production==
Part of the film was filmed at Doddington Place Gardens, near Faversham. The Victorian mansion was used as the ancestral home to Tom Crick. 

==References==
 

== External links ==
* 

 

 
 
 
 
 
 