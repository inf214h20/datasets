The Eagle's Mate
{{infobox film
| name           = The Eagles Mate
| image          = The Eagles Mate - 1915 - newspaperscene.jpg
| imagesize      =
| caption        = Scene from the film. James Kirkwood
| producer       = Adolph Zukor
| based on       =  
| writer         = Eve Unsell (scenario)
| starring       = Mary Pickford
| music          =
| cinematography = Emmett A. Williams
| editing        =
| distributor    = Paramount Pictures
| released       =   reels (5,165 feet)
| country        = United States
| language       = Silent film (English intertitles)
}} Famous Players James Kirkwood. The film is based on a novel, The Eagles Mate, by Anna Alice Chapin. It is a surviving film. 

==Cast==
*Mary Pickford - Anemone Breckenridge James Kirkwood - Lancer Morne
*Ida Waterman - Sally Breckenridge
*Robert Broderick - Abner Morne

unbilled
*Harry C. Browne - Fisher Morne
*Helen Gilmore - Hagar Morne
*J. Albert Hall - 
*R. J. Henry -
*Jack Pickford - A young clansman

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 


 