Loves of an Actress
{{Infobox film
| name           = Loves of an Actress
| image          =
| caption        =
| director       = Rowland V. Lee
| producer       = Paramount Famous Lasky Corporation
| writer         = Ernest Vajda(screen story) Rowland V. Lee(scenario) Julian Johnson(intertitles)
| starring       = Pola Negri Nils Asther
| music          = Karl Hajos
| cinematography = Victor Milner
| editing        = Robert Bassler E. Lloyd Sheldon
| distributor    = Paramount Pictures
| released       = August 18, 1928
| runtime        = 80 minutes
| country        = USA
| language       = Silent
}} lost   1928 silent film romance directed by Rowland V. Lee and starring Pola Negri. It was produced by Adolph Zukor and Jesse Lasky with the distribution through Paramount Pictures. The film had a soundtrack of either Vitaphone or Movietone of music and sound effects.  

==Cast==
*Pola Negri - Rachel
*Nils Asther - Raoul Duval
*Mary McAllister - Lisette Richard Tucker - Baron Hartman
*Philip Strange - Count Vareski
*Paul Lukas - Dr. Durande
*Nigel De Brulier - Samson
*Robert Fischer - Count Moorency
*Helen Giere - Marie(as Helene Giere)
*Dean Harrell - ?_uncredited

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 


 
 