The White Shadow (film)
{{Infobox film
| name           = The White Shadow
| image          = 
| caption        = 
| director       = Graham Cutts Alfred Hitchcock (uncredited)
| producer       = Michael Balcon Victor Saville Michael Morton (novel Children of Chance)
| starring       = Betty Compson Clive Brook Henry Victor A.B. Imeson|A. B. Imeson
| music          = 
| cinematography = 
| editing        = 
| studio         = Gainsborough Pictures Selznick Releasing (US)
| released       = August 1923 (UK) 13 October 1924 (US)
| runtime        = 82 minutes
| country        = United Kingdom Silent English English intertitles
| budget         = 
}}
The White Shadow (1923 in film|1923), also known as White Shadows in the US, is a British drama film directed by Graham Cutts and starring Betty Compson, Clive Brook, and Henry Victor. 

==Preservation status==
The film was long thought to be lost film|lost. In August 2011, the National Film Preservation Foundation announced that the first three reels of the six-reel picture had been found in the garden shed of Jack Murtagh in Hastings, New Zealand in 1989 and donated to the NFPF. One film can was mislabeled Two Sisters, while the other simply stated Unidentified American Film. Only later were they identified.

The film was restored by Park Road Studios and is now in the New Zealand Film Archive.   

==Plot==
The plot concerns twin sisters, one good, the other evil.   

==Cast==
* Betty Compson as Nancy Brent / Georgina Brent
* Clive Brook as Robin Field
* Henry Victor as Louis Chadwick
* A.B. Imeson|A. B. Imeson as Mr. Brent
* Olaf Hytten as Herbert Barnes
* Daisy Campbell as Elizabeth Brent

==Production== Michael Morton. Woman to Woman, before she returned to the United States. 

The film was made at the Gainsborough Pictures studio in Hoxton, London. 

Writing about the film in 1969, producer Michael Balcon said:
 "Engrossed in our first production  , we had made no preparations for the second. Caught on the hop, we rushed into production with a story called The White Shadow. It was as big a flop as Woman to Woman had been a success."  

==Bibliography==
* Rachel Low, The History of British Film: Volume IV, 1918–1929 (Routledge, 1997)

==References==
 

==External links==
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 


 