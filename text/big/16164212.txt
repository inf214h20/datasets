I'm Going Home (film)
{{Infobox Film
| name           = Im Going Home
| image          = Je rentre à la maison (Im Going Home).jpg
| caption        = French release poster
| director       = Manoel de Oliveira
| producer       = Paulo Branco   
| writer         = Manoel de Oliveira
| starring       = Michel Piccoli Catherine Deneuve John Malkovich
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       = 14 August 2002  (United States) 
| runtime        = 90 minutes
| country        = France Portugal French English English
| budget         = 
| gross          =  $140,872 
}}
Im Going Home ( ,  ) is a 2001 French-Portuguese film written and directed by Manoel de Oliveira.

==Plot==
Gilbert Valence (Michel Piccoli) is a grand old theatre actor who receives the shocking news that his wife, daughter, and son-in-law have been killed in a car accident. As time passes, Valence busies himself with his daily life in Paris, turning down unsuitable roles in low-brow television productions and looking after his 9-year-old grandson. When an American filmmaker (John Malkovich) miscasts him in an ill-conceived adaptation of James Joyces Ulysses (novel)|Ulysses, Valence finds himself compelled to make a decision about his life. 

==Cast==
* Michel Piccoli - Gilbert Valence
* Catherine Deneuve - Marguerite
* John Malkovich - John Crawford, Film Director
* Antoine Chappey - George
* Leonor Baldaque - Sylvia
* Leonor Silveira - Marie
* Ricardo Trêpa - Guard
* Jean-Michel Arnold - Doctor
* Adrien de Van - Ferdinand
* Sylvie Testud - Ariel
* Isabel Ruth - Milkmaid
* Andrew Wale - Stephen
* Robert Dauney - Haines
* Jean Koeltgen - Serge
* Mauricette Gourdon - Guilhermine, the Housekeeper

==Reception== Globos de Ouro. Michel Piccoli was nominated for Best Actor at the 2001 European Film Awards.
 Ionesco and The Tempest are frankly de trop, but this patient detailing of an actors life has a fascination akin to watching a sun slowly disappear beneath the horizon." 

==References==
 

==External links==
*  
*  
*  

 
 

 
 
 
 
 
 
 


 
 