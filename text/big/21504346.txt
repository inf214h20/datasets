A Little Bit of Heaven (1940 film)
{{Infobox film
| name           = A Little Bit of Heaven
| image          = ALittleBitOfHeaven1940Poster.jpg
| caption        = Film poster
| director       = Andrew Marton
| producer       = Joe Pasternak
| writer         = Harold Goldman Grover Jones
| starring       = Gloria Jean
| music          = Charles Previn
| cinematography = 
| editing        = 
| distributor    = Universal Pictures
| released       =  
| runtime        = 
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
A Little Bit of Heaven is a 1940 musical film starring soprano singer Gloria Jean. The story casts Gloria as a young member of a large Irish working-class family who becomes a singing sensation on a local radio station. The familys new-found wealth causes some discord until the family realizes that their closeness is what they value the most.

The films title comes from a traditional Irish song that Gloria Jean sings at a family gathering. Her 2005 biography is similarly titled: Gloria Jean: A Little Bit of Heaven. The film is an unofficial sequel to 1939s The Under-Pup, featuring several cast members from that film, but as different characters. The group of girls in The Under-Pup was called "The Penguins", and Gloria Jean sings the groups song early in this film. 

==Cast==
*Gloria Jean - Midge Loring
*Robert Stack - Bob Terry
*Hugh Herbert - Pop Loring
*C. Aubrey Smith - Grandpa
*Stuart Erwin - Cotton 
*Nan Grey - Janet Loring
*Eugene Pallette - Herrington
*Billy Gilbert - Tony
*Kenneth Brown - Tonys son 
*Billy Lenhart - Tonys son 
*Nana Bryant - Mom Loring 
*Tommy Bond - Jerry 
*Charles Previn - Radio Conductor 
*Kitty ONeil - Mrs. Mitchell 
*Helen Brown - Herringtons secretary

== External links ==
*  

 
 
 
 
 
 
 

 