North of Hudson Bay
 
{{Infobox film
| name           = North of Hudson Bay
| image          = 
| caption        = 
| director       = John Ford
| producer       = 
| writer         = Jules Furthman
| starring       = Tom Mix Kathleen Key
| cinematography = Daniel B. Clark
| editing        =  Fox Film Corporation
| released       =  
| runtime        = 50 minutes
| country        = United States  Silent English intertitles
| budget         = 
}}

North of Hudson Bay is a 1923 American action film directed by John Ford. Approximately 40 minutes of footage still exist.    

==Cast==
* Tom Mix - Michael Dane
* Kathleen Key - Estelle McDonald Jennie Lee - Danes mother
* Frank Campeau - Cameron McDonald
* Eugene Pallette - Peter Dane
* Will Walling - Angus McKenzie
* Frank Leigh - Jeffrey Clough
* Fred Kohler - Armand LeMoir

==See also==
*Tom Mix filmography

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 


 
 