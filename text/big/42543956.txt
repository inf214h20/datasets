Space Travelers (2000 film)
{{Infobox film
| name           = Space Travelers 
| image          = 2000spacetravelers.jpg
| film name      = スペーストラベラーズ
| alt            = 
| caption        = Space Travelers  theatrical poster
| director       = Katsuyuki Motohiro
| producer       = Toru Horibe Masatake Kondô Yuji Usui Yasushi Sutô
| writer         = Yoshikazu Okada
| screenplay     = 
| story          = 
| based on       =   
| narrator       = 
| starring       = Takeshi Kaneshiro Eri Fukatsu Masanobu Ando Hiroyuki Ikeuchi Sawa Suzuki Masahiro Kômoto Teruo Takeno Toshio Kakei Ken Watanabe
| music          = Akihiko Matsumoto
| cinematography = Osamu Fujiishi
| editing        = Takuya Taguchi Fuji Television Network Robot Films Toei Company Toho Co., Ltd.
| released       =   
| runtime        = 125 minutes
| country        = Japan
| language       = Japanese 
| budget         = 
| gross          = JPY ¥1,130,000,000.
}}
 Fuji Television Network and Robot films, which was both companies were also involve with the production of the live featured film.

== Plot ==
Three men, who grew up together in the orphanage, wanting to finally realize their childhood dreams of living in paradise decide to rob a bank together. Nishiyama (Takeshi Kaneshiro), the leader, Takamura (Hiroyuki Ikeuchi), a very self-contained guy and Fujimoto (Masanobu Ando), an otaku obsessed by an anime called "Space Travelers". Their plan is very simple, enter the bank during closing time, take the money and leave within 5 minutes, then board a plane to paradise. Unfortunately for them, their plan did not go as planned since the bank manager and security guard decided to lock themselves in the bank vault that wont open until the next day instead of giving them the money. Not able to reach the money and surrounded by cops, they are stuck in the bank. They then decide to use the remaining customers and staff as hostages to gather some time to plan an escape, but they soon realize that an escape is almost impossible.

With the time passing, the hostages begin to sympathize with the three robbers and decide to help them. Nishiyama gets an idea that could help them, try to fool the police into thinking that they are more than what they are in reality. They all decide to take on the identity of an member of Fujimoto favorite anime and calling themselves "The Space Travelers". Strangely every one has a character that really corresponds to their personality and they will soon begin to develop a real association to their character, forgetting their own personality, as they all begin to be more active in the new improvised band of bank robbers. This matter doesnt help the police at all, confusing them even more as they try to solve the delicate hostage situation.  

== Cast ==

=== The Space Travelers ===
*Takeshi Kaneshiro as Nishiyama ("Hayabusa Jetter")
:The leader of the three men robbing the bank. He does not believe in a broken home and thinks people should not divorce, even if they dont love each other anymore parents should stay together for the childrens sake. 
*Masanobu Ando as Makoto Fujimoto ("Black Cat")
:He is obsessed with an anime called "The Space Travelers". He has an outgoing personality. He and Midori hit it off through their conversations together throughout the hostage situation and develop an attraction for one another later on.
*Hiroyuki Ikeuchi as Takamura ("Dragon Attack")
:The quiet one of the three robbers. He is the only one in the group armed with an real gun. He doesnt say much but seems to be the one that reacts the fastest amongst the three robbers. 
*Eri Fukatsu as Midori ("Irene Bear")
:A bank teller who is engaged to her fellow bank co-worker Kiyoshi Nonomura. She later realizes that her fiancee doesnt really love her when he is hiding in the bank vents and she pleads with him to save her, instead he escapes and leaves her behind. 
*Sawa Suzuki as Kimiko Fukaura ("Gold Papillon")
:Koichi Fukauras soon to be ex-wife. She and her soon to be ex-husband go to the bank during closing time hoping to split their assets in half right away so that they can rid each other from their life right after. 
*Toshio Kakei as Koichi Fukaura ("Hoi")
:Kimiko Fukauras soon to be ex-husband. He and his soon to be ex-wife go to the bank during closing time hoping to split their assets in half right away so that they can rid each other from their life right after. 
*Masahiro Komoto as Takahiro Shimizu ("Karl Hendrix")
:Midoris fellow bank co-worker. He is a soft spoken man who doesnt speak up much. He finally has an outburst when he thinks Nishiyama had murdered the bank manager and security guard in the bank vault, because of his outburst two traffic officers ticketing cars in front of the bank get tipped off of an robbery in process and contact the police force.   
*Teruo Takeno as Shintaro Kurasawa ("Electric Sunny")
:A bank customer who owns an electronic and appliance repair shop who is stuck at the bank during closing time hoping an employee can help him resolve his issue with his ATM card. Unfortunately all the employees cannot and will not help him because it was closing time at the bank. 
*Ken Watanabe as Sakamaki ("Crusher")
:A bank customer who goes to the bank during closing time requesting for an huge amount of money to be currency exchanged. The bank employees tells him to come back the next day since the bank vault has already been closed. He has ties to terrorist groups which makes the police initially think he is the bank robber.

=== Law enforcements ===
*Jin Nakayama as Sudo
*Koh Takasugi as SAT Taicho
*Shigemitsu Ogi as SIT Taicho
*Isao Nonaka as Det. Arai
*Yozaburo Ito as Det. Nishikawa
*Masayasu Kitayama as Officer
*Bokuzo Masana as Officer

=== Bank employees and others===
*Guts Ishimatsu as Shoda
*Ren Osugi as Tsuneda
*Masatoshi Hamada as Kiyoshi Nonomura
*Naoto Kokaji as Toyoyama
*Maiko Shobu as Momoe Kimura
*Runa Ito as Minako
*Kikuyo Saito as Atsuko Kikushima
*Aki Hoshino as Yukari Unno
*Megumi Ujiie as Moe Susuki
*Miho Ogata as Ayana Nagata
*Tatsuro Hoshiyama - Danshi Kouin Amemiya

=== J-Six Babys ===
A boyband who plans to fake their group disbandment in order to trick their fans and make a huge sum of money with their farewell tour, only to regroup a few month later and make another huge sum of money with their comeback tour. They hold a press conference to announce their disbandment only to be interrupted and all the reporters leaving their press conference to cover the bank hostage story instead.
*Shoko Takada - band manager
*Jovijova 
*Chikara Ishikura
*Meisui Kinoshita
*Satoshi Sakata
*Tomoharu Hasegawa 

== Anime ==
An full length straight to video animation film called "Space Travelers: The Animation" was released on June 23, 2000 to tied back to the characters each of the main characters in the live action film takes on and the anime Fujimoto is obsessed with.

==See also==
 

==References==
 

== External links ==
* 
*  
* 
*http://www.fareastfilms.com/reviewsPage/Space-Travelers-1678.htm
*http://asianwiki.com/Space_Travelers

 

 
 
 
 
 
 
 
 
 