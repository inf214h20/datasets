Delayed Action
{{Infobox film
| name           = Delayed Action
| image          = "Delayed_Action"_(1954).jpg
| image_size     = 
| caption        =  John Harlow
| producer       = Robert S. Baker Monty Berman
| writer         = Geoffrey Orme
| narrator       =  Robert Ayres June Thorburn Alan Wheatley  Bruce Seton
| cinematographer = Gerald Gibbs
| music          =  John Lanchbery
| editor         =  Bill Lewthwaite
| studio         = Kenilworth Film Productions
| distributor    = General Film Distributors
| released       = July 1954 (UK)
| runtime        = 58 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| preceded_by    = 
| followed_by    = 
}} John Harlow Robert Ayres, June Thorburn and Alan Wheatley. 

==Plot==
Two criminals do a deal with a suicidal man, who will confess to crimes they have committed before killing himself. However he subsequently has a change of heart. 

==Cast== Robert Ayres ...  Ned Ellison
* June Thorburn ...  Anne Curlew
* Alan Wheatley ...  Mark Cruden
* Bruce Seton ...  Sellars Michael Balfour ...  Honey
* Michael Kelly ...  Lobb John Horsley ...  Worsley
* Olive Kirby ...  Angela Bentley
* Ballard Berkeley ...  Insp. Crane Ian Fleming ...  Dr. Jepson
* Myrtle Reed ...  Jackie
* Dennis Chinnery ...  Bank cashier Charles Lamb ...  Bank clerk
* Frederick Leister ... Sir Francis

==Critical reception==
*TV Guide wrote, "robbers pay suicidal writer Ayres to confess to their crime and kill himself should their scheme fail. An interesting premise in an otherwise dull movie."   The Saint and Randall and Hopkirk (Deceased)."  

==References==
 

==External links==
* 

 
 
 
 
 
 
 