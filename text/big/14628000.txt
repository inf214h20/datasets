The Other Side of the Mirror (film)
 
The Other Side of the Mirror: Bob Dylan at the Newport Folk Festival is a  .
 controversial electric set from 1965.

This film features previously unseen footage, chronicling the changes in Dylans style when he appeared at Newport in three successive years. This film was broadcast by  , Federico García Lorca|Lorca, T. S. Eliot and Ezra Pound never achieved. He reached a mass audience with poetry." {{cite news
| url = http://www.usatoday.com/life/music/news/2007-09-05-dylan-cover_N.htm
| title = Dylan projects are blowin’ in| publisher = USA Today| date = 2007-09-06 | first=Edna | last=Gundersen | accessdate=2010-05-25}} 

==Critical Reception==
The documentary garnered positive reviews from critics. In his review for The New York Times, A.O. Scott picked out Like A Rolling Stone and Its All Over Now, Baby Blue as being "great" versions that "provide a thrilling climax to the film without quite overshadowing the others." 

==Chapter List of DVD==

===1963===
*"North Country Blues" (7/26 afternoon workshop)
*"With God On Our Side" (with Joan Baez&mdash;7/26 afternoon workshop spliced with 7/28 night performance)
*"Talkin World War III Blues" (7/26 night performance)
*"Who Killed Davey Moore?" (7/26 night performance)
*"Only a Pawn in Their Game" (7/26 night performance)
*"Blowin in the Wind" (with The Freedom Singers, Joan Baez, and Peter, Paul and Mary&mdash;7/26 night performance)

===1964===
*"Mister Tambourine Man" (7/24 afternoon workshop)
*Johnny Cash sings "Dont Think Twice, Its All Right" (night performance)
*Joan Baez sings "Mary Hamilton" as Bob Dylan (7/24 night performance)
*"It Aint Me Babe" (with Joan Baez&mdash;7/24 night performance)
*Joan Baez interview
*"With God On Our Side" (with Joan Baez&mdash;7/26 night performance) Chimes of Freedom" (7/26 night performance)

===1965===
*"If You Gotta Go, Go Now" (7/24 afternoon workshop)
*"Love Minus Zero/No Limit" (7/24 afternoon workshop) 
*Daytime rehearsal with electric band
*"Maggies Farm" (7/25 night performance)
*"Like a Rolling Stone" (7/25 night performance)
*"Mr Tambourine Man" (7/25 night performance)
*"Its All Over Now, Baby Blue" (7/25 night performance)

== Notes ==
 

==External links==
* 

 

 
 
 
 
 
 