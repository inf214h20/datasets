The Gold of Naples
{{Infobox film
| name           = LOro di Napoli
| image          = LorodiNapoli.jpg
| image_size     =
| caption        = Poster
| director       = Vittorio De Sica
| producer       = Dino De Laurentiis  Marcello Girosi Carlo Ponti 
| writer         = Giuseppe Marotta (novel) Vittorio De Sica
| narrator       = 
| starring       = Silvana Mangano Sophia Loren Paolo Stoppa Totò
| music          = Alessandro Cicognini
| cinematography = Carlo Montuori
| editing        = Eraldo Da Roma   
| distributor    = Ponti-De Laurentiis Cinematografica, Paramount Pictures
| released       = 11 February 1957 (USA)
| runtime        = 131 minutes
| country        = Italy
| language       = Italian
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}} 1954 cinema Italian anthology film directed by Vittorio De Sica. It was entered into the 1955 Cannes Film Festival.   

==Plot==
The film is a tribute to   exploited by a Criminal|hoodlum; an unfaithful pizza seller (Loren) losing her husbands ring; the funeral of a child; the impoverished inveterate gambler Count Prospero B. being reduced to force his concierges preteen kid to play cards with him (and regularly being defeated); the unexpected and unusual wedding of Teresa, a prostitute; the exploits of "professor" Ersilio Micci, a "wisdom seller" who "solves problems".

==Cast==
*Silvana Mangano ...  Teresa (segment "Teresa") 
*Sophia Loren ...  Sofia (segment "Pizze a credito") 
*Eduardo De Filippo ...  Don Ersilio Miccio (segment "Il professore") 
*Paolo Stoppa ...  Don Peppino, il vedovo (segment "Pizze a credito") 
*Erno Crisa ...  Don Nicola (segment "Teresa") 
*Totò ...  Don Saverio Petrillo (segment "Il guappo") 
*Lianella Carell ...  Carolina, sua moglie (segment "Il guappo") 
*Giacomo Furia ...  Rosario, marito di Sofia (segment "Pizze a credito") 
*Tina Pica ...  La donna anziana (segment "Il professore") 
*Alberto Farnese ...  Alfredo, lamante di Sofia (segment "Pizze a credito") 
*Tecla Scarano ...  Lamica del vedovo (segment "Pizze a credito")

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 
 


 
 