Abandoned (2001 film)
{{Infobox film
| name           = Torzók
| image          = 
| caption        = 
| director       = Árpád Sopsits
| producer       = Ferenc Kardos László Kántor
| writer         = Árpád Sopsits
| screenplay     = 
| story          = 
| based on       =  
| starring       = Tamás Mészáros Szabolcs Csizmadia Attila Zsilák Péter Müller
| music          = Tamás Görgényi Peter Pejtsik
| cinematography = Péter Szatmári
| editing        = Béla Barsi
| studio         = 
| distributor    = 
| released       =  
| runtime        = 100 minutes
| country        = Hungary
| language       = Hungarian
| budget         = 
| gross          = 
}}

Abandoned ( ) is a 2001 Hungarian film directed by Arpád Sopsits. It was Hungarys submission to the 74th Academy Awards for the Academy Award for Best Foreign Language Film, but was not accepted as a nominee.      

==Plot==
Abandoned at an orphanage by his recently divorced father, Aron endures a life full of cruelty and despair, punctuated by beating from the orphanage staff and ridicule from the other boys. His only friend is his classmate Attila, who helps him discover love and gives him strength to fight back.

==See also==
 
*Cinema of Hungary
*List of submissions to the 74th Academy Awards for Best Foreign Language Film

==References==
 

==External links==
*  

 

 
 
 
 
 

 
 