Broken Chains (film)
{{Infobox film
| name           = Broken Chains
| image          = Broken chains 1.jpg
| image size     = 
| caption        = Theatrical poster
| director       = Allen Holubar
| producer       = Allen Holubar
| writer         = Carey Wilson (scenario)
| based on       =  
| narrator       = 
| starring       = Colleen Moore Malcolm McGregor Ernest Torrence
| music          = 
| cinematography = Byron Haskins
| editing        =  Goldwyn Pictures Corporation Goldwyn Distributing Company
| released       =  
| runtime        = 70 mins.
| country        = United States
| language       = Silent English intertitles
| budget         = 
| gross          =
}}
 silent Western Goldwyn Pictures Corporation and the Chicago Daily News. 

==Story==
  and Malcolm McGregor in Broken Chains.]]

Wealthy Peter Wyndham is useless in attempting to prevent the theft of Hortense Allen’s jewelry. A butler is killed during the robbery, and unable to face his cowardice, Peter heads west. He takes a job working for his fathers lumber mill. Meanwhile, elsewhere Mercy Boone’s newborn child has died. Boyan Boone, her husband is callous towards the loss. He is a thug, and ne’er-do-well with a band of thieves working with him. When Mercy attempts to escape she meets Peter before Boyan returns her to his cabin where he chains her. Peter finds her and they begin a romance under Boylans nose. Boylan learns and beats up Peter, who summons the strength to fight him for the honor of Mercy.

==Cast==
*Malcolm McGregor - Peter Wyndham
*Colleen Moore - Mercy Boone
*Ernest Torrence - Boyan Boone
*Claire Windsor - Hortense Allen
*James Marcus - Pat Mulcahy
*Beryl Mercer - Mrs. Mulcahy
*William Orlamond - Slog Sallee
*Gerald Pring - Butler

==Background==
  and Malcolm McGregor in Broken Chains.]]
 Goldwyn Pictures Corporation held a scenario writing contest with first prize winning $10,000 through the Chicago Daily News. Miss Lavina Henry, nom de plume for Miss Winifred Westover (Kimball), of Appalachia won the contest with her story "Broken Chains". The story was then used as the basis for the films scenario written by The scenario was written by Carey Wilson.    Allen Holubar was brought in from Associated First National for the project. 
 Flaming Youth. 

==Production notes== Santa Cruz in an area known as Poverty Flats.   Allen Holubar, Cedric Gibbons, and others visited the area scouting locations”. 

==Reception==
A critic for the Santa Cruz Evening News wrote, "Colleen Moore, as Mercy Boone in Broken Chains... attains new laurels as an emotional actress. Her work is thoroughly convincing during the difficult sequences, especially those in the cabin of Boykan (sic) Boone, her renegade husband, when, chained by an ankle to a cleat on the floor, she witnesses the life and death struggle between Boone and Ted Wyndham."  

==References==
 

==Bibliography==
*Jeff Codori (2012), Colleen Moore; A Biography of the Silent Film Star,  , Print ISBN 978-0-7864-4969-9, EBook ISBN 978-0-7864-8899-5.

==External links==
 
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 