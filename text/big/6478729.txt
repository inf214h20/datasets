The ButterCream Gang
{{Infobox Film
| name           = The ButterCream Gang
| image          = 
| image_size     = 
| caption        = 
| director       = Bruce Neibaur
| producer       = Forrest S. Baker III Don A. Judd
| writer         = Forrest S. Baker III
| narrator       = 
| starring       = Jason Johnson Michael D. Weatherred Kevin Bushong
| music          = Kurt Bestor
| cinematography = T.C. Christensen Stephen L. Johnson Lori Petersen
| distributor    = Feature Films for Families PorchLight Entertainment
| released       =  
| runtime        = 93 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}

The ButterCream Gang is a 1992 childrens direct-to-video film produced by Feature Films for Families, with music by Kurt Bestor.

==Plot==
 
Pete Turner (Weatherred) is the leader of a good-deed group, the ButterCream Gang, in the small rural town of Elk Ridge. Before he moves away to live with his aunt (affectionately known as Aunt Spaghetti due to her famous ketchup spaghetti sauce) in Chicago, Pete bikes over to their treehouse, searching for Scott Carpenter (Johnson), Eldon Flowers (Blaser), and Lanny Glenn (Glenn). He climbs up to the tree, being surprised by the rest of the gang. After a brief discussion, Pete nominates Scott to become the new leader of their small band. At the bus station, Pete says good-bye to all his family and friends, then departs on the bus to Chicago. 

Throughout the school year, Scott and Pete keep in touch by writing letters. As President of the ButterCreamers, though shortest in stature, Scott exceeds the others in honesty and conviction. Pete and Scott exchange several letters, but after a while the two lose touch as Pete befriends a local gang. One night, Pete and his two friends are arrested for committing a petty crime in an alley. A while after the arrest, Pete returns home with his gang, the Blades, to an angry Aunt Maria. After Pete sends his gang away to save them from being scolded, Aunt Maria reveals to Pete a letter indicating that he had been expelled from school for breaking into a kids locker (cabinet)|locker. Aunt Maria decides to send Pete back to Elk Ridge, and says that she and Petes grandfather, Mr. Turner, agree that going back to Elk Ridge would be the best thing for him.

One day, Scott, Eldon and Lanny go over to Mr. Turners house, hoping to find Mr. Turner. Instead, they are surprised to find that Pete was the one who was home. They are also surprised by Petes new "threads".  After the boys ask Pete numerous questions about his lack of contact, he replies by offering to buy them treats at Mr. Graffs store. There, Pete and Mr. Graff greet each other for the first time since Pete had returned to Elk Ridge, and Scott suspects Pete shoplifting some treats for the gang. While the boys wait outside, Scott notices that the treats he gave to the gang were not paid for.
 suspicions about Pete. Eldon and Lanny doubt him. After mowing the lawn, Scott goes to borrow Margarets report she had written on gangs in big cities. That night, Scott encounters Pete in his room. Pete confesses stealing, and that he would apologize and pay for the treats he stole, and would clean up his act. However this would prove to be a deception by Pete for unknown reasons.
 bully young children at the play park, and a whole bunch of other dirty deeds.

The afternoon before the dance, Scott, Lanny, and Eldon are in their treehouse discussing the issue of Petes lack of help toward the gang. They come to an agreement that if Pete did not start helping that he would be dismissed. Margaret later comes to the treehouse, reporting to the gang the actions Pete and his friends had recently performed. Immediately afterward, she collects on the favor Scott owed her, which was to take her to the dance that night. While doing his paper route, Scott passes the cemetery on his bike, and catches Pete and his friends all biking in a circle around Margaret, verbally abusing her. Scott breaks up the circle, knocking one of Petes friends off his bike to the ground. After Scott insists that Margaret leaves, the boys claim that theyre having a little fun. After the boys insult Margaret behind her back, Scott delivers the message to Pete what he and Eldon and Lanny had agreed on. Pete then removes himself from Scotts gang, firmly claiming that he now had his own gang. After Pete and his friends leave, Scott then goes home to get ready for the dance.

That night during the dance, in which Eldon and Lanny also attend, Pete and his friends, who happen to be at the dance and notice Scott, light a mini firecracker, which Pete throws right near where Scott and Margaret are dancing. When it explodes, this interrupts the dance. Scott then catches Pete and his gang running off, and runs after them, followed by Eldon and Lanny. Violence escalates, as Scott and Pete agree to a fight the next day in the field.

The next day at Church, Scotts baseball coach and Pastor Reverend Willke gives the "Was That Somebody You" speech. Specifically he claims that the concepts an eye-for-eye or tooth-for-tooth have been replaced with "turn the other cheek". During this church session, Scott suspects that Willke had known about the fight Scott had planned with Pete, by the way Willke was looking at him while giving the speech, which tied right into Scotts plan. Eldon and Lanny both have been thinking the same thing, but Scott calms them down by explaining that he heard Willke practicing earlier in the week. Later that day, while Pete and his friends are waiting in the field for the fight, Eldon and Lanny arrive, claiming that Scott wanted to meet Pete alone by the rusted truck in another field. Pete then goes over, expecting a fight, but Scott, following the lecture Willke had given, claims there is a better way than fighting. When Pete claims hes afraid of nothing, Scott then challenges Pete to spend the day with him, if he had the guts.

The next day, Pete follows up, and the two spend the day fishing in the swimming hole, as well as Human swimming|swimming, jumping off the diving platform they built, swinging off the rope, and skipping stones, generally reliving the good times they had together as best friends. At the end of the day, Scott and Pete are in the treehouse, reflecting on the day. Scott then further confronts Pete on his recent behavior. The conversation goes in different directions, the tension mounting. Before Scott departs, Pete enforces to Scott that there are only two types of people: friends, and your enemies.

The next day, Scott and his baseball team, the Red Sox, warm up for their championship game against the Braves. For the majority of the game, the Red Sox are dominant, leading by 6 home runs. When Pete shows up to the game, his presence somehow hurts the Red Soxs momentum as the Braves come back to even the score. When Scott is the last chance the Red Sox have of re-taking the lead, his concentration is thrown off by Pete, who jinxes him while Scott hits the ball, as the catcher catches the ball, eliminating Scott. An angered Scott then runs back to the fence where Pete stood on the other side, scolding him for messing him up. Mr. Turner, who was at the game, apologizes to Scott for Petes behavior. Scott angrily replies, stating that Mr. Turner could send Pete back to Chicago where he belonged. Coach Willke, who is furious with Scott, removes him from the game, claiming he wanted to see him after. An ashamed Scott apologizes, and Mr. Turner forgives him. The Red Sox then lose the championship game in dramatic fashion. 

After the game, Scott goes to meet Coach Willke, who is understanding of Scott losing his temper after costing his team a possible shot at winning the championship. While throwing Scott a few balls for him to swing, he teaches him about Mahatma Gandhis use of nonviolent resistance to deal with opposition. He also stated that an "eye-for-an-eye" makes the world blind. Scott finally hits a ball out of the ballpark, re-claiming his good hit he had seemed to lose in the game. 

One day, when Scott is heading over to the local Annual Picnic on his bike, he is stopped by Pete in the bush. While Pete tells Scott to never, ever threaten him, Petes two friends grab him off his bike, and take him into the bushes to beat him. Scott later shows up to the picnic with his nose bleeding. He meets up with Lanny and Eldon, who are concerned with Scotts bleeding nose. Scott tells them not to worry, and asks them to do Scotts paper route this afternoon without any explanation. He then heads back to the field, looking around for Pete. When they find each other, Pete is ready to fight. Scott, once again following the wisdom of his coach, walks away whilst stating to Pete that an "eye-for-an-eye" makes the world blind. At home, Scott has another discussion with his parents. His father then teaches Scott about unconditional love. After reporting the stolen treats to Mr. Graff, claiming that Pete felt he deserved the items he stole for all the help Pete gave around the store. Graff then claims that he would give Pete the treats, in appreciation for the help, and also to prevent him from stealing. Scott then uses what his father taught him to try to redeem his friend. Scott goes over to Pete and his friends, claiming that Pete was his friend, and would always be his friend. When Pete challenges Scott to take his bike from him, Scott gives it to him. Ever since, Petes gang repays his kindness with malicious pranks and assaults.

Pete attempts to get back to Chicago by staging his "kidnapping".  With help from a seedy drifter, money is demanded from Petes grandfather under threat of Petes life.  But Scott and his friends, taking this at face value, attack the pretend kidnapper, who is no match for Eldons patented "Earthquake Stomp".

Angrily, Pete tells them off and runs off to the grocery store, where he demands money.  The grocer refuses to let him rob him by offering to give him the money. Flabbergasted by this, Pete attacks the stores goods—terrorizing the shoppers—and sends treats flying across the room.  After Scott arrives, Pete breaks down and runs away. 

Later in the year, Scott and his friends are pleasantly shocked to read in a newspaper article that Pete has started a new ButterCream Gang in Chicago, keeping it a complete secret from everyone despite their extreme worry. Later at the treehouse, Margaret suggests that the Buttercreamers start letting girls join, and she kisses Scott on the cheek and tells him to think about it. Then the Buttercreamers receive word that Mrs. Jenkins fell down and the movie ends when they go to help her.

==Explanation of title== the war. churn butter with their men gone. A group of boys began going around town to help them do this (hence the name) and other chores. Over the years, the group expanded to four members and eventually came to do all sorts of helpful things for the locals.

 Draper and Riverton, Utah.

==Availability==

The ButterCream Gang was released on VHS in 1992 by Feature Films for Families (Murray, Utah;  ). 

In 1995, it was released with the title The Treehouse Gang: A ButterCream Gang Adventure on VHS by Feature Films for Families (Salt Lake City, Utah;  ).

A DVD version was released in 2002, 2003, and 2004 by Feature Films for Families (Murray, Utah;  ).

==Sequels==
The ButterCream Gang in Secret of Treasure Mountain was the 1993 sequel.
Also The ButterCreamers, have made numerous appearances in other Feature Films For Families movies, including "The Penny Promise", this is a sort of running continuity with the company, constantly recruiting the ButterCreamers to influence the good of community, friendship and trust.

==References==
 

==External links==
* 

 
 
 