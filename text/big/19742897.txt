Pavalakkodi
 
{{Infobox film|
  name           = Pavalakkodi|
  image          = Pavalakkodi.jpg|
  caption        = S. S. Mani Bhagavathar as Lord Krishna and M. K. Thyagaraja Bhagavathar as Arjuna in  Pavalakkodi|
  director       = K. Subramanyam|
  writer         = |
  starring       = M. K. Thyagaraja Bhagavathar S. D. Subbulakshmi R. S. Sundarambal  S. S. Mani Bhagavathar  |
  producer       =  S. M. Lakshmana Chettiar|
  distributor    = |
  music          = Papanasam Sivan |
  cinematography =  |
  editing = |
  released   = 1934 in film|1934|
  runtime        = | Tamil }}

Pavalakkodi ( ) is a 1934  .   

==Production== Malaya and Adyar was chosen for filming. Subramanyam established his studio at the spot and called it Meenakshi Cinetones. Filming was done outdoors using sunlight. Scenes were shot continuously when the sun was shining brightly and breaks were called when it became cloudy. Whenever the cloud cover broke, cast and crew rushed to resume filming abandoning their food packets. The abandoned food attracted a lot of crows from the wooded Adyar surroundings. The cawing of crows was a nuisance to recording (songs and dialogues were recorded directly on the spot). Subramanyam hired an Anglo Indian named Joe to fire his rifle in the air and shoo away the crows. Joe was credited in the film credits as "Crow Shooter". The studio did not have its own lab and scenes were shot without knowing how they would turn out. All the post production work was done at Bombay. 

==Plot==
Pavalakkodi tells the mythical love story of Arjuna (Thyagaraja Bhagavathar) and Pavalakkodi (S. D. Subbulakshmi), the princess of Coral Island.

==Cast and crew==
* M. K. Thyagaraja Bhagavathar - Arjuna
* R. S. Sundarambal - Subhadra
* S. S. Mani Bhagavathar - Lord Krishna
* S. D. Subbalakshmi - Princess Pavalakkodi
* K. K. Parvati Bai
* K. Subramanyam - Direction
* Papanasam Sivan - Music, Lyrics
* Joe - Crow Shooter  

==Soundtrack==
The film had 55 songs,  of which 22 were sung by M. K. Thyagaraja Bhagavathar.   Some of the popular songs from the movie are:

* Somasekara
* Kanna Kariyamugil Vanna
* Vanithaikul Uyirvaana
* Munnam oru Sanyasi Vadivaaki

==Reception==
Pavalakkodi was a box office hit and ran for nine months.  It was also one of the earliest commercially successful Tamil films. On the successful completion of the film, Subramanyam married S. D. Subbulakshmi as his second wife.  Pavalakkodi set the stage for M. K. Thyagaraja Bhagavathars eventful career as the first superstar of Tamil Cinema. The single remaining print of this film has been preserved at National Film Archives, Pune  

==References==
 

 

 
 
 
 