Their First Mistake
{{Infobox film 
| image          = Theirfirstmistaketitlecard.jpg
| caption  =
| name           = Their First Mistake George Marshall
| producer       = Hal Roach
| writer         = H.M. Walker
| starring       = Stan Laurel Oliver Hardy
| released       =  
| country        = United States
| runtime        = 20 59"
| language       = English
}} George Marshall and produced by Hal Roach. 

== Plot ==
Mrs Hardy is annoyed that her husband Oliver seems to spend more time with his friend Stanley than with her. After a furious argument, Mrs Hardy says that she will leave him if Ollie goes out with Stan again. Stan suggests that Ollie adopts a baby, which he does. Unfortunately, his wife has left their apartment on returning, and a process server delivers a paper informing Ollie that she is suing him for divorce, naming Stan as co-respondent. The boys are now left to look after the infant on their own.

==Production==
Their First Mistake is a rare film for the duo, as there is no resolution at the end. In the original script, Mrs. Hardy came back to Oliver with another adopted baby, but so much time and money had been spent on adding Laurels improvisations that there was no budget left to film it.

== Note ==
The neighbor in the hall to whom Laurel and Hardy give the cigar is director George Marshall.

== Cast ==
* Stan Laurel
* Oliver Hardy
* Mae Busch
* Billy Gilbert
* George Marshall

==References==
 

==External links==
*  
* 
*  

 
 
 

 
 
 
 
 
 
 
 

 