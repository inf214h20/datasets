Vampire (2011 film)
{{Infobox film
| name           = Vampire
| image          = File:Vampire2011IwaiFilmPoster.jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| film name      = 
| director       = Shunji Iwai
| producer       = 
| writer         = Shunji Iwai
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = Kevin Zegers, Keisha Castle-Hughes, Amanda Plummer
| music          = Shunji Iwai
| cinematography = Shunji Iwai
| editing        = 
| studio         = Convergence Entertainment, Rockwell Eyes, V Project Canada Productions
| distributor    = 
| released       =  
| runtime        = 120 minutes
| country        = United States, Japan
| language       = English
| budget         = 
| gross          = 
}}
Vampire is a 2011 dramatic horror-thriller film that was directed and written by Shunji Iwai. It was first released on January 22, 2011 at the Sundance Film Festival and is the first film he has directed in English.  The movie stars Kevin Zegers as a teacher that believes himself to be a blood-drinking vampire.

==Synopsis== Trevor Morgan), a man who also believes himself to be a vampire, emerges onto the scene and is far more violent than Simon ever dared to be.

==Cast==
*Kevin Zegers as Simon
*Keisha Castle-Hughes as Jellyfish
*Amanda Plummer as Helga Trevor Morgan as Renfield
*Adelaide Clemens as Ladybird
*Yû Aoi as Mina
*Kristin Kreuk as Maria Lucas
*Rachael Leigh Cook as Laura King
*Jodi Balfour as Michaela (initially, hesitatingly calling herself Renée)
*Katharine Isabelle as Lapis Lazuli
*Mandy Playdon as Stella Snyder

==Production==
Iwai was inspired to create Vampire after he "got the idea about a serial killer who was more like a friend to his victims" and liked the question of "if the victims are working with the killer in helping kill themselves, would it be considered murder or aided suicide?".       While further developing the idea for Vampire Iwai wanted to "strip away the romantic idea behind vampirism", but also wanted to explore the idea of a vampire that is "not a supernatural creature but rather a real human being".    He based the character of Simon partially on "the strange habits that we all have" and stated that if he had not come up with the idea of Simon, he would have likely passed on Vampires theme.  Iwai penned the script for Vampire himself. However as Iwai had difficulty with spoken English dialogue, asked the performers to "not follow the script too closely and try to be more spontaneous so that the dialogue would be natural." 

Actor Kevin Zegers was asked to perform in the film after Iwai met Zegers while dining with friends.    Zegers immediately accepted and was one of the first actors brought in.  Filming for Vampire took place in Vancouver, Canada during spring of 2010, and Zegers later stated that the process was very taxing due to the films nature.  Iwai confirmed this, saying that the weather was "always gray and rainy, which can bring the mood down" and that he "gave a lot of freedom to Kevin with his character, Simon, so he really took on the sadness and troubles Simon’s character was going through." 

==Reception== Pikunikku (Picnic), will be right at home with his American debut."    Reviewers for Twitch Film gave predominantly positive reviews,  echoing io9s sentiments and stating that "for those not turned off by some graphic violence and a whole lot of artistic license, there is a lot to like about this portrait of a killer more empathetic than psychopathic." 

===Awards=== Sundance Film Festival (2011, nominated)
*Festival Prize at the Strasbourg International Film Festival (2011, won)
*Special Mention for Feature Film at the Fantasia Film Festival (2011, won) 

==References==
 

==External links==
*  
*   on Fearnet

 

 
 
 
 