Guerillas in Pink Lace
{{Infobox film
| name           = Guerillas in Pink Lace
| image          = 
| caption        =  George Montgomery George Montgomery George Montgomery starring  George Montgomery
| music          = Gene Kauer
| cinematography = Emmanuel I. Rojas
| editing        = Kenneth Crane
| studio          = Mont Productions
| distributor    = 
| released       = 1964
| runtime        = 96 minutes
| country        = Philippines
| language       = English/Japanese
| budget         =
| gross          =
| preceded by    =
| followed by    =
}} Philippine Techniscope invasion of George Montgomery who also produced, directed and co-wrote the screenplay as well as featuring his own father in a role as a Priest.

==Plot== priest Fr evacuating American civilians to Australia when he would rather minister to the troubled souls who will be left behind to face the Japanese.  Murphy supports the priest in his decision by allowing the priest to give him his pass so Murphy can escape to Australia. He gleefully dons a pair of spectacles and a cassock to impersonate the 74-year-old priest.
 troupe of exotic dancers who have scored similar passes due to one of them having sexual relations with a US Army Lieutenant Colonel who had the power to issue the passes.  Facing the wrath of the many Americans unable to leave, they decide to avoid hostility by travelling with Murphy, thinking he is an actual priest; despite the fact that he never wears his cassock correctly and carries a M1911A1 .45 calibre pistol in a shoulder holster.

Murphys bad luck returns when their plane is shot down by the Japanese.  With the survivors separating during the night, Murphy awakes to find himself on a life raft with the five dancers.  They come ashore on one of the many small Philippine islands.  Murphy soon discovers a Japanese radio station but his reconnaissance leads him to believe that the post is only manned by two idiots.  The partys luck changes when Murphy finds a suitable hiding place, a large supply of fresh fruit and water with Murphy able to enter the Japanese outpost to steal food and make a distress call to the US Navy.  Their luck turns to bad again when Murphy finds a large company of Japanese troops.  When they are discovered, Murphy and the girls decide to give the Japanese a party they will never forget with the dynamite they discover in a supply building.

==Cast== George Montgomery as  Murphy aka Father Osgood  
Joan Shawlee as Miss Gloria Maxine  
Valerie Varda as Lori  
Jane Earl  as Jane  
Ruth Earl as Marguerite  
Robin Grace as Josie  
Diki Lerner as a Japanese Private First Class 
Joe Sison as a Japanese Superior Private   
Ken Loring as a gambler  Jack Lewis as Lt. Col Lewis  
Jim Montgomery as Father Osgood

==See also==
* Operation Petticoat Father Goose

==Notes==
 

==External links==
*  

 
 
 
 
 
 
 
 