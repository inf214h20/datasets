Doctor Rhythm
{{Infobox film
| name           = Doctor Rhythm
| image          = Doctor_Rhythm_1938_Poster.jpg
| border         = yes
| caption        = Theatrical release poster
| director       = Frank Tuttle
| producer       = Emanuel Cohen
| writer         = {{Plainlist|
* Richard Connell
* Jo Swerling
}}
| based on       =  
| starring       = {{Plainlist|
* Bing Crosby
* Mary Carlisle
* Beatrice Lillie
* Andy Devine
}}
| music          = James V. Monaco
| cinematography = {{Plainlist|
* Floyd Crosby
* Charles Lang
}}
| editing        = Alex Troffey
| studio         = Major Pictures Corporation
| distributor    = Paramount Pictures
| released       =  
| runtime        = 80 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Doctor Rhythm is a 1938 American musical comedy film directed by Frank Tuttle and starring Bing Crosby, Mary Carlisle, Beatrice Lillie, and Andy Devine.    Based on the short story The Badge of Policeman ORoon by O. Henry,    the film is about a doctor who pretends to be a policeman assigned as the bodyguard of a wealthy matron, whose beautiful niece becomes the object of his affections. The film features the songs "On the Sentimental Side" and "My Heart is Taking Lessons".

==Plot==
Dr. Bill Remsen (Bing Crosby) helps cover for his ailing policeman friend (Andy Devine) and takes the policemans latest assignment as the bodyguard for a quirky but wealthy matron Mrs. Lorelei Dodge-Blodgett (Bea Lillie). Soon Bill falls in love with the ladys beautiful niece (Mary Carlisle). When the older woman becomes the target of thieves, Bill is able to thwart their efforts.

==Cast==
* Bing Crosby as Dr. Bill Remsen
* Mary Carlisle as Judy Marlowe
* Beatrice Lillie as Mrs. Lorelei Dodge-Blodgett
* Andy Devine as Officer Lawrence ORoon
* Rufe Davis as Al, the zookeeper
* Laura Hope Crews as Mrs. Minerva Twombling
* Fred Keating as Chris LeRoy John Hamilton as Insp. Bryce
* Sterling Holloway as Luke, the ice cream man
* Henry Wadsworth as Otis Eaton, the drunk
* Franklin Pangborn as Mr. Stenchfield, the store clerk
* Harold Minjir as Mr. Coldwater
* William Austin as Mr. Martingale, the floorwalker
* Gino Corrado as Cazzatta
* Harry Stubbs as Police captain
* Frank Elliott as Loreleis butler
* Charles R. Moore as Tooter, the Chauffeur
* Louis Armstrong as Trumpet player    

==Production==
===Soundtrack===
* "On the Sentimental Side" (James V. Monaco, Johnny Bruke) by Bing Crosby
* "My Heart is Taking Lessons" (James V. Monaco, Johnny Bruke) by Bing Crosby   

==References==
 

==External links==
*  
*  

 
 
 
 
 
 