Clash of the Empires
{{Infobox film name           = Clash of the Empires image          =  caption        =  director       = Joseph Lawson producer       =   screenplay     = Eric Forsberg story          =  starring       =   music          = Chris Ridenhour cinematography = Richard Vialet editing        = Rob Pallatina studio         = The Asylum distributor    = The Asylum released       = 18th February 2013 runtime        =  country        = United States language       = English budget         =  gross          =
}}
 American Fantasy film|fantasy/adventure film produced by The Asylum and directed by Joseph Lawson. The film stars Christopher Judge, Bai Ling and Sun Korng.

The film was originally titled Age of the Hobbits and set for release  . This led to a lawsuit against The Asylum for copyright infringement.    The lawsuit resulted in a temporary restraining order preventing The Asylum from releasing the film on its scheduled release date.    It has been alternatively titled "Lord of the Elves" in the United Kingdom.

==Cast==
*Christopher Judge
*Bai Ling
*Sun Korng as Goben
*Kyle Morris as Goben (voice)
*Jon Kondelik as Gelling (voice)
*Joseph J. Lawson as Koto (voice)
*Kelly P. Lawson as the Java Witch Queen (voice)

==Plot== Java Man ("Java Men"). The hobbits ally with early humans against the Javas. According to The Asylum, "In an ancient age, the small, peace-loving hobbits are enslaved by the Java Men, a race of flesh-eating dragon-riders. The young hobbit Goben must join forces with their neighbor giants, the humans, to free his people and vanquish their enemies." 

==Legal issues== The Hobbit MGM and The Hobbit producer Saul Zaentz commenced legal action against The Asylum for Age of the Hobbits, claiming that they were "free-riding" on the worldwide promotional campaign for Peter Jacksons forthcoming films. The Asylum claimed its movie is legally sound because its hobbits are not based on the J. R. R. Tolkien creations.    The Asylum argued that "Age of the Hobbits is about the real-life human subspecies, Homo floresiensis, discovered in 2003 in Indonesia, which have been uniformly referred to as hobbits in the scientific community." 

A lawsuit by Warner Bros. resulted in a temporary restraining order preventing The Asylum from releasing the film on its scheduled release date of December 11. The federal judge presiding over the case found that the film violated the "hobbit" trademark and was likely to cause confusion among consumers. As a result, Age of the Hobbits became the first Asylum film to be blocked from release. A hearing was also scheduled for January 28, 2013 to decide whether the restraining order should become a preliminary injunction. 

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 