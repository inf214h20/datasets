Tombés du ciel
{{Infobox Film 
| name           = Tombés du ciel (Lost in Transit)
| image          = 
| caption        = 
| director       = Philippe Lioret 
| producer       = Steven Spielberg Laurie MacDonald Walter F. Parkes 
| writer         = Screenplay Philippe Lioret Michael Ganz
| starring       = Jean Rochefort Marisa Paredes Ticky Holgado Laura del Sol
| music          = Jeff Cohen 
| cinematography = 
| editing        =  DreamWorks Distribution LLC 
| released       = 23 June 1994
| runtime        = 91 min 
| country        = France, Spain  French
| budget         =  
| gross          = 
}}

Tombés du ciel (Lost in Transit, literally "Fallen from the Sky") is a 1994 French film directed by Philippe Lioret. The film is about a man who loses his passport and spends a couple of days at a Paris airport, where he meets four people in similar circumstances. Tombés du ciel won the Grand Prize at the 6th Yubari International Fantastic Film Festival held in February 1995. 

==Cast and roles==
* Jean Rochefort - Arturo Conti
* Marisa Paredes - Suzana, Arturos wife
* Ticky Holgado - Serge
* Laura del Sol - Angela
* Sotigui Kouyaté - Knak
* Ismaïla Meite - Zola
* Jean-Louis Richard - Monsieur Armanet
* José Artur - Newspaper shop keeper
* Olivier Saladin - Restaurant manager
* François Morel (actor)|François Morel - Policeman
* Claude Derepp - Bébert, policeman
* Jacques Mathou - Policeman
* Christian Sinniger - Policeman
* Yves Osmu - Policeman
* Dimitri Radochevitch - Bus driver
* Pierre LaPlace - Inspector

==See also==
* Mehran Karimi Nasseri
* The Terminal, a 2004 film directed by Steven Spielberg, based on the same story.
* List of people who have lived at airports

==Notes==
 

==External links==
* 
*    

 
 
 
 
 
 


 