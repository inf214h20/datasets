Old Fashioned (film)
{{Infobox film
| name           = Old Fashioned
| image          = Old Fashioned film.jpg
| caption        = Theatrical release poster
| director       = Rik Swartzwelder
| producer       = Nathan Nazario Dave DeBorde Nini Hadjis Rik Swartzwelder
| writer         = Rik Swartzwelder
| starring       = Elizabeth Ann Roberts Rik Swartzwelder LeJon Woods Tyler Hollinger Nini Hadjis
| music          = Kazimir Boyle 
| cinematography = David George 
| editing        = Jonathan Olive Phillip Sherwood Robin Katz
| studio         =  
| distributor    = Freestyle Releasing
| released       =  
| runtime        = 115 minutes
| country        = United States
| language       = English
| budget         =
| gross          = $1.9 million 
}}
 Christian romance film, directed and written by Rik Swartzwelder who also stars in the film alongside Elizabeth Ann Roberts. The film was released in theaters on February 6, 2015. 

==Plot==
Clay Walsh, a former frat boy, runs an antique shop in a small Midwestern college town. There, he has become notorious for his lofty and outdated theories on love and romance as well as for his devout belief in God. When Amber Hewson, a free-spirited young woman rents the apartment above his shop, she finds herself surprisingly drawn to his strong faith and noble ideas, which are new and intriguing to her. And Clay, though he tries to fight and deny it, simply cannot resist being attracted to her spontaneous and passionate embrace of life. Ultimately, Clay must step out from behind his relational theories and Amber must overcome her own fears and deep wounds as the two of them, together, attempt the impossible: an "old-fashioned" and God-honoring courtship in contemporary America.

==Cast==
 
* Elizabeth Ann Roberts as Amber 
* Rik Swartzwelder as Clay
* LeJon Woods as David
* Tyler Hollinger as Brad
* Nini Hadjis as Lisa
* Maryanne Nagel as Carol
* Lindsay Heath as Trish
* Joseph Bonamico as George
* Dorothy Silver as Aunt Zella
* Angele Perez as Cosie
* Anne Marie Nestor as Kelly
 

==Reception==
===Box office===
The film opened early on just three screens on February 6 with a per screen average of $12,988, taking in a total of $38,965. It expanded for the four-day Presidents Day holiday and ended the weekend with $1,083,308  for a two-week total of $1,126,199 .   As of March 3, 2015, the movie has grossed $1,833,410.

On February 20, the film expanded to 298 screens. 

===Critical reception===
Old Fashioned received negative reviews from critics. On Rotten Tomatoes, the film holds a rating of 21%, based on 19 reviews, with an average rating of 4.4/10.  On Metacritic, the film has a score of 29 out of 100, based on 7 critics, indicating "generally unfavorable reviews". 

The film also found some support from mainstream critics as well. Most notably, Glenn Kenny writing for   wrote: "It’s incredibly rare to see an American movie with a Christian perspective that’s more invested in philosophizing and empathizing than in eschatological pandering, and for that alone Old Fashioned deserves commendation. The movie’s also very well acted and beautifully shot... Old Fashioned is both unusual and intelligent enough that, despite it not being entirely MY cup of tea, I’m hoping that it’ll succeed at doing at least a little more than addressing the converted." 

==Awards==
The film premiered in 2014 at the 3rd Annual Mt. Hood Independent Film Festival where it won the JBM Award. 

==Reference list==
 
http://www.thecinemasnob.com/midnight-screenings/midnight-screenings-old-fashioned

==External links==
* 
*  at Internet Movie Database
*  at Rotten Tomatoes
*  at Fandango

 
 
 
 
 
 
 
 
 
 