Kaalamellam Kadhal Vaazhga
{{Infobox film
| name           = Kaalamellam Kadhal Vaazhga
| image          = 
| image_size     =
| caption        = 
| director       = R. Balu
| producer       = Sivasakthi Pandian
| writer         = R. Balu
| screenplay     =  Murali Kausalya Kausalya Gemini Ganesan Manivannan Deva
| cinematography = Thangar Bachan
| editing        = B. Lenin V. T. Vijayan
| distributor    = Sivasakthi Movie Makers
| studio         = Sivasakthi Movie Makers
| released       = 12 February 1997
| runtime        =
| country        = India
| language       = Tamil
| budget         =
| preceded_by    =
| followed_by    =
| website        =
}}
 1997 Tamil Murali alongside Kausalya (actress)|Kausalya, in her debut role, while Gemini Ganesan and Manivannan play supporting roles.

The film was remade in Kannada as Kushalave Kshemave with Ramesh Aravind and Srilakshmi.

==Cast== Murali  Kausalya
*Gemini Ganesan
*Manivannan Karan
*Vivek Vivek
*Venniradai Moorthy
*R. Sundarrajan
*Nassar
*Charle

==Production== Kausalya made her debut with the film.

==Release==
A critic from Indolink.com stated that "overall, if it is a long time since you saw a movie with good comedy, above average music and crystal clear photography, here is one for you." 

The success of Kadhal Kottai (1996) and Kalamellam Kadhal Vaazhga, prompted Sivashakthi Pandian to announce another love story Kaadhale Nimmadhi soon after this films release. 

==Soundtrack==
{{Infobox album|  
  Name        = Kaalamellam Kadhal Vaazhga
|  Type        = Soundtrack Deva
|  Cover       =
|  Released    = 1997
|  Recorded    = Feature film soundtrack
|  Length      =
|  Label       = Lucky Audio
|  Producer    =
|  Reviews     =
|  Last album  = 
|  This album  = Kaalamellam Kadhal Vaazhga (1997)
|  Next album  = 
}}
The soundtrack of the film was composed by Deva (music director)|Deva, was well received by the audience. The lyrics written by Deva (music director)|Deva, Pazhani Bharathi and Ponniyin Selvan.
{{tracklist
| headline        = Track-list
| extra_column    = Singer(s)
| total_length    = 
| title1          = Annanagar Andalu
| extra1          = Sabesh
| length1         =
| title2          = Baghavanae  Mano
| length2         = 
| title3          = Oru Mani Adithal Hariharan
| length3         = 
| title4          = Vennilavae Vennilavae
| extra4          = S. P. Balasubrahmanyam, Swarnalatha
| length4         = 
| title5          = Babilona
| extra5          = Krishnaraj
| length5         =
}}

==References==
 

 
 
 
 
 
 


 