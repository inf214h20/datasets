Head over Heels (2001 film)
{{Infobox film
| name           = Head Over Heels
| image          = Head over heels ver1.jpg
| caption        = Theatrical release poster
| image_size     = Mark Waters
| producer       = Robert Simonds
| writer         = John J. Strauss Ed Decter David Kidd Ron Burch
| narrator       =
| starring       = Monica Potter Freddie Prinze Jr. Shalom Harlow
| music          = Randy Edelman Steve Porcaro
| cinematography = Mark Plummer
| editing        = Cara Silverman Universal Pictures
| released       =  
| runtime        = 86 minutes
| country        = United States English Russian Russian
| budget         = $14 million   
| gross          = $13,127,022 
}}
 Mark Waters about a woman, Amanda Pierce (Monica Potter) living in New York City who works at Metropolitan Museum of Art restoring paintings.

Early in the film, she moves in with four supermodels and falls for a man living in an apartment that they can see across the street.  After the models try to help Amanda get the man, they find out he might not be what he appears to be.

==Plot== Ivana Miličević), Sarah OHare), and Holly (Tomiko Fraser). When Amanda discovers that Jim Winston (Freddie Prinze, Jr.), the guy she likes, lives in the apartment across from hers she starts spying on him to try to find his flaw. One night Amanda sees Jim kill a woman, Megan OBrien (Tanja Reichert), and by the time the police arrive they dont believe Amanda because she is the only witness, and the evidence is gone. Annoyed about the polices lack of effort to find out what has really happened, Amanda and her new friends investigate on their own. When Amanda finds out about what she thinks is Jims involvement with Megans death, she confronts him. Amandas judgment turns out to be wrong and Jims (who turns out to be undercover cop, Bob Smoot, who was trying to gain Hallorans (Jay Brazeau) trust by staging his partner Megans death) cover is blown. Amanda discovers that Jim is investigating Halloran, a Russian man who has been smuggling in money and who Amanda has been privately restoring a painting for. Later, Jim, Amanda and her roommates get captured but later escape when Roxana seduces their Russian guard and with the help of the models realize what Halloran was really doing, smuggling diamonds. Amanda, Jim (now Bob Smoot), and the models go to a fashion runway and take down Strukov. They are all awarded special commendations for meritorious service from the FBI. After the cops take care of things Jim asks Amanda if they can start over, but she refuses and Jim leaves.  At the end Amanda and Jim (going by his real name Bob) "meet" again and the movie ends when Bob takes Amanda up to his new apartment and shows her the view, which turns out to be of Amanda and the models apartment. Lisa and the models are jumping around happily while Bob and Amanda laugh. They kiss and close the curtains to his apartment window.

==Cast==
* Monica Potter as Amanda Pierce
* Freddie Prinze, Jr. as Jim Winston/FBI Special Agent Bob Smoot
* Shalom Harlow as Jade (roommate, Canadian) Ivana Miličević as Roxana Milla Slasnakova (roommate, Russian) Sarah OHare as Candi (roommate, Australian)
* Tomiko Fraser as Holly (roommate, African-American) The Met)
* Tanja Reichert as Megan OBrien
* Tanner as Hamlet the Great Dane
* Jay Brazeau as Mr. Halloran / Vadiim Strukov
* Stanley DeSantis as Alfredo (Fashion designer)
* Betty Linde as Polly
* Norma MacMillan as Gladys
* Bethoe Shirkoff as Noreen
* Tom Shorthouse as Mr. Rankin (Amandas boss)
* Joe Pascual as Officer Rodriguez
* J.B. Bivens as Mitch (apartment building super)

==Reception==
The film opened on April 20, 2001 to largely negative reviews, receiving a 10% "Certified Rotten" rating at review aggregator   wrote: "With her Julia Roberts-like vulnerability and kewpie-doll eyes,Potter certainly outshines Prinze, who doesnt deviate much from his past teen dream roles—though hes winsome just the same. 

By most standards, the film was financially unsuccessful. Released on February 2, 2001, the film opened at #7 in 2,338 theaters and grossed $4,804,595 in the opening weekend at the North American box office. The final domestic grossing was $10.4 million while the foreign market grossed $2.7 million for a worldwide total of $13,127,022. Against its $14 million budget, the film was a flop. 

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 