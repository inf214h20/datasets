Running Wild (1927 film)
{{infobox film
| name           = Running Wild
| image          =
| imagesize      =
| caption        =
| director       = Gregory La Cava
| producer       = Adolph Zukor Jesse L. Lasky Gregory La Cava William Le Baron Carl Laemmle, Jr.
| writer         = Gregory La Cava (original screen story) Roy Briant (scenario &  intertitles )
| starring       = W. C. Fields
| cinematography = Paul Vogel
| editing        = Ralph Block
| distributor    = Paramount Pictures
| released       = June 11, 1927
| runtime        = 65 minutes
| country        = United States
| language       = Silent film(English intertitles)
}} silent comedy film featuring W. C. Fields in a film built around his unique talents.

==Preservation status==
The film was directed by Gregory La Cava and is preserved at the Library of Congress. It also received, along with several other Paramount titles, wide distribution on VHS tape in 1987 in celebration of Paramounts 75th anniversary 1912-1987.  

==Cast==
*W. C. Fields - Elmer Finch
*Mary Brian - Elizabeth
*Marie Shotwell - Mrs. Finch
*Claude Buchanan - Dave Harvey Frederick Burton - Mr. Harvey
*Barnett Raskin - Junior Frank Evans - Amos Barker
*Edward Roseman - Arvo, the Hypnotist

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 


 