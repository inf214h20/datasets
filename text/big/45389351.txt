An Affair of Three Nations
 
{{Infobox film
| name           = An Affair of Three Nations
| image          = 
| image size     =
| caption        = 
| director       = Arnold Daly Ashley Miller
| producer       = Pathé Frères Films
| writer         = 
| narrator       =
| starring       = Arnold Daly Sheldon Lewis William Harrigan
| music          =
| cinematography = 
| editing        =
| distributor    = Pathé Exchange|Pathé Exchange, Inc.
| released       =  
| runtime        = 
| country        = United States Silent
| budget         =
}}
An Affair of Three Nations is a 1915 American silent film based on a story by John T. McIntyre.  The film was the first in the “Ashton-Kirk, Investigator” series, and was followed by The Menace of the Mute.   The film was directed by Arnold Daly and Ashley Miller, and produced by Pathé Frères Films. As with a lot of silent films, the survival status of this film is unknown. 

==Synopsis==
Arnold Daly plays the part of Ashton Kirk, a wealthy and scholarly young man who solves mysteries that have the police puzzled. Kirk is asked by Stella Morse, (Louise Rutter), to find out who is threatening her uncle, Dr. Morse (Sheldon Lewis). Morse has a Japanese Espionage|spy system has been trying to get hold of the treaty, which could ruin relations between its country and America. Kirk manages to get the treaty himself and prevents a war from breaking out. 

==Cast==
*Arnold Daly - Ashton Kirk
*Sheldon Lewis - Doctor Morse
*William Harrigan - Phillip Warwick
*Charles Laite - Pendleton
*Charles Krauss - Count Drevenoff
*Geoffrey Stein - Okin
*Martin Sabine - Karowski
*George Melville - Humadi
*Louise Rutter - Stella Morse
*Doris Mitchell - Nanon

==See also==
*Lost film

==References==
 

==External links==
* 
* 
 
 
 
 
 
 
 
 