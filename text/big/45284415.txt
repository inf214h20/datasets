Bulldog Courage (1922 film)
 
{{Infobox film
| name           = Bulldog Courage
| image          = 
| alt            = 
| caption        = 
| film name      =  
| director       = Edward A. Kull
| producer       =  
| writer         = {{Plain list|*George Larkin
*Ollie Kirkby}}
| screenplay     = 
| story          = Jeanne Poe
| based on       =  
| starring       = {{Plain list|*George Larkin
*Bessie Love}}
| narrator       =  
| music          = 
| cinematography = Harry Neumann  Fred Allen 
| production companies = Russell Productions 
| distributor    = State Rights   
| released       =    reels   
| country        =  Silent (English intertitles)
| budget         = 
| gross          =  
}} silent Western Western film directed by Edward A. Kull,  and starring George Larkin and Bessie Love. It was written by Larkin and his wife Ollie Kirkby,  with a screenplay by Jeanne Poe. 

The film is extant, in the collection of the British Film Institute. 

==Plot==
College athlete Jimmy Brent (Larkin) is sent to Wyoming to beat up Big Bob Phillips, his uncles rival for the hand of Mary Allen. When Jimmy arrives in Wyoming, he falls in love with Gloria Phillips (Love), and decides not to beat up Phillips. When Phillips mistakenly thinks that Jimmy is the cause of cattle rustling, Jimmy fights Phillips, catches the actual cattle rustlers, and gets the girl. 

==Cast==
*George Larkin as Jimmy Brent 
*Bessie Love as Gloria Phillips
*Albert MacQuarrie as John Morton
*Karl Silvera as Smokey Evans
*Frank Whitman as Big Bob Phillips
*Bill Patton as Sheriff Webber
*Barbara Tennant as Mary Allen

==Reception==
Although few contemporaneous reviews of the film exist today, Bessie Love considers this film as one of the first indicators of decline in her silent film career. 

==References==
 

==External links==
* 
* 

 
 
 
 
 
 

 
 