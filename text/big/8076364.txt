Camille 2000
{{Infobox film
| name = Camille 2000
| image = Camille 2000 - Movie poster.jpg
| caption = DVD cover
| director = Radley Metzger
| producer = Radley Metzger
| writer = Michael de Forrest
| starring = Daniele Gaubert Nino Castelnuovo Eleonora Rossi-Drago Philippe Forquet Roberto Bisacco
| music = Piero Piccioni
| cinematography = Ennio Guarnieri
| editing =
| distributor =
| released =  
| runtime = 115 minutes
| country = Italy
| awards =
| language =
| budget =
}} La Dame aux Camélias by Alexandre Dumas, fils. It was adapted by Michael DeForrest and directed by Radley Metzger. It stars Danièle Gaubert and Nino Castelnuovo with Eleonora Rossi Drago and Massimo Serato. The film featured a drug use theme not present in the source story. It may have been considered a pornographic film in 1969 but is more of a drama based on the expectations of a man finding the wrong love.

==Plot==
Marguerite, a beautiful woman of affairs, falls for the young and promising Armand, but sacrifices her love for him for the sake of his future and reputation.

==Cast==
Danièle Gaubert as Marguerite Gautier

Nino Castelnuovo as Armand Duval

Eleonora Rossi Drago as Prudence (credited as Eleonora Rossi-Drago)

Roberto Bisacco as Gastion

==Reception==
The film was critically panned and has 17% on  . Whoever painted that big sign in front of the theater has an accurate critical sense. The sign says: "See Daniele Gaubert presented in the nude ... and with great frequency." That captures the essence of Metzgers art."  On August 11, 2005, he added the film to his "Most Hated List." 

==References==
 

==See also==
*Camille (disambiguation)

== External links ==
*  
*  

 

 
 
 
 
 
 
 

 
 