Before Breakfast
 
 
{{Infobox film
| name           = Before Breakfast
| image          = Before Breakfast (1919) - 1.jpg
| image_size     =  Still with Lloyd and Daniels
| director       = Hal Roach
| producer       = Hal Roach
| writer         =
| starring       = Harold Lloyd
| music          =
| cinematography =
| editing        =
| studio         = Rolin Films
| distributor    = Pathé Exchange
| released       =  
| runtime        =
| country        = United States Silent (English intertitles)
| budget         =
}}
 short comedy film starring Harold Lloyd.   

==Cast==
* Harold Lloyd as The Boy
* Snub Pollard
* Bebe Daniels
* Sammy Brooks
* Lew Harvey
* Noah Young

==See also==
* List of American films of 1919
* Harold Lloyd filmography

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 

 