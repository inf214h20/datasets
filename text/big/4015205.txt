Rubberface
 

{{Infobox Film
| name           = Rubberface
| image          = Rubberfacecover.jpg
| image_size     = 
| caption        = VHS cover
| director       = Glen Salzman Rebecca Yates
| producer       = Glen Salzman Rebecca Yates
| writer         = Nada Harcourt Michael jean
| narrator       = 
| starring       = Jim Carrey Adah Glassbourg
| music          = Rainer Wiens
| cinematography = 
| editing        = 
| distributor    = Trimark Pictures 1983
| runtime        = 55 minutes
| country        = Canada
| language       = English
| budget         = 
| preceded_by    =
| followed_by    = 
}}
 1983 television CBC television starring Jim Carrey. Originally titled Introducing Janet, it was changed to Rubberface for the video release after Carreys success.

==Plot==
Rubberface revolves around a young, depressed woman named Janet Taylor (Adah Glassbourg). She meets a dish washer named Tony Maroni (Jim Carrey), an aspiring comedian, at a comedy club. Maroni, however, has little success in comedy, while Janet proves to be far more successful. As the film progresses, their varying ambitions and capabilities as comedians result in their cooperation, which concludes the plot.

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 