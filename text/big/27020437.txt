T. D. Dasan Std. VI B
{{Infobox film
| name           = T. D. Dasan Std. VI B
| image          = T D Dasan Std VI B.jpg
| image_size     = 
| caption        = 
| director       = Mohan Raghavan
| producer       = Paul Vadukumcherry Paul Valikodath
| writer         = Mohan Raghavan
| narrator       = 
| starring       = Master Alexander Biju Menon Jagadish Shweta Menon Sreevalsan J. Menon
| cinematography = Arun Varma
| editing        = Vinod Sukumaran
| studio         = Chithranjali
| distributor    = Presta De Lexa Moviedom Release
| released       =  
| runtime        = 102 Minutes
| country        = India
| language       = Malayalam
| budget         = 
| gross          = 
}}
T. D. Dasan Std. VI B is a 2010 Malayalam film written and directed by Mohan Raghavan and produced by Paul Vadukumcherry.The film deals with a childs desire to see his father, and the beginning is made when he gets a clue from a piece of paper he finds in his mothers trunk. The wanderings of the boys mind and the real unravel of world which is a mix of dreams, desires and the present are portrayed in the movie. The film features Master Alexander, Biju Menon, Jagadish and Shweta Menon in the lead roles.
Master Alexander won the Best Child Actor award in an online poll conducted by The Times of India. 
The film opened in Kerala theatres on 9 April 2010 to universal acclaim. It was screened at various international film festivals and has garnered several awards. Despite all the acclaim, the film performed poorly at the box office.

T. D. Dasan Std. VI B was the only film directed by Mohan Raghavan who died a year after the release of the film.

==Plot==
T. D. Dasan (Master Alexander) is a young boy who lives with his mother (Shwetha Menon). His father had left them a few years back. Dasan gets his dads address from his mothers old trunk box and writes him a letter.

Dasans father had moved out of that address and the letter reaches the current resident Nandakumar Poduval (Biju Menon), an Ad film maker who lives with his thirteen-year-old daughter Ammu (Tina Rose) at Bangalore. Nandan requests Ammus caretaker Madhavan (Jagadish) to find out the whereabouts of the person and deliver the letter to him. But Madhavan is not that enthusiastic and the letter ends up in the waste bin. Ammu sees this and feels bad about it. She starts writing replies to Dasan, as if they were written to him by his dad. The young boy is excited at the thought of having found his dad, and shares all his feelings and needs with his dad. Ammu prompty replies with pens and other gifts Dasan asked his father.

Later Dasans mother gets to know this and gets shattered by this news. Nandakumar also gets to know this news and wants to meet Dasan, apologize and tell him the truth. Dasans mother was later found dead under mysterious circumstances. When Nandakumar comes to meet Dasan, he understands that Dasan is now an orphan and that he has now accepted Nandakumar as his dad. Nandakumar first thinks of leaving the place without Dasan but changes his mind soon after.

==Cast==
* Master Alexander - T.D.Dasan
* Tina Rose - Ammu
* Biju Menon - Nanda Kumar
* Jagadish - Madhavan
* Swetha Menon - Chandrika
*Valsala Menon - Ammumma
* Shrruiti Menon - Megha
* Jagathy Sreekumar - Menon Suresh Krishna - Raman Kutty
* Mala Aravindan - Teacher
* Ottapalam Pappan
* Alosious Panikulangara - Baalu
* Sreehari - Advocate
* Dennis Parakkadan - Vagrant
* Gayathri

==Reception==
T. D. Dasan Std. VI B had an overwhelmingly positive reception from the critics. Paresh Palicha of Rediff.com said the movie "is miles ahead of the films that are served in the garb of entertainment week after week, and leaves us with a sense of elation. A brilliant effort indeed!"   This is what Nowrunning  had to say. "Mohan Raghavans debut film matches up in texture, dimension and depth to the wonderfully crafted films of Walter Salles that often transcend boundaries. This is the work of an artist who refuses to see the medium as a mere commodity, and the highly sensitive writing and truly marvelous direction make this film a captivating watch." Sify.com said "With no loud statements, buffoonery or gimmicks that we usually see in every other film, debutant director Mohan Raghavans T D Dasan Std VI B is a little gem that could perhaps shock you with its inherent honesty." 

Indiaglitz also had given a glowing review for the movie.  Despite the favourable critic opinion, the movie was a non-starter at the box-office. Budget constrains heavily limited the marketing of the movie, with the film ending up as a minuscule release. The collection was hardly noticeable.

The film was initially adjudged the Golden Pheasant Award for Best Film at the International Film Festival of Kerala 2010. However, it was decided to award a foreign film after some jury members felt that giving the award for a regional film will diminish the interest of foreign delegates in IFFK. The issue was revealed by actor P. Sreekumar in a press conference, which garnered wide criticism from the Malayalam film fraternity against the IFFK. 

The film was one of the two South Indian films screened at the 11th New York Indian Film Festival (NYIFF 2011).  The film won the award for the Best Screenplay at the festival.   It was an official selection in the competition section for the Asian New Talent Award at the Shanghai International Film Festival. 

===Film festival participation===
* Chennai International Film Festival
* FILCA Film Festival
* Habitat International Film Festival
* International Film Festival of Kerala
* New York Indian Film Festival
* Pune International Film Festival
* Shanghai International Film Festival

===Awards===
; Kerala State Film Awards Best Debut Director - Mohan Raghavan  Second Best Actor - Biju Menon

; New York Indian Film Festival
* Best Screenplay - Mohan Raghavan
 John Abraham Award
* Best Film

; Kerala Film Critics Association Awards
* Best Story - Mohan Raghavan
* Best Child Artist-Master Alexander

; Asianet Film Awards Best Child Artist - Master Alexander

; World Malayali Council Awards
* Special Jury Award - Mohan Raghavan

; INSPIRE Awards 
* Best Debutant Director - Mohan Raghavan
* Best Supporting Actress - Shweta Menon

; Amrita TV|Amrita-FEFKA Film Awards  
* Best Film
* Best Director - Mohan Raghavan
* Best Screen Play- (Mohan Raghavan)
* Best Child Artist-Master Alexander

; Jaihind TV Film Awards
* Best Screenplay ( ) - Mohan Raghavan
Mathrubhumi Film Awards
* Best Child Artist - Master Alexander
Times Of India Awards
* Best Child Artist - Master Alexander

==References==
 

==External links==
*  
* Prema Manmathan:  , The Hindu.

 
 
 
 
 
 