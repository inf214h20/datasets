Arasu Vidumurai
 
{{Infobox film
| name           =  Arasu Vidumurai
| image          = 
| caption        =  Film poster 
| director       =  C.Balasubramaniam
| producer     =  S.V.Films  & CBS Cine International
| writer          =  Lakshmi
| starring       =  Rajesh
| music          = Thomas Rathnam 
| cinematography =  Anbarasan
| editing        =  V.T.Vijayan
| studio         = Prasad Studios
| released       =  
| country        = India language        = Tamil
}} Tamil Romance film written and directed by  C.Balasubramaniyam.The film will star Rajesh,Elumalai,Delhi Ganesh, Dr.vincent Therraisnathan.The story of the film revolves around  a small town. The film is produced by S.V.Films & CBS Cineinternational   and features background score and soundtrack composed by Thomas Rathnam  with cinematography handled by  Anbarasu. 

==Cast==
* Rajesh,  
* Swathi (Raattinam Movie Fame)
* Delhi Ganesh
* Elumalai 
* Dr.Vincent Therraisnathan

==Production==
S.V. Films & CBS Cine International,  

===Filming===
Filmed in and around Chennai, Tiruvannamalai. Gingee, Polur, Dehradun  and foreign.

==Released==

 

==Controversies==
Few controversies happened to be about this film. Heroine was changed after a few days shooting. 

==Soundtrack==
{{Infobox album 
| Name = Arasu Vdumurai,   
| Longtype =
| Type = Soundtrack
| Artist = Thomas Rathnam 
| Cover = 
| Border = yes
| Alt =
| Caption = Cover Art
| Released = 
| Recorded = 2012 Feature film soundtrack
| Length =  Tamil
| Label = 
| Producer = Thomas Rathnam 
| Last album = Uyirodu (2012)
| This album = Arasu Vidumurai (2012) 
| Next album = Thiru Guru (2012)
}}

Thomas  Rathnam  composed the soundtrack. Director Balasubramaniyam had chosen the best tunes and lyrics. The soundtrack album consist of 5 tracks. The lyrics were written by  
Kavi Perarasu Vaira muthu, Ka.Chezhian. 
{{Track listing
| headline = Tracklist
| extra_column = Singer(s)
| total_length = 34.56
| all_music =
| lyrics_credits = yes
| title1 = Vanga puyalondru
| extra1 = 
| lyrics1 = Kavi Perarasu Vairamuthu
| length1 = 04:18
| title2 = Enannga Ennanga
| extra2 = Dr.Vincent Therraisnathan, J.Kevin jason
| length2 = 03:56
| lyrics2 = Ka.Chezhian
| title3 = Yar
| extra3 = Karthik,Padmalatha, Sarafin
| lyrics3 = Ka.Chezhian
| length3 = 05:04
| title4 = Kimu keepi
| extra4 = 
| length4 = 03:43
| lyrics4 = Kavi Perarasu Vairamuthu
| title5 = 
| extra5 = 
| length5 = 
| lyrics5 = 
}}

===Critical reception===

The soundtrack album of Arasu vidumurai consisting of  5 songs and 1 Theme music was released on 22  June 2012 in Chennai The album received positive critic and public reviews. Sify movies, msn entertainment and ibn live gave the album a positive verdict stating that "Overall, it is a classy and refreshing album. Behindwoods gave the album a positive verdict with 3.5 out of 5 stars and mentioned "Overall it’s a good album with tracks.Thomas Rathnam  had done a very good job.

==References==
 

 
 
 
 