Ricochet (film)
{{Infobox film
| name           = Ricochet
| image          = Ricochet ver2.jpg
| caption        = Promotional poster 
| writer         = Fred Dekker   Menno Meyjes   Steven E. de Souza
| starring       = Denzel Washington  John Lithgow  Ice-T  Kevin Pollak
| director       = Russell Mulcahy
| music          = Alan Silvestri
| cinematography = Peter Levy
| editing        = Peter Honess
| studio         = HBO Films Silver Pictures Cinema Plus L.P. Indigo Productions
| distributor    = Warner Bros.
| released       =  
| runtime        = 109 minutes
| country        = United States English
| Michael Levy   Joel Silver
| gross          = $21,760,000
}}
 Los Angeles attorney (Washington) and a vengeful criminal (Lithgow) he arrested and caused to be convicted when he was previously a cop.

==Plot==
The film begins in 1983, depicting Nicholas Styles (Washington), a mild-mannered rookie officer of the Los Angeles Police Department and law student. The young Styles is witnessing the beginning of his career and adult life, as he meets his future wife (Victoria Dillard) and is drifting away from his childhood friend Odessa (Ice-T), who is drifting into a life of crime in South Central Los Angeles, where the two of them grew up.
 Josh Evans), shortly after Blake has murdered several drug dealers and stole the drug money. Styles catches Blake at gunpoint in the carnival, and is forced into a standoff when Blake takes a hostage at gunpoint, using her as a human shield. Styles manages to get Blake to release the hostage by stripping his equipment and uniform off, demonstrating that he has no other weapons or body armor, before placing his revolver on the ground. He has stripped, however, to gain access to a backup gun hidden in his athletic supporter, which he uses to shoot Blake in the knee, allowing him to take the killer down. The incident is caught by an amateur videographer, and is shown on television, making Styles a local hero and drawing the welcome attention of the Los Angeles County District Attorney (Wagner) and a local councilman (John Cothran, Jr.). He and Doyle are immediately promoted to Detective, while Blake is sent to prison.

The film cuts ahead eight years to 1991. Styles has since had a famous and eventful career in the Police Department, has gone on to further fame and success as an Assistant District Attorney, has married and has had two daughters. He is moving gradually into politics, beginning with raising funds for a childrens community center at the Watts Towers with the patronage of the local councilman. At the same time, Blake has nurtured a psychotic fixation and lust for revenge against Styles while in prison, and has degenerated into further violence fighting against the Aryan Brotherhood. After killing an AB member with whom he had a grudge, Blake strikes a deal with the leader of the gang to plot an escape. Shortly before their escape, Kim, who has been incarcerated with Blake, is paroled, and plans to assist in Blakes escape and revenge plot on the outside. Blake and the AB members stage a violent and deadly prison escape during a parole hearing, which only Blake and the AB leader survive. Shortly after, Blake murders the gang leader, shoots him in the knee, and burns his corpse. Kim had previously switched Blakes and the gang leaders dental records, and Blake expects the coroner to believe that Blake was the one killed (due to the dental records and knee injury), thus faking his own death.

Meanwhile, Styles is planning a telethon to raise money for his community center, planning to broadcast it from a church at which his father (John Amos) is a minister. He also finds his old friend Odessa, who has become a major drug-dealer in the neighborhood, to convince him forcefully to avoid the new center. Blake has returned to Los Angeles, and is keeping Styles under surveillance. On the night of the telethon, Blake cuts the power to Styles house, and then shows up impersonating a utility worker to the babysitter watching Styles daughters. He drugs the babysitter, and takes the opportunity to bug the house. Blake has sent $10,000 in cash and an anonymous letter to the telethon, posing as an anonymous benefactor. Later that night, after the telethon, Blake and Kim ambush the city councilman, who is taking the proceeds of the telethon to deposit them at the bank. They murder the councilman, staging his death to appear as a suicide, dressing him in Drag (clothing)|drag, planting child pornography, leaving a suicide note that implicates him and Styles in child molestation, and stealing the $10,000 in cash.

The next morning, when the councilman is discovered, the implications of molestation and the missing money lead to a scandal implicating Styles, creating negative and intrusive media attention and suspicion from the District Attorney for whom he works. Later that evening, Styles is abducted by Blake, and realizes that Blake is alive, has murdered the councilman, and intends revenge. Blake explains that Styles owes his successful life to Blake, while Blake has sat in prison. Blake and Kim keep Styles for several days, regularly injecting him with heroin and cocaine.
 rape him. Blake, who had hired her, records the incident on video.

Blake and Kim finally deposit Styles unconscious body on the steps of City Hall, making him appear as a derelict. When he is found, his colleagues and the media treat his story with skepticism, which is only furthered when he unsuccessfully tries to lead them to the location where he was held and when the drugs and a gonorrhea infection (from the prostitute) are discovered during his post-release medical examination. His wife, who overheard about Styles examination, thinks he betrayed her with the prostitute. This later revelation alienates Styles from his wife, and he spends an evening on the sofa, drunkenly talking back to the negative media coverage of himself on TV while being recorded by Blake until he passes out.

The next morning, Styles sees that a note has been left on his VCR to play it. When he does, he sees a video of Blake going up to his daughters room and holding a hatchet over them just before the tape cuts out. Terrified and enraged, Styles finds the girls room and the rest of the house empty with only a note that his wife has taken them to the park. Styles digs his old service revolver out of a nightstand and runs down the streets in his bathrobe to the park. As he is leaving, Kim returns to the house with another videotape.

Tired, hung-over, and disoriented, Styles sees a black-clad figure approaching the stage where his girls are putting on a play. He tackles the figure and holds him at gunpoint; the figure turns over to reveal that he is nothing more than a clown who is part of the performance. The incident occurs in front of and is videotaped by several prominent local people, whose children are in the play. This latest incident and his reaction to it cause the District Attorney to question Styles sanity.
 paranoid to the District Attorney, who suspends him from his position.

Afterwards, Styles old partner Doyle, who is the only person still willing to believe him, approaches him with evidence of Blakes obsession. This evidence was found in his prison personal effects, and Doyle reveals that he has a lead that an Aryan Brotherhood-affiliated bookstore is planning to get tickets and false passports for someone. Styles and Doyle go to the bookstore that night, and Styles beats information out of the owner. Kim is witnessed running away from the store, and Doyle chases him down an alley. Blake ambushes him and shoots him repeatedly in the alley, before tossing the gun to Styles who picks it up, leaving his fingerprints on it. Blake then escapes, and Doyle dies in Styles arms, finally realizing that his friend had been right all along. Bizarrely, Styles leaves the scene without taking the gun that implicates him.

Now pursued as a murder suspect, Styles has few options. Desperately, he contacts his old, and perhaps last, friend Odessa, for help. Styles evacuates his family from their home and takes them to the housing project Odessa uses as a drug lab. Putting his family in Odessas hands, Styles and Odessas gang initiate a plan to bring Blake into the open. After the project building is cleared, Styles goes to the roof and begins raving to the street below, appearing to be deranged and suicidal. The media arrives and broadcasts him live. Believing that Styles will kill himself and deny Blake the satisfaction of seeing him incarcerated, Blake arrives at the project. Odessas gangsters spot Blake there, and the plan continues. Styles fakes his own death, by starting a fiery explosion in the building and escaping. Odessas gang abducts Kim, and Odessa sends a message to Blake that Styles is alive and intends to get him, challenging him to come to the Watts Towers.
 electrical mains to the metal tower, electrifying Blake while Styles swings clear of the tower on a harness. Styles knocks the stunned Blake off the tower, and he lands on a spike protruding from the tower, impaling and killing him.

Styles comes down from the towers, rejoining his wife and children. He calls out to Odessa one last time, inviting him to basketball that Saturday. Meanwhile, the television news crews, who had played a major role in Styles discommendation, are there, broadcasting the latest events that have dramatically proven Styles innocent. Styles, at last, joins a newscaster (Mary Ellen Trainor) on camera as she is broadcasting. When she asks him for a comment, he turns off the news camera and dishes the news media as the screen fades out.

==Cast==
* Denzel Washington as Detective/Lieutenant/Assistant District Attorney Nicholas "Nick" Styles
* John Lithgow as Earl Talbot Blake
* Ice-T as Odessa
* Kevin Pollak as Detective Larry Doyle
* Lindsay Wagner as D.A. Priscilla Brimleigh Josh Evans as Kim
* Mary Ellen Trainor as Gail Wallens, the character she portrayed in Die Hard
* Victoria Dillard as Alice
* John Amos as Reverend Styles
* John Cothran, Jr. as Councilman Farris
* Thomas Rosales, Jr. as Drug dealer
* Jesse Ventura as Chewalski (Blakes cellmate)
* Linda Dona as Wanda

==Reception==
The movie had a mixed reception from critics.  

===Box office=== Cape Fear, a film starring Robert De Niro and Nick Nolte with a very similar storyline. 

==International remake==
The second half of Farz (2001 film)|Farz, A Hindi language film, is a scene for scene copy of Ricochet.

==References==
 

== External links ==
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 