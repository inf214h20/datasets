Hunter × Hunter: Phantom Rouge
{{Infobox film
| name           = Hunter × Hunter: Phantom Rouge
| image          =  
| alt            = 
| caption        = Japanese release poster
| director       = Yuzo Sato
| producer       = Minami Ichikawa
| writer         = Shōji Yonemura
| based on       =  
| music          = Yoshihisa Hirano
| cinematography =  Madhouse
| distributor    = Toho
| released       =  
| runtime        = 97 minutes
| country        = Japan
| language       = Japanese
| budget         = 
| gross          =  United States dollar|US$12,595,288   
}} Japanese animated feature film based on the Hunter × Hunter franchise created by Yoshihiro Togashi. The film features the four main characters, Gon, Killua, Kurapika and Leorio reunited to face a dangerous individual who once was a member of their greatest enemies, the criminal organization "Phantom Troupe". The film opened at number one in Japan on January 12, 2013.

==Plot== Killua dreaming Gon on Leorio and Kurapika were Phantom Troupe.
 Hisoka while Nobunaga and Machi who Pakunoda to attack his former companions but they are all destroyed by them. After Omokage retreats, Nobunaga and Machi leave as well, warning Gon and Killua to not stand between Omokage and the Phantom Troupe as they have a score to settle with him.

In the next day, Ritz leads Gon and Killua to the mansion from Kurapikas vision and after leaving her behind for her safety, they meet another puppet, now based on Illumi who attacks them. Just as the Illumi doll is about to steal Killuas eyes, Gon intervenes and has his own eyes stolen instead. Ashamed for failing to protect Gon and believing that he had betrayed his friend, Killua flees and contemplates suicide but is saved by Gon who had tracked him with his acute sense of smell. Gon then reveals that he had let the Illumi doll steal his eyes by his own volition so they could track down Omokage. He also states that he had long realized that Ritz was one of his puppets as well. Reunited with Leorio and Kurapika, they set for Omokages location to confront him.

Back at his hideout, Omokage reclaims his eyes from Ritz, who is revealed to be a puppet made from his deceased sister and when Gon and his friends appear, he reveals that he let them track him down by purpose as he intends to claim Leorio and Killuas eyes to use on his puppets as well. After confessing to Kurapika that he also took part in the Kurta Clans massacre and claiming that he currently has no other scarlet eyes in his possession, Omokage sends the Pyro and Illumi dolls to attack the Hunters, but Gon and Kurapika defeat them with Leorio and Killuas help and retrieve their eyes. However, Omokage activates six other dolls based on members of the Phantom Troupe to attack them but Hisoka appears to fight by their side. While Hisoka deals with three of the puppets, Omokage absorbs the other three to attain their powers and fight Gon and co. until they join forces in a combined effort that allows Kurapika to successfully restrain him with his chain.

With Omokage defeated, Kurapika offers him a chance to be spared in exchange for having his powers sealed for life, but he refuses. Killua offers himself to kill Omokage in Kurapikas place but the puppeteer is then stabbed by Ritz, who claims that he had already caused enough suffering to her and her friends. The real Phantom Troupe arrives soon after, but they decide to let Kurapika and Hisoka leave, claiming that they will settle their scores with them in another day. As the whole place is put on fire, Ritz decides to let herself be consumed by the flames along her brother and thanks Gon and Killua for the moments they spent with her. Some time later, Gon and Killua part ways with their friends as they leave to continue their adventures together.

==Cast and characters==

*Gon Freecss: Megumi Han 
*Killua Zoldyck: Mariya Ise
*Kurapika: Miyuki Sawashiro
*Leorio: Keiji Fujiwara
*Omokage: Naohito Fujiki
*Retz: Aya Hirano
*Hisoka: Daisuke Namikawa
*Illumi Zoldyck: Masaya Matsukaze
*Pairo: Umika Kawashima
*Nobunaga: Naoya Uchida
*Machi: Rena Maeda
*Uvogin: Akio Ohtsuka
*Pakunoda: Romi Park Kenn
*Franklin: Hidenobu Kiuchi 
*Feitan: Kappei Yamaguchi 
*Chrollo Lucilfer: Mamoru Miyano 
*Shizuku: Miho Arakawa 
*Shalnark: Noriko Hidaka

==Development==
In March 2012, a source linked to the Hunter × Hunter anime told Mainichi Shimbun that a theatrical film had been green-lit.  The years 34 issue of Weekly Shōnen Jump, released in July 2012, included the first image for Gekijō-ban Hunter × Hunter Phantom Rouge. It depicts Kurapikas "rouge" or scarlet eye with the number four reflecting in it, revealed that the "dramatic action" film would tell an original story and feature the Phantom Troupe.  Days later, the official website for the movie went live and streamed a teaser trailer that was also shown on television. An August issue of the magazine outlined the story and revealed the second visual image.  The first theatrical trailer was released in September. 
 Yuzu provides the them song "Reason".  A second teaser was posted to the website in November. Tickets for the film, to be released on January 12, 2013, went on sale on November 3 and included clear file folders featuring artwork from the series while supplies lasted.  

Hunter × Hunter creator Yoshihiro Togashi wrote the two-part manga   to act as a prequel to the film. A collected version of the story, referred to as "Volume 0" of the series, was given to the first million moviegoers.  A light novel adaptation of the film was released by Shueisha on January 15, 2013. 

==Release==
Hunter × Hunter: Phantom Rouge opened in 257 screens on January 12, 2013 and debuted at number one in the Japanese box office with 357,976 tickets sold and earning 456,779,000 yen (about US$5,143,930).  It earned a total of US$12,595,288 by its sixth weekend.  The home video DVD of the movie was released on July 7, 2013, selling 4,783 copies its first week for fifth place on Oricons Japan Animation DVD chart.  It sold 7,308 copies in four weeks before falling off the chart.  The film received its television debut on Nippon TV on December 27, 2013 at 2:38 am, earning a 0.7% average household rating. 
 Scotland Loves Anime festival screened the film in October 2013. 

==References==
 

==External links==
*  
*  

 
 

 
 
 
 
 
 
 