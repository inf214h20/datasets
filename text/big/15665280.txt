Doraemon: Nobita in the Wan-Nyan Spacetime Odyssey
{{Infobox film
| name           = Doraemon: Nobita in the Wan-Nyan Spacetime Odyssey
| image          = Doraemon2004dvd.jpg
| caption        = Film poster
| director       = Tsutomu Shibayama
| producer       = 
| writer         = 
| starring       = Nobuyo Ōyama   Noriko Ohara
| music          = 
| cinematography = 
| editing        = 
| studio = Shin-Ei Animation
| distributor    = Toho
| released       =  
| runtime        = 84 minutes
| country        = Japan Japanese
| budget         = 
| gross          = 2,290,570,000 yen (US$26,663,978)
}} 1979 Doraemon movie premiering on 6 March 2004 based in Japan and 3 August 2006 based in Hong Kong　Disney Channel India 2015 March 14th & Toon Disney WE SO DO I! 2015 April 17th 7:00pm & 8:30pm.

== Plot ==
An elderly dog goes to a time machine, which he plans to use to drive to the future so he can meet with "someone" who gives him a kendama. However, the machine encounters "temporal turbulence" and goes haywire and the dog is regressed back to his infant state, eventually found by a researcher cat.

In the present day, Nobita finds a stray dog drowning in a river while playing with Gian and Suneo by the river side. Feeling sorry for the dog, he decides to take it home by hiding it in a "kennel on the wall" and names it Ichi (originated from "One", which is a homophone of the sound of a barking dog in Japanese), and secretly feeds it and plays with Ichi with his kendama to find. Soon after, he also adopts a stray cat named Zubu (or wet in Japanese, due to the discovery of it in a rainstorm). Eventually, Nobitas mother gets suspicious and checks in on Nobitas room. To avoid being caught with Ichi and Zubu he and Doraemon travel through The Anywhere Door to the Mountains, but accidentally finds more stray dogs and cats endangered by deforestation. With so many pets, Nobita and his friends decide to send them back in time, 300 million years ago, where there was no other living beings around. After using the Ray of Evolution to allow them to operate a food-making machine, they depart, with Nobita promising Ichi that he will return later.

However, when they try to visit them the next day (from their point of view), they encountered a time-space anomaly, sending them crash landing 1,000 years after their original time-destination; to their surprise, they found out the dogs and cats have overused the Ray of Evolution which makes them evolved enough to form a civilized society that rivaled those of Future Society. As the time machine is broken, they have to wait until it can be fixed. In the meantime, they explore the society, where they meet with a group of teenage thieves: Bulltaro, Duk, Chiko, and the leader, Hachi. Nobita is certain that Hachi is really Ichi in disguise, despite the difference in time. Nobita and his friends accompany the thieves in infiltrating an amusement park, where they believe is the location where the thieves parents are held prisoner. Using a drill, the group arrives at a room containing a time machine, but the group are attacked by the guards and separated; Doraemon is stunned and taken by the guards while the rest are taken prisoner.

Meanwhile, the government predict a cluster of asteroids that will collide with Earth and to evacuate the citizens, they order chunks of Noradium, materials capable of building spacecrafts to be sent to the government, but they are stolen by Nekojara, a treacherous high-ranking official. Doraemon awakes to meet with Nekojara, who explains that he plans to use a Noradium-powered time machine to travel to the future so he can take revenge against humans for abandoning unwanted animals using the "devolve" function on the Ray of Evolution as written in a prophecy book written by his ancestor, Zubu. He manages to trick Doraemon to fix the ray after threatening to kill Shami, an idol cat who Doraemon falls in love with, who is actually his underling. After being freed from prison, the others stage a mission to rescue Doraemon and take the Noradium back. Doraemon manages to break the Ray of Evolution and escapes from the machine with Shami (whom Nekojara abandons) while Nobita and Hachi successfully stop the time machine from functioning. However, a meteor hits the machine and sinks it, rendering the Noradium unusable. Hachi is also sent underwater and as Nobita rescues Hachi from drowning, Hachi learns that he is in fact Ichi and is the elderly dog shown in the beginning of the film. He remembers that he had stored a Nobita-shaped statue built purely by Noradium. The group takes the statue and delivers it to the government, destroying Nekojaras time machine and captures him and his underling along the way.

The government successfully build several spacecrafts and quickly evacuate the citizens right before the asteroids fall. As Nobita departs along with his friends in the repaired time machine, he bids Ichi farewell.

==Cast==
{| class="wikitable"
|-
! Character
! Voice
|-
| Doraemon
| Nobuyo Ōyama
|-
| Nobita Nobi
| Noriko Ohara
|-
| Shizuka Minamoto
| Michiko Nomura
|-
| Takeshi "Gian" Goda
| Kazuya Tatekabe
|-
| Suneo Honekawa
| Kaneta Kimotsuki
|-
| Ichi/Hachi
| Megumi Hayashibara Osamu Saka (old)
|-
| Chiko
| Hitomi Shimatani
|-
| Duk
| Tomokazu Seki
|-
| Bulltaro
| Hisao Egawa
|-
| Shami
| Mika Kanai Hitomi Shimatani (singing)
|-
| Nekojara
| Shigeru Izumiya
|-
| Nyago
| Toshio Furukawa
|-
| President
| Tōru Ōhira
|-
| Zubu
| Yūko Mizutani
|-
| Hachis Mother
| Keiko Han
|-
| Tama
| Nana Yamaguchi
|-
| Nekojaras soldiers
| Shin Aomori Shinya Ōtaki
|-
| Secretaries Kenichi Ogata Bin Shimada
|-
| Advisor
| Junichi Sugawara
|-
| Attraction Presenter
| Shinichiro Ohta
|-
| Policemen
| Yuu Shimaka Kōzō Mito
|-
| Fish Vendor Takashi Taguchi
|-
| Nyako
| Yūko Satō
|-
| Anchorman
| Noritsugu Watanabe
|}

==References==
 

== External links ==
* 
* 
*  

 
 

 
 
 
 
 
 
 
 