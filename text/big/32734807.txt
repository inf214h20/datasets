Blackbird (2007 film)
{{Infobox film
| name           = Blackbird
| image          =
| caption        =
| director       = Adam Rapp
| producer       = Mark Fallon
| screenplay     = Adam Rapp
| starring       = Gillian Jacobs Paul Sparks Danny Hoch
| music          = Dawn Landes
| cinematography = Richard Rutkowski
| editing        = Michael Taylor
| studio         = Blackbird Project
| distributor    =
| released       =  
| runtime        =
| country        = United States
| language       = English
| budget         = $475,000
| gross          =
}}

Blackbird is a 2007 American drama film. It was adapted from a play written by Adam Rapp.

It played at the Edinburgh Film Festival. It won "Best Narrative Feature" at the Charlotte Film Festival for writers Adam Rapp and Bruce Romans.   

== Cast ==
* Stephen Adly Guirgis as Mercato Guy Boyd as Landlord
* Annie Parisse as Angie
* Michael Shannon as Murl
* Danny Hoch as Pinchback
* Gillian Jacobs as Froggy
* Anthony Rapp
* Ross Brodar as Artist poker player
* Kristina Dargelyte as Bartender
* Christopher Denham as Clarke
* Robert Oppel as Skateboarder
* Paul Sparks as Baylis

== References ==
 

== External links ==
*  

 
 
 
 
 