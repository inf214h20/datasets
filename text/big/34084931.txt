The Lone Wolf in Paris
 
{{Infobox film
| name           = The Lone Wolf in Paris
| image          =
| caption        =
| director       = Albert S. Rogell
| producer       =
| writer         =
| narrator       =
| starring       =
| music          =
| cinematography = Lucien Ballard Otto Meyer
| studio         = Columbia Pictures
| released       =  
| runtime        = 66 minutes
| country        = United States English
| budget         =
}}

The Lone Wolf in Paris (1938) is an American film, one of Columbias Lone Wolf film series.

In the start-and-stop history of the Lone Wolf series, this entry is the only one with Lederer as star.  It stands alone between Melvyn Douglass The Lone Wolf Returns in 1935, and the first of Warren Williams series of nine, The Lone Wolf Spy Hunt, released the following year.

== Cast ==

* Francis Lederer as Michael Lanyard
* Frances Drake as Princess Thania of Arvonne
* Olaf Hytten as Jenkins
* Walter Kingsford as Grand Duke Gregor de Meyerson
* Leona Maricle as Baroness Cambrell
* Albert Dekker as Marquis Louis de Meyerson
* Maurice Cass as Alfonse Fromont, hotel manager
* Bess Flowers as Davna
* Ruth Robinson as The Queen of Arvonne
* Pio Peretti as Prince Paul of Arvonne
* Eddie Fetherston as Mace, henchman

== External links ==
*  

 

 
 
 
 
 
 
 
 


 