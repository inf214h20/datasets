Tom Thumb (film)
{{Infobox film name           = Tom Thumb image          = TomthumbPoster.jpg caption        = Original poster by Reynold Brown director       = George Pal producer       = George Pal writer         = Ladislas Fodor starring       = Russ Tamblyn Alan Young Terry-Thomas Peter Sellers June Thorburn music          = Douglas Gamley Kenneth V. Jones cinematography = Georges Périnal editing        = Frank Clarke studio         = Galaxy Pictures Limited distributor    = Metro-Goldwyn-Mayer released       =   runtime        = 98 minutes country        = UK US language  English
|budget         = $909,000 The Eddie Mannix Ledger’, Margaret Herrick Library, Center for Motion Picture Study, Los Angeles  gross          = $3,250,000 
}}
 same name, is about a tiny man who manages to outwit two thieves determined to make a fortune from him.
 British supporting adoptive parents, and comic actors Terry-Thomas and Peter Sellers as the villainous duo who try to exploit the tiny hero for profit.
 Tom Howard animated and Ken Jones wrote the music and Peggy Lee wrote the songs.

The film is referenced in The Wonderful World of the Brothers Grimm (1962) and Pinkeltje (film)|Pinkeltje (1978). The film is also featured in Thats Dancing! (1985)

The filming locations for the movie were in Hollywood, Los Angeles, California, USA and London, England.

==Cast==
*Russ Tamblyn as Tom Thumb
*Alan Young as Woody
*Terry-Thomas as Ivan
*Peter Sellers as Antony
*June Thorburn as Forest Queen
*Bernard Miles as Jonathan, Toms Father
*Jessie Matthews as Anne, Toms Mother Ian Wallace as The Cobbler
*Peter Butterworth as Kapellmeister
*Peter Bull as Town Crier
*Stan Freberg as Yawning man (voice)
*Dal McKennon as Con-Fu-Shon (voice)
*Suzanna Leigh and Grace Lantz (Gracie Stafford) (uncredited)

==Soundtrack==
*"Tom Thumbs Tune"
:(uncredited)
:Music and Lyrics by Peggy Lee
:Sung and danced by Russ Tamblyn and the Puppetoons
*"After All These Years"
:(uncredited)
:Music by Fred Spielman
:Lyrics by Janice Torre
:Sung by Jessie Matthews (dubbed by Norma Zimmer)
*"Talented Shoes"
:(uncredited)
:Music by Fred Spielman
:Lyrics by Janice Torre
:Sung by Ian Wallace
*"The Yawning Song"
:(uncredited)
:Music by Fred Spielman
:Lyrics by Kermit Goell
:Sung by Stan Freberg
*"Are You a Dream"
:(uncredited)
:Music and Lyrics by Peggy Lee
:Sung by Alan Young

==Reception==
The film was the 8th most popular movie at the British box office in 1959. 

According to MGM records the film earned $1,800,000 in the US and Canada and $1,450,000 elsewhere, making a profit of $612,000. 

==Awards==
At the 1959 Academy Awards, the film won an Oscar for Tom Howard in the category of Best Effects, Special Effects.

At the 1959 BAFTA Awards, Terry Thomas was Nominated for a BAFTA Film Award in the category of Best British Actor.

At the 1959 Golden Globes, the film was nominated for Best Motion Picture - Musical.

At the 1959 Laurel Awards, the film was nominated for Top Musical, while Russ Tamblyn was nominated for a Golden Laurel for Top Male Musical Performance.

At the 1959 Writers Guild of America Ladislas Fodor was nominated for a WGA Award (Screen) for Best Written American Musical.

==Home video==
Tom Thumb has been released as a DVD for all regions and to VHS.   

==References==
 

==External links==
* 
*  at Rotten Tomatoes
*  at Yahoo! Movies
*  at Answers.com
* 
*  at Toonerific Cartoons
*  at Soundtrack Collector
*  at Moria.co.nz
*  at RopeOfSilicon.com
*  at Kiddie Matinee
*  at VideoHounds MovieRetriever.com
*  at Zelluloid.de (German)
*  at MovieMaster.de (German)

 
 

 
 
 
 
 
 
 
 
 
 