How Do You Like Them Bananas?
 
{{Infobox film
| name           = How Do You Like Them Bananas?
| image          = 
| image_size     = 
| caption        = 
| director       = Lionel Rogosin
| producer       = Lionel Rogosin
| writer         = 
| narrator       = 
| starring       = Edward Sorensen, Dean Preece
| music          = 
| cinematography = Jerry Kay
| editing        = John Schultz
| distributor    = 
| released       =  
| runtime        = 10 minutes
| country        = United States
| language       = English
}}
How Do You Like Them Bananas? (1966) is the sixth film directed by American independent filmmaker Lionel Rogosin. It consists of a short sketch, done in an improvised slapstick style, featuring a "Reverend" (Sorensen) and a "Banker" (Preece) haggling over a donation to a local parish.

==External links==
*  

 
 
 


 