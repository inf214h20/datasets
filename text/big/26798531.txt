Job, czyli ostatnia szara komórka
{{Infobox film 
  | name = Job, czyli ostatnia szara komórka
  | image = 
  | caption = 
  | director = Konrad Niewolski
  | producer = Mariusz Łukomski
  | writer = Konrad Niewolski
  | starring = Tomasz Borkowski Andrzej Andrzejewski Borys Szyc Agnieszka Włodarczyk
  | music = Sebastian Skalski  Rafał Hoffman
  | cinematography = Mikołaj Łebkowski
  | editing = Jarosław Pietraszek
  | distributor = Monolith Video
  | released = November 3, 2006
  | runtime = 92 minutes
  | country = Poland Polish
  | budget = 2,900,000 zlotys ($1,017,000/€755,000/£668,000)
  | preceded_by = 
  | followed_by =
    }}
 2006 Poland|Polish comedy film directed by Konrad Niewolski.

The film is a set of many popular jokes, and also a story of three friends - Adi, Pele and Chemik, who experience many adventures.

== Cast ==
* Tomasz Borkowski - Adi
* Andrzej Andrzejewski - Pele
* Borys Szyc - Chemik
* Agnieszka Włodarczyk - Karolina
* Elżbieta Jarosik - Gorzyńska   
* Aleksander Mikołajczak - Film critic
* Maria Klejdysz - Grandmother of Pele    Henryk Gołębiewski - Uncle Edi   
* Arkadiusz Detmer - Łukasz from "Symetria"   
* Jerzy Schejbal - Ambassador of Hungary
* Piotr Zelt - Football fan at the doctors   
* Krzysztof Ibisz - Host of the game show
* Janusz Onufrowicz - Rastafari man
* Sławomir Sulej - Man from ORMO
* Rafał Cieszyński - Fitness instructor 
* Paweł Nowisz - Driving instructor

== External links ==
* 
 

 
 
 
 
 


 