The Scapegoat (1959 film)
 
{{Infobox film
| name           = The Scapegoat
| image          = The Scapegoat, film poster.jpg
| image_size     = 180px
| caption        = Theatrical release poster
| director       = Robert Hamer
| producer       = Michael Balcon
| writer         = Robert Hamer Gore Vidal
| based on       =  
| narrator       =
| starring       = Alec Guinness Nicole Maurey Bette Davis
| music          = Bronislau Kaper
| cinematography = Paul Beeson Jack Harris
| distributor    =MGM
| released       = August 6, 1959 (US)
| runtime        = 91 minutes
| country        = UK English
| budget         = $943,000  . 
| gross          = $1,195,000 
}}
 1959 crime novel of the same name by Daphne du Maurier, and starring Alec Guinness, Nicole Maurey and Bette Davis.

==Plot== French at a British university, vacations in France. There, by chance, he meets his double, French nobleman Jacques De Gué (Guinness again). They become acquainted. Barratt becomes drunk and accepts De Gués invitation to share his hotel room. When he wakes up the next morning, Barratt finds himself alone in the room, with his clothes and passport missing. De Gués chauffeur Gaston (Geoffrey Keen) shows up to take his master home, and Barratt is unable to convince him that he is not the nobleman. Gaston calls Dr. Aloin (Noel Howlett), who diagnoses the Englishman as suffering from schizophrenia.
 Pamela Brown) and formidable mother, the Countess (Bette Davis). None of them believe his story - it appears that De Gué is a malicious liar - so Barratt resigns himself to playing along. As time goes on, he feels needed, something missing in his sterile prior life.

The next day, brother-in-law Aristide (Peter Bull) discusses business with him. Later, in the nearby town, Barratt is nearly run down by De Gués mistress, Béla (Nicole Maurey), on her horse. He spends the usual Wednesday afternoon tryst getting acquainted with her. The next time they meet, before he can confess the truth, she informs him that she has already guessed it.

Barratt delves into the neglected family Glassblowing|glass-making business. He decides to renew a contract with the local foundry, even on unfavourable terms, to avoid throwing the longtime employees out of work. The Countess is upset by his decision and mentions a marriage contract. When Barratt investigates, he learns that Françoises considerable wealth, tied up by her businessman father, would come under his control if she were to die. Françoise finds him reading the contract and becomes very upset, accusing him of wanting to see her dead. Barratt consoles her by telling her that the contract can be changed. He begins to suspect the reason for De Gués disappearance.

One day, Barratt receives a message from Béla. He goes to see her and spends a pleasant afternoon with her, though she denies having sent for him. When he returns to the chateau, he learns that Françoise has died from a fall. Blanche accuses Barratt of murder, stating that she overheard him with his wife in her room just before her death. However, Gaston provides an unshakable alibi, having driven Barratt to his rendezvous with Béla. 

Barratt is not surprised when De Gué resurfaces shortly afterwards. They meet in private; the Frenchman demands his identity back, but Barratt refuses. Both men have come armed and shots are exchanged. Barratt emerges victorious and returns to his new life and Béla.

==Cast==
* Alec Guinness as John Barratt / Jacques De Gué  
* Bette Davis as Countess De Gué    
* Nicole Maurey as Béla  
* Irene Worth as Françoise   Pamela Brown as Blanche  
* Annabel Bartlett as Marie-Noel  
* Geoffrey Keen as Gaston  
* Noel Howlett as Dr. Aloin  
* Peter Bull as Aristide  
* Leslie French as Lacoste   Alan Webb as Inspector  
* Maria Britneva as Maid  
* Eddie Byrne as Barman  
* Alexander Archdale as Gamekeeper  
* Peter Sallis as Customs Official

==Production==
According to Robert Osborne of Turner Classic Movies, the original choice for Barratt / De Gué was Cary Grant, but Daphne du Maurier insisted on Guinness because he reminded her of her father, actor Gerald du Maurier.

Osborne also states that when Hamer was drunk, Guinness handled the directing chores.
==Box Office==
According to MGM records the film earned $570,000 in the US and Canada and $625,000 elsewhere resulting in a loss of $382,000. 

==References==
 

==External links==
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 