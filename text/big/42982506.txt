The Devil Came from Akasava
{{Infobox film
 | name =The Devil Came from Akasava
 | image = Devil_Akasava.jpg
 | image_size =
 | caption =
 | director = Jesús Franco (as Jess Frank)
 | producer =Artur Brauner Karl Heinz Mannchen
 | writer = Ladislas Fodor Paul André
 | based on =   Fred Williams Horst Tappert
 | music = Manfred Hübler Siegfried Schwab
 | cinematography = Manuel Merino
 | editing =
 | studio=
 | distributor = 
 | released = 5 March 1971 	
 | runtime = 83 min 
 | country = West Germany Spain
 | language = German
 | budget =
 }} Spanish adventure film|adventure-spy film directed by Jesús Franco. 

==Background== Edgar Wallace film adaptations that were particularly popular in Germany during the 1960s.

==Plot== British scientist Fred Williams), professors nephew who is also concerned of his fate and arrives in the country for further investigation.

==Cast==
*Soledad Miranda (as Susann Korda): Jane Morgan Fred Williams: Rex Forrester
*Jesús Franco (cameo): Tino Celli 
*Horst Tappert: Dr. Andrew Thorrsen
*Alberto Dalbés: Irving Lambert
*Ewa Strömberg: Ingrid Thorrsen
*Ángel Menéndez: Prof. Walter Forrester
*Siegfried Schürenberg: Sir Philipp
*Walter Rilla: Lord Kingsley Paul Müller: Dr. Henry
*Blandine Ebinger: Abigaile Kingsley
*Howard Vernon: Humphrey

==References==
 
==External links==
* 
 
 
 
 
 
 
 
 
 
 
 
 
 