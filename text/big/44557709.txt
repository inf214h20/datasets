Touch the Wall
Touch New York on  Sunday November 23, one day before USA Swimming Foundations Golden Goggles awards which were also in New York. The New York premiere was co-presented with USA Swimming.    The film was directed and produced by Christo Brock and Grant Barbeito, a filmmaking duo from Los Angeles. 

==Plot summary==
Touch the Wall is the story of two Olympic swimmers – Gold-Medalist Missy Franklin and Silver-Medalist Kara Lynn Joyce – and their journey to the 2012 London Olympics.  When the veteran Joyce joins teenager Franklin and her age-group swim club, everything changes.  The veteran Kara finds a new start and a world-class training partner; Missy finds a veteran and older sister to learn from.  Together they train, compete, and support each other until the pool becomes too big for the two of them.  Thrown apart by coach and circumstance, they reunite at Olympic Trials to redefine what it means to win.

==Cast==
*Missy Franklin
*Kara Lynn Joyce
*Rowdy Gaines
*Jack Roach
*Cullen Jones David Marsh

==Critical reception==
Despite a lackluster reception from New York critics, the film was widely embraced by the athletic and swim press for its authenticity and emotional power.  The film was noted for its celebration of female physical and emotional strength.  The Denver Post said the film was often exhilarating and full of insight.  

==Audience Reaction==
The film has been broadly embraced by the swim and athletic communities  The film has a 94% Audience rating from Rotten Tomatoes   and a 8.7 rating on IMDB.com  

==References==
 

==External links==
 
* 



 
 
 
 
 
 
 
 
 