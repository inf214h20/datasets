The Way We Laughed
{{Infobox film
| name           = The Way We Laughed
| image          = The Way We Laughed .jpg
| image_size     = 
| caption        = 
| director       = Gianni Amelio
| producer       = 
| writer         = Gianni Amelio Daniele Gaglianone Lillo Iacolino Alberto Taragli
| narrator       = 
| starring       = Francesco Giuffrida Enrico Lo Verso
| music          = Franco Piersanti
| cinematography = Luca Bigazzi
| editing        = 
| distributor    = 
| released       = September 1998 (premiere at Venice Film Festival|VFF) 2 October 1998 (Italy) 21 November 2001 (U.S.)
| runtime        = 124 minutes
| country        = Italy
| language       = Italian
| budget         = 
| gross          =
| preceded_by    = 
| followed_by    = 
}} Sicilian brothers, Giovanni and Pietro, who emigrate to the city of Turin. Giovanni works hard to help Pietro study to be a teacher, but Pietro does poorly. Then Pietro disappears.

The film won the Golden Lion at the Venice Film Festival.

== Cast ==
*Enrico Lo Verso as Giovanni
*Fabrizio Gifuni as Pelaia
*Francesco Giuffrida as Pietro
*Rosaria Danzè as Lucia
*Renato Liprandi as the professor

== External links ==
*  

 

 
 
 
 
 
 
 
 
 