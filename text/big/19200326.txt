Faith (film)
{{Infobox film
| name           = Faith
| image          = Faith 1916.jpg
| image_size     =
| caption        = James Kirkwood
| producer       =
| writer         = James Kirkwood
| starring       = Mary Miles Minter
| cinematography = Carl Widen
| editing        =
| distributor    = Mutual Film
| released       =  
| runtime        =
| country        = United States Silent English intertitles
| budget         =
}}
 silent drama James Kirkwood. The film survives and is preserved at George Eastman House, Rochester. 

==Plot==
Faith is an orphan who befriends Mark Strong, an alcoholic lawyer. While Mark is trying to get sober again, Faith is falsely accused for theft and is arrested. Mark immediately comes to the rescue.

==Cast==
* Mary Miles Minter - Faith
* Perry Banks - Mark Strong
* Clarence Burton - John Thorpe
* Lizette Thorne - Helen
* Margaret Shelby - Laura
* Josephine Taylor - Mrs. Thorpe
* Gertrude Le Brandt - Mrs. Stimson

==Reception==
The New York Times noticed that producers of the film were attempting to make leading actress Minter famous with this picture, but gave all credit to Banks, stating he "walked away with this film". 

==References==
 

==External links==
* 

 
 
 
 
 
 
 

 