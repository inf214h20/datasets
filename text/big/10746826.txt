The Girl and the General
{{Infobox film

| name           = The Girl and the General

| image          = Girl_general_moviep2.jpg

| alt            = 

| caption        = American theatrical release poster

| director       = Pasquale Festa Campanile

| producer       = Carlo Ponti Luciano Perugia

| writer         = Luigi Malerba

| starring       = Rod Steiger Virna Lisi Umberto Orsini

| music          = Ennio Morricone

| cinematography = Ennio Guarnieri

| editing        = 

| studio         = 

| distributor    = Metro-Goldwyn-Mayer

| released       =  

| runtime        = 103 minutes

| country        = Italy

| language       = Italian

| budget         = 

| gross          = 
}}
The Girl and the General ( ) is a 1967 anti-war Italian comedy film starring Rod Steiger and Virna Lisi and produced by Carlo Ponti.

In it, a young woman (Virna Lisi) and a soldier team up to deliver an Austrian General (Rod Steiger) to Italian forces during World War I. Their quest for the 1000 Lire reward changes their lives unexpectedly.

== External links ==
*  

 
 
 
 
 
 
 
 
 


 
 