Rabbit Hole (film)
 
{{Infobox film name           = Rabbit Hole image          = Rabbit Hole Poster.jpg alt            = caption        = Theatrical release poster director       = John Cameron Mitchell producer       = Nicole Kidman Leslie Urdang Gigi Pritzker Per Saari Dean Vanech screenplay     = David Lindsay-Abaire based on       =   starring       = Nicole Kidman Aaron Eckhart Dianne Wiest Miles Teller Tammy Blanchard Sandra Oh music          = Anton Sanko cinematography = Frank G. DeMarco editing        = Joe Klotz studio         = Blossom Films OddLot Entertainment distributor  Lionsgate
|released       =   runtime        = 91 minutes   country        = United States language       = English budget         = $5 million  gross          = $5,143,154    
}} adaptation by play of Academy Award, Golden Globe Screen Actors nationwide on January 14, 2011. 

==Plot==
Becca and Howie Corbett mourn the death of their 4-year-old son Danny who was killed in a car accident when he ran out into the street after his dog. Becca wants to give away Dannys clothes, remove Dannys things, and sell their house, but Howie is angry at Beccas elimination of anything that reminds them of their child. Howie also wants to resume sexual relations with Becca and have another child, but she rejects his advances.

Beccas mother, Nat, compares herself with Becca as she lost a 30-year-old son from a drug overdose. Becca states the two deaths are not comparable but eventually realizes their grief is the same and will never stop. Beccas sister, Izzy, is pregnant, and Becca keeps giving Izzy advice about becoming a mother, which Izzy resents.

Becca and Howie attend a self-help group, but Becca is irritated by some members of the group, particularly by one couple who attribute their childs death to Gods will. Howie continues to attend the meetings without Becca, and he and long-time member Gabby almost begin an affair. However, Howie backs out of it.
 parallel universes, and gives it to Becca, who thinks it is wonderful. Howie does not like Beccas meetings with Jason.

Howie and Becca begin to have new activities, such as bowling and playing games, and they start to accept their sons death.

Howie and Becca decide to have a garden lunch. The scene begins with Howie telling Becca how the lunch would take place, while simultaneously the screen fades into the lunch as Howie continues to speak in the background. The film ends with Becca and Howie sitting in their garden alone holding hands after all their guests have left.

==Cast==
*Nicole Kidman as Becca Corbett
*Aaron Eckhart as Howie Corbett
*Dianne Wiest as Nat, Beccas mother
*Miles Teller as Jason, the driver
*Tammy Blanchard as Izzy, Beccas sister
*Sandra Oh as Gabby
*Patricia Kalember as Peg Mike Doyle as Craig
*Jon Tenney as Rick
*Stephen Mailer as Kevin
*Giancarlo Esposito as Auggie
*Rob Campbell as Bob
*Phoenix List as Danny

==Production==
Rabbit Hole was filmed primarily in the  . Retrieved August 27, 2009. 

Due to a scheduling conflict, Kidman declined a role in Woody Allens You Will Meet a Tall Dark Stranger, in favor of this film.  In a 2014 interview on The Howard Stern Show, Eckhart claimed to have researched his role by pretending to have lost a child in a support group. 
 compose the score,  but then Abel Korzeniowski was announced.  Ultimately, the position went to Anton Sanko.

==Release== premiered at the 2010 Toronto International Film Festival in September 2010, then played at three other film festivals (Mill Valley Film Festival in October, and both Denver Film Festival, and Rome Film Festival in November). The film opened in Canada and the United States in December 17 2010 in a limited release in 5 theaters and grossed $53,778 averaging $10,756 per theater and ranking 38th at the box office. The widest release domestically for the film was 131 theaters and it ended up earning $2,229,058 in the U.S. and  $2,914,096 internationally for a total of $5,143,154, recovering its $5 million production budget.  

==Reception==
Rabbit Hole received positive reviews and has a rating of 86% on Rotten Tomatoes based on 189 reviews with an average score of 7.6 out of 10. The consensus states "Its often painful to watch, but Rabbit Holes finely written script and convincing performances make it worth the effort."  The film also has a score of 76 out of 100 on Metacritic based on 39 reviews. 

Festival and other advance showings of the film garnered good reviews, particularly for Kidman and Wiest. The film received a   gave it 3.5 stars out of 4, calling it "... entertaining and surprisingly amusing, under the circumstances. The film is in a better state of mind than its characters. Its humor comes, as the best humor does, from an acute observation of human nature. We have known people something like this. We smile in recognition. Apart from anything else, "Rabbit Hole" is a technical challenge. It is simple enough to cover the events in the story, not so simple to modulate them for humor and even warmth. I knew what the movie would be about, but I was impressed by how it was about it."  Richard Corliss of Time (magazine)|Time magazine named it one of the Top 10 Movies of 2010. 

==Differences from the play==
The play has a cast of five roles, while a few other characters such as Gabby are only mentioned in dialogue. In contrast, the film has a cast of over a dozen actors. While the entire play takes place in the home of Becca and Howie, the film has a variety of locations. Past incidents, such as Beccas bad experience in the grief support group, are referred to in the plays dialogue but are depicted on screen in the film.         The videos that Howie obsesses over are actually seen in the film, though not in the play.    The two subplots of Howies relationship with a woman from the grief support group and Beccas relationship with Jason, the driver of the car that hit Danny, have both been expanded. The film also adds new characters who do not appear in the play: sister Izzys boyfriend and Howies best friend. 
	
Jason is an aspiring science fiction story writer in the play, but an aspiring comic book artist in the film. 

In the opinion of critic Jim Lane, the film is more focused on the husband and wife and less of an ensemble piece. Lane writes 
 On stage, Rabbit Hole is a tightly focused five-character drama punctuated with sharp, surprising flashes of aching humor. In the movie, however, supporting roles are trimmed into near irrelevance, elbowed into the background by the spotlight focused on Becca and Howie—or, more bluntly, on Nicole Kidman and Aaron Eckhart.

Here’s what David Lindsay-Abaire seems not to understand about his own play: It’s like an atom in which the five characters are electrons revolving around the missing nucleus that was Danny.... Without their nucleus, these electrons wobble and flail in their orbits, by turns clutching at and repelling one another.... In the movie, Rabbit Hole’s symmetrical stage design is torn between the age-old pitfall of “opening up” a play and the Hollywood urge to focus on Kidman and Eckhart (who are, after all, the stars).....The movie orbits Becca and Howie instead of the lost Danny.  

The director of a 2010 stage production of Rabbit Hole, Robert A. Norman, declared, "The 2010 movie version starring Nicole Kidman lacked the humor and hopefulness of the stage script. Our production will have plenty of both of those things."  However, Abaire, who wrote both the stage play and screenplay, believes, "For the film, we cut so much that worked in the play that I worried we had cut all the laughs. But there were all these other laughs I didnt know were there." 

==Accolades==
;Wins
*Denver Film Festival Excellence in Acting Awards - Aaron Eckhart 
*Heartland Film Festival Truly Moving Picture Award - Nicole Kidman and Per Saari 

;Nominations
*Academy Award for Best Actress - Nicole Kidman
*Alliance of Women Film Journalists Award for Best Actress - Nicole Kidman
*Alliance of Women Film Journalists Award for Best Adapted Screenplay - David Lindsay-Abaire
*Broadcast Film Critics Association Award for Best Actress - Nicole Kidman
*Chicago Film Critics Association Award for Best Screenplay - David Lindsay-Abaire
*Dallas-Fort Worth Film Critics Association Award for Best Actress - Nicole Kidman
*Detroit Film Critics Society Award for Best Actress - Nicole Kidman
*Golden Globe Award for Best Actress - Motion Picture Drama - Nicole Kidman
*Houston Film Critics Society Award for Best Actress - Nicole Kidman
*Independent Spirit Award for Best Director - John Cameron Mitchell
*Independent Spirit Award for Best Female Lead - Nicole Kidman
*Independent Spirit Award for Best Male Lead - Aaron Eckhart
*Independent Spirit Award for Best Screenplay - David Lindsay-Abaire
*Italian Online Movie Award for Best Actress - Nicole Kidman
*Las Vegas Film Critics Society Award for Best Actress - Nicole Kidman
*Online Film Critics Society Award for Best Actress - Nicole Kidman
*San Diego Film Critics Society Award for Best Actor - Aaron Eckhart
*Satellite Award for Best Actress - Motion Picture Drama - Nicole Kidman
*Satellite Award for Best Supporting Actress - Motion Picture - Dianne Wiest
*Screen Actors Guild Award for Outstanding Performance by a Female Actor in a Leading Role - Motion Picture - Nicole Kidman
*St. Louis Gateway Film Critics Association Award for Best Actress - Nicole Kidman
*Utah Film Critics Association Award for Best Actress - Nicole Kidman
*Washington D.C. Area Film Critics Association Award for Best Actress - Nicole Kidman

==References==
 

==External links==
* 
* 
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 