Suren Suror Putek
{{Infobox film
| name           = Suren Suror Putek
| image          = 
| director       = Chandra Mudoi
| producer       = 
| starring       = Jatin Bora  Prastuti Parashar  Nishita Goswami  Chetana Das
| music          = Dr Hitesh Baruah
| cinematography =
| editing        = 
| studio         = 
| distributor    = Gargi Entertainment Co. Pvt. Ltd.
| released       = 2005
| runtime        =  
| country        = India
| language       = Assamese
| budget         = 
| gross          = 
}}
Suren Suror Putek ( ) is an Assamese language commercial movie directed by Chandra Mudoi. The title role is played by Jatin Bora.       This is the first Assamese film to highlight the art of magic and the talented magicians of the state. 

==Plot summary==
The film revolves around the escapades of Soni (Jatin Bora), the son of a thief named Suren. Realising that he will always be looked down upon as his fathers son, the youth takes to magic in a bid to achieve success and earn respect. 

==Cast==
*Jatin Bora as Soni
*Prastuti Parashar
*Nishita Goswami
*Dr Rajen Jaiswal
*Chiranjiv Mahanta
*Emon Kashyap
*Prabal Barthakur
*Gargi

==Awards==
*Jyotirupa Join Media Award For Excellence in Film Television & Music - Best Actor in Comic Role: Jatin Bora

==See also==
*Jollywood

==References==
 

 
 
 


 