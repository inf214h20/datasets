Corrado (film)
 
{{Infobox film
| name        = Corrado
| image       = Corrado.jpg
| director    = Adamo Paolo Cultraro
| producer    = Adamo Paolo Cultraro
| writer      = Adamo Paolo Cultraro
| music       = Ryan Franks
| cinematography = David Fox
| editing     = Paolo Viscomi
| studio      =
| distributor =
| released    =  
| runtime     = 83 minutes
| country     = United States
| language    = English
| budget      =
}} Johnny Messner, Candace Elaine, and Edoardo Ballerini.

==Plot==
Corrado is the story of a Los Angeles hit man of the same name. Corrado (Messner) is given the task of eliminating the aging kingpin Vittorio Spinello. He readily accepts the job and is about to perform the hit when he is interrupted by Spinellos new nurse, Julia (Elaine). He shoots the aging Spinello by accident, instead of suffocating him as intended, and flees the scene. Julia is wrongly blamed for the death, and is herself about to be killed by Vittorios son Paolo (Sizemore), when Corrado rescues her. They are then pursued all over Los Angeles by Paolo and his goons in a bid to escape.

==Cast== Johnny Messner as Corrado
* Tom Sizemore as Paolo Spinello
* Candace Elaine as Julia
* Edoardo Ballerini as Salvatore
* Ken Kercheval as Vittorio Spinello
* Joseph Gannascoli as Frankie
* Frank Stallone as Tommaso
* Tony Curran as Officer Tony
* Stelio Savante as Antonio

==External links==
*  
*  

 
 
 
 
 


 