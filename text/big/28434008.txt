The Greatest Ears in Town: The Arif Mardin Story
 
 
The Greatest Ears in Town: The Arif Mardin Story is a 2010 documentary about the Grammy winning music producer Arif Mardin. The documentary was produced by his son Joe Mardin and was directed by Doug Biro. It was made in the years prior to Arifs death in 2006 from pancreatic cancer. 

==Synopsis==
The documentary goes behind the scenes in the recording studio with Arif Mardin. It shows him working with artists such as Norah Jones and Jewel (singer)|Jewel. The documentary also features artists such as Aretha Franklin and producers such as George Martin reflecting on his life and career. It also explores his personality and humorous nature.

==Release== Recording Academy. It was first screened in New York City on the day of its release. After its premiere screening, a discussion about Arif Mardin followed. It was hosted by the Recording Academys Producers and Engineers Wing. The discussion was moderated by Joe Mardin and panelists included Phil Ramone, Russ Titelman, Doug Biro, Jimmy Douglass, Michael OReilly and Frank Filipetti. It later premiered at the Grammy Museum in Los Angeles, which featured a panel discussion with Quincy Jones and Chaka Khan. It also premiered in Miami, which featured a panel discussion with Barry Gibb.

==External links==
*  
*http://latimesblogs.latimes.com/music_blog/2010/06/producer-arif-mardin-celebrated-in-documentary-the-greatest-ears-in-town.html
*http://www.grammy365.com/gallery/new-york-premiere-screening-greatest-ears-town-arif-mardin-story-photos
 
 
 
 
 
 
 

 