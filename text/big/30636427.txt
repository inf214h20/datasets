Roop Ki Rani Choron Ka Raja (1993 film)
{{Infobox film
| name           = Roop Ki Rani Choron Ka Raja
| image          =
| image size     =
| alt            =
| caption        =
| director       = Satish Kaushik
| producer       =Boney Kapoor
| writer         = Javed Akhtar
| screenplay     =
| story          =
| based on       =  
| narrator       =
| starring       =Anil Kapoor Sridevi Anupam Kher Paresh Rawal Jackie Shroff Ram Sethi
| music          = 
| cinematography =Baba Azmi
| editing        =Waman Bhonsle Gurudutt Shirali
| studio         =
| distributor    =
| released       =  
| runtime        =190 min.
| country        =India
| language       = Hindi

}}

Roop ki Rani Choron Ka Raja is a 1993 Bollywood film starring Anil Kapoor, Sri Devi, Jackie Shroff, Anupam Kher and Johnny Lever. It was the highest budget film of that time. The film was directed by Satish Kaushik and written by Javed Akhtar. The project was announced in 1987 and was initially being directed by Shekhar Kapur, who left the project halfway through, he was subsequently replaced by Satish Kaushik, despite its massive budget, escalated by the delay, it flopped at the box office upon its release in 1993.

Actor Arshad Warsi, then a dancer-choreographer choreographed the title track for the film. 

==Plot==

Jagmohan Lal aka Jugran is a crime lord who wants to climb up the crime ladder. Once, when Customs Officer Verma tries to nab him, he escapes. To save his skin, Jugran kills his twin brother Manmohan Lal and takes the identity of latter. The world is led to believe that Jugran is still on the run. Meanwhile, Jugran kills Verma and a Dr. Ashok, destroying both the families in process. While Vermas orphaned sons Ramesh and Ravi are separated from each other, Dr. Ashoks daughter Seema is left with her distraught mother, who has lost her sanity.

Many years later, Jugran has become a big name in underworld. He lives a double life as his twin brother Manmohan, who is highly respected in society. Meanwhile, Ravi Verma has become an Inspector and Ramesh has become a crook and a safecracker by the name of Romeo. While both the brothers are unaware of each others identities, there is one things that binds them together: one has a lock, other has the only key that can open the lock. Seema too has become a crook called Simmi and her path starts crossing with Romeos. Romeo is now working for Seth Girdharilal, who mentors many thieves and goons like him.

Only Ravi knows that Jugran killed his father. One day, Seema runs into "Manmohan", who realizes that he had killed her father. Jugran himself needs somebody to pull a Rs. 100 crore heist. He meets her as Jugran and tells her that he will need her help to find Romeo for the heist. Simmi hesitates, until Jugran reveals that he knows her fathers murderers identity. She catches upon Romeo and tries to seduce him, but Romeo berates her. Now, Simmi reveals her story to him. Romeo is surprised to see Simmis mother, since it means that Simmi is actually his childhood sweetheart Seema.

Now he reveals his true identity to Seema, causing the duo to pair up. Meanwhile, Ravi too gets an inkling that Jugran is planning something big with help of Simmi and Romeo. Ravi and Romeo run into each other, only to learn that they are brothers. After many twists and turns, Simmi and Romeo finally succeed in turning over the loot to Jugran. After they turn over the loot to Jugran, he binds and gags them after telling Simmi the truth, leaving the duo to die by falling in a boiler containing boiling acid. The duo are saved by Romeos pet pigeon.

Romeo and Simmi escape, but they get separated from each other. Here, Romeo runs into Ravi, whereupon the latter tries to arrest him. After Romeo tells him the truth, an enraged Ravi reveals to Ramesh that Jugran killed their father too. Ramesh escapes and rejoins Simmi. When he sees "Manmohan" on TV, Ramesh tells Seema that Manmohan and Jugran are the same person. Meanwhile, Ravi gets a tipoff, upon which he turns up at a party almost simultaneously with Ramesh and Seema. Ramesh reveals that when his pigeon perched on Jugrans left hand, it left claw marks, just as seen on Manmohans left hand. Therefore, Manmohan and Jugran are same person.

Ravi scoffs at the claim and tells them that Manmohans fingerprints are available and it wont be difficult to find out that Manmohan and Jugran are indeed two different people. Seeing that his charade is coming to an abrupt stop, Jugran tries to run away. It is revealed that Ramesh had given the tipoff to Ravi about Jugran. Jugran succeeds in escaping, while his men try to kill Ravi, Ramesh and Seema. The trio manage to kill their way to Jugran, who himself tries to shoot them. The trio quickly retaliate, killing him on spot.

==Cast==
* Anil Kapoor - Ramesh Verma / Romeo
* Sridevi - Seema Soni / Simmi
* Anupam Kher - Jagmohan Lal Jugran / Manmohan Lal
* Akash Khurana - Dsouza uncle
* Paresh Rawal - Girdharilal
* Jackie Shroff - Ravi Verma Bindu - Yashudhara Devi
* Ajit Vachani - Inspector / SP Pathak
* Dalip Tahil - Mr. Verma (Customs Officer)
* Johnny Lever - Sub Insp. Rang Birangi Lal Chauhan
* Anjan Srivastav - Mr. Narang
* Lalit Tiwari
* Razak Khan

==Reception==
Film with high expectations which failed badly at the box office   as film had many gimmicks but no soul and proper story. Critics further criticized poor dialogues and Jackie Shroffs minimal footage in the film along with not so interesting music.

==Music==
Music was performed by Laxmikant-Pyarelal along with the lyrics being penned by Javed Akhtar.

# Roop ki rani Choron ka raja (Rap) - Bali Bhrambhatt
# Romeo naam mera,chori hain kaam mera - Vinod Rathod
# Main hoon roop ki rani - Kavita Krishnamurthy
# Parda utha -  Amit Kumar  and Kavita Krishnamurthy
# Chai mein chini -  Amit Kumar and Kavita Krishnamurthy
# Jaane wale zara rukja -  Kavita Krishnamurthy
# Tu roop ki rani tu choron ka raja - Amit Kumar  and Kavita Krishnamurthy

==References==
 

==External links==
*  
*   at Bollywood Hungama

 
 
 
 
 