Titli (2014 film)
 
 
:Not to be confused with the 2002 Bengali film Titli (2002 film)|Titli
{{Infobox film
| name           = Titli
| image          = Cannes_Titli_Film_Poster.jpg
| caption        = Film Poster
| director       = Kanu Behl
| producer       = Aditya Chopra Dibakar Banerjee
| writer         = Sharat Katariya   Kanu Behl
| starring       = Ranvir Shorey Amit Sial Shashank Arora Lalit Behl Shivani Raghuvanshi
|cinematography= Siddharth Diwan
| editing        = Namrata Rao
| studio         = Yash Raj Films & Dibakar Banerjee Productions Pvt. Ltd.
| released       =  
| country        = India
| runtime        = 124 minutes
| distributor    = Westend Films
| language       = Hindi
}}
Titli (English: Butterfly)  is a 2014 Bollywood drama film written and directed by Kanu Behl, co-produced by Dibakar Banerjee Productions Pvt. Ltd and Aditya Chopra under the banner of Yash Raj Films. 
   It features actors Ranvir Shorey, Amit Sial, Shashank Arora, Lalit Behl and Shivani Raghuvanshi in the lead roles. 

The International sales partner is Westend Films. In Titli, Behl captures the volatility of a society where violence lies uneasily just below the surface. The film is slated for commercial release in India around mid-2015.
The directorial debut film was screened in the Un Certain Regard section of the 2014 Cannes Film Festival,   

==Plot==
In the badlands of Delhis dystopic underbelly, Titli, the youngest member of a violent car-jacking brotherhood plots a desperate bid to escape the family business.
His struggle to do so is countered at each stage by his indignant brothers, who finally try marrying him off to settle him. Titli, finds an unlikely ally in his new wife, caught though she is in her own web of warped reality and dysfunctional dreams. They form a strange, beneficial partnership, only to confront their inability to escape the bindings of their family roots.  But, is escape the same as freedom? 

==Cast==
* Shashank Arora as Titli 
* Ranvir Shorey as Vikram 
* Amit Sial as Pradeep  
* Lalit Behl as Daddy  
* Prashant Singh as Prince 
* Shivani Raghuvanshi as Neelu

==Production==

===Development===
Behl, who co-wrote and also assisted director Dibakar Banerjee in Love Sex aur Dhokha (LSD) (2010), started writing Titli as LSD neared completion. In 2011, the news report of a car-jacker gang in Delhi led by a local goon, Joginder Joga, inspired him to start working on the story of a thriller. However, as he developed the script, other themes started joining in, from his personal experiences growing up in the city.    Though he denied it being autobiographical, he mentioned in an interview that the idea of intra-family conflict was derived from his own clashes with his father as a rebellious teenager. He eventually co-wrote the script with Sharat Katariya, and it covered themes of patriarchy, family dysfunction, gender-based violence and oppression, and "a desire for freedom". Through the protagonist, the film also explores the circular nature of life – "how we often end up becoming exactly the person we are trying to run away from."      
 NFDC Film Bazaars Screenwriters Lab and won the Post-Production Award at Film Bazaars Work-In-Progress Lab in 2013.  Here it also won an award for Best Work-In-Progress Lab Project and was selected for Film Bazaar Recommends, where the 2014 Cannes Film Festival selection committee first saw the film. 

===Filming===
For the lead roles, relative newcomers Shashank Arora and Shivani Raghuvanshi were selected, as the director didnt want scenes "acted-out". Actors Amit Sial and Ranvir Shorey were chosen to play the role of two elder brothers to Titlis character. Next, he decided to cast his own father Lalit Behl, who is a Delhi-based director and actor, for the role of the patriarch of the family, considering the film itself was based on his early life experiences. 

The film was shot across various locations in Delhi.  During filming, he allowed the actors to explore the scenes and improvise as no scripts were brought to the set. The production team redesigned a house to give a claustrophobic feel to the family home, where much of the filming was done, to provide a contrast from the expansive real world outside, which the protagonist is trying to escape into. For this purpose, rooms were made smaller, the entrance was made labyrinthine, and even the natural light was reduced in the rooms, so that the tube light haze could add to the effect. 

By early May 2014, the movies post-production was completed, ahead of its Cannes premiere due in the same month. 

==International Film Festivals attended==

* 2014 Cannes Film Festival 
* 5th Beijing International Film Festival 
* 13th Indian Film Festival of Los Angeles 
* Melbourne International Film Festival 
* Rio de Janeiro International
* International Film Festival of Colombo
* Zurich Film Festival 
* Filmfest Hamburg 
* Festival international du film indépendant de Bordeaux (FIFIB)
* BFI London Film Festival 
* Chicago International Film Festival 
* Philadelphia Film Festival
* Seattle South Asian Film Festival  
* Hawaii International Film Festival
* San Francisco International South Asian Film Festival (SFISAFF) by 3rd-i Films
* AFI Fest, Los Angeles
* South Asian International Film Festival (SAIFF), New York  
* Black Movie Festival, Geneva
* International Film Festival Rotterdam, Netherlands  
* Gothenburg Film Festival, Sweden  
* Festival du film dAsie du Sud Transgressif (FFAST), Paris
* Gijón International Film Festival, Spain 

==Awards==
* Nominated for Caméra dOr at 2014 Cannes Film Festival  
* Critics Prize at FIFIB, Bordeaux
* Best Film at Seattle South Asian Film Festival  
* NETPAC Award at Hawaii International Film Festival
* Best Film at SAIFF, New York  
* Best International Film at Malatya International, Turkey  
* Best Actress and Best Film, Gijón International Film Festival, Spain 
* Audience Award at Festival du Film dAsie du Sud Transgressif (FFAST), Paris 

==See also==
* Variety: http://variety.com/2014/film/festivals/cannes-film-review-titli-1201198314/
* Screen Daily: http://www.screendaily.com/reviews/the-latest/titli/5072243.article
* The Hollywood Reporter: http://www.hollywoodreporter.com/review/titli-cannes-review-705883
* Fourth Reel Film: http://fourthreefilm.com/2014/08/titli/

==References==
 

==External links==
*   
*  
*  
*  

 

 
 
 
 
 
 
 