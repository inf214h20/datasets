The Garden of Sinners: Overlooking View
{{Infobox film name           = The Garden of Sinners: Overlooking View director       = Ei Aoki producer       = {{Plainlist|
*Atsuhiro Iwakami
*Hikaru Kondo
}} screenplay     = Masaki Hiramatsu story          = Kinoko Nasu based on       =   starring       = {{Plainlist|
*Maaya Sakamoto
*Kenichi Suzumura
*Takako Honda
*Rie Tanaka
}} music          = {{Plainlist|
*Yuki Kajiura
*Kalafina
}} cinematography = {{Plainlist|
*Seiji Matsuda
*Yuichi Terao
}} studio         = ufotable distributor    = Aniplex released       =   runtime        = 50 minutes country        = Japan language  Japanese
}}
  is a 2007 Japanese   (2007).

==Plot==
Mikiya Kokuto brings over some ice cream while visiting Shiki, but Shiki is not happy to see him and says that she does not like ice cream so she decides not to eat it. Afterwards, Shiki and Toko discuss the recent spate of unusual suicide incidents where several high school girls fell to their death, but left no suicide note and had apparently no reason to commit suicide. Toko concludes that the girls did not intend to die. At night, Shiki visits the deserted part of town containing the Fujo buildings which have now fallen into disrepair and are about to be demolished, and while there stumbles upon the body of the newest victim. She also notices several ghostly figures floating above the Fujo building.

The next day, Shiki discusses the building with Toko, as she can feel something abnormal about it. Toko suggests that the figures are records and memories of the victims who have died so far but have for some reason not dissipated into the world. In the afternoon, Shiki is walking outside when another victim falls to her death. Deciding to take action, Shiki visits the Fujo building at sunset and battles a mysterious apparition that manages to take control of Shikis left arm and tries to choke her to death with it. Using her mystic eyes, Shiki destroys her arm which is revealed to be a puppet arm that Toko created for her. While the arm is being repaired, Shiki eats the ice cream that Kokuto bought using her remaining arm. The next day, Shiki takes back the repaired and improved arm, returns to the Fujo building and destroys all the floating figures and the apparition, who attempts to use her power of suggestion to make Shiki fall. It has no effect on her because she never had interest in such things.

Afterwards, Kirie Fujo awakes in her physical body on her hospital bed after being killed by Shiki in her spiritual body. Toko walks in and asks why she did it, Fujo replies that she didnt intend to make them fall, she just wanted to make friends with them, and so called to them, as she was stuck in the hospital with an incurable condition ever since she could remember. After Toko leaves, Fujo decides that the moment of her death when she was killed by Shiki was the most that she has ever felt alive, and wanted to relive that feeling. She wheels herself to the roof of the Fujo building, where she commits suicide by falling from the roof. Kokuto wakes up, feeling as if he had slept for a very long period of time, and Shiki demands that he stay over at her house to finish the ice cream he bought. In the Epilogue, Toko is walking near the Fujo building with Azaka Kokuto, where Fujos corpse has been found. Toko comments that she just probably could not fly today.   

==Cast==
 
*Maaya Sakamoto as  
*Kenichi Suzumura as  
*Takako Honda as  
*Rie Tanaka as  

==References==
 

==External links==
*   
*   
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 