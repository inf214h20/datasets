Troublesome Night 6
 
 
{{Infobox film
| name = Troublesome Night 6
| image =
| caption =
| film name = {{Film name| traditional = 陰陽路之兇周刊
| simplified = 阴阳路之凶周刊
| pinyin = Yīn Yáng Lù Zhī Xīong Zhōu Kān
| jyutping = Jam1 Joeng4 Lou6 Zi1 Hung1 Zau1 Hon1}}
| director = Herman Yau
| producer = Nam Yin
| writer = Nam Yin Chang Kwok-tse
| starring = 
| music = Mak Jan-hung
| cinematography = Joe Chan
| editing = Chan Kei-hop
| studio = Sheen Melody Ltd. Nam Yin Production Co., Ltd.
| distributor =
| released =  
| runtime = 110 minutes
| country = Hong Kong
| language = Cantonese
| budget =
| gross =
}}
Troublesome Night 6 is a 1999 Hong Kong horror comedy film produced by Nam Yin and directed by Herman Yau. It is the sixth of the 19 films in the Troublesome Night film series.

==Plot==
Four reporters from a tabloid magazine stalk a model for their gossip column. The model had just broken up with her rich boyfriend and she commits suicide to escape pressure from the media. The reporters relentlessly post photos of the suicide aftermath in their magazines. Years later, they die violent deaths after meeting the angry ghost of the model. Two police officers are assigned to investigate the case and they encounter paranormal events.

==Cast==
* Louis Koo as Inspector Wong / Chak
* Gigi Lai as Kwok Siu-Heung
* Simon Lui as Chung Amanda Lee as Long Hair
* Wayne Lai as Mr Lai
* Belinda Hamnett as Mrs Lin
* Frankie Ng as Bob Wu Chi-ming
* Peter Ngor as Alan Tam
* Law Lan as Heungs mother
* Lam Yi-tung as Fan
* Man Sai as Vore
* Nnadia Chan as Lin (Kwok Siu-Heungs sister)
* Ronald Wong as Bar waiter
* Fung Hong-ling as Lin Tianzheng
* Raymond Leung as Officer Lau
* Sunny Luk as Doctor
* Herman Yau as Camera man #1
* Ray Pang as Bouncer #1
* Tam Kon-chung as Bouncer #2
* Lok Wai-kuen as Reporter #1
* Macy Yik as Reporter #3
* Lam Wai-lun as Reporter #4
* Fanny Lee as Reporter #5
* Choi Chi-fung as Reporter #6
* Ho Wo-hing as Reporter #7
* Chu Wing-kin as Reporter
* Alan Chan as Reporter Tak
* Chan Kai-tong as Zhengs driver
* Lee Pui-yu as Mr Lais receptionist
* Donna Ng as Dona Lin
* Ka Yum-lung as Professional photo editor
* Alexs Chan as Car cleaner
* Terry Lee as Policeman
* Tang Chuk-shun as Camera man #2
* Chan Yun-ping
* Kong Foo-keung
* Lau Chin-ying
* Yip Kin-kwok
* Wong Kai-yuen

==External links==
*  
*  

 

 
 
 
 
 
 
 
 


 
 