The Case of Becky (1915 film)
{{Infobox film
| name           = The Case of Becky
| image          =
| alt            = 
| caption        = 
| director       = Frank Reicher
| producer       = David Belasco Jesse L. Lasky Margaret Turnbull
| starring       = Blanche Sweet Theodore Roberts James Neill Carlyle Blackwell Jane Wolfe Gertrude Kellar
| music          = 
| cinematography = Walter Stradling
| editing        = 
| studio         = Jesse L. Lasky Feature Play Company
| distributor    = Paramount Pictures
| released       =  
| runtime        = 
| country        = United States
| language       = English
| budget         = 
| gross          =
}}
 drama silent Margaret Turnbull. The film stars Blanche Sweet, Theodore Roberts, James Neill, Carlyle Blackwell, Jane Wolfe and Gertrude Kellar. The film was released on September 13, 1915, by Paramount Pictures.  

==Plot==
 

== Cast == 	
*Blanche Sweet as Dorothy / Becky
*Theodore Roberts as Balzamo
*James Neill as Dr. Emerson
*Carlyle Blackwell as Dr. John Arnold
*Jane Wolfe as Carrie, Balzamos assistant 
*Gertrude Kellar as Miss Emerson, Dr. Emersons sister 

== References ==
 

== External links ==
*  
 
 
 
 
 
 
 
 