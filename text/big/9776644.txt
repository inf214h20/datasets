The Strangers (2008 film)
 
 
{{Infobox film
| name = The Strangers
| image = Strangersposter.jpg
| alt = 
| caption = Theatrical release poster
| director = Bryan Bertino
| producer = Doug Davison Roy Lee Nathan Kahane
| writer = Bryan Bertino
| starring = Liv Tyler Scott Speedman Glenn Howerton
| music = tomandandy
| cinematography = Peter Sova
| editing = Kevin Greutert
| studio = Vertigo Entertainment Mandate Pictures Intrepid Pictures
| distributor = Rogue Pictures
| released=  
| runtime = 85 minutes  
| country = United States
| language = English
| budget = $9 million 
| gross = $82.4 million 
}}
The Strangers  is a 2008 American horror film written and directed by Bryan Bertino and starring Liv Tyler and Scott Speedman as a young couple who are terrorized by three masked assailants, who break into the remote summer home and destroy all means of escape or calling for help by destroying their car, cell phones, and phone line.
 Manson killings (though website iO9 submits there is lack of legitimacy towards claiming the events are inspired as true).    Critical reaction to the film was mixed.  It holds a rating of 45% on Rotten Tomatoes, based on 151 reviews.    Metacritic reported an average score of 47 out of 100, based on 27 reviews. 

==Plot== 911 call of a boy screaming that he and his friend found two bodies lying in blood in an old vacation house.

Kristen McKay and James Hoyt arrive at a remote house owned by Jamess parents after attending a friends wedding reception. James had proposed to Kristen there, but she refused, saying she wasnt ready to make this big step. James calls his friend Mike to come pick him up in the morning. Shortly after 4 a.m., a young blonde woman, whose face is obscured by low lighting, knocks on the front door asking for Tamara. When told by James and Kristen she is at the wrong house, she asks if theyre sure, then leaves.

James leaves to get Kristen cigarettes. Kristen hears another knock on the door; the same woman has returned to ask for Tamara again. Kristen refuses to open up. She hears strange noises, then her cellphone goes missing. As her fear grows, Kristen hears a noise from the back door, grabs a large kitchen knife and opens the curtains to find a man wearing a sack mask staring back at her. Panicked, she hides in the bedroom until James returns. She explains what just transpired and James investigates. He finds his car has been ransacked and the blonde woman, now wearing a doll mask, watching him from afar. His phone has had its battery stolen and he realizes the house has been breached.

The couple attempt to leave in Jamess car, but a third stranger wearing a pin-up girls mask drives a truck into his car, totaling it and forcing them to flee. Back in the house, Kristen and James find a shotgun. Mike arrives early and is accidentally shot by James, mistaking him for an intruder. A devastated James remembers an old radio transmitter in the backyard shed. He leaves the house and encounters and attempts to shoot Pin-Up Girl, but is ambushed by the masked man.

Kristen hears a shot and decides to make a run for the shed. There, she finds the radio and tries to contact someone for help. Pin-Up Girl appears and smashes the radio. Kristen rushes back to the house, where she is confronted by Dollface, who is now armed with a knife. The masked man arrives with the captured James and the gun and incapacitates Kristen.

James and Kristen are tied to chairs in the living room. The strangers reveal themselves and take turns slowly stabbing them before finally leaving. Two young Mormon boys who are distributing religious tracts come to the house, finding the totaled car and the door open. The boys head into the house and discover the bodies. Kristen, who is still alive, wakes up and screams.

==Cast==
* Liv Tyler as Kristen McKay
* Scott Speedman as James Hoyt
* Gemma Ward as Dollface
* Kip Weeks as Man in the Mask
* Glenn Howerton as Mike
* Laura Margolis as Pin-up girl
* Alex Fisher and Peter Clayton-Luce as Mormon boys

==Production==
===Screenplay and inspiration===
 
Director Bryan Bertino also wrote the films script, which was originally titled The Faces.   Bertino took a particular interest in the horror genre, noting how one can connect to an audience by scaring them. He also stated that he was significantly inspired by thriller films of the 1970s while writing the film. 

According to production notes,  the film was inspired by true events from director Bryan Bertinos childhood: a stranger came to his home asking for someone who was not there, and Bertino later found out that empty houses in the neighborhood had been broken into that night:   

 
 Helter Skelter; Keddie Cabin Sierra Nevada.     

The 2006 French film entitled THEM is very similar in plot.

===Casting===
When casting the two leads in the film, Bertino sought Liv Tyler for the part of Kristen; Tyler, who had not worked for several years due to the birth of her son, read the script out of a stack of others she had been offered; "It spoke to me", she said.  "I especially liked Bryans way of saying a lot, but not saying everything. Often in movies, its all spelled out for you, and the dialogue is very explanatory. But Bryan doesnt write like that; he writes how normal people communicate—with questions lingering. I knew it would be interesting to act that." 

Canadian actor Scott Speedman was cast as James, Kristens longtime boyfriend. Speedman was also riveted by the script: "The audience actually gets time to breathe with the characters before things get scary as hell. That got me interested from the first pages", he said. 

In casting the three masked intruders, Bertino chose Australian fashion model Gemma Ward for the part of Dollface, feeling she had the exact "look" he had imagined. In preparing for the role, which was her first major acting part, Ward read Helter Skelter for inspiration. Kip Weeks was then cast as the looming Man in the Mask, and television actress Laura Margolis, who found the script to be a real "page turner", was cast in the part of Pin-Up Girl.    

===Filming=== set crew.  Though the film takes place in 2005, the house itself was deliberately constructed with an architecture reminiscent of 1970s ranch houses and dressed in furnishings applicable to the era.  The property was located on the outskirts of Timmonsville, South Carolina. During production, it was reported that star Liv Tyler came down with tonsillitis due to screaming so much.    Despite some weather complications, the film was largely shot in chronological order. 

===Marketing and promotion=== teaser trailer;  this trailer was released on the internet several weeks later, and can be found on YouTube.  It was not until March 2008 that a full-length trailer for the film was released, which can be found on Apple Inc.|Apples QuickTime trailer gallery.  The trailer originally began running in theaters attached to Rogue Pictures sci-fi film Doomsday (2008 film)|Doomsday (2008) in March 2008, and television advertisements began airing on networks in early-mid April 2008 to promote the films May release. Two one-sheet posters for the film were released in August 2007, one showing the three masked Strangers,  and the other displaying a wounded Liv Tyler.  In April 2008, roughly two months before the films official theatrical debut, the final, official one-sheet for the film was released,  featuring Liv Tyler standing in a darkened kitchen with a masked man looming behind her in the shadows.

==Release==
The producers originally planned for a summer release in 2007, which was eventually postponed to November 2007. It was pushed back yet one more time, and officially opened in the United States and Canada on May 30, 2008; in its opening weekend, the film grossed $20,997,985 in 2,467 theaters, ranking #3 at the box office and averaging $8,514 per theater.    As of June 23, 2008 the film has grossed $52,597,610 in the U.S. alone exceeding industry estimates,  and is considered a large box office success considering the production budget was a mere $9 million. The film opened in the United Kingdom later that summer on August 29, 2008, and as of September 21, 2008, had grossed £4,025,916.  The overall box office return was highly successful for a horror film earning an outstanding $82.3 million at the box office worldwide. The movie received a rating of R from the MPAA.

===Critical reception===
The film received mixed reviews from critics. It holds a rating of 45% on Rotten Tomatoes, based on 151 reviews.    Metacritic reported an average score of 47 out of 100, based on 27 reviews.  Among the positive reviews, Jeannette Catsoulis of The New York Times said The Strangers is "suspenseful," "highly effective," and "smartly maintain  its commitment to tingling creepiness over bludgeoning horror."  Michael Rechtshaffen of The Hollywood Reporter called the film a "creepily atmospheric psychological thriller with a death grip on the psychological aspect."  James Berardinelli of ReelViews gave the film 3 out of 4 stars, saying that, "This is one of those rare horror movies that concentrates on suspense and terror rather than on gore and a high body count."  Scott Tobias of The Onions The A.V. Club|A.V. Club said that "as an exercise in controlled mayhem, horror movies dont get much scarier." 

Among the moderate to negative reviews, Roger Ebert of the Chicago Sun-Times gave the film one-and-a-half stars out of four and said, "The movie deserves more stars for its bottom-line craft, but all the craft in the world cant redeem its story."  Elizabeth Weitzman of the New York Daily News said that "Bertino does an excellent job building dread" and that the film is "more frightening than the graphic torture scenes in movies like Hostel (2005 film)|Hostel and Saw (2004 film)|Saw," but criticized the "undeveloped protagonists" for being "colossally stupid and frustratingly passive."  Stephen Hunter of the Washington Post panned the film, calling it "a fraud from start to finish."  Mick LaSalle of the San Francisco Chronicle, said the film "uses cinema to ends that are objectionable and vile," but admitted that "it does it well, with more than usual skill." 

Additional positive feedback for the film came from Joblo.com reviewer Berge Garabedian, who praised director Bertino for "building the tension nicely, with lots of silences, creepy voices, jump scares, use of songs and a sharp eye behind the camera, as well as plenty of Steadicam give it all more of a voyeuristic feel."  Empire Magazine remarked on the films retro-style, saying, "Like much recent horror, from the homages of the Grindhouse gang through flat multiplex remakes of drive-in classics, The Strangers looks to the 70s.", and ultimately branded the film as "an effective, scary emotional work-out."  Slant Magazines Nick Schager listed The Strangers as the 9th best film of 2008.  Also, the film was ranked #13 on "Bravo!|Bravos 13 Scarier Movie Moments" television piece. 

===Home media===
The Strangers was released on DVD and Blu-ray Disc|Blu-ray in the United States on October 21, 2008. Both the Blu-ray and DVD feature rated and unrated versions of the film, with the unrated edition running approximately two minutes longer. Bonus materials include two deleted scenes and a making-of featurette. The DVD was released in the UK on December 26, 2008. The film was available on Universal VOD (Video on Demand) from November 19, 2008 through March 31, 2009. 

==Soundtrack==
 
A soundtrack, consisting of 19 scores composed by score producer tomandandy, was released on May 27, 2008 and was distributed by Lakeshore Records.

The album was received with generally positive reviews by critics. "Its a creepy score for what appears to be a movie that will make you jump as well as make sure that the doors are locked at night," writes reviewer Jeff Swindoll.  "This is an impressive score and adds a tremendous chill-factor to the film," says Zach Freeman, grading it with an A.

==Sequel==
  and directed by Laurent Briet.     Shock Till You Drop reported that Realitivity Media put The Strangers: Part II on hold because they found that the movie might not be in their interest, even though   will return as Kristen McKay while the original three masked villains are also set to return, however, in an interview Tyler had announced that she would only have a minor role.

Tyler claimed that Part II would be released in 2014    however this didnt occur. 

In January 2015 it was stated the sequel was back on track. 

==See also==
* List of films featuring home invasions

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 