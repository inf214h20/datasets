Monica (film)

{{Infobox film
| name           = Monica
| image          = Monica_Poster.jpg
| image_size     =
| caption        =
| director       = Sushen Bhatnagar
| producer       = Kush Bhargava
| writer         = Sushen Bhatnagar
| narrator       =
| starring       = Divya Dutta Ashutosh Rana Rajit Kapur
| music          = Raju Rao
| cinematography = Chandan Goswami
| editing        = Santosh Kumar Aadesh Verma
| distributor    =
| released       = 
| runtime        =
| country        = India Hindi
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}

Monica is a 2011 Bollywood movie with Divya Dutta, Ashutosh Rana and Rajit Kapur in the lead roles and produced by ghazal singer Anup Jalota and Kush Bhargava.     It is directed by Sushen Bhatnagar and is inspired from real life incidents such as the Shivani Bhatnagar murder case and the 2G spectrum scam.      

==Plot==
The film traces the story of Monica Jaitley (Divya Dutta), who is an ambitious woman who wants to rise to the top at any cost. She has had a tormented life as a child as she was sexually abused in her chiildhood. Leaving her horrid past behind, she falls in love with journalist Raj Jaitley (Rajit Kapoor). They do get married; however have an unhappy married life. She becomes a journalist herself and this creates a rift between them. The story traces her rise to the top and the compromises she ends up making and its impact on her personal life.        

==Cast==
 
* Divya Dutta ... Monica Jaitley (Journalist)
* Rajit Kapur ... Raj Jaitley (Monicas husband)
* Ashutosh Rana ... Chandrakant Pandit (Politician)
* Kitu Gidwani ... Pamela Grewal (Industrialist) Yashpal Sharma ... Public Prosecutor Mathur 
* Tinnu Anand ... Assem Ray
* Kunika ... Judge
* Yatin Karyekar ... M.J.
* Mithilesh Chaturvedi ... Defence Lawyer
* Saurabh Dubey ... Shrikant Vohra
* Dadhey Pandey ... Sandeep Mishra (Broker)
* Anil Rastogi ... Monicas Father  
* Veda Rakesh ... Monicas Mother  
* Padam Singh ... Gopinath Pandey  
* Zarine Viccajee ... House Owner - Nainita

==Reception==
Mahesh Bhatt tweeted, "Monica is a brave film, and Divya Dutta bears her soul on the screen like Shabana Azmi once did in the movie Arth".  

Koimoi.com gave it a half star and a very unflattering review - "On the whole, Monica is a dull show and will not be able to prove itself at the ticket windows. Resounding flop!"  

Glamsham.com gave it 1.5/5 and their final take on the movie was "The hotchpotch film is a disappointment. MONICA, no my darling!"  

Indian Express review - "Monica has the intention, and a couple of effective performances, but doesnt keep up with its execution."  

Hindustan Times - Mayank Shekhar gave it 1/5 stars  

The Times of India gave it 3/5 stars and a decent review - "Dont be mislead by the title and dont be foxed by the low key publicity. This ones truly a surprise. Check it out."  

==References==
 

==External links==
*  

 
 
 
 
 