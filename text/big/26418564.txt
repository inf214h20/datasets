Megasandesam
 
{{Infobox film
| name           = Megasandesam
| image          = Megasandesam.jpg
| director       = Rajasenan
| producer       = K. Radakrishnan
| writer         = Satheesh Poduval   M. Sindhuraj
| narrator       = Rajasenan Napoleon
| music          = M. G. Radhakrishnan   Ouseppachan (Background score)
| cinematography = Venu (cinematographer)
| editing        = A. Sreekar Prasad 
| released       = April 2001
| runtime        = 170 mins
| country        = India
| language       = Malayalam
}}
Megasandesam is a 2001 Malayalam film by Rajasenan starring Suresh Gopi and Samyuktha Varma .This film was a Sleeper Hit

==Plot==
While returning to Ooty, Balagopal learns that one of his students, Rosy Samuel, died in a scooter accident, and that she had loved him. Rosys ghost visits him and, although friendly at first, she becomes jealous of Balagopals girlfriend Anjali and transforms into an evil spirit baying for  Anjalis blood. For the rest of the movie, Balagopal, with the help of Fr. Rosario, tries to put Rosy into her grave.
Film was a blockbuster and ran 150 days in theatre.

==Cast==
*Suresh Gopi ...  Balagopal 
*Samyuktha Varma ...  Anjali Varma (voice dubbed by sreeja)
*Rajshri Nair ...  Rosy Samuel (as Rajasree Nair) (voice dubbed by Bhagyalekshmi) Napoleon ...  Fr. Rosario 
*Oduvil Unnikrishnan ...  Easwara Varma 
*Harisree Ashokan ...  Kuttikrishnan 
*Indrans ...  Thommy 
*Narendra Prasad ...  Samuel 
*Urmila Unni ...  Mariya  Abhirami ...  Kavitha 
*Mallika Sukumaran ...  College Professor 
*K. T. S. Padannayil ...  Chandran Nair

==External links==
*  

 
 
 
 
 
 

 
 