A Bedtime Story
{{Infobox film
| name           = A Bedtime Story
| image          = A Bedtime Stroy movie Poster.jpg
| caption        = Theatrical release poster
| director       = Norman Taurog
| producer       = Ernest Cohen
| writer         = Benjamin Glazer Roy Horniman (novel Bellamy The Magnificent) Nunnally Johnson Waldemar Young (screenplay)
| starring       = Maurice Chevalier Helen Twelvetrees Edward Everett Horton
| music          =
| cinematography = Charles Lang
| editing        = Otho Lovering
| distributor    = Paramount Pictures
| released       =  
| runtime        = 87 minutes
| country        = United States
| language       = English / French
| budget         =
}}

A Bedtime Story is a 1933 romantic comedy film starring Maurice Chevalier.

==Plot==
Chevalier plays a Parisian playboy who finds himself obliged to care for an abandoned baby. The film was directed by Norman Taurog and also stars Edward Everett Horton, Helen Twelvetrees, and Baby LeRoy (in his film debut, as the baby).

==Production problem==
The film was notable for the performance of Baby LeRoy, a one-year-old who had been selected from an orphanage by Chevalier and Taurog for his charming appeal. When certain scenes needed to be re-shot, they found that the baby had grown two front teeth, even though the later scenes would be showing the bare gums. There was no way round this. 

==Cast==
*Maurice Chevalier  as Monsieur Rene
*Helen Twelvetrees  as Sally
*Edward Everett Horton  as Victor Dubois
*Adrienne Ames  as Paulette
*Baby LeRoy  as Monsieur "Baby"
*Earle Foxe  as Max de lEnclos
*   as Mademoiselle Gabrielle
*   as Suzanne Dubois
*Gertrude Michael  as Louise
*Ernest Wood  as Robert
*   as General Louses father
*Henry Kolker  as Agent de Police
*George MacQuarrie  as Henry Joudain
*Paul Panzer  as Concierge
*Frank Reicher  as Aristide

==References==
 

== External links ==
*  
*  

 

 

 
 
 
 
 
 
 
 
 
 


 
 