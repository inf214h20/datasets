The Crooked Eye
 
The Crooked Eye is a 20-minute drama film short adapted, directed and edited by D. C. Douglas.  It is based on the short story of the same name by Betty Malicoat. In 2009 the film won for Best Animated Short at the Red Rock Film Festival in Utah. 

==Synopsis==
The Crooked Eye follows a quiet woman through her daily drudgery while persistent memories of a recently unraveled marriage dreamily connect the guilty moments that made her world so unreal and unreliable.

==Cast==
Linda Hunt ... Sharons Narrator
 Fay Masterson ... Sharon
  Katherine Boecher ... Rosemary
  D. C. Douglas ... Frank
  Joe Duer ... Roy
  Clement Blake ... Wayne
  Monnae Michaell ... Sharons Supervisor
  Ari Barak ... Sharons Doctor
  Robin Daléa ... Sharons Tough Co-Worker
  Karen McClain ... Sharons Loud Co-Worker

==Awards==
*Won Grand Jury Prize at Red Rock Film Festival (2009)
*Won Best Screenplay in a HD Short at HDFest (2009)
*Won the STIFFY Award at Seattle True Independent Film Festival (2009)

==Trivia==
The movie was filmed entirely on green screen.  All the environments were animated in post.

==References==
 

==External links==
 

 
 
 