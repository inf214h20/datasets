The Phone Call (2013 film)
 
{{Infobox film
| name           = The Phone Call
| image          = 
| caption        = 
| director       = Mat Kirkby
| producer       = 
| writer         = Mat Kirkby James Lucas
| narrator       = 
| starring       = Sally Hawkins
| music          = 
| cinematography =
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 20 minutes
| country        = United Kingdom
| language       = English
| budget         = 
}}
 short drama film directed by Mat Kirkby. It won the Academy Award for Best Live Action Short Film at the 87th Academy Awards.   

The film stars Sally Hawkins as Heather, a crisis hotline counsellor trying to dissuade Stanley (Jim Broadbent), a distraught caller, from a suicide attempt following the death of his wife. 

==Cast==
* Sally Hawkins as Heather
* Jim Broadbent as Stanley
* Edward Hogg as Daniel
* Prunella Scales as Joan

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 
 