The Quick Gun
{{Infobox film
| name           = The Quick Gun
| image          =
| image_size     =
| caption        =
| director       = Sidney Salkow
| producer       = Grant Whytock
| writer         = Robert E. Kent
| based on       = story by Steve Fisher
| narrator       =
| starring       = Audie Murphy Merry Anders
| music          =
| cinematography = Lester Shorr
| editing        = Grant Whytock
| studio         = Admiral Pictures
| distributor    = Columbia Pictures
| released       =  
| runtime        = 91 mins
| country        = United States
| language       = English
| budget         = $400,000 Don Graham, No Name on the Bullet: The Biography of Audie Murphy, Penguin, 1989 p 296 
| gross          =
| website        =
}} Western film starring Audie Murphy.  It was one of a series of low-budget films he made in the 1960s. 

==Plot==
Clint Cooper returns to his home town, which he had left in disgrace, to claim his fathers ranch. On the way he runs into his old gang, led by Spangler, who plan on robbing the town. Clint arrives back in town to find most of the men have left on a cattle drive. He tries to warn them of the impending robbery. He agrees to help the sheriff, an old friend of his called Scotty, defend the town against the gang. Clint also starts up a relationship with his old girlfriend, Helen.

==Cast==
*Audie Murphy as Clint Cooper
*Merry Anders as Helen Reed
*James Best as Scotty Grant
*Ted de Corsia as Spangler
*Walter Sande as Tom Morrison
*Rex Holman as Rick Morrison Charles Meredith as Reverend Staley
*Frank Ferguson as Dan Evans
*Mort Mills as Cagle
*Gregg Palmer as Donovan
*Frank Gerstle as George Keely Stephen Roberts as Dr. Stevens

==Production==
Murphy was paid $37,500 for his performance. 

==References==
 

==External links==
*  
*  

 
 
 
 
 


 