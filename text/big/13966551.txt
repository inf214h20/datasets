Jeremy (film)
{{Infobox film
| name           = Jeremy
| image          = Jeremy1973.jpg
| image_size     = 
| caption        = Theatrical poster
| director       = Arthur Barron
| producer       = Elliott Kastner George Pappas
| writer         = Arthur Barron
| narrator       = 
| starring       = Robby Benson Glynnis OConnor
| music          = 
| cinematography = Paul Goldsmith
| editing        = Zina Voynow
| distributor    = United Artists
| released       = May 1973 (Cannes Film Festival)
| runtime        = 90 minutes
| country        = United States
| language       = English
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
Jeremy is a 1973 film written and directed by Arthur Barron and starring Robby Benson and Glynnis OConnor.    They play two high school students, who share a tentative monthlong romance.      

Benson was nominated for a Golden Globe Award for his performance in the film, in 1974.    It also won the prize for Best First Work in the 1973 Cannes Film Festival.    The two stars dated each other in real life, and appeared together again in Ode to Billy Joe (film)|Ode To Billy Joe.   

==Plot==
The film begins with Jeremy (Benson) engaging in his normal activities, including playing basketball and playing cello with his teacher and in school. He meets and has a conversation with Susan (OConnor), who has recently transferred to the Manhattan school from Detroit. as she rehearses ballet moves. Following this encounter, Jeremy pursues Susan, but finds himself unable to speak to her, and only observes her from a distance. However, after performing in a school recital, Susan speaks to Jeremy herself, and they agree to go on a date.

Although they are both shy, the date goes well. Their nervous courtship continues, and they express their love for each other. Returning home after spending the day with Jeremy, Susan is told by her father that they will be returning to Detroit almost immediately. Jeremy sees Susan off at the airport in the final scene.   

==Cast==
* Robby Benson - Jeremy Jones
* Glynnis OConnor - Susan Rollins
* Len Bari - Ralph Manzoni
* Leonardo Cimino - Cello teacher
* Ned Wilson - Susans father
* Chris Bohn - Jeremys father
* Pat Wheel - Grace Jones - Jeremys mother
* Ted Sorel - Music teacher in school
* Bruce Friedman - Shop owner
* Eunice Anderson - Susans aunt

==Soundtrack==
The soundtrack was released in 1973 on LP by United Artists Records (UA-LA145-G). The  soundtrack was composed by Lee Holdridge, but also contained two original songs:  Joseph Brooks.
*"Jeremy" sung by Glynnis OConnor. Written by Lee Holdridge and Dorothea Joyce.

==References==
 

==External links==
* 

 
 
 
 
 
 
 


 