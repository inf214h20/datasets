Clouds (film)
Clouds Don Thompson Will Arntz.

==Synopsis==
Clouds tells the story of a physicist (Michael Patrick Gaffney) trying to come to terms with the cosmos, and ultimately understanding that love must be a part of any complete description of the universe.

==Cast==
* Michael Patrick Gaffney as Robert St. John, the physicist.
* Jennifer Jordan Day as Beatrice, his love interest
* Richard Barrows as Tab
* Rob Nilsson as Frank
* Patricia Ann Rubens as Mrs. Martin

==Reception== New York Times reviewer called Clouds "the dumbest intelligent movie Ive ever seen," 
{{cite news
|url=http://query.nytimes.com/gst/fullpage.html?res=9907E5D61539F93BA3575AC0A9669C8B63
|title=FILM REVIEW; A Physicist Of Big Ideas And Humor
|author=Scott, A.O.
|date=8 September 2000
|work=The New York Times
|accessdate=8 October 2010}}  while Film Threat said
: Though Clouds is not a perfect film, the actors make most of the more stilted dialog work, and even the slowest paced moments feature nicely composed visuals from DP Gary Lindsay. By the time we come to the final message “go and love some more” (effectively appropriated from Harold and Maude) it’s apparent that the film is just a good, solid effort with an intriguing plot, and an impressive directorial debut for Thompson. 
{{cite web
|url=http://www.filmthreat.com/reviews/1023/
|title=Clouds
|work=Film Threat
|author=Sweeney, James
|date=14 August 2000
|accessdate=8 October 2010}} 
Despite these mixed reviews, Clouds was awarded the "Feature Film Award" at the 1999 New York International Independent Film and Video Festival  and the "Premio Nuovo" at the 1999 Brooklyn International Film Festival. 

==References==
 

==External links==
*  
*  

 