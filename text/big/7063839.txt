Angel in My Pocket
{{Infobox film
| name           = Angel in My Pocket
| image          = Movieangelinmypocket.jpg
| caption        = Promotional poster for Angel in My Pocket
| director       = Alan Rafkin
| producer       = Edward Montagne
| eproducer      =
| aproducer      =
| writer         = James Fritzell Everett Greenbaum Henry Jones Gary Collins
| music          = Jerry Keller Lyn Murray Dave Blume
| cinematography = William Margulies
| editing        = Sam E. Waxman
| distributor    = Universal Pictures
| released       = January, 1969
| runtime        = 105 mins
| country        = United States
| awards         = English
| budget         =
| gross          =
| preceded by    =
| followed by    =
}}
 Henry Jones, Gary Collins. The film has never been released to home video in any format.      

==Plot==

The Reverend Samuel D. Whitehead, ex-United States Marine Corps|Marine, bricklayer, and recent seminary graduate, is ecstatic to receive his first "calling," or assignment as Pastor of his own church (building)|church. But the Church of the Redeemer in Wood Falls, Kansas, will prove a challenging assignment and nearly his undoing.   

The trouble begins almost immediately after he drives into town with his family. A  , which the sheriff will not stop. Sam attempts to intervene and succeeds only in getting struck in the face, so he drives on to see the church. There he learns that the church sorely needs major renovation, which has not been done in decades because the two founding families, the Sinclairs and the Greshams, have been running a feud for decades and cannot agree on the simplest decision that would benefit the church (or on anything else, either). Worse yet, Sam delivers his first sermon by preaching against physical violence—only to discover most of the brawlers in attendance, including one who blames him for making him vulnerable to someone elses assault. 
 organ for caretaker repair the superannuated boiler—but unknown to Sam, the two men turn the boiler into a still and start producing raisin jack, a variety of moonshine. Next, he takes his children out of school after seeing the appalling conditions there—which prompts his Bishop to warn him not to interfere in town affairs. Finally, he performs a marriage between a Sinclair and a Gresham—and when the secret gets out at a church social (after "Bubba" spikes the church punch with some of his raisin jack), Sam must physically restrain the heads of the families from brawling in the church fellowship hall, and then send everyone home. Not long afterward, the Bishop informs him that he is removed from his pastorate. 
 Attorney Art Shields, to run for mayor as a write-in candidate, with the election two days away. That leads to a confrontation along the main street among three different political parades, including Arts. Then the churchs old boiler explodes, and the church burns down to its foundations as a result—and the attempt by the fire department to fight the fire turns pathetic when the fire hose springs multiple leaks. When the Sinclairs and the Greshams argue yet again about who was responsible for the faulty equipment, Sam roars at them to "go someplace else, yell your heads off, and let this poor church die in peace!"  

The next day, the Whiteheads are moving out—when Art Shields joyously announces that he is trouncing the opposition in the election and will definitely be the next mayor. Art offers Sam a job with the town, but Sam declines, saying that he needs to find another church. But as he is about to leave town, Will Sinclair and Axel Gresham—reconciled at last, and at the head of a procession of building-material trucks—intercept him, tell him that they intend rebuilding the church, and beg him to stay on. 

==Cast==
*Andy Griffith as The Reverend Samuel D. Whitehead, Pastor of the Church of the Redeemer, Wood Falls, Kansas
*Lee Meriwether as Mary Elizabeth Whitehead, his wife. alcoholic brother-in-law.
*Kay Medford as Racine, his mother-in-law. Henry Jones as Will Sinclair, Mayor of Wood Falls and head of one of the two founding (and feuding) families.
*Edgar Buchanan as Axel Gresham, head of the other founding family and Will Sinclairs principal opponent in the mayoral election. Gary Collins Attorney at law and write-in candidate for mayor.
*Parker Fennelly as Calvin Grey, the caretaker
*Jack Dodson as Norman Gresham
*Elena Verdugo as Lila Sinclair, whose marriage to Norman brings the families to blows yet again.
*Herbie Faye as Mr. Welch   

==Themes==
   Democratic and Republican Parties. That, during the immediate past Federal election, Presidential Candidate George Wallace famously said, "Theres not a dimes worth of difference between the two parties," might or might not be a coincidence. 

There is potentially a bit of borrowing from "Romeo and Juliette" in that the families of the two people wanting to marry, are sworn enemies of each other, with the Gresham and Sinclair families easily being mistaken for the feuding Montague and Capulet families.
 Episcopal Church, however the title given to the senior minister in an Episcopal parish is "Rector (ecclesiastical)#Anglican churches|rector," not "pastor." 

==Background and Production==
This film was one of three originally planned by Universal Pictures to feature Andy Griffith in the wake of his television series success. Griffiths disappointment in this film led to a cancellation of the project. Hence, the other two films were never made.   

==Reception== NBC Television Saturday Night at the Movies program. The film has appeared on television only infrequently after that, and has never been released to home video.

==References==
 

==External links==
* 
* 
* 

 
 
 
 
 
 
 