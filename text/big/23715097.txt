Minnaram
{{Infobox film
| name           = Minnaram
| image          = Minnaram DVD cover.jpg
| image_size     = 
| alt            = 
| caption        = DVD cover
| director       = Priyadarshan
| producer       = R. Mohan
| screenplay     = Priyadarshan
| story = Cherian Kalpakavadi
| narrator       = 
| starring       = Mohanlal  Shobana Thilakan  Sankaradi  K. P. Ummer  Jagathy Sreekumar  Geetha Vijayan Neha Dhupia  Venu Nagavally Lalu Alex
| music          = S.P. Venkatesh
| cinematography = K. V. Anand
| editing        = N. Gopalakrishnan Goodknight Films 
| distributor    = Manorajyam Release (Kerala)
| released       = 16 September 1994
| runtime        = 150 minutes
| country        = India
| language       = Malayalam
| budget         = 2.5 CRORE
| gross          = 13.5 CRORE
| preceded_by    = 
| followed_by    = 
}}

Minnaram (  musical film written and directed by Priyadarshan,  starring Mohanlal,      Shobana, Thilakan, Sankaradi, K. P. Ummer, Jagathy Sreekumar, Geetha Vijayan, Venu Nagavally, and Lalu Alex. 

==Cast==
 
*Mohanlal as Bobby
*Shobana as Neena
*Thilakan   as Retd I.G Mathews
*Lalu Alex as Romy Anju as Teena
*Jagathi Sreekumar as Unnunni
*K. P. Ummer as Chackochan
*Venu Nagavalli as Baby
*Sankaradi as Iyer Augustine as Doctor
*Manian Pillai Raju as Mazhuvan Manikantan/Lassar
*Kuthiravattam Pappu as Tutor
*T. P. Madhavan as Doctor
*Geetha Vijayan as Jaya
*Subhashini as Daisy
*Neha Dhupia as Manju Reena as Reena
*Bobby Kottarakkara as Manuel
*Ravi Menon as Priest
*Nandhu
*Antony Perumbavoor as Man at batmindon court
* Baby Ambili as child artist
*Thara Thomas Moolayil as Child 
 

==Plot==
Bobby (Mohanlal), orphaned at a young age, is brought up as his own son by his uncle, Matthews (Thilakan). He is betrothed to Chackochans daughter, Tina. However, his old flame from college, Nina (Shobhana), turns up with a toddler allegedly fathered by Bobby. He denies this vehemently and goes to great lengths to prove her wrong, whilst keeping this under wraps from his fiancée.

Eventually, having discovered the truth, Chackochan calls off the wedding, publicly humiliating Matthews for the dishonesty. With nothing left to lose, Bobby is prepared to accept Nina as his wife. On accosting her, she divulges the truth about the child - Bobbys cousin had an affair with Ninas sister, who died in childbirth.
 polycythemia rubra vera(PV, PCV) and wanted to ensure her niece would not be orphaned. Bobby races to get the medicine (cyclophosphamide) to treat this acute relapse, but cannot reach her in time to save her.
==Box Office==
The film had super songs and comedies, it was an all time blockbuster of Malayalam film industry

==Music==
The acclaimed soundtrack of this movie was composed by S. P. Venkatesh for which the acclaimed lyrics were penned by Shibu Chakravarthy and Gireesh Puthenchery. All the songs of this movie were instant hits.
{| class="wikitable"
|-
! Song Title !! Singer(s)
|-
| "Chinkarakinnaram" || M. G. Sreekumar, K. S. Chithra
|-
| "Darlings of Mine" || Dr. Kalyan, Anupama
|-
| "Kunjoonjaal" || M. G. Sreekumar, K. S. Chithra
|-
| "Manjakunjikkalulla" || M. G. Sreekumar, Suju
|-
| "Nilaave Maayumo" || M. G. Sreekumar
|-
| "Nilaave Maayumo" || K. S. Chithra
|- Sujatha
|-
| "Thaliraninjoru" || M. G. Sreekumar, K. S. Chithra
|}

==References==
 

==External links==
*  

 

 
 
 
 

 