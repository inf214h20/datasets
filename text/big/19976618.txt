The Ball of Count Orgel
 
{{Infobox film
| name           = Le Bal du comte dOrgel
| image          = 
| image_size     = 
| caption        = 
| director       = Marc Allégret
| producer       = Phillipe Grumbach
| writer         = Marc Allégret
| narrator       = 
| starring       = Jean-Claude Brialy
| music          = Raymond Le Sénéchal
| cinematography = 
| editing        = Victoria Mercanton
| distributor    = Cocinor
| released       = 1 July 1970
| runtime        = 95 minutes
| country        = France French
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
 French film from 1970. It was the last film directed by Marc Allégret, who was also the producer of this film. It was screened at the 1970 Cannes Film Festival, but wasnt entered into the main competition.   

==Plot== book of ball hosted by the Comte dOrgel ( ). 

Set in 1920, the Comte hosts a soirée and dance for the upper echelons of Parisian society. One of the guests is a handsome young man named François de Séryeuse (played by Bruno Garcin), who during the course of the ball falls in love with the Comtes wife, Comtesse Mahé (played by Sylvie Fennec). 
 passion on stage during a performance of The Tempest with François. Mahé continues to dream about him, however she is confined in her marriage.

==Cast==
* Jean-Claude Brialy : Le comte Anne dOrgel ( )
* Sylvie Fennec : La comtesse Mahé dOrgel ( )
* Bruno Garcin : François de Seyrieuse
* Micheline Presle : Madame de Seyrieuse ( )
* Gérard Lartigau : Paul Robin
* Sacha Pitoëff : Le prince Naoumof ( )
* Marpessa Dawn : Marie
* Claude Gensac : Mademoiselle dOrgel ( )
* Ginette Leclerc : Hortense dAusterlitz (  )
* Aly Raffy : Mirza
* Marcel Charvey : Lambassadeur ( )
* Béatrice Chatelier : Amina
* Max Montavon : Un invité ( )
* Wendy Nicholls : Hester

==References==
 

== External links ==
*  
 
 
 
 
 
 
 
 
 

 