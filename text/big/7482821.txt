Bravo, My Life
 
 
{{Infobox film
| name           = Bravo, My Life
| image          = Bravo, My Life (2005 film) poster.jpg
| caption        = Theatrical poster
| film name      = {{Film name
| hangul         =  ...  
| rr             = Saranghae, Malsoonssi
| mr             = Saranghae, Malsunssi}}
| director       = Park Heung-shik
| producer       = Bae Yong-guk
| writer         = Park Heung-shik   Kang Byeong-hwa   Jang Hak-gyo
| starring       = Moon So-ri Lee Jae-eung
| music          = Jo Seong-woo
| cinematography = Choi Young-taek
| editing        = Oh Myeong-jun
| distributor    = Showeast
| released       =  
| runtime        = 92 minutes
| country        = South Korea
| language       = Korean
| budget         =
| gross          =   
}}
Bravo, My Life ( ; lit. "I Love You, Mal-soon") is a 2005 South Korean film directed by Park Heung-shik about an adolescent boy who starts to come of age in the late 70s and early 80s, largely oblivious to the dramatic political events occurring around him.  The film sold 406,526 tickets nationwide. 

==Plot== President Park Chung-hees Assassination of Park Chung-hee|assassination. But for 14-year-old Gwang-ho, it is more importantly his first day at junior high, where the kids are interested in football and brawling. Gwang-hos mother, Mal-soon, whose husband is working in Saudi Arabia, devotes everything to her children. Despite a nagging illness, Mal-soon wears heavy make-up as she sells cosmetics door-to-door. Meanwhile, as his sexual awareness increases, Gwang-ho turns his attention and affection to their pretty neighbor Eun-sook, an assistant nurse who is the complete opposite of Gwang-hos mother. One day, Gwang-ho receives a "chain letter|good-luck letter." The letter states that unless he immediately writes and sends the same letter to someone else, he will be faced with bad luck. He starts sending it to people around him, but as those people start vanishing, Gwang-ho is racked with guilt, suspecting the letter of luck is the cause of their disappearance.

==Cast==
* Moon So-ri as Kim Mal-soon
* Lee Jae-eung as Gwang-ho
* Yoon Jin-seo as Eun-sook
* Kim Dong-young as Chul-ho
* Park Yoo-seon as Hye-sook
* Kim Bong-geun
* Lee Han-wi
* Kang Min-hwi as Jae-myung
* Park Myung-shin
* Lee Kan-hee as Sang-soos mother

==References==
 

==External links==
*   
* 
* 
* 

 
 
 
 
 


 