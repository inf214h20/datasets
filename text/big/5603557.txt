Should Married Men Go Home?
{{Infobox film
| name = Should Married Men Go Home?
| image = L&H_Should_Married_Men_Go_Home_1928.jpg
| caption = Theatrical poster
| director =  Leo McCarey James Parrott
| producer = Hal Roach
| writer = Leo McCarey James Parrott H.M. Walker
| starring = Stan Laurel Oliver Hardy
| music =
| cinematography = George Stevens
| editing = Richard C. Currier
| distributor = Metro-Goldwyn-Mayer
| released =  
| runtime = 20 min.
| country = United States
| language = Silent film English intertitles
| budget =
}}
Should Married Men Go Home? is a silent two-reel comedy produced by the Hal Roach Studios and starring Laurel and Hardy. It was filmed in March and May 1928, and released by Metro-Goldwyn-Mayer on September 8 of that year. It was the first Roach film to bill Laurel and Hardy as a team—previously, their appearances together were under the Roach "All-Star Comedy" banner. Footage of the film featuring Laurel and Hardy on location in between shooting and some apparent out-takes has recently surfaced on YouTube. 

== Plot ==
Ollie and his wife are enjoying a quiet Sunday at home until Stan shows up, eager to play some golf. After Stan breaks the Hardys Victrola and nearly sets fire to their house, Mrs. Hardy chases the boys out. At the golf course, they are partnered with a pair of comely young lasses to complete a foursome. The girls want to be treated to sodas, but the boys are short of money. Stan leaves his watch to settle the thirty-cent bill. On the course, they tangle with rude golfer Edgar Kennedy, and wind up in a mud-throwing battle with several other linksters.

== Cast ==
* Stan Laurel as Stan
* Oliver Hardy as Ollie
* Kay Deslys as Mrs. Hardy (uncredited)
* Edna Marion as Blonde Girlfriend (uncredited)
* Viola Richard as Brunette Girlfriend (uncredited) Charlie Hall as Soda Jerk (uncredited)
* Edgar Kennedy as Golfer (uncredited)
* John Aasen as Very Tall Golfer (uncredited)
* Chet Brandenburg as Caddy (uncredited)
* Dorothy Coburn as Muddy Combatant (uncredited)
* Jack Hill as Muddy Combatant (uncredited)
* Sam Lufkin as Shop Manager (uncredited)
* Lyle Tayo as Lady Golfer (uncredited)

== Production ==
 

=== Supporting cast ===
John Aasen was (un)credited as "Very Tall Golfer" and it was indeed typecasting: Aasen was 8-foot-9.

Golf course girlfriends Edna Marion and Viola Richard had both been notified that Should Married Men Go Home? would be their last outing for the Hal Roach Studios — that their contracts would not be renewed. Marion had appeared in nearly fifty films, most for Roach, yet would go on to appear in only a half-dozen Poverty Row productions; she would be completely out of pictures by 1932. Viola Richard had enlivened several Charley Chase and Laurel and Hardy silent comedies before Married Men, yet would see service only twice more — both in 1935, both for Roach, both only as an extra; perhaps it was her considerable resemblance to "It (1927 film)|It Girl" Clara Bow that held her back in casting offices.

=== Routines for the future ===
The soda fountain routine — funny here in silent guise — would be reworked with sound and become even better a year later in the L&H talkie Men O War. Jimmy Finlayson would take over dispensing duties from Charlie Hall in the talkie.
 Come Clean when the Hardys again pretend not to be home when the Laurels come calling.

=== Out on the links ===
Oil derricks are visible just off some of the fairways on the golf course.

Should Married Men Go Home? slots right in with the L&H shorts of its era as far as the pattern of action at the finale: a widening circle of anarchy and mayhem envelops The Boys, the bystanders and everyone in between. Mud is the aggressive instrument of choice in Married Men, while in Youre Darn Tootin its ripped pants, pies in The Battle of the Century and auto parts in Two Tars.

=== Title and industry excitement ===
As was typical, the project had a working title: this ones was Follow Through. Also typically, it was H.M. Walker|H. M. "Beanie" Walker who wordsmithed the final release title. Skretvedt, Randy (1996). Laurel and Hardy: The Magic Behind the Movies. Beverly Hills: Past Times Publishing. ISBN 0-940410-29-X 

The time lag between the primary filming in March and the September release of Should Married Men Go Home? was unusually long—some six months—and included some summer vacation "away time." When the company reconvened in Los Angeles in Autumn 1928, the fame and popularity of the Laurel and Hardy team had charged up the town. L&H historian Randy Skretvedt notes that:

:"By the time the film was released on September 8, L&H had become tremendously popular. MGMs publicity department began issuing billboard-sized posters to publicize the team. If this was an uncommon practice for a series of short subjects, MGM felt the team deserved the extra fanfare." 

== Critical reputation ==
Laurel and Hardy authors and critics are strangely silent on Should Married Men Go Home? Despite its inclusion in 1967s feature-length compilation The Further Perils of Laurel and Hardy, it is today either overlooked or underappreciated;  the best that prolific commentator Leslie Halliwell can muster for it is a lukewarm "Goodish star slapstick, but the preliminary domestic scene is the funniest." 

The lone exception to this is early L&H analyst William K. Everson who wrote (perhaps not coincidentally) in 1967: "One of the best of the "forgotten" Laurel and Hardy films, Should Married Men Go Home? admittedly overlaps with several other of their films but is no less funny because of it."  He cites Ollies collapsing of the front fence, the soda fountain routine and Edgar Kennedys toupee woes as high spots.

== References ==
 

== External links ==
* 
*  
* 

 
 

 
 
 
 
 
 
 
 