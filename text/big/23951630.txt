Silver Blaze (1937 film)
{{Infobox film
| name           = Silver Blaze
| image          = "Silver_Blaze"_(1937).jpg
| image_size     =
| caption        = U.S. poster
| director       = Thomas Bentley
| producer       = Julius Hagen
| writer         = Arthur Conan Doyle (story "Silver Blaze") H. Fowler Mear (adaptation) Arthur Macrae
| narrator       = Ian Fleming
| music          =
| cinematography = Sydney Blythe William Luff
| editing        = Michael C. Chorlton Alan Smith
| distributor    =
| released       = July 1937
| runtime        = 71 minutes USA: 65 minutes (TCM print)
| country        = United Kingdom English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}
 1937 Cinema Ian Fleming The Hound of the Baskervilles.   

== Synopsis==
Sherlock Holmes takes a holiday by visiting his old friend, Sir Henry Baskerville (Lawrence Grossmith). Holmes vacation ends when he and Watson suddenly find themselves in the middle of a double-murder mystery; they must find Professor Robert Moriarty (Lyn Harding) and Silver Blaze before the horse race, and bring the criminals to justice. Neither Moriarty nor Baskerville appear in the original story.

== Cast ==
* Arthur Wontner as Sherlock Holmes Ian Fleming as Dr. Watson
* Lyn Harding as Professor Moriarty John Turnbull as Inspector Lestrade Robert Horton as Col. Ross
* Lawrence Grossmith as Sir Henry Baskerville
* Judy Gunn as Diana Baskerville
* Arthur Macrae as Jack Trevor
* Arthur Goullet as Col. Sebastian Moran
* Martin Walker as James Straker
* Eve Gray as Mrs. Mary Straker
* Gilbert Davis as Miles Stanford
* Minnie Rayner as Mrs. Hudson
* D. J. Williams as Silas Brown
* Ralph Truman as Bert Prince
* Ronald Shiner as Simpson the Stable Boy / Jockey (uncredited)

==Critical reception==
TV Guide wrote that the film "suffers from too slight a plot stretched out to feature length. Wontner is good in his final portrayal of the great detective, and the film does have some interesting moments; but on the whole this is lackluster Holmes, an all too elementary case."  

== Soundtrack ==
 

==References==
 

== External links ==
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 


 
 