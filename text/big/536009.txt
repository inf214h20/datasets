The Talented Mr. Ripley (film)
 
{{Infobox film
| name           = The Talented Mr. Ripley
| image          = Talented mr ripley.jpg
| alt            =
| caption        = Theatrical release poster
| director       = Anthony Minghella
| producer       = William Horberg Tom Sternberg
| screenplay     = Anthony Minghella
| based on       =  
| starring       = Matt Damon Gwyneth Paltrow Jude Law Cate Blanchett
| music          = Gabriel Yared
| cinematography = John Seale
| editing        = Walter Murch
| distributor    = Paramount Pictures Miramax Films
| released       =   
| runtime        = 138 minutes
| country        = United States
| language       = English
| budget         = $40 million 
| gross          = $128,798,265  . Box Office Mojo. Internet Movie Database|IMDb. Retrieved 2012-02-23. 
}} adaptation of novel of the same name, the film stars Matt Damon as Tom Ripley, Jude Law as Dickie Greenleaf, Gwyneth Paltrow as Marge Sherwood and Cate Blanchett as Meredith Logue.
 Plein Soleil in 1960.

==Plot== Princeton with his son, Dickie, because Ripley is wearing a borrowed Princeton blazer.  Greenleaf recruits Ripley to travel to Italy and persuade Dickie to return to the United States, for which he will pay Ripley $1,000 ( ). Ripley accepts the proposal, even though he did not attend Princeton and has never met Dickie.
 jazz lover.  The two concoct a scheme for Ripley to wring additional funds from Herbert Greenleaf by regularly mailing letters suggesting Dickie is wavering and will likely return to America if Ripley can remain in Italy and continue applying pressure.
 San Remo, where Dickie is shopping for a new residence. While at sea, Ripley suggests he return to Italy the following year and the two become housemates.  Dickie dismisses Ripleys plan, informs him that he intends to marry Marge and admits he has grown weary of Tom.  Upset by this news, Ripley confronts Dickie about his behavior and lashes out in rage, repeatedly hitting Dickie with an oar, killing him. To conceal the murder, Ripley scuttles the boat with Dickies body aboard before swimming ashore.

When the hotel concierge mistakes him for Dickie, Ripley realizes he can assume Dickies identity. He forges Dickies signature, modifies his passport and begins living off Dickies trust fund. He uses Dickies typewriter to communicate with Marge, making her believe that Dickie has left her and has decided to stay in Rome. He checks into two separate hotels as himself and as Dickie, passing messages via the hotel staff to create the illusion that Dickie is still alive. His situation is complicated by the reappearance of Meredith, who still believes that he is Dickie. 

Ripley rents a large apartment and spends a lonely Christmas buying himself expensive presents. Freddie tracks Ripley to his apartment in Rome through the American Express office, expecting to find Dickie.  Freddie is immediately suspicious of Ripley as the apartment is not furnished in Dickies style, while Ripley appears to have adopted Dickies hair-style and mannerisms. On his way out, Freddie encounters the buildings landlady who refers to Ripley as "Signor Dickie" and remarks on the piano music constantly emanating from the apartment. Freddie notes that Dickie does not play piano and goes back to confront Ripley who attacks Freddie, hitting him over the head with a heavy statue, murdering him.  Ripley carries the body to Freddies car, drives to the woods, abandoning the vehicle and leaving Freddies corpse lying on the ground, where it is quickly discovered.

Ripleys existence then becomes a cat-and-mouse game with the Italian police and Dickies friends. Ripley eludes imminent capture and clears himself by forging a suicide note addressed to Ripley in Dickies name. He then moves to Venice and rents an apartment under his real name. Though trusted by Dickies father, Ripley is disquieted when Mr. Greenleaf hires American private detective Alvin MacCarron to investigate Dickies disappearance. Marge suspects Ripleys involvement in Dickies death and confronts him after finding Dickies rings in Ripleys bathroom. Ripley appears poised to murder Marge but is interrupted when Peter Smith-Kingsley, a mutual friend, enters the apartment with a key Ripley had given him. 

MacCarron, after uncovering certain sordid details about Dickies past, reveals to Ripley that Mr. Greenleaf has requested the investigation be dropped.  MacCarron will not share his revelations with the Italian police and asks Ripley to promise to do the same.  In exchange for his candor, and implications made in Dickies suicide note, Herbert Greenleaf intends to transfer a substantial portion of Dickies trust fund income to Ripley. Marge is dismayed at the resolution, furiously accusing Ripley of involvement in Dickies disappearance before Greenleaf and MacCarron drag her away.

Now lovers, Ripley and Peter go on a cruise together, only to discover that Meredith is also on board. Ripley realizes that he cannot prevent Peter from communicating with Meredith and discovering that he has been passing himself off as Dickie.  Peter and Meredith know each other and would certainly meet at some point on the voyage. He cannot solve this dilemma by murdering Meredith, because she is accompanied by her family. Ripley enters Peters room and suggests the two remain below deck for the duration of the cruise, but quickly dismisses this idea as he cannot offer Peter a legitimate reason for doing so.  Ripley sobs as he strangles Peter in his bed then returns to his own cabin, where he sits alone.

==Cast==
* Matt Damon as Tom Ripley
* Gwyneth Paltrow as Marge Sherwood
* Jude Law as Dickie Greenleaf
* Cate Blanchett as Meredith Logue
* Philip Seymour Hoffman as Freddie Miles
* Jack Davenport as Peter Smith-Kingsley
* James Rebhorn as Herbert Greenleaf
* Sergio Rubini as Inspector Roverini
* Philip Baker Hall as Alvin MacCarron
* Celia Weston as Aunt Joan
* Ivano Marescotti as  Col. Verrechia of the  carabinieri

==Release==

===Reception===
Critical reaction was positive, and the film has a rating of 83% on Rotten Tomatoes. 
 Eagle Scout eyes fixed just a blink more than the calm gaze of any non-murdering young man. And in that opacity we see horror".   

Charlotte OSullivan of Sight and Sound  wrote, "A tense, troubling thriller, marred only by problems of pacing (the middle section drags) and some implausible characterisation (Merediths obsession with Ripley never convinces), its full of vivid, miserable life".    Time (magazine)|Time named it one of the ten best films of the year and called it a "devious twist on the Patricia Highsmith crime novel".    James Berardinelli gave the film two and a half stars out of four, calling it "a solid adaptation" that "will hold a viewers attention", but criticized "Damons weak performance" and "a running time thats about 15 minutes too long."  Berardinelli compared the film unfavorably with the previous adaptation, Purple Noon, which he gave four stars.  He wrote, "The remake went back to the source material, Patricia Highsmiths The Talented Mr. Ripley. The result, while arguably truer to the events of Highsmiths book, is vastly inferior. To say it suffers by comparison to Purple Noon is an understatement. Almost every aspect of René Cléments 1960 motion picture is superior to that of Minghellas 1999 version, from the cinematography to the acting to the screenplay. Matt Damon might make a credible Tom Ripley, but only for those who never experienced Alain Delons portrayal." 

In his review for  , Amy Taubin criticized Minghella as a "would-be art film director who never takes his eye off the box office, doesnt allow himself to become embroiled in such complexity. He turns The Talented Mr. Ripley into a splashy tourist trap of a movie. The effect is rather like reading The National Enquirer in a café overlooking the Adriatic Sea|Adriatic".    Damon was apparently unhappy with the films departures from Highsmiths novel, telling an interviewer shortly after the film was released, "Id like to make the whole film all over again with the same cast and same title but make it completely like the book." 

===Awards===
{| class="wikitable"
|-
! Year
! Award
! Category
! Recipient(s)
! Result
|-
| rowspan="5" | 1999 Academy Awards Best Actor in a Supporting Role
| Jude Law
|  
|- Best Adapted Screenplay
| Anthony Minghella
|  
|- Best Art Direction Roy Walker   Bruno Cesari  
|  
|- Best Costume Design Gary Jones
|  
|- Best Original Score
| Gabriel Yared
|  
|- style="border-top:2px solid gray;"
| rowspan="7" | 2000 BAFTA Awards Best Film
| William Horberg Tom Sternberg
|  
|- Best Actor in a Supporting Role
| Jude Law
|  
|- Best Actress in a Supporting Role
| Cate Blanchett
|  
|- Best Direction
| rowspan="2" | Anthony Minghella
|  
|- Best Adapted Screenplay
|  
|- Best Cinematography
| John Seale
|  
|- Best Film Music
| Gabriel Yared
|  
|- style="border-top:2px solid gray;"
| 2000
| Berlin International Film Festival
| Golden Bear
| Anthony Minghella
|  
|- style="border-top:2px solid gray;"
| rowspan="2" | 2000 Broadcast Film Critics Association Awards Best Film
!
|  
|- Best Composer
| Gabriel Yared
|  
|- style="border-top:2px solid gray;"
| 2000 Chicago Film Critics Association Awards
| Best Cinematography
| John Seale
|  
|- style="border-top:2px solid gray;"
| 2001 Empire Awards Best British Actor
| Jude Law
|  
|- style="border-top:2px solid gray;"
| rowspan="5" | 2000 Golden Globe Awards Best Motion Picture – Drama
!
|  
|- Best Actor – Motion Picture Drama
| Matt Damon
|  
|- Best Supporting Actor – Motion Picture
| Jude Law
|  
|- Best Director
| Anthony Minghella
|  
|- Best Original Score
| Gabriel Yared
|  
|- style="border-top:2px solid gray;"
| rowspan="6" | 2000
| rowspan="6" | Las Vegas Film Critics Society Awards
| Best Film
!
|  
|-
| Best Actor
| Matt Damon
|  
|-
| Best Director
| rowspan="2" | Anthony Minghella
|  
|-
| Best Screenplay
|  
|-
| Best Cinematography
| John Seale
|  
|-
| Best Score
| Gabriel Yared
|  
|- style="border-top:2px solid gray;"
| rowspan="2" | 2000 London Film Critics Circle Awards
| British Screenwriter of the Year
| Anthony Minghella
|  
|- British Supporting Actor of the Year
| Jude Law
|  
|- style="border-top:2px solid gray;"
| rowspan="2" | 2000 MTV Movie Awards
| Best Musical Sequence
| Matt Damon Rosario Fiorello Jude Law
|  
|- Best Villain
| Matt Damon
|  
|- style="border-top:2px solid gray;"
| rowspan="3" | 2000 National Board of Review Awards
|  
!
!
|- Best Supporting Actor
| Philip Seymour Hoffman
|  
|- Best Director
| Anthony Minghella
|  
|- style="border-top:2px solid gray;"
| 2000 Online Film Critics Society Awards Best Adapted Screenplay
| Anthony Minghella
|  
|- style="border-top:2px solid gray;"
| rowspan="6" | 1999 Satellite Awards Best Film
!
|  
|- Best Supporting Actor – Motion Picture Drama
| Jude Law
|  
|- Best Director
| rowspan="2" | Anthony Minghella
|  
|- Best Adapted Screenplay
|  
|- Best Cinematography
| John Seale
|  
|- Best Editing
| Walter Murch
|  
|- style="border-top:2px solid gray;"
| rowspan="4" | 2000
| rowspan="4" | Teen Choice Awards Choice Movie – Drama
!
|  
|-
| Choice Movie Actor – Drama
| Matt Damon
|  
|-
| Choice Movie Breakout Performance
| Jude Law
|  
|-
| Choice Movie Liar
| Matt Damon
|  
|- style="border-top:2px solid gray;"
| 2000 Writers Guild of America Awards Best Adapted Screenplay
| Anthony Minghella
|  
|}

==References==
 

==External links==
 
*  
*  
*  
*  
*  
*  
*  

 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 