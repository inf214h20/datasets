The Bells (1918 film)
{{infobox film
| name           = The Bells
| image          =The Bells (1918) - Ad 1.jpg
| imagesize      =190px
| caption        =Ad for film
| director       = Ernest C. Warde
| producer       = Anderson-Brunton Company
| based on       =   Jack Cunningham(film scenario) Lois Wilson
| cinematography =
| editing        =
| distributor    = Pathé Exchange
| released       =   reels
| country        = USA
| language       = Silent film (English intertitles)
}}
 lost 1918 American silent drama film released by Pathé Exchange and based on the play, The Bells, by Emile Erckmann and Alexandre Chatrian. The play had been a favorite vehicle for actor Henry Irving.
 Lois Wilson. The Bells with Lionel Barrymore and Boris Karloff.  

==Plot==
As reported in a film publication,  Mathias, the struggling innkeeper in an Alsatian hamlet, murders a wealthy Jew who comes to spend a night at the inn in order to pay off debts and a mortgage.

The murderer is never discovered, but the season passes into local history as the "Polish Jews winter." Mathias prospers, and years later his daughter becomes engaged to the captain of the Gendarme (historical)|gedarmes. Mathias prepares her dowry, and the sight of the gold coins brings again to his tortured conscience the ever-present sound of the sleigh-bells that heralded the approach of the ill-fated Jewish guest. In his sleep he dreams he is on trial and a hypnotist wrings a confession from him. In an ecstasy of fear he expires in the arms of his wife and daughter, the victim of Heavens justice.

==Cast==
*Frank Keenan - Mathias Lois Wilson - Annette
*Edward Coxen - Christian
*Carl Stockdale - Gari
*Albert R. Cody - Nickel
*Joseph J. Dowling - Lisparre
*Ida Lewis - Catherine
*Burton Law - Koveski (*as Bert Law)

==References==
 

==External links==
* 
* 
* 
* 

 
 
 
 
 
 
 
 
 


 