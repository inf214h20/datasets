The Age of Adaline
 
{{Infobox film
| name           = The Age of Adaline
| image          = The Age of Adaline film poster.png
| image_size     = 
| caption        = Theatrical release poster
| film name      = 
| director       = Lee Toland Krieger
| producer       = Sidney Kimmel   Gary Lucchesi   Brett Ratner   Tom Rosenberg
| writer         = J. Mills Goodloe   Salvador Paskowitz
| starring       = Blake Lively  Michiel Huisman  Kathy Baker  Amanda Crew  Harrison Ford  Ellen Burstyn
| narrator       = Hugh Ross
| music          = Rob Simonsen
| cinematography = David Lanzenberg
| editing        = Melissa Kent
| studio         = Lakeshore Entertainment  Sidney Kimmel Entertainment  RatPac-Dune Entertainment Lionsgate
| released       =  
| runtime        = 112 minutes  
| country        = United States
| language       = English
| budget         = $25 million 
| gross          = $26 million   
}} epic romance romance fantasy fantasy film directed by Lee Toland Krieger and written by J. Mills Goodloe and Salvador Paskowitz. The film stars Blake Lively, Michiel Huisman, Kathy Baker, Amanda Crew, Harrison Ford, and Ellen Burstyn. The film was released on April 24, 2015.

==Plot==
Adaline Bowman (Blake Lively) is seen purchasing fake IDs at an apartment in San Francisco. The forger asks her why she chose to be 29 – with her looks, she could shave off a few years. She smiles and says he’s too kind. While leaving, she asks why he makes fakes IDs when he has the potential for much more. She also notes that the autographed baseballs on his desk show his real name. It’s the little things that slip you up, she says. Adaline goes home to her apartment where her dog greets her. It is seen that during her 107 years of being alive, she has raised the same kind of dog over and over again. She notes she is late for work at the library’s office of archives.

While working, she opens a film reel and her life is explained. She was born on New Years Day in 1908, got married, had a child, and became a widow when her husband suffered an accident during the building of the Golden Gate Bridge. One night, an unexplainable snow begins to fall as she is driving to her parent’s house. She suffers a car accident/lightning strike combination that causes her to remain 29 years old forever. At first it is not noticeable, but as her daughter grows older, it becomes more apparent. One day she is pulled over by a cop, who takes away her ID as it says she is in her late 40s. She decides to move away and does a year of research at a medical college but cannot find anything to explain her condition.

Late one rainy night, she is walking home when two FBI agents follow her. They put her in a car and try to take her on an airplane. She escapes out the trunk and decides she will spend her life running away with a new look and identity every decade. She explains this to her daughter and they have a heartfelt goodbye. At present day, she adds a cosigner to her account and has a flashback to when she first opened the account. She had invested in Xerox and it has paid off well, which explains her financial situation. She is planning to leave soon to live on a farm in Oregon.

It’s New Years Eve and she enters the hotel to a party. On the wall is a picture of her with friends and it’s clearly from many decades ago. She stares at it and moves on to find her friend who is playing the piano for a party. Her friend is blind and jokes to Adaline that they’re cougars as only young men go after her. A young man walks up to Adaline and she quickly pins him, Sherlock Holmes style. From his expensive heirloom watch to the paint on his hands, she knows he’s an artist who comes from a wealthy family. As they make small talk, Ellis Jones (Michiel Huisman) walks in the room. They have a moment of eye contact until a brunette kisses Ellis on the cheek and Adaline looks away.

At midnight, she walks out of the room and calls her daughter. Her daughter sings happy birthday on the phone and confirms lunch the next day. A young man walks up to Adaline and tries a smooth line about kissing a stranger at midnight. He asks if she’s heard it before and she says yes. She says goodbye to her friend and walks to the elevator. Before the elevator closes, Ellis pushes his hand to open it and they ride down together. He tries to smooth talk her and fails. She politely turns him down all the way to her taxi.

The next day at work, her coworkers mention a generous benefactor is coming by to donate some books. Ellis brings her books that have flowers in the name. Apparently, he’s seen her before at board meetings and knew she worked there. He asks her to be in the photograph they’re doing for publicity and she quickly says no, she doesn’t like being photographed. He suggests a date instead and she says no. He then says he’ll withdraw his donation if she declines the date. In the next scene, they are in the tunnels beneath San Francisco. They found a boat. He tells her a great deal about himself and then asks about her. All she is willing to say is “I have a dog.” As they leave, he offers to tell her a joke and if she laughs then she has to go out with him again. He tells a terrible joke: A horse owner is bragging that their horse can, incredibly, bat and catch in the game of baseball very well. Upon being asked if the horse can pitch, the owner replies: "What? Who ever heard of a horse pitching?". Adaline laughs at this punchline, thereby committing her to a second date with Ellis.

Adaline goes to Ellis’ apartment for the next date where they have hot dogs, wine, and listen to jazz. They spend the night together. The next morning, Ellis is on the phone having trouble with a work call in Portuguese. (He majored in mathematics and discovered an algorithm. Ellis’ friend figured out how to make money off it so they split the profits and his friend is off in Fiji while Ellis is a philanthropist.) Adaline rapidly fires off some Portuguese on his phone and leaves. Somewhere in there is a scene between her and her daughter, who could look like Adaline’s grandmother. She talks about having trouble getting up stairs and wants to move to a retirement home in Arizona. Adaline is upset and says she planned on moving to Oregon so they could see each other more. Her daughter urges Adaline to stop running, as the people who were interested in catching her have long since passed.

Adaline also has a flashback to an unknown man. He is fiddling with an engagement ring. She doesn’t go to meet him. Presumably because of this memory, she doesn’t respond to any of Ellis’ calls. He shows up at her apartment in Chinatown and she freaks out, demanding to know how he found her address. She brushes him off. While looking through old photographs, she has a change of heart. She’s clearly very lonely. During this time, she has had to put her dog down. She goes back to Ellis’ work to apologize. They go on a date to an old covered drive-in movie theater. She explains the history as if she was there. They drink wine and look at the stars on the ceiling. He asks her to attend his parents’ 40 year anniversary party and she says yes. On the way there, she picks up his sister, Kikki Jones (Amanda Crew). They enter his home and she is greeted by William Jones (Harrison Ford), who immediately calls her Adaline. She says that was her mother. He’s very shaken and says they were “very close.”

The next morning William can’t stop talking about “Adaline” which makes his wife a little annoyed. There’s a flashback which explains how they met. Adaline was having car trouble in England and he was a soldier studying medicine overseas. They both returned to America together. She pushed William to follow his dream of astronomy instead of medicine. He was the one with the engagement ring. He describes things about Adaline that Ellis picks up on, such as her interest in languages and driving skills. That night, they play trivia. William is on a 47 game winning streak. Adaline pretends to not know an answer but after a diss by Ellis she goes all out and wins. The family jokes that they didn’t know what would happen first, William’s loss in trivia or the arrival of Della – a meteor he predicted would come and also Adaline’s nickname.

The day of the party, everyone is out doing things. Adaline talks to William and he notices a scar on her left hand – a scar from stitches he made while they were hiking decades ago. He rummages through an old shed to find a picture to make sure he isn’t crazy. Yep, same scar. He runs after her, asking her if this is the reason she left him. She says yes. He begs her not to run but she says she doesn’t know how. She runs back to the house, writes a note to Ellis, packs, grabs his car keys and leaves. Ellis comes home and is confused. William doesnt explain anything. Ellis drives his dad’s car to chase after her.

Adaline is driving in the woods when she stops. She calls her daughter and they have a moment. She decides she will stop running. As she turns the car around, a tow truck plows into her and drives off. Inexplicably, snow begins falling again. Ellis pulls up and sees what’s happening. An ambulance takes her to the hospital. She wakes up to Ellis and decides to come clean. Her daughter arrives, sees Ellis, and says “I’m…her grandmother.” Adaline tells her “he knows” and she cries with joy and hugs him.

One year later, Ellis and Adaline are going to a New Years Eve party. Adaline suggests her daughter to go out but she has a date night in (another puppy). Before leaving, Adaline checks the mirror and does a double take. She plucks a gray hair, a sign of aging. Apparently, a combination of the defibrillator and hypothermia have resumed her normal aging process. Also, Della the meteor arrives. It’s 50 years too late, but it shines brighter than ever.

==Cast==
 
* Blake Lively as Adaline Bowman 
* Michiel Huisman as Ellis Jones 
* Harrison Ford as William Jones 
** Anthony Ingruber as young William
* Ellen Burstyn as Flemming Prescott 
* Kathy Baker as Kathy Jones
* Amanda Crew as Kikki Jones
* Lynda Boyd as Regan
* Anjali Jay as Cora
* Richard Harmon as Tony
* Mark Ghanimé as Caleb
* Barclay Hope as Stanley Chesterfield
* Chris William Martin as Dale Davenport
* Lane Edwards as Dr. Larry Levyne
* Peter J. Gray as Clarence James Prescott 
 

==Production==
On May 12, 2010, it was announced that The Age of Adaline would be co-financed and co-produced by Lakeshore Entertainment and Sidney Kimmel Entertainment. J. Mills Goodloe and Salvador Paskowitz wrote the script.  Sierra / Affinity has the international rights, while producers were Steve Golin, Alix Madigan, Tom Rosenberg and Gary Lucchesi.  On July 20, 2010 it was reported that Andy Tennant was set to direct the film.  On October 31, 2010, Summit Entertainment bought the US distribution rights to the film, which was set to begin shooting in March 2011, for an early 2012 release. 

On February 22, 2011, it was reported that Gabriele Muccino was in talks to direct the film, replacing Tennant, with the film re-titled from The Age of Adaline to simply Adaline.  On May 14, 2012, it was announced that Spanish director Isabel Coixet would direct the film instead.  On October 16, 2013, director Lee Toland Krieger was reported to be the director of the film in place. 

===Casting=== TheWrap reported that Natalie Portman had been offered the lead role.  On August 25, Portman told Entertainment Weekly that she had declined the offer. 

On October 16, 2013, Blake Lively and Ellen Burstyn were cast in the film to play the lead roles, with Lively starring as the title character.    On January 15, 2014, Harrison Ford joined the cast, and the film was set to begin shooting in March.    On February 11, 2014, Michiel Huisman joined the cast to star opposite Lively, as the love interest of Adaline.   

===Filming===
Filming began on March 10, 2014 in Vancouver, and continued through May 5.   On March 11, 2014, filming at the Hotel Vancouver commenced. 

==Release==
On August 15, 2014, Lionsgate set the film for a January 23, 2015 worldwide release.  Later, the date was shifted to April 24, 2015. 

==Reception==
The Age of Adaline has received mixed reviews from critics, though Livelys performance was praised. On Rotten Tomatoes, the film has a rating of 52%, based on 102 reviews, with an average rating of 5.6/10. The sites consensus reads "The Age of Adaline ruminates on mortality less compellingly than similarly themed films, but is set apart by memorable performances from Blake Lively and Harrison Ford." 

On Metacritic, the film has a score of 51 out of 100, based on 30 critics, indicating "mixed or average reviews".  In CinemaScore polls conducted during the opening weekend, cinema audiences gave The Age of Adaline an average grade of "A-" on an A+ to F scale. 

==See also==
* Life Is Beautiful (Lana Del Rey song)|"Life Is Beautiful" (Lana Del Rey song), the lead single from the albums soundtrack

==References==
 

==External links==
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 