Di Di Hollywood
{{Infobox film
| name           = Di Di Hollywood
| image          = Di_Di_Hollywood_poster.jpg
| caption        = Poster
| director       = Bigas Luna
| producer       = Bigas Luna Beatriz Bodegas Pedro Pastor 
| writer         = Carmen Chaves Gastaldo Bigas Luna
| starring       = Elsa Pataky Peter Coyote Paul Sculfor
| music          = Lluís Lu
| cinematography = Albert Pascual
| editing        = Regino Hernández Jaume Martí
| studio         = El Virgili Films La Canica Films Malvarrosa Media
| distributor    = Maya Entertainment
| released       =  
| runtime        = 96 min 
| country        = Spain
| language       = Spanish English 
| budget         = $8,600,000 
| gross          = $1,002,398 (Spain) 
}}

Di Di Hollywood is a 2010 drama film, written, directed and produced by Bigas Luna. It stars Elsa Pataky, Peter Coyote, and Paul Sculfor. It was released in Spain on 15 October 2010.

==Summary==
Diana Diaz (Pataky) is a bar worker that leaves Madrid for Miami. When she reaches Miami, she meet Robert (Hacha) and with him heads for Hollywood, willing to do anything to become famous. When she meets agent Michael McLean (Coyote), he changes her name to Di Di and gives her false hopes of becoming famous. But she discovers that he really just wants to use her in a cover up relationship with secretly gay actor Steve Richards (Sculfor). 

==Cast==
* Elsa Pataky as Diana Diaz "Di Di"   
* Peter Coyote as Michael Stein 
* Paul Sculfor as Steve Richards 
* Giovanna Zacaría as Nora 
* Luis Hacha as Robert 
* Flora Martínez as María 
* Jean-Marie Juan as David 
* Leonardo García as Aldo 
* Ben Temple as Richard Low 
* Ana Soriano as Madre de Diana

==Production==

===Filming===
Filming took place in Madrid, Spain;  Valencia, Spain; Elche, Spain; Ciudad de la Luz;  and the hospital scene was shot in Benidorm, Spain. Filming was shot from October - November 2009.   

==Soundtrack==
 
#"Where No Endings End" by Keren Ann (3:37) 
#"Time of Our Lives" by Gram Rabbit (4:06)   
#"Sad Song" by Au Revoir Simone (4:09) 
#"She Wolf (Shakira song)|Loba" by Shakira (3:07)
#"La Vie en rose" by Louis Armstrong (3:26)
#"In My Book" by Gram Rabbit (3:25) 
#"Heidis Theme" by Decoder Ring (2:37) 
#"Candy Flip" by Gram Rabbit (4:46) 
#"Azabache" by Lucas Masciano (5:48) 
#"Amor y lujo" by Mónica Naranjo (4:06) 
#"Curtain Up" by John Cacavas
#"Fiera inquieta" by Nicolas Uribe
#"If I Were A Boy" by Kym Mazelle (4:09) Blondie (3:31)

==Reviews== Variety reviewed is as "the script proves unable to make Di Di’s journey interesting or credible, while the uncharismatic Pataky is unconvincing as star material." 

==References==
{{reflist|
*   
*   
}}

==External links==
* 
 
 
 
 