Rowdy Ramudu Konte Krishnudu
{{Infobox film
| name           = Rowdy Ramudu Konte Krishnudu
| image          =
| caption        = Jandhyala  
| story          = Jandhyala &   Satyanand
| screenplay     =  
| producer       = N. T. Rama Rao
| director       = K. Raghavendra Rao Ralyalakshmi
| Chakravarthy
| cinematography = K. S. Prakash
| editing        = Ravi
| studio         = Ramakrishna Cine Studios   
| released       =  
| runtime        = 2:22:26
| country        =   India
| language       = Telugu
| budget         =
| gross          = 
}}
 Ralyalakshmi in the lead roles and music composed by K. Chakravarthy|Chakravarthy.   

==Cast==
 
*N. T. Rama Rao as Ramu
*Nandamuri Balakrishna as Balakrishna
*Sridevi as Mutyalu Ralyalakshmi as Lakshmi
*Rao Gopal Rao as Bhaji Prasad Satyanarayana as Satyanayana Jaggayya as I.G. Kantha Rao as Heroes father
*Mukkamala as Baba 
*Nutan Prasad as Giri
*Raavi Kondala Rao as Jailor
*Chalapathi Rao as Hargopal
*Prasad Babu as Madhav
*Lakshmi Kanth as Ranga
*Saradhi Chitti Babu
*Chidatala Appa Rao as Kotigadu
*Potti Prasad 
*Jagga Rao
*Jayasudha as Special Appearance
*Pushpalata as Heroes mother Latha as Special Appearance
*Jayamalini as Item Number
 

==Soundtrack==
{{Infobox album
| Name        = Rowdy Ramudu Konte Krishnudu
| Tagline     = 
| Type        = film Chakravarthy
| Cover       = 
| Released    = 1980
| Recorded    = 
| Genre       = Soundtrack
| Length      = 30:35
| Label       = AVM Audio Chakravarthy
| Reviews     =
| Last album  = Aatagadu   (1980)  
| This album  =  Rowdy Ramudu Konte Krishnudu   (1980)
| Next album  = Challenge Ramudu   (1980)
}}

Music composed by K. Chakravarthy|Chakravarthy. Lyrics written by Veturi Sundararama Murthy. Music released on AVM Audio Company. 
{|class="wikitable"
|-
!S.No!!Song Title !!Singers !!length
|- 1
|Ooo My Darling SP Balu, P. Susheela
|4:13
|- 2
|Konte Korikundi Maddeppedi Ramesh,P. Susheela
|4:15
|- 3
|Pappulo Uppu SP Balu,P. Susheela
|4:20
|- 4
|Jingala Jamjam SP Balu,P. Susheela
|4:50
|- 5
|Apoorva Sahodarulam SP Balu,Maddeppedi Ramesh
|3:35
|- 6
|Ammo Idhe Menaka SP Balu,Maddeppedi Ramesh,P. Susheela
|5:30
|- 7
|Seetakalam Vachindi SP Balu,P. Susheela
|3:52
|}

==Others==
* VCDs and DVDs on - Universal Videos, SHALIMAR Video Company, Hyderabad

==References==
 

 
 
 


 