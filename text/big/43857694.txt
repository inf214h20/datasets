Free the Nipple
 
{{Infobox film
| name           = Free the Nipple
| image          = 
| alt            = 
| caption        = 
| director       = Lina Esco
| producer       =  	
| writer         = Hunter Richards
| starring       =  
| music  =  	
| cinematography = Berenice Eveno
| editing        = Matt Landon
| studio         =  
| distributor    =  
| released       =  
| runtime        = 
| country        = United States
| language       = English
| budget         = $937,000
| gross          = 
}}

Free the Nipple is a 2014 American comedy feature film directed by Lina Esco and written by Hunter Richards. Lina Esco started the Free the Nipple (campaign)|"Free the Nipple" campaign after it was nearly impossible to release the film. The campaign addresses societal taboos about public exposure of female breasts, Esco created this film to draw public attention to the issue of gender equality and encourage discussion over Americas glorification of violence and repression of sexuality.    The Free The Nipple campaign supports feminine body equality and empowerment through social action while working to defeat female body oppression and archaic censorship laws.  The film stars Casey LaBow, Monique Coleman, Zach Grenier, Lina Esco, Griffin Newman and Lola Kirke.                          When in post-production during February 2014, the film was picked up for distribution by Paris-based WTFilms. 

== Synopsis ==
Led by Liv (Lola Kirke), an army of passionate women launch a revolution to "Free the Nipple" and decriminalize female toplessness. Based on true events, a mass movement of topless women, backed by First Amendment lawyers, graffiti installations and national publicity stunts, invade New York City to protest censorship hypocrisies and promote gender equality legally and culturally in the US.

== Cast ==
 
* Casey LaBow as Cali
* Monique Coleman as Roz
* Zach Grenier as Jim Black
* Lina Esco as With
* Lola Kirke as Liv
* Michael Panes as Lawyer
* John Keating as Kilo
* Griffin Newman as Orson
* Leah Kilpatrick as Elle
* Jen Ponton as Charlie
* Liz Chuday as Blogger Liz
* Leah Kilpatrick as Elle
* Sarabeth Stroller as Pippy 
 

==  Advocacy ==
Miley Cyrus, who had worked with Esco on LOL (2012 film)|LOL, came forward to support the film and a womans right to bare a nipple in public.      Joining Cyrus in her support of Esco and the film are Rumer Willis, Nico Tortorella, Lydia Hearst, Giles Matthey,    Cara Delevingne,    and Russell Simmons.   

==Release==
The film was picked up by IFC Films, and it was announced on September 29, 2014, that Sundance Selects picked the film for North American release.   

== References ==
 

== External links ==
*  
*  

 
 
 
 
 
 
 