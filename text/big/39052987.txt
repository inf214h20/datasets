Illicit (film)
{{Infobox film
| name           = Illicit
| image          = Illicit_1931_Poster.jpg
| border         = 
| alt            = 
| caption        = Lobby card
| director       = Archie Mayo
| producer       = Darryl F. Zanuck
| writer         = {{Plainlist|
* Edith Fitzgerald
* Robert Riskin
}}
| screenplay     = Harvey F. Thew
| starring       = {{Plainlist|
* Barbara Stanwyck James Rennie
* Ricardo Cortez
* Natalie Moorhead
}}
| music          = {{Plainlist|
* Erno Rapee
* Louis Silvers
}}
| cinematography = Robert Kurrle William Holmes
| studio         = Warner Bros.
| distributor    = Warner Bros.
| released       =  
| runtime        = 79 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} James Rennie, Ricardo Cortez, and Natalie Moorhead. Based on a play by Edith Fitzgerald and Robert Riskin, the film is about a young couple living together out of wedlock because the woman does not believe in marriage. When they finally get married, both become unfaithful to each other. Illicit was produced and distributed by Warner Bros.   

==Plot==
 
Barbara Stanwyck plays as a woman who has modern ideas about love. She believes that marriage kills love and leads to unhappiness and, inevitably, divorce. Although her boyfriend, played by James Rennie, and his father, Claude Gillingwater, try to persuade Stanwyck to get married, she resists their arguments. Stanwyck and Rennie live together for a while without getting married but eventually Stanwyck caves in to avoid scandal and agrees to marry Rennie. Stanwyck receives a telegram from her ex-boyfriend, played by Ricardo Cortez, saying that he wants to visit her. Rennie tries to prevent Stanwyck from seeing him, but she does so anyway. Cortez tries to persuade Stanwyck not to get married, tells her that he is still in love with her and warns her that she will be unhappy if she marries, but she remains unconvinced. Stanwyck marries Rennie and eventually they settle down and start to behave like a typical married couple. They become bored with each other, avoid each other and fight over silly things. Rennie becomes interested in Natalie Moorhead, who tells him that she is in love with him. Soon, he begins a torrid affair with her. He spends less and less time with Stanwyck. Eventually Stanwyck tells Rennie that they need to separate for a time. Rennie becomes bored with Moorehead while Stanwyck does the same with Cortez, who attempts to rekindle their old relationship. The separation makes Rennie and Stanwyck realize how much they love each other. They resume their relationship, initially as a courtship. Eventually, they move back in together as husband and wife.

==Cast==
* Barbara Stanwyck as Anne Vincent  James Rennie as Dick Ives II
* Ricardo Cortez as Price Baines
* Natalie Moorhead as Margie True Charles Butterworth as Georgie Evans
* Claude Gillingwater as Ives Sr. 
* Joan Blondell as Helen Duckie Childers
* Hazel Howell as Girl at the Bridal Shower
* Lucille Ward as Susan - Annes Maid  
* Barbara Weeks as Girl at the Bridal Shower

==Production==
===Soundtrack===
*"When Love Comes in the Moonlight" Played during the credits (from the Warner Bros. musical Oh Sailor Behave).
*"Maybe Its Love" Sung by James Rennie and Barbara Stanwyck (from the Warner Bros. musical Maybe Its Love).
*"Looking for the Lovelight in the Dark" Played on the radio (from the Warner Bros. musical Top Speed).
*"Pretty Little You" Played on the radio (from the Warner Bros. film Son of the Gods). On with the Show).
*"Get Happy" Played in nightclub

==Preservation==
The film survives intact and has been shown on television and cable.

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 