A Rhapsody in Black and Blue
 
Rhapsody in Black and Blue is a short ten minute film that was created and released in 1932, starring Sidney Easton "A Rhapsody in Black and Blue." IMDb. IMDb.com, n.d. Web. 08 Mar. 2013.  and Fannie Belle DeKinght.   It is an early example of a "music video", showcasing the tunes I’ll Be Glad When You Are Dead You Rascal You., and Shine,  sung and played by well-known jazz artist Louis Armstrong. The screenplay was written by Phil Cohan.

==Plot==
A Husband who would rather listen to jazz and drum on pots and pans than mop the floor is whacked over the head with the mop by his wife when she hears him listening to I’ll Be Glad When You Are Dead You Rascal You.. He falls into a dream in which he is the king of Jazzville,  sitting on a royal throne with servants to fan him.  In the dream Louis Armstrong plays and sings jazz for him while dressed in a leopard print cave man outfit.  When he wakes up and sees his flustered wife still standing over him, he smiles and breaks a vase over his own head.

==Critical response==
 
The racism in this film was appalling and offensive to Black America but as stated by Krin Gabbard,

::“Joe Glaser   seized any opportunity to find work for Armstrong, and if Glaser made no effort to ask if the movies were good for the Negro people, neither did Armstrong”. Gabbard, Krin. Jammin at the Margins: Jazz and the American Cinema. Chicago: University of Chicago, 1996. Print. 
In the book Jammin’ at the Margins, Krin Gabbard quotes Miles Davis saying in his autobiography,

::"...some of the images of black people that I would fight against all through my career. I loved Satchmo, but I couldn’t stand all that grinning he did”. 

Phil Cohan tried to portray Armstrong’s role in the film as degrading, but instead Louis decided to embrace his role, and he played his trumpet and sang just as he would any other night with power and authority owning every word he sang.  

==References==
 

 
 
 
 
 
 