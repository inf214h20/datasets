Who Shot Patakango?
{{Infobox film
| name           = Who Shot Patakango?
| image          = Who Shot Patakango?.jpg
| image_size     = 
| alt            = 
| caption        = 
| director       = Robert Brooks
| producer       = 
| writer         = Halle Brooks (writer) Robert Brooks (writer)
| narrator       = 
| starring       = 
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 
| country        = 
| language       = 
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
Who Shot Patakango?, a.k.a. Who Shot Pat? is a 1989 movie starring Sandra Bullock.
==Plot== The Outsiders.

==Cast==
David Edwin Knight as Bic Bickham 
Sandra Bullock as Devlin Moran 
Kevin Otto as Mark Bickham 
Aaron Ingram as Cougar 
Brad Randall as Patakango 
Chris Cardona as Freddie 
Michael Puzzo as Goldie 
Christopher Crean as Tony 
Gregg Marc Miller as Vinnie 
Damon Chandler as Mr. Donnelly 
Bridget Fogle as Mitsy 
Phil Rosenthal as Principal 
Clint Jordan as Ricky (Dick) 
Ella Arolovi as Marianna 
Nicholas Reiner as Carmen 
Henry Paul as Dice Player 
Allison Janney as Miss Penny

==References==
* 

 
 
 