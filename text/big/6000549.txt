The Razor's Edge (1946 film)
{{Infobox film
| name           = The Razors Edge
| image          = Razors_edge.jpg
| caption        = Original film poster artwork by Norman Rockwell
| director       = Edmund Goulding
| producer       = Darryl F. Zanuck
| based on       =  
| writer         = Lamar Trotti Darryl F. Zanuck (uncredited) John Payne Herbert Marshall Anne Baxter Clifton Webb Alfred Newman Edmund Goulding (uncredited)
| cinematography = Arthur C. Miller
| editing        = J. Watson Webb, Jr.
| distributor    = 20th Century Fox
| released       =  
| runtime        = 145 minutes
| country        = United States
| language       = English
| budget         = $1.2 million
| gross          = $5 million (est. US/ Canada rentals) 
}}
 1944 novel. John Payne, Anne Baxter, Clifton Webb, Herbert Marshall, supporting cast Lucile Watson, Frank Latimore and Elsa Lanchester. Marshall plays Somerset Maugham. The film was directed by Edmund Goulding.

The Razors Edge tells the story of Larry Darrell, an American pilot traumatised by his experiences in World War I, who sets off in search of some transcendent meaning in his life. The story begins through the eyes of Larrys friends and acquaintances as they witness his personality change after the War. His rejection of conventional life and search for meaningful experience allows him to thrive while the more materialistic characters suffer reversals of fortune.
 Best Picture, Best Actress in a Supporting Role.

==Plot==
The film, in which W. Somerset Maugham (Herbert Marshall) is himself a minor character, drifting in and out of the lives of the major players, opens at a party held  following World War I in 1919 at a country club in Chicago, Illinois.  Elliott Templeton (Clifton Webb), an expatriate, has returned to the United States for the first time since before the war to visit his sister, Louisa Bradley (Lucile Watson), and his niece, Isabel (Gene Tierney), engaged to be married to Larry Darrell (Tyrone Power), of whom Elliott strongly disapproves for rejecting both inclusion in their social stratum and working in the common world.
 John Payne), a millionaire who is hopelessly in love with Isabel, too. Larry and Isabel agree to postpone their marriage so that he can go to Paris to try to clear his muddled thoughts. Meanwhile, Larry’s childhood friend, Sophie Nelson (Anne Baxter), settles into a happy marriage with Bob MacDonald (Frank Latimore), only to lose him and their baby in a tragic car accident.
 defrocked priest, Kosti (Fritz Kortner), urges him travel to India to learn from a mystic. Larry studies at a monastery in the Himalayas under the tutelage of the Holy Man (Cecil Humphreys), then makes a lone pilgrimage to the mountaintop where he finds Enlightenment (spiritual)|enlightenment. The Holy Man tells Larry to return to the world to share what he now knows about life.
 sold short" hypnotic suggestion.  Later, while slumming at a disreputable nightclub, they encounter Sophie, now a drunkard. Larry undertakes Sophies reformation, and out of lofty motives arranges to marry her; but when he tells Isabel, who is still in love with him, she plots to prove to Larry that Sophies reform is only temporary.  She successfully tempts Sophie back into drinking and Sophie disappears. Larrys last endeavour to reclaim his childhood companion from her depravity and despair proves fruitless. Sophie is murdered and her death reunites Larry and Maugham during the police investigation.
 Midwesterner like Elliott. Larry persuades Miss Keith (Lanchester), her social secretary, to allow him to use a blank invitation to counterfeit one for Elliott. Isabel inherits her uncles fortune, which she can use to underwrite Grays attempt to rebuild his fathers bankrupt brokerage. Larry refuses to reconcile with Isabel, deducing that she caused Sophies return to drinking, and ultimately, her murder. Instead he decides to work his way back to America aboard a tramp steamer.  Maugham tries to console Isabel with the knowledge that Larry is happy because he has found in himself the quality of true "goodness."

==Cast==
  
* Tyrone Power as Larry Darrell
* Gene Tierney as Isabel Bradley John Payne as Gray Maturin
* Anne Baxter as Sophie MacDonald
* Clifton Webb as Elliott Templeton
* Herbert Marshall as W. Somerset Maugham
 
* Lucile Watson as Louisa Bradley
* Frank Latimore as Bob MacDonald
* Elsa Lanchester as Miss Keith
* Cecil Humphreys as the Holy Man
* Fritz Kortner as Kosti
 

==Production history== service in Marines in January 1946.

Zanuck originally hired George Cukor to direct, but creative differences led to Cukors removal. Although Maugham wanted his friend (whom he had in mind when he created the character) Gene Tierney for Isabel,  Zanuck chose Maureen OHara but told her not to tell anyone. As OHara recounted in her autobiography, she shared the secret with Linda Darnell, but Zanuck found out, fired OHara, and hired Tierney.  Betty Grable and Judy Garland were originally considered for the role of Sophie before Baxter was cast. Maugham wrote an early draft of the screenplay but it is unknown how much of his version, if any, was used in the final script.
 Academy Awards==
;Wins    Best Actress in a Supporting Role: Anne Baxter
;Nominations Best Motion Picture: 20th Century-Fox Best Actor in a Supporting Role: Clifton Webb
*  ,  , Paul S. Fox

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 