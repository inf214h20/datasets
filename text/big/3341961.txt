Sansho the Bailiff
 
{{Infobox film
| name           = Sansho the Bailiff
| image          = Sansho Dayu poster.jpg
| caption        = Japanese theatrical release poster
| director       = Kenji Mizoguchi
| producer       = Masaichi Nagata
| writer         = Fuji Yahiro Yoshikata Yoda Mori Ōgai (story)
| narrator       =
| starring       = Kinuyo Tanaka Yoshiaki Hanayagi Kyōko Kagawa Eitarō Shindō
| music          = Fumio Hayasaka Tamekichi Mocizuki Kinshichi Kodera
| cinematography = Kazuo Miyagawa
| editing        =
| distributor    = Daiei Film
| released       =  
| runtime        = 124 minutes
| country        = Japan
| language       = Japanese
| budget         =
| gross          =
}} Japanese jidaigeki|period film directed by Kenji Mizoguchi. Based on a short story of the same name by Mori Ōgai, it tells the story of two aristocratic children sold into slavery. It is often considered one of Mizoguchis finest films, along with Ugetsu and The Life of Oharu.    It bears his trademark interest in freedom, poverty and womans place in society, and features beautiful images and long and complicated shots. The director of photography for this film was Mizoguchis regular collaborator Kazuo Miyagawa.

In the United Kingdom and Republic of Ireland|Ireland, it is known by its Japanese title Sanshō Dayū.   

==Plot== Minister of the Right, is administered by the eponymous Sanshō (Eitarō Shindō), a bailiff (or Stewardship|steward). Sanshōs son Tarō (Akitake Kōno), the second-in-charge, is a much more humane master, and he convinces the two they must survive in the manor before they can escape to find their father.

The children grow to young adulthood at the slave camp. Anju (Kyōko Kagawa) still believes in the teachings of her father, which advocate treating others with humanity, but Zushiō (Yoshiaki Hanayagi) has repressed his humanity, becoming one of the overseers who punishes other slaves, in the belief that this is the only way to survive.

Anju hears a song from a new slave girl from Sado which mentions her and her brother in the lyrics. This leads her to believe their mother is still alive. She tries to convince Zushiō to escape, but he refuses, citing the difficulty and their lack of money.

Zushiō is ordered to take Namiji, an older woman, out of the slave camp to be left to die in the wilderness due to her sickness.  Anju accompanies them, and while they break branches to provide covering for the dying woman they recall their earlier childhood memories.  At this point Zushiō changes his mind and asks Anju to escape with him to find their mother. Anju asks him to take Namiji with him, convincing her brother she will stay behind to distract the guards. Zushiō promises to return for Anju. However, after Zushiōs escape, Anju commits suicide by walking into a lake, drowning herself so that she will not be tortured and forced to reveal her brothers whereabouts.

After Zushiō escapes in the wilderness, he finds his old mentor, Tarō - Sanshōs son - at an Imperial temple. Zushiō asks Tarō to take care of Namiji, who is recovering after being given medicine, so that he can go to Kyoto to appeal to the Chief Advisor on the appalling conditions of slaves. Tarō writes him a letter as proof of who he is.

Although initially refusing to see him, the Chief Advisor realizes the truth after seeing a statuette from Zushiō. He then tells Zushiō that his exiled father died the year before and offers Zushiō the post of the governor of Tango, the very province where Sanshos manor is situated in.

As Governor of Tango the first thing Zushiō does is to order an edict forbidding slavery both on public and private grounds. No one believes he can do this, since Governors have no command over private grounds; although Sanshō offers initial resistance (having his men destroy the signs which state the edict), Zushiō orders him and his minions arrested, thus freeing the slaves. When he looks for Anju among Sanshōs slaves, he finds out his sister has sacrificed herself for his freedom. The manor is burned down by the ex-slaves, while Sanshō and his family are exiled. To appease the Ministry for doing something so radical, Zushiō resigns immediately afterwards, stating that he had done exactly what he had intended to do.

Zushiō leaves for Sado where he searches for his aged mother, who he believes is still a courtesan.  After hearing a man state that she has died in a tsunami, he goes to the beach she is supposed to have died on. He finds a nearly blind, decrepit old woman sitting on the beach singing the same song he heard years before. Realizing she is his mother, he reveals to her his identity, but Tamaki assumes he is a trickster until he gives her their statuette.  Zushiō tells her both Anju and their father have died, and apologizes for not coming for her in the pomp of his governors post.  Instead he followed his fathers proverb and chose mercy toward others (rather than the temporal glories of the world) by freeing the slaves held by Sanshō, among other kind deeds.  He tells his mother he has been true to his fathers teachings.  The film ends with her poignant acknowledgement.

== Cast ==
*Kinuyo Tanaka - Tamaki
*Kyōko Kagawa - Anju
*Eitarō Shindō - Sanshō
*Yoshiaki Hanayagi - Zushiō
*Ichirō Sugai - Minister of Justice Chief Advisor to the Emperor Morozane Fujiwara
*Masahiko Tsugawa - Zushiō as a Boy
*Masao Shimizu - Masauji Taira
*Chieko Naniwa - Ubatake
*Kikue Mori - Priestess
*Akitake Kōno - Tarō
*Ryōsuke Kagawa - Ritsushi Kumotake

==Reception==
Sansho was the last of Mizoguchis films to win an award at the Venice Film Festival, which brought him to the attention of Western critics and film-makers. It is greatly revered by many critics; The New Yorker film critic Anthony Lane wrote in his September, 2006 profile on Mizoguchi, "I have seen Sansho only once, a decade ago, emerging from the cinema a broken man but calm in my conviction that I had never seen anything better; I have not dared watch it again, reluctant to ruin the spell, but also because the human heart was not designed to weather such an ordeal."   Rogerebert.com extolled the movie: "I dont believe theres ever been a greater motion picture in any language. This one sees life and memory as a creek flowing into a lake out into a river and to the sea."  

==Stage production== The Thin Red Line) commissioned director Terrence Malick to write a stage play based on Sansho the Bailiff.  A private workshop of the play was undertaken in fall 1993 at the Brooklyn Academy of Music.  It was directed by Andrzej Wajda with sets and costumes by Eiko Ishioka, lighting by Jennifer Tipton, sound by Hans Peter Kuhn, choreography by Suzushi Hanayagi, and a large all-Asian cast.  A smaller-scale workshop was mounted by Geisler-Roberdeau under Malicks own direction in Los Angeles in spring 1994.  Plans to produce the play on Broadway were postponed indefinitely.

==Release==

===Home media=== Gion Bayashi. Masters of Cinema re-released the single film in Blu-ray and DVD in a Dual Format combo in April 2012. 

==References==
 

==Further reading==
* {{cite book
 | last1 = Andrew
 | first1 =Dudley
 | author-link =  Dudley Andrew
 | last2 = Cavanaugh
 | first2 = Carole
 | year = 2000
 | title = Sanshō dayū
 | publisher = British Film Institute
 | isbn = 0-85170-541-3
}}

==External links==
*  
*  
*  
*  
*   and QuickTime trailer
*   essay by Mark Le Fanu

 

 
 
 
 
 
 
 
 
 
 
 
 