Fashion King (film)
{{Infobox film name           = Fashion King image          =  director       = Oh Ki-hwan producer       = Cha Seung-jae writer         = Yoon In-wan based on       =    starring       = Joo Won   Choi Sulli   Ahn Jae-hyun   Park Se-young music          = Lee Jun-oh   cinematography =    editing        =  distributor    = Next Entertainment World released       =   runtime        = 114 minutes country        = South Korea language       = Korean budget         = gross          =
}}
Fashion King ( ) is a 2014 South Korean film that comically depicts the coming of age of a high school student as he matures into adulthood and discovers a passion for fashion design.    
 webtoon series Fashion King written by Kian84 (whose real name is Kim Hee-min), which was published on Naver from May 5, 2011 to June 6, 2013, and received four million Hit (Internet)|hits. 

==Plot==
Woo Ki-myung, a completely ordinary high school student, has a crush on Hye-jin, the prettiest, most popular girl in class. So to win her heart, he decides to transform his image and become the coolest, best dressed person in the world. As his mentor Nam-jung introduces him to the world of fashion, Woo-myung finds himself entering into a rivalry with Won-ho, the schools toughest fighter. Meanwhile, he doesnt notice Eun-jin, who harbors a secret crush on him.

==Cast==
*Joo Won as Woo Ki-myung
*Choi Sulli as Kwak Eun-jin
*Ahn Jae-hyun as Kim Won-ho
*Park Se-young as Park Hye-jin
*Kim Sung-oh as Kim Nam-jung
*Lee Il-hwa as Ki-myungs mother
*Shin Ju-hwan as Kim Chang-joo
*Min Jin-woong as Kim Doo-chi
*Kim Ian as Hyuk-soo
*Park Doo-sik as Sung-chul
*Woo Sang-jeon as Old monk
*Lee Geung-young as Won-hos father (cameo appearance|cameo) Han Hye-jin as Herself, MC (cameo)
*Hong Seok-cheon as Himself, MC (cameo)
*Kim Na-young as Herself, MC (cameo)
*Horan (Clazziquai) as Female assistant (cameo)
*Lee Joo-young as Fashion King Korea judge (cameo)
*Jung Doo-young as Fashion King Korea judge (cameo)
*Oh Se-il as Fashion King Korea judge (cameo)
*Ahn Sun-young as Hanbok judge (cameo)
*Lee Hyo-jae as Hanbok judge (cameo)
*Hwang Byung-gook as Ki-myungs homeroom teacher (cameo) Nana as Kim Hae-na (cameo) 

==References==
 

==External links==
*   
* 
* 
* 
*  at Naver  

 
 
 
 
 