Wend Kuuni
{{Infobox film
| name           = Wend Kuuni
| image          = WendKuuni.jpg
| image_size     = 
| caption        = 
| director       = Gaston Kaboré
| producer       = 
| writer         = Gaston Kaboré {{Cite news
  | last = Canby
  | first = Vincent
  | author-link = Vincent Canby
  | title = Wend Kuuni
  | newspaper = New York Times
  | date = 1983-03-27
  | url = http://query.nytimes.com/gst/fullpage.html?res=9507EFDA103BF934A15750C0A965948260
  | accessdate =2008-01-17
  | postscript =  }}
 
| narrator       = 
| starring       = 
| music          = Rene B. Guirma 
| cinematography = 
| editing        = Andree Davanture 
| distributor    = California Newsreel (USA)
| released       = March 27, 1983 (USA)
| runtime        = 75 minutes
| country        = Burkina Faso More
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
Wend Kuuni (also known as Gods Gift) is a 1983 Burkina Faso|Burkinabé drama film directed by Gaston Kaboré.

==Plot==
The movie starts out with a village leader coming into the house of a crying mother. He says to give up hope that your husband is still alive. Resisting from the comments, the mother decides to run away with her child.
 mute and Mossi village. The village leader comes and says that he is not from this village, but will raise him. The traveller thanks them and heads off.

The village chiefs decide to send a search party to find the boys parents. When no parents are found, Tinga agrees to adopt the boy, and names him "Wend Kuuni," because destiny brought the boy to his home.

At the house, Wend Kuuni had the job of herding the goats. He made friends with his stepsister Pougnere and was very happy living in this household.

Meanwhile, the audience witnesses a quarrel between one of the village elders (Bila) and his young wife. She accuses him of being impotent, and he calls her a witch. Tinga calms them both down and Bila later tells Tinga that the fight has been settled.

That day, Wend Kuuni accidentally left his knife in the field where the goats were grazing. When he went back that night to retrieve it, he finds Bila hanging from a tree branch. In that instant, he remembers the death of his mother after they had been chased out of the village, and he also remembers how to speak. He screams "mother!" before running back to the village to inform the others.

At the end of the film, Wend Kuuni recounts his story to Pougnere. He recalls his sick mother being chased out of their village, and ending up under a tree in the middle of the field. When he woke up, his mother was dead. He spoke of running for hours, falling asleep, and waking up to the traveler finding him.

The character comes back in Buud Yam

 {{cite web
  | last = Blaise
  | first = Judd
  | title = Wend Kuuni Plot Synopsis
  | publisher = Allmovie
  | url = http://www.allmovie.com/cg/avg.dll?p=avg&sql=1:151893~T0
  | accessdate =2008-01-17 }}  {{cite web
  | title = Wend Kuuni
  | publisher = California Newsreel
  | url = http://www.newsreel.org/nav/title.asp?tc=CN0105
  | accessdate =2008-01-17 }} 

==Cast==
*Serge Yanogo as Wendkouni
*Rosine Yanogo as Pognere
*Joseph Nikiema as Tinga
*Colette Kaboré as Lale
*Simone Tapsoba as Koudbila
*Yaya Wima as Bila
*Martine Ouedraogo as Timpoko
*Boucare Ouedraogo as Razougou

==Awards==
In 1985, Wend Kuuni won the César award for best French language film and in 1986 it won the Distribution Help Award at the Fribourg International Film Festival.

==References==
 

==External links==
* 
*  
* 
 

 
 
 
 
 