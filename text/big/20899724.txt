Griffin and Phoenix (2006 film)
{{multiple issues|
 
 
}}

{{Infobox Film
| name           = Griffin and Phoenix
| image          = Griffin&Phoenix.jpg
| caption        = Promotional film poster
| director       = Ed Stone
| producer       = Sidney Kimmel,  Paul Brooks,  Jason Blum
| writer         = John Hill
| starring       = Dermot Mulroney  Amanda Peet  Sarah Paulson   Blair Brown
| music          = Roger Neill
| cinematography = David M. Dunlap
| editing        =
| studio         = Blumhouse Productions
| distributor    = Gold Circle Films 2006
| runtime        = 102 minutes
| country        = US English
| budget         = $500,000
| gross          = $1,355,967
}} 2006 romance Griffin and Phoenix. Since 2007, the film remake has been shown periodically on the Lifetime Movie Network.

==Plot==
The film starts with Henry Griffin (Dermot Mulroney) learning from his oncologist that his cancer has spread and that he only has one year, two at most, left to live. This discovery starts him on a journey of self-reflection which includes starting a novel and taking a psychology class at New York University. In this class, he meets Sarah Phoenix (Amanda Peet). He asks her on a date and after hesitation she comes. Then the two of them goof around and eventually stay up all night together watching the sunrise. Griffin believes they are close but Phoenix doesnt show as much interest. Although Phoenix sends Griffin the occasional mixed signals, the two team up for a whirlwind of adventurous and almost childlike activities. However Phoenix doesnt want to hurt Griffin or her self by falling in love because shes too weak to deal with it as she too has cancer and is also short of time. One day while at Griffins house after their adventure, while cleaning she comes across his books on dying and "living life to the fullest" and comes to the conclusion that Griffin knows shes ill and is being nice only out of sympathy. She storms out and after their confrontation Griffin explains that the books belong to him and he too has cancer. This causes the two to fall ever more deeply for one another, and their relationship deepens. Together, they strive to make the most of their last days of life. However while exploring and making the most of their last days, Griffin explains how when he gets seriously ill he will just leave and he wishes for her to not come looking for him because its something he feels he must do himself. But Phoenix is first to get sick and uses Griffins same words against him. This deeply hurts Griffin and causes him to overact. Later, however, after realizing he was wrong, he decides he wants to spend the last moments of Phoenixs life with her anyhow. The movie ends with Griffin making Christmas for Phoenix, as it is her favorite season and it would be their last holiday together.

==External links==
*  
*   at Gold Circles website
*   at Lifetimes website
*   at Rotten Tomatoes

 
 
 
 
 
 
 
 
 


 