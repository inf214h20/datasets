The Adventures of Rusty
{{Infobox film
| name           = The Adventures of Rusty
| image          = Adventures of Rusty.JPG
| image size     = 
| alt            = 
| caption        = 
| director       = Paul Burnford Rudolph C. Flothow
| writer         = Aubrey Wisberg Al Martin
| based on       = 
| screenplay     = 
| narrator       = 
| starring       = Ted Donaldson Margaret Lindsay Conrad Nagel
| music          = Marlin Skiles
| cinematography = L. William OConnell
| editing        = Reginald Browne
| studio         = Larry Darmour Productions
| distributor    = Columbia Pictures
| released       =  
| runtime        = 67 min
| country        = USA
| language       = English
| budget         = 
| gross          = 
}}

The Adventures of Rusty is a 1945 drama film, the first in the Rusty (film series)|"Rusty" series of childrens films. The series of eight films were made in the 1940s by Columbia Pictures with stories centered on Rusty a German shepherd dog. The film is notable for featuring the famous Ace the Wonder Dog as Rusty, the only appearance by Ace in the Rusty films.

==Plot==
The films plot involves Danny Mitchell (Ted Donaldson), a young kid in the American town of Lawtonville, who is grieving over the loss of his dog. He is also struggling to adjust to his new stepmother, Ann (Margaret Lindsay), and has a difficult relationship with his father (Conrad Nagel) - causing him to call on a psychiatrist (Addison Richards) for assistance. However, Danny befriends Rusty, a ferocious German shepherd who was brought to America from Germany during World War II. Having worked a police dog for the Gestapo, however, Rusty is ill-tempered and Danny struggles to train him. A subplot involves two Nazi saboteurs (Arno Frey and Eddie Parker) who arrive in Lawtonville, attempting to evade the Coast Guard and blow up an installation. They ultimately try to take Rusty by speaking to him in German language|German.

==Cast==
* Ted Donaldson as Danny Mitchell
* Margaret Lindsay as Ann Mitchell
* Conrad Nagel as Hugh Mitchell
* Gloria Holden as Louise Hover Robert Williams as Will Nelson
* Addison Richards as Dr. Banning, Psychiatrist
* Arno Frey as Tausig
* Eddie Parker as Ehrlich
* Ace the Wonder Dog as Rusty

===Cast notes===
The dog portraying Rusty would change frequently over the course of the series, with Flame having the most appearances as the character (four). Dannys parents would also be portrayed by different actors in almost every installment.

==See also==
* The Return of Rusty (1946) 64 minutes. Directed by William Castle
* For the Love of Rusty (1947) 68 minutes. Directed by John Sturges
* The Son of Rusty ((1947) 70 minutes. Directed by Lew Landers
* My Dog Rusty ((1948) 67 minutes. Directed by Lew Landers
* Rusty Leads the Way ((1948) 59 minutes. Directed by Will Jason
* Rusty Saves a Life ((1949) 68 minutes. Directed by Seymour Friedman
* Rustys Birthday ((1949) 61 minutes. Directed by Seymour Friedman

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 