Dog (film)
{{Infobox Film
| name = Dog
| image = Dog(film).jpg
| caption =
| director = Suzie Templeton
| producer = 
| writer = Suzie Templeton
| narrator =
| starring =
| music = 
| cinematography = 
| editing = Tony Fish
| distributor = 
| released = 2001
| runtime = 5 minutes 40 seconds
| country = United Kingdom
| language = English
| budget = 
| gross =
| preceded_by =
| followed_by =
}}
Dog is a British Academy of Film and Television Arts|BAFTA-winning stop motion animated short film written, directed and animated by Suzie Templeton. The film was made at the Royal College of Art in 2001.

== Synopsis ==
A young boy longs for reassurance about how his mother died. To protect each other, he and his father hold their agony inside, where it festers.

== Awards ==
*McLaren Award for New British Animation: 2001 Edinburgh International Film Festival, Scotland
*National Film Board of Canada Grand Prize (Student Competition Year): 2001 Ottawa International Animation Festival, Canada
*Cinewomen Award for Best Female Director: 2001 FAN International Short Film and Animation Festival, UK
*Best Animation: 2001 Royal Television Society London Centre Student Television Awards, UK
* , UK
*Paul Berry Award for Best Student Film: 2002 British Animation Awards, UK
*Best Experimental Animation: 2002 California SUN International Animation Festival, USA
*Best Animation: 2002 Royal Television Society National Student Television Awards, UK
*Best Animation: 2002 Brooklyn International Film Festival, USA  
*Grand Prix: 2002 Dervio International Cartoons and Comics Festival, Italy
*Best Overall Film: 2002 Melbourne International Animation Festival, Australia
*Best Student Animation: 2002 Palm Springs International Festival of Short Films, USA
*Special Mention: 2002 Regensburg Short Film Week, Germany
*Bronze Award: 2002 WorldFest-Houston International Film Festival, USA
*Hiroshima Prize: 2002 Hiroshima International Animation Festival, Japan
*Winner of Professional Category: 2002 D&AD/Campaign Screen New Directors Competition, England
*Anyzone Award: 2002 Holland Animation Film Festival, The Netherlands
*Grand Prix - Narrative Film: 2002 Holland Animation Film Festival, The Netherlands
*Grand Prix: 2002 Siena International Short Film Festival, Italy
*Best Story: 2002 Black Nights Film Festival, Tallinn, Estonia
*Best Animation: 2003 Tampere Film Festival, Finland

==External links==
* 
* 

 
 
 
 
 
 
 


 