The Light Shines Only There
 
{{Infobox film
| name           = The Light Shines Only There
| image          = The_Light_Shines_Only_There.jpg
| caption        = Original Japanese poster.
| director       = Mipo O
| producer       = Hideki Hoshino
| writer         = Yasushi Sato (original novel) Ryo Takada
| starring       = Go Ayano Chizuru Ikewaki Masaki Suda  
| music          = Takuto Tanaka
| cinematography = 
| editing        = Etsuko Kimura
| distributor    = 
| released       =  
| runtime        = 90 minutes
| country        = Japan
| language       = Japanese
| budget         = 
}}
 Best Foreign Language Film at the 87th Academy Awards, but was not nominated.   

==Cast==
* Go Ayano as Tatsuo Sato
* Chizuru Ikewaki as Chinatsu Ohshiro
* Masaki Suda as Takuji Ohshiro
* Kazuya Takahashi as Nakajima
* Shohei Hino as Matsumoto
* Hiroko Isayama as Kazuko Ohshiro
* Taijiro Tamura as Taiji Ohshiro

==Plot==
In a Japanese port town, Tatsuo Sato (Go Ayano), a traumatized man, spends his days drifting aimlessly and his nights drinking himself to oblivion. Whiling his hours away at a pachinko parlor, he meets Takuji Ohshiro (Masaki Suda), a young man on parole who impulsively invites him to a shabby house on the outskirts of town. There, Tatsuo glimpses Takuji’s bedridden father and callous mother, and meets his world-weary older sister Chinatsu Ohshiro (Chizuru Ikewaki). While immediately drawn to each other, romance is an unaffordable luxury for the emotionally closed-off Tatsuo and the disillusioned Chinatsu, who sells herself to provide for her family and keep her brother out of jail. As Tatsuo and Chinatsu take tentative steps towards a relationship, the happy-go-lucky Takuji latches onto Tatsuo, binding their fates. Each step they take to build a better life sets off a chain of actions that have devastating consequences. 

==Awards==
* Montreal World Film Festival: Best Director (Mipo O)  
* Raindance Film Festival: Best International Feature  
* Tama Cinema Forum: Best Actress (Chizuru Ikewaki) Best New Actor (Masaki Suda) 
*Kinema Junpo #1 film of 2015  

==Reception==

The Hollywood Reporter was extremely positive about the film, singling out the director, lead actors, screenplay, and cinematography for praise and noting, "talented director Mipo Oh plunges into a fierce character study of three young people on the way down".  In her roundup of films nominated for Best International Feature at  the UKs Raindance Film Festival, Becca Spackman of Critics Associated wrote, "An exploration into humanity and dependent tendencies, streaked with heartbreak and loss, this beautiful entry from Japan will definitely strike a chord with both audiences and critics alike."  (The film later won the award.) 

The film received positive responses from Japans English-language newspapers. In his first review, Mark Schilling of The Japan Times singled out Chizuru Ikewakis performance, noting, "As Chinatsu, Ikewaki’s performance is at once complex and transparent, drilling down into the essence of her character’s longing and self-loathing, her capacity for love and her longing for oblivion."  In a second review for The Japan Times, Schilling wrote, "this romantic drama has deservedly been named as Japan’s nominee for an Academy Award for Best Foreign Language Film."  Meanwhile, Don Brown of The Asahi Shimbun wrote of director Mipo O, "With her third and latest feature, The Light Shines Only There, she has earned her spot as one of Japan’s most promising directing talents." 

==See also==
* List of submissions to the 87th Academy Awards for Best Foreign Language Film
* List of Japanese submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*    
*   (English)
*  
*   (English)
*   (English)
{{Navboxes title = Awards list =
 
 
}}
 
 
 
 
 
 
 