Georgia, Georgia
 
{{Infobox film
| name           = Georgia, Georgia
| image          = 
| caption        = 
| director       = Stig Björkman
| producer       = Jack Jordan
| writer         = Maya Angelou
| starring       = Dirk Benedict
| screenplay     = Maya Angelou
| music          = 
| soundtrack     = Maya Angelou
| cinematography = Andreas Bellis
| editing        = Sten-Göran Camitz
| distributor    = 
| released       =  
| runtime        = 91 minutes
| country        = Sweden United States
| language       = English
| budget         = 
}}

Georgia, Georgia is a 1972 Swedish-American drama film directed by Stig Björkman. It was entered into the 23rd Berlin International Film Festival.    Its screenplay, written by Maya Angelou, is the first screenplay written by a black woman;  Angelou also wrote the films soundtrack, despite having very little additional input in the making of the film.  

==Cast==
* Dirk Benedict as Michael Winters
* Lars-Erik Berenett as Reception Clerk
* Stig Engström as Lars
* James Thomas Finley Jr. as Jack
* Roger Furman as Herbert Thompson
* Minnie Gentry as Mrs. Alberta Anderson
* Tina Hedström as Waitress
* Randolph Henry as Gus
* Diana Kjær as Birgit
* Vibeke Løkkeberg as Guest
* Diana Sands as Georgia Martin
* Artie Sheppard as Scottie
* Terry Whitmore as Bobo

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 
 
 
 