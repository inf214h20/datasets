Cuckoo (2014 film)
{{Infobox film
| name           = Cuckoo
| image          = Cuckoo 2014.jpg
| alt            =  
| caption        = Theatrical release poster
| director       = Raju Murugan
| producer       = 
| writer         =  Dinesh Malavika
| music          = Santhosh Narayanan 
| cinematography = P. K. Varma
| editing        = Shanmugam Velusamy 
| studio         = Fox Star Studios  
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Tamil
| budget         =  
| gross          =  
}}
 Dinesh and Malavika Nair as a visually challenged pair, while Aadukalam Murugadoss plays a pivotal role. The film was released on March 21, 2014 and received generally positive reviews.

==Plot==
Tamizh is a visually-impaired young man who works as a singer for an entertainment troupe. During one of the weddings he performs at, he meets Sudhanthirakodi, a young woman, who is studying to become a teacher and is also visually-impaired. Tamizh teases her, causing her to leave him waiting for her as she presents her wedding gift to the newly weds and leaves. The following day, he bumps into her on the train and asks his friend to mislead her into getting down at a wrong station. As a result, Kodi is late for her first day of teaching practice. She and her friends then go after whoever who tricked her. The next time he gets on the train, Tamizh candidly talks to his friend about how they played a prank on Kodi the other day, not realizing she is right in front of him. Angered, she hits him with her walking stick, causing him to bleed. Tamizh is embarrassed and is angry when his friends tease him about the incident. However, Kodi feels bad for what she did and apologizes to him the next time they meet. As their friendship grows deeper, Tamizh falls in love with Kodi. With the help of his friends, he composes a romantic poem for her which he records in a CD. When he goes to meet her at her school, he slips the CD into her bag, only to learn from her best friend that Kodi dreams of marrying a person who can see. She has always hoped that Vinod, the young man who volunteers reading to her at the service center will propose to her. On that very day, Vinod takes Kodi out for lunch to show her a surprise. Thinking that he is going to propose, she happily follows. However, she is heartbroken when Vinod introduces her to his fiance, who plans to make Kodi her charity case. When she returns home, she is surprised to find Tamizhs CD and is touched when she listens to it. Later, Tamizh goes through much difficulty to find Kodis favourite clock with her late fathers voice recording as the alarm. Realizing how much Tamizh is in love with her, Kodi finally learns to love him back. However, her greedy elder brother had promised to marry her off to someone else in return for a favour, forcing her and Tamizh to elope. Tamizh manages to obtain enough money to pay off Kodis brother, but he is tricked by a group of corrupt policemen before being hit by an oncoming van. Unknown to him, Kodi is also in the van on the way to meet him. As they are both silent throughout the journey, neither of them realize the other is also in the van. When Tamizh has finally recovered from the accident, he finds that Kodi has left Chennai to stay away from her brothers family. After a period of time, a reporter friend tells him Kodi was seen in Mumbai. Tamizh goes to the city in search of his one true love and they are finally united.

==Cast==
* Dinesh Ravi as Thamizh
* Malavika as Sudhanthirakodi
* Aadukalam Murugadoss as Gavas
* Elango as Elango
* Nandhini as Sangeetha
* E. Ramdoss as Dakkar
* Vairabalan as Sekhar
* Ambed as Jileki
* Eeshwar Chandrababu as Kuberan
* Swathi Shanmugam as Kanakavalli
* MGR Chinnavan
* Ajith Bala as Senthil
* Vijay Dass
* Raju Murugan as himself
* N. Linguswamy as himself (cameo appearance)

==Production==
Fox Star Studios in association with The Next Big Film Productions launched the film in July 2013, marking the debut of director Raja Murugan,   who had previously assisted Linguswamy in Bheema (film)|Bheema (2008) and Paiyaa (2010) and also worked as a writer for Ananda Vikatan magazine.  Nandita Swetha was initially selected to portray the character of Sudhanthirakodi,  before she was replaced by Malayalam newcomer Malavika Nair. The team completed the making of the film by November 2013, with Dinesh revealing his satisfaction on how the film shaped up.  Actor Kamal Haasan revealed the first look of the film in January 2014, with a teaser trailer released thereafter. 

==Soundtrack==
{{Infobox album Name = Cuckoo Type = Soundtrack
|Artist= Santhosh Narayanan Cover =  Caption = Released = 17 February 2014 Genre = Feature film soundtrack Length = Language = Tamil
|Label = Think Music Producer = Santhosh Narayanan Last album =   (2013) This album = Cuckoo (2014) Next album =   (2014) Reviews =
}} IANS wrote, "Santosh Narayanans life-affirming music makes you ignore fewer flaws you may across in the film".   
{{tracklist
| headline        = Track listing
| extra_column    = Singer(s)
| lyrics_credits   = yes
| total_length    =

| title1          = Enda Mapla
| extra1          = Gana Bala, Sathish, Dhee
| lyrics1         = Gana Bala, RK Sundar
| length1         = 04:22
| title2          = Manasula Soora Kaathey
| extra2          = Sean Roldan, Divya Ramani 
| lyrics2         = Yugabharathi
| length2         = 04:26
| title3          = Potta Pulla
| extra3          = Sean Roldan
| lyrics3         = Yugabharathi
| length3         = 04:15
| title4          = Agasatha
| extra4          = Kalyani Nair, Pradeep Kumar
| lyrics4         = Yugabharathi
| length4         = 05:02
| title5          = Kalyanamam Kalyanam
| extra5          = Andony Dasan
| lyrics5         = Yugabharathi
| length5         = 03:55
| title6          = Kodaiyila
| extra6          = Vaikom Vijayalakshmi, Kalyani Nair, Pradeep Kumar
| lyrics6         = Yugabharathi
| length6         = 04:03
}}

==Release==
The satellite rights of the film were sold to STAR Vijay.

== Critical reception ==
Cuckoo received generally positive reviews from critics.  Rediff gave it 4 stars out of 5 and wrote, "Cuckoo is a heartwarming love story between two visually challenged individuals that keeps you totally engrossed with its brilliant performances, sensational music and stunning visuals, definitely a must watch".  The Times of India gave it 3.5 stars out of 5 and wrote, "What is refreshing about the film is that it treats the disability of the lead characters in a matter-of-fact way and seldom resorts to manipulation....what makes the film feel unique is the setting and the array of interesting supporting characters that the director has created". However he added, "in the climatic portions, Dinesh...is made to go melodramatic..this change of tone makes the final half an hour of the movie feel cinematic".  Sify called it "a bold and daring film with a great first half which is lively and entertaining" and added "it is technically a well-made film though too heavy and melodramatic towards the climax. 

Indo-Asian News Service|IANS reviewer gave the film 3.5 stars and stated, "The reason I think Cuckoo is one of the better love stories of modern-day cinema is because it never takes advantage of its lead characters. It never expects or forces the audience to sympathize with Kodi or Tamizh at any given point in the film".  Behindwoods gave the film 3 stars out of 5 and stated, "The picturesque inventiveness of the technical team combined with naturally heartening performances by the cast places the film in the rack that has been reserved for good movies".  Baradwaj Rangan was more critical of the film, "you have a terrific premise. You’ve populated the narrative with interesting characters. You have great music. Now you need to pull together these building blocks and assemble a compelling movie. That’s where the problems begin". He went on to add, "The best stretch...is the film’s opening...Had the rest of Cuckoo worked at this level, we might have had a masterpiece". 

== References ==
 

==External links==
*  

 
 
 
 
 
 
 
 