Aletta Jacobs: Het Hoogste Streven
 
{{Infobox film
| name           = Aletta Jacobs: Het Hoogste Streven
| image          = 
| image_size     = 
| caption        = 
| director       = Nouchka van Brakel
| producer       = 
| writer         =Eugenie Jansen, Nouchka van Brakel
| narrator       = 
| starring       = 
| music          =   
| cinematography = 
| editing        = 
| distributor    = 
| released       = 1995
| runtime        = 65 minutes
| country        = Netherlands Dutch
| budget         = 
}} 1995 Netherlands|Dutch documentary film directed by Nouchka van Brakel.

==Cast==
*Luutgard Willems	... 	Aletta Jacobs
*Hans Kesting	... 	C.V. Gerritsen
*Max Arian	... 	Vader Jacobs
*Catherine ten Bruggencate	... 	Moeder Jacobs
*Truus te Selle	... 	Mademoiselle
*Edwin de Vries	... 	Professor Rosenstein
*Sacha Bulthuis	... 	Jeanette Broese van Groenou
*Anne Martien Lousberg	... 	Meretrix
*Monic Hendrickx	... 	Amsterdamse meretrix
*Hilt de Vos	... 	Belgische feministe
*Krijn ter Braak	... 	Hoogleraar
*Rijkent Vleeshouwer	... 	Hoogleraar
*Albert van Ham	... 	Hoogleraar
*Wigbolt Kruyver	... 	Huwelijksbeambte
*Juul Vrijdag	... 	Kiesvrouw

== External links ==
*  

 
 
 
 


 
 