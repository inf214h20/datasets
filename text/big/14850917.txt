Diane of the Follies
 
{{Infobox film
| name           = Diane of the Follies
| image          = 
| caption        = 
| director       = Christy Cabanne
| producer       = Fine Arts Film Company
| writer         = D. W. Griffith (as Granville Warwick)
| starring       = Lillian Gish
| cinematography = 
| editing        = 
| distributor    = Triangle Film Corporation
| released       =  
| runtime        = 50 minutes 
| country        = United States  Silent English intertitles
| budget         = 
}}

Diane of the Follies is a 1916 American drama film directed by  Christy Cabanne. The film is considered to be lost film|lost. 

==Cast==
* Lillian Gish as Diane
* Sam De Grasse as Phillips Christy
* Howard Gaye as Don Livingston
* Lillian Langdon as Marcia Christy
* Allan Sears as Jimmie Darcy (as A.D. Sears)
* Wilbur Higby as Theatrical Manager
* William De Vaull as Butler
* Wilhelmina Siegmann as Bijou Christy
* Adele Clifton as Follies Girl
* Clara Morris as Follies Girl
* Helen Wolcott as Follies Girl
* Grace Heins as Follies Girl

==See also==
* List of American films of 1916
* Lillian Gish filmography

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 
 

 