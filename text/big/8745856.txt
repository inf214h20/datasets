Fitzwilly
{{Infobox film
| name           = Fitzwilly
| image          = Fitzwilly 1967.jpg
| caption        = film poster by Frank Frazetta
| director       = Delbert Mann
| producer       = Walter Mirisch
| writer         = Screenplay:  
| based on       =  
| starring       = Dick Van Dyke Barbara Feldon Edith Evans John McGiver Harry Townes
| music          = John Williams
| cinematography = Joseph Biroc
| editing        = Ralph Winters
| studio         = The Mirisch Corporation
| distributor    = United Artists
| released       =  
| runtime        = 102 minutes
| country        = United States English
| budget         =
| gross          = $2,100,000 (US/ Canada) 
}}
Fitzwilly is a 1967 film directed by Delbert Mann, based on Poyntz Tylers 1960 novel A Garden of Cucumbers and adapted for the screen by Isobel Lennart.  Its title refers to the nickname of its protagonist, Claude Fitzwilliam, an unusually intelligent and highly educated mastermind of a butler played by Dick Van Dyke. The film co-stars Barbara Feldon in her first feature-film role.

==Story== heiress whose swindles — including the operation of the fictional charity and thrift shop, St. Dismas — to maintain "Miss Vicki" in the lifestyle to which she is accustomed.

The staffs secret operations threaten to unravel when Miss Woodworth hires an assistant, Juliet Nowell (Barbara Feldon), to assist with her creation of a dictionary that contains all possible phonetic misspellings of words.  Juliet is surprised to learn from Miss Vicki that Fitzwilly graduated with honors from Williams College, and she opines that he should be doing something more "worthy" than being a butler, like joining the Peace Corps.

After Juliet inadvertently and unknowingly foils several minor operations, Fitzwilly becomes determined to get rid of her.  He conceives a plan to court her in order to induce her to quit.  Unexpectedly, the two fall in love.  Still unaware of Fitzwillys secret life, Juliet does quit when Fitzwilly refuses to discuss ending his life in service.

Juliet stumbles upon evidence of Fitzwillys past crimes, and returns to the mansion to confront him.  Fitzwilly proposes marriage and agrees to end the criminal operations and tell Miss Vicki everything, but there is a problem: due to Juliets past interference, the household is $75,000 short of funds, and they have to raise the money by Christmas Day.  This leads to a complex setpiece in which the Woodworth staff orchestrates the robbery of Gimbels department store on Christmas Eve.

Although the operation is initially successful, one of the household, Albert (John McGiver), allows himself to be caught to "atone" for his sins.  He steadfastly refuses to implicate anyone else.  Miss Woodworth casually blackmails the assistant district attorney ("the son of my oldest friend") into engineering a suspended sentence on a lesser charge, and blithely offers to write a counter check to the store to cover the amount of the take.

Believing that the entire household is destined for prison, Fitzwilly uncomfortably toasts his and Juliets engagement with Juliet, her father (Harry Townes) and Miss Vicki.  His discomfort is alleviated when it is revealed that Miss Vickis dictionary has been rewritten as a screenplay, and sold to a Hollywood studio for $500,000.

==Soundtrack== Alan and Marilyn Bergman.
 The Long Goodbye, another Williams credit. 

==Cast==
 
*Dick Van Dyke - Claude Fitzwilliam
*Barbara Feldon - Juliet Nowell
*John McGiver - Albert
*Edith Evans - Miss Victoria Woodworth
*Harry Townes - Mr. Nowell
*John Fiedler - Mr. Morton Dunne  Anne Seymour - Grimsby 
*Norman Fell - Oberblatz
*Cecil Kellaway - Buckmaster
*Stephen Strimpell - Byron Casey
*Helen Kleeb - Mrs. Mortimer Paul Reed - Prettikin
*Albert Carrier - Pierre
*Nelson Olmsted - Simmons
*Dennis Cooney - Assistant D.A. Elliot Adams
*Noam Pitlik - Charles
*Anthony Eustrel - Garland
*Sam Waterston - Oliver
 

==Discography==

The CD soundtrack (including the re-recording album and the original complete soundtrack) composed by John Williams is available on Music Box Records label.

==See also==
* List of American films of 1967

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 