The Lifeguard
 
 
{{Infobox film
| name = The Lifeguard
| image = The lifeguard film.jpg
| image_size = 
| alt = 
| caption = movie poster
| director = Liz W. Garcia
| producer = Liz W. Garcia Milan Chakraborty Joshua Harto Mike Landry Ed McWilliams Carlos Velazquez
| writer = Liz W. Garcia David Lambert
| music = Fred Avril John Peters

| editing = Jennifer Davidoff Cook Elizabeth Kling
| studio = Attic Light Films C Plus Pictures Houndstooth La Pistola Productivity Media Wild Invention Focus World Screen Media Films
| released =  
| runtime = 98 minutes
| country = United States
| language = English
| budget = 
| gross = 
}} David Lambert. The film was released via video on demand on July 30, 2013, and received a limited release in theaters on August 30. 

==Plot==
Reporter Leigh London is a talented writer living in New York City. She is in a relationship with an engaged man and finds it hard to accept his situation. After becoming disillusioned with her life in the city, she decides to leave (without notifying her job) and go back to her hometown. Her mother questions why she has returned so suddenly but her father is delighted, especially after Leigh announces her decision to stay indefinitely. She becomes reacquainted with two of her former friends, art appraiser Todd and vice principal Mel. While having some drinks with them, she announces that shes taken up her old high school job of the lifeguards position of the community pool, where she meets the maintenance mans son Jason.

Over the next few weeks, she becomes friends with Jason and his best friend Matt, and encourages wild behavior on her two high school friends, hanging out with the teens and smoking pot. Todd happily goes along with Leighs behavior; while Mel does too, it takes its toll on her relationship with her husband as they are trying to conceive a baby. Leigh begins to spend a lot more time with Jason, and soon she realizes her attraction to him and vice versa. One night, after hanging out at the pool after hours with their friends, Leigh goes to the bathroom/changing room where Jason follows. They then kiss and immediately have sex on the sink basin. This starts a relationship between the two that continues over the summer, leading to frequent sexual encounters. One day while the two are having sex in bed, Leigh realizes shes falling in love with Jason and talks him into staying around longer instead of moving to Vermont with Matt as he had planned. Matt has been kicked out by his mother and has made it clear that he is desperate to leave town.

After Jason postpones the Vermont plans, Matt becomes very disheartened. Meanwhile, Mels husband becomes increasingly frustrated with her "carefree" behavior. While at the pool one morning, Todd discovers Leigh and Jason kissing but doesnt say anything. Leighs mother asked her to move out as she isnt the only one trying to get their life in order; Leigh stays with Jason. Her cat goes missing in the process. Todd lets it slip to Mel that Leigh is staying at Jasons and Mel discovers the relationship between the two, to her horror. She approaches Leigh in anger and plans to inform Jasons father. Leigh and Jason then go searching for her cat, only to discover Matt has committed suicide by hanging himself from a tree in the woods. Matt repeatedly mentions throughout the film that he hates life in the town and is desperate to get to Vermont. It seems Jasons postponement was his last bit of hope broken.

This takes its toll on Jason very hard and he is deeply distraught. Leigh takes the responsibility to inform Matts mother, who is also heartbroken at the news. Leigh consoles her until her relatives arrive. At the funeral, she makes her peace with Mel and goes to meet Jason one last time before they both leave. They share an emotional hug, both showing a strong affection for one another. She gives him $1,000 she won from a journalist prize, and walks away with tears in her eyes knowing the feelings she has for Jason could never truly be acted upon. The final scene ends with a postcard from Jay stating he still thinks about her a lot and Matt also. 

==Cast==
* Kristen Bell as Leigh London David Lambert as Little Jason
* Mamie Gummer as Mel
* Martin Starr as Todd Alex Shaffer as Matt
* Joshua Harto as John
* Amy Madigan as Justine
* John Finn as Big Jason
* Paulie Litt as Lumpy
* Sendhil Ramamurthy as Raj
* Adam LeFevre as Hans
* Mike Landry as Officer Miller
* Lisa Ann Goldsmith as Matts mother
* Terri Middleton as Matts Aunt

==Production== Fox Chapel Edgeworth and Leetsdale were also considered as locations. 

==Release==
The film competed at the 2013 Sundance Film Festival for Best Dramatic Film. 

==Critical reception==
After its premiere at Sundance, The Lifeguard received generally unfavorable reviews. Peter Debruge of  : "Not even a checklist of indie film attributes can inject a sense of originality into this familiar narrative."    Jordan Hoffman of Film.com gave the film a 2.7/10 and stated: "The Lifeguard is a painfully dull (alleged) drama utterly lacking in originality or self-awareness."   

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 
 