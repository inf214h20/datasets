A Serious Man
 
{{Infobox film
| name           = A Serious Man
| image          = Aseriousman.jpg
| caption        = Theatrical release poster
| alt            = A man standing on the roof of a house, looking off to his left. His hands are on his hips. Behind him is a TV aerial. Joel Coen Ethan Coen
| producer       =  
| writer         =  
| starring       =  
| music          = Carter Burwell
| cinematography = Roger Deakins
| editing        = Roderick Jaynes
| studio         =  
| distributor    = Focus Features
| released       =  
| runtime        = 106 minutes 
| country        = United States
| language       = English
| budget         = $7 million
| gross          =  $31,312,437   
}} dark comedy {{cite book
 | last = Booker
 | first = M. Keith
 | title = Historical Dictionary of American Cinema
 | publisher = Scarecrow Press
 | year = 2011
 | page = 75
 | url = http://books.google.co.uk/books?id=Y04MQEgHbZsC&printsec=frontcover#v=onepage&q&f=false Joel and National Board of Reviews Top 10 Film Lists of 2009, and a nomination for the Academy Award for Best Picture.

==Plot==

In an unnamed Eastern European shtetl, a Jewish man tells his wife that he was helped on his way home by Traitle Groshkover, whom he has invited in for soup. She says Groshkover is dead and must be a dybbuk. Groshkover (Fyvush Finkel) arrives and laughs off the accusation, but she plunges an icepick into his chest. Bleeding, he exits into the snowy night.

In Minnesota in the late 1960s, Larry Gopnik (Michael Stuhlbarg) is a professor of physics. His wife, Judith (Sari Lennick), tells him that she needs a Get (divorce document)|get (a Jewish divorce document) so she can marry widower Sy Ableman (Fred Melamed).
Their son Danny (Aaron Wolff) owes twenty dollars for marijuana to an intimidating Hebrew school classmate. He has the money, but it is hidden in a transistor radio that was confiscated by his teacher. Daughter Sarah is always washing her hair and going out. Larrys brother, Arthur (Richard Kind), sleeps on the couch and spends his free time filling a notebook with what he calls a "probability map of the universe".

Larry faces an impending vote on his application for tenure, and his department head (Ari Hoptman) lets slip that anonymous letters have urged the committee to deny him. Clive Park, a student worried about losing his scholarship, meets with Larry in his office to argue he should not fail the class. After he leaves, Larry finds an envelope stuffed with cash. When Larry attempts to return it, Clives father threatens to sue Larry either for defamation if Larry accuses Clive of bribery, or for keeping the money if he does not give him a passing grade.

At the insistence of Judith and Sy, Larry and Arthur move into a nearby motel. Judith empties the couples bank accounts, leaving Larry penniless, so he enlists the services of a divorce attorney (Adam Arkin). Larry learns Arthur faces charges of solicitation and sodomy.

Larry turns to his Jewish faith for consolation. He consults two rabbis (Simon Helberg and George Wyner) but his synagogues senior rabbi, Marshak, is never available. Larry and Sy are involved in separate, simultaneous car crashes. Larry is unharmed, but Sy dies. At Judiths insistence, Larry pays for Sys funeral. At the funeral, Sy is eulogized as "a serious man".
 bar mitzvah, unaware that his son is under the influence of marijuana. During the service, Judith apologizes to Larry for all the recent trouble and informs him that Sy liked him so much that he even wrote letters to the tenure committee. Danny finally meets with Marshak, who returns the radio and counsels Danny to "be a good boy".

Larrys department head compliments him on Dannys bar mitzvah and hints that he will receive tenure. The mail brings a large bill from Arthurs lawyer. Larry decides to pass Clive, whereupon Larrys doctor calls, asking to see him immediately about the results of a chest X-ray.  At the same moment, Dannys teacher struggles to open the emergency shelter as a massive tornado bears down on the school.

==Cast==
 
 
* Michael Stuhlbarg as Lawrence "Larry" Gopnik
* Richard Kind as Arthur Gopnik
* Sari Lennick as Judith Gopnik
* Fred Melamed as Sy Ableman
* Aaron Wolff as Danny Gopnik
* Jessica McManus as Sarah Gopnik
* Alan Mandell as Rabbi Marshak
* Adam Arkin as Don Milgram
* George Wyner as Rabbi Nachtner
* Amy Landecker as Mrs. Vivienne Samsky
* Katherine Borowitz as Mimi Nudell
* Allen Lewis Rickman as Velvel
* Yelena Shmulenson as Dora
* Fyvush Finkel as Traitle Groshkover
 
* Simon Helberg as Rabbi Scott Ginsler
* Andrew S. Lentz as Mark Sallerson
* Jack Swiler as Howard Altar (boy on bus)
* Tim Harlan-Marks as Hebrew school bus driver
* Benjy Portnoe as Ronnie Nudell
* Brent Braunschweig as Mitch Brandt
* Ari Hoptman as Arlen Finkle Michael Lerner as Solomon Schlutz
* David Kang as Clive Steve Park as Clives father
* Peter Breitmayer as Mr. Brandt
 

Open auditions for the roles of Danny and Sarah were held on May 4, 2008, at the Sabes Jewish Community Center in St. Louis Park, Minnesota, one of the scheduled shooting locations. Open auditions for the role of Sarah were also held in June 2008 in Chicago|Chicago, Illinois.    

==Production== rambler homes Brooklyn Center, Hopkins  before a suitable location was found in Bloomington, Minnesota|Bloomington.  The look of the film is partly based on the Brad Zellar book Suburban World: The Norling Photographs, a collection of photographs of Bloomington in the 1950s and 60s. 

Location filming began on September 8, 2008, in Minnesota. An office scene was shot at Normandale Community College in Bloomington, Minnesota|Bloomington. The film also used a set built in the schools library, as well as small sections of the second floor science building hallway. The synagogue is the Bnai Emet Synagogue (St. Louis Park, Minnesota)|Bnai Emet Synagogue in St. Louis Park. The Coen brothers also shot some scenes in St. Olaf Colleges old science building because of its similar period architecture.   Scenes were also shot at the Minneapolis legal offices of Meshbesher & Spence, the name of whose founder and president, Ronald I. Meshbesher, is mentioned as the criminal lawyer recommended to Larry in the film.  Filming wrapped on November 6, 2008, after 44 days, ahead of schedule and within budget. 

Despite many accurate recreations of the look and feel of period fashions, automobiles, and culture of 1967 Minnesota, there remain some anachronisms, including references to two albums, Carlos Santanas Abraxas (album)|Abraxas and Creedence Clearwater Revivals Cosmos Factory, that were released in 1970.

Longtime collaborator Roger Deakins rejoined the Coen brothers as cinematographer, following his absence from Burn After Reading. This was his tenth film with the Coen brothers. 
Costume designer Mary Zophres returned for her ninth collaboration with the directors.   

 The Coens themselves stated that the "germ" of the story was a rabbi from their adolescence: a "mysterious figure" who had a private conversation with each student at the conclusion of their religious education.  Joel Coen said that it seemed appropriate to open the film with a Yiddish folk tale, but as the brothers didnt know any suitable ones, they wrote their own. 

==Soundtrack==

All of the films original music is by Carter Burwell,  who also worked on every previous Coen Brothers film except O Brother, Where Art Thou?.  The film also contains pieces of Yiddish music including "Dem Milner’s Trern" by Mark Warshawsky and performed by Sidor Belarsky,    which deals with the abuse and recurring evictions of Jews from Shtetlekh. 

The soundtrack also includes the following songs by popular 1960’s artists:

{{tracklist extra_column = Artist title1 = Somebody To Love  extra1 = Jefferson Airplane length1 = 2:58 title2 = Today
|extra2 = Jefferson Airplane length2 = 3:02 title3 = Comin Back to Me extra3 = Jefferson Airplane length3 = 5:16 title4 = Machine Gun  extra4 = Jimi Hendrix length4 = 12:36
}}

==Release==
The film had a limited release on October 2, 2009, in the United States. It premiered at the Toronto International Film Festival {{cite web| title = A Serious Man premiere at the 2009 Toronto International Film Festival
| url = http://www.digitalhit.com/galleries/34/507 | year = 2009 | author = Evans, Ian | work = DigitalHit.com | accessdate =December 12, 2009 }}  on September 12, 2009. 

===Box office performance===
{| class="wikitable" style="width:100%;"
|-
! rowspan="2" | Film
!| Release date
! colspan="3" | Box office revenue
! colspan="2" text="wrap" | Box office ranking
! rowspan="2" style="text-align:center;"| Budget
! rowspan="2" style="text-align:center;"| Reference
|-
! United States
! United States
! International
! Worldwide
! All time United States
! All time worldwide
|-
| A Serious Man
| style="text-align:center;"| October 2009
| style="text-align:center;"| $9,228,768
| style="text-align:center;"| $22,201,566
| style="text-align:center;"| $31,430,334 
| style="text-align:center;"| #3,818
| style="text-align:center;"| Unknown
| style="text-align:center;"| $7,000,000 
| style="text-align:center;"| 
|}
As of February 10, 2010, it has had worldwide gross earnings of $31,312,437 

===Critical reception===
{| class="wikitable" style="width:99%;"
|-
! Film
! Rotten Tomatoes
! Metacritic
! Entertainment Weekly
|-
| A Serious Man
| style="text-align:center;"| 89% (208 reviews) 
| style="text-align:center;"| 79/100 (35 reviews) 
| style="text-align:center;"| A- 
|}

A Serious Man received mostly positive reviews from critics, with an average score of 89% from   soundtrack motif, reflecting Larry’s normal sense of order becoming increasingly disrupted. He writes, "what can happen when the wheel falls off the cart, as Velvel says happened to him on the road that night, or when the truth is found to be lies, that lyric from Somebody to Love that serves as bookends for this film." 

Claudia Puig of USA Today wrote, "A Serious Man is a wonderfully odd, bleakly comic and thoroughly engrossing film. Underlying the grim humor are serious questions about faith, family, mortality and misfortune."  Time (magazine)|Time critic Richard Corliss described it as "disquieting" and "haunting". 

Many critics commented on the link between the film and the Biblical Book of Job. K. L. Evans wrote that "we identify it as a Job story because its central character is tormented by his failure to account for the miseries that befall him."  David Tollerton in his essay "Job of Suburbia?" commented that the "more substantial connection between A Serious Man and the Book of Job – the connection that reaches deeper – is their similarly absurd presentations of the human struggle with anguish and the divine." 

The  , is in their bleak, black, belittling mode, and its hell to sit through....As a piece of movie-making craft, A Serious Man is fascinating; in every other way, its intolerable."  Steve Zemmelman commented that this kind of viewer response results from the films lack of narrative resolution: "The film is perplexing and the dialogue reminds the viewer repeatedly that we are in an encounter with the ever-conflictual and the infinitely mysterious." 

===Awards===

A Serious Man has received critical acclaim and many awards,   with the majority of the films awards and nominations in the categories of Screenplay, Best Picture, Cast, and Cinematography. Joel and Ethan Coen were awarded Best Original Screenplay at the 2009 National Board of Review Awards and the 2010 National Society of Film Critics Awards.  The screenplay also received a nomination for Best Original Screenplay at the 2010 Academy Awards. Other nominations for Best Original Screenplay include acknowledgment from the Writers Guild of America Award, BAFTA,  the Broadcast Film Critics Associations 15th Annual Critics Choice Awards  and the 2010 Boston Society of Film Critics. 

The film was also nominated for Best Picture at the 82nd Academy Awards;  BBC News called it "one of the less talked about nominees".    Other nominations for Best Picture include the Broadcast Film Critics Associations 15th Annual Critics Choice Awards, Boston Society of Film Critics and the Chicago Film Critics Association. The film was listed as one of the ten best films of 2009 by the National Board of Review of Motion Pictures, the American Film Institute, the Satellite Awards and the Southeastern Film Critics Association Awards.
 Independent Spirit Awards. 
 Nikola Tesla Award  at the 2009 14th Satellite Awards. 
 

==References==
 

==External links==
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 