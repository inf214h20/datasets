Buenos Aires Vice Versa
{{Infobox film
| name           = Buenos Aires Vice Versa
| image          = Bavvposter.jpg
| image size     =
| caption        = Theatrical release poster
| director       = Alejandro Agresti
| producer       = Alejandro Agresti Axel Harding
| writer         = Alejandro Agresti
| narrator       =
| starring       = Vera Fogwill Nicolás Pauls Fernán Mirás
| music          = Alejandro Agresti Paul M. van Brugge
| cinematography = Ramiro Civita
| editing        = Alejandro Agresti Alejandro Brodersohn
| distributor    =
| released       =   
| runtime        = 122 minutes
| country        = Argentina Netherlands Spanish
| budget         =
}} Argentine and Dutch dramatic film, written and directed by Alejandro Agresti. The film was produced by Alejandro Agresti and Axel Harding, and co-produced by Emjay Rechsteiner. 

The picture deals with the alienation felt by the children who survived the Argentine military dictatorship of the 1970s.

==Plot==
Opening Title Graphic: As the film begins a message appears and reminds the audience that approximately 30,000 people died during the Dirty War due to the military dictatorships reign during the late 1970s and early 1980s.

The story is then dedicated to the surviving children of the murdered. Two such children, now adults are the main characters. One, Daniela (Vera Fogwill), now has her degree in film and is having trouble finding work. Shes hired by an older couple, living in recluse, to film Buenos Aires for them. She goes out and documents the city. Yet, her customers are upset as they dont remember the Buenos Aires Daniela has filmed.  She then shoots a reel of tourist-type shots. The other, Damián, played by Nicolás Pauls, works in a low-rent motel who discovers the real story about his parents during the dictatorship.

The story is largely episodic, mashing together more than 6 different story lines.

==Cast==
* Vera Fogwill as Daniela
* Nicolás Pauls as Damián
* Fernán Mirás as Mario
* Mirta Busnelli as Loca TV
* Carlos Roffé as Service
* Mario Paolucci as Amigo
* Laura Melillo as Ciega
* Harry Havilio as Tío
* Nazareno Casero as Bocha
* Carlos Galettini as Don Nicolás
* Floria Bloise as Doña Amalia
* Inés Molina as Chica

==Background==
 
The film is based on the aftermath of the real political events that took place in Argentina after   between 9,000 and 30,000 people deemed left-wing "subversives" disappeared from society. 

==Distribution==
 
The film was first presented at the Mar del Plata Film Festival in  November 1996. It opened wide in Argentina on September 18, 1997.

The film was screened at various film festivals, including: the 1996 Cannes Film Festival,    France; the Contemporary Latin American Film Series at UCLA, Los Angeles; the Oslo Film Festival, Norway; the Havana Film Festival, Cuba; and others.

==Critical reception==
Film critic Karen Jaehne praised the film, and wrote, "The film tells you enough about each character to raise your sympathy and not enough to let us see any possible resolution of the dilemma of loneliness. Its an intelligent film that observes mannerisms and social behavior in a way that makes you nod and say, yes, thats how it is...It builds toward a very powerful ending that reminds us of all urban disaster, but the problem that has made Buenos Aires a metropolitan orphanage is undeniable. Buenos Aires - Vice Versa is a wise film - worth watching and will undoubtedly make it to a festival near you." 

==Awards==
Wins
*   Prize, Alejandro Agresti; OCIC Award - Honorable Mention, Alejandro Agresti; 1996.
* Havana Film Festival: Special Jury Prize, Alejandro Agresti; 1996.
* Argentine Film Critics Association Awards: Silver Condor; Best Editing, Alejandro Agresti, Alejandro Brodersohn; Best Film; Best New Actress, Vera Fogwill; Best Original Screenplay, Alejandro Agresti; 1998.

Nominations
* Netherlands Film Festival: Golden Calf, Best Director of a Feature Film, Alejandro Agresti; 1997.
* Argentine Film Critics Association Awards: Silver Condor, Best Director, Alejandro Agresti; Best New Acto, Nazareno Casero; Best New Actor), Nicolás Pauls; Best Supporting Actor, Carlos Roffé; Best Supporting Actress, Mirta Busnelli; 1998.

==References==
 

==External links==
*  
*   at cinenacional.com  
*   film review at Cineismo by Guillermo Ravaschino  
*   film clip

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 