Sarangadhara
 
 
{{Infobox play   
| name       = Sarangadhara   సారంగధర   சாரங்கதாரா
| image      =  
| image_size =
| caption    =
| writer     = Gurazada Apparao
| characters = Sarangadhara  Rajaraja Narendra Chitrangi Vijayaditya
| setting    = Rajamahendri 
| premiere   = 1883 
| place      = Rajamahendri, British India  Telugu
| subject    =  
| genre      = Historical
| web        =
}}
Sarangadhara (   The famous literature Gurajada Apparao wrote the story in long poetic form in English and published in “Indian Leisure Hour” in 1883.  It was a favourite Telugu drama and made into South Indian films.

==The story==
Rajaraja Narendra was ruling the Vengi country with capital of Rajamahendri. He had a son by name Sarangadhara. The king had a second wife Chitrangi and he was very much affectionate towards her. He had a bitter enemy, his step mothers son by name Vijayaditya.

One day Chitrangi, invited her step son Sarangadhara for feast. But as Sarangadhara was leaving for hunting he ignored the invitation. His step mother was very angry towards him and it was brought to the notice of Vijayaditya by the intelligence agency. Vijayaditya made bad propaganda of this accusing of an affair between Sarangadhara and his step mother with the sinister motive of creating conflict in the house of Rajaraja Narendra. Without proper inquiry, Rajaraja Narendra ordered the chopping off the hands and legs of innocent Sarangadhara. It was mandatory to obey kings orders to and so the punishment was meted out to the prince in the mountain forest.

Sarangadhara was lying in a pool of blood screaming in pain drawing the attention of Meghanadha a devotee of Siva, who immediately rushed to Sarangadhara and did what ever he could to alleviate the pain and advised Sarangadhara to pray to Lord Shiva and get his blessings.  Lord Siva impressed by Sarangadharas prayer gave his lost legs and hands and made him a beautiful person.

==Sarangadhareswara temple==
It is an ancient and famous temple in Rajahmundry City (Korukonda road), East Godavari District, Andhra Pradesh. The Lord Siva blessed and gave the rebirth to Sarangadhara. The hill was named as Sarangadhara Metta and the Lord is known as Sarangadhareswara.

==Film and stage adaptations==
In his early years as a stage artiste associated with the Rama Vilasa Sabha of Chitoor, actor Chittor V. Nagaiah secured critical acclaim for his performance in the role of Chitrangi. Pammal Sambandha Mudaliar adaptated to stage dramas and popularized the story in Tamil. S. G. Kittappa and M. K. Thyagaraja Bhagavathar had acted and sang in the various Tamil stage adaptations of Sarangadhara.

===1930 film===
Sarangadhara is a 1930 silent film directed by Y. V. Rao under the General Pictures Corporation.


===1935 Tamil film=== JBH and Homi Wadia brothers in Mumbai. Starred by Kothamangalam Cheenu and T. M. Saradhambal in the lead roles. Kothamangalam Cheenu sang songs like vidhiyai vendRavar yaar and songs already popularized by S. G. Kittappa in stage dramas such as vEdikkaiyaagavE and kOdaiyilE in the movie.


===1936 Tamil film===
Naveena Sarangadhara is a 1936 Tamil film directed by K. Subramaniam under Murugan Talkies. Naveena in Tamil means modern. The movie was titled thus in order to distinguish itself from the earlier versions and also in order to highlight the slight modifications made to the original story. The cast are M. K. Thyagaraja Bhagavathar and S. D. Subbulakshmi in the lead roles supported by S. S. Mani Bhagavathar, G. Pattu Iyer and Indubala. Papanasam Sivan had written the lyrics and composed music for M. K. Thyagaraja Bhagavathar songs such as sivaperuman krupai vENdum, gnana kumarai nadana singari and abaraatham seithaRiyEn had a successful run wherever the movie was screened.

===1937 Telugu film===
 

===1957 Telugu film===
 

===1958 Tamil film===
 

==References==
 

 
 
 
 
 
 
 