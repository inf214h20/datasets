That's Black Entertainment
 
{{Infobox film
| name           = Thats Black Entertainment
| image          = Thats Black Entertainment (1989 film).jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| director       = William Greaves
| producer       = Norm Revis, Jr. David Arpin
| writer         = G. William Jones
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = 
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = Video Communications 1989
| runtime        = 60 minutes
| country        = 
| language       = 
| budget         = 
| gross          = 
| oclc           = 21096403
| isbn           = 978-1-55739-250-3
}}
 1989 documentary film starring African-American performers and featuring clips from black films from 1929-1957.

==Film clips included==
Many entertainers, along with their musical numbers, and the film they starred in, include:
*Paul Robeson (Song of Freedom)
*Bessie Smith (St. Louis Blues (1929 film)|St. Louis Blues)
*Eubie Blake, Nina Mae McKinney, and The Nicholas Brothers (Pie, Pie Blackbird)
*Lena Horne (The Duke Is Tops) Killer Diller)
*Sammy Davis, Jr. and Ethel Waters (Rufus Jones for President)
*Cab Calloway (Cab Calloways Jitterbug Party)
*Ethel Waters (Carib Gold)

Not only musical clips were shown, but dramatic clips as well, like Murder in Harlem (1935), Juke Joint (1947), Four Shall Die (1940), and Souls of Sin (1949).

The film also includes clips from white films stereotyping blacks, including D.W. Griffith|D.W. Griffiths Birth of a Nation, 
and a blackfaced Bing Crosby in Crooners Holiday (1932).

==Celebrity appearances==
*Billie Allen
*Louis Armstrong
*Albert Ammons
*Eubie Blake
*Clarence Brooks
*Cab Calloway
*Nat King Cole
*Bing Crosby
*Dorothy Dandridge
*Sammy Davis, Jr.
*Duke Ellington
*Francine Everett
*Stepin Fetchit
*William Greaves
*Alfred Hawkins
*Billie Holiday
*Lena Horne
*Pete Johnson
*July Jones
*Moms Mabley
*Nina Mae McKinney
*Clarence Muse
*The Nicholas Brothers
*Jesse Owens
*Paul Robeson
*Frank Sugar Chile Robinson
*Bessie Smith
*Fredi Washington
*Ethel Waters
*Spencer Williams
 

==References==
 

==External links==
* 

 
 
 
 
 
 

 