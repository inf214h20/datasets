Remote Control (1988 film)
{{multiple issues|
 
 
}}

{{Infobox film
| name           = Remote Control
| image          =
| image size     =
| alt            =
| caption        =
| director       = Jeff Lieberman
| producer       = Scott Rosenfelt  Mark Levinson
| writer         = Jeff Lieberman
| narrator       =
| starring       = Kevin Dillon Deborah Goodrich Christopher Wynne Frank Beddor Jennifer Tilly Bert Remsen Peter Bernstein
| cinematography = Tim Suhrstedt
| editing        = Scott Wallace
| studio         =
| distributor    = International Video Entertainment
| released       =  
| runtime        = 88 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
| preceded by    =
| followed by    =
}}

Remote Control is a 1988 comedy horror film starring Kevin Dillon. It was produced by Scott Rosenfelt and Mark Levinson and was released by International Video Entertainment.  The film has developed a small cult following since its home video release. 

==Plot==

A video store clerk stumbles onto an alien plot to take over earth by brainwashing people with a bad 50s science fiction movie. He and his friends race to stop the aliens before the tapes can be distributed world-wide.

==Blu-ray/DVD release==

A 25th Anniversary edition was released in 2013 on Blu-ray and DVD by director Jeff Lieberman through his website, jeffliebermandirector.com.

==External links==
* 
 

 
 
 
 
 
 
 
 
 


 
 