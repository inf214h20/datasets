The Soldier and the Lady
{{Infobox film
| name           = The Soldier and the Lady
| image          = 
| alt            =
| caption        = 
| film name      =  Edward Donahue (assistant)
| producer       = Pandro S. Berman Joseph Ermolieff (associate)
| writer         = 
| screenplay     = Mortimer Offner Anthony Veiller Anne Morrison Chapin
| story          = 
| based on       =  
| starring       = Anton Walbrook Elizabeth Allan Margot Grahame Akim Tamiroff Fay Bainter Eric Blore
| narrator       = 
| music          = Nathaniel Shilkret
| cinematography = Joseph H. August
| editing        = Frederic Knudtson RKO Radio Pictures
| distributor    = 
| released       =   }}
| runtime        = 85 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =  
}}

The Soldier and the Lady is the 1937 American adventure film version of the oft-produced Jules Verne novel, Michel Strogoff.  Produced by Pandro S. Berman, he hired as his associate producer, Joseph Ermolieff.  Ermolieff had produced two earlier versions of the film, Michel Strogoff in France, and Der Kurier des Zaren in Germany, both released in 1936. Both the earlier films had starred the German actor, Adolf Wolhbrück. Berman also imported Wolhbrück, changing his name to Anton Walbrook to have him star in the American version. Other stars of the film were Elizabeth Allan, Margot Grahame, Akim Tamiroff, Fay Bainter and Eric Blore. RKO Radio Pictures had purchased the rights to the French version of the movie, and used footage from that film in the American production.  The film was released on April 9, 1937.

==References==
 

==External links==
* 

 
 
 
 
 
 

 