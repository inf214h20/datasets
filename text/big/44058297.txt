The Falling (film)
 
{{Infobox film
| name           = The Falling
| image          = TheFalling2014Poster.jpg
| caption        = UK release poster
| director       = Carol Morley
| producer       = {{plainlist|
* Luc Roeg
* Cairo Cannon
}}
| screenplay     = Carol Morley
| starring       = {{plainlist|
* Maisie Williams
* Florence Pugh
}}
| music          = 
| cinematography = Agnes Godard 
| editing        = 
| studio         = {{plainlist|
* BBC Films
* British Film Institute
}}
| distributor    = Metrodome UK
| released       =  
| runtime        = 102&nbsp;minutes
| country        = United Kingdom
| language       = English
| budget         = £750,000 
| gross          = £144,370 
}}
The Falling is a 2014 drama film written and directed by Carol Morley, starring Maisie Williams and Florence Pugh as best friends at an all-girls school.

== Plot ==
In 1969, Lydia and Abbie are best friends at an English girls school.  Lydia, the neglected daughter of an agoraphobic, becomes fixated on Abbie, who has begun to explore her sexuality.  When Lydia suffers from a fainting spell, it becomes an epidemic, and she must convince the administration to take action. 

== Cast ==
* Maisie Williams as Lydia Lamb
* Florence Pugh as Abbie
* Greta Scacchi as Miss Mantel
* Monica Dolan as Miss Alvaro
* Maxine Peake as Eileen Lamb
* Mathew Baynton as Mr Hopkins

== Production ==
BFI funded the film £750K.  Production began in October 2013. 

The soundtrack is by Tracey Thorn. Morley asked Thorn to provide the music for the film after editing had begun. 

== Release ==
The Falling  premiered at the BFI London Film Festival.  Metrodome will release the film in the United Kingdom. 

== Reception == Time Out London rated it 4/5 stars and wrote, "Carol Morley shows startling versatility and ambition with this jawdropping mash-up of If and Picnic at Hanging Rock".   Mike McCahill of The Daily Telegraph rated it 4/5 stars and called it a continuation of the themes in Nicolas Roegs Performance (film)|Performance and Dont Look Now.   Geoffrey Macnab of The Independent rated it 4/5 stars and wrote, "Carol Morleys The Falling is beguiling and disturbing, a beautifully made and very subtle affair that combines melodrama, rites of passage and supernatural elements in an utterly intriguing way."   Peter Bradshaw of The Guardian rated it 5/5 stars and wrote, "Director Carol Morley has come up with another brilliant and very distinctive feature, about an epidemic of fainting that grips a girls school in the 1960s."  

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 
 


 