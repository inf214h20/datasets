Operacja Samum
 
{{Infobox film
| name           = Operacja Samum
| image          = 
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Władysław Pasikowski
| producer       = Piotr Dejmek, Andrzej Terej
| writer         = 
| screenplay     = Maciej Dutkiewicz, Robert Brutter
| story          = 
| narrator       = 
| starring       = Marek Kondrat, Bogusław Linda, Olaf Lubaszenko
| music          = Goran Bregović
| cinematography = Paweł Edelman
| editing        = 
| studio         = Studio D.T. Film  Warner Bros. Poland, HBO Films, Telewizja Polska
| released       =  
| runtime        = 90 min.
| country        = Poland
| language       = Polish, English, Arabic, Russian, Turkish
| budget         = 
| gross          = 
}}
 1999 Poland|Polish spy film directed by Władysław Pasikowski, starring Marek Kondrat, Bogusław Linda and Olaf Lubaszenko. The film is a fictional account of Operation Simoom, a top secret Polish intelligence operation to withdraw CIA agents before the start of the Persian Gulf War.

==Plot== Iraqi invasion Polish Office for State Protection (UOP), the main Polish intelligence agency. Mayer’s doctor tells him that he may die from an unspecified disease. The film then shifts to Baghdad, Iraq, where Iraqi security forces raid a CIA safehouse maintained by CIA agent Jeff Magnus (Kristof Konrad). Walton and his two other CIA agents manage to escape into hiding as they prepare to leave Iraq before a possible war. Walton is warned by a Polish-American Mossad agent named Karen Pierce (Anna Korcz) to leave Iraq immediately.  Meanwhile, Pierce is having an affair with Mayer’s son Paweł (Radosław Pazura), who is a Polish engineer working in Iraq. Paweł is detained and Pierce escapes after they are stopped by Iraqi secret police. While in a military prison, Paweł is tortured for not disclosing Pierce’s location to the Iraqis. The Iraqis suspect that Paweł is a spy.

Józef Mayer is summoned to UOP.  The head official of the agency (Tadeusz Huk) informs Mayer that his son has gone missing after meeting Pierce.  Mayer then inquires to an attaché of the American Embassy in Warsaw regarding the whereabouts of Karen Pierce.  The film shifts back to Iraq, where two Polish UOP agents, Edward Broński (Bogusław Linda) and Stanisław Kosiński (Olaf Lubaszenko), are inquiring on the whereabouts of Pierce and Mayer’s son.  Mayer manages to discover from his embassy attaché that Pierce is a Mossad agent.  At the same time, Magnus and the other CIA agents prepare to leave Iraq via a helicopter extraction, until their helicopter is shot down. They escape and go back into hiding. In Poland, the head of the UOP meets with Hayes (Jerzy Skolimowski), a high-level official of the CIA.  Because the UOP still has agents in Iraq, Hayes asks the head of the UOP to extract his CIA agents out of Iraq.  Hayes says that if the extraction is successful, Poland’s foreign debts to the United States would be decreased dramatically.  Hayes stresses that his agents must be evacuated out of Iraq, because they hold sensitive materials viable for war.  Meanwhile, Mayer demands to go into Iraq and get his son out, undercover as a construction engineer.  Pierce manages to escape Iraq and meets with the head of Mossad, Shopsovitz (Gustaw Holoubek).  Shopsovitz reveals that Magnus and his team hold a microfilm containing Saddam Hussein’s daily schedules.  Pierce agrees to take the microfilm from Magnus.

In Baghdad, Mayer arrives and meets with Broński and Kosiński.  The agents provide Mayer with an Iraqi driver named Faisal (Tugrul Ҫetiner), who is fluent in Polish.  Mayer and Faisal drive to the Iraqi construction site where his son worked.  They discover that Mayer’s son is possibly being held at a nearby police station.  Meanwhile, Pierce goes back into Iraq and receives the Magnus’ microfilm.  The next day, Pierce finds Broński in Baghdad and gives him information on the location of the CIA agents.  As Broński and Kosiński head out of the Polish Embassy to find the agents, they are followed by Iraqi secret police.  Kosiński manages to drop out of the car without notice of the secret police agents.  As he heads off on foot, he tracks down the CIA agents in hiding.  Kosiński gets the agents on board of a bus with other Europeans and gets them inside the Polish Embassy without being noticed by the secret police.  Broński and Kosiński give them Polish passports and new cover identities.  Back in Poland, the chiefs of UOP, Mossad and the CIA agree to use Mayer as bait to get the CIA agents out of Iraq.  They want the Iraqis to believe that Mayer is going to get the agents out himself, but in reality he only wants to get his son out.  At the Embassy, Kosiński and the CIA agents leave on a bus towards the border.  Meanwhile, Mayer infiltrates the Iraqi police station and gets his wounded son out.  Kosiński and the agents cross the Iraqi-Turkish border without trouble or raising suspicions.  At another border checkpoint into Turkey, Broński assists Mayer, his son, Pierce and Faisal get across.  Once they cross the border, Pierce is shot and killed by an Iraqi colonel and his troops.  Back in Poland, Mayer, Broński, Kosiński and the UOP chief are awarded medals of distinction at the American Embassy.  As the film closes, Mayer reacquaints with his estranged son.

==Cast==
* Marek Kondrat as Major Józef Mayer, retired agent of UOP
* Bogusław Linda as Edward Broński, an active UOP agent
* Olaf Lubaszenko as Stanisław Kosiński, an active UOP agent
* Tadeusz Huk as the Colonel, head chief of the UOP
* Anna Korcz as Karen Pierce, a Polish-American Mossad agent
* Radosław Pazura as Paweł Mayer, a Polish engineer working in Iraq, the son of Józef Mayer
* Jerzy Skolimowski as Hayes, a high-ranking CIA official
* Gustaw Holoubek as Shopsovitz, the head chief of Mossad
* Kristof Konrad as Jeff Magnus, an active CIA agent
* Tugrul Ҫetiner as Faisal, an Iraqi driver and assistant working for UOP agents

==External links==
* 
* 
* 

 
 
 
 