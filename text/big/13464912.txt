The Shameless Old Lady
 
{{Infobox film
| name           = The Shameless Old Lady
| image          =
| caption        =
| director       = René Allio
| producer       = Claude Nedjar
| writer         = René Allio Bertolt Brecht Sylvie Victor Lanoux
| music          =
| cinematography = Denys Clerval 
| editing        = Sophie Coussein 
| distributor    =
| released       =  
| runtime        = 94 minutes
| country        = France
| language       = French
| budget         =
}}
 Sylvie and Victor Lanoux. Sylvie won the National Society of Film Critics Award for Best Actress for this role, one of her last and her only lead.

== Plot ==
Madame Bertini, a newly widowed 70-year-old miser, has lived a sheltered life married to her husband for some sixty years. She determines to venture into the modern world and have as much fun as possible, and in doing so finds that she loves it. She blows her life savings, much to the disapproval of the young people around her.

== Cast == Sylvie - Madame Bertini
*Victor Lanoux - Pierre
*Malka Ribowska - Rosalie
*François Maistre - Gaston
*Etienne Bierry - Albert
*Pascale de Boysson - Simone
*Jean-Louis Lamande - Charles
*Lena Delanne - Victoire
*Jeanne Hardeyn - Rose
*Robert Bousquet - Robert
*Jean Bouise - Alphonse
*André Jourdan - Lucien
*Pierre Decazes - Charlot
*André Thorent - Dufour

== External links ==
* 

 
 
 


 