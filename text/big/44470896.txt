Working Man Trilogy
{{Infobox media franchise title = Working Man Trilogy
  imagesize = 250px caption = The first film, "Kansas City Trucking Co." creator = Tim Kincaid  (as Joe Gage) 
| director= Tim Kincaid  (as Joe Gage) 
| producer= Sam Gage films =
*  
*  
*  
}}
 gay pornographic producer on all three. The series featured universally masculine, working class male actors engaging in various sexual activities with each other, a notable divergence from the usual Butch and femme|masculine/feminine partner roles found in earlier gay porn.

== Films ==
{| class="wikitable" style="width:99%;"
|-
! Film
! Director
! Writer
! Producer
! Cinematographer
! Starring
|-
| Kansas City Trucking Co.
| rowspan="3" style="text-align:center;"| Tim Kincaid  (as Joe Gage) 
| rowspan="3" style="text-align:center;"| Tim Kincaid  (as Joe Gage) 
| rowspan="3" style="text-align:center;"| Sam Gage
| rowspan="2" style="text-align:center;"| Nick Elliot
| style="text-align:center;"| 
*Richard Locke
*Steve Boyd
*Jack Wrangler
|-
| El Paso Wrecking Corp.
| style="text-align:center;"| 
*Richard Locke
*Fred Halsted
|-
| L.A. Tool & Die
| style="text-align:center;"| 
Richard Youngblood 
| style="text-align:center;"| 
*Richard Locke
*Michael Kearns
*Will Seagers
*Paul Barresi
|}
 
 mainstream films within his porn films.    The trilogy is tied together via narration of the main character Hank (Richard Locke), a truck driver. The scenes follows his sexual adventures through various truck trips and jobs.

Masturbation is the most commonly depicted sexual act within the series. Gage dismissed frequent anal sex as "not very cinematic" due to his desire to focus more closely on the penis: "The whole idea of making homosexual pornography…   you strip it down to its absolute basics,   the worship of the phallus, the worship of the penis. If you’re going to make homosexual pornography, you’d better light the dick. So masturbation and oral sex are … the best way to photograph. You’re highlighting the penis—that’s what it’s about.” 

The series begins with Locke working with a new heterosexual trucking partner (Steve Boyd) in Kansas City Trucking Co.. A number of the films porn scenes presented as sexual fantasies and dreams. Dialogue focuses on Locke consoling Boyd over his sexual frustration due to his distance from his girlfriend, until the two leads engage in a three way orgy with another man at the end.   
 Male friendship, glory hole scene, remembered especially for its plethora of slobber.   
 romance focus in porn. Gage admitted he wanted his lead to find an ending with a man that was grieving.   

==Production==
Joe Gage met Sam Gage, then a casting director, at a party. Joe Gage pitched the idea to Sam Gage, and Sam Gage helped Joe Gage find investors for the project.   

Joe and Sam Gage hired both professional porn stars and members of the surrounding community to star in the film. According to Joe Gage, "the majority of men who applied wanted to appear in the films for political reasons as much as anything else." 

All sexual activities and dialogue was scripted in detail.  The working class nature of the film was inspired by Joe Gages own sexual experiences. 

LA Tool & Die took 20 days to shoot, much longer than most porn films. 

==Reception==
Kansas City Trucking Co was released at the end of 1976, and it rose to popularity in 1977, launching the series.  The Working Man Trilogy became the biggest selling gay porn film series of the pre-condom era.   

The film was particular popular due to the general disinterest of the characters in labels of gayness despite frequent homosexual activity, a trait that appealed to similar minded men who felt otherwise alienated by the focus on gay identity tropes in earlier gay porn. 

==Legacy== hardcore erotica of all time."  

Stallion critic Jerry Douglas commented that the trilogy "introduced a new sort of hero to the gay film, and celebrated the freedom of the sexual revolution that had spread across America during the years that they were being made. Today, in retrospect, the trio stand together as the definitive cinematic statement on the emergence of the macho homosexual who sexual transiency and voracity influenced larger and larger numbers of gay--until the advent of AIDS."  

The series broke new ground in its emphasis on exclusively rugged men and masculinity, as opposed to the man-woman gender roles replicated in male-male porn videos prior to Kansas City Trucking Co. The characters of the trilogy were not classically coded as homosexual. Especially in El Paso Wrecking Corp, male camaraderie is emphasized outside the realm of sex.   

In the wake of the series, the phrase "Gage men" rose to prominence briefly as term for masculine, hunky, hairy men like the ones featured in the film. 

Filmmaker Wash West cites Kansas City Trucking Co as one of his biggest influences: “If you look back at the early days in the 1970s during the ‘golden age’ of porn, people were really interested in making ‘films.’ For example, Kansas City Trucking Company (1976) and LA Plays Itself (1972) were the work of real filmmakers—well-constructed, beautifully shot, visually experimental, and sexy.… They were my biggest influences. I wanted to return to elements of ’70s filmmaking and bring that up to date.”   

==See also==
* Tim Kincaid

==References==
 

 
 
 
 
 