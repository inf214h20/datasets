Alludugaaru Vachcharu
{{Infobox film
| name           = Alludugaaru Vachcharu
| image          =
| caption        =
| writer         = Posani Krishna Murali  Selva
| screenplay     = Ravi Raja Pinisetty
| producer       = Sunkara Madhu Murali Mullapudi Brahmanandam
| director       = Ravi Raja Pinisetty Kausalya Heera Rajagopal
| music          = M. M. Keeravani
| cinematography = V. Jayaram
| editing        = A. Sreekar Prasad
| studio         = Melody Theaters
| released       =  
| runtime        = 152 minutes
| country        = India
| language       = Telugu
| budget         =
}}
 Telugu drama film produced by Sunkara Madhu Murali and Mullapudi Brahmanandam and directed by Ravi Raja Pinisetty. Jagapati Babu, Kausalya (actress)|Kausalya, Heera Rajagopal play the lead roles. The film is a remake of the Tamil movie Pooveli from 1998.      

==Plot==
Murali (Jagapati Babu) a orphan who is crave to have family relations. Murali a professional violinist follows in love with a playback singer Shalini (Heera Rajagopal) and goes around expressing his love for her. But when she insults his love, he promises he wouldnt trouble her again and that one day, she would understand him and come to him. One day Murali meets his childhood friend Maha / Mahalakshmi  (Kausalya (actress)|Kausalya), who has recently lost her lover Madhu (Abbas (actor)|Abbas) in an accident. To force her father to accept her love, she had already told him that she was married. Circumstances force her uncle to think Murali is her husband and they go back to her house in the village. But Mahas father Raghava Rao (Nassar) is so much angry on their marriage and does not accept Murali as his son-in-law.

Actually, Murali wants to earn the displeasure of the members of her household so that he can leave, but his plans backfire and earn him their love and affection. As he continues living there, he starts to like having a family around and also falls in love with Maha. Mahas grandmother (Rama Prabha), finds out the arrangement between Murali and Maha, but since she likes Maha, plays a trick to get them both married.

Meanwhile Shalini developed feelings for Murali, comes to the village before the wedding creating doubts in the minds of the others. This leads to some confusion and problems. Finally movie ends with marriage of Murali & Maha.

==Cast==
 
*Jagapati Babu as Murali Kausalya as Mahalakshmi / Maha
*Heera Rajagopal as Shalini Abbas as Madhu
*Nassar as Raghava Rao Srihari as Srihari
*Giri Babu as Shankar Rao
*Brahmanandam as 
*M. M. Keeravani as Himself
*Tanikella Bharani as Madhus father
*M. S. Narayana as Ateesu AVS as Inspector
*Sarika Ramachandra Rao as Madman
*Gadiraju Subba Raju as Murthy
*Mithai Chitti as Master
*Rama Prabha as Mahas Bamma
*Rajitha as Shankar Raos wife
*Delhi Rajeswari 
*Amesha Jalil as Rajyam
*Madhurisen as Shalinis friend
*Sudeepa Pinky
*Oorvasi Patil
*Master Tanush
*Baby Vaishanvi 
 

==Soundtrack==
{{Infobox album
| Name        = Alludugaaru Vachcharu
| Tagline     = 
| Type        = film
| Artist      = M. M. Keeravani
| Cover       = 
| Released    = 1999
| Recorded    = 
| Genre       = Soundtrack
| Length      = 27:31
| Label       = HMV Audio
| Producer    = M. M. Keeravani
| Reviews     =
| Last album  = Seetharama Raju   (1999) 
| This album  = Alludugaaru Vachcharu   (1999)
| Next album  = Raghavaiah Gaari Abbayi   (1999)
}}

The music for the film was composed by M. M. Keeravani with lyrics by Sirivennela Sitaramasastri. It was released by HMV Audio Company.

{{Track listing
| collapsed =
| headline =
| extra_column = Singer(s)
| total_length = 27:31
| all_writing =
| all_lyrics =
| all_music =
| writing_credits =
| lyrics_credits = 
| music_credits =

| title1  = Chaalichalani Kulukulalona Chitra
| length1 = 5:14

| title2  = Marugela Musugela  Hariharan
| length2 = 5:00

| title3  = Gundelo Sandadi 
| extra3  = Sashi Preetham,Chitra
| length3 = 3:28

| title4  = Rangu Rangu Rekkala
| extra4  = M.M. Keeravani
| length4 = 4:54

| title5  = Aiswarya Rai Neeku 
| extra5  = Naveen
| length5 = 4:30

| title6  = Noraara Pilichina SP Balu
| length6 = 4:25 
}}

==References==
 

 
 

 