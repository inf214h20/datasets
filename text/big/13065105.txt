I for India
 
 

I for India is an English and Hindi language film released in the United Kingdom in August 2007.

Directed by Sandhya Suri, it chronicles home movie footage (filmed on Super 8mm) and reel-to-reel tape recordings made by her father, Doctor Yash Pal Suri, starting from the 1960s. With the primitive state of telecommunications at the time, Dr.Suri sent the films and tapes to his family in India as a form of contact and information as to his new life in the UK with his wife and family. Dr.Suri reveals much of his innermost emotions,such as missing his family and home town, his difficulties at settling in a new country, and concern at the casual racism to which he was often subjected. The film also includes footage of various British TV programmes of its era regarding Asian emigrants (perceived as naively patronising), also incorporating part of an interview featuring Margaret Thatchers views on emigration shortly before she became British Prime Minister.

It was distributed by ICA Projects. It was released theatrically in both the U.K. and the U.S.

==External links==
* 
* 

 
 
 
 
 
 
 
 
 


 