Cell Phone (film)
{{Infobox film
| name           = Cell Phone
| image          = Cell Phone movie poster.jpg
| image_size     = 
| caption        = Promotional poster for Cell Phone
| director       = Feng Xiaogang
| producer       = Wang Zhongjun  Yang Buting
| writer         = Liu Zhenyun
| narrator       = 
| starring       = Ge You  Zhang Guoli  Fan Bingbing  Xu Fan
| music          = Su Cong
| cinematography = Zhao Fei
| editing        = Zhou Ying
| distributor    = 
| released       = 18 December 2003
| runtime        = 107 minutes
| country        = Peoples Republic of China
| language       = Mandarin Chinese
| budget         = 
| preceded_by    = 
| followed_by    = 
| website        = 
}} Chinese comedy-drama film directed by Feng Xiaogang and starring Ge You, Zhang Guoli, Xu Fan and Fan Bingbing. It was first released on 18 December 2003 in Mainland China and was subsequently screened at the Cleveland International Film Festival on 18 March 2005. With box office earnings of over Renminbi|¥50 million, Cell Phone became the best-selling domestic film in 2003. 

Written by Liu Zhenyun, based on his own novel of the same title, the film revolves around two successful men whose marriages were wrecked when their wives uncovered their extramarital affairs through traces left in their cellphones. More broadly, the film explores the role of cellphones in interpersonal relationships in modern Peoples Republic of China|China, where the rapid development in information technology is having huge impacts on the way people communicate.

==Plot==
Yan Shouyi is a TV host who has an affair with Wu Yue (Fan Bingbing), a young and attractive woman working in publishing. Yan diligently erases all text messages and call records between him and Wu on his cellphone before he gets home everyday, in order to avoid detection by his wife Yu Wenjuan (Lu Zhang). One night, after telling Yu that he has a work meeting with Fei Mo (Zhang Guoli), a TV producer and Yans superior, Yan rendezvous with Wu and switches off his cellphone. Not being able to reach Yan a while later, Yu calls Fei, who has been in the know of Yans affair.

==Cast==
* Ge You as Yan Shouyi (S: 严守一, T: 嚴守一, P: Yán Shǒuyī), a TV host
* Zhang Guoli as Fei Mo (S: 费 墨, T: 費 墨, P: Fèi Mò), a TV producer, Yans superior
* Xu Fan as Shen Xue (C: 沈 雪, P: Shěn Xuě), a teacher at an arts academy, Yans lover after his divorce with his wife
* Fan Bingbing as Wu Yue (C: 武 月, P: Wǔ Yuè), a young attractive woman working in publishing who has an affair with Yan
* Lu Zhang as Yu Wenjuan (S: 于文娟, T: 於文娟, P: Yú Wénjuān), Yans wife who divorces him when she discovers his affair with Wu

==Reception==
The popularity of Cell Phone among the cinema-goers is evident in its strong box office performance. Having achieved Renminbi|¥50 million in box office within a month after release (United States dollar|US$6.4 million in total according to Business Week ), the film became the best-selling domestic production in 2003.    The film also clinched all three top awards at the 2004 Hundred Flowers Awards, which is based on viewer voting. However, it failed to bag any award or even acquire nomination for any of the major awards at the jury-based Golden Rooster Awards in the same year. According to Golden Roosters leading juror Zhong Chengxiang, albeit being popular among viewers, Cell Phone lacked "class and style".  Producer Wang Zhongjun retorted that viewers approval is of the utmost importance and rejected the Golden Rooster as an award that has "not a single bit of commercial driving force".  On the other hand, the official Huabiao Awards gave out an unprecedented special award to Cell Phone for its achievements in "market development", signifying official recognition of the market forces and taste of the masses.

===Awards and nominations===
* Huabiao Awards, 2004
** Market Development Award
** Best Film (nominated)
** Best New Actress (nominated) — Fan Bingbing
* Golden Rooster Awards, 2004
** Best Supporting Actor (nominated) — Zhang Guoli
** Best Supporting Actress (nominated) — Fan Bingbing
** Best Cinematography (nominated)
** Best Art Direction (nominated)
* Hundred Flowers Awards, 2004
** Best Film
** Best Actor — Ge You
** Best Actor (nominated) — Zhang Guoli
** Best Actress — Fan Bingbing
** Best Actress (nominated) — Xu Fan

==References==
 

==External links==
 
*  
*  

 

 
 
 
 
 
 
 
 
 