Almost Summer
{{Infobox film
| name           = Almost Summer
| image          = Almost_summer_poster.jpg
| image size     = 
| alt            = 
| caption        = Theatrical Poster
| director       = Martin Davidson
| producer       = Rob Cohen
| writer         = Judith Berg   Sandra Berg   Martin Davidson Marc Reid Rubel
| narrator       = 
| starring       = Bruno Kirby   Lee Purcell Charles Lloyd
| cinematography = Stevan Larner
| editing        = Lynzee Klingman
| studio         = 
| distributor    = Universal Pictures
| released       =  
| runtime        = 88 min
| country        = United States
| language       = English
| budget         = 
| gross          = 
| preceded by    = 
| followed by    = 
}}

Almost Summer is a 1978 film directed by Martin Davidson, and produced by Motown Productions for Universal Pictures. It is the only Motown theatrical feature not to center on African-American characters.  Though not successful at the box office, the film has since acquired a certain degree of historical importance because many observers consider it to be the first of a series of distinctive "youth-genre" films of which other, more prominent examples include Fast Times at Ridgemont High and The Breakfast Club.            

Set in a generic Southern California high school, the plot of the film revolves around a student council election, which stirs up assorted petty jealousies among various characters.  Most of the soundtrack was written by Mike Love of the Beach Boys and performed by a studio band assembled by Love known as Celebration.  The title song, which became a mid-level hit single, opens with the lyrics "Susie wants to be a lady director, and Eddie wants to drive a hearse; Johnny wants to be a doctor or lawyer, and Linda wants to be a nurse" &mdash; reflecting a total lack of world-changing idealism on the part of the teenage characters, thus clearly marking them as the first wave of a new generation, which in the fullness of time would receive the label "Generation X."

The best-known actor to star in the film was  , and Mark Bowden.
 Motion Picture Association of America, but earned a B &mdash; "objectionable in part" &mdash; rating from the United States Conference of Catholic Bishops Office for Film and Broadcasting; the latter body observed that "the film presents in uncritical fashion a suffocatingly materialistic and amoral environment, has offensive jokes at the expense of people with physical disabilities, and flaunts a gratuitous bit of nudity."

Outdoor scenes were filmed at Verdugo Hills High School in Tujunga, California.

==Cast==
* Bruno Kirby ...  Bobby DeVito
* Lee Purcell ...  Christine Alexander
* John Friedrich ...  Darryl Fitzgerald
* Didi Conn ...  Donna DeVito Thomas Carter ...  Dean Hampton
* Tim Matheson ...  Kevin Hawkins
* Michael Stearns ...  Vice Cop
* Sherry Hursey ...  Lori Ottinger
* Gene LeBell ...  Rose Bowl Coach
* Harvey Lewis ...  Stanley Lustgarten
* Allen G. Norman...  Scratch
* Petronia Paley ...  Nicole Henderson
* Donna Wilkes ...  Meredith David Wilson ...  Duane Jackson

==References==
 

==External links==
* 
*List of American films of 1978

 

 
 
 
 
 
 
 