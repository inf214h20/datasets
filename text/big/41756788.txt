Farewell to Cinderella
{{Infobox film
| name =  Farewell to Cinderella
| image =
| image_size =
| caption =
| director = Maclean Rogers 
| producer = A. George Smith   Arthur Richardson   Maclean Rogers  
| narrator = John Robinson   Glennis Lorimer    Sebastian Smith
| music = 
| cinematography = Geoffrey Faithfull 
| editing = 
| studio = George Smith Productions 
| distributor = RKO
| released = April 1937
| runtime = 64 minutes
| country = United Kingdom English 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}} John Robinson and Glennis Lorimer. The film was made at the Nettlefold Studios in Walton-on-Thames as a quota quickie for release by the Hollywood firm RKO. 

==Cast== 
* Anne Pichon as Margaret  John Robinson as Stephen Moreley 
* Glennis Lorimer as Betty Temperley
* Sebastian Smith as Andy Weir 
* Arthur Rees as Uncle William 
* Ivor Barnard as Mr. Temperley 
* Margaret Damer as Mrs. Temperley 
* Ena Grossmith as Emily

==References==
 

==Bibliography==
* Chibnall, Steve. Quota Quickies: The British of the British B Film. British Film Institute, 2007.
* Low, Rachael. Filmmaking in 1930s Britain. George Allen & Unwin, 1985.
* Wood, Linda. British Films, 1927-1939. British Film Institute, 1986.

==External links==
* 

 
 
 
 
 
 
 
 

 