Helping Hands (film)
{{Infobox film
| name           = Helping Hands
| image          =
| image_size     =
| caption        = Edward Cahn
| producer       = MGM
| writer         = Hal Law Robert A. McGowan
| narrator       =
| starring       =
| music          =
| cinematography = Jackson Rose
| editing        = Leon Borgeau MGM
| released       =  
| runtime        = 10 44"
| country        = United States
| language       = English
| budget         =
}}
 short comedy Edward Cahn.  It was the 201st Our Gang short (202nd episode, 113th talking short, 114th talking episode, and 33rd MGM produced episode) that was released.

==Plot==
Spanky receives a letter from his recently drafted older brother. Inspired by the letters patriotic sentiments, Spanky and the gang organize a "home guard," prepared to do battle should the Nazis invade California. This attracts the attention of Army Major Sanford, who informs the kids that they would be of even greater service to Uncle Sam by looking out for fire hazards, collecting scrap metal and paper, and encouraging their parents to buy war stamps and bonds.

==Cast==
===The Gang=== Mickey Gubitosi (later known as Robert Blake) as Mickey
* Darla Hood as Darla
* Billy Laughlin as Froggy
* George McFarland as Spanky
* Billie Thomas as Buckwheat

===Additional cast===
* Vincent Graeff as Sentree
* James Gubitosi as Nick
* Edward Soo Hoo as Lee Wong
* Harvard Peck as Boy examined by Froggy
* Leon Tyler as Swedish boy
* Freddie Chapman as Kid complaining in line
* Mickey Laughlin as Kid hit with baseball bat
* Margaret Bert as Mickeys mother
* Sam Flint as Major Sanford
* Byron Foulger as Mr. Morton, head of civilian counsel
* Joe Young as Clerk
* Raphael Dolciame as Kid
* Ralph Hodges as Kid
* Mickey McGuire as Kid
* Michael Miller as Kid
* Tommy Dee Miller as Kid
* Emmet Vogan as Darlas father (scene deleted)

==Note==
The film marked the first of the wartime propaganda-themed shorts in the Our Gang series. Critics and fans both have cited that the wartime films marked a noticeable decline in the series.   

==See also==
* Our Gang filmography

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 


 