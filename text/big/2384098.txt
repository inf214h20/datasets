American Shaolin
 
{{multiple issues|
 
 
}}

{{Infobox film
| name           = American Shaolin
| image          = American_shaolin2.jpg
| caption        = American Shaolin DVD Cover
| director       = Lucas Lowe
| producer       = Keith W. Strandberg
| writer         = Keith W. Strandberg
| starring       = Reese Madigan Trent Bushey
| music          = Richard Yuen
| cinematography = Viking Chiu  Luis Cubilles
| editing        = Allan Poon
| distributor    =
| released       = 1992
| runtime        = 106 min.
| country        =  
| awards         = English
| budget         =
| preceded by    =
| followed by    =
}}
American Shaolin is a 1992 martial arts film, starring (among others) Reese Madigan, Kim Chan and Daniel Dae Kim. It was directed by Lucas Lowe.

==Plot==
During a martial arts tournament, the American finalist Drew Carson (Reese Madigan) is humiliated by his opponent, The ruthless and sadistic kickboxer Trevor Gottitall (Trent Bushey) who pantses him during the match. To add to the insult, Drews teacher Master Kwan (Kim Chan) confesses that he is not—as he had claimed—a Shaolin monk, and therefore he had not passed on the actual knowledge of Shaolin kung fu to Drew. Determined to learn the actual art to prevent another such situation, Drew departs for China and arrives at the Shaolin Temple. At first, the monks do not let him enter, but with the help of a pretty tea shop waitress, Ashena (Alice Zhang Hung), and an old monk (Zhang Zhi Yen) who gives him a decisive advise, he waits outside of the temple for a week, after which he manages to be admitted. The old monk also turns out to be the abbot of the temple, Master San De, and he and his stern taskmaster train Drew and a number of other young apprentices in the ways of the Shaolin.

At first Drew causes much trouble as his American teenage temperament clashes with the tranquility within the temple and with his fellow student, Gao ( , he accompanies—along with Ashena—a delegation of his fellow students and the abbot to a martial arts tournament in Shanghai.

At the tournament, Drew encounters the American kickboxer Trevor Gottitall once again. Trevor taunts Drew before proceeding with this match against Gao. Gao initially gains the upper hand, but Trevor resorts to his dirty fighting techniques and injures Gao. With Gao pinned against the ropes, Trevor demands a match against the "American Shaolin". Drew rises, but sits down again, refusing to fight Trevor on the principle of non-violence and selflessness. Infuriated, Trevor continues to beat up Gao and hurls him out of the ring. Encouraged by Master San De, Drew finally enters the ring to fight Trevor. Trevor immediately used dirty tricks again, but Drew prevails and even offers his hand to the defeated Trevor. The crowd voices their support for Shaolin, and Master San De declares that this is "the future of Shaolin".

==Production==

*During the only  day of filming within the walls of the Forbidden City, the entire  film cast and crew were inexplicably ordered to leave by the government, despite having been granted permission to shoot there. The principal photography on American Shaolin took place from May through late August 1991, two years after the Tiananmen Square protests of 1989, and the filming, particularly within Beijing itself, was under heavy governmental scrutiny.

*In the United States, the film was released on video by Academy Entertainment as American Shaolin: King of the Kickboxers 2.  Although both this film and The King of the Kickboxers share the same director, they are completely unrelated in terms of plot and characters. The movie has never been released on DVD (in the United States) and as of December 30, 2009, there have been no plans to release the movie onto DVD (in the United States).

*The DVD is available in Australia. 

==References==
 

==External links==
*  

 
 
 
 
 