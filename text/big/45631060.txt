Thamarai (film)
{{Infobox film
| name           = Thamarai
| image          = 
| image_size     =
| caption        = 
| director       = K. K. Rajsirpy
| producer       = K. K. Rajsirpy
| writer         = K. K. Rajsirpy
| starring       =   Deva
| cinematography = A. Karthik Raja
| editing        = B. Lenin V. T. Vijayan
| distributor    =
| studio         = Kalai Sirpy
| released       =  
| runtime        = 130 minutes
| country        = India
| language       = Tamil
}}
 1994 Tamil Tamil drama Rupini and Rohini in Deva and was released on 19 August 1994.   

==Plot==

According to the prophecy, Kali temple has to be shifted in Madurai Veeran temples place. This prophecy creates troubles between the two communities of the village. In the meantime, Thamarai (Napoleon (actor)|Napoleon) comes back from jail to help the "Madurai Veeran" community against the vicious village chief Subbarayan (Rajesh (actor)|Rajesh).

Five years back, a brute Thamarai came to Subbarayans village. Being short-tempered, he clashed against the ruthless Subbarayan many times. Thamarai and Poogodai fell in love with each other but the poor palm wine seller Sarasu (Rupini (actress)|Rupini) developed a soft corner for Thamarai. Poogodai later got married with a rich groom and Thamarai went to jail for cutting the hand of the grooms father. The next day, the groom committed suicide.

During the Madurai Veeran temple festival, Subbarayan plans to demolish the Madurai Veeran temple but Thamarai prevents it. In the confrontation, Sarasu died by saving Thamarai. What transpires next forms the rest of the story.

==Cast==

  Napoleon as Thamarai Rupini as Sarasu Rohini as Poogodai Rajesh as Subbarayan Poosari
*R. Sundarrajan as Veerappan
*Supergood Kannan as Maniarasu
*Poorani as Elanji
*Kavitha
*Vijaya Chandrika
*A. K. Veerasamy as Sangili
*Kumarimuthu as Manickam
*Suryakanth
*Krishnamoorthy as Semalai
*Nalinikanth
*Bayilvan Ranganathan
*Vellai Subbiah
*Thidir Kannaiah
*Durai Ramachandran
*A. Madhur Swamy
*Nadesh Alex as Parisal
 

==Production==
Actor Napoleon (actor)|Napoleon, who played villains and character roles until then, signed up to play for the first time the hero role. But the film that first got released in which he had played the hero role was Seevalaperi Pandi. 

==Soundtrack==

{{Infobox Album |  
| Name        = Thamarai
| Type        = soundtrack Deva
| Cover       = 
| Released    = 1994
| Recorded    = 1994 Feature film soundtrack |
| Length      = 28:42
| Label       =  Deva
| Reviews     = 
}}

The film score and the soundtrack were composed by film composer Deva (music director)|Deva. The soundtrack, released in 1994, features 6 tracks with lyrics written by Vairamuthu and Deva.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Track !! Song !! Singer(s) !! Duration
|- 1 || Aattu Mandhaiya Otti Rotta || Mano (singer)|Mano, Sindhu || 4:41
|- 2 || Chandrabose || 5:09
|- 3 || Enge Then Thuli Iru Kaiyil Ottaamal || Mano, Sunandha || 4:55
|- 4 || Madurai Veeran Sami || Swarnalatha || 4:59
|- 5 || Mukka Marakka Moonguthaiya || S. P. Sailaja || 4:34
|- 6 || Vellai Kili Oruththi || K. S. Chithra || 4:47
|}

==References==
 

 
 
 
 
 