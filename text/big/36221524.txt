Kadhal Rojavae
{{Infobox film
| name = Kadhal Rojavae
| image = Kadhal Rojavae.jpg Keyaar
| writer = Robin Bhatt
| screenplay =
| starring = George Vishnu Pooja Kumar
| producer = M. Bhaskar
| editing =
| cinematography =
| studio = Oscar Films International
| music = Ilaiyaraaja
| released = January 14, 2000
| runtime =
| country = India Tamil
}}
 Tamil romantic film directed by Kothanda Ramaiah|Keyaar, which was a remake of the 1991 Hindi film Dil Hai Ke Manta Nahin  by Mahesh Bhatt. The film featured newcomers and Pooja Kumar in the lead roles, while Sarath Babu, Charle and S. S. Chandran play supporting roles. The films music is composed by Ilaiyaraaja, and the film opened in January 2000 to a negative response at the box office. 

==Cast==
* George Vishnu
* Pooja Kumar as Pooja
* Sarath Babu
* Charle
* S. S. Chandran

==Production== Keyaar was still shooting causing a rift between the actor and producer Bhaskar during the making of the project.  Pooja Kumar, who was crowned Miss India USA in 1995, was selected to make her debut as heroine and during production she was signed and dropped from other Tamil films of the period including V. I. P. (film)|V. I. P and Chinna Raja. The actress notably returned more than a decade later starring in the lead role in Kamal Haasans bilingual Vishwaroopam. Kadhal Rojavae released after a delay in production in 2000.

==Release==
The film opened in January 2000 to mixed reviews with Savitha of The Hindu mentioning "Both the newcomers have a long way to go in honing their acting skills". The critic also added that the "The high point of the film is the music by Ilaiyaraja. The same can be said about the background score that lifts the mood of the film to some extent."  The film went largely unnoticed at the box office due to low profile nature of the cast.

==Soundtrack==
* Thottu Thottu Pallakku - SPB, Sujatha
* Ilavnil - SPB, Chithra
* Ninaitha Varam - Unnikrishnan, Sunitha
* Chinna Vennila - Mano, Anuradha Sriram
* Kalyana Jodi - SPB
* Midnight Mama - SPB, Chitra
* Pudhuponnu - Unnikrishnan, Sunitha
* Sirithale - Minmini

==References==
 

==External links==
* 

 

 
 
 
 
 