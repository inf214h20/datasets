Game for Vultures
 

{{Infobox film
| name           = Game for Vultures
| image          = AGameForVultures1979Poster.jpg
| alt            =  
| caption        = American poster
| director       = James Fargo
| producer       = Hazel Adair
| writer         = Philip Baird
| starring       = Richard Harris Richard Roundtree Denholm Elliott Joan Collins
| music          = Tony Duhig Jon Field Alex Thomson
| editing        = Peter Tanner
| studio         = 
| distributor    = Columbia Pictures
| released       =  
| runtime        = 113 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
}} Michael Hartmann set during the Rhodesian Bush War.

==Plot==
 smuggler David Iroquois helicopters black African nationalists. However, word of his plan soon reaches the latter, who apply strong political pressure to kill the deal in its cradle – the aircraft shipment in question is impounded upon reaching neighbouring South-West Africa.

Meanwhile, one of the many indigenous guerillas resisting the Rhodesian regime is Gideon Marunga (Roundtree), veteran combatant and reluctant participant in atrocities directed against unarmed civilians by his fellow insurgents. Marunga discovers that Swansey, with the aid of the Rhodesian Special Air Service and South African sympathizers, hopes to lead an armed raid on the airfield where the Iroquois are being temporarily held – with the intention of stealing them across the border into Rhodesia.

On the day of the assault, Marunga arrives at the airfield and stalls the attacking paratroops, while his accomplices succeed in destroying some of the helicopters. In the firefight which ensues he comes face to face with Swansey, and the two men subsequently share a weary moment of reflection on their stalemate. Both abruptly part ways; the smuggler permits his enemy to escape unarmed into the night.
 security forces. The films storyline closes as Marunga and Swansey confront each other on the battlefield again – this time through the sights of their rifles.

==Cast==
* Richard Harris – David Swansey
* Richard Roundtree – Gideon Marunga
* Denholm Elliott – Raglan Thistle
* Joan Collins – Nicolle
* Ray Milland – Colonel Brettle
* Sven-Bertil Taube – Larry Prescott 
* Ken Gampu – Sixpence 
* Tony Osoba – Daniel "Danny" Batten 
* Neil Hallett – Tony Knight  Mark Singleton – Sir Benjamin Peckover 
* Alibe Parsons – Alice Kamore 
* Victor Melleney – Mallan 
* Jana Cilliers – Ruth Swansey 
* John Parsonson – Peter Swansey 
* Elaine Proctor – Brigid
* Ndaba Mhlongo – Chowa
* Ian Steadman – Du Preez
* Wilson Dunster – Uffa
* Peter van Dissel – Van Rensburg

==Soundtrack== Jade Warrior.

==Reception==
Though generally well-written and produced, Game for Vultures was not a massive commercial or critical success. Some critics condemned the apparent bias of the plot, which ran counter to the traditionally accepted view of Rhodesias predominantly white government as being a racially oppressive one, while its black nationalist opponents were widely regarded as freedom fighters representing a just cause.
 DVD Region 2 release.

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 