Mere Dost Picture Abhi Baki Hai
{{Infobox film
| name           = Mere Dost Picture Abhi Baki Hai
| image          = MDPABHfilm.jpg
| alt            =  
| caption        = Promotional Poster
| director       = Rajnish Raj Thakur
| producer       = Narendra Singh
  Line producer  = Jai prakash Upadhyay
| writer         = 
| screenplay     = Rajnish Raj Thakur
| story          = 
| starring       = Sunil Shetty Rajpal Yadav Om Puri Udita Goswami
| music          = Sukhwinder Singh Subash Pradhan Parvez Qadir Rajendra Shiv
| cinematography = Surindra Rao
| editing        = Aarif Sheikh 
| studio         = 
| distributor    = FilmyBox Movies
| released       =  
| runtime        = 123 mins
| country        = India
| language       = Hindi
| budget         = 
| gross          = 
}}
Mere Dost Picture Abhi Baki Hai (   . The films title is adapted from a scene of Om Shanti Om featuring Shahrukh Khan saying "Picture abhi baki hai, mere dost (The film isnt over yet, my friend)" apparently.

==Cast==
* Sunil Shetty as Amar Joshi
* Udita Goswami as Mohini
* Rajpal Yadav as Suraj
* Om Puri as Baig Saab
* Neena Gupta as Mummyji
* Rakesh Bedi as Monty
* Akhil Mishra as Guptaji
* Shayan Munshi as Hero
* Kurush Deboo as Producer
* Rajeev Verma as Amars father
* Mumaith Khan as Tina
* Deepak Shirke as Sudama Bhosle
* Shahwar Ali as Hero
* Avtar Gill
* Suresh Menon
* Sonali Sachdeva
* Dinesh Hingoo
* Ali

==Plot==

Picture Abhi Baki Hai is the journey of Amar Joshi (Sunil Shetty) who runs a video library in banaras and aspires to be a film maker. Despite facing objection from his father Amar Joshi decides to sell his video library and joins a film Institute in London.

After completing his course he lands in the city of dreams "Mumbai" to make his film. Suraj (Rajpal Yadav) is a struggling actor doing bit roles in T.V. serials, who Is Amars only connection in Bollywood.

Amar Joshi starts his struggle to make his film by meeting different type of producers who have their own take on Amars story. After many failed attempts He finally bumps into Monty Chadda (Rakesh Bedi) a P.R. Publicity man who sees good potential in Amar and decides to produce his film.

Amar & Monty take help of star secretary Guptaji (Akhil Mishra) to convince Mohini (Udita Goswami) & her starry mother Mummyji (Neena Gupta) who agrees to do the film.

Amars film starts, But is stalled all of a sudden when Monty suddenly disappears. Amar is summoned by Sudama Bhosle (Deepak Shirke) a don who was financing his film. He wants Amar to complete his film and also wants him to cast his girlfriend "Tina" (Mummait Khan) Amar reluctantly agrees as he has no option. But as luck would have it Sudama Bhosle is shot dead and Amars film is stalled once again. Monty Chaddha resurfaces again and decides to make Amars Film into a magnum opus. so a veteran silver jubilee writer Mr. Baig (Om Puri) is brought on board. Once in, Mr. Baig takes over the reins of the film from Amar and changes it into a totally different film all together.

What follows is a chain of events which gives the audience a never seen before insight into the modus operadi of how films are generally made in Bollywood. In a nutshell, Picture Abhi Baki Hai will take you on a wild journey inside the dreamy world of Bollywood where you will see the reality of the largest film industry of the world. It wont just make you laugh; it will make you appreciate the efforts of those who try to entertain you every minute.

==References==
 

==External links==
*  
*  

 
 
 