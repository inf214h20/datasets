Lost in Transit (film)
{{Infobox film
| name           = Lost in Transit
| image          = Lost in Transit poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Donald Crisp
| producer       = Julia Crawford Ivers
| screenplay     = Gardner Hunting Kathlyn Williams
| starring       = George Beban Helen Jerome Eddy Pietro Sosso Vera Lewis Henry A. Barrows Frank Bennett
| music          = 
| cinematography = Faxon M. Dean 
| editing        = 
| studio         = Pallas Pictures
| distributor    = Paramount Pictures
| released       =  
| runtime        = 50 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =
}}
 drama silent film directed by Donald Crisp and written by Gardner Hunting and Kathlyn Williams. The film stars George Beban, Helen Jerome Eddy, Pietro Sosso, Vera Lewis, Henry A. Barrows and Frank Bennett. The film was released on September 3, 1917, by Paramount Pictures.  
 
==Plot==
 

== Cast ==
*George Beban as Niccolo Darini
*Helen Jerome Eddy as Nita Lapi
*Pietro Sosso as Lapi
*Vera Lewis as Mrs. Flint
*Henry A. Barrows as Mr. Kendall
*Frank Bennett as Paolo Marso
*Robert White as Baby

== References ==
 

== External links ==
*  
*  

 
 
 
 
 
 
 

 