Lawrence of Arabia (film)
 
 
 
{{Infobox film
| name           = Lawrence of Arabia
| image          = Lawrence of arabia ver3 xxlg.jpg
| caption        = Theatrical release poster
| director       = David Lean
| producer       = Sam Spiegel
| screenplay     = {{Plain list|
* Robert Bolt Michael Wilson
}}
| starring       = {{Plain list| 
* Alec Guinness
* Anthony Quinn
* Jack Hawkins
* José Ferrer
* Anthony Quayle
* Claude Rains Arthur Kennedy
* Omar Sharif
* Peter OToole
 
}}
| music          = Maurice Jarre
| cinematography = Freddie Young|F.A. Young
| editing        = Anne V. Coates
| studio         = Horizon Pictures
| distributor    = Columbia Pictures
| released       =  
| runtime        = 222 minutes
| country        = United Kingdom
| language       = English
| budget         = $15 million   
| gross          = $70 million 
}}
 epic adventure adventure drama Michael Wilson. greatest and most influential films in the history of cinema. The dramatic score by Maurice Jarre and the Super Panavision 70 cinematography by Freddie Young are also highly acclaimed.  The film was nominated for ten Academy Awards and won seven in total including Best Director, Best Sound Editing, Best Film Editing, and Best Picture.
 Britain and its army and his newfound comrades within the Arabian desert tribes.

==Plot summary==
The film is presented in two parts, separated by an intermission.

===Part I===
In 1935, T. E. Lawrence is killed in a motorcycle accident. At his memorial service at St Pauls Cathedral, a reporter tries to gain insights into this remarkable, enigmatic man from those who knew him, with little success.
 lieutenant stationed General Murray, Prince Faisal his revolt Bedouin guide is killed by Sherif Ali for drinking from a well without permission. Lawrence later meets Colonel Brighton, who orders him to keep quiet, make his assessment of Faisals intentions, and leave. Lawrence promptly ignores Brightons commands when he meets Faisal. His knowledge, attitude and outspokenness pique the Princes interest.

Brighton advises Faisal to retreat to Yenbo after a major defeat, but Lawrence proposes a daring surprise attack on Aqaba which, if successful, would provide a port from which the British could offload much-needed supplies. While strongly fortified against a naval assault, the town is lightly defended on the landward side. He convinces Faisal to provide fifty men, led by a sceptical  Sherif Ali. Two teenage orphans, Daud and Farraj, attach themselves to Lawrence as his servants. They cross the Nefud Desert, considered impassable even by the Bedouins, travelling day and night on the last stage to reach water. Gasim succumbs to fatigue and falls off his camel unnoticed during the night. The rest make it to an oasis, but Lawrence turns back for the lost man and against all odds brings him back. Sherif Ali, won over, burns Lawrences British uniform and gives him Arab robes to wear.
 General Allenby, major and British have designs on Arabia. Pressed, the general states they have no such designs.

===Part II=== flogged and possibly raped. He is then thrown out into the street. The experience traumatises Lawrence, who attempts to return to the British headquarters in Cairo, but he never fits in there. A short time later in Jerusalem, Allenby urges him to support the generals "big push" on Damascus, but Lawrence is a changed, tormented man, unwilling to return. After Allenby insists that Lawrence has a destiny, he finally relents. Lawrence naively believes that the warriors will come for him rather than for money.
 colonel and immediately ordered home, his usefulness at an end to both Faisal and the British diplomats, a dejected Lawrence is driven away in a staff car.

==Cast== Thomas Edward Pictures of Lawrence suggest also that OToole carried some resemblance to him, in spite of their considerable height difference. OTooles looks prompted a different reaction from Noël Coward, who after seeing the première of the film quipped "If you had been any prettier, the film would have been called Florence of Arabia".  Prince Faisal. Faisal was originally to be portrayed by Laurence Olivier; Guinness, who performed in other David Lean films, got the part when Olivier dropped out. Guinness was made up to look as much like the real Faisal as possible; he recorded in his diaries that, while shooting in Jordan, he met several people who had known Faisal who actually mistook him for the late prince. Guinness said in interviews that he developed his Arab accent from a conversation he had with Omar Sharif. Auda abu Tayi. Quinn got very much into his role; he spent hours applying his own makeup, using a photograph of the real Auda to make himself look as much like him as he could. One anecdote has Quinn arriving on-set for the first time in full costume, whereupon Lean, mistaking him for a native, asked his assistant to ring Quinn and notify him that they were replacing him with the new arrival. General Allenby. Sam Spiegel pushed Lean to cast Cary Grant or Laurence Olivier (who was engaged at the Chichester Festival Theatre, and declined). Lean, however, convinced him to choose Hawkins because of his work for them on The Bridge on the River Kwai. Hawkins shaved his head for the role and reportedly clashed with David Lean several times during filming. Alec Guinness recounted that Hawkins was reprimanded by Lean for celebrating the end of a days filming with an impromptu dance. Hawkins became close friends with OToole during filming, and the two often improvised dialogue during takes, much to Leans dismay.
* Omar Sharif as Sherif Ali. The role was offered to many actors before Omar Sharif was cast. Horst Buchholz was the first choice, but had already signed on for the film One, Two, Three. Alain Delon had a successful screen test, but ultimately declined because of the brown contact lenses he would have had to wear. Maurice Ronet and Dilip Kumar were also considered.  Sharif, who was already a major star in the Middle East, was originally cast as Lawrences guide Tafas, but when the above actors proved unsuitable, Sharif was shifted to the part of Ali.
* José Ferrer as the Turkish Bey. Ferrer was initially unsatisfied with the small size of his part, and accepted the role only on the condition of being paid $25,000 (more than OToole and Sharif combined) plus a factory-made Porsche.  However, he afterwards considered this his best film performance, saying in an interview: "If I was to be judged by any one film performance, it would be my five minutes in Lawrence." Peter OToole once said that he learned more about screen acting from Ferrer than he could in any acting class.
* Anthony Quayle as Colonel Harry Brighton. Quayle, a veteran of military roles, was cast after Jack Hawkins, the original choice, was shifted to the part of Allenby. Quayle and Lean argued over how to portray the character, with Lean feeling Brighton to be an honourable character, while Quayle thought him an idiot.
* Claude Rains as Mr. Dryden.  Arthur Kennedy as Jackson Bentley. In the early days of the production, when the Bentley character had a more prominent role in the film, Kirk Douglas was considered for the part; Douglas expressed interest but demanded a star salary and the highest billing after OToole, and thus was turned down by Spiegel. Later, Edmond OBrien was cast in the part.  OBrien filmed the Jerusalem scene, and (according to Omar Sharif) Bentleys political discussion with Ali, but he suffered a heart attack on location and had to be replaced at the last moment by Kennedy, who was recommended to Lean by Anthony Quinn.  General Murray.  Michel Ray The Brave One and Anthony Manns The Tin Star. 
* I. S. Johar as Gasim. Johar was a well-known Bollywood actor who occasionally appeared in international productions.
* Zia Mohyeddin as Tafas. Mohyeddin was one of Pakistans best-known actors.
* Gamil Ratib as Majid. Ratib was a veteran Egyptian actor. His English was not considered good enough, so he was dubbed by Robert Rietti in the final film.
* Ian MacNaughton as Michael George Hartley, Lawrences companion in OTooles first scene. 
* John Dimech as Daud.  Hugh Miller as the RAMC colonel. Miller worked on several of Leans films as a dialogue coach, and was one of several members of the film crew to be given bit parts (see below).
* Fernando Sancho as the Turkish sergeant. 
* Stuart Saunders as the regimental sergeant major
* Jack Gwillim as the club secretary. Gwillim was recommended to Lean for the film by close friend Anthony Quayle.
* Kenneth Fortescue as Allenbys aide
* Harry Fowler as Corporal Potter
* Howard Marion-Crawford as the medical officer. Marion-Crawford was cast at the last possible minute, during the filming of the "Damascus" scenes in Seville.
* John Ruddock as Elder Harith.
* Norman Rossington as Corporal Jenkins
* Jack Hedley as a reporter
* Henry Oscar as Silliam, Faisals servant.
* Peter Burton as a Damascus sheik

Various members of the films crew portrayed minor characters. First assistant director Roy Stevens played the truck driver who transports Lawrence and Farraj to the Cairo HQ at the end of Act I; the Sergeant who stops Lawrence and Farraj ("Where do you think youre going to, Mustapha?") is construction assistant, Fred Bennett; and screenwriter Robert Bolt has a wordless cameo as one of the officers watching Allenby and Lawrence confer in the courtyard (he is smoking a pipe). Steve Birtles, the films gaffer, plays the motorcyclist at the Suez Canal; David Lean himself is rumored to be the voice shouting "Who are you?" Finally, continuity girl Barbara Cole appears as one of the nurses in the Damascus hospital scene.

; Nonfictional characters
* T. E. Lawrence Prince Faisal
* Auda ibu Tayi General Allenby General Murray
* Farraj and Daud, Lawrences servants
* Gasim, the man Lawrence rescues from the desert
* Talal, the man who charges the Turkish column at Tafas

; Fictional characters
* Sherif Ali: A combination of numerous Arab leaders, particularly Sharif Nassir—Faisal I of Iraq|Faisals cousin—who led the Harith  forces involved in the attack on Aqaba. The character was created largely because Lawrence did not serve with any one Arab leader (aside from Auda ibu Tayi|Auda) throughout the majority of the war; most such leaders were amalgamated in Alis character. This character was, however, almost certainly named after Sharif Ali ibn Hussein, a leader in the Harith tribe, who played a part in the Revolt and is mentioned and pictured in T.E. Lawrences memoir Seven Pillars of Wisdom. Sir Ronald Henry McMahon, who historically fulfilled Drydens role as a political liaison. He was created by the screenwriters to "represent the civilian and political wing of British interests, to balance Allenbys military objectives."
* Colonel Brighton: In essence a composite of all of the British officers who served in the Middle East with Lawrence, most notably Lt. Col.   referred to his character as an "idiot".)
 ]]
* Turkish Bey: The Turkish Bey who captures Lawrence in Deraa was—according to Lawrence himself—General Hajim Bey (in Turkish, Hacim Muhiddin Bey), though he is not named in the film. Though the incident was mentioned in Lawrences autobiography Seven Pillars of Wisdom, some biographers (Jeremy Wilson, John Mack) argue that Lawrences account is to be believed; others (Michael Asher, Lawrence James) argue that contemporary evidence suggests that Lawrence never went to Deraa at this time and that the story is invented. Michael Wilsons original script, but Robert Bolt reduced his role significantly for the final script. Thomas did not start reporting on Lawrence until after the end of World War I, and held Lawrence in high regard, unlike Bentley, who seems to view Lawrence in terms of a story he can write about.
* Tafas: Lawrences guide to Faisal is based on his actual guide, Sheikh Obeid el-Rashid, of the Hazimi branch of the Beni Salem, whom Lawrence referred to as Tafas several times in Seven Pillars. Tafas and Lawrence did meet Sherif Ali at a well during Lawrences travels to Faisal, but the encounter was not fatal for either party. (Indeed, this scene created much controversy among Arab viewers.)
* Medical officer: This unnamed officer who confronts Lawrence in Damascus is based on an actual incident in Seven Pillars of Wisdom. Lawrences meeting the officer again while in British uniform was, however, an invention of Wilson or Bolt.

==Historical accuracy==
Most of the films characters are either real or based on real characters to varying degrees. The events depicted in the film are largely based on accepted historical fact and Lawrences own writing about events, though they have various degrees of romanticisation.
 attack on Faisal in 1920. Little background on the history of the region, the First World War, and the Arab Revolt is provided, probably because of Bolts increased focus on Lawrence (while Wilsons draft script had a broader, more politicised version of events). The theme (in the second half of the film) that Lawrences Arab army deserted almost to a man as he moved further north was completely fictional. The films timeline of the Arab Revolt and World War I, and the geography of the Hejaz region, are frequently questionable. For instance, Bentley interviews Faisal in late 1917, after the fall of Aqaba, saying the United States has not yet entered the war, yet America had been in the war for several months by that time. Further, Lawrences involvement in the Arab Revolt prior to the attack on Aqaba—such as his involvement in the seizures of Yenbo and Wejh—is completely excised. The rescue and execution of Gasim is based on two separate incidents, which were conflated for dramatic reasons. The film shows Lawrence representing the Allied cause in the Hejaz almost alone with only one British officer, Colonel Brighton (Anthony Quayle) there to assist him. In fact, there were numerous British officers such as colonels Cyril Wilson, Stewart Francis Newcombe and Pierce C. Joyce, all of whom arrived before Lawrence began serving in Arabia.  In addition, there was a French military mission led by Colonel Edouard Brémond serving in the Hejaz, of which no mention is made in the film.  The film shows Lawrence as the sole originator of the attacks on the Hejaz railroad. The first attacks on this began in early January 1917 led by officers such as Newcombe.  The first successful attack on the Hejaz railroad with a locomotive-destroying "Garland mine" was led by Major H. Garland in February 1917, a month before Lawrences first attack.  The film shows the Hashemite forces as comprising Bedouin guerrillas, whereas in fact the core of the Hashemite forces was the Regular Arab Army recruited from Ottoman Arab POWs, who wore British-style uniforms with keffiyahs and fought in conventional battles.  The film makes no mention of the Sharifian Army, and leaves the viewer with the impression that the Hashemite forces were composed exclusively of Bedouin irregulars.

===Representation of Lawrence===
  as T. E. Lawrence]]
Many complaints about the films accuracy centre on the characterisation of Lawrence. The perceived problems with the portrayal begin with the differences in his physical appearance: the 6-foot 2-inch (1.87 m) Peter OToole was almost nine inches (23&nbsp;cm) taller than the man he played.  His behaviour, however, has caused much more debate.
 assumed names, is a matter of debate. Even during the war, Lowell Thomas wrote in With Lawrence in Arabia that he could take pictures of him only by tricking him, although Lawrence did later agree to pose for several photos for Thomass stage show. Thomass famous comment that Lawrence "had a genius for backing into the limelight" referred to the fact that his extraordinary actions prevented him from being as private as he would have liked. Others disagree, pointing to Lawrences own writings in Seven Pillars of Wisdom to support the argument that he was egotistical.

Lawrences sexual orientation remains a controversial topic among historians; though Bolts primary source was ostensibly Seven Pillars, the films portrayal seems informed by Richard Aldingtons then-recent Biographical Inquiry (1955), which posited among other things that Lawrence was homosexual. The film features Lawrences alleged sadomasochism as a major part of his character (for instance, his "match trick" in Cairo, his "enjoyment" of killing Gasim). While Lawrence almost certainly engaged in flagellation and like activities after the Deraa incident, there is no biographical evidence he was a masochist before then. The films depiction of Lawrence as an active participant in the Tafas Massacre was disputed at the time by historians, including biographer Basil Liddell Hart, but most current biographers accept the films portrayal of the massacre as reasonably accurate.
 Kut in Mesopotamia in 1916.

Furthermore, in the film, Lawrence is only made aware of the Sykes–Picot Agreement very late in the story and is shown to be appalled by it, whereas the "real" Lawrence, while fighting alongside the Arabs, knew about it much earlier. 

Lawrences biographers have had a mixed reaction towards the film. Authorised biographer Jeremy Wilson noted that the film has "undoubtedly influenced the perceptions of some subsequent biographers", such as the depiction of the films Ali as the real Sherif Ali, rather than a composite character, and also the highlighting of the Deraa incident.  (In fairness to Lean and his writers, the Deraa connection was made by several Lawrence biographers, including Edward Robinson (Lawrence the Rebel) and Anthony Nutting (The Man and the Motive) before the films release). The films historical inaccuracies are, in Wilsons view, more troublesome than what can be allowed under normal dramatic license. At the time, Liddell Hart publicly criticised the film, engaging Bolt in a lengthy correspondence over its portrayal of Lawrence. 
 cinematic white saviour.   

===Representation of other characters===
The film portrays General Allenby as cynical and manipulative, with a superior attitude to Lawrence, but there is much evidence that Allenby and Lawrence respected and liked each other. Lawrence once said Allenby was "an admiration of mine"  and later that he was "physically large and confident and morally so great that the comprehension of our littleness came slow to him".  In contrast to the fictional Allenbys words at Lawrences funeral in the film, upon Lawrences death Allenby remarked, "I have lost a good friend and a valued comrade. Lawrence was under my command, but, after acquainting him with my strategical plan, I gave him a free hand. His co-operation was marked by the utmost loyalty, and I never had anything but praise for his work, which, indeed, was invaluable throughout the campaign."  Allenby also spoke highly of him numerous times, and, much to Lawrences delight, publicly endorsed the accuracy of Seven Pillars of Wisdom. Although Allenby did manipulate Lawrence during the war, their relationship lasted for years after its end, indicating that in real life they were friendly, if not close. The Allenby family was particularly upset by the Damascus scenes, where Allenby coldly allows the town to fall into chaos as the Arab Council collapses. 

Similarly, General Murray, though initially sceptical of the Arab Revolts potential, thought highly of Lawrences abilities as an intelligence officer; indeed, it was largely through Lawrences persuasion that Murray came to support the revolt. The intense dislike shown toward Lawrence in the film is in fact the opposite of Murrays real feelings, although for his part Lawrence seemed not to hold Murray in any high regard.
 Auda abu Prince Faisal.

Faisal, far from being the middle-aged man depicted, was in reality in his early 30s at the time of the revolt. Faisal and Lawrence respected each others capabilities and intelligence. They worked well together. 

The reactions of those who knew Lawrence and the other characters say much about the films veracity.  Its most vehement critic of its accuracy was Professor A.W. Lawrence|A.W.(Arnold) Lawrence, the protagonists younger brother and literary executor, who had sold the rights to Seven Pillars of Wisdom to Spiegel for £25,000. Arnold Lawrence went on a campaign in the United States and Britain denouncing the film, famously saying, "I should not have recognised my own brother". In one pointed talk show appearance, he remarked that he had found the film "pretentious and false". He went on to say his brother was "one of the nicest, kindest and most exhilarating people I’ve known. He often appeared cheerful when he was unhappy." Later, to the New York Times, Arnold said, "  a psychological recipe. Take an ounce of narcissism, a pound of exhibitionism, a pint of sadism, a gallon of blood-lust and a sprinkle of other aberrations and stir well." Lowell Thomas was also critical of the portrayal of Lawrence and most of the films characters, believing the train attack scenes were the only reasonably accurate aspect of the film.

The criticisms were not restricted to Lawrence. The Allenby family lodged a formal complaint against Columbia about the portrayal of him. Descendants of Auda abu Tayi and the real Sherif Ali, despite the fact that the films Ali was fictional, went further, suing Columbia. The Auda case went on for almost 10 years before it was dropped. 

The film has its defenders. Biographer Michael Korda, author of Hero: The Life and Legend of Lawrence of Arabia, offers a different opinion. While the film is neither "the full story of Lawrences life or a completely accurate account of the two years he spent fighting with the Arabs," Korda argues that criticising its inaccuracy "misses the point": "The object was to produce, not a faithful docudrama that would educate the audience, but a hit picture."  Stephen E. Tabachnick goes further than Korda, arguing that the films portrayal of Lawrence is "appropriate and true to the text of Seven Pillars of Wisdom."  British historian of the Arab Revolt David Murphy wrote that though the film was flawed due to various inaccuracies and omissions, "it was a truly epic movie and is rightly seen as a classic". 

==Production==

===Pre-production===
  Leslie Howard or Robert Donat as Lawrence, but had to pull out owing to financial difficulties. David Lean himself had been approached to direct a 1952 version for the Rank Organisation, but the project fell through. Also, at the same time as pre-production of the film, Terence Rattigan was developing his play Ross (play)|Ross which centred primarily on Lawrences alleged homosexuality. Ross had begun life as a screenplay, but was re-written for the stage when the film project fell through. Sam Spiegel grew furious and unsuccessfully attempted to have the play suppressed, furor at which helped to gain publicity for the film.  Dirk Bogarde had accepted the role in Ross; he described the cancellation of the project as "my bitterest disappointment". Alec Guinness played the role on stage.

Lean and Sam Spiegel were coming off the huge success of The Bridge on the River Kwai, and were eager to work together again. For a time, Lean was interested in a biopic of Gandhi, with Alec Guinness to play the title role and Emeric Pressburger writing the screenplay. Despite extensive pre-production work (including location scouting in India and a meeting with Jawaharlal Nehru), Lean eventually lost interest in the project.  Lean then returned his attention to T.E. Lawrence. Columbia Pictures had an interest in a Lawrence project dating back to the early 50s, and when Spiegel convinced a reluctant A.W. Lawrence to sell the rights to The Seven Pillars of Wisdom for £25,000, the project got underway.

When Lawrence of Arabia was first announced, Lawrences biographer Lowell Thomas offered producer Spiegel and screenwriters Bolt and Wilson a large amount of research material he had produced on Lawrence during and after his time with him in the Arab Revolt. Spiegel rejected the offer. 
 Michael Wilson wrote the original draft of the screenplay. However, Lean was dissatisfied with Wilsons work, primarily because his treatment focused on the historical and political aspects of the Arab Revolt. Lean hired Robert Bolt to re-write the script in order to make it a character study of Lawrence himself. While many of the characters and scenes are Wilsons invention, virtually all of the dialogue in the finished film was written by Bolt.
 The Searchers (1956) to help him develop ideas as to how to shoot the film. Several scenes in the film directly recall Fords film, most notably Alis entrance at the well and the composition of many of the desert scenes and the dramatic exit from Wadi Rum. Lean biographer Kevin Brownlow even notes the physical similarity between Rumm and Fords Monument Valley.  The films plot structure also bears similarity to Orson Welless Citizen Kane (1941), particularly the opening scenes with Lawrences death and the reporter inquiring notables at Lawrences funeral.

===Filming===

 

The film was made by Horizon Pictures and Columbia Pictures. Shooting began on 15 May 1961 and ended on 20 October 1962.
 Toni Gardner, imam be present to ensure that there were no misquotations.

  pavilion of the Parque de María Luisa in Seville appeared as Jerusalem.]]
  Sierra Nevada Plaza de España. All of the interiors were shot in Spain, including Lawrences first meeting with Faisal and the scene in Audas tent.

The Tafas massacre was filmed in Ouarzazate, Morocco, with Moroccan army troops substituting for the Turkish army; however, Lean could not film as much as he wanted because the soldiers were uncooperative and impatient.  One of the second-unit directors for the Morocco scenes was André de Toth, who suggested a shot wherein bags of blood would be machine-gunned, spraying the screen with blood. Second-unit cinematographer Nicolas Roeg approached Lean with this idea, but Lean found it disgusting. De Toth subsequently left the project.
 sign a recognizance of good behaviour for him to be released from jail and continue working on the script.

Camels caused several problems on set. OToole was not used to riding camels and found the saddle to be uncomfortable. While in Amman during a break in filming, he bought a piece of foam rubber at a market and added it to his saddle. Many of the extras copied the idea and sheets of the foam can be seen on many of the horse and camel saddles. The Bedouins nicknamed OToole "Ab al-Isfanjah"    in regards to "Ab al Isfanjah"  . Where is the problem precisely? The Arabic has a laam-alif seating a hamza, and a ta marbuta. So? I have transliterated both hamzas, yielding this: ′Ab al-′Isfanjah, to be hypercorrect.--> ( ), meaning "Father of the Sponge".  The idea spread and to this day, many Bedouins add foam rubber to their saddles.
 Battle of Abu El Lissal in 1917. In another mishap, OToole seriously injured his hand during filming by punching through the window of a caravan while drunk. A brace or bandage can be seen on his left thumb during the first train attack scene, presumably due to this incident.
 Arab nationalism.

===Music=== Sir Adrian Boult is credited as the conductor of the score in the films credits, he could not conduct most of the score, due in part to his failure to adapt to the intricate timings of each cue, and Jarre replaced him as the conductor. The score went on to garner Jarre his first Academy Award for Music Score—Substantially Original  and is now considered one of the greatest scores of all time, ranking number three on the American Film Institutes top twenty-five film scores. 

The original soundtrack recording was originally released on Colpix Records, the records division of Columbia Pictures, in 1962. A remastered edition appeared on Castle Music, a division of the Sanctuary Records Group, on 28 August 2006.

Kenneth Alfords march The Voice of the Guns (1917) is prominently featured on the soundtrack. One of Alfords other pieces, the Colonel Bogey March, was the musical theme for Leans previous film, The Bridge on the River Kwai.

A complete recording of the score was not heard until 2010 when Tadlow Music produced a CD of the music, with Nic Raine conducting the City of Prague Philharmonic from scores reconstructed by Leigh Phillips.

==Release==

===Theatrical run===

The film premiered at the Odeon Leicester Square in London on 10 December 1962 (Royal Premiere) and was released in the United States on 16 December 1962.

The original release ran for about 222&nbsp;minutes (plus overture, intermission, and exit music). A post-premiere memo (13 December 1962) noted that the film was 24,987.5&nbsp;ft (70&nbsp;mm) and 19,990&nbsp;ft (35&nbsp;mm). With 90&nbsp;ft of 35&nbsp;mm film projected every minute, this corresponds to exactly 222.11 minutes.

In an email to Robert Morris, co-author of a book on Lawrence of Arabia, Richard May, VP Film Preservation at Warner Bros., noted that Gone With the Wind, never edited after its premiere, is 19,884&nbsp;ft of 35&nbsp;mm film (without leaders, overture, intermission, entracte or walkout music) corresponding to 220.93 min.

Thus, Lawrence of Arabia, slightly more than 1 minute longer than Gone With the Wind, is the longest movie ever to win a Best Picture Oscar.

In January 1963, Lawrence was released in a version edited by 20 minutes; when it was re-released in 1971, an even shorter cut of 187&nbsp;minutes was presented. The first round of cuts was made at the direction and even insistence of David Lean, to assuage criticisms of the films length and increase the number of showings per day; however, during the 1989 restoration he passed blame for the cuts onto by-then-deceased producer Sam Spiegel.  In addition, a 1966 print, used for initial television and video releases, accidentally altered a few scenes by reversing the image. 

The film was screened out of competition at the 1989 Cannes Film Festival.    and at the 2012 Karlovy Vary International Film Festival.   

===Restored directors cut===
The current "restored version", undertaken by Robert A. Harris and Jim Painten (under the supervision of director David Lean), was released in 1989 with a 216-minute length (plus overture, intermission, and exit music).
 Charles Gray (who had already done Hawkins voice for several films after the former developed throat cancer in the late 1960s). A full list of cuts can be found at the Internet Movie Database.  Reasons for the cuts of various scenes can be found in Leans notes to Sam Spiegel, Robert Bolt, and Anne V. Coates.  The film runs 227&nbsp;minutes in the most recent directors cut available on Blu-ray Disc and DVD. 

===Home media===
Lawrence of Arabia has been released in five different DVD editions, including an initial release as a two-disc set (2001), followed by a shorter single disc edition (2002), a high resolution version of the directors cut with restored scenes (2003) issued as part of the Superbit series, as part of the Columbia Best Pictures collection (2008), and in a fully restored special edition of the directors cut (2008).   

Martin Scorsese and Steven Spielberg helped restore a version of the film for a DVD release in 2000. 

===New restoration, Blu-ray and theatrical re-release=== 8K Motion 4K Digital intermediate digital restoration was made for Blu-ray Disc|Blu-ray and theatrical re-release    during 2012 by Sony Pictures to celebrate the films 50th anniversary.  The Blu-ray edition of the film was released in the United Kingdom on 10 September 2012 and in the United States on 13 November 2012.  The film received a one-day theatrical release on 4 October 2012, a two-day release in Canada on 11 and 15 November 2012, and was also re-released in the United Kingdom on 23 November 2012. 

According to Grover Crisp, executive VP of restoration at Sony Pictures, the new 8K scan has such high resolution that when examined, showed a series of fine concentric lines in a pattern "reminiscent of a fingerprint" near the top of the frame. This was caused by the film emulsion melting and cracking in the desert heat during production. Sony had to hire a third party to minimise or eliminate the rippling artefacts in the new restored version.   The digital restoration was done by Sony Colorworks DI, Prasad Corporation and MTI Film. 
 4K digitally-restored version of the film was screened at the 2012 Cannes Film Festival,   at the 2012 Karlovy Vary International Film Festival,  at the V Janela Internacional de Cinema  in Recife, Brazil, and at the 2013 Cinequest Film Festival in San Jose California. 

==Reception== greatest ever made. Additionally, its visual style has influenced many directors, including George Lucas, Sam Peckinpah, Martin Scorsese, Ridley Scott, and Steven Spielberg, who called the film a "miracle". 
 list of BFI poll of the best British films and in 2001 the magazine Total Film called it "as shockingly beautiful and hugely intelligent as any film ever made" and "faultless".  It has ranked in the top ten films of all time in a Sight and Sound directors poll. Additionally, OTooles performance is often considered one of the greatest in all of cinema, topping lists from both Entertainment Weekly and Premiere Magazine|Premiere. T. E. Lawrence, portrayed by OToole, was selected as the AFIs 100 Years... 100 Heroes and Villains|tenth-greatest hero in cinema history by the American Film Institute. 

In addition, Lawrence of Arabia is currently one of the highest-rated films on Metacritic; it holds a perfect 100/100 rating, indicating "universal acclaim," based on seven reviews. However, some critics—notably Bosley Crowther  and Andrew Sarris —have criticised the film for an indefinite portrayal of Lawrence and lack of depth.

==Awards and honours==
{| class="wikitable"  style="margin:auto; width:100%;"
|-
! Award !! Category !! Name !! Outcome
|- 35th Academy Academy Award Best Picture||Sam Spiegel|| 
|- Academy Award Best Director||David Lean|| 
|- Academy Award Best Art John Box, John Stoll and Dario Simoni||  
|- Academy Award Best Cinematography||Freddie Frederick A. Young|| 
|- Academy Award Best Substantially Maurice Jarre|| 
|- Academy Award Best Film Anne V. Coates|| 
|- Academy Award Best Sound||John John Cox|| 
|- Academy Award Best Actor||Peter OToole|| 
|- Academy Award Best Supporting Omar Sharif|| 
|- Academy Award Best Adapted Robert Bolt Michael Wilson|| 
|- 16th British BAFTA Award Best Film Sam Spiegel and David Lean|| 
|- BAFTA Award Best British Sam Spiegel and David Lean|| 
|- BAFTA Award Best British Peter OToole|| 
|- BAFTA Award Best British Robert Bolt Michael Wilson|| 
|- BAFTA Award Best Foreign Anthony Quinn|| 
|- 20th Golden Golden Globe Best Motion David Lean and Sam Spiegel|| 
|- Golden Globe Best Director David Lean|| 
|- Golden Globe Best Supporting Omar Sharif|| 
|- Golden Globe Most Promising Peter OToole|| 
|- Golden Globe Most Promising Omar Sharif|| 
|- Golden Globe Best Cinematography, Freddie Young|Frederick A. Young|| 
|- Golden Globe Best Actor Peter OToole|| 
|}

==Legacy==
Film director Steven Spielberg considers this his favourite film of all time and the one that convinced him to become a filmmaker. 

In 1991 this film was deemed "culturally, historically, or aesthetically significant" and selected for preservation in the United States Library of Congress National Film Registry. 

In 2013, artist James Georgopoulos included the Super Panavision 70 camera used to shoot Lawrence of Arabia in his popular Cameras of Cinema series.  

==Sequel==
 
In 1990, the made-for-television film A Dangerous Man: Lawrence After Arabia was aired. It depicts events in the lives of Lawrence and Faisal subsequent to Lawrence of Arabia and featured Ralph Fiennes as Lawrence and Alexander Siddig as Prince Faisal.

==See also==
* Cinema of Jordan
* Films considered the greatest ever
* BFI Top 100 British films
* White savior narrative in film

==References==
 
*  
*  

== Further reading ==
* Morris, L. Robert and Raskin, Lawrence (1992). Lawrence of Arabia: The 30th Anniversary Pictorial History. Doubleday & Anchor, New York. A book on the creation of the film, authorised by Sir David Lean.

==External links==
 
 
* 
*   at the American Film Institute Catalog of Motion Pictures
* 
* 
* 
* 

 
 
 
 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 