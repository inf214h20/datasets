The Bridge on the River Kwai
 
{{Infobox film
| name = The Bridge on the River Kwai
| image = The Bridge on the River Kwai poster.jpg
| caption = Original release poster
| director = David Lean
| producer = Sam Spiegel Michael Wilson
| based on =  
| starring = William Holden Alec Guinness Jack Hawkins Sessue Hayakawa
| music = Malcolm Arnold
| cinematography = Jack Hildyard Peter Taylor
| studio = Horizon Pictures
| distributor = Columbia Pictures
| released =   (World Premiere, London)
| runtime = 161 minutes
| country = United Kingdom United States 
| language = English
| budget = $2,840,000. Sheldon Hall, Epics, Spectacles, and Blockbusters: A Hollywood History Wayne State University Press, 2010 p 161 
| gross =$30.6 million (first release) 
}} epic film Le Pont Ceylon (now known as Sri Lanka). The bridge in the film was located near Kitulgala.
 one of the greatest films in history.  

==Plot== Japanese Prisoner-of-war prison camp River Kwai that will connect Bangkok and Rangoon. The senior British officer, Lieutenant Colonel Nicholson (Alec Guinness), reminds Saito that the Geneva Conventions exempt officers from manual labour.
 Major Clipton (James Donald), the British medical officer, intervenes, Saito leaves the officers standing all day in the intense tropical heat. That evening, the officers are placed in a punishment hut, while Nicholson is locked in an iron box.
 Commander Shears (William Holden), gets away, although badly wounded. He stumbles into a village. The villagers help him escape by boat.
 ritual suicide. face and announces a general amnesty, releasing Nicholson and his officers.
 Captain Reeves Major Hughes John Boxer) to design and build a proper bridge, despite its military value to the Japanese, for the sake of maintaining his mens morale. The Japanese engineers had chosen a poor site, so the original construction is abandoned and a new bridge is begun downstream.

Shears is enjoying his hospital stay in Ceylon, when British Major Warden (Jack Hawkins) asks him to volunteer for a commando mission to destroy the bridge before its completed. Shears is appalled at the idea and reveals that he is not an officer at all. He switched uniforms with a dead officer after the sinking of their cruiser, USS Houston (CL-30), as a ploy to get better treatment. Warden already knows this. Faced with the prospect of being charged with impersonating an officer, Shears volunteers.

Meanwhile, Nicholson drives his men hard to complete the bridge on time. For him, its completion will exemplify the ingenuity and hard work of the British Army for generations. When he asks that their Japanese counterparts join in as well, a resigned Saito replies that he has already given the order.
 Canadian Lieutenant Joyce (Geoffrey Horne) reach the river in time with the assistance of Siamese women bearers and their village chief, Khun Yai. Under cover of darkness, Shears and Joyce plant explosives on the bridge towers below the water line.

A train carrying soldiers and important dignitaries is scheduled to be the first to cross the bridge the following day, so Warden waits to destroy both. However, at daybreak the commandos are horrified to see that the water level has dropped, exposing the wire connecting the explosives to the detonator. Making a final inspection, Nicholson spots the wire and brings it to Saitos attention. As the train is heard approaching, they hurry down to the riverbank to investigate.

Joyce, manning the detonator, breaks cover and stabs Saito to death. Aghast, Nicholson yells for help, while attempting to stop Joyce from reaching the detonator. When Joyce is shot dead by Japanese fire, Shears swims across the river, but is fatally wounded as he reaches Nicholson. Recognizing the dying Shears, Nicholson exclaims, "What have I done?" Warden fires his Mortar (weapon)|mortar, mortally wounding Nicholson. The dazed colonel stumbles towards the detonator and collapses on the plunger, just in time to blow up the bridge and send the train hurtling into the river below. Witnessing the carnage, Clipton shakes his head uttering, "Madness!&nbsp;... Madness!"

==Cast==
* William Holden as Commander Shears
* Alec Guinness as Lieutenant Colonel Nicholson
* Jack Hawkins as Major Warden
* Sessue Hayakawa as Colonel Saito
* James Donald as Major Clipton
* Geoffrey Horne as Lieutenant Joyce
* André Morell as Colonel Green
* Peter Williams as Captain Reeves John Boxer as Major Hughes Percy Herbert as Private Grogan Harold Goodwin as Private Baker

==Historical parallels==
  in June 2004. The round truss spans are the originals; the angular replacements were supplied by the Japanese as war reparations.]] Khwae Yai Thai town of Kanchanaburi.

According to the Commonwealth War Graves Commission:
 
"The notorious Burma-Siam railway, built by Commonwealth of Nations|Commonwealth, Dutch and American prisoners of war, was a Japanese project driven by the need for improved communications to support the large Japanese army in Burma. During its construction, approximately 13,000 prisoners of war died and were buried along the railway. An estimated 80,000 to 100,000 civilians also died in the course of the project, chiefly forced labour brought from Malaya and the Dutch East Indies, or conscripted in Siam (Thailand) and Burma. Two labour forces, one based in Siam and the other in Burma, worked from opposite ends of the line towards the centre." 
 
 conditions were much worse than depicted.  The real senior Allied officer at the bridge was British Lieutenant Colonel Philip Toosey. Some consider the film to be an insulting parody of Toosey. 
   On a BBC Timewatch programme, a former prisoner at the camp states that it is unlikely that a man like the fictional Nicholson could have risen to the rank of lieutenant colonel; and if he had, due to his collaboration he would have been "quietly eliminated" by the other prisoners. Julie Summers, in her book The Colonel of Tamarkan, writes that Pierre Boulle, who had been a prisoner of war in Thailand, created the fictional Nicholson character as an amalgam of his memories of collaborating French officers.  He strongly denied the claim that the book was anti-British, although many involved in the film itself (including Alec Guinness) felt otherwise. 

Toosey was very different from Nicholson and was certainly not a collaborator who felt obliged to work with the Japanese. Toosey in fact did as much as possible to delay the building of the bridge.  While Nicholson disapproves of acts of sabotage and other deliberate attempts to delay progress, Toosey encouraged this: termites were collected in large numbers to eat the wooden structures, and the concrete was badly mixed.    

In an interview that forms part of the 1969 BBC2 documentary "Return to the River Kwai" made by former POW John Coast, Boulle outlined the reasoning that led him to conceive the character of Nicholson. A transcript of the interview and the documentary as a whole can be found in the new edition of John Coasts book Railroad of Death.  Coasts documentary sought to highlight the real history behind the film (partly through getting ex-POWs to question its factual basis, for example Dr Hugh de Wardener and Lt-Col Alfred Knights), which had angered so many former POWs.  The documentary itself was described by one newspaper reviewer when it was shown on Boxing Day 1974 (The Bridge on the River Kwai had been shown on BBC1 on Christmas Day 1974) as "Following the movie, this is a rerun of the antidote." {{cite news
 | title = Boxing Day  
 | pages = 14
 | newspaper = The Guardian
 | location = London
 | date = 24 December 1974
}} 

Some of the characters in the film use the names of real people who were involved in the Burma Railway. Their roles and characters, however, are fictionalised. For example, a Sergeant-Major Risaburo Saito was in real life second in command at the camp. In the film, a Colonel Saito is camp commandant. In reality, Risaburo Saito was respected by his prisoners for being comparatively merciful and fair towards them; Toosey later defended him in his war crimes trial after the war, and the two became friends.

The destruction of the bridge as depicted in the film is entirely fictional. In fact, two bridges were built: a temporary wooden bridge and a permanent steel/concrete bridge a few months later. Both bridges were used for two years, until they were destroyed by Allied aerial bombing. The steel bridge was repaired and is still in use today.

==Japanese views of the book and movie==

The Japanese resented the conclusion in the  movie that their engineers were less capable than British engineers.  In fact, Japanese engineers had been surveying the route of the railway since 1937 and they were highly organized.   The Japanese also resented the "glorification of the superiority of Western civilization" represented in the movie by the British being able to build a bridge that the Japanese could not. 

==Production==

===Screenplay===
 
 Michael Wilson, credit was Academy rectify the situation by retroactively awarding the Oscar to Foreman and Wilson, posthumously in both cases. Subsequent releases of the film finally gave them proper screen credit. David Lean himself also claimed that producer Sam Spiegel cheated him out of his rightful part in the credits since he had had a major hand in the script. 

The film was relatively faithful to the novel, with two major exceptions. Shears, who is a British commando officer like Warden in the novel, became an American sailor who escapes from the POW camp. Also, in the novel, the bridge is not destroyed: the train plummets into the river from a secondary charge placed by Warden, but Nicholson (never realising "what have I done?") does not fall onto the plunger, and the bridge suffers only minor damage. Boulle nonetheless enjoyed the film version though he disagreed with its climax.

===Filming===
  in Sri Lanka, before the explosion]]
  in Sri Lanka (photo taken 2004), where the bridge was made for the film.]]
Many directors were considered for the project, among them: John Ford, William Wyler, Howard Hawks, Fred Zinnemann and Orson Welles. 
 Britain and the United States. {{cite book|author=Monaco, Paul|title=A History of American Movies: A Film-by-film Look at the Art, Craft, and Business of Cinema
|url=http://books.google.com/books?id=tgnKY6k5tHYC&pg=PA149|year=2010|publisher=Scarecrow Press|page=349}}  It is set in Thailand, but was filmed mostly near Kitulgala, Ceylon (now Sri Lanka), with a few scenes shot in England.

Director David Lean clashed with his cast members on multiple occasions, particularly Alec Guinness and James Donald, who thought the novel was anti-British. Lean had a lengthy row with Guinness over how to play the role of Nicholson; Guinness wanted to play the part with a sense of humour and sympathy, while Lean thought Nicholson should be "a bore." On another occasion, Lean and Guinness argued over the scene where Nicholson reflects on his career in the army. Lean filmed the scene from behind Guinness, and exploded in anger when Guinness asked him why he was doing this. After Guinness was done with the scene, Lean said "Now you can all fuck off and go home, you English actors. Thank God that Im starting work tomorrow with an American actor (William Holden)." 
 Matthew when Matthew was recovering from polio. Guinness called his walk from the Oven to Saitos hut while being saluted by his men the "finest work Id ever done." 

  and Chandran Rutnam while shooting The Bridge on the River Kwai.]]

Lean nearly drowned when he was swept away by a river current during a break from filming; Geoffrey Horne saved his life. 

The filming of the bridge explosion was to be done on 10 March 1957, in the presence of Solomon Bandaranaike|S.W.R.D. Bandaranaike, then Prime Minister of Ceylon, and a team of government dignitaries. However, cameraman Freddy Ford was unable to get out of the way of the explosion in time, and Lean had to stop filming. The train crashed into a generator on the other side of the bridge and was wrecked. It was repaired in time to be blown up the next morning, with Bandaranaike and his entourage present. 

According to the supplemental material in the Blu-ray digipak, a thousand tons of explosives were used to blow up the bridge. This is highly unlikely, as the film shows roughly 50&nbsp;kg of plastic explosive being used simply to knock down the bridges supports.

According to Turner Classic Movies, the producers nearly suffered a catastrophe following the filming of the bridge explosion. To ensure they captured the one-time event, multiple cameras from several angles were used. Ordinarily, the film would have been taken by boat to London, but due to the Suez crisis this was impossible; therefore the film was taken by air freight. When the shipment failed to arrive in London, a worldwide search was undertaken. To the producers horror the film containers were found a week later on an airport tarmac in Cairo, sitting in the hot Egyptian sun. Although it was not exposed to sunlight, the heat-sensitive colour film stock should have been hopelessly ruined; however, when processed the shots were perfect and appeared in the film.

===Music===
A memorable feature of the film is the tune that is whistled by the POWs—the first strain of the march "Colonel Bogey"—when they enter the camp.  The march was written in 1914 by Kenneth J. Alford, a pseudonym of British Bandmaster Frederick J. Ricketts. The Colonel Bogey strain was accompanied by a counter-melody using the same chord progressions, then continued with film composer Malcolm Arnolds own composition, "The River Kwai March," played by the off-screen orchestra taking over from the whistlers, though Arnolds march was not heard in completion on the soundtrack. Mitch Miller had a hit with a recording of both marches.

Besides serving as an example of British fortitude and dignity in the face of privation, the "Colonel Bogey March" suggested a specific symbol of defiance to British film-goers, as its melody was used for the song "Hitler Has Only Got One Ball." Lean wanted to introduce Nicholson and his soldiers into the camp singing this song, but Sam Spiegel thought it too vulgar, and whistling was substituted. However, the lyrics were, and continue to be, so well known to the British public that they didnt need to be laboured.

The soundtrack of the film is largely diegesis|diegetic; background music is not widely used. In many tense, dramatic scenes, only the sounds of nature are used. An example of this is when commandos Warden and Joyce hunt a fleeing Japanese soldier through the jungle, desperate to prevent him from alerting other troops.

Arnold won an Academy Award for the films score.
 Lawrence of Arabia.

==Box office performance== Peyton Place at $12,000,000; in third place was Sayonara at $10,500,000. 

The movie was re-released in 1964 and earned an estimated $2.6 million in North American rentals. 

==Awards==

===Academy Awards===
The Bridge on the River Kwai won seven Oscars: Best Picture — Sam Spiegel Best Director — David Lean Best Actor — Alec Guinness Best Writing, Michael Wilson, Carl Foreman, Pierre Boulle Best Music, Scoring of a Dramatic or Comedy Film — Malcolm Arnold Best Film Peter Taylor Best Cinematography — Jack Hildyard
It was nominated for Best Actor in a Supporting Role — Sessue Hayakawa

===BAFTA Awards===
Winner of 3 BAFTA Awards Best British Film — David Lean, Sam Spiegel Best Film from any Source — David Lean, Sam Spiegel Best British Actor — Alec Guinness

===Golden Globe Awards=== Golden Globes Best Motion Picture – Drama — David Lean, Sam Spiegel Best Director — David Lean Best Actor – Drama — Alec Guinness
Recipient of one nomination Best Supporting Actor — Sessue Hayakawa

===Other awards=== New York Film Critics Circle Awards for Best Film
*  ) New York Film Critics Circle Awards for Best Director (David Lean) New York Film Critics Circle Awards for Best Actor (Alec Guinness)

===Other nominations=== Grammy Award for Best Soundtrack Album, Dramatic Picture Score or Original Cast (Malcolm Arnold)

===Recognition===
The film has been selected for preservation in the United States National Film Registry.
 The Dam Busters.

The British Film Institute placed The Bridge on the River Kwai as the eleventh greatest British film.

====American Film Institute recognition====
* 1998 — AFIs 100 Years... 100 Movies — #13
* 2001 — AFIs 100 Years... 100 Thrills — #58
* 2003-   AFIs 100 Years... 100 Heroes and Villains
** Lieutenant Shears- Nominated Villain 
* 2005 — AFIs 100 Years... 100 Movie Quotes
** "Madness. Madness" — Nominated
* 2006 — AFIs 100 Years... 100 Cheers — #14
* 2007 — AFIs 100 Years... 100 Movies (10th Anniversary Edition) — #36

==First TV broadcast== Lawrence of Arabia, but that broadcast was split into two parts over two evenings, due to the films nearly four-hour length. 

==Restorations==
The film was restored in 1985 by Columbia Pictures. The separate dialogue, music and effects were located and remixed with newly recorded "atmospheric" sound effects.    The image was restored by OCS, Freeze Frame, and Pixel Magic with George Hively editing. 

On 2 November 2010 Columbia Pictures released a newly restored The Bridge on the River Kwai for the first time on Blu-ray. According to Columbia Pictures, they followed an all-new 4K digital restoration from the original negative with newly restored 5.1 audio.  The original negative for the feature was scanned at 4k (roughly four times the resolution in High Definition), and the colour correction and digital restoration were also completed at 4k. The negative itself manifested many of the kinds of issues one would expect from a film of this vintage: torn frames, imbedded emulsion dirt, scratches through every reel, colour fading. Unique to this film, in some ways, were other issues related to poorly made optical dissolves, the original camera lens and a malfunctioning camera. These problems resulted in a number of anomalies that were very difficult to correct, like a ghosting effect in many scenes that resembles color mis-registration, and a tick-like effect with the image jumping or jerking side-to-side. These issues, running throughout the film, were addressed to a lesser extent on various previous DVD releases of the film and might not have been so obvious in standard definition. 

==In popular culture== Tamil film director, saw the shooting of this film at Kitulgala, Sri Lanka during his school trip and was inspired to become a film director. 

* In Andrew Lloyd Webbers musical Starlight Express, CB the Red Caboose mentions that "the State Police, they dont suspect I got Old 97 wrecked, Interpol dont know that I crossed the bridge on the river Kwai!".

*In the 2001 strategy game,  , developed by Pyro Studios and published by Eidos Interactive, one of the missions was named after the films title as an homage. In addition, one of the characters, named Guinness, is based on Alec Guinness, the star of the movie.

===Parodies===
* In the 1958 motion picture The Geisha Boy, comedian Jerry Lewis plays a magician traveling in Japan to entertain GIs. During his visit, Lewiss character inadvertently becomes close with an orphaned Japanese boy. Sessue Hayakawa, playing the orphans grandfather, reenacts a scene that he performed in The Bridge on the River Kwai: his workers are building a small bridge that greatly resembles the one in that film and whistling the familiar "Colonel Bogey March". When Lewis stares in wonder at Hayakawa and the bridge he is building in his backyard, Hayakawa acknowledges that others have mistaken him for "the actor" and then says, "I was building bridges long before he was." This is followed by a brief clip of Alec Guinness from the film.
 LP record Bridge On the River Wye (Parlophone LP PMC 1190,PCS 3036 (November 1962)). This spoof of the film was based on the script for the 1957 Goon Show episode "An African Incident". Shortly before its release, for legal reasons, producer George Martin edited out the K every time the word Kwai was spoken. 

* The comedy team of Wayne and Shuster performed a sketch titled "Kwai Me a River" on their 27 March 1967 TV show, in which an officer in the British Dental Corps is captured by the Japanese and, despite being comically unintimidated by any abuse the commander of the POW camp inflicts on him, is forced to build a (dental) "bridge on the river Kwai" for the commander and plans to include an explosive in the appliance to detonate in his mouth. 
 Top Gear lorries and build a bridge over the river Kwai.

==See also==
 
* BFI Top 100 British films
* List of historical drama films
* List of historical drama films of Asia
* To End All Wars (film)
* Return from the River Kwai (1989 film)
* Siam–Burma Death Railway (film)

==References==
 

==Bibliography==
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 

==External links==
 
*  
*  
*  
*  
*  
*  
*  
*  
 
 
 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 