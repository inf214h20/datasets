Anywhere but Here (film)
 
{{Infobox film
| name           = Anywhere but Here
| image          = Anywhere-but-here-movie-poster-1999-1020272455.jpg
| caption        = Movie poster
| director       = Wayne Wang
| producer       = Laurence Mark
| based on       =  
| screenplay     = Alvin Sargent
| narrator       = Natalie Portman
| starring       = Susan Sarandon Natalie Portman Shawn Hatosy
| music          = Danny Elfman
| cinematography = Roger Deakins
| distributor    = 20th Century Fox
| released       =  
| runtime        = 109 minutes
| country        = United States
| language       = English
| budget         = $23 million
| gross          = $23,631,929
}} the novel Mona Simpson. The screenplay was written by Alvin Sargent, and the film was directed by Wayne Wang. It was produced by Laurence Mark, Petra Alexandria, and Ginny Nugent. It stars Susan Sarandon and Natalie Portman. Filming began in late June 1998.  It debuted at the Toronto Film Festival on September 17 in 1999, before being released on November 12.

== Plot == Wisconsin town and moves to Beverly Hills to realize her dreams.  She may not know what those dreams are herself. Adele is confused and always tries to deal with matters the easy way. She buys things she cant afford and her more practical daughter is upset when she does this.

When living in Beverly Hills, Adele improvises from day to day, often unable to pay the bills. She wants her daughter to become an actress, but Ann is much more interested in going away to Brown University. After Adele fails in many respects, she accepts her daughters plans and decides to help her.

== Cast ==
* Susan Sarandon as Adele August
* Natalie Portman as Ann August
* Shawn Hatosy as Benny
* Hart Bochner as Josh Spritzer
* Eileen Ryan as Lillian Ray Baker as Ted John Diehl as Jimmy
* Bonnie Bedelia as Carol
* Faran Tahir as Hisham Badir
* Caroline Aaron as Gail Letterfine
* Corbin Allred as Peter
* Michael Milhoan as Cop
* John Carroll Lynch as Jack Irwin
* Steve Berra as Hal
* Eva Amurri, the daughter of Susan Sarandon, has a cameo appearance as a girl on television.

==Reception==
The film received positive reviews from critics. It currently holds a 64% on Rotten Tomatoes.  The consensus reads: "The clever reversal of roles between Portman and Sarandons characters (Portman is constantly worried and looking out for her mother, not vice versa) makes the movie interesting and worth watching. Transcends the tired cliche well." 

Roger Ebert of the Chicago Sun-Times gave the film three stars and noted "The movies interest is not in the plot, which is episodic and colorful, but in the performances. Sarandon bravely makes Adele into a person who is borderline insufferable. Sarandons role is trickier and more difficult, but Portmans will get the attention. In Anywhere But Here, she gets yanked along by her out of control mother, and her best scenes are when she fights back, not emotionally, but with incisive observations." 

==Filming locations==
* Beverly Hills High School, Beverly Hills, California
* Beverly Hills Hotel & Bungalows, Beverly Hills
* Los Angeles International Airport, Los Angeles, California
* Los Angeles, California
* Westwood Village, Westwood, Los Angeles, California|Westwood, Los Angeles, California,

==Soundtrack==
{{Infobox album
| Name        = Anywhere But Here: Music from the Motion Picture
| Type        = soundtrack
| Artist      = Various Artists
| Cover       = Anywhere But Here soundtrack.png
| Released    = November 2, 1999
| Genre       = Pop
| Label       = Atlantic Records|Atlantic/Warner Music Group|Wea.
| Length      = 59:25
| Producer    = Danny Elfman, Steve Power, Lisa Loeb, Michael Beinhorn, Malcolm Burn, Marius de Vries, Joe Hardy, Jay Joyce, k.d. lang, Pierre Marchand, Rick Nowels, Carmen Rizzo, Glenn Rosenstein, Don Was, Wilbur C. Rimes, Ellen Segal, Pocket Size, Billy Harvey 
| Misc        = {{Singles
  | Name           = Anywhere But Here: Music from the Motion Picture
  | Type           = soundtrack
  | Single 1       = Big Deal (song)|Leavings Not Leaving
  | Single 1 date  =     
}}
}}
{{Album ratings
| rev1 = AllMusic 
| rev1score =  
}}

A soundtrack to the film was released on November 2, 1999,  ten days before the theatrical release. The soundtrack was distributed by Atlantic Records and Warner Music Group|Wea.
 Big Deal", on September 28, 1999. 

{{tracklist
| writing_credits   = no
| extra_column      = Recording artist(s)
| total_length      = 59:25
| title1            = Anywhere But Here
| extra1            = k.d. lang
| length1           = 3:45
| title2            = Walking
| extra2            = Pocket Size
| length2           = 4:14
| title3            = Scream and Shout
| extra3            = 21st Century Girls
| length3           = 3:25
| title4            = Leavings Not Leaving string arrangements David Campbell
| extra4            = LeAnn Rimes
| writer4           = Diane Warren
| length4           = 4:53
| title5            = I Wish
| extra5            = Lisa Loeb
| length5           = 2:27
| title6            = Free Marie Wilson
| length6           = 3:56
| title7            = Amity Sally Taylor
| length7           = 3:17
| title8            = Ice Cream
| extra8            = Sarah McLachlan
| length8           = 2:43
| title9            = Furniture
| extra9            = Kacy Crowley
| length9           = 3:29
| title10           = Twisted Road
| extra10           = Patty Griffin
| length10          = 2:48
| title11           = Strange Wind Poe
| length11          = 3:59
| title12           = Everything Around Me Is Changing
| extra12           = Sinéad Lohan
| length12          = 4:42
| title13           = Come Here
| extra13           = Lili Haydn
| length13          = 4:22
| title14           = Chotee
| extra14           = Bif Naked
| length14          = 3:50
| title15           = Anywhere But Here Score Suite
| extra15           = Danny Elfman
| length15          = 7:35
}}

==Awards and nominations==
Golden Globe Awards
* Nominated - Best Performance by an Actress in a Supporting Role in a Motion Picture - Natalie Portman
Young Artist Awards
* Nominated - Best Performance in a Feature Film - Leading Young Actress - Natalie Portman
Hollywood Makeup Artist and Hair Stylist Guild Awards
* Nominated - Best Contemporary Hair Styling (Feature) - Paul LeBlanc

==Box office==
The film opened at #5 at the North American box office and made $5.6 million USD in its opening weekend.

==References==
 

==External links==
 
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 