Gattaca
 
{{Infobox film
| name           = Gattaca
| image          = Gataca Movie Poster B.jpg
| image_size     =
| alt            =
| caption        = Theatrical release poster
| director       = Andrew Niccol
| producer       = Danny DeVito Michael Shamberg Stacey Sher Gail Lyon
| writer         = Andrew Niccol
| starring       = Ethan Hawke Uma Thurman
| music          = Michael Nyman
| cinematography = Slawomir Idziak
| editing        = Lisa Zeno Churgin
| studio         = Jersey Films
| distributor    = Columbia Pictures
| released       =  
| runtime        = 106 minutes
| country        = United States
| language       = English
| budget         = $36 million
| gross          = $12.5 million 
}} biopunk vision of a future society driven by eugenics where potential children are conceived through genetic manipulation to ensure they possess the best hereditary traits of their parents.  The film centers on Vincent Freeman, played by Hawke, who was conceived outside the eugenics program and struggles to overcome genetic discrimination to realize his dream of traveling into space.

The movie draws on concerns over reproductive technologies which facilitate eugenics, and the possible consequences of such technological developments for society. It also explores the idea of destiny and the ways in which it can and does govern lives. Characters in Gattaca continually battle both with society and with themselves to find their place in the world and who they are destined to be according to their genes.

The films title is based on the first letters of guanine, adenine, thymine, and cytosine, the four nucleobases of DNA.  It was a 1997 nominee for the Academy Award for Best Art Direction and the Golden Globe Award for Best Original Score.
 flopped at the box office, but it received generally positive reviews and has since gained a cult following.

==Plot==
In "the not-too-distant future", eugenics (in the form of conceiving "improved" children by genetic manipulation) is common, and DNA plays the primary role in determining social class.  A genetic registry database uses biometrics to instantly identify and classify those so created as "valids" while those conceived by traditional means and more susceptible to genetic disorders are derisively known as "in-valids". Genetic discrimination is forbidden by law, but in practice genotype profiling is used to identify valids to qualify for professional employment while in-valids are relegated to menial jobs.

Vincent Freeman is conceived naturally without the aid of genetic selection, and immediately after birth, his genetics indicate a high probability of several disorders and an estimated life span of 30.2 years. His parents, regretting their decision, use genetic selection to give birth to their next child Anton who is genetically superior to Vincent. Growing up, the two brothers often play a game of "Chicken (game)|chicken" by swimming out to sea with the first one giving up and returning to shore determined the loser, which Vincent always loses. Vincent dreams of a career in space travel but is reminded of his genetic inferiority. One day, Vincent challenges Anton to a game of chicken and bests him before Anton starts to drown. Vincent saves Anton and then leaves home on his own.

Vincent works as an in-valid, cleaning office spaces including that of Gattaca Aerospace Corporation, a space-flight conglomerate. He gets a chance to become a "borrowed ladder", posing as a valid by using genetic hair, skin, blood and urine samples from a donor, Jerome Eugene Morrow, who is a former swimming star paralyzed by a car accident.  With Jeromes "second to none" genetic makeup, Vincent gains employment at Gattaca, and is assigned to be navigator for an upcoming trip to Saturns moon Titan (moon)|Titan. To keep his identity hidden, Vincent must meticulously groom and scrub down daily to remove his own genetic material, and pass daily DNA scanning and frequent urine tests using Jeromes samples.

Gattaca becomes embroiled in controversy when one of its administrators is murdered a week before the planned flight. The police find a fallen eyelash of Vincents at the scene. An investigation is launched to find the murderer, Vincent being the top suspect. Through this, Vincent becomes close to a co-worker, Irene Cassini, and falls in love with her. Though a valid, Irene is aware that her higher risk of heart failure will prevent her from joining any deep space Gattaca mission. Vincent also learns more about Jerome, and discovers that his paralysis is by his own hand; after coming in second place in a swim meet, Jerome became depressed and threw himself in front of a car to attempt suicide.

Vincent is able to repeatedly evade scrutiny from the investigation, and soon it is revealed that Gattacas mission director was the killer, as the administrator was threatening to pull the mission. Vincent learns the identity of the detective that closed the case, his brother Anton, who has become aware of Vincents presence. The brothers meet, and Anton warns Vincent what he is doing is illegal, but Vincent asserts he has gotten to this position on his own, and did not need help as Anton did the last time they played chicken. Anton challenges Vincent to one more game of chicken. As the two swim out in the dead of night, Anton is surprised at Vincents stamina, and Vincent reveals that his trick to winning was never saving any energy for the swim back. Anton turns back and starts to drown, but Vincent rescues him and swims them both back to shore safely using celestial navigation.

The day of the launch arrives. Jerome reveals that he has stored enough DNA samples for Vincent to last several lifetimes upon his return and gives him an envelope to open once in flight. After saying his goodbyes to Irene, Vincent prepares to board but discovers there is one final genetic test before he can board, and he currently lacks any of Jeromes samples. He is surprised when Dr. Lamar, the person in charge of background checks, reveals that he has been aware that Vincent has been posing as a valid. Lamar admits that his son looks up to Vincent and wonders if his son, genetically selected but "not all that they promised", could break the limits just as Vincent has. He then wipes the information from the test, passing Vincent as a valid. As the rocket launches, Jerome dons his swimming medal and burns himself in his homes incinerator; Vincent opens the note from Jerome to find only a lock of Jeromes hair attached to it. Vincent muses on this, stating "For someone not meant for this world, I must confess, I’m suddenly having a hard time leaving it. Of course, they say every atom in our bodies was once a part of a star. Maybe Im not leaving; maybe Im going home."

==Cast==
* Ethan Hawke as Vincent Anton Freeman, impersonating Jerome Eugene Morrow
** Mason Gamble as young Vincent
** Chad Christ as teenage Vincent
* Uma Thurman as Irene Cassini
* Jude Law as Jerome Eugene Morrow
* Loren Dean as Anton Freeman
** Vincent Nielson as young Anton
** William Lee Scott as teenage Anton
* Gore Vidal as Director Josef
* Xander Berkeley as Dr. Lamar
* Jayne Brook as Marie Freeman
* Elias Koteas as Antonio Freeman
* Maya Rudolph as Delivery nurse
* Blair Underwood as Geneticist
* Ernest Borgnine as Caesar
* Tony Shalhoub as German
* Alan Arkin as Detective Hugo
* Dean Norris as Cop on the Beat
* Ken Marino as Sequencing technician
* Cynthia Martells as Cavendish
* Gabrielle Reece as Gattaca Trainer

==Production==
 
  complex]]
 ]] The Forum Kramer Junction Solar Electric Generating Station.

===Design=== turbine cars are based on 1960s car models like Rover P6, Citroën DS19 and Studebaker Avanti,  and futuristic buildings represent modern architecture of the 1950s.

==Release== The Devils Kiss the Seven Years in Tibet.  Over the first weekend the film brought in $4.3 million. It ended its theatrical run with a domestic total of $12.5 million against a reported production budget of $36 million. 

===Home media===
Gattaca was released on DVD on July 1, 1998,  and was also released on Superbit DVD. {{cite web
 | url		= http://www.amazon.com/dp/B00005R23Z
 | title	= Amazon.com: Gattaca (Superbit Collection) (1997)
 | publisher	= Amazon.com
 | accessdate	= April 7, 2013
}}  Special Edition DVD and Blu-ray Disc|Blu-ray versions were released on March 11, 2008.   Both editions contain a deleted scene featuring historical figures like Einstein, Lincoln, etc., who according to the texts are supposedly genetically deficient. 

==Critical reception== rating average of 7.1/10. The critical consensus states that "Intelligent and scientifically provocative, Gattaca is an absorbing sci fi drama that poses important interesting ethical questions about the nature of science."  On Metacritic, the film received "generally favorable reviews" with a score of 64 out of 100.  Roger Ebert stated, "This is one of the smartest and most provocative of science fiction films, a thriller with ideas."  James Berardinelli praised it for "energy and tautness" and its "thought-provoking script and thematic richness." 

Despite critical acclaim, Gattaca was not a box office success but it is said to have crystallized the debate over the controversial topic of human genetic engineering.    The films dystopian depiction of "genoism" has been cited by many bioethicists and laymen in support of their hesitancy about, or opposition to, eugenics and the societal acceptance of the genetic determinism|genetic-determinist ideology that may frame it.  In a 1997 review of the film for the journal Nature Genetics, molecular biologist Lee M. Silver stated that "Gattaca is a film that all geneticists should see if for no other reason than to understand the perception of our trade held by so many of the public-at-large". 
 bioethicist James James Hughes criticized the premise and influence of the film Gattaca,  arguing that:
# Astronaut-training programs are entirely justified in attempting to screen out people with heart problems for safety reasons;
# In the United States, people are already discriminated against by insurance companies on the basis of their propensities to disease despite the fact that genetic enhancement is not yet available;
# Rather than banning genetic testing or genetic enhancement, society needs genetic information privacy laws that allow justified forms of genetic testing and data aggregation, but forbid those that are judged to result in genetic discrimination (such as the U.S. Genetic Information Nondiscrimination Act signed into law on May 21, 2008). Citizens should then be able to make a complaint to the appropriate authority if they believe they have been discriminated against because of their genotype.

===Accolades===
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 90%;"
|- bgcolor="#CCCCCC" align="center"
! colspan="4" style="background: LightSteelBlue;" | Awards
|- bgcolor="#CCCCCC" align="center"
! Award
! Category
! Name
! Outcome
|- Academy Awards Best Art Direction
| Jan Roelfs Nancy Nye
|  
|- Art Directors Guild Award Excellence in Production Design
| Jan Roelfs Sarah Knowles Natalie Richards
|  
|-
|style="border-top:10px black" rowspan=1| Bogey Awards Bogey Award
| 
|-
|style="border-top:10px black" rowspan=2| Festival international du film fantastique de Gérardmer|Gérardmer Film Festival
| Special Jury Prize Andrew Niccol
| 
|-
| Fun Trophy
| 
|- Golden Globe Awards Best Original Score
| Michael Nyman
|  
|-
|style="border-top:10px black" rowspan=1| Hugo Awards Best Dramatic Presentation
| Andrew Niccol
|  
|-
|style="border-top:10px black" rowspan=1| London Film Critics Circle Awards Best Screenwriter of the Year
| Andrew Niccol
|  
|-
| Paris Film Festival Grand Prix
|  
|-
|style="border-top:10px black" rowspan=1| Satellite Awards Best Art Direction and Production Design
| Jan Roelfs
|  
|-
|style="border-top:10px black" rowspan=3| Saturn Awards Best Costume
| Coleen Atwood
|  
|- Best Music
| Michael Nyman
|  
|- Best Home Video Release
|  
|- Sitges – Catalonian International Film Festival
| Best Motion Picture
| Andrew Niccols
|  
|-
| Best Original Soundtrack
| Michael Nyman
|  
|-
|}

==Soundtrack==
{{Infobox album  
| Name = Gattaca
| Type = soundtrack
| Artist = Michael Nyman
| Cover = 
| Released =   minimalism
| Length = 54:55
| Label = Virgin Records America
| Producer = Michael Nyman
| Last album = Concertos (1997 album)|Concertos 1997
| This album = Gattaca 1997
| Next album = The Suit and the Photograph 1998
| Misc =
   {{Listen
   |filename=Michael_Nyman-Gattaca-The_Other_Side.ogg
   |title=The Other Side
   |description=From the Gattaca soundtrack by Michael Nyman
   |format=Ogg}}
}} score for Gattaca was composed by Michael Nyman, and the original soundtrack was released on October 21, 1997. 

==Legacy==

===Television series===
On October 30, 2009, Variety (magazine)|Variety reported that Sony Pictures was developing a television adaptation of the feature film as a one-hour police procedural set in the future. The show was to be written by Gil Grant, who has written for 24 (TV series)|24 and NCIS (TV series)|NCIS. 

===Political references=== Wikipedia entry on Gattaca in a speech at Liberty University on October 28, 2013 supporting Virginia Attorney General Ken Cuccinellis campaign for Governor of Virginia. Paul accused pro-choice politicians of advocating eugenics in a manner similar to the events in Gattaca.  

==See also==
* List of films featuring surveillance Transhumanism § Genetic divide

 

==References==
{{Reflist|30em|refs=
 {{cite web
|url=http://www.metrotimes.com/editorial/review.asp?id=51785
|title=Gattaca — Movie Review
|publisher=Metro times
|accessdate=2008-06-01
|last=
|first=}} 
 " , Mick LaSalle, San Francisco Chronicle, Friday, October 24, 1997, URL retrieved 19th February 2009 
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
  |title=Gattaca soundtrack overview|accessdate=2008-10-30|publisher=AllMusic}} 
 {{cite web
|url=http://www.soundtrack.net/albums/database/?id=968
|title=Gattaca soundtrack
|accessdate=2008-09-06
|publisher=SoundtrackNet, LLC}} 
}}

==Further reading==

Jon Frauley. 2010. "Biopolitics and the Governance of Genetic Capital in Gattaca." Criminology, Deviance and the Silver Screen: The Fictional Reality and the Criminological Imagination. New York: Palgrave Macmillan.

==External links==
 
*  
*  
*  
*  
*  
*  
*  
*  

 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 